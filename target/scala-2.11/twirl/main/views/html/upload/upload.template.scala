
package views.html.upload

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object upload_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

     object upload_Scope1 {
import service.UserProvider

class upload extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[UserProvider,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*2.2*/(userProvider: UserProvider):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.30*/("""

"""),_display_(/*4.2*/main(userProvider, "UniCarb-DB")/*4.34*/{_display_(Seq[Any](format.raw/*4.35*/("""
"""),format.raw/*5.1*/("""<script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.8.0/xlsx.full.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.8.0/jszip.js"></script>
    <div class="upload">
        <div class="container-fluid content">
            <div class="searchdiv">
                <h4>PubMed ID</h4>
                <div class="search-field short">
                    <input id="pid-input" type="number"/>
                </div>
                <button type="button" class="btn btn-primary" onclick="fetchDetails()">
                    Fetch details from PubMed
                </button>

                <h4>Title</h4>
                <div class="search-field long">
                    <input id="title-input" type="text"/>
                </div>

                <h4>Authors</h4>
                <div class="search-field long">
                    <input id="authors-input" type="text" class="search-field"/>
                </div>

                <h4>Journal</h4>
                <div class="search-field long">
                    <input id="journal-input" type="text"/>
                </div>

                <h4>Year</h4>
                <div class="search-field short">
                    <input id="year-input" type="text"/>
                </div>
            </div>
            <div class="searchdiv">
                <h4>Mirage File (.xls, .xlsx)</h4>
                <div class="search-field">
                    <input id="mirage-input" type="file" accept=".xls, .xlsx" onchange="parseMirage()"/>
                </div>
                <div id="mirage-output">

                </div>
            </div>
            <div class="searchdiv">
                <h4>GlycoWorkbench Files (.gwp)</h4>
                <div class="search-field">
                    <input id="gwp-input" type="file" multiple accept=".gwp" onchange="parseGwpNotes()"/>
                </div>
                <div id="gwp-mirage-results">

                </div>
            </div>
        </div>
    </div>

    <script>
        var gwpFieldCount = 1;

        function fetchDetails()"""),format.raw/*62.32*/("""{"""),format.raw/*62.33*/("""
            """),format.raw/*63.13*/("""// Example: 27456831
           var id = $("#pid-input").val();

           if(id != "")"""),format.raw/*66.24*/("""{"""),format.raw/*66.25*/("""
               """),format.raw/*67.16*/("""$.getJSON("http://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi", """),format.raw/*67.88*/("""{"""),format.raw/*67.89*/("""
                        """),format.raw/*68.25*/(""""db": "pubmed",
                        "retmode": "json",
                        "id": id,
                        "rettype": "abstract"
                    """),format.raw/*72.21*/("""}"""),format.raw/*72.22*/(""", function(data)"""),format.raw/*72.38*/("""{"""),format.raw/*72.39*/("""
                        """),format.raw/*73.25*/("""if(data.result.uids.length>0) """),format.raw/*73.55*/("""{"""),format.raw/*73.56*/("""
                            """),format.raw/*74.29*/("""var authorArray = data.result[id].authors;
                            var authorStr = "";

                            $.each(authorArray, function(index, author)"""),format.raw/*77.72*/("""{"""),format.raw/*77.73*/("""
                                """),format.raw/*78.33*/("""if(index == authorArray.length -1)
                                    authorStr += " and "
                                else if(authorStr != "") authorStr += ", ";

                                authorStr += author.name;
                            """),format.raw/*83.29*/("""}"""),format.raw/*83.30*/(""");

                            $("#authors-input").val(authorStr);

                            var title = data.result[id].title;
                            $("#title-input").val(title);

                            var journal = data.result[id].fulljournalname;
                            $("#journal-input").val(journal);

                            var year = data.result[id].pubdate.split(" ")[0];
                            $("#year-input").val(year);
                        """),format.raw/*95.25*/("""}"""),format.raw/*95.26*/("""
                    """),format.raw/*96.21*/("""}"""),format.raw/*96.22*/("""
               """),format.raw/*97.16*/(""");
           """),format.raw/*98.12*/("""}"""),format.raw/*98.13*/("""
        """),format.raw/*99.9*/("""}"""),format.raw/*99.10*/("""

        """),format.raw/*101.9*/("""function parseMirage()"""),format.raw/*101.31*/("""{"""),format.raw/*101.32*/("""
            """),format.raw/*102.13*/("""var x = document.getElementById('mirage-input').files[0];

            var reader = new FileReader();

            reader.onload = function(e)"""),format.raw/*106.40*/("""{"""),format.raw/*106.41*/("""
                """),format.raw/*107.17*/("""var data = e.target.result;
                var workbook = XLSX.read(data, """),format.raw/*108.48*/("""{"""),format.raw/*108.49*/("""type : 'binary'"""),format.raw/*108.64*/("""}"""),format.raw/*108.65*/(""");

                var csv = XLSX.utils.sheet_to_csv(workbook.Sheets["MIRAGE Form"]);

                // Weird POC example of possible solution time. Due to bugs in XLSX SheetJS library and in jquery.csv,
                // I simply cannot get this to work any other way. Good luck!
                // TODO [EK]: Look at making this work with just using row numbers.
                var position = csv.search("Date stamp");

                var labelStartSubstr = csv.substr(position, csv.length-1);
                var valueStartSubstr = labelStartSubstr.substr(labelStartSubstr.search(",") + 1 , labelStartSubstr.length-1);
                var value = valueStartSubstr.substr(0, valueStartSubstr.search(","))
                console.log(valueStartSubstr);
                console.log(value);
            """),format.raw/*122.13*/("""}"""),format.raw/*122.14*/(""";

            reader.onerror = function(ex)"""),format.raw/*124.42*/("""{"""),format.raw/*124.43*/("""
                """),format.raw/*125.17*/("""console.log(ex);
            """),format.raw/*126.13*/("""}"""),format.raw/*126.14*/(""";

            reader.readAsBinaryString(x);
        """),format.raw/*129.9*/("""}"""),format.raw/*129.10*/(""";

        function parseGwpNotes() """),format.raw/*131.34*/("""{"""),format.raw/*131.35*/("""
            """),format.raw/*132.13*/("""var gwp = document.getElementById('gwp-input').files[0];

            var reader = new FileReader();

            reader.onload = function(e)"""),format.raw/*136.40*/("""{"""),format.raw/*136.41*/("""
                """),format.raw/*137.17*/("""var text = e.target.result;
                xmlDoc = $.parseXML(text);
                $xml = $(xmlDoc);
                notes = $xml.find("Notes");
                fields = notes[0].innerHTML.split("\n");

                $.each(fields, function(index, field)"""),format.raw/*143.54*/("""{"""),format.raw/*143.55*/("""
                    """),format.raw/*144.21*/("""var splitField = field.split(": ");
                    var name = splitField[0];
                    var value = splitField[1];

                    if(splitField[0] != "") """),format.raw/*148.45*/("""{"""),format.raw/*148.46*/(""" """),format.raw/*148.47*/("""// guard against empty lines.
                        if(!value) value = "";
                        $("#gwp-mirage-results").append(
                            "<span class='gwp-field'><label>" +
                                "<b>" + name + ": </b>" +
                                "<input required type='text' value='" + value.trim() + "'/>" +
                            "</label></span>");
                    """),format.raw/*155.21*/("""}"""),format.raw/*155.22*/("""
                """),format.raw/*156.17*/("""}"""),format.raw/*156.18*/(""");
            """),format.raw/*157.13*/("""}"""),format.raw/*157.14*/(""";

            reader.onerror = function(ex)"""),format.raw/*159.42*/("""{"""),format.raw/*159.43*/("""
                """),format.raw/*160.17*/("""console.log(ex);
            """),format.raw/*161.13*/("""}"""),format.raw/*161.14*/(""";

            reader.readAsText(gwp);
        """),format.raw/*164.9*/("""}"""),format.raw/*164.10*/("""

    """),format.raw/*166.5*/("""</script>
""")))}))
      }
    }
  }

  def render(userProvider:UserProvider): play.twirl.api.HtmlFormat.Appendable = apply(userProvider)

  def f:((UserProvider) => play.twirl.api.HtmlFormat.Appendable) = (userProvider) => apply(userProvider)

  def ref: this.type = this

}


}
}

/**/
object upload extends upload_Scope0.upload_Scope1.upload
              /*
                  -- GENERATED --
                  DATE: Wed Jun 19 09:50:52 UTC 2019
                  SOURCE: /app/app/views/upload/upload.scala.html
                  HASH: 7ae1d48d2576f21aee0c87325a10334eb5099a85
                  MATRIX: 817->30|940->58|968->61|1008->93|1046->94|1073->95|3193->2187|3222->2188|3263->2201|3379->2289|3408->2290|3452->2306|3552->2378|3581->2379|3634->2404|3821->2563|3850->2564|3894->2580|3923->2581|3976->2606|4034->2636|4063->2637|4120->2666|4311->2829|4340->2830|4401->2863|4684->3118|4713->3119|5228->3606|5257->3607|5306->3628|5335->3629|5379->3645|5421->3659|5450->3660|5486->3669|5515->3670|5553->3680|5604->3702|5634->3703|5676->3716|5847->3858|5877->3859|5923->3876|6027->3951|6057->3952|6101->3967|6131->3968|6968->4776|6998->4777|7071->4821|7101->4822|7147->4839|7205->4868|7235->4869|7316->4922|7346->4923|7411->4959|7441->4960|7483->4973|7653->5114|7683->5115|7729->5132|8018->5392|8048->5393|8098->5414|8301->5588|8331->5589|8361->5590|8809->6009|8839->6010|8885->6027|8915->6028|8959->6043|8989->6044|9062->6088|9092->6089|9138->6106|9196->6135|9226->6136|9301->6183|9331->6184|9365->6190
                  LINES: 30->2|35->2|37->4|37->4|37->4|38->5|95->62|95->62|96->63|99->66|99->66|100->67|100->67|100->67|101->68|105->72|105->72|105->72|105->72|106->73|106->73|106->73|107->74|110->77|110->77|111->78|116->83|116->83|128->95|128->95|129->96|129->96|130->97|131->98|131->98|132->99|132->99|134->101|134->101|134->101|135->102|139->106|139->106|140->107|141->108|141->108|141->108|141->108|155->122|155->122|157->124|157->124|158->125|159->126|159->126|162->129|162->129|164->131|164->131|165->132|169->136|169->136|170->137|176->143|176->143|177->144|181->148|181->148|181->148|188->155|188->155|189->156|189->156|190->157|190->157|192->159|192->159|193->160|194->161|194->161|197->164|197->164|199->166
                  -- GENERATED --
              */
          