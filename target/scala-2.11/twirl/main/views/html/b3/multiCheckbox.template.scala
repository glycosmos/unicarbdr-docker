
package views.html.b3

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object multiCheckbox_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class multiCheckbox extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template3[Array[scala.Tuple2[Field, Seq[scala.Tuple2[Symbol, Any]]]],Array[scala.Tuple2[Symbol, Any]],b3.B3FieldConstructor,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(fieldsWithArgs: (Field, Seq[(Symbol,Any)])*)(globalArgs: (Symbol,Any)*)(implicit fc: b3.B3FieldConstructor):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.110*/("""
"""),_display_(/*2.2*/b3/*2.4*/.multifield(fieldsWithArgs.map(_._1):_*)/*2.44*/(globalArgs, fieldsWithArgs.map(_._2).flatten)/*2.90*/ { implicit cfc =>_display_(Seq[Any](format.raw/*2.108*/("""
    """),format.raw/*3.5*/("""<div """),_display_(/*3.11*/toHtmlArgs(bs.Args.inner(globalArgs).toMap)),format.raw/*3.54*/(""">
        """),_display_(/*4.10*/fieldsWithArgs/*4.24*/.map/*4.28*/ { case (field, fieldArgs) =>_display_(Seq[Any](format.raw/*4.57*/("""
            """),_display_(/*5.14*/b3/*5.16*/.checkbox(field, fieldArgs:_*)),format.raw/*5.46*/("""
        """)))}),format.raw/*6.10*/("""
    """),format.raw/*7.5*/("""</div>
""")))}))
      }
    }
  }

  def render(fieldsWithArgs:Array[scala.Tuple2[Field, Seq[scala.Tuple2[Symbol, Any]]]],globalArgs:Array[scala.Tuple2[Symbol, Any]],fc:b3.B3FieldConstructor): play.twirl.api.HtmlFormat.Appendable = apply(fieldsWithArgs:_*)(globalArgs:_*)(fc)

  def f:((Array[scala.Tuple2[Field, Seq[scala.Tuple2[Symbol, Any]]]]) => (Array[scala.Tuple2[Symbol, Any]]) => (b3.B3FieldConstructor) => play.twirl.api.HtmlFormat.Appendable) = (fieldsWithArgs) => (globalArgs) => (fc) => apply(fieldsWithArgs:_*)(globalArgs:_*)(fc)

  def ref: this.type = this

}


}

/**/
object multiCheckbox extends multiCheckbox_Scope0.multiCheckbox
              /*
                  -- GENERATED --
                  DATE: Wed Jun 19 09:50:50 UTC 2019
                  SOURCE: /app/app/views/b3/multiCheckbox.scala.html
                  HASH: a0b3adc179b147db87d7719089e254ad269dcf46
                  MATRIX: 871->1|1075->109|1102->111|1111->113|1159->153|1213->199|1269->217|1300->222|1332->228|1395->271|1432->282|1454->296|1466->300|1532->329|1572->343|1582->345|1632->375|1672->385|1703->390
                  LINES: 27->1|32->1|33->2|33->2|33->2|33->2|33->2|34->3|34->3|34->3|35->4|35->4|35->4|35->4|36->5|36->5|36->5|37->6|38->7
                  -- GENERATED --
              */
          