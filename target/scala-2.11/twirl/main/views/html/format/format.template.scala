
package views.html.format

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object format_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class format extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template0[play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply():play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.1*/("""<!--
  ~ Copyright (c) 2014. Copyright (c) 2014. Matthew Campbell
  ~
  ~ The program can not be distributed to others with out the consent of the copyright holder, until such a time that the copyright holder has released the code for public use.
  -->

<div class="info">
    <h3>Structure Format</h3>
    <a href="/format/cfg"><span class='label label-dark'><span class='icon-adjust icon-white'></span> SNFG</span></a>
    <a href="/format/uoxf"><span class='label label-dark'><span class='icon-stop icon-white'></span> Oxford</span></a>
    <a href="/format/iupac"><span class='label label-dark'><span class='icon-stop icon-white'></span> Text</span></a>
</div>
"""))
      }
    }
  }

  def render(): play.twirl.api.HtmlFormat.Appendable = apply()

  def f:(() => play.twirl.api.HtmlFormat.Appendable) = () => apply()

  def ref: this.type = this

}


}

/**/
object format extends format_Scope0.format
              /*
                  -- GENERATED --
                  DATE: Wed Jun 19 09:50:52 UTC 2019
                  SOURCE: /app/app/views/format/format.scala.html
                  HASH: 7a7df48df04afa276eed2fc8c874f645780f1b0a
                  MATRIX: 836->0
                  LINES: 32->1
                  -- GENERATED --
              */
          