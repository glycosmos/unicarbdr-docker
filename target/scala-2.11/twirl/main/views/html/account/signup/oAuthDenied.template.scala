
package views.html.account.signup

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object oAuthDenied_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

     object oAuthDenied_Scope1 {
import service.UserProvider

class oAuthDenied extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template2[UserProvider,String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*2.2*/(userProvider: UserProvider, providerKey: String):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.51*/("""

"""),_display_(/*4.2*/main(userProvider, "Access denied")/*4.37*/{_display_(Seq[Any](format.raw/*4.38*/("""

"""),format.raw/*6.1*/("""<div class="container-fluid content">
    <div class="row-fluid info">
        <div class="row-fluid">
            <div class="page-header row-fluid">
                <h1 class="subheader span8">OAuth access denied</h1>
            </div>
            <p>If you want to use UniCarb-DB with OAuth, you must accept the connection.<br/>
            </p>
            <p>If you rather not like to do this, you can also <a href=""""),_display_(/*14.74*/routes/*14.80*/.HomeController.signup),format.raw/*14.102*/("""">sign up with a username and password instead</a>.

        </div>
    </div>
</div>
""")))}),format.raw/*19.2*/("""

"""))
      }
    }
  }

  def render(userProvider:UserProvider,providerKey:String): play.twirl.api.HtmlFormat.Appendable = apply(userProvider,providerKey)

  def f:((UserProvider,String) => play.twirl.api.HtmlFormat.Appendable) = (userProvider,providerKey) => apply(userProvider,providerKey)

  def ref: this.type = this

}


}
}

/**/
object oAuthDenied extends oAuthDenied_Scope0.oAuthDenied_Scope1.oAuthDenied
              /*
                  -- GENERATED --
                  DATE: Wed Jun 19 09:50:52 UTC 2019
                  SOURCE: /app/app/views/account/signup/oAuthDenied.scala.html
                  HASH: c0d9e0370af948dac4b326dc827f4686b047df48
                  MATRIX: 847->30|991->79|1019->82|1062->117|1100->118|1128->120|1578->543|1593->549|1637->571|1754->658
                  LINES: 30->2|35->2|37->4|37->4|37->4|39->6|47->14|47->14|47->14|52->19
                  -- GENERATED --
              */
          