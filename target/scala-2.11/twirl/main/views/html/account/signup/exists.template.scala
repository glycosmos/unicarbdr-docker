
package views.html.account.signup

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object exists_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

     object exists_Scope1 {
import service.UserProvider

class exists extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[UserProvider,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*2.2*/(userProvider: UserProvider):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.30*/("""

"""),_display_(/*4.2*/main(userProvider, "User exists")/*4.35*/{_display_(Seq[Any](format.raw/*4.36*/("""
    """),format.raw/*5.5*/("""<div class="container-fluid content">
        <div class="row-fluid info">
            <div class="row-fluid">
                <div class="page-header row-fluid">
                    <h1 class="subheader span8">User exists</h1>
                </div>
                <p>This user already exists.</p>
            </div>
        </div>
    </div>
""")))}),format.raw/*15.2*/("""








"""))
      }
    }
  }

  def render(userProvider:UserProvider): play.twirl.api.HtmlFormat.Appendable = apply(userProvider)

  def f:((UserProvider) => play.twirl.api.HtmlFormat.Appendable) = (userProvider) => apply(userProvider)

  def ref: this.type = this

}


}
}

/**/
object exists extends exists_Scope0.exists_Scope1.exists
              /*
                  -- GENERATED --
                  DATE: Wed Jun 19 09:50:52 UTC 2019
                  SOURCE: /app/app/views/account/signup/exists.scala.html
                  HASH: 2ba2670a64068c9dcc66c7e23177c9564f750b20
                  MATRIX: 825->30|948->58|976->61|1017->94|1055->95|1086->100|1462->446
                  LINES: 30->2|35->2|37->4|37->4|37->4|38->5|48->15
                  -- GENERATED --
              */
          