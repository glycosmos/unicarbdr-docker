
package views.html.account.signup

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object password_reset_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

     object password_reset_Scope1 {
import service.UserProvider

class password_reset extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template2[UserProvider,Form[controllers.Signup.PasswordReset],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*2.2*/(userProvider: UserProvider, resetForm: Form[controllers.Signup.PasswordReset]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {
import helper._
implicit def /*5.2*/implicitFieldConstructor/*5.26*/ = {{ FieldConstructor(twitterBootstrapBasic.f) }};
Seq[Any](format.raw/*2.81*/("""

"""),format.raw/*5.75*/("""

"""),_display_(/*7.2*/main(userProvider, "Password reset")/*7.38*/{_display_(Seq[Any](format.raw/*7.39*/("""

"""),format.raw/*9.1*/("""<div class="container-fluid content">
    <div class="row-fluid info">
        <div class="row-fluid">
            <div class="page-header row-fluid">
                <h1 class="subheader span8">Reset password</h1>
            </div>
            <p>"""),_display_(/*15.17*/form(routes.Signup.doResetPassword, 'class -> "form-horizontal", 'role -> "form")/*15.98*/ {_display_(Seq[Any](format.raw/*15.100*/("""

                """),_display_(/*17.18*/if(resetForm.hasGlobalErrors)/*17.47*/ {_display_(Seq[Any](format.raw/*17.49*/("""
            """),format.raw/*18.13*/("""<p class="alert alert-danger">"""),_display_(/*18.44*/resetForm/*18.53*/.globalError.message),format.raw/*18.73*/("""</p>
            """)))}),format.raw/*19.14*/("""
            """),_display_(/*20.14*/input(
            resetForm("token"),
            '_label -> "",
            '_showConstraints -> false,
            '_class -> "search-field"
            )/*25.14*/ { (id, name, value, args) =>_display_(Seq[Any](format.raw/*25.43*/(""" """),format.raw/*25.44*/("""<input type="hidden" value=""""),_display_(/*25.73*/value),format.raw/*25.78*/("""" name=""""),_display_(/*25.87*/name),format.raw/*25.91*/("""" id=""""),_display_(/*25.98*/id),format.raw/*25.100*/("""" """),_display_(/*25.103*/toHtmlArgs(args)),format.raw/*25.119*/(""">  """)))}),format.raw/*25.123*/("""

            """),_display_(/*27.14*/auth/*27.18*/._passwordPartial(resetForm)),format.raw/*27.46*/("""

            """),format.raw/*29.13*/("""<br><br>
            <button type="submit" class="btn btn-primary u-form-group">Reset my password</button>
            """)))}),format.raw/*31.14*/("""</p>
        </div>
    </div>
</div>

""")))}),format.raw/*36.2*/("""
"""))
      }
    }
  }

  def render(userProvider:UserProvider,resetForm:Form[controllers.Signup.PasswordReset]): play.twirl.api.HtmlFormat.Appendable = apply(userProvider,resetForm)

  def f:((UserProvider,Form[controllers.Signup.PasswordReset]) => play.twirl.api.HtmlFormat.Appendable) = (userProvider,resetForm) => apply(userProvider,resetForm)

  def ref: this.type = this

}


}
}

/**/
object password_reset extends password_reset_Scope0.password_reset_Scope1.password_reset
              /*
                  -- GENERATED --
                  DATE: Wed Jun 19 09:50:52 UTC 2019
                  SOURCE: /app/app/views/account/signup/password_reset.scala.html
                  HASH: a10e0239ac55bcd5a2a7bbe6dc00ca9dc05e26ae
                  MATRIX: 888->30|1069->129|1101->153|1180->109|1209->202|1237->205|1281->241|1319->242|1347->244|1624->494|1714->575|1755->577|1801->596|1839->625|1879->627|1920->640|1978->671|1996->680|2037->700|2086->718|2127->732|2293->889|2360->918|2389->919|2445->948|2471->953|2507->962|2532->966|2566->973|2590->975|2621->978|2659->994|2695->998|2737->1013|2750->1017|2799->1045|2841->1059|2992->1179|3062->1219
                  LINES: 30->2|34->5|34->5|35->2|37->5|39->7|39->7|39->7|41->9|47->15|47->15|47->15|49->17|49->17|49->17|50->18|50->18|50->18|50->18|51->19|52->20|57->25|57->25|57->25|57->25|57->25|57->25|57->25|57->25|57->25|57->25|57->25|57->25|59->27|59->27|59->27|61->29|63->31|68->36
                  -- GENERATED --
              */
          