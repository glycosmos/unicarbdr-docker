
package views.html.account.signup.email

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object verify_email_en_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class verify_email_en extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template4[String,String,String,String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(verificationUrl: String, token: String, name: String, email: String):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.71*/("""

"""),format.raw/*3.1*/("""<div class="container-fluid content">
    <div class="row-fluid info">
        <div class="row-fluid">
            <div class="page-header row-fluid">
                <h1 class="subheader span8">Verify E-mail</h1>
            </div>
                """),_display_(/*9.18*/name),format.raw/*9.22*/(""",
                <p>You recently signed up for Access to the submission tool of UniCarb-DB.<br />
                    <br />
                    Follow this link to <a href=""""),_display_(/*12.51*/verificationUrl),format.raw/*12.66*/("""">activate your account</a> now.
                </p>
                <br />
                <p>
                    Cheers,<br />
                    <i>The UniCarb-DB team</i>
                </p>
        </div>
    </div>
</div>




"""))
      }
    }
  }

  def render(verificationUrl:String,token:String,name:String,email:String): play.twirl.api.HtmlFormat.Appendable = apply(verificationUrl,token,name,email)

  def f:((String,String,String,String) => play.twirl.api.HtmlFormat.Appendable) = (verificationUrl,token,name,email) => apply(verificationUrl,token,name,email)

  def ref: this.type = this

}


}

/**/
object verify_email_en extends verify_email_en_Scope0.verify_email_en
              /*
                  -- GENERATED --
                  DATE: Wed Jun 19 09:50:52 UTC 2019
                  SOURCE: /app/app/views/account/signup/email/verify_email_en.scala.html
                  HASH: 4620782278e9500f72f71a5778098cc0d5a5ad52
                  MATRIX: 807->1|971->70|999->72|1275->322|1299->326|1502->502|1538->517
                  LINES: 27->1|32->1|34->3|40->9|40->9|43->12|43->12
                  -- GENERATED --
              */
          