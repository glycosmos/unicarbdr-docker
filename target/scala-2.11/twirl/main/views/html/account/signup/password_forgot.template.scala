
package views.html.account.signup

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object password_forgot_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

     object password_forgot_Scope1 {
import service.UserProvider

class password_forgot extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template2[UserProvider,Form[providers.MyUsernamePasswordAuthProvider.MyIdentity],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*2.2*/(userProvider: UserProvider, emailForm: Form[providers.MyUsernamePasswordAuthProvider.MyIdentity]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {
import helper._
implicit def /*5.2*/implicitFieldConstructor/*5.26*/ = {{ FieldConstructor(twitterBootstrapBasic.f) }};
Seq[Any](format.raw/*2.100*/("""

"""),format.raw/*5.75*/("""

"""),_display_(/*7.2*/main(userProvider, "Password forgotten")/*7.42*/{_display_(Seq[Any](format.raw/*7.43*/("""
"""),format.raw/*8.1*/("""<div class="container-fluid content">
    <div class="row-fluid info">
        <div class="row-fluid">
            <div class="page-header row-fluid">
                <h1 class="subheader span8">Password forgotten</h1>
            </div>
            <p>
                """),_display_(/*15.18*/form(routes.Signup.doForgotPassword, 'class -> "form-inline", 'role -> "form")/*15.96*/ {_display_(Seq[Any](format.raw/*15.98*/("""
                    """),_display_(/*16.22*/if(emailForm.hasGlobalErrors)/*16.51*/ {_display_(Seq[Any](format.raw/*16.53*/("""
                        """),format.raw/*17.25*/("""<p class="alert alert-danger">"""),_display_(/*17.56*/emailForm/*17.65*/.globalError.message),format.raw/*17.85*/("""</p>
                    """)))}),format.raw/*18.22*/("""

                    """),format.raw/*20.21*/("""<br>
                    """),_display_(/*21.22*/auth/*21.26*/._emailPartial(emailForm)),format.raw/*21.51*/("""
                    """),format.raw/*22.21*/("""<button type="submit" class="btn btn-primary u-form-group">Send reset instructions</button>
                """)))}),format.raw/*23.18*/("""
            """),format.raw/*24.13*/("""</p>

        </div>
    </div>
</div>

""")))}),format.raw/*30.2*/("""
"""))
      }
    }
  }

  def render(userProvider:UserProvider,emailForm:Form[providers.MyUsernamePasswordAuthProvider.MyIdentity]): play.twirl.api.HtmlFormat.Appendable = apply(userProvider,emailForm)

  def f:((UserProvider,Form[providers.MyUsernamePasswordAuthProvider.MyIdentity]) => play.twirl.api.HtmlFormat.Appendable) = (userProvider,emailForm) => apply(userProvider,emailForm)

  def ref: this.type = this

}


}
}

/**/
object password_forgot extends password_forgot_Scope0.password_forgot_Scope1.password_forgot
              /*
                  -- GENERATED --
                  DATE: Wed Jun 19 09:50:52 UTC 2019
                  SOURCE: /app/app/views/account/signup/password_forgot.scala.html
                  HASH: 00481ad3365430f25ef92caa61d5dc0138ad3760
                  MATRIX: 910->30|1110->148|1142->172|1222->128|1251->221|1279->224|1327->264|1365->265|1392->266|1690->537|1777->615|1817->617|1866->639|1904->668|1944->670|1997->695|2055->726|2073->735|2114->755|2171->781|2221->803|2274->829|2287->833|2333->858|2382->879|2522->988|2563->1001|2634->1042
                  LINES: 30->2|34->5|34->5|35->2|37->5|39->7|39->7|39->7|40->8|47->15|47->15|47->15|48->16|48->16|48->16|49->17|49->17|49->17|49->17|50->18|52->20|53->21|53->21|53->21|54->22|55->23|56->24|62->30
                  -- GENERATED --
              */
          