
package views.html.account.signup

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object no_token_or_invalid_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

     object no_token_or_invalid_Scope1 {
import service.UserProvider

class no_token_or_invalid extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[UserProvider,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*2.2*/(userProvider: UserProvider):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.30*/("""

"""),_display_(/*4.2*/main(userProvider, "UniCarb-DB")/*4.34*/{_display_(Seq[Any](format.raw/*4.35*/("""
"""),format.raw/*5.1*/("""<div class="container-fluid content">
    <div class="row-fluid info">
        <div class="row-fluid">
            <div class="page-header row-fluid">
                <h1 class="subheader span8">Token Error</h1>
            </div>
            <p><p>The given token has either expired or does not exist.</p></p>
        </div>
    </div>
</div>

""")))}),format.raw/*16.2*/("""
"""))
      }
    }
  }

  def render(userProvider:UserProvider): play.twirl.api.HtmlFormat.Appendable = apply(userProvider)

  def f:((UserProvider) => play.twirl.api.HtmlFormat.Appendable) = (userProvider) => apply(userProvider)

  def ref: this.type = this

}


}
}

/**/
object no_token_or_invalid extends no_token_or_invalid_Scope0.no_token_or_invalid_Scope1.no_token_or_invalid
              /*
                  -- GENERATED --
                  DATE: Wed Jun 19 09:50:52 UTC 2019
                  SOURCE: /app/app/views/account/signup/no_token_or_invalid.scala.html
                  HASH: 1982de9afcf125599e8d61493436007a8676dd40
                  MATRIX: 864->30|987->58|1015->61|1055->93|1093->94|1120->95|1496->441
                  LINES: 30->2|35->2|37->4|37->4|37->4|38->5|49->16
                  -- GENERATED --
              */
          