
package views.html.account.signup

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object unverified_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

     object unverified_Scope1 {
import service.UserProvider

class unverified extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[UserProvider,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*2.2*/(userProvider: UserProvider):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.30*/("""

"""),_display_(/*4.2*/main(userProvider, "Unverified user")/*4.39*/{_display_(Seq[Any](format.raw/*4.40*/("""

"""),format.raw/*6.1*/("""<div class="container-fluid content">
    <div class="row-fluid info">
        <div class="row-fluid">
            <div class="page-header row-fluid">
                <h1 class="subheader span8">Verify your e-mail</h1>
            </div>
            <p>
                Before you can use the submission tool, you first need to verify your e-mail address.</br>
                An e-mail has been sent to the registered address. Please follow the embedded link to activate your account.
                <br/>
            </p>
        </div>
    </div>
</div>

""")))}),format.raw/*21.2*/("""

"""))
      }
    }
  }

  def render(userProvider:UserProvider): play.twirl.api.HtmlFormat.Appendable = apply(userProvider)

  def f:((UserProvider) => play.twirl.api.HtmlFormat.Appendable) = (userProvider) => apply(userProvider)

  def ref: this.type = this

}


}
}

/**/
object unverified extends unverified_Scope0.unverified_Scope1.unverified
              /*
                  -- GENERATED --
                  DATE: Wed Jun 19 09:50:52 UTC 2019
                  SOURCE: /app/app/views/account/signup/unverified.scala.html
                  HASH: d00a4d25d6e89495c5a1d3f474938921513ceb78
                  MATRIX: 837->30|960->58|988->61|1033->98|1071->99|1099->101|1689->661
                  LINES: 30->2|35->2|37->4|37->4|37->4|39->6|54->21
                  -- GENERATED --
              */
          