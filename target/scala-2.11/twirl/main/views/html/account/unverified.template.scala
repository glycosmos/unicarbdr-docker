
package views.html.account

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object unverified_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

     object unverified_Scope1 {
import service.UserProvider

class unverified extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[UserProvider,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*2.2*/(userProvider: UserProvider):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.30*/("""

"""),_display_(/*4.2*/main(userProvider, "UniCarb-DB")/*4.34*/{_display_(Seq[Any](format.raw/*4.35*/("""

"""),format.raw/*6.1*/("""<h1>"""),_display_(/*6.6*/Messages("playauthenticate.verify.account.title")),format.raw/*6.55*/("""</h1>
<p>
    """),_display_(/*8.6*/Messages("playauthenticate.verify.account.before")),format.raw/*8.56*/(""" """),format.raw/*8.57*/("""<a href=""""),_display_(/*8.67*/routes/*8.73*/.Account.verifyEmail),format.raw/*8.93*/("""">"""),_display_(/*8.96*/Messages("playauthenticate.verify.account.first")),format.raw/*8.145*/("""</a>.<br/>
</p>
""")))}),format.raw/*10.2*/("""
"""))
      }
    }
  }

  def render(userProvider:UserProvider): play.twirl.api.HtmlFormat.Appendable = apply(userProvider)

  def f:((UserProvider) => play.twirl.api.HtmlFormat.Appendable) = (userProvider) => apply(userProvider)

  def ref: this.type = this

}


}
}

/**/
object unverified extends unverified_Scope0.unverified_Scope1.unverified
              /*
                  -- GENERATED --
                  DATE: Wed Jun 19 09:50:52 UTC 2019
                  SOURCE: /app/app/views/account/unverified.scala.html
                  HASH: 238aa04388ddf69b5143b188dc8113eb207cd08f
                  MATRIX: 830->30|953->58|981->61|1021->93|1059->94|1087->96|1117->101|1186->150|1226->165|1296->215|1324->216|1360->226|1374->232|1414->252|1443->255|1513->304|1560->321
                  LINES: 30->2|35->2|37->4|37->4|37->4|39->6|39->6|39->6|41->8|41->8|41->8|41->8|41->8|41->8|41->8|41->8|43->10
                  -- GENERATED --
              */
          