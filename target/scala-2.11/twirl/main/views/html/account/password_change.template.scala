
package views.html.account

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object password_change_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

     object password_change_Scope1 {
import service.UserProvider

class password_change extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template2[UserProvider,Form[controllers.Account.PasswordChange],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*2.2*/(userProvider: UserProvider, changeForm: Form[controllers.Account.PasswordChange]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {
import helper._
implicit def /*5.2*/implicitFieldConstructor/*5.26*/ = {{ FieldConstructor(twitterBootstrapBasic.f) }};
Seq[Any](format.raw/*2.84*/("""

"""),format.raw/*5.75*/("""

"""),_display_(/*7.2*/main(userProvider, "UniCarb-DB")/*7.34*/{_display_(Seq[Any](format.raw/*7.35*/("""

"""),format.raw/*9.1*/("""<h1>"""),_display_(/*9.6*/Messages("playauthenticate.change.password.title")),format.raw/*9.56*/("""</h1>
<p>
    """),_display_(/*11.6*/form(routes.Account.doChangePassword, 'class -> "form-inline", 'role -> "form")/*11.85*/ {_display_(Seq[Any](format.raw/*11.87*/("""

    """),_display_(/*13.6*/if(changeForm.hasGlobalErrors)/*13.36*/ {_display_(Seq[Any](format.raw/*13.38*/("""
"""),format.raw/*14.1*/("""<p class="alert alert-danger">
    """),_display_(/*15.6*/changeForm/*15.16*/.globalError.message),format.raw/*15.36*/("""
"""),format.raw/*16.1*/("""</p>
""")))}),format.raw/*17.2*/("""

"""),_display_(/*19.2*/auth/*19.6*/._passwordPartial(changeForm)),format.raw/*19.35*/("""

"""),format.raw/*21.1*/("""<button type="submit" class="btn btn-primary u-form-group">"""),_display_(/*21.61*/Messages("playauthenticate.change.password.cta")),format.raw/*21.109*/("""</button>
""")))}),format.raw/*22.2*/("""
"""),format.raw/*23.1*/("""</p>
""")))}),format.raw/*24.2*/("""
"""))
      }
    }
  }

  def render(userProvider:UserProvider,changeForm:Form[controllers.Account.PasswordChange]): play.twirl.api.HtmlFormat.Appendable = apply(userProvider,changeForm)

  def f:((UserProvider,Form[controllers.Account.PasswordChange]) => play.twirl.api.HtmlFormat.Appendable) = (userProvider,changeForm) => apply(userProvider,changeForm)

  def ref: this.type = this

}


}
}

/**/
object password_change extends password_change_Scope0.password_change_Scope1.password_change
              /*
                  -- GENERATED --
                  DATE: Wed Jun 19 09:50:52 UTC 2019
                  SOURCE: /app/app/views/account/password_change.scala.html
                  HASH: c54de0c0a17f564ddddfad6c7699843f453b674a
                  MATRIX: 886->30|1070->132|1102->156|1181->112|1210->205|1238->208|1278->240|1316->241|1344->243|1374->248|1444->298|1485->313|1573->392|1613->394|1646->401|1685->431|1725->433|1753->434|1815->470|1834->480|1875->500|1903->501|1939->507|1968->510|1980->514|2030->543|2059->545|2146->605|2216->653|2257->664|2285->665|2321->671
                  LINES: 30->2|34->5|34->5|35->2|37->5|39->7|39->7|39->7|41->9|41->9|41->9|43->11|43->11|43->11|45->13|45->13|45->13|46->14|47->15|47->15|47->15|48->16|49->17|51->19|51->19|51->19|53->21|53->21|53->21|54->22|55->23|56->24
                  -- GENERATED --
              */
          