
package views.html.account

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object ask_link_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

     object ask_link_Scope1 {
import service.UserProvider

class ask_link extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template3[UserProvider,Form[Account.Accept],com.feth.play.module.pa.user.AuthUser,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*2.2*/(userProvider: UserProvider, acceptForm: Form[Account.Accept], newAccount: com.feth.play.module.pa.user.AuthUser):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {
import helper._
def /*7.2*/label/*7.7*/:play.twirl.api.HtmlFormat.Appendable = {_display_(

Seq[Any](format.raw/*7.11*/("""
"""),_display_(/*8.2*/auth/*8.6*/._providerIcon(newAccount.getProvider())),format.raw/*8.46*/(""" """),_display_(/*8.48*/Messages("playauthenticate.link.account.question",newAccount)),format.raw/*8.109*/("""
""")))};implicit def /*5.2*/implicitFieldConstructor/*5.26*/ = {{ FieldConstructor(twitterBootstrapBasic.f) }};
Seq[Any](format.raw/*2.115*/("""

"""),format.raw/*5.75*/("""

"""),format.raw/*9.2*/("""

"""),_display_(/*11.2*/main(userProvider, "UniCarb-DB")/*11.34*/{_display_(Seq[Any](format.raw/*11.35*/("""

"""),format.raw/*13.1*/("""<div class="container-fluid content">
    <div class="row-fluid info">
        <div class="row-fluid">
            <div class="page-header row-fluid">
                <h1 class="subheader span8">Link account</h1>
            </div>
            <p>
                """),_display_(/*20.18*/form(routes.Account.doLink, 'class -> "form-horizontal", 'role -> "form")/*20.91*/ {_display_(Seq[Any](format.raw/*20.93*/("""

                    """),_display_(/*22.22*/if(acceptForm.hasGlobalErrors)/*22.52*/ {_display_(Seq[Any](format.raw/*22.54*/("""
                        """),format.raw/*23.25*/("""<p class="alert alert-danger">"""),_display_(/*23.56*/acceptForm/*23.66*/.globalError.message),format.raw/*23.86*/("""</p>
                    """)))}),format.raw/*24.22*/("""

                    """),_display_(/*26.22*/inputRadioGroup(
                        acceptForm("accept"),
                        options = Seq(
                        "true"-> "Yes, link this account",
                        "false"-> "No, log out and create a new user with this account"
                        ),
                        '_label -> label,
                        '_showConstraints -> false
                    )),format.raw/*34.22*/("""

                    """),format.raw/*36.21*/("""<button type="submit" class="btn btn-default">Ok</button>
                """)))}),format.raw/*37.18*/("""
            """),format.raw/*38.13*/("""</p>

        </div>
    </div>
</div>

""")))}),format.raw/*44.2*/("""
"""))
      }
    }
  }

  def render(userProvider:UserProvider,acceptForm:Form[Account.Accept],newAccount:com.feth.play.module.pa.user.AuthUser): play.twirl.api.HtmlFormat.Appendable = apply(userProvider,acceptForm,newAccount)

  def f:((UserProvider,Form[Account.Accept],com.feth.play.module.pa.user.AuthUser) => play.twirl.api.HtmlFormat.Appendable) = (userProvider,acceptForm,newAccount) => apply(userProvider,acceptForm,newAccount)

  def ref: this.type = this

}


}
}

/**/
object ask_link extends ask_link_Scope0.ask_link_Scope1.ask_link
              /*
                  -- GENERATED --
                  DATE: Wed Jun 19 09:50:52 UTC 2019
                  SOURCE: /app/app/views/account/ask_link.scala.html
                  HASH: 0cb5228cd889289b07f784c19f17a36cc8e27f83
                  MATRIX: 883->30|1089->239|1101->244|1181->248|1208->250|1219->254|1279->294|1307->296|1389->357|1422->163|1454->187|1534->143|1563->236|1591->359|1620->362|1661->394|1700->395|1729->397|2021->662|2103->735|2143->737|2193->760|2232->790|2272->792|2325->817|2383->848|2402->858|2443->878|2500->904|2550->927|2961->1317|3011->1339|3117->1414|3158->1427|3229->1468
                  LINES: 30->2|34->7|34->7|36->7|37->8|37->8|37->8|37->8|37->8|38->5|38->5|39->2|41->5|43->9|45->11|45->11|45->11|47->13|54->20|54->20|54->20|56->22|56->22|56->22|57->23|57->23|57->23|57->23|58->24|60->26|68->34|70->36|71->37|72->38|78->44
                  -- GENERATED --
              */
          