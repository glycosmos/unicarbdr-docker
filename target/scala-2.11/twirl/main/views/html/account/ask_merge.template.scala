
package views.html.account

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object ask_merge_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

     object ask_merge_Scope1 {
import service.UserProvider

class ask_merge extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template4[UserProvider,Form[Account.Accept],com.feth.play.module.pa.user.AuthUser,com.feth.play.module.pa.user.AuthUser,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*2.2*/(userProvider: UserProvider, acceptForm: Form[Account.Accept], aUser: com.feth.play.module.pa.user.AuthUser, bUser: com.feth.play.module.pa.user.AuthUser):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {
import helper._
def /*7.2*/label/*7.7*/:play.twirl.api.HtmlFormat.Appendable = {_display_(

Seq[Any](format.raw/*7.11*/("""
"""),_display_(/*8.2*/Messages("playauthenticate.merge.accounts.question",aUser,bUser)),format.raw/*8.66*/("""
""")))};implicit def /*5.2*/implicitFieldConstructor/*5.26*/ = {{ FieldConstructor(twitterBootstrapBasic.f) }};
Seq[Any](format.raw/*2.156*/("""

"""),format.raw/*5.75*/("""

"""),format.raw/*9.2*/("""

"""),_display_(/*11.2*/main(userProvider, "UniCarb-DB")/*11.34*/{_display_(Seq[Any](format.raw/*11.35*/("""

"""),format.raw/*13.1*/("""<h1>"""),_display_(/*13.6*/Messages("playauthenticate.merge.accounts.title")),format.raw/*13.55*/("""</h1>
<p>
    """),_display_(/*15.6*/form(routes.Account.doMerge, 'class -> "form-horizontal", 'role -> "form")/*15.80*/ {_display_(Seq[Any](format.raw/*15.82*/("""

    """),_display_(/*17.6*/if(acceptForm.hasGlobalErrors)/*17.36*/ {_display_(Seq[Any](format.raw/*17.38*/("""
"""),format.raw/*18.1*/("""<p class="alert alert-danger">
    """),_display_(/*19.6*/acceptForm/*19.16*/.globalError.message),format.raw/*19.36*/("""
"""),format.raw/*20.1*/("""</p>
""")))}),format.raw/*21.2*/("""

"""),_display_(/*23.2*/inputRadioGroup(
acceptForm("accept"),
options = Seq(
"true"-> Messages("playauthenticate.merge.accounts.true"),
"false"->Messages("playauthenticate.merge.accounts.false")
),
'_label -> label,
'_showConstraints -> false
)),format.raw/*31.2*/("""

"""),format.raw/*33.1*/("""<button type="submit" class="btn btn-default">"""),_display_(/*33.48*/Messages("playauthenticate.merge.accounts.ok")),format.raw/*33.94*/("""</button>
""")))}),format.raw/*34.2*/("""
"""),format.raw/*35.1*/("""</p>
""")))}),format.raw/*36.2*/("""
"""))
      }
    }
  }

  def render(userProvider:UserProvider,acceptForm:Form[Account.Accept],aUser:com.feth.play.module.pa.user.AuthUser,bUser:com.feth.play.module.pa.user.AuthUser): play.twirl.api.HtmlFormat.Appendable = apply(userProvider,acceptForm,aUser,bUser)

  def f:((UserProvider,Form[Account.Accept],com.feth.play.module.pa.user.AuthUser,com.feth.play.module.pa.user.AuthUser) => play.twirl.api.HtmlFormat.Appendable) = (userProvider,acceptForm,aUser,bUser) => apply(userProvider,acceptForm,aUser,bUser)

  def ref: this.type = this

}


}
}

/**/
object ask_merge extends ask_merge_Scope0.ask_merge_Scope1.ask_merge
              /*
                  -- GENERATED --
                  DATE: Wed Jun 19 09:50:52 UTC 2019
                  SOURCE: /app/app/views/account/ask_merge.scala.html
                  HASH: 6bb0aa5b71fca3327d2762f26fc5fa1aba899bfb
                  MATRIX: 924->30|1171->280|1183->285|1263->289|1290->291|1374->355|1407->204|1439->228|1519->184|1548->277|1576->357|1605->360|1646->392|1685->393|1714->395|1745->400|1815->449|1856->464|1939->538|1979->540|2012->547|2051->577|2091->579|2119->580|2181->616|2200->626|2241->646|2269->647|2305->653|2334->656|2575->877|2604->879|2678->926|2745->972|2786->983|2814->984|2850->990
                  LINES: 30->2|34->7|34->7|36->7|37->8|37->8|38->5|38->5|39->2|41->5|43->9|45->11|45->11|45->11|47->13|47->13|47->13|49->15|49->15|49->15|51->17|51->17|51->17|52->18|53->19|53->19|53->19|54->20|55->21|57->23|65->31|67->33|67->33|67->33|68->34|69->35|70->36
                  -- GENERATED --
              */
          