
package views.html.dataUpload

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object uploaded_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

     object uploaded_Scope1 {
import service.UserProvider

class uploaded extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[UserProvider,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*2.2*/(userProvider: UserProvider):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.30*/("""
"""),_display_(/*3.2*/main(userProvider, "About UniCarb-DB")/*3.40*/{_display_(Seq[Any](format.raw/*3.41*/("""
    """),format.raw/*4.5*/("""<br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <div class="row-fluid">
        <div class="span12 centered">
            <h2 class="logo-text">
                You have successfully uploaded your data to UniCarb-DB!
            </h2>
        </div>
    </div>
    <br/>
    <br/>
    <div class="row-fluid">
        <div class="span12 centered">
            <a href="">View your data</a>
        </div>
    </div>

    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
""")))}))
      }
    }
  }

  def render(userProvider:UserProvider): play.twirl.api.HtmlFormat.Appendable = apply(userProvider)

  def f:((UserProvider) => play.twirl.api.HtmlFormat.Appendable) = (userProvider) => apply(userProvider)

  def ref: this.type = this

}


}
}

/**/
object uploaded extends uploaded_Scope0.uploaded_Scope1.uploaded
              /*
                  -- GENERATED --
                  DATE: Wed Jun 19 09:50:51 UTC 2019
                  SOURCE: /app/app/views/dataUpload/uploaded.scala.html
                  HASH: 6e8ef510cf6e9049c395bda6f67173856bee57dc
                  MATRIX: 827->30|950->58|977->60|1023->98|1061->99|1092->104
                  LINES: 30->2|35->2|36->3|36->3|36->3|37->4
                  -- GENERATED --
              */
          