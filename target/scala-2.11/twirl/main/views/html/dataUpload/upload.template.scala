
package views.html.dataUpload

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object upload_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class upload extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template0[play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply():play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*3.5*/("""<div class="searchdiv">
        <h4>Mirage File (.xlsx)</h4>
        <div class="search-field">
            <input id="mirage-input" type="file" name="file" accept=".xlsx">
        </div>
        <div id="mirage-output">

        </div>
    </div>
    <div class="searchdiv">
        <h4>GlycoWorkbench Files (.gwp)</h4>

        <div id="error-gwp" style="color: red; display: none">No file selected!</div>
        <div class="search-field">
            <input id="gwp-input" type="file" name="file-two" accept=".gwp" multiple="multiple">
        </div>
        <div id="gwp-mirage-results">

        </div>
    </div>

    <p>
        <input type="submit" value="Next" class="btn btn-primary">
    </p>

"""))
      }
    }
  }

  def render(): play.twirl.api.HtmlFormat.Appendable = apply()

  def f:(() => play.twirl.api.HtmlFormat.Appendable) = () => apply()

  def ref: this.type = this

}


}

/**/
object upload extends upload_Scope0.upload
              /*
                  -- GENERATED --
                  DATE: Wed Jun 19 09:50:51 UTC 2019
                  SOURCE: /app/app/views/dataUpload/upload.scala.html
                  HASH: f0f2f679d1524d1ba91fee5e0245f0913a272227
                  MATRIX: 840->6
                  LINES: 32->3
                  -- GENERATED --
              */
          