
package views.html.dataUpload

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object uploadresult_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

     object uploadresult_Scope1 {
import service.UserProvider
import service.{FormSample, StructureForm, StructureForms, FormLC, FormMS, FormStructure}
import src.TableFragment
import models.lists.Treatment
import service.FormTables

class uploadresult extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template10[Form[StructureForms],Form[FormMS],Form[FormSample],Form[FormLC],Form[FormStructure],List[StructureForm],Map[String, Map[String, String]],List[TableFragment],Form[FormTables],UserProvider,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*6.2*/(listForm: Form[StructureForms],
        formMS: Form[FormMS],
        formSample: Form[FormSample],
        formLC: Form[FormLC],
        formStructure: Form[FormStructure],
        list: List[StructureForm],
        sampleOptions: Map[String, Map[String, String]],
        tables: List[TableFragment],
        formTables: Form[FormTables],
        userProvider: UserProvider):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*15.36*/("""

"""),_display_(/*17.2*/main(userProvider, "MIRAGE UniCarb-DB")/*17.41*/{_display_(Seq[Any](format.raw/*17.42*/("""

    """),format.raw/*19.5*/("""<link rel="stylesheet" type="text/css" media="screen" href='"""),_display_(/*19.66*/routes/*19.72*/.Assets.versioned("stylesheets/mirageForms.css")),format.raw/*19.120*/("""'>

    <div id="loading-overlay" style="
        position: fixed;
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: rgba(250,250,250, 1);
        z-index: 12;">
        <div id="loading-info" style="position: absolute;
            top: 50%;
            left: 50%;
            font-size: 12px;
            text-align: center;
            width: 680px;
            height: 200px;
            border-radius: 4px;
            transform: translate(-50%,-50%);
            -ms-transform: translate(-50%,-50%);">
            <img src="../assets/images/wait.gif" style="width: 400px; margin: 20px"> <span style="font-size: 80px; vertical-align: middle"></span>
            <br><div><h5 style="font-size: 18px">Loading... Please Wait</h5></div>
        </div>
    </div>
    <br>

    <div class="container-fluid content">
        <ul class="breadcrumb">
            <li><i class="icon-home" ></i><a href="/"> UniCarb-DB</a> <span class="divider">></span></li>
            <li class="active"><i class="icon-upload" ></i>Upload<span class="divider"></span></li>
        </ul>
        <div class="page-header row-fluid">
            <h1 class="subheader span8">Collecting Data</h1>
        </div>
        <p>Add data...</p>
        <br />
        <br />
        <br />
        """),_display_(/*59.10*/helper/*59.16*/.form(action = routes.UploadDataController.uploadData(), 'onsubmit -> "return validation();")/*59.109*/ {_display_(Seq[Any](format.raw/*59.111*/("""
        """),format.raw/*60.9*/("""<button type="submit" class="btn btn-primary">Upload</button>
        <div class="validation-error" style="display: none; color: red;">
            <br/>
            <span style=" border: solid 1px #ff0000; border-radius: 4px; padding: 6px">Missing data in one or more fields!</span>
        </div>
        <div id="sheet-container" >


            <div id="sheet-btns">
                <div id="sheet-btns-container">
                    <div id="sample-btn" class="sheet-btn">Sample Preparation</div><!--
                --><div id="lc-btn" class="sheet-btn">LC settings</div><!--
                --><div id="ms-one-btn" class="sheet-btn">MS part 1</div><!--
                --><div id="ms-two-btn" class="sheet-btn">MS part 2</div><!--
                --><div id="structure-btn" class="sheet-btn">MS Structures</div>
                </div><!--
            --><div id="folder-end"></div>
            </div>
            <div class="sheet">

                    <div id="sample-sheet" style="display: none">
                        """),_display_(/*81.26*/views/*81.31*/.html.dataUpload.sheets.samplesheet(formSample,formTables, sampleOptions, tables)),format.raw/*81.112*/("""
                    """),format.raw/*82.21*/("""</div>
                    <div id="lc-sheet">
                        """),_display_(/*84.26*/views/*84.31*/.html.dataUpload.sheets.lcsheet(formLC, sampleOptions)),format.raw/*84.85*/("""
                    """),format.raw/*85.21*/("""</div>
                    <div id="ms-one-sheet">
                        """),_display_(/*87.26*/views/*87.31*/.html.dataUpload.sheets.mssheet(formMS,formTables, tables, sampleOptions)),format.raw/*87.104*/("""
                    """),format.raw/*88.21*/("""</div>
                    <div id="ms-two-sheet">
                        """),_display_(/*90.26*/views/*90.31*/.html.dataUpload.sheets.mssheet2(formMS, formTables, tables, sampleOptions)),format.raw/*90.106*/("""
                    """),format.raw/*91.21*/("""</div>
                    <div id="structure-sheet" style="display: none">
                        """),_display_(/*93.26*/views/*93.31*/.html.dataUpload.sheets.uploadstructures(listForm, list, formStructure)),format.raw/*93.102*/("""
                    """),format.raw/*94.21*/("""</div>

                </div>
        </div>
        """)))}),format.raw/*98.10*/("""
    """),format.raw/*99.5*/("""</div>

    <div id="loading-overlay" style="
        position: fixed;
        display: none;
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: rgba(200,200,200, 1);
        z-index: 12;">
        <div id="loading-info" style="position: absolute;
            top: 50%;
            left: 50%;
            font-size: 12px;
            text-align: center;
            width: 680px;
            height: 200px;
            border-radius: 4px;
            transform: translate(-50%,-50%);
            -ms-transform: translate(-50%,-50%);">
            <img src="../assets/images/wait.gif" style="width: 160px; margin: 20px"> <span style="font-size: 80px; vertical-align: middle">UniCarb-DB</span>
        </div>


    <style>
            #loading-overlay """),format.raw/*127.30*/("""{"""),format.raw/*127.31*/("""
                """),format.raw/*128.17*/("""position: fixed;
                width: 100%;
                height: 100%;
                top: 0;
                left: 0;
                right: 0;
                bottom: 0;
                background-color: rgba(250,250,250, 1);
                z-index: 12;
            """),format.raw/*137.13*/("""}"""),format.raw/*137.14*/("""
            """),format.raw/*138.13*/("""#loading-info"""),format.raw/*138.26*/("""{"""),format.raw/*138.27*/("""
                """),format.raw/*139.17*/("""position: absolute;
                top: 50%;
                left: 50%;
                font-size: 12px;
                text-align: center;
                width: 680px;
                height: 200px;
                border-radius: 4px;
                transform: translate(-50%,-50%);
                -ms-transform: translate(-50%,-50%);
            """),format.raw/*149.13*/("""}"""),format.raw/*149.14*/("""

            """),format.raw/*151.13*/(""".sheet"""),format.raw/*151.19*/("""{"""),format.raw/*151.20*/("""
                """),format.raw/*152.17*/("""box-sizing: border-box;
                padding: 40px;
                border-bottom: solid 1px #DDDDDD;
                border-left: solid 1px #DDDDDD;
                border-right: solid 1px #DDDDDD;
                width: 100%;
                margin-top: -0px;
            """),format.raw/*159.13*/("""}"""),format.raw/*159.14*/("""
            """),format.raw/*160.13*/(""".sheet-btn"""),format.raw/*160.23*/("""{"""),format.raw/*160.24*/("""
                """),format.raw/*161.17*/("""box-sizing: border-box;
                display: inline-block;
                margin: 0px;
                text-align: center;
                padding: 10px;
                width: 20%;
                border: solid 1px #DDDDDD;
                cursor: pointer;
                border-radius: 30px 30px 0px 0px;
                -webkit-margin-start: 0px;
                -webkit-margin-after: 0px;
                -moz-margin-start: 0px;
                overflow: hidden;
            """),format.raw/*174.13*/("""}"""),format.raw/*174.14*/("""
            """),format.raw/*175.13*/("""#sheet-btns"""),format.raw/*175.24*/("""{"""),format.raw/*175.25*/("""
                """),format.raw/*176.17*/("""width: 100%;
                overflow: hidden;
                -webkit-touch-callout: none; /* iOS Safari */
                -webkit-user-select: none; /* Safari */
                -khtml-user-select: none; /* Konqueror HTML */
                -moz-user-select: none; /* Firefox */
                -ms-user-select: none; /* Internet Explorer/Edge */
                user-select: none; /* Non-prefixed version, currently
                                  supported by Chrome and Opera */
            """),format.raw/*185.13*/("""}"""),format.raw/*185.14*/("""
            """),format.raw/*186.13*/("""#sheet-btns-container"""),format.raw/*186.34*/("""{"""),format.raw/*186.35*/("""
                """),format.raw/*187.17*/("""width: 90%;
                min-width: 800px;
                display: inline-block;
            """),format.raw/*190.13*/("""}"""),format.raw/*190.14*/("""
            """),format.raw/*191.13*/("""#sheet-container"""),format.raw/*191.29*/("""{"""),format.raw/*191.30*/("""
                """),format.raw/*192.17*/("""display: inline-block;
                margin-top: 60px;
                width: 100%;
            """),format.raw/*195.13*/("""}"""),format.raw/*195.14*/("""
            """),format.raw/*196.13*/(""".footer"""),format.raw/*196.20*/("""{"""),format.raw/*196.21*/("""
                """),format.raw/*197.17*/("""visibility: hidden;
            """),format.raw/*198.13*/("""}"""),format.raw/*198.14*/("""
            """),format.raw/*199.13*/("""#folder-end"""),format.raw/*199.24*/("""{"""),format.raw/*199.25*/("""
                """),format.raw/*200.17*/("""width: 10%;
                box-sizing: border-box;
                display: inline-block;
                margin: 0px;
                padding: 17px;
                border: none;
                border-bottom: solid 1px #DDDDDD;
            """),format.raw/*207.13*/("""}"""),format.raw/*207.14*/("""
    """),format.raw/*208.5*/("""</style>
    <style>
            .red-star"""),format.raw/*210.22*/("""{"""),format.raw/*210.23*/("""
                """),format.raw/*211.17*/("""color: #ff0000;
                font-size: 16px;
                padding-left: 4px;
            """),format.raw/*214.13*/("""}"""),format.raw/*214.14*/("""
    """),format.raw/*215.5*/("""</style>

        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script>

        var validationError = false;

            function validation() """),format.raw/*224.35*/("""{"""),format.raw/*224.36*/("""
                """),format.raw/*225.17*/("""var isValid = true;
                $(".val input, .val textarea").each(function () """),format.raw/*226.65*/("""{"""),format.raw/*226.66*/("""
                   """),format.raw/*227.20*/("""if ($(this).val() == "")"""),format.raw/*227.44*/("""{"""),format.raw/*227.45*/("""
                       """),format.raw/*228.24*/("""$(".validation-error").show();
                       validationError = true;
                       isValid = false;
                       validate($(this));
                       if(($(this).parent().parent().hasClass("v-sample")))"""),format.raw/*232.76*/("""{"""),format.raw/*232.77*/("""
                           """),format.raw/*233.28*/("""$("#sample-btn").css("color", "red");
                       """),format.raw/*234.24*/("""}"""),format.raw/*234.25*/("""
                       """),format.raw/*235.24*/("""if(($(this).parent().parent().hasClass("v-lc")))"""),format.raw/*235.72*/("""{"""),format.raw/*235.73*/("""
                           """),format.raw/*236.28*/("""$("#lc-btn").css("color", "red");
                        """),format.raw/*237.25*/("""}"""),format.raw/*237.26*/("""
                       """),format.raw/*238.24*/("""if(($(this).parent().parent().hasClass("v-ms1")))"""),format.raw/*238.73*/("""{"""),format.raw/*238.74*/("""
                           """),format.raw/*239.28*/("""$("#ms-one-btn").css("color", "red");
                        """),format.raw/*240.25*/("""}"""),format.raw/*240.26*/("""
                   """),format.raw/*241.20*/("""}"""),format.raw/*241.21*/("""
                """),format.raw/*242.17*/("""}"""),format.raw/*242.18*/(""");
                if(isValid)"""),format.raw/*243.28*/("""{"""),format.raw/*243.29*/("""
                    """),format.raw/*244.21*/("""$('#loading-overlay').show();
                """),format.raw/*245.17*/("""}"""),format.raw/*245.18*/("""
                """),format.raw/*246.17*/("""return isValid;
            """),format.raw/*247.13*/("""}"""),format.raw/*247.14*/("""

            """),format.raw/*249.13*/("""function validate(element) """),format.raw/*249.40*/("""{"""),format.raw/*249.41*/("""
                """),format.raw/*250.17*/("""if($(element).val() == "")"""),format.raw/*250.43*/("""{"""),format.raw/*250.44*/("""
                    """),format.raw/*251.21*/("""$(element).css("""),format.raw/*251.36*/("""{"""),format.raw/*251.37*/(""" """),format.raw/*251.38*/("""'border': 'solid 1px #ff0000'"""),format.raw/*251.67*/("""}"""),format.raw/*251.68*/(""");
                    el = $(element).parent().parent().parent();
                    $(el).siblings(".red-star").css('color', '#ff0000');
                """),format.raw/*254.17*/("""}"""),format.raw/*254.18*/("""else """),format.raw/*254.23*/("""{"""),format.raw/*254.24*/("""
                    """),format.raw/*255.21*/("""$(element).css("""),format.raw/*255.36*/("""{"""),format.raw/*255.37*/(""" """),format.raw/*255.38*/("""'border': ''"""),format.raw/*255.50*/("""}"""),format.raw/*255.51*/(""");
                    el = $(element).parent().parent().parent();
                    $(el).siblings(".red-star").css('color', '#aaaaaa');
                """),format.raw/*258.17*/("""}"""),format.raw/*258.18*/("""
            """),format.raw/*259.13*/("""}"""),format.raw/*259.14*/("""

            """),format.raw/*261.13*/("""function validatePage(elements, page) """),format.raw/*261.51*/("""{"""),format.raw/*261.52*/("""
                """),format.raw/*262.17*/("""var isValid = true;
                $(elements).each(function () """),format.raw/*263.46*/("""{"""),format.raw/*263.47*/("""
                    """),format.raw/*264.21*/("""if($(this).val() == "")"""),format.raw/*264.44*/("""{"""),format.raw/*264.45*/("""
                        """),format.raw/*265.25*/("""isValid = false;
                    """),format.raw/*266.21*/("""}"""),format.raw/*266.22*/("""
                    """),format.raw/*267.21*/("""validate($(this));
                """),format.raw/*268.17*/("""}"""),format.raw/*268.18*/(""");
                if(isValid)"""),format.raw/*269.28*/("""{"""),format.raw/*269.29*/("""
                    """),format.raw/*270.21*/("""$(page).css("color", "black");
                """),format.raw/*271.17*/("""}"""),format.raw/*271.18*/("""
                """),format.raw/*272.17*/("""return isValid;
            """),format.raw/*273.13*/("""}"""),format.raw/*273.14*/("""

            """),format.raw/*275.13*/("""$(document).ready(function()"""),format.raw/*275.41*/("""{"""),format.raw/*275.42*/("""
                """),format.raw/*276.17*/("""$('#loading-overlay').fadeOut(400);

                $(".val input, .val textarea").each(function () """),format.raw/*278.65*/("""{"""),format.raw/*278.66*/("""
                    """),format.raw/*279.21*/("""setStars($(this));
                """),format.raw/*280.17*/("""}"""),format.raw/*280.18*/(""");

                $(".val input, .val textarea").focus(function () """),format.raw/*282.66*/("""{"""),format.raw/*282.67*/("""
                    """),format.raw/*283.21*/("""$(this).css("""),format.raw/*283.33*/("""{"""),format.raw/*283.34*/(""" """),format.raw/*283.35*/("""'border': 'solid 1px #018FD5'"""),format.raw/*283.64*/("""}"""),format.raw/*283.65*/(""");
                """),format.raw/*284.17*/("""}"""),format.raw/*284.18*/(""");
                $(".val input, .val textarea").focusout(function () """),format.raw/*285.69*/("""{"""),format.raw/*285.70*/("""
                    """),format.raw/*286.21*/("""if(validationError)"""),format.raw/*286.40*/("""{"""),format.raw/*286.41*/("""
                        """),format.raw/*287.25*/("""if(validatePage($(".v-sample input, .v-sample textarea,"), $("#sample-btn")) & validatePage($(".v-lc input, .v-lc textarea"), $("#lc-btn")) & validatePage($(".v-ms1 input, .v-ms1 textarea"), $("#ms-one-btn")))"""),format.raw/*287.234*/("""{"""),format.raw/*287.235*/("""
                            """),format.raw/*288.29*/("""$(".validation-error").hide();
                        """),format.raw/*289.25*/("""}"""),format.raw/*289.26*/("""
                    """),format.raw/*290.21*/("""}"""),format.raw/*290.22*/(""" """),format.raw/*290.23*/("""else """),format.raw/*290.28*/("""{"""),format.raw/*290.29*/("""
                        """),format.raw/*291.25*/("""validate($(this));
                    """),format.raw/*292.21*/("""}"""),format.raw/*292.22*/("""
                """),format.raw/*293.17*/("""}"""),format.raw/*293.18*/(""");


                function setStars(element) """),format.raw/*296.44*/("""{"""),format.raw/*296.45*/("""
                    """),format.raw/*297.21*/("""if($(element).val() == "")"""),format.raw/*297.47*/("""{"""),format.raw/*297.48*/("""
                        """),format.raw/*298.25*/("""el = $(element).parent().parent().parent();
                        $(el).siblings(".red-star").css('color', '#ff0000');
                    """),format.raw/*300.21*/("""}"""),format.raw/*300.22*/("""else """),format.raw/*300.27*/("""{"""),format.raw/*300.28*/("""
                        """),format.raw/*301.25*/("""el = $(element).parent().parent().parent();
                        $(el).siblings(".red-star").css('color', '#aaaaaa');
                    """),format.raw/*303.21*/("""}"""),format.raw/*303.22*/("""
                """),format.raw/*304.17*/("""}"""),format.raw/*304.18*/("""
         

                """),format.raw/*307.17*/("""var sheets = [
                    """),format.raw/*308.21*/("""{"""),format.raw/*308.22*/("""element: $("#sample-sheet"), btn: $("#sample-btn")"""),format.raw/*308.72*/("""}"""),format.raw/*308.73*/(""",
                    """),format.raw/*309.21*/("""{"""),format.raw/*309.22*/("""element: $("#lc-sheet"),btn: $("#lc-btn")"""),format.raw/*309.63*/("""}"""),format.raw/*309.64*/(""",
                    """),format.raw/*310.21*/("""{"""),format.raw/*310.22*/("""element: $("#ms-one-sheet"),btn: $("#ms-one-btn")"""),format.raw/*310.71*/("""}"""),format.raw/*310.72*/(""",
                    """),format.raw/*311.21*/("""{"""),format.raw/*311.22*/("""element: $("#ms-two-sheet"),btn: $("#ms-two-btn")"""),format.raw/*311.71*/("""}"""),format.raw/*311.72*/(""",
                    """),format.raw/*312.21*/("""{"""),format.raw/*312.22*/("""element: $("#structure-sheet"), btn: $("#structure-btn")"""),format.raw/*312.78*/("""}"""),format.raw/*312.79*/("""
                """),format.raw/*313.17*/("""];

                var sheetClick = function(element)"""),format.raw/*315.51*/("""{"""),format.raw/*315.52*/("""
                    """),format.raw/*316.21*/("""for(var e in sheets)"""),format.raw/*316.41*/("""{"""),format.raw/*316.42*/("""
                        """),format.raw/*317.25*/("""if(sheets[e].btn.text() == element.text())"""),format.raw/*317.67*/("""{"""),format.raw/*317.68*/("""
                            """),format.raw/*318.29*/("""sheets[e].element.css("display","block");
                            sheets[e].btn.css("""),format.raw/*319.47*/("""{"""),format.raw/*319.48*/(""""border-bottom": "hidden", "background-color": "inherit""""),format.raw/*319.104*/("""}"""),format.raw/*319.105*/(""");
                        """),format.raw/*320.25*/("""}"""),format.raw/*320.26*/(""" """),format.raw/*320.27*/("""else """),format.raw/*320.32*/("""{"""),format.raw/*320.33*/("""
                            """),format.raw/*321.29*/("""sheets[e].element.css("display", "none");
                            sheets[e].btn.css("""),format.raw/*322.47*/("""{"""),format.raw/*322.48*/(""""border-bottom": "solid 1px #dddddd", "background-color": "#f0f0f0""""),format.raw/*322.115*/("""}"""),format.raw/*322.116*/(""");
                        """),format.raw/*323.25*/("""}"""),format.raw/*323.26*/("""
                    """),format.raw/*324.21*/("""}"""),format.raw/*324.22*/("""
                """),format.raw/*325.17*/("""}"""),format.raw/*325.18*/("""


                """),format.raw/*328.17*/("""$(".sheet-btn").click(function (e) """),format.raw/*328.52*/("""{"""),format.raw/*328.53*/("""
                    """),format.raw/*329.21*/("""sheetClick($(e.target));
                """),format.raw/*330.17*/("""}"""),format.raw/*330.18*/(""")

                sheetClick($("#sample-btn"));

                $(window).keydown(function(event)"""),format.raw/*334.50*/("""{"""),format.raw/*334.51*/("""
                    """),format.raw/*335.21*/("""if(event.keyCode == 13) """),format.raw/*335.45*/("""{"""),format.raw/*335.46*/("""
                        """),format.raw/*336.25*/("""event.preventDefault();
                        return false;
                    """),format.raw/*338.21*/("""}"""),format.raw/*338.22*/("""
                """),format.raw/*339.17*/("""}"""),format.raw/*339.18*/(""");

                // Check if browser is Firefox, if so add date picker
                if(typeof InstallTrigger !== 'undefined')"""),format.raw/*342.58*/("""{"""),format.raw/*342.59*/("""
                    """),format.raw/*343.21*/("""$( function() """),format.raw/*343.35*/("""{"""),format.raw/*343.36*/("""
                        """),format.raw/*344.25*/("""var date = $(".date-stamp dd input").val();
                        $(".date-stamp dd input").datepicker();
                        $(".date-stamp dd input").datepicker("option", "dateFormat", "yy-mm-dd");
                        $(".date-stamp dd input").val(date);
                    """),format.raw/*348.21*/("""}"""),format.raw/*348.22*/(""" """),format.raw/*348.23*/(""");
                """),format.raw/*349.17*/("""}"""),format.raw/*349.18*/("""


                """),format.raw/*352.17*/("""$(".date-stamp dd input").change(function (e) """),format.raw/*352.63*/("""{"""),format.raw/*352.64*/("""
                    """),format.raw/*353.21*/("""var text = $(e.target).val();
                    $(".date-stamp dd input").each(function() """),format.raw/*354.63*/("""{"""),format.raw/*354.64*/("""
                        """),format.raw/*355.25*/("""$(this).val(text);
                        setStars($(this));
                    """),format.raw/*357.21*/("""}"""),format.raw/*357.22*/(""");
                """),format.raw/*358.17*/("""}"""),format.raw/*358.18*/(""")
                $(".responsible dd input").change(function (e) """),format.raw/*359.64*/("""{"""),format.raw/*359.65*/("""
                    """),format.raw/*360.21*/("""var text = $(e.target).val();
                    $(".responsible dd input").each(function() """),format.raw/*361.64*/("""{"""),format.raw/*361.65*/("""
                        """),format.raw/*362.25*/("""$(this).val(text);
                        setStars($(this));
                    """),format.raw/*364.21*/("""}"""),format.raw/*364.22*/(""");
                """),format.raw/*365.17*/("""}"""),format.raw/*365.18*/(""")
                $(".affiliation dd input").change(function (e) """),format.raw/*366.64*/("""{"""),format.raw/*366.65*/("""
                    """),format.raw/*367.21*/("""var text = $(e.target).val();
                    $(".affiliation dd input").each(function() """),format.raw/*368.64*/("""{"""),format.raw/*368.65*/("""
                        """),format.raw/*369.25*/("""$(this).val(text);
                        setStars($(this));
                    """),format.raw/*371.21*/("""}"""),format.raw/*371.22*/(""");
                """),format.raw/*372.17*/("""}"""),format.raw/*372.18*/(""")
                $(".contactInformation dd input").change(function (e) """),format.raw/*373.71*/("""{"""),format.raw/*373.72*/("""
                    """),format.raw/*374.21*/("""var text = $(e.target).val();
                    $(".contactInformation dd input").each(function() """),format.raw/*375.71*/("""{"""),format.raw/*375.72*/("""
                        """),format.raw/*376.25*/("""$(this).val(text);
                        setStars($(this));
                    """),format.raw/*378.21*/("""}"""),format.raw/*378.22*/(""");
                """),format.raw/*379.17*/("""}"""),format.raw/*379.18*/(""");

                $(".table-fragment-softwareTwo-input-container").click(function()"""),format.raw/*381.82*/("""{"""),format.raw/*381.83*/("""
                    """),format.raw/*382.21*/("""setUpSoftwareList($(".table-fragment-softwareTwo-column0 select"), $(".table-fragment-softwareThree-column0 select"));
                """),format.raw/*383.17*/("""}"""),format.raw/*383.18*/(""");
                $(".table-fragment-softwareTwo-add").click(function()"""),format.raw/*384.70*/("""{"""),format.raw/*384.71*/("""
                    """),format.raw/*385.21*/("""setUpSoftwareList($(".table-fragment-softwareTwo-column0 select"), $(".table-fragment-softwareThree-column0 select"));
                """),format.raw/*386.17*/("""}"""),format.raw/*386.18*/(""");
                $(".table-fragment-softwareThree-add").click(function()"""),format.raw/*387.72*/("""{"""),format.raw/*387.73*/("""
                    """),format.raw/*388.21*/("""setUpSoftwareList($(".table-fragment-softwareTwo-column0 select"), $(".table-fragment-softwareThree-column0 select"), true);
                """),format.raw/*389.17*/("""}"""),format.raw/*389.18*/(""");
                $(".table-fragment-softwareFour-input-container").click(function()"""),format.raw/*390.83*/("""{"""),format.raw/*390.84*/("""
                    """),format.raw/*391.21*/("""setUpSoftwareList($(".table-fragment-softwareFour-column0 select"), $(".table-fragment-softwareFive-column0 select"));
                """),format.raw/*392.17*/("""}"""),format.raw/*392.18*/(""");
                $(".table-fragment-softwareFour-add").click(function()"""),format.raw/*393.71*/("""{"""),format.raw/*393.72*/("""
                    """),format.raw/*394.21*/("""setUpSoftwareList($(".table-fragment-softwareFour-column0 select"), $(".table-fragment-softwareFive-column0 select"));
                """),format.raw/*395.17*/("""}"""),format.raw/*395.18*/(""");
                $(".table-fragment-softwareFive-add").click(function()"""),format.raw/*396.71*/("""{"""),format.raw/*396.72*/("""
                    """),format.raw/*397.21*/("""setUpSoftwareList($(".table-fragment-softwareFour-column0 select"), $(".table-fragment-softwareFive-column0 select"), true);
                """),format.raw/*398.17*/("""}"""),format.raw/*398.18*/(""");
                $(".table-fragment-softwareSix-input-container").click(function()"""),format.raw/*399.82*/("""{"""),format.raw/*399.83*/("""
                    """),format.raw/*400.21*/("""setUpSoftwareList($(".table-fragment-softwareSix-column0 select"), $(".table-fragment-softwareSeven-column0 select"));
                """),format.raw/*401.17*/("""}"""),format.raw/*401.18*/(""");
                $(".table-fragment-softwareSix-add").click(function()"""),format.raw/*402.70*/("""{"""),format.raw/*402.71*/("""
                    """),format.raw/*403.21*/("""setUpSoftwareList($(".table-fragment-softwareSix-column0 select"), $(".table-fragment-softwareSeven-column0 select"));
                """),format.raw/*404.17*/("""}"""),format.raw/*404.18*/(""");
                $(".table-fragment-softwareSeven-add").click(function()"""),format.raw/*405.72*/("""{"""),format.raw/*405.73*/("""
                    """),format.raw/*406.21*/("""setUpSoftwareList($(".table-fragment-softwareSix-column0 select"), $(".table-fragment-softwareSeven-column0 select"), true);
                """),format.raw/*407.17*/("""}"""),format.raw/*407.18*/(""");

                function setUpSoftwareList(fromElement, toElement, last) """),format.raw/*409.74*/("""{"""),format.raw/*409.75*/("""
                    """),format.raw/*410.21*/("""var opList = [];
                    fromElement.each(function () """),format.raw/*411.50*/("""{"""),format.raw/*411.51*/("""
                        """),format.raw/*412.25*/("""opList.push($(this).val());
                    """),format.raw/*413.21*/("""}"""),format.raw/*413.22*/(""");

                    if(last)"""),format.raw/*415.29*/("""{"""),format.raw/*415.30*/("""
                        """),format.raw/*416.25*/("""toElement.last().find('option').remove();
                        for (var o in opList)"""),format.raw/*417.46*/("""{"""),format.raw/*417.47*/("""
                            """),format.raw/*418.29*/("""toElement.last().append('<option value=' + opList[o] + '>' + opList[o] + '</option>');
                        """),format.raw/*419.25*/("""}"""),format.raw/*419.26*/("""
                    """),format.raw/*420.21*/("""}"""),format.raw/*420.22*/(""" """),format.raw/*420.23*/("""else """),format.raw/*420.28*/("""{"""),format.raw/*420.29*/("""
                        """),format.raw/*421.25*/("""toElement.each(function () """),format.raw/*421.52*/("""{"""),format.raw/*421.53*/("""
                            """),format.raw/*422.29*/("""if ($.inArray($(this).val(), opList) != -1)"""),format.raw/*422.72*/("""{"""),format.raw/*422.73*/("""
                                """),format.raw/*423.33*/("""var selected = $(this).val();
                                $(this).find('option').remove();
                                for (var o in opList)"""),format.raw/*425.54*/("""{"""),format.raw/*425.55*/("""
                                    """),format.raw/*426.37*/("""$(this).append('<option value=' + opList[o] + '>' + opList[o] + '</option>');
                                """),format.raw/*427.33*/("""}"""),format.raw/*427.34*/("""
                                """),format.raw/*428.33*/("""$(this).val(selected);
                            """),format.raw/*429.29*/("""}"""),format.raw/*429.30*/(""" """),format.raw/*429.31*/("""else """),format.raw/*429.36*/("""{"""),format.raw/*429.37*/("""
                                """),format.raw/*430.33*/("""$(this).find('option').remove();
                                for (var o in opList)"""),format.raw/*431.54*/("""{"""),format.raw/*431.55*/("""
                                    """),format.raw/*432.37*/("""$(this).append('<option value=' + opList[o] + '>' + opList[o] + '</option>');
                                """),format.raw/*433.33*/("""}"""),format.raw/*433.34*/("""
                            """),format.raw/*434.29*/("""}"""),format.raw/*434.30*/("""
                        """),format.raw/*435.25*/("""}"""),format.raw/*435.26*/(""");
                    """),format.raw/*436.21*/("""}"""),format.raw/*436.22*/("""

                """),format.raw/*438.17*/("""}"""),format.raw/*438.18*/("""
            """),format.raw/*439.13*/("""}"""),format.raw/*439.14*/(""");
    </script>
""")))}),format.raw/*441.2*/("""

"""))
      }
    }
  }

  def render(listForm:Form[StructureForms],formMS:Form[FormMS],formSample:Form[FormSample],formLC:Form[FormLC],formStructure:Form[FormStructure],list:List[StructureForm],sampleOptions:Map[String, Map[String, String]],tables:List[TableFragment],formTables:Form[FormTables],userProvider:UserProvider): play.twirl.api.HtmlFormat.Appendable = apply(listForm,formMS,formSample,formLC,formStructure,list,sampleOptions,tables,formTables,userProvider)

  def f:((Form[StructureForms],Form[FormMS],Form[FormSample],Form[FormLC],Form[FormStructure],List[StructureForm],Map[String, Map[String, String]],List[TableFragment],Form[FormTables],UserProvider) => play.twirl.api.HtmlFormat.Appendable) = (listForm,formMS,formSample,formLC,formStructure,list,sampleOptions,tables,formTables,userProvider) => apply(listForm,formMS,formSample,formLC,formStructure,list,sampleOptions,tables,formTables,userProvider)

  def ref: this.type = this

}


}
}

/**/
object uploadresult extends uploadresult_Scope0.uploadresult_Scope1.uploadresult
              /*
                  -- GENERATED --
                  DATE: Wed Jun 19 09:50:51 UTC 2019
                  SOURCE: /app/app/views/dataUpload/uploadresult.scala.html
                  HASH: 4e16710089f43fcc5ee5dca95da3022236c4f1b2
                  MATRIX: 1185->205|1658->582|1687->585|1735->624|1774->625|1807->631|1895->692|1910->698|1980->746|3364->2103|3379->2109|3482->2202|3523->2204|3559->2213|4619->3246|4633->3251|4736->3332|4785->3353|4884->3425|4898->3430|4973->3484|5022->3505|5125->3581|5139->3586|5234->3659|5283->3680|5386->3756|5400->3761|5497->3836|5546->3857|5674->3958|5688->3963|5781->4034|5830->4055|5916->4110|5948->4115|6815->4953|6845->4954|6891->4971|7195->5246|7225->5247|7267->5260|7309->5273|7339->5274|7385->5291|7767->5644|7797->5645|7840->5659|7875->5665|7905->5666|7951->5683|8257->5960|8287->5961|8329->5974|8368->5984|8398->5985|8444->6002|8958->6487|8988->6488|9030->6501|9070->6512|9100->6513|9146->6530|9674->7029|9704->7030|9746->7043|9796->7064|9826->7065|9872->7082|9998->7179|10028->7180|10070->7193|10115->7209|10145->7210|10191->7227|10318->7325|10348->7326|10390->7339|10426->7346|10456->7347|10502->7364|10563->7396|10593->7397|10635->7410|10675->7421|10705->7422|10751->7439|11023->7682|11053->7683|11086->7688|11157->7730|11187->7731|11233->7748|11358->7844|11388->7845|11421->7850|11719->8119|11749->8120|11795->8137|11908->8221|11938->8222|11987->8242|12040->8266|12070->8267|12123->8291|12387->8526|12417->8527|12474->8555|12564->8616|12594->8617|12647->8641|12724->8689|12754->8690|12811->8718|12898->8776|12928->8777|12981->8801|13059->8850|13089->8851|13146->8879|13237->8941|13267->8942|13316->8962|13346->8963|13392->8980|13422->8981|13481->9011|13511->9012|13561->9033|13636->9079|13666->9080|13712->9097|13769->9125|13799->9126|13842->9140|13898->9167|13928->9168|13974->9185|14029->9211|14059->9212|14109->9233|14153->9248|14183->9249|14213->9250|14271->9279|14301->9280|14486->9436|14516->9437|14550->9442|14580->9443|14630->9464|14674->9479|14704->9480|14734->9481|14775->9493|14805->9494|14990->9650|15020->9651|15062->9664|15092->9665|15135->9679|15202->9717|15232->9718|15278->9735|15372->9800|15402->9801|15452->9822|15504->9845|15534->9846|15588->9871|15654->9908|15684->9909|15734->9930|15798->9965|15828->9966|15887->9996|15917->9997|15967->10018|16043->10065|16073->10066|16119->10083|16176->10111|16206->10112|16249->10126|16306->10154|16336->10155|16382->10172|16512->10273|16542->10274|16592->10295|16656->10330|16686->10331|16784->10400|16814->10401|16864->10422|16905->10434|16935->10435|16965->10436|17023->10465|17053->10466|17101->10485|17131->10486|17231->10557|17261->10558|17311->10579|17359->10598|17389->10599|17443->10624|17682->10833|17713->10834|17771->10863|17855->10918|17885->10919|17935->10940|17965->10941|17995->10942|18029->10947|18059->10948|18113->10973|18181->11012|18211->11013|18257->11030|18287->11031|18364->11079|18394->11080|18444->11101|18499->11127|18529->11128|18583->11153|18753->11294|18783->11295|18817->11300|18847->11301|18901->11326|19071->11467|19101->11468|19147->11485|19177->11486|19234->11514|19298->11549|19328->11550|19407->11600|19437->11601|19488->11623|19518->11624|19588->11665|19618->11666|19669->11688|19699->11689|19777->11738|19807->11739|19858->11761|19888->11762|19966->11811|19996->11812|20047->11834|20077->11835|20162->11891|20192->11892|20238->11909|20321->11963|20351->11964|20401->11985|20450->12005|20480->12006|20534->12031|20605->12073|20635->12074|20693->12103|20810->12191|20840->12192|20926->12248|20957->12249|21013->12276|21043->12277|21073->12278|21107->12283|21137->12284|21195->12313|21312->12401|21342->12402|21439->12469|21470->12470|21526->12497|21556->12498|21606->12519|21636->12520|21682->12537|21712->12538|21760->12557|21824->12592|21854->12593|21904->12614|21974->12655|22004->12656|22132->12755|22162->12756|22212->12777|22265->12801|22295->12802|22349->12827|22460->12909|22490->12910|22536->12927|22566->12928|22726->13059|22756->13060|22806->13081|22849->13095|22879->13096|22933->13121|23249->13408|23279->13409|23309->13410|23357->13429|23387->13430|23435->13449|23510->13495|23540->13496|23590->13517|23711->13609|23741->13610|23795->13635|23906->13717|23936->13718|23984->13737|24014->13738|24108->13803|24138->13804|24188->13825|24310->13918|24340->13919|24394->13944|24505->14026|24535->14027|24583->14046|24613->14047|24707->14112|24737->14113|24787->14134|24909->14227|24939->14228|24993->14253|25104->14335|25134->14336|25182->14355|25212->14356|25313->14428|25343->14429|25393->14450|25522->14550|25552->14551|25606->14576|25717->14658|25747->14659|25795->14678|25825->14679|25939->14764|25969->14765|26019->14786|26183->14921|26213->14922|26314->14994|26344->14995|26394->15016|26558->15151|26588->15152|26691->15226|26721->15227|26771->15248|26941->15389|26971->15390|27085->15475|27115->15476|27165->15497|27329->15632|27359->15633|27461->15706|27491->15707|27541->15728|27705->15863|27735->15864|27837->15937|27867->15938|27917->15959|28087->16100|28117->16101|28230->16185|28260->16186|28310->16207|28474->16342|28504->16343|28605->16415|28635->16416|28685->16437|28849->16572|28879->16573|28982->16647|29012->16648|29062->16669|29232->16810|29262->16811|29368->16888|29398->16889|29448->16910|29543->16976|29573->16977|29627->17002|29704->17050|29734->17051|29795->17083|29825->17084|29879->17109|29995->17196|30025->17197|30083->17226|30223->17337|30253->17338|30303->17359|30333->17360|30363->17361|30397->17366|30427->17367|30481->17392|30537->17419|30567->17420|30625->17449|30697->17492|30727->17493|30789->17526|30966->17674|30996->17675|31062->17712|31201->17822|31231->17823|31293->17856|31373->17907|31403->17908|31433->17909|31467->17914|31497->17915|31559->17948|31674->18034|31704->18035|31770->18072|31909->18182|31939->18183|31997->18212|32027->18213|32081->18238|32111->18239|32163->18262|32193->18263|32240->18281|32270->18282|32312->18295|32342->18296|32391->18314
                  LINES: 34->6|48->15|50->17|50->17|50->17|52->19|52->19|52->19|52->19|92->59|92->59|92->59|92->59|93->60|114->81|114->81|114->81|115->82|117->84|117->84|117->84|118->85|120->87|120->87|120->87|121->88|123->90|123->90|123->90|124->91|126->93|126->93|126->93|127->94|131->98|132->99|160->127|160->127|161->128|170->137|170->137|171->138|171->138|171->138|172->139|182->149|182->149|184->151|184->151|184->151|185->152|192->159|192->159|193->160|193->160|193->160|194->161|207->174|207->174|208->175|208->175|208->175|209->176|218->185|218->185|219->186|219->186|219->186|220->187|223->190|223->190|224->191|224->191|224->191|225->192|228->195|228->195|229->196|229->196|229->196|230->197|231->198|231->198|232->199|232->199|232->199|233->200|240->207|240->207|241->208|243->210|243->210|244->211|247->214|247->214|248->215|257->224|257->224|258->225|259->226|259->226|260->227|260->227|260->227|261->228|265->232|265->232|266->233|267->234|267->234|268->235|268->235|268->235|269->236|270->237|270->237|271->238|271->238|271->238|272->239|273->240|273->240|274->241|274->241|275->242|275->242|276->243|276->243|277->244|278->245|278->245|279->246|280->247|280->247|282->249|282->249|282->249|283->250|283->250|283->250|284->251|284->251|284->251|284->251|284->251|284->251|287->254|287->254|287->254|287->254|288->255|288->255|288->255|288->255|288->255|288->255|291->258|291->258|292->259|292->259|294->261|294->261|294->261|295->262|296->263|296->263|297->264|297->264|297->264|298->265|299->266|299->266|300->267|301->268|301->268|302->269|302->269|303->270|304->271|304->271|305->272|306->273|306->273|308->275|308->275|308->275|309->276|311->278|311->278|312->279|313->280|313->280|315->282|315->282|316->283|316->283|316->283|316->283|316->283|316->283|317->284|317->284|318->285|318->285|319->286|319->286|319->286|320->287|320->287|320->287|321->288|322->289|322->289|323->290|323->290|323->290|323->290|323->290|324->291|325->292|325->292|326->293|326->293|329->296|329->296|330->297|330->297|330->297|331->298|333->300|333->300|333->300|333->300|334->301|336->303|336->303|337->304|337->304|340->307|341->308|341->308|341->308|341->308|342->309|342->309|342->309|342->309|343->310|343->310|343->310|343->310|344->311|344->311|344->311|344->311|345->312|345->312|345->312|345->312|346->313|348->315|348->315|349->316|349->316|349->316|350->317|350->317|350->317|351->318|352->319|352->319|352->319|352->319|353->320|353->320|353->320|353->320|353->320|354->321|355->322|355->322|355->322|355->322|356->323|356->323|357->324|357->324|358->325|358->325|361->328|361->328|361->328|362->329|363->330|363->330|367->334|367->334|368->335|368->335|368->335|369->336|371->338|371->338|372->339|372->339|375->342|375->342|376->343|376->343|376->343|377->344|381->348|381->348|381->348|382->349|382->349|385->352|385->352|385->352|386->353|387->354|387->354|388->355|390->357|390->357|391->358|391->358|392->359|392->359|393->360|394->361|394->361|395->362|397->364|397->364|398->365|398->365|399->366|399->366|400->367|401->368|401->368|402->369|404->371|404->371|405->372|405->372|406->373|406->373|407->374|408->375|408->375|409->376|411->378|411->378|412->379|412->379|414->381|414->381|415->382|416->383|416->383|417->384|417->384|418->385|419->386|419->386|420->387|420->387|421->388|422->389|422->389|423->390|423->390|424->391|425->392|425->392|426->393|426->393|427->394|428->395|428->395|429->396|429->396|430->397|431->398|431->398|432->399|432->399|433->400|434->401|434->401|435->402|435->402|436->403|437->404|437->404|438->405|438->405|439->406|440->407|440->407|442->409|442->409|443->410|444->411|444->411|445->412|446->413|446->413|448->415|448->415|449->416|450->417|450->417|451->418|452->419|452->419|453->420|453->420|453->420|453->420|453->420|454->421|454->421|454->421|455->422|455->422|455->422|456->423|458->425|458->425|459->426|460->427|460->427|461->428|462->429|462->429|462->429|462->429|462->429|463->430|464->431|464->431|465->432|466->433|466->433|467->434|467->434|468->435|468->435|469->436|469->436|471->438|471->438|472->439|472->439|474->441
                  -- GENERATED --
              */
          