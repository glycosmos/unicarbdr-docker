
package views.html.dataUpload.sheets

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object lcsheet_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

     object lcsheet_Scope1 {
import service.FormLC
import helper._

class lcsheet extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template2[Form[_$1] forSome { 
   type _$1 >: _root_.scala.Nothing <: _root_.scala.Any
},Map[String, Map[String, String]],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*3.2*/(form: Form[_], optionMaps: Map[String, Map[String,String]]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*3.62*/("""

"""),format.raw/*5.1*/("""<div id="ms-table">

    <h3 class="ms-head">LC-MS Settings</h3>
    <h5>For uploading of data to UniCarb-DB</h5>

    <br /> <br />
    <br /> <br />
    <h4>General features — (a) Global descriptors</h4>
    <br />
    <div class="ms-row" title="The date on which the work described was completed; given in the standard ‘YYYY-MM-DD’ format (with hyphens).">
        <div class="ms-label">
            <p>Date stamp</p>
        </div>
        <div class="ms-input">"""),_display_(/*18.32*/helper/*18.38*/.inputDate(form("l_date"), '_label -> null, '_class -> "search-field row-fluid date-stamp val v-lc")),format.raw/*18.138*/("""</div>
        <span class="red-star">*</span>
    </div>
    <div class="ms-row" title="The (stable) primary contact person for this data set; this could be the experimenter, lab head, line manager etc. Where responsibility rests with an institutional role (e.g., one of a number of duty officers) rather than a person, give the official name of the role rather than any one person.">
        <div class="ms-label">
            <p>Responsible person. Provide name,</p>
        </div>
        <div class="ms-input">"""),_display_(/*25.32*/helper/*25.38*/.inputText(form("l_responsiblePerson"), '_label -> null, '_class -> "search-field row-fluid responsible val v-lc")),format.raw/*25.152*/("""</div>
        <span class="red-star">*</span>
    </div>
    <div class="ms-row">
        <div class="ms-label">
            <p>Affiliation</p>
        </div>
        <div class="ms-input">"""),_display_(/*32.32*/helper/*32.38*/.inputText(form("l_affiliation"), '_label -> null, '_class -> "search-field row-fluid affiliation val v-lc")),format.raw/*32.146*/("""</div>
        <span class="red-star">*</span>
    </div>
    <div class="ms-row">
        <div class="ms-label">
            <p>Stable contact information</p>
        </div>
        <div class="ms-input">"""),_display_(/*39.32*/helper/*39.38*/.inputText(form("l_contactInformation"), '_label -> null, '_class -> "search-field row-fluid contactInformation val v-lc")),format.raw/*39.160*/("""</div>
        <span class="red-star">*</span>
    </div>
    <br /> <br /> <br />


    <h4>1. HPLC</h4>
    <div class="ms-row">
        <div class="ms-label"><p>HPLC manufacturer</p></div>
        <div class="ms-input">"""),_display_(/*48.32*/select(form("l_hplcManufacturer"), options(optionMaps.get("HplcManufacturer")), '_label -> null)),format.raw/*48.128*/("""</div>
    </div>
    <div class="ms-row">
        <div class="ms-label"><p>HPLC brand</p></div>
        <div class="ms-input">"""),_display_(/*52.32*/select(form("l_hplcBrand"), options(optionMaps.get("HplcBrand")), '_label -> null)),format.raw/*52.114*/("""</div>
    </div>
    <div class="ms-row">
        <div class="ms-label"><p>Injector</p></div>
        <div class="ms-input">"""),_display_(/*56.32*/helper/*56.38*/.inputText(form("l_injector"), '_label -> null, '_class -> "search-field row-fluid")),format.raw/*56.122*/("""</div>
    </div>
    <div class="ms-row">
        <div class="ms-label"><p>Injector settings</p></div>
        <div class="ms-input">"""),_display_(/*60.32*/helper/*60.38*/.inputText(form("l_injectorSettings"), '_label -> null, '_class -> "search-field row-fluid")),format.raw/*60.130*/("""</div>
    </div>
    <br /> <br /> <br />


    <h4>2. Method run</h4>
    <div class="ms-row">
        <div class="ms-label"><p>Temperature (°C)</p></div>
        <div class="ms-input">"""),_display_(/*68.32*/helper/*68.38*/.inputText(form("l_temp"), '_label -> null,  '_class -> "search-field number-field")),format.raw/*68.122*/("""</div>
    </div>
    <div class="ms-row">
        <div class="ms-label"><p>Solvent a</p></div>
        <div class="ms-input">"""),_display_(/*72.32*/helper/*72.38*/.inputText(form("l_solventA"), '_label -> null, '_class -> "search-field row-fluid val v-lc")),format.raw/*72.131*/("""</div>
        <span class="red-star">*</span>
    </div>
    <div class="ms-row">
        <div class="ms-label"><p>Solvent b</p></div>
        <div class="ms-input">"""),_display_(/*77.32*/helper/*77.38*/.inputText(form("l_solventB"), '_label -> null, '_class -> "search-field row-fluid val v-lc")),format.raw/*77.131*/("""</div>
        <span class="red-star">*</span>
    </div>
    <div class="ms-row">
        <div class="ms-label"><p>Other solvent</p></div>
        <div class="ms-input">"""),_display_(/*82.32*/helper/*82.38*/.inputText(form("l_solventD"), '_label -> null, '_class -> "search-field row-fluid")),format.raw/*82.122*/("""</div>
    </div>
    <div class="ms-row">
        <div class="ms-label"><p>Flow rate </p></div>
        <div class="ms-input">"""),_display_(/*86.32*/helper/*86.38*/.inputText(form("l_flowRate"), '_label -> null, '_class -> "search-field number-field val v-lc")),format.raw/*86.134*/("""</div>
        <span class="red-star">*</span>
    </div>
    <div class="ms-row">
        <div class="ms-label"><p>Gradient </p></div>
        <div class="ms-input">"""),_display_(/*91.32*/helper/*91.38*/.inputText(form("l_flowGradient"), '_label -> null, '_class -> "search-field row-fluid")),format.raw/*91.126*/("""</div>
    </div>
    <div class="ms-row">
        <div class="ms-label"><p>Run time </p></div>
        <div class="ms-input">"""),_display_(/*95.32*/helper/*95.38*/.inputText(form("l_runtime"), '_label -> null,  '_class -> "search-field row-fluid number-field")),format.raw/*95.135*/("""</div>
    </div>
    <div class="ms-row">
        <div class="ms-label"><p>Phase</p></div>
        <div class="ms-input">"""),_display_(/*99.32*/helper/*99.38*/.inputText(form("l_phase"), '_label -> null, '_class -> "search-field row-fluid val v-lc")),format.raw/*99.128*/("""</div>
        <span class="red-star">*</span>
    </div>
    <br /> <br /> <br />


    <h4>3. Column</h4>
    <div class="ms-row">
        <div class="ms-label"><p>Manufacturer</p></div>
        <div class="ms-input">"""),_display_(/*108.32*/select(form("l_manufacturer"), options(optionMaps.get("ColumnManufacturer")), '_label -> null)),format.raw/*108.126*/("""</div>
    </div>
    <div class="ms-row">
        <div class="ms-label"><p>Model</p></div>
        <div class="ms-input">"""),_display_(/*112.32*/select(form("l_model"), options(optionMaps.get("ColumnModel")), '_label -> null)),format.raw/*112.112*/("""</div>
    </div>
    <div class="ms-row">
        <div class="ms-label"><p>Type of chromatography</p></div>
        <div class="ms-input">"""),_display_(/*116.32*/helper/*116.38*/.inputText(form("l_chromatography"), '_label -> null, '_class -> "search-field row-fluid")),format.raw/*116.128*/("""</div>
    </div>
    <div class="ms-row">
        <div class="ms-label"><p>Type of material</p></div>
        <div class="ms-input">"""),_display_(/*120.32*/helper/*120.38*/.inputText(form("l_material"), '_label -> null, '_class -> "search-field row-fluid")),format.raw/*120.122*/("""</div>
    </div>
    <div class="ms-row">
        <div class="ms-label"><p>Column diameter (mm)</p></div>
        <div class="ms-input">"""),_display_(/*124.32*/helper/*124.38*/.inputText(form("l_columnDiameter"), '_label -> null,  '_class -> "search-field row-fluid number-field")),format.raw/*124.142*/("""</div>
    </div>
    <div class="ms-row">
        <div class="ms-label"><p>Column length (mm)</p></div>
        <div class="ms-input">"""),_display_(/*128.32*/helper/*128.38*/.inputText(form("l_columnLength"), '_label -> null,  '_class -> "search-field row-fluid number-field")),format.raw/*128.140*/("""</div>
    </div>
    <div class="ms-row">
        <div class="ms-label"><p>Particle size (μm)</p></div>
        <div class="ms-input">"""),_display_(/*132.32*/helper/*132.38*/.inputText(form("l_particleSize"), '_label -> null, '_class -> "search-field")),format.raw/*132.116*/("""</div>
    </div>
    <br>
</div>

    <script>
        var l_hplcManufacturer = '"""),_display_(/*138.36*/form("l_hplcManufacturer")/*138.62*/.value),format.raw/*138.68*/("""';
        var l_hplcBrand = '"""),_display_(/*139.29*/form("l_hplcBrand")/*139.48*/.value),format.raw/*139.54*/("""';
        var l_manufacturer = '"""),_display_(/*140.32*/form("l_manufacturer")/*140.54*/.value),format.raw/*140.60*/("""';
        var l_model = '"""),_display_(/*141.25*/form("l_model")/*141.40*/.value),format.raw/*141.46*/("""';


            $(document).ready(function() """),format.raw/*144.42*/("""{"""),format.raw/*144.43*/("""
                """),format.raw/*145.17*/("""if ($("#l_hplcManufacturer").val() != l_hplcManufacturer) """),format.raw/*145.75*/("""{"""),format.raw/*145.76*/("""
                    """),format.raw/*146.21*/("""$("#l_hplcManufacturer").css("border", "solid 1px #ffbb44");
                """),format.raw/*147.17*/("""}"""),format.raw/*147.18*/("""
                """),format.raw/*148.17*/("""$("#l_hplcManufacturer").change(function () """),format.raw/*148.61*/("""{"""),format.raw/*148.62*/("""
                        """),format.raw/*149.25*/("""$("#l_hplcManufacturer").css("border", "");
                """),format.raw/*150.17*/("""}"""),format.raw/*150.18*/(""");
                if ($("#l_hplcBrand").val() != l_hplcBrand) """),format.raw/*151.61*/("""{"""),format.raw/*151.62*/("""
                    """),format.raw/*152.21*/("""$("#l_hplcBrand").css("border", "solid 1px #ffbb44");
                """),format.raw/*153.17*/("""}"""),format.raw/*153.18*/("""
                """),format.raw/*154.17*/("""$("#l_hplcBrand").change(function () """),format.raw/*154.54*/("""{"""),format.raw/*154.55*/("""
                        """),format.raw/*155.25*/("""$("#l_hplcBrand").css("border", "");
                """),format.raw/*156.17*/("""}"""),format.raw/*156.18*/(""");
                if ($("#l_manufacturer").val() != l_manufacturer) """),format.raw/*157.67*/("""{"""),format.raw/*157.68*/("""
                    """),format.raw/*158.21*/("""$("#l_manufacturer").css("border", "solid 1px #ffbb44");
                """),format.raw/*159.17*/("""}"""),format.raw/*159.18*/("""
                """),format.raw/*160.17*/("""$("#l_manufacturer").change(function () """),format.raw/*160.57*/("""{"""),format.raw/*160.58*/("""
                    """),format.raw/*161.21*/("""$("#l_manufacturer").css("border", "");
                """),format.raw/*162.17*/("""}"""),format.raw/*162.18*/(""");
                console.log("model: |" + l_model +"|");
                console.log("elemet: |" + $("#l_model").val() + "|");
                if ($("#l_model").val() != l_model) """),format.raw/*165.53*/("""{"""),format.raw/*165.54*/("""
                    """),format.raw/*166.21*/("""$("#l_model").css("border", "solid 1px #ffbb44");
                    console.log("model: |" + l_model +"|");
                    console.log("elemet: |" + $("#l_model").val() + "|");
                """),format.raw/*169.17*/("""}"""),format.raw/*169.18*/("""
                """),format.raw/*170.17*/("""$("#l_model").change(function () """),format.raw/*170.50*/("""{"""),format.raw/*170.51*/("""
                    """),format.raw/*171.21*/("""$("#l_model").css("border", "");
                """),format.raw/*172.17*/("""}"""),format.raw/*172.18*/(""");
            """),format.raw/*173.13*/("""}"""),format.raw/*173.14*/(""");
    </script>"""))
      }
    }
  }

  def render(form:Form[_$1] forSome { 
   type _$1 >: _root_.scala.Nothing <: _root_.scala.Any
},optionMaps:Map[String, Map[String, String]]): play.twirl.api.HtmlFormat.Appendable = apply(form,optionMaps)

  def f:((Form[_$1] forSome { 
   type _$1 >: _root_.scala.Nothing <: _root_.scala.Any
},Map[String, Map[String, String]]) => play.twirl.api.HtmlFormat.Appendable) = (form,optionMaps) => apply(form,optionMaps)

  def ref: this.type = this

}


}
}

/**/
object lcsheet extends lcsheet_Scope0.lcsheet_Scope1.lcsheet
              /*
                  -- GENERATED --
                  DATE: Wed Jun 19 09:50:52 UTC 2019
                  SOURCE: /app/app/views/dataUpload/sheets/lcsheet.scala.html
                  HASH: fe8f5b5806887279346a7dc6c246e7da2f59d213
                  MATRIX: 940->41|1095->101|1123->103|1617->570|1632->576|1754->676|2297->1192|2312->1198|2448->1312|2666->1503|2681->1509|2811->1617|3044->1823|3059->1829|3203->1951|3453->2174|3571->2270|3726->2398|3830->2480|3983->2606|3998->2612|4104->2696|4266->2831|4281->2837|4395->2929|4610->3117|4625->3123|4731->3207|4885->3334|4900->3340|5015->3433|5209->3600|5224->3606|5339->3699|5537->3870|5552->3876|5658->3960|5813->4088|5828->4094|5946->4190|6140->4357|6155->4363|6265->4451|6419->4578|6434->4584|6553->4681|6703->4804|6718->4810|6830->4900|7078->5120|7195->5214|7346->5337|7449->5417|7617->5557|7633->5563|7746->5653|7908->5787|7924->5793|8031->5877|8197->6015|8213->6021|8340->6125|8504->6261|8520->6267|8645->6369|8809->6505|8825->6511|8926->6589|9037->6672|9073->6698|9101->6704|9160->6735|9189->6754|9217->6760|9279->6794|9311->6816|9339->6822|9394->6849|9419->6864|9447->6870|9522->6916|9552->6917|9598->6934|9685->6992|9715->6993|9765->7014|9871->7091|9901->7092|9947->7109|10020->7153|10050->7154|10104->7179|10193->7239|10223->7240|10315->7303|10345->7304|10395->7325|10494->7395|10524->7396|10570->7413|10636->7450|10666->7451|10720->7476|10802->7529|10832->7530|10930->7599|10960->7600|11010->7621|11112->7694|11142->7695|11188->7712|11257->7752|11287->7753|11337->7774|11422->7830|11452->7831|11662->8012|11692->8013|11742->8034|11971->8234|12001->8235|12047->8252|12109->8285|12139->8286|12189->8307|12267->8356|12297->8357|12341->8372|12371->8373
                  LINES: 33->3|38->3|40->5|53->18|53->18|53->18|60->25|60->25|60->25|67->32|67->32|67->32|74->39|74->39|74->39|83->48|83->48|87->52|87->52|91->56|91->56|91->56|95->60|95->60|95->60|103->68|103->68|103->68|107->72|107->72|107->72|112->77|112->77|112->77|117->82|117->82|117->82|121->86|121->86|121->86|126->91|126->91|126->91|130->95|130->95|130->95|134->99|134->99|134->99|143->108|143->108|147->112|147->112|151->116|151->116|151->116|155->120|155->120|155->120|159->124|159->124|159->124|163->128|163->128|163->128|167->132|167->132|167->132|173->138|173->138|173->138|174->139|174->139|174->139|175->140|175->140|175->140|176->141|176->141|176->141|179->144|179->144|180->145|180->145|180->145|181->146|182->147|182->147|183->148|183->148|183->148|184->149|185->150|185->150|186->151|186->151|187->152|188->153|188->153|189->154|189->154|189->154|190->155|191->156|191->156|192->157|192->157|193->158|194->159|194->159|195->160|195->160|195->160|196->161|197->162|197->162|200->165|200->165|201->166|204->169|204->169|205->170|205->170|205->170|206->171|207->172|207->172|208->173|208->173
                  -- GENERATED --
              */
          