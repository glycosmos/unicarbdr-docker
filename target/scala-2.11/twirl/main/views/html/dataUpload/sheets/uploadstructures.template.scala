
package views.html.dataUpload.sheets

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object uploadstructures_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

     object uploadstructures_Scope1 {
import service.{FormMS, FormStructure, StructureForm, StructureForms}
import helper._

class uploadstructures extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template3[Form[StructureForms],List[StructureForm],Form[FormStructure],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*3.2*/(listForm: Form[StructureForms], list: List[StructureForm], form: Form[FormStructure]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {

implicit def /*5.2*/implicitFieldConstructor/*5.26*/ = {{ b3.vertical.fieldConstructor }};
Seq[Any](format.raw/*3.88*/("""

"""),format.raw/*5.62*/("""

"""),format.raw/*7.1*/("""<script src=""""),_display_(/*7.15*/routes/*7.21*/.Assets.versioned("javascripts/st.js")),format.raw/*7.59*/(""""></script>
<script src=""""),_display_(/*8.15*/routes/*8.21*/.Assets.versioned("javascripts/d3.js")),format.raw/*8.59*/(""""></script>

    <section id="structureLayout">
        <div id="layouts">

            <h4 class="ms-head">Structures</h4>
            <br />

            """),_display_(/*16.14*/for((st, index) <- list.zipWithIndex) yield /*16.51*/ {_display_(Seq[Any](format.raw/*16.53*/("""
                        """),format.raw/*17.25*/("""<div style="border-bottom: solid 1px #f0f0f0"></div>
                        """),_display_(/*18.26*/views/*18.31*/.html.dataUpload.sheets.structureitem(st, index)),format.raw/*18.79*/("""  """)))}),format.raw/*18.82*/("""

                    """),format.raw/*20.21*/("""</tbody>
                </table>
            </div>
        </div>
    </section>
    <div id="big-chart-overlay">
        <div id="big-chart">
            <div id="big-chart-btn"><img src="../assets/images/icons/icon_xmark.png" width="16px"></div>
        </div>
    </div>


    <style>
        #big-chart-overlay """),format.raw/*33.28*/("""{"""),format.raw/*33.29*/("""
            """),format.raw/*34.13*/("""position: fixed;
            display: none;
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: rgba(0,0,0,0.5);
            z-index: 2;
        """),format.raw/*44.9*/("""}"""),format.raw/*44.10*/("""
        """),format.raw/*45.9*/("""#big-chart"""),format.raw/*45.19*/("""{"""),format.raw/*45.20*/("""
            """),format.raw/*46.13*/("""position: absolute;
            top: 50%;
            left: 50%;
            font-size: 12px;
            background-color: white;
            width: 800px;
            height: 500px;
            border-radius: 4px;
            transform: translate(-50%,-50%);
            -ms-transform: translate(-50%,-50%);
        """),format.raw/*56.9*/("""}"""),format.raw/*56.10*/("""
        """),format.raw/*57.9*/("""#big-chart-btn"""),format.raw/*57.23*/("""{"""),format.raw/*57.24*/("""
            """),format.raw/*58.13*/("""position: absolute;
            right: 0px;
            top: 0px;
            width: 46px;
            height: 30px;
            border-radius: 0px 4px 0px 2px;
            border: solid 1px #fff;
            background-color: #8f1620;
            color: #fff;
            font-size: 20px;
            text-align: center;
            opacity: 0.6;
            cursor: pointer;
            padding-top: 6px;
        """),format.raw/*72.9*/("""}"""),format.raw/*72.10*/("""
    """),format.raw/*73.5*/("""</style>

    <script>
        $(document).ready(function()"""),format.raw/*76.37*/("""{"""),format.raw/*76.38*/("""

            """),format.raw/*78.13*/("""$("#big-chart-btn").click(function () """),format.raw/*78.51*/("""{"""),format.raw/*78.52*/("""
                """),format.raw/*79.17*/("""$("#big-chart-overlay").hide();
            """),format.raw/*80.13*/("""}"""),format.raw/*80.14*/(""");

        """),format.raw/*82.9*/("""}"""),format.raw/*82.10*/(""");

        function itemExpander(element) """),format.raw/*84.40*/("""{"""),format.raw/*84.41*/("""

            """),format.raw/*86.13*/("""var e = $(element).children(".item-extension");
            if($(e).is(':visible'))"""),format.raw/*87.36*/("""{"""),format.raw/*87.37*/("""
                """),format.raw/*88.17*/("""e.hide(100);
                $(element).children(".down-arrow").show();
            """),format.raw/*90.13*/("""}"""),format.raw/*90.14*/(""" """),format.raw/*90.15*/("""else """),format.raw/*90.20*/("""{"""),format.raw/*90.21*/("""
                """),format.raw/*91.17*/("""e.show(100);
                $(element).children(".down-arrow").hide();
            """),format.raw/*93.13*/("""}"""),format.raw/*93.14*/("""
        """),format.raw/*94.9*/("""}"""),format.raw/*94.10*/("""

        """),format.raw/*96.9*/("""function specOverlay(jsonObject, largest) """),format.raw/*96.51*/("""{"""),format.raw/*96.52*/("""
            """),format.raw/*97.13*/("""$("#big-chart-overlay").show();
            $("#big-chart-overlay .st-base").remove();
            chart = st.chart
                    .ms()
                    .xlabel("m/z")
                    .ylabel("Abundance")
                    .margins([50, 80, 80, 120]);
            chart.render("#big-chart");           // render chart to id 'stgraph'

            var set = st.data.set()        // data type (set)
                    .ylimits([0, largest])        // y-axis limits
                    .x("peaks.mz")             // x-accessor
                    .y("peaks.intensity")      // y-accessor
                    .title("spectrumId");      // id-accessor

            chart.load(set);               // bind the data set

            set.add([jsonObject], []);
        """),format.raw/*115.9*/("""}"""),format.raw/*115.10*/("""
    """),format.raw/*116.5*/("""</script>"""))
      }
    }
  }

  def render(listForm:Form[StructureForms],list:List[StructureForm],form:Form[FormStructure]): play.twirl.api.HtmlFormat.Appendable = apply(listForm,list,form)

  def f:((Form[StructureForms],List[StructureForm],Form[FormStructure]) => play.twirl.api.HtmlFormat.Appendable) = (listForm,list,form) => apply(listForm,list,form)

  def ref: this.type = this

}


}
}

/**/
object uploadstructures extends uploadstructures_Scope0.uploadstructures_Scope1.uploadstructures
              /*
                  -- GENERATED --
                  DATE: Wed Jun 19 09:50:51 UTC 2019
                  SOURCE: /app/app/views/dataUpload/sheets/uploadstructures.scala.html
                  HASH: 3ab0b8b3d9e1ea5c5b856330ac8218deee8148c5
                  MATRIX: 964->89|1137->178|1169->202|1235->175|1264->238|1292->240|1332->254|1346->260|1404->298|1456->324|1470->330|1528->368|1712->525|1765->562|1805->564|1858->589|1963->667|1977->672|2046->720|2080->723|2130->745|2475->1062|2504->1063|2545->1076|2832->1336|2861->1337|2897->1346|2935->1356|2964->1357|3005->1370|3350->1688|3379->1689|3415->1698|3457->1712|3486->1713|3527->1726|3969->2141|3998->2142|4030->2147|4117->2206|4146->2207|4188->2221|4254->2259|4283->2260|4328->2277|4400->2321|4429->2322|4468->2334|4497->2335|4568->2378|4597->2379|4639->2393|4750->2476|4779->2477|4824->2494|4936->2578|4965->2579|4994->2580|5027->2585|5056->2586|5101->2603|5213->2687|5242->2688|5278->2697|5307->2698|5344->2708|5414->2750|5443->2751|5484->2764|6288->3540|6318->3541|6351->3546
                  LINES: 31->3|35->5|35->5|36->3|38->5|40->7|40->7|40->7|40->7|41->8|41->8|41->8|49->16|49->16|49->16|50->17|51->18|51->18|51->18|51->18|53->20|66->33|66->33|67->34|77->44|77->44|78->45|78->45|78->45|79->46|89->56|89->56|90->57|90->57|90->57|91->58|105->72|105->72|106->73|109->76|109->76|111->78|111->78|111->78|112->79|113->80|113->80|115->82|115->82|117->84|117->84|119->86|120->87|120->87|121->88|123->90|123->90|123->90|123->90|123->90|124->91|126->93|126->93|127->94|127->94|129->96|129->96|129->96|130->97|148->115|148->115|149->116
                  -- GENERATED --
              */
          