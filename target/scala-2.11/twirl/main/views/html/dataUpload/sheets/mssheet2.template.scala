
package views.html.dataUpload.sheets

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object mssheet2_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

     object mssheet2_Scope1 {
import src.TableFragment
import helper._

class mssheet2 extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template4[Form[_$1] forSome { 
   type _$1 >: _root_.scala.Nothing <: _root_.scala.Any
},Form[_$2] forSome { 
   type _$2 >: _root_.scala.Nothing <: _root_.scala.Any
},List[TableFragment],Map[String, Map[String, String]],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*3.2*/(form: Form[_], tablesForm: Form[_], tables: List[TableFragment], optionMaps: Map[String, Map[String,String]]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*3.112*/("""



"""),format.raw/*7.1*/("""<div id="ms-table">
    <br /> <br /> <br />
    <h4>4. Spectrum and peak list generation and annotation</h4>
    <br />
    <div class="ms-area-label">
        <p>For this section, if software other than that listed in 1b (Control and analysis software) is used to perform a task, it must be supplied in each case.</p>
    </div>

    """),_display_(/*15.6*/views/*15.11*/.html.dataUpload.sheets.formtablefragment(tablesForm, tables(5))),format.raw/*15.75*/("""


    """),format.raw/*18.5*/("""<br/><br/><br/>
    <h5>Spectrum and peak list generation and annotation — (a) Spectrum description</h5>
    <br/>
    <div class="ms-area-label" title="The location and filename under which the original raw data file from the mass spectrometer is stored, if available. Also give the type of the file where appropriate, or else a description of the software or reference resource used to generate it. Ideally this should be a URI or filename, or most preferably an LSID, where feasible. Due to the nature of the raw files (proprietary formats, no open source software, licensing, etc), the validation of raw data can only be possible if the information is provided in an open XML format (mzXML, mzData, mzML)">
        <p>Location of source (‘raw’) files</p>
    </div>
    """),_display_(/*24.6*/views/*24.11*/.html.dataUpload.sheets.formtablefragment(tablesForm, tables(6))),format.raw/*24.75*/("""


    """),format.raw/*27.5*/("""<br/><br/><br/>
    <h5>Peak list generation and annotation — (b) Peak list generation</h5>
    <br/>
    <div class="ms-area-label">
        <p>Since several different applications may be used for the data acquisition, data post processing and spectrum annotation each used software should be recorded separately together with the information what modification has been done to the data.</p>
    </div>
    <br />

    """),_display_(/*35.6*/views/*35.11*/.html.dataUpload.sheets.formtablefragment(tablesForm, tables(7))),format.raw/*35.75*/("""
    """),format.raw/*36.5*/("""<br /><br /><br />
    <h5 title="Information about the produced data file.">
        Data file(s)
    </h5>
    """),_display_(/*40.6*/views/*40.11*/.html.dataUpload.sheets.formtablefragment(tablesForm, tables(8))),format.raw/*40.75*/("""


    """),format.raw/*43.5*/("""<br/><br/>
    <div class="ms-row">
        <div class="ms-label"><p>Acquisition number for all acquisitions.</p></div>
        <div class="ms-input">"""),_display_(/*46.32*/helper/*46.38*/.inputText(form("m_acquisitionNumber"), '_label -> null, '_class -> "search-field row-fluid")),format.raw/*46.131*/("""</div>
    </div>
    <div class="ms-row">
        <div class="ms-label"><p>Generation of peak lists from raw data</p></div>
        <div class="ms-input">"""),_display_(/*50.32*/helper/*50.38*/.inputText(form("m_peakLists"), '_label -> null, '_class -> "search-field row-fluid")),format.raw/*50.123*/("""</div>
    </div>
    <div class="ms-row">
        <div class="ms-label"><p>Raw data scoring</p></div>
        <div class="ms-input">"""),_display_(/*54.32*/helper/*54.38*/.inputText(form("m_rawDataScoring"), '_label -> null, '_class -> "search-field row-fluid")),format.raw/*54.128*/("""</div>
    </div>
    <div class="ms-row">
        <div class="ms-label"><p>Smoothing; whether applied, parameters</p></div>
        <div class="ms-input">"""),_display_(/*58.32*/helper/*58.38*/.inputText(form("m_smoothing"), '_label -> null, '_class -> "search-field row-fluid")),format.raw/*58.123*/("""</div>
    </div>
    <div class="ms-row">
        <div class="ms-label"><p>Background threshold, or algorithm used</p></div>
        <div class="ms-input">"""),_display_(/*62.32*/helper/*62.38*/.inputText(form("m_backgroundThreshold"), '_label -> null, '_class -> "search-field row-fluid")),format.raw/*62.133*/("""</div>
    </div>
    <div class="ms-row">
        <div class="ms-label"><p>Signal-to-noise estimation and method</p></div>
        <div class="ms-input">"""),_display_(/*66.32*/helper/*66.38*/.inputText(form("m_signalToNoise"), '_label -> null, '_class -> "search-field row-fluid")),format.raw/*66.127*/("""</div>
    </div>
    <div class="ms-row">
        <div class="ms-label"><p>Percentage peak height for centroiding</p></div>
        <div class="ms-input">"""),_display_(/*70.32*/helper/*70.38*/.inputText(form("m_peakHeight"), '_label -> null, '_class -> "search-field row-fluid")),format.raw/*70.124*/("""</div>
    </div>
    <div class="ms-row">
        <div class="ms-label"><p>Retention times for all acquisitions</p></div>
        <div class="ms-input">"""),_display_(/*74.32*/helper/*74.38*/.inputText(form("m_retentionTimes"), '_label -> null, '_class -> "search-field row-fluid")),format.raw/*74.128*/("""</div>
    </div>
    <div class="ms-row">
        <div class="ms-label"><p>m/z and intensity values</p></div>
        <div class="ms-input">"""),_display_(/*78.32*/helper/*78.38*/.inputText(form("m_intensityValues"), '_label -> null, '_class -> "search-field row-fluid")),format.raw/*78.129*/("""</div>
    </div>


    <br/><br/><br/><br/>
    <h5>Peak list generation and annotation — (c) Annotation and scoring</h5>
    <br/>
    <div class="ms-area-label">
        <p>Since several different applications may be used for the data acquisition, data post processing and spectrum annotation each used software should be recorded separately together with the information what modification has been done to the data.</p>
    </div>
    <br />
    """),_display_(/*89.6*/views/*89.11*/.html.dataUpload.sheets.formtablefragment(tablesForm, tables(9))),format.raw/*89.75*/("""
    """),format.raw/*90.5*/("""<br/><br /><br />
    <h5 title="Information about the annotation data file. This includes the file format and if applicable the URI to access the file.">
        Data file(s) generated by the software
    </h5>
    """),_display_(/*94.6*/views/*94.11*/.html.dataUpload.sheets.formtablefragment(tablesForm, tables(10))),format.raw/*94.76*/("""

    """),format.raw/*96.5*/("""<br/><br/>
    <h5>Database settings and matching</h5>
    <div class="ms-row">
        <div class="ms-label"><p>Database queried</p></div>
        <div class="ms-input">"""),_display_(/*100.32*/select(form("m_databaseQueried"), options(optionMaps.get("Database")), '_label -> null)),format.raw/*100.119*/("""</div>
    </div>
    <div class="ms-row">
        <div class="ms-label"><p>Taxonomical restrictions</p></div>
        <div class="ms-input">"""),_display_(/*104.32*/helper/*104.38*/.inputText(form("m_taxonomicalRestrictions"), '_label -> null, '_class -> "search-field row-fluid")),format.raw/*104.137*/("""</div>
    </div>
    <div class="ms-row">
        <div class="ms-label"><p>Other restrictions</p></div>
        <div class="ms-input">"""),_display_(/*108.32*/helper/*108.38*/.inputText(form("m_otherRestrictions"), '_label -> null, '_class -> "search-field row-fluid")),format.raw/*108.131*/("""</div>
    </div>
    <div class="ms-row">
        <div class="ms-label"><p>Allowed cleavages</p></div>
        <div class="ms-input">"""),_display_(/*112.32*/helper/*112.38*/.inputText(form("m_allowedCleavages"), '_label -> null, '_class -> "search-field row-fluid")),format.raw/*112.130*/("""</div>
    </div>
    <div class="ms-row">
        <div class="ms-label"><p>Parent error</p></div>
        <div class="ms-input">"""),_display_(/*116.32*/helper/*116.38*/.inputText(form("m_parentError"), '_label -> null, '_class -> "search-field row-fluid")),format.raw/*116.125*/("""</div>
    </div>
    <div class="ms-row">
        <div class="ms-label"><p>Fragment error</p></div>
        <div class="ms-input">"""),_display_(/*120.32*/helper/*120.38*/.inputText(form("m_fragmentError"), '_label -> null, '_class -> "search-field row-fluid")),format.raw/*120.127*/("""</div>
    </div>

    <div class="ms-row">
        <div class="ms-label"><p>Scoring method</p></div>
        <div class="ms-input">"""),_display_(/*125.32*/helper/*125.38*/.inputText(form("m_scoringMethod"), '_label -> null, '_class -> "search-field row-fluid")),format.raw/*125.127*/("""</div>
    </div>
    <div class="ms-row">
        <div class="ms-label"><p>Scoring value format</p></div>
        <div class="ms-input">"""),_display_(/*129.32*/helper/*129.38*/.inputText(form("m_scoringValueFormat"), '_label -> null, '_class -> "search-field row-fluid")),format.raw/*129.132*/("""</div>
    </div>
    <div class="ms-row">
        <div class="ms-label"><p>Scoring algorithm</p></div>
        <div class="ms-input">"""),_display_(/*133.32*/helper/*133.38*/.inputText(form("m_scoringAlgorithm"), '_label -> null, '_class -> "search-field row-fluid")),format.raw/*133.130*/("""</div>
    </div>
    <div class="ms-row">
        <div class="ms-label"><p>Scoring result</p></div>
        <div class="ms-input">"""),_display_(/*137.32*/helper/*137.38*/.inputText(form("m_scoringResult"), '_label -> null, '_class -> "search-field row-fluid")),format.raw/*137.127*/("""</div>
    </div>
    <div class="ms-row">
        <div class="ms-label"><p>Validation status</p></div>
        <div class="ms-input">"""),_display_(/*141.32*/helper/*141.38*/.inputText(form("m_validationStatus"), '_label -> null, '_class -> "search-field row-fluid")),format.raw/*141.130*/("""</div>
    </div>
    <div class="ms-row">
        <div class="ms-label"><p>Validation value format</p></div>
        <div class="ms-input">"""),_display_(/*145.32*/helper/*145.38*/.inputText(form("m_validationMethod"), '_label -> null, '_class -> "search-field row-fluid")),format.raw/*145.130*/("""</div>
    </div>
    <div class="ms-row">
        <div class="ms-label"><p>Validation result</p></div>
        <div class="ms-input">"""),_display_(/*149.32*/helper/*149.38*/.inputText(form("m_validationResult"), '_label -> null, '_class -> "search-field row-fluid")),format.raw/*149.130*/("""</div>
    </div>
    <br/><br/><br/>
</div>"""))
      }
    }
  }

  def render(form:Form[_$1] forSome { 
   type _$1 >: _root_.scala.Nothing <: _root_.scala.Any
},tablesForm:Form[_$2] forSome { 
   type _$2 >: _root_.scala.Nothing <: _root_.scala.Any
},tables:List[TableFragment],optionMaps:Map[String, Map[String, String]]): play.twirl.api.HtmlFormat.Appendable = apply(form,tablesForm,tables,optionMaps)

  def f:((Form[_$1] forSome { 
   type _$1 >: _root_.scala.Nothing <: _root_.scala.Any
},Form[_$2] forSome { 
   type _$2 >: _root_.scala.Nothing <: _root_.scala.Any
},List[TableFragment],Map[String, Map[String, String]]) => play.twirl.api.HtmlFormat.Appendable) = (form,tablesForm,tables,optionMaps) => apply(form,tablesForm,tables,optionMaps)

  def ref: this.type = this

}


}
}

/**/
object mssheet2 extends mssheet2_Scope0.mssheet2_Scope1.mssheet2
              /*
                  -- GENERATED --
                  DATE: Wed Jun 19 09:50:52 UTC 2019
                  SOURCE: /app/app/views/dataUpload/sheets/mssheet2.scala.html
                  HASH: b02736cedf6e47237797227cdf2e325913a2c9a6
                  MATRIX: 1045->44|1251->154|1281->158|1644->495|1658->500|1743->564|1777->571|2578->1346|2592->1351|2677->1415|2711->1422|3158->1843|3172->1848|3257->1912|3289->1917|3429->2031|3443->2036|3528->2100|3562->2107|3740->2258|3755->2264|3870->2357|4053->2513|4068->2519|4175->2604|4336->2738|4351->2744|4463->2834|4646->2990|4661->2996|4768->3081|4952->3238|4967->3244|5084->3339|5266->3494|5281->3500|5392->3589|5575->3745|5590->3751|5698->3837|5879->3991|5894->3997|6006->4087|6175->4229|6190->4235|6303->4326|6780->4777|6794->4782|6879->4846|6911->4851|7154->5068|7168->5073|7254->5138|7287->5144|7486->5315|7596->5402|7766->5544|7782->5550|7904->5649|8068->5785|8084->5791|8200->5884|8363->6019|8379->6025|8494->6117|8652->6247|8668->6253|8778->6340|8938->6472|8954->6478|9066->6567|9227->6700|9243->6706|9355->6795|9521->6933|9537->6939|9654->7033|9817->7168|9833->7174|9948->7266|10108->7398|10124->7404|10236->7493|10399->7628|10415->7634|10530->7726|10699->7867|10715->7873|10830->7965|10993->8100|11009->8106|11124->8198
                  LINES: 35->3|40->3|44->7|52->15|52->15|52->15|55->18|61->24|61->24|61->24|64->27|72->35|72->35|72->35|73->36|77->40|77->40|77->40|80->43|83->46|83->46|83->46|87->50|87->50|87->50|91->54|91->54|91->54|95->58|95->58|95->58|99->62|99->62|99->62|103->66|103->66|103->66|107->70|107->70|107->70|111->74|111->74|111->74|115->78|115->78|115->78|126->89|126->89|126->89|127->90|131->94|131->94|131->94|133->96|137->100|137->100|141->104|141->104|141->104|145->108|145->108|145->108|149->112|149->112|149->112|153->116|153->116|153->116|157->120|157->120|157->120|162->125|162->125|162->125|166->129|166->129|166->129|170->133|170->133|170->133|174->137|174->137|174->137|178->141|178->141|178->141|182->145|182->145|182->145|186->149|186->149|186->149
                  -- GENERATED --
              */
          