
package views.html.dataUpload.sheets

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object mssheet_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

     object mssheet_Scope1 {
import service.{FormSample, FormMS}
import src.TableFragment
import helper._

class mssheet extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template4[Form[_$1] forSome { 
   type _$1 >: _root_.scala.Nothing <: _root_.scala.Any
},Form[_$2] forSome { 
   type _$2 >: _root_.scala.Nothing <: _root_.scala.Any
},List[TableFragment],Map[String, Map[String, String]],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*4.2*/(form: Form[_], tablesForm: Form[_], tables: List[TableFragment], optionMaps: Map[String, Map[String,String]]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*4.112*/("""

"""),format.raw/*6.1*/("""<div id="ms-table">
    <h3 class="ms-head">MIRAGE MS Guidelines</h3>
    <h5>Guidelines for reporting mass spectrometric analysis data of glycans</h5>

    <br /> <br />
    <br /> <br />
    <h4>1. General features</h4>
    <br />
    <h5>General features — (a) Global descriptors</h5>
    <div class="ms-row" title="The date on which the work described was completed; given in the standard ‘YYYY-MM-DD’ format (with hyphens).">
        <div class="ms-label">
            <p>Date stamp</p>
        </div>
        <div class="ms-input">"""),_display_(/*19.32*/helper/*19.38*/.inputDate(form("m_date"), '_label -> null, '_class -> "search-field row-fluid date-stamp val v-ms1")),format.raw/*19.139*/("""</div>
        <span class="red-star">*</span>
    </div>
    <div class="ms-row" title="The (stable) primary contact person for this data set; this could be the experimenter, lab head, line manager etc. Where responsibility rests with an institutional role (e.g., one of a number of duty officers) rather than a person, give the official name of the role rather than any one person.">
        <div class="ms-label">
            <p>Responsible person. Provide name,</p>
        </div>
        <div class="ms-input">"""),_display_(/*26.32*/helper/*26.38*/.inputText(form("m_responsiblePerson"), '_label -> null, '_class -> "search-field row-fluid responsible val v-ms1")),format.raw/*26.153*/("""</div>
        <span class="red-star">*</span>
    </div>
    <div class="ms-row">
        <div class="ms-label">
            <p>Affiliation</p>
        </div>
        <div class="ms-input">"""),_display_(/*33.32*/helper/*33.38*/.inputText(form("m_affiliation"), '_label -> null, '_class -> "search-field row-fluid affiliation val v-ms1")),format.raw/*33.147*/("""</div>
        <span class="red-star">*</span>
    </div>
    <div class="ms-row">
        <div class="ms-label">
            <p>Stable contact information</p>
        </div>
        <div class="ms-input">"""),_display_(/*40.32*/helper/*40.38*/.inputText(form("m_contactInformation"), '_label -> null, '_class -> "search-field row-fluid contactInformation val v-ms1")),format.raw/*40.161*/("""</div>
        <span class="red-star">*</span>
    </div>
    <div class="ms-row">
        <div class="ms-label"><p>Instrument manufacturer</p></div>
        <div class="ms-input">"""),_display_(/*45.32*/select(form("m_manufacturer"), options(optionMaps.get("InstrumentManufacturer")), '_label -> null)),format.raw/*45.130*/("""</div>
    </div>
    <div class="ms-row">
        <div class="ms-label"><p>Instrument model</p></div>
        <div class="ms-input">"""),_display_(/*49.32*/select(form("m_model"), options(optionMaps.get("InstrumentModel")), '_label -> null)),format.raw/*49.116*/("""</div>
    </div>
    <div class="ms-row" title="Any significant (i.e., affecting behaviour) deviations from the manufacturer’s specification for the mass spectrometer.">
        <div class="ms-label">
            <p>Customizations</p>
        </div>
            <div class="ms-input">"""),_display_(/*55.36*/helper/*55.42*/.inputText(form("m_customizations"), '_label -> null, '_class -> "search-field")),format.raw/*55.122*/("""</div>
    </div>
    <div class="ms-row">
        <div class="ms-label"><p>Ion mode</p></div>
        <div class="ms-input">"""),_display_(/*59.32*/helper/*59.38*/.select(field = form("m_ionMode"), options = Seq("Positive"->"Positive", "Negative"->"Negative"), '_label -> null)),format.raw/*59.152*/("""</div>
    </div>
    <br />

    <br /> <br />
    <h5>General features — (b) Control and analysis software</h5>
    """),_display_(/*65.6*/views/*65.11*/.html.dataUpload.sheets.formtablefragment(tablesForm, tables(4))),format.raw/*65.75*/("""


    """),format.raw/*68.5*/("""<br />
    <br />
    <br />
    <h4>2. Ion Sources</h4>
    <br />

    <h5 style="display: inline-block">Ion Sources — (a) Electrospray Ionisation (ESI)</h5>
    <input id ="esi-check" type="checkbox" name="esi-check" style="display: inline-block; margin-left: 20px"  """),_display_(/*75.112*/if(form("m_CheckESI").value.contains("true"))/*75.157*/{_display_(Seq[Any](format.raw/*75.158*/("""checked="checked"""")))}),format.raw/*75.176*/(""" """),format.raw/*75.177*/(""">
    <div id="esi-container">
        <div class="ms-row" title="Whether the sprayer is fed (by, for example, chromatography or CE) or is loaded with sample once (before spraying).">
            <div class="ms-label"><p>Supply type (static, or fed)</p></div>
            <div class="ms-input">"""),_display_(/*79.36*/helper/*79.42*/.inputText(form("m_supplyTypeESI"), '_label -> null, '_class -> "search-field val v-ms1")),format.raw/*79.131*/("""</div>
            <span class="red-star">*</span>
        </div>
        <div class="ms-row">
            <div class="ms-label"><p>Interface name</p></div>
            <div class="ms-input">"""),_display_(/*84.36*/helper/*84.42*/.inputText(form("m_interfaceNameESI"), '_label -> null, '_class -> "search-field row-fluid")),format.raw/*84.134*/("""</div>
        </div>
        <br />
        <br />
        <div title="Where the interface was bought from, plus its catalog number; list any modifications made to the standard specification. If the interface is entirely custom-built, describe it or provide a reference if available.">
            <div class="ms-area-label" >
                <p>Catalog number, vendor, and any modifications made to the standard specification.</p>
            </div>
            <div class="ms-input">"""),_display_(/*92.36*/helper/*92.42*/.textarea(form("m_interfaceDescriptionESI"), '_label -> null, '_class -> "search-field row-fluid")),format.raw/*92.140*/("""</div>
        </div>
        <br />
        <div class="ms-row">
            <div class="ms-label"><p>Sprayer name</p></div>
            <div class="ms-input">"""),_display_(/*97.36*/helper/*97.42*/.inputText(form("m_sprayerNameESI"), '_label -> null, '_class -> "search-field row-fluid val v-ms1")),format.raw/*97.142*/("""</div>
            <span class="red-star">*</span>
        </div>
        <br />
        <br />
        <div title="Where the sprayer was bought from, plus its catalog number; list any modifications made to the standard specification. If the sprayer is entirely custom-built, describe it briefly or provide a reference if available">
            <div class="ms-area-label">
                <p>Sprayer type, coating, manufacturer, model and catalog number (where available)</p>
            </div>
            <div class="ms-input">"""),_display_(/*106.36*/helper/*106.42*/.textarea(form("m_sprayerDescriptionESI"), '_label -> null, '_class -> "search-field row-fluid")),format.raw/*106.138*/("""</div>
        </div>
        <br />
        <div  title="Voltages that are considered as discriminating from an understood standard measurement mode, or important for the interpretation of the data. These might include the voltage applied to the sprayer tip, the voltage applied to the sampling cone, the voltage used to accelerate the ions into the rest of the mass spectrometer (mass analysis + detection) by MS level.">
            <div class="ms-area-label">
                <p>Relevant voltages where appropriate (tip, cone, acceleration)</p>
            </div>
            <div class="ms-input">"""),_display_(/*113.36*/helper/*113.42*/.textarea(form("m_voltagesESI"), '_label -> null, '_class -> "search-field")),format.raw/*113.118*/("""</div>
        </div>
        <br />
        <div class="ms-row">
            <div class="ms-label"><p>Degree of prompt fragmentation evaluated</p></div>
            <div class="ms-input">"""),_display_(/*118.36*/helper/*118.42*/.select(field = form("m_evaluatedFragmentationESI"), options = Seq("Yes" -> "Yes", "No" -> "No", "Not applicable" -> "Not applicable", "Not relevant" -> "Not relevant"), '_label -> null)),format.raw/*118.228*/("""</div>
        </div>
        <div class="ms-row" title="State whether in-source dissociation was performed (increased voltage between sample orifice and first skimmer).">
            <div class="ms-label"><p>Whether in-source dissociation performed</p></div>
            <div class="ms-input">"""),_display_(/*122.36*/helper/*122.42*/.select(field = form("m_inSourceDissociationESI"), options = Seq("Yes" -> "Yes", "No" -> "No"), '_label -> null)),format.raw/*122.154*/("""</div>
        </div>
        <br /> <br />
        <div title="Where appropriate, and if considered as discriminating elements of the source parameters, describe these values.">
            <div class="ms-area-label"><p>Other parameters if discriminant for the experiment (such as nebulizing gas and pressure)</p></div>
            <div class="ms-input">"""),_display_(/*127.36*/helper/*127.42*/.textarea(form("m_otherParametersESI"), '_label -> null, '_class -> "search-field row-fluid")),format.raw/*127.135*/("""</div>
        </div>
    </div>

    <br /> <br />
    <h5 style="display: inline-block">Ion sources — (b) MALDI</h5>
    <input id ="maldi-check" type="checkbox" name="maldi-check" style="display: inline-block; margin-left: 20px" """),_display_(/*133.115*/if(form("m_CheckMALDI").value.contains("true"))/*133.162*/{_display_(Seq[Any](format.raw/*133.163*/("""checked="checked"""")))}),format.raw/*133.181*/(""" """),format.raw/*133.182*/(""">
    <div id="maldi-container">
        <div class="ms-row" title="The material of which the target plate is made (usually stainless steel, or coated glass); if the plate has a special construction then that should be briefly described and catalogue and lot numbers given where available">
            <div class="ms-label"><p>Plate composition (or type)</p></div>
            <div class="ms-input">"""),_display_(/*137.36*/helper/*137.42*/.inputText(form("m_PlateCompositionMALDI"), '_label -> null, '_class -> "search-field val v-ms1")),format.raw/*137.139*/("""</div>
            <span class="red-star">*</span>
        </div>
        <div class="ms-row" title="The material in which the sample is embedded on the target (e.g., 2,5-dihydroxybenzoic acid (DHB)).">
            <div class="ms-label"><p>Matrix composition (if applicable)</p></div>
            <div class="ms-input">"""),_display_(/*142.36*/helper/*142.42*/.inputText(form("m_matrixCompositionMALDI"), '_label -> null, '_class -> "search-field row-fluid val v-ms1")),format.raw/*142.150*/("""</div>
            <span class="red-star">*</span>
        </div>
        <div class="ms-row" title="The method of laying down (matrix and) sample on the target plate (including matrix concentration and solvents applied); for example, matrix+sample in single deposition; or matrix, then matrix+sample (if several matrix substances are used, name each), Recrystallization using volatile solvent; where chromatographic eluent is directly applied to the plate by apparatus, or for other approaches, describe the process and instrumentation involved very briefly and cross-reference.">
            <div class="ms-label"><p>Deposition technique</p></div>
            <div class="ms-input">"""),_display_(/*147.36*/helper/*147.42*/.inputText(form("m_depositionTechniqueMALDI"), '_label -> null, '_class -> "search-field row-fluid val v-ms1")),format.raw/*147.152*/("""</div>
            <span class="red-star">*</span>
        </div>
        <div class="ms-row" title="Voltages considered as relevant for the interpretation of the data. This might include the grid voltage (applied to the grid that sits just in front of the target), the acceleration voltage (used to accelerate the ions into the analyzer part of the mass spectrometer (mass analysis + detection), etc.">
            <div class="ms-label"><p>Relevant voltages where appropriate</p></div>
            <div class="ms-input">"""),_display_(/*152.36*/helper/*152.42*/.inputText(form("m_voltagesMALDI"), '_label -> null, '_class -> "search-field row-fluid val v-ms1")),format.raw/*152.141*/("""</div>
            <span class="red-star">*</span>
        </div>
        <div class="ms-row" title="Yes/No. If yes, provide data showing results">
            <div class="ms-label"><p>Degree of prompt fragmentation evaluated</p></div>
            <div class="ms-input">"""),_display_(/*157.36*/helper/*157.42*/.inputText(form("m_evaluatedFragmentationMALDI"), '_label -> null, '_class -> "search-field row-fluid")),format.raw/*157.145*/("""</div>
        </div>
        <div class="ms-row" title="Confirm whether post-source decay, laser-induced decomposition, or in-source dissociation was performed; if so provide a brief description of the process (for example, summarize the stepwise reduction of reflector voltage).">
            <div class="ms-label"><p>PSD (or LID/ISD) summary, if performed</p></div>
            <div class="ms-input">"""),_display_(/*161.36*/helper/*161.42*/.inputText(form("m_psdMALDI"), '_label -> null, '_class -> "search-field row-fluid")),format.raw/*161.126*/("""</div>
        </div>
        <div class="ms-row" title="State whether a delay between laser shot and ion acceleration is employed.">
            <div class="ms-label"><p>Operation with or without delayed extraction</p></div>
            <div class="ms-input">"""),_display_(/*165.36*/helper/*165.42*/.inputText(form("m_delayedExtractionMALDI"), '_label -> null, '_class -> "search-field row-fluid val v-ms1")),format.raw/*165.150*/("""</div>
            <span class="red-star">*</span>
        </div>
        <div class="ms-row" title="The type of laser and the wavelength of the generated pulse (in nanometers).">
            <div class="ms-label"><p>Laser (e.g., nitrogen) and wavelength (nm)</p></div>
            <div class="ms-input">"""),_display_(/*170.36*/helper/*170.42*/.inputText(form("m_laserMALDI"), '_label -> null, '_class -> "search-field row-fluid val v-ms1")),format.raw/*170.138*/("""</div>
            <span class="red-star">*</span>
        </div>
        <br /> <br />
        <div title="Other details of the laser used to irradiate the matrix-embedded sample if considered as important for the interpretation of data; this might include the pulse energy in microJoules, focus diameter in microns, attenuation details, pulse duration in nanoseconds at full-width half maximum, frequency of shots in Hertz and average number of shots fired to generate each combined mass spectrum.">
            <div class="ms-area-label"><p>Other laser related parameters, if discriminating for the experiment</p></div>
            <div class="ms-input">"""),_display_(/*176.36*/helper/*176.42*/.textarea(form("m_otherParmasMALDI"), '_label -> null, '_class -> "search-field row-fluid")),format.raw/*176.133*/("""</div>
        </div>
        <br />
    </div>

    <h4>3. Ion transfer optics</h4>
    <div class="ms-row" title="e.g. ‘simple’ quadrupoles, hexapoles, stacked ring electrodes, TOF">
        <div class="ms-label">
            <p>Hardware options</p>
        </div>
        <div class="ms-input">"""),_display_(/*186.32*/helper/*186.38*/.inputText(form("m_hardwareOptions"), '_label -> null, '_class -> "search-field row-fluid")),format.raw/*186.129*/("""</div>
    </div>
    <br /> <br /> <br />

    <br/>
    <h5>Post-source componentry — (a)Collision cell</h5>
    <br /> <br/>
    <h5 style="display: inline-block">Collision-Induced Dissociation (CID)</h5>
    <input id ="cid-check" type="checkbox" name="cid-check" style="display: inline-block; margin-left: 20px" """),_display_(/*194.111*/if(form("m_CheckCID").value.contains("true"))/*194.156*/{_display_(Seq[Any](format.raw/*194.157*/("""checked="checked"""")))}),format.raw/*194.175*/(""" """),format.raw/*194.176*/(""">
    <div id="cid-container">
        <div class="ms-row" title="The composition of the gas used to fragment ions in the collision cell (TOF-TOF, linear trap, Paul trap, or FT- ICR cell).">
            <div class="ms-label"><p>Gas composition</p></div>
            <div class="ms-input">"""),_display_(/*198.36*/helper/*198.42*/.inputText(form("m_cidGasComposition"), '_label -> null, '_class -> "search-field row-fluid val v-ms1")),format.raw/*198.145*/("""</div>
            <span class="red-star">*</span>
        </div>
        <div class="ms-row" title="The pressure of the gas used to fragment ions in the collision cell.">
            <div class="ms-label"><p>Gas pressure</p></div>
            <div class="ms-input">"""),_display_(/*203.36*/helper/*203.42*/.inputText(form("m_cidGasPressure"), '_label -> null, '_class -> "search-field row-fluid")),format.raw/*203.132*/("""</div>
        </div>
        <br /> <br />
        <div  title="The specifics for the process of imparting a particular impetus to ions with a given m/z value, as they travel into the collision cell for fragmentation. This could be a global figure (e.g., for tandem TOFs), or a complex function; for example a gradient (stepped or continuous) of m/z values (for quads) or activation frequencies (for traps) with associated collision energies (given in eV)">
            <div class="ms-area-label">
                <p>Collision energy CID/function</p>
            </div>
            <div class="ms-input">"""),_display_(/*210.36*/helper/*210.42*/.textarea(form("m_cidCollisionEnergy"), '_label -> null, '_class -> "search-field row-fluid val v-ms1")),format.raw/*210.145*/("""</div>
            <span class="red-star">*</span>
        </div>
    </div>
    <br />

    <h5 style="display: inline-block">Electron Transfer Dissociation (ETD)</h5>
    <input id ="etd-check" type="checkbox" name="etd-check" style="display: inline-block; margin-left: 20px" """),_display_(/*217.111*/if(form("m_CheckETD").value.contains("true"))/*217.156*/{_display_(Seq[Any](format.raw/*217.157*/("""checked="checked"""")))}),format.raw/*217.175*/(""" """),format.raw/*217.176*/(""">
    <div id="etd-container">
        <div class="ms-row">
            <div class="ms-label"><p>Reagent gas</p></div>
            <div class="ms-input">"""),_display_(/*221.36*/helper/*221.42*/.inputText(form("m_etdReagentGas"), '_label -> null, '_class -> "search-field row-fluid")),format.raw/*221.131*/("""</div>
        </div>
        <div class="ms-row">
            <div class="ms-label"><p>Pressure</p></div>
            <div class="ms-input">"""),_display_(/*225.36*/helper/*225.42*/.inputText(form("m_etdPressure"), '_label -> null, '_class -> "search-field row-fluid")),format.raw/*225.129*/("""</div>
        </div>
        <div class="ms-row">
            <div class="ms-label"><p>Reaction time</p></div>
            <div class="ms-input">"""),_display_(/*229.36*/helper/*229.42*/.inputText(form("m_etdReactionTime"), '_label -> null, '_class -> "search-field row-fluid")),format.raw/*229.133*/("""</div>
        </div>
        <div class="ms-row">
            <div class="ms-label"><p>Number of reagent atoms</p></div>
            <div class="ms-input">"""),_display_(/*233.36*/helper/*233.42*/.inputText(form("m_etdNumberOfAtoms"), '_label -> null, '_class -> "search-field row-fluid")),format.raw/*233.134*/("""</div>
        </div>
    </div>
    <br /> <br />

    <h5 style="display: inline-block">Electron Capture Dissociation (ECD)</h5>
    <input id ="ecd-check" type="checkbox" name="ecd-check" style="display: inline-block; margin-left: 20px" """),_display_(/*239.111*/if(form("m_CheckECD").value.contains("true"))/*239.156*/{_display_(Seq[Any](format.raw/*239.157*/("""checked="checked"""")))}),format.raw/*239.175*/(""" """),format.raw/*239.176*/(""">
    <div id="ecd-container">
        <div class="ms-row">
            <div class="ms-label"><p>Emitter type</p></div>
            <div class="ms-input">"""),_display_(/*243.36*/helper/*243.42*/.inputText(form("m_ecdEmitterType"), '_label -> null, '_class -> "search-field row-fluid val v-ms1")),format.raw/*243.142*/("""</div>
            <span class="red-star">*</span>
        </div>
        <div class="ms-row">
            <div class="ms-label"><p>Voltage</p></div>
            <div class="ms-input">"""),_display_(/*248.36*/helper/*248.42*/.inputText(form("m_ecdVoltage"), '_label -> null, '_class -> "search-field row-fluid")),format.raw/*248.128*/("""</div>
        </div>
        <div class="ms-row">
            <div class="ms-label"><p>Current</p></div>
            <div class="ms-input">"""),_display_(/*252.36*/helper/*252.42*/.inputText(form("m_ecdCurrent"), '_label -> null, '_class -> "search-field row-fluid")),format.raw/*252.128*/("""</div>
        </div>
    </div>
    <br />

    <br /> <br /> <br />
    <h5 style="display: inline-block">Post-source componentry — (b) TOF drift tube</h5>
    <input id ="tof-check" type="checkbox" name="tof-check" style="display: inline-block; margin-left: 20px" """),_display_(/*259.111*/if(form("m_CheckTOF").value.contains("true"))/*259.156*/{_display_(Seq[Any](format.raw/*259.157*/("""checked="checked"""")))}),format.raw/*259.175*/(""" """),format.raw/*259.176*/(""">
    <div id="tof-container">
        <div class="ms-row" title="Whether a Reflectron is present, and if so, whether it is used. Depending on the type of instrument provide exact details on the reflectron mode (e.g. V or W mode).">
            <div class="ms-label"><p>Reflectron status (on, off, none)</p></div>
            <div class="ms-input">"""),_display_(/*263.36*/helper/*263.42*/.inputText(form("m_reflectronStatus"), '_label -> null, '_class -> "search-field row-fluid val v-ms1")),format.raw/*263.144*/("""</div>
            <span class="red-star">*</span>
        </div>
    </div>
    <br/> <br /> <br />

    <h5 style="display: inline-block">Post-source componentry — (c) Ion trap</h5>
    <input id ="trap-check" type="checkbox" name="trap-check" style="display: inline-block; margin-left: 20px" """),_display_(/*270.113*/if(form("m_CheckIonTrap").value.contains("true"))/*270.162*/{_display_(Seq[Any](format.raw/*270.163*/("""checked="checked"""")))}),format.raw/*270.181*/(""" """),format.raw/*270.182*/(""">
    <div id="trap-container">
        <div class="ms-row" title="The final MS level achieved in generating this data set with an ion trap or equivalent (e.g., MS^10).">
            <div class="ms-label"><p>Final MS stage achieved</p></div>
            <div class="ms-input">"""),_display_(/*274.36*/helper/*274.42*/.inputText(form("m_finalMsStage"), '_label -> null, '_class -> "search-field row-fluid")),format.raw/*274.130*/("""</div>
        </div>
    </div>

    <br /> <br /> <br />
    <h5 style="display: inline-block" title="gas, pressure, instrument-specific parameters e.g. wave velocity/height depending on the particular vendor's options for tuning this component">
        Post-source componentry – (d) Ion mobility
    </h5>
    <input id ="mobility-check" type="checkbox" name="mobility-check" style="display: inline-block; margin-left: 20px" """),_display_(/*282.121*/if(form("m_CheckIonMobility").value.contains("true"))/*282.174*/{_display_(Seq[Any](format.raw/*282.175*/("""checked="checked"""")))}),format.raw/*282.193*/(""" """),format.raw/*282.194*/(""">
    <div id="mobility-container">
        <div class="ms-row">
            <div class="ms-label"><p>Gas</p></div>
            <div class="ms-input">"""),_display_(/*286.36*/helper/*286.42*/.inputText(form("m_ionMobilityGas"), '_label -> null, '_class -> "search-field row-fluid val v-ms1")),format.raw/*286.142*/("""</div>
            <span class="red-star">*</span>
        </div>
        <div class="ms-row">
            <div class="ms-label"><p>Pressure</p></div>
            <div class="ms-input">"""),_display_(/*291.36*/helper/*291.42*/.inputText(form("m_ionMobilityPressure"), '_label -> null, '_class -> "search-field row-fluid")),format.raw/*291.137*/("""</div>
        </div>
        <div class="ms-row">
            <div class="ms-label"><p>Instrument-specific parameters</p></div>
            <div class="ms-input">"""),_display_(/*295.36*/helper/*295.42*/.inputText(form("m_ionMobilityInstrumentParameters"), '_label -> null, '_class -> "search-field row-fluid")),format.raw/*295.149*/("""</div>
        </div>
    </div>

    <br /> <br /> <br />
    <h5 style="display: inline-block" title="Peak selection, pulse width, voltage, decay time, IR ... other important experiment parameters.">
        Post-source componentry — (e) FT-ICR
    </h5>
    <input id ="icr-check" type="checkbox" name="icr-check" style="display: inline-block; margin-left: 20px" """),_display_(/*303.111*/if(form("m_CheckFTICR").value.contains("true"))/*303.158*/{_display_(Seq[Any](format.raw/*303.159*/("""checked="checked"""")))}),format.raw/*303.177*/(""" """),format.raw/*303.178*/(""">
    <div id="icr-container">
        <div class="ms-row">
            <div class="ms-label"><p>Peak selection</p></div>
            <div class="ms-input">"""),_display_(/*307.36*/helper/*307.42*/.inputText(form("m_ftIcrPeakSelection"), '_label -> null, '_class -> "search-field row-fluid")),format.raw/*307.136*/("""</div>
        </div>
        <div class="ms-row">
            <div class="ms-label"><p>Pulse</p></div>
            <div class="ms-input">"""),_display_(/*311.36*/helper/*311.42*/.inputText(form("m_ftIcrPulse"), '_label -> null, '_class -> "search-field row-fluid")),format.raw/*311.128*/("""</div>
        </div>
        <div class="ms-row">
            <div class="ms-label"><p>Width</p></div>
            <div class="ms-input">"""),_display_(/*315.36*/helper/*315.42*/.inputText(form("m_ftIcrWidth"), '_label -> null, '_class -> "search-field row-fluid")),format.raw/*315.128*/("""</div>
        </div>
        <div class="ms-row">
            <div class="ms-label"><p>Voltage</p></div>
            <div class="ms-input">"""),_display_(/*319.36*/helper/*319.42*/.inputText(form("m_ftIcrVoltage"), '_label -> null, '_class -> "search-field row-fluid")),format.raw/*319.130*/("""</div>
        </div>
        <div class="ms-row">
            <div class="ms-label"><p>Decay time</p></div>
            <div class="ms-input">"""),_display_(/*323.36*/helper/*323.42*/.inputText(form("m_ftIcrDecayTime"), '_label -> null, '_class -> "search-field row-fluid")),format.raw/*323.132*/("""</div>
        </div>
        <div class="ms-row">
            <div class="ms-label"><p>IR</p></div>
            <div class="ms-input">"""),_display_(/*327.36*/helper/*327.42*/.inputText(form("m_ftIcrIR"), '_label -> null, '_class -> "search-field row-fluid")),format.raw/*327.125*/("""</div>
        </div>
        <div class="ms-row">
            <div class="ms-label"><p>Other parameters</p></div>
            <div class="ms-input">"""),_display_(/*331.36*/helper/*331.42*/.inputText(form("m_ftIcrOtherParameters"), '_label -> null, '_class -> "search-field row-fluid")),format.raw/*331.138*/("""</div>
        </div>
    </div>

    <br /> <br /> <br />
    <h5 style="display: inline-block">Post-source componentry — (f) Detectors</h5>
    <input id ="detectors-check" type="checkbox" name="detectors-check" style="display: inline-block; margin-left: 20px" """),_display_(/*337.123*/if(form("m_CheckDetectors").value.contains("true"))/*337.174*/{_display_(Seq[Any](format.raw/*337.175*/("""checked="checked"""")))}),format.raw/*337.193*/(""" """),format.raw/*337.194*/(""">
    <div id="detectors-container">
        <div class="ms-row" title="Needs defintion if non OEM detector were used (e.g. microchannel plate, channeltron etc.)">
            <div class="ms-label" >
                <p>Detector type</p>
            </div>
            <div class="ms-input">"""),_display_(/*343.36*/helper/*343.42*/.inputText(form("m_detectorType"), '_label -> null, '_class -> "search-field row-fluid")),format.raw/*343.130*/("""</div>
        </div>
    </div>
    <br /> <br /> <br />

    """),_display_(/*348.6*/inputText(form("journalReferenceID"),'style -> "display: none", '_label -> null)),format.raw/*348.86*/("""
    """),_display_(/*349.6*/helper/*349.12*/.repeat(form("gwpFiles"), min = 10)/*349.47*/ { field =>_display_(Seq[Any](format.raw/*349.58*/("""
        """),_display_(/*350.10*/helper/*350.16*/.inputText(field,'style -> "display: none", '_label -> null)),format.raw/*350.76*/("""
    """)))}),format.raw/*351.6*/("""
"""),format.raw/*352.1*/("""</div>

    <script>
            var m_manufacturer = '"""),_display_(/*355.36*/form("m_manufacturer")/*355.58*/.value),format.raw/*355.64*/("""';
            var m_model = '"""),_display_(/*356.29*/form("m_model")/*356.44*/.value),format.raw/*356.50*/("""';
            var m_evaluatedFragmentationESI = '"""),_display_(/*357.49*/form("m_evaluatedFragmentationESI")/*357.84*/.value),format.raw/*357.90*/("""';
            var m_inSourceDissociationESI = '"""),_display_(/*358.47*/form("m_inSourceDissociationESI")/*358.80*/.value),format.raw/*358.86*/("""';

            $(document).ready(function() """),format.raw/*360.42*/("""{"""),format.raw/*360.43*/("""
                """),format.raw/*361.17*/("""if (m_manufacturer != "" && $("#m_manufacturer").val() != m_manufacturer) """),format.raw/*361.91*/("""{"""),format.raw/*361.92*/("""
                    """),format.raw/*362.21*/("""$("#m_manufacturer").css("border", "solid 1px #ffbb44");
                """),format.raw/*363.17*/("""}"""),format.raw/*363.18*/("""
                """),format.raw/*364.17*/("""$("#m_manufacturer").change(function () """),format.raw/*364.57*/("""{"""),format.raw/*364.58*/("""
                    """),format.raw/*365.21*/("""$("#m_manufacturer").css("border", "");
                """),format.raw/*366.17*/("""}"""),format.raw/*366.18*/(""");
                if ($("#m_model").val() != m_model) """),format.raw/*367.53*/("""{"""),format.raw/*367.54*/("""
                    """),format.raw/*368.21*/("""$("#m_model").css("border", "solid 1px #ffbb44");
                """),format.raw/*369.17*/("""}"""),format.raw/*369.18*/("""
                """),format.raw/*370.17*/("""$("#m_model").change(function () """),format.raw/*370.50*/("""{"""),format.raw/*370.51*/("""
                    """),format.raw/*371.21*/("""$("#m_model").css("border", "");
                """),format.raw/*372.17*/("""}"""),format.raw/*372.18*/(""");
                if (m_evaluatedFragmentationESI != "" && $("#m_evaluatedFragmentationESI").val() != m_evaluatedFragmentationESI) """),format.raw/*373.130*/("""{"""),format.raw/*373.131*/("""
                    """),format.raw/*374.21*/("""$("#m_evaluatedFragmentationESI").css("border", "solid 1px #ffbb44");
                """),format.raw/*375.17*/("""}"""),format.raw/*375.18*/("""
                """),format.raw/*376.17*/("""$("#m_evaluatedFragmentationESI").change(function () """),format.raw/*376.70*/("""{"""),format.raw/*376.71*/("""
                    """),format.raw/*377.21*/("""$("#m_evaluatedFragmentationESI").css("border", "");
                """),format.raw/*378.17*/("""}"""),format.raw/*378.18*/(""");
                if (m_inSourceDissociationESI != "" && $("#m_inSourceDissociationESI").val() != m_inSourceDissociationESI) """),format.raw/*379.124*/("""{"""),format.raw/*379.125*/("""
                    """),format.raw/*380.21*/("""$("#m_inSourceDissociationESI").css("border", "solid 1px #ffbb44");
                """),format.raw/*381.17*/("""}"""),format.raw/*381.18*/("""
                """),format.raw/*382.17*/("""$("#m_inSourceDissociationESI").change(function () """),format.raw/*382.68*/("""{"""),format.raw/*382.69*/("""
                    """),format.raw/*383.21*/("""$("#m_inSourceDissociationESI").css("border", "");
                """),format.raw/*384.17*/("""}"""),format.raw/*384.18*/(""");


                checker($("#esi-check"), "#esi-container");
                $("#esi-check").click(function () """),format.raw/*388.51*/("""{"""),format.raw/*388.52*/("""
                   """),format.raw/*389.20*/("""checker($("#esi-check"), "#esi-container");
                """),format.raw/*390.17*/("""}"""),format.raw/*390.18*/(""");

                checker($("#maldi-check"), "#maldi-container");
                $("#maldi-check").click(function () """),format.raw/*393.53*/("""{"""),format.raw/*393.54*/("""
                    """),format.raw/*394.21*/("""checker($("#maldi-check"), "#maldi-container");
                """),format.raw/*395.17*/("""}"""),format.raw/*395.18*/(""");


                checker($("#cid-check"), "#cid-container");
                //$("#cid-check").prop('checked', true);
                $("#cid-check").click(function () """),format.raw/*400.51*/("""{"""),format.raw/*400.52*/("""
                    """),format.raw/*401.21*/("""checker($("#cid-check"), "#cid-container");
                """),format.raw/*402.17*/("""}"""),format.raw/*402.18*/(""");
                checker($("#etd-check"), "#etd-container");
                $("#etd-check").click(function () """),format.raw/*404.51*/("""{"""),format.raw/*404.52*/("""
                    """),format.raw/*405.21*/("""checker($("#etd-check"), "#etd-container");
                """),format.raw/*406.17*/("""}"""),format.raw/*406.18*/(""");
                checker($("#ecd-check"), "#ecd-container");
                $("#ecd-check").click(function () """),format.raw/*408.51*/("""{"""),format.raw/*408.52*/("""
                    """),format.raw/*409.21*/("""checker($("#ecd-check"), "#ecd-container");
                """),format.raw/*410.17*/("""}"""),format.raw/*410.18*/(""");
                checker($("#tof-check"), "#tof-container");
                $("#tof-check").click(function () """),format.raw/*412.51*/("""{"""),format.raw/*412.52*/("""
                    """),format.raw/*413.21*/("""checker($("#tof-check"), "#tof-container");
                """),format.raw/*414.17*/("""}"""),format.raw/*414.18*/(""");
                checker($("#trap-check"), "#trap-container");
                $("#trap-check").click(function () """),format.raw/*416.52*/("""{"""),format.raw/*416.53*/("""
                    """),format.raw/*417.21*/("""checker($("#trap-check"), "#trap-container");
                """),format.raw/*418.17*/("""}"""),format.raw/*418.18*/(""");
                checker($("#mobility-check"), "#mobility-container");
                $("#mobility-check").click(function () """),format.raw/*420.56*/("""{"""),format.raw/*420.57*/("""
                    """),format.raw/*421.21*/("""checker($("#mobility-check"), "#mobility-container");
                """),format.raw/*422.17*/("""}"""),format.raw/*422.18*/(""");
                checker($("#icr-check"), "#icr-container");
                $("#icr-check").click(function () """),format.raw/*424.51*/("""{"""),format.raw/*424.52*/("""
                    """),format.raw/*425.21*/("""checker($("#icr-check"), "#icr-container");
                """),format.raw/*426.17*/("""}"""),format.raw/*426.18*/(""");
                checker($("#detectors-check"), "#detectors-container");
                $("#detectors-check").click(function () """),format.raw/*428.57*/("""{"""),format.raw/*428.58*/("""
                    """),format.raw/*429.21*/("""checker($("#detectors-check"), "#detectors-container");
                """),format.raw/*430.17*/("""}"""),format.raw/*430.18*/(""");

                function checker(checkBox,container) """),format.raw/*432.54*/("""{"""),format.raw/*432.55*/("""
                    """),format.raw/*433.21*/("""if(!checkBox.is(':checked'))"""),format.raw/*433.49*/("""{"""),format.raw/*433.50*/("""
                        """),format.raw/*434.25*/("""checkBox.prop('checked', false);
                        $(container + " input, " + container + " textarea, " + container + " select").each(function () """),format.raw/*435.120*/("""{"""),format.raw/*435.121*/("""
                            """),format.raw/*436.29*/("""$(this).val("NA");
                            $(this).trigger("focusout");
                            $(this).attr('readonly', 'readonly');
                        """),format.raw/*439.25*/("""}"""),format.raw/*439.26*/(""");
                    """),format.raw/*440.21*/("""}"""),format.raw/*440.22*/(""" """),format.raw/*440.23*/("""else """),format.raw/*440.28*/("""{"""),format.raw/*440.29*/("""
                        """),format.raw/*441.25*/("""checkBox.prop('checked', true);
                        $(container + " input, " + container + " textarea, " + container + " select").each(function () """),format.raw/*442.120*/("""{"""),format.raw/*442.121*/("""
                            """),format.raw/*443.29*/("""if($(this).val() === "NA")"""),format.raw/*443.55*/("""{"""),format.raw/*443.56*/("""
                                """),format.raw/*444.33*/("""$(this).val("");
                            """),format.raw/*445.29*/("""}"""),format.raw/*445.30*/("""
                            """),format.raw/*446.29*/("""$(this).trigger("focusout");
                            $(this).removeAttr('readonly');
                        """),format.raw/*448.25*/("""}"""),format.raw/*448.26*/(""");
                    """),format.raw/*449.21*/("""}"""),format.raw/*449.22*/("""
                """),format.raw/*450.17*/("""}"""),format.raw/*450.18*/("""
            """),format.raw/*451.13*/("""}"""),format.raw/*451.14*/(""");
    </script>"""))
      }
    }
  }

  def render(form:Form[_$1] forSome { 
   type _$1 >: _root_.scala.Nothing <: _root_.scala.Any
},tablesForm:Form[_$2] forSome { 
   type _$2 >: _root_.scala.Nothing <: _root_.scala.Any
},tables:List[TableFragment],optionMaps:Map[String, Map[String, String]]): play.twirl.api.HtmlFormat.Appendable = apply(form,tablesForm,tables,optionMaps)

  def f:((Form[_$1] forSome { 
   type _$1 >: _root_.scala.Nothing <: _root_.scala.Any
},Form[_$2] forSome { 
   type _$2 >: _root_.scala.Nothing <: _root_.scala.Any
},List[TableFragment],Map[String, Map[String, String]]) => play.twirl.api.HtmlFormat.Appendable) = (form,tablesForm,tables,optionMaps) => apply(form,tablesForm,tables,optionMaps)

  def ref: this.type = this

}


}
}

/**/
object mssheet extends mssheet_Scope0.mssheet_Scope1.mssheet
              /*
                  -- GENERATED --
                  DATE: Wed Jun 19 09:50:51 UTC 2019
                  SOURCE: /app/app/views/dataUpload/sheets/mssheet.scala.html
                  HASH: fc4dd1b0f37be9ee3978f25091ed7e091884794c
                  MATRIX: 1078->81|1284->191|1312->193|1877->731|1892->737|2015->838|2558->1354|2573->1360|2710->1475|2928->1666|2943->1672|3074->1781|3307->1987|3322->1993|3467->2116|3675->2297|3795->2395|3956->2529|4062->2613|4375->2899|4390->2905|4492->2985|4645->3111|4660->3117|4796->3231|4941->3350|4955->3355|5040->3419|5074->3426|5373->3697|5428->3742|5468->3743|5518->3761|5548->3762|5870->4057|5885->4063|5996->4152|6215->4344|6230->4350|6344->4442|6858->4929|6873->4935|6993->5033|7181->5194|7196->5200|7318->5300|7877->5831|7893->5837|8012->5933|8643->6536|8659->6542|8758->6618|8975->6807|8991->6813|9200->6999|9523->7294|9539->7300|9674->7412|10058->7768|10074->7774|10190->7867|10452->8100|10510->8147|10551->8148|10602->8166|10633->8167|11062->8568|11078->8574|11198->8671|11546->8991|11562->8997|11693->9105|12406->9790|12422->9796|12555->9906|13105->10428|13121->10434|13243->10533|13542->10804|13558->10810|13684->10913|14116->11317|14132->11323|14239->11407|14528->11668|14544->11674|14675->11782|15008->12087|15024->12093|15143->12189|15829->12847|15845->12853|15959->12944|16285->13242|16301->13248|16415->13339|16762->13657|16818->13702|16859->13703|16910->13721|16941->13722|17258->14011|17274->14017|17400->14120|17695->14387|17711->14393|17824->14483|18458->15089|18474->15095|18600->15198|18908->15477|18964->15522|19005->15523|19056->15541|19087->15542|19269->15696|19285->15702|19397->15791|19567->15933|19583->15939|19693->16026|19868->16173|19884->16179|19998->16270|20183->16427|20199->16433|20314->16525|20584->16766|20640->16811|20681->16812|20732->16830|20763->16831|20946->16986|20962->16992|21085->17092|21298->17277|21314->17283|21423->17369|21592->17510|21608->17516|21717->17602|22014->17870|22070->17915|22111->17916|22162->17934|22193->17935|22570->18284|22586->18290|22711->18392|23036->18688|23096->18737|23137->18738|23188->18756|23219->18757|23524->19034|23540->19040|23651->19128|24110->19558|24174->19611|24215->19612|24266->19630|24297->19631|24476->19782|24492->19788|24615->19888|24829->20074|24845->20080|24963->20175|25155->20339|25171->20345|25301->20452|25697->20819|25755->20866|25796->20867|25847->20885|25878->20886|26063->21043|26079->21049|26196->21143|26363->21282|26379->21288|26488->21374|26655->21513|26671->21519|26780->21605|26949->21746|26965->21752|27076->21840|27248->21984|27264->21990|27377->22080|27541->22216|27557->22222|27663->22305|27841->22455|27857->22461|27976->22557|28269->22821|28331->22872|28372->22873|28423->22891|28454->22892|28773->23183|28789->23189|28900->23277|28991->23341|29093->23421|29126->23427|29142->23433|29187->23468|29237->23479|29275->23489|29291->23495|29373->23555|29410->23561|29439->23562|29523->23618|29555->23640|29583->23646|29642->23677|29667->23692|29695->23698|29774->23749|29819->23784|29847->23790|29924->23839|29967->23872|29995->23878|30069->23923|30099->23924|30145->23941|30248->24015|30278->24016|30328->24037|30430->24110|30460->24111|30506->24128|30575->24168|30605->24169|30655->24190|30740->24246|30770->24247|30854->24302|30884->24303|30934->24324|31029->24390|31059->24391|31105->24408|31167->24441|31197->24442|31247->24463|31325->24512|31355->24513|31517->24645|31548->24646|31598->24667|31713->24753|31743->24754|31789->24771|31871->24824|31901->24825|31951->24846|32049->24915|32079->24916|32235->25042|32266->25043|32316->25064|32429->25148|32459->25149|32505->25166|32585->25217|32615->25218|32665->25239|32761->25306|32791->25307|32935->25422|32965->25423|33014->25443|33103->25503|33133->25504|33282->25624|33312->25625|33362->25646|33455->25710|33485->25711|33686->25883|33716->25884|33766->25905|33855->25965|33885->25966|34027->26079|34057->26080|34107->26101|34196->26161|34226->26162|34368->26275|34398->26276|34448->26297|34537->26357|34567->26358|34709->26471|34739->26472|34789->26493|34878->26553|34908->26554|35053->26670|35083->26671|35133->26692|35224->26754|35254->26755|35411->26883|35441->26884|35491->26905|35590->26975|35620->26976|35762->27089|35792->27090|35842->27111|35931->27171|35961->27172|36121->27303|36151->27304|36201->27325|36302->27397|36332->27398|36418->27455|36448->27456|36498->27477|36555->27505|36585->27506|36639->27531|36821->27683|36852->27684|36910->27713|37105->27879|37135->27880|37187->27903|37217->27904|37247->27905|37281->27910|37311->27911|37365->27936|37546->28087|37577->28088|37635->28117|37690->28143|37720->28144|37782->28177|37856->28222|37886->28223|37944->28252|38086->28365|38116->28366|38168->28389|38198->28390|38244->28407|38274->28408|38316->28421|38346->28422
                  LINES: 36->4|41->4|43->6|56->19|56->19|56->19|63->26|63->26|63->26|70->33|70->33|70->33|77->40|77->40|77->40|82->45|82->45|86->49|86->49|92->55|92->55|92->55|96->59|96->59|96->59|102->65|102->65|102->65|105->68|112->75|112->75|112->75|112->75|112->75|116->79|116->79|116->79|121->84|121->84|121->84|129->92|129->92|129->92|134->97|134->97|134->97|143->106|143->106|143->106|150->113|150->113|150->113|155->118|155->118|155->118|159->122|159->122|159->122|164->127|164->127|164->127|170->133|170->133|170->133|170->133|170->133|174->137|174->137|174->137|179->142|179->142|179->142|184->147|184->147|184->147|189->152|189->152|189->152|194->157|194->157|194->157|198->161|198->161|198->161|202->165|202->165|202->165|207->170|207->170|207->170|213->176|213->176|213->176|223->186|223->186|223->186|231->194|231->194|231->194|231->194|231->194|235->198|235->198|235->198|240->203|240->203|240->203|247->210|247->210|247->210|254->217|254->217|254->217|254->217|254->217|258->221|258->221|258->221|262->225|262->225|262->225|266->229|266->229|266->229|270->233|270->233|270->233|276->239|276->239|276->239|276->239|276->239|280->243|280->243|280->243|285->248|285->248|285->248|289->252|289->252|289->252|296->259|296->259|296->259|296->259|296->259|300->263|300->263|300->263|307->270|307->270|307->270|307->270|307->270|311->274|311->274|311->274|319->282|319->282|319->282|319->282|319->282|323->286|323->286|323->286|328->291|328->291|328->291|332->295|332->295|332->295|340->303|340->303|340->303|340->303|340->303|344->307|344->307|344->307|348->311|348->311|348->311|352->315|352->315|352->315|356->319|356->319|356->319|360->323|360->323|360->323|364->327|364->327|364->327|368->331|368->331|368->331|374->337|374->337|374->337|374->337|374->337|380->343|380->343|380->343|385->348|385->348|386->349|386->349|386->349|386->349|387->350|387->350|387->350|388->351|389->352|392->355|392->355|392->355|393->356|393->356|393->356|394->357|394->357|394->357|395->358|395->358|395->358|397->360|397->360|398->361|398->361|398->361|399->362|400->363|400->363|401->364|401->364|401->364|402->365|403->366|403->366|404->367|404->367|405->368|406->369|406->369|407->370|407->370|407->370|408->371|409->372|409->372|410->373|410->373|411->374|412->375|412->375|413->376|413->376|413->376|414->377|415->378|415->378|416->379|416->379|417->380|418->381|418->381|419->382|419->382|419->382|420->383|421->384|421->384|425->388|425->388|426->389|427->390|427->390|430->393|430->393|431->394|432->395|432->395|437->400|437->400|438->401|439->402|439->402|441->404|441->404|442->405|443->406|443->406|445->408|445->408|446->409|447->410|447->410|449->412|449->412|450->413|451->414|451->414|453->416|453->416|454->417|455->418|455->418|457->420|457->420|458->421|459->422|459->422|461->424|461->424|462->425|463->426|463->426|465->428|465->428|466->429|467->430|467->430|469->432|469->432|470->433|470->433|470->433|471->434|472->435|472->435|473->436|476->439|476->439|477->440|477->440|477->440|477->440|477->440|478->441|479->442|479->442|480->443|480->443|480->443|481->444|482->445|482->445|483->446|485->448|485->448|486->449|486->449|487->450|487->450|488->451|488->451
                  -- GENERATED --
              */
          