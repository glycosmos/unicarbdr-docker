
package views.html.dataUpload.sheets

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object formtablefragment_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

     object formtablefragment_Scope1 {
import src.TableFragment
import service.{FormSample, FormMS}
import helper._
import collection.JavaConversions._

class formtablefragment extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template2[Form[_$1] forSome { 
   type _$1 >: _root_.scala.Nothing <: _root_.scala.Any
},TableFragment,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*5.2*/(form: Form[_],tableFragment: TableFragment):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {
import java.math.BigInteger; var counter=1

Seq[Any](format.raw/*5.46*/("""
"""),format.raw/*7.1*/("""




        """),format.raw/*12.9*/("""<div class="separator"></div>
        <div class="table-fragment-"""),_display_(/*13.37*/tableFragment/*13.50*/.getName()),format.raw/*13.60*/("""-container" style="float: left">
            <div class="table-fragment-title-container">
            """),_display_(/*15.14*/for((f, i) <- tableFragment.getFields().zipWithIndex) yield /*15.67*/ {_display_(Seq[Any](format.raw/*15.69*/("""
                """),format.raw/*16.17*/("""<div class="table-fragment-title table-fragment-"""),_display_(/*16.66*/tableFragment/*16.79*/.getName()),format.raw/*16.89*/("""-column"""),_display_(/*16.97*/i),format.raw/*16.98*/("""">
                """),_display_(/*17.18*/f/*17.19*/.getTitle()),format.raw/*17.30*/("""
                """),format.raw/*18.17*/("""</div>
            """)))}),format.raw/*19.14*/("""
            """),format.raw/*20.13*/("""</div>
            <div class="separator"></div>
            <div class="table-fragment-"""),_display_(/*22.41*/tableFragment/*22.54*/.getName()),format.raw/*22.64*/("""-input-container">
            """),_display_(/*23.14*/for((f, i) <- tableFragment.getFields().zipWithIndex) yield /*23.67*/ {_display_(Seq[Any](format.raw/*23.69*/("""
                """),_display_(/*24.18*/if(f.getType() == 0)/*24.38*/ {_display_(Seq[Any](format.raw/*24.40*/("""
                    """),_display_(/*25.22*/inputText(form(f.getName() + "[0]"), '_value -> (f.getName() + "[0]"), 'id -> (f.getName() + "[0]"), '_label -> null, '_class -> ("table-fragment-input table-fragment-" + tableFragment.getName() + "-column" + i))),format.raw/*25.234*/("""

                """)))}),format.raw/*27.18*/("""
                """),_display_(/*28.18*/if(f.getType() == 1)/*28.38*/ {_display_(Seq[Any](format.raw/*28.40*/("""
                    """),_display_(/*29.22*/select(form(f.getName() + "[0]"), options(f.getList()), '_label -> null, '_class -> ("table-fragment-input table-fragment-" + tableFragment.getName() + "-column" + i))),format.raw/*29.189*/("""
                """)))}),format.raw/*30.18*/("""
            """)))}),format.raw/*31.14*/("""
            """),format.raw/*32.13*/("""</div>
            <div class="table-fragment-"""),_display_(/*33.41*/tableFragment/*33.54*/.getName()),format.raw/*33.64*/("""-add table-fragment-add btn-primary">+</div>
        </div>
        <div class="separator"></div>

<style>
    .separator"""),format.raw/*38.15*/("""{"""),format.raw/*38.16*/("""
        """),format.raw/*39.9*/("""clear: both;
    """),format.raw/*40.5*/("""}"""),format.raw/*40.6*/("""
    """),format.raw/*41.5*/(""".table-fragment-container"""),format.raw/*41.30*/("""{"""),format.raw/*41.31*/("""
        """),format.raw/*42.9*/("""margin-left: 20px;
        width: 1000px;
        clear: both;
    """),format.raw/*45.5*/("""}"""),format.raw/*45.6*/("""
    """),format.raw/*46.5*/(""".table-fragment-title-container"""),format.raw/*46.36*/("""{"""),format.raw/*46.37*/("""
        """),format.raw/*47.9*/("""float: left;
    """),format.raw/*48.5*/("""}"""),format.raw/*48.6*/("""
    """),format.raw/*49.5*/(""".table-fragment-input-container"""),format.raw/*49.36*/("""{"""),format.raw/*49.37*/("""
        """),format.raw/*50.9*/("""float: left;
    """),format.raw/*51.5*/("""}"""),format.raw/*51.6*/("""
    """),format.raw/*52.5*/(""".table-fragment-title"""),format.raw/*52.26*/("""{"""),format.raw/*52.27*/("""
        """),format.raw/*53.9*/("""box-sizing: border-box;
        float: left;
        padding: 2px;
        padding-top: 20px;
        padding-bottom: 0px;
        width: 200px;
    """),format.raw/*59.5*/("""}"""),format.raw/*59.6*/("""
    """),format.raw/*60.5*/("""dd"""),format.raw/*60.7*/("""{"""),format.raw/*60.8*/("""
        """),format.raw/*61.9*/("""margin-left: 0px;
        -webkit-margin-start: 0px;
    """),format.raw/*63.5*/("""}"""),format.raw/*63.6*/("""
    """),format.raw/*64.5*/("""dt label"""),format.raw/*64.13*/("""{"""),format.raw/*64.14*/("""
        """),format.raw/*65.9*/("""visibility: hidden;
    """),format.raw/*66.5*/("""}"""),format.raw/*66.6*/("""
    """),format.raw/*67.5*/("""d1"""),format.raw/*67.7*/("""{"""),format.raw/*67.8*/("""
        """),format.raw/*68.9*/("""margin: 0px;
        -webkit-margin-before: 0em;
        -webkit-margin-after: 0em;
    """),format.raw/*71.5*/("""}"""),format.raw/*71.6*/("""
    """),format.raw/*72.5*/(""".table-fragment-input"""),format.raw/*72.26*/("""{"""),format.raw/*72.27*/("""
        """),format.raw/*73.9*/("""box-sizing: border-box;
        float: left;
        padding: 0px;
        margin: 0px;
    """),format.raw/*77.5*/("""}"""),format.raw/*77.6*/("""
    """),format.raw/*78.5*/(""".table-fragment-input input"""),format.raw/*78.32*/("""{"""),format.raw/*78.33*/("""
        """),format.raw/*79.9*/("""box-sizing: border-box;
        float: left;
        margin: 0px;
        border: solid 1px #DDDDDD;
        height: 30px;
        text-indent: 0px;
        border-radius: 0px;
    """),format.raw/*86.5*/("""}"""),format.raw/*86.6*/("""
    """),format.raw/*87.5*/(""".table-fragment-input select"""),format.raw/*87.33*/("""{"""),format.raw/*87.34*/("""
        """),format.raw/*88.9*/("""box-sizing: border-box;
        float: left;
        padding: 0px;
        margin: 0px;
        border: solid 1px #DDDDDD;
        height: 30px;
        text-indent: 0px;
        border-radius: 0px;
    """),format.raw/*96.5*/("""}"""),format.raw/*96.6*/("""

    """),format.raw/*98.5*/(""".table-fragment-add"""),format.raw/*98.24*/("""{"""),format.raw/*98.25*/("""
        """),format.raw/*99.9*/("""box-sizing: border-box;
        float: left;
        height: 30px;
        align-self: flex-end;
        padding: 4px;
        padding-left: 6px;
        padding-right: 6px;
        font-size: large;
        border-radius: 0px 6px 6px 0px;
        text-align: center;
        cursor: pointer;
        margin-top: 6px;
    """),format.raw/*111.5*/("""}"""),format.raw/*111.6*/("""


"""),format.raw/*114.1*/("""</style>

<script>
    var """),_display_(/*117.10*/tableFragment/*117.23*/.getName()),format.raw/*117.33*/("""lists = """),format.raw/*117.41*/("""{"""),format.raw/*117.42*/("""rows: 0, list: []"""),format.raw/*117.59*/("""}"""),format.raw/*117.60*/(""";
    var """),_display_(/*118.10*/tableFragment/*118.23*/.getName()),format.raw/*118.33*/("""tableObj = [];
    """),_display_(/*119.6*/for((f, i) <- tableFragment.getFields().zipWithIndex) yield /*119.59*/ {_display_(Seq[Any](format.raw/*119.61*/("""
        """),format.raw/*120.9*/("""var obj = """),format.raw/*120.19*/("""{"""),format.raw/*120.20*/("""}"""),format.raw/*120.21*/(""";

        obj.index = 0;
        obj.name = '"""),_display_(/*123.22*/f/*123.23*/.getName()),format.raw/*123.33*/("""';
        obj.type = Number('"""),_display_(/*124.29*/f/*124.30*/.getType()),format.raw/*124.40*/("""');
        obj.width = '"""),_display_(/*125.23*/f/*125.24*/.getWidth()),format.raw/*125.35*/("""';
        """),_display_(/*126.10*/if(f.getType() == 1)/*126.30*/{_display_(Seq[Any](format.raw/*126.31*/("""
            """),format.raw/*127.13*/("""obj.list = [];
            """),_display_(/*128.14*/for(l <- f.getList()) yield /*128.35*/{_display_(Seq[Any](format.raw/*128.36*/("""
                """),format.raw/*129.17*/("""var s = '"""),_display_(/*129.27*/l),format.raw/*129.28*/("""';
                obj.list.push(s);
            """)))}),format.raw/*131.14*/("""
        """)))}),format.raw/*132.10*/("""
        """),_display_(/*133.10*/if(f.getInfo != null)/*133.31*/{_display_(Seq[Any](format.raw/*133.32*/("""
        """),format.raw/*134.9*/("""$('.table-fragment-' + '"""),_display_(/*134.34*/tableFragment/*134.47*/.getName()),format.raw/*134.57*/("""' + '-column' + '"""),_display_(/*134.75*/i),format.raw/*134.76*/("""').prop('title', '"""),_display_(/*134.95*/f/*134.96*/.getInfo()),format.raw/*134.106*/("""');
        obj.info = '"""),_display_(/*135.22*/f/*135.23*/.getInfo()),format.raw/*135.33*/("""';
        """)))}),format.raw/*136.10*/("""

        """),_display_(/*138.10*/tableFragment/*138.23*/.getName()),format.raw/*138.33*/("""tableObj.push(obj);

            """),_display_(/*140.14*/for(d <- 1 to 10) yield /*140.31*/{_display_(Seq[Any](format.raw/*140.32*/("""
                """),format.raw/*141.17*/("""var listObj = """),format.raw/*141.31*/("""{"""),format.raw/*141.32*/("""}"""),format.raw/*141.33*/(""";
                listObj.name = '"""),_display_(/*142.34*/tableFragment/*142.47*/.getName()),format.raw/*142.57*/("""';
                listObj.list = [];

                """),_display_(/*145.18*/if(!form(f.getName() + "[" + d + "]").value.toString().equals(""))/*145.84*/{_display_(Seq[Any](format.raw/*145.85*/("""
                    """),_display_(/*146.22*/print("--> " + form(f.getName() + "[" + d + "]").value)),format.raw/*146.77*/("""
                    """),_display_(/*147.22*/tableFragment/*147.35*/.getName()),format.raw/*147.45*/("""lists.list.push("""),format.raw/*147.61*/("""{"""),format.raw/*147.62*/("""id: '"""),_display_(/*147.68*/(f.getName() +  d)),format.raw/*147.86*/("""', value: '"""),_display_(/*147.98*/(form(f.getName() + "[" + d + "]").value)),format.raw/*147.139*/("""'"""),format.raw/*147.140*/("""}"""),format.raw/*147.141*/(""");
                    """),_display_(/*148.22*/tableFragment/*148.35*/.getName()),format.raw/*148.45*/("""lists.rows = """),_display_(/*148.59*/tableFragment/*148.72*/.getName()),format.raw/*148.82*/("""lists.rows < Number('"""),_display_(/*148.104*/d),format.raw/*148.105*/("""') ?  Number('"""),_display_(/*148.120*/d),format.raw/*148.121*/("""') : """),_display_(/*148.127*/tableFragment/*148.140*/.getName()),format.raw/*148.150*/("""lists.rows;

                """)))}),format.raw/*150.18*/("""
            """)))}),format.raw/*151.14*/("""
    """)))}),format.raw/*152.6*/("""

    """),format.raw/*154.5*/("""$(document).ready(function()"""),format.raw/*154.33*/("""{"""),format.raw/*154.34*/("""
        """),format.raw/*155.9*/("""setColumns("""),_display_(/*155.21*/tableFragment/*155.34*/.getName()),format.raw/*155.44*/("""tableObj, '"""),_display_(/*155.56*/tableFragment/*155.69*/.getName()),format.raw/*155.79*/("""');

        for(var t = 0; t < """),_display_(/*157.29*/tableFragment/*157.42*/.getName()),format.raw/*157.52*/("""lists.rows; t++)"""),format.raw/*157.68*/("""{"""),format.raw/*157.69*/("""
            """),_display_(/*158.14*/tableFragment/*158.27*/.getName()),format.raw/*158.37*/("""Add();
        """),format.raw/*159.9*/("""}"""),format.raw/*159.10*/("""
        """),format.raw/*160.9*/("""for(var t in """),_display_(/*160.23*/tableFragment/*160.36*/.getName()),format.raw/*160.46*/("""lists.list)"""),format.raw/*160.57*/("""{"""),format.raw/*160.58*/("""
            """),format.raw/*161.13*/("""$("#" + """),_display_(/*161.22*/tableFragment/*161.35*/.getName()),format.raw/*161.45*/("""lists.list[t].id).val("""),_display_(/*161.68*/tableFragment/*161.81*/.getName()),format.raw/*161.91*/("""lists.list[t].value);
        """),format.raw/*162.9*/("""}"""),format.raw/*162.10*/("""

        """),format.raw/*164.9*/("""$(".table-fragment-"""),_display_(/*164.29*/tableFragment/*164.42*/.getName()),format.raw/*164.52*/("""-add").click(function() """),format.raw/*164.76*/("""{"""),format.raw/*164.77*/("""
            """),_display_(/*165.14*/tableFragment/*165.27*/.getName()),format.raw/*165.37*/("""Add();
            $("#softwareTwoVersion0").val("hello");
        """),format.raw/*167.9*/("""}"""),format.raw/*167.10*/(""");
    """),format.raw/*168.5*/("""}"""),format.raw/*168.6*/(""");

    var """),_display_(/*170.10*/tableFragment/*170.23*/.getName()),format.raw/*170.33*/("""Add = function () """),format.raw/*170.51*/("""{"""),format.raw/*170.52*/("""
        """),format.raw/*171.9*/("""var tableObj = """),_display_(/*171.25*/tableFragment/*171.38*/.getName()),format.raw/*171.48*/("""tableObj;
        var tableName = '"""),_display_(/*172.27*/tableFragment/*172.40*/.getName()),format.raw/*172.50*/("""';
        $(".table-fragment-"""),_display_(/*173.29*/tableFragment/*173.42*/.getName()),format.raw/*173.52*/("""-input-container").append(createElement(tableObj, tableName));
        setColumns(tableObj, tableName);
        $(".table-fragment-"""),_display_(/*175.29*/tableFragment/*175.42*/.getName()),format.raw/*175.52*/("""-add").css("margin-top",0);
    """),format.raw/*176.5*/("""}"""),format.raw/*176.6*/("""

    """),format.raw/*178.5*/("""function createElement(obj, tableName)"""),format.raw/*178.43*/("""{"""),format.raw/*178.44*/("""
        """),format.raw/*179.9*/("""var element = "";
        for(var i = 0; i < obj.length; i++)"""),format.raw/*180.44*/("""{"""),format.raw/*180.45*/("""
            """),format.raw/*181.13*/("""obj[i].column = i;
            element += (obj[i].type == 0) ? createInputText(obj[i], tableName) : createInputSelect(obj[i], tableName);
        """),format.raw/*183.9*/("""}"""),format.raw/*183.10*/("""
        """),format.raw/*184.9*/("""return element;
    """),format.raw/*185.5*/("""}"""),format.raw/*185.6*/("""

    """),format.raw/*187.5*/("""function createInputSelect(obj, tableName) """),format.raw/*187.48*/("""{"""),format.raw/*187.49*/("""
        """),format.raw/*188.9*/("""obj.index++;
        var dlElement = '<dl class="table-fragment-input table-fragment-' + tableName + '-column' + obj.column + '" id="' + obj.name + '_' + obj.index + '_field">';
        var ddElement = '<dd><select name="' + obj.name + '[' + obj.index + ']"  id="' + obj.name +  obj.index + '">';
        var optionList = "";
        for(var o in obj.list)"""),format.raw/*192.31*/("""{"""),format.raw/*192.32*/("""
            """),format.raw/*193.13*/("""optionList += '<option value="' + obj.list[o] + '">' + obj.list[o] + '</option>';
        """),format.raw/*194.9*/("""}"""),format.raw/*194.10*/("""
        """),format.raw/*195.9*/("""return dlElement + ddElement + optionList + '</select></dd></dl>';
    """),format.raw/*196.5*/("""}"""),format.raw/*196.6*/("""

    """),format.raw/*198.5*/("""function createInputText(obj, tableName)"""),format.raw/*198.45*/("""{"""),format.raw/*198.46*/("""
        """),format.raw/*199.9*/("""var dlElement = '<dl class="table-fragment-input table-fragment-' + tableName + '-column' + obj.column + '" id="' + obj.name + '_' + (++obj.index) + '_field">';
        var ddElement = '<dd><input type="text" name="' + obj.name + '[' + obj.index + ']"  id="' + obj.name + obj.index + '"></dd></dl>';

        return dlElement + ddElement;
    """),format.raw/*203.5*/("""}"""),format.raw/*203.6*/("""

    """),format.raw/*205.5*/("""function setColumns(tableObj, tableName) """),format.raw/*205.46*/("""{"""),format.raw/*205.47*/("""
        """),format.raw/*206.9*/("""var containerWidth = 34;
        for(var o in tableObj)"""),format.raw/*207.31*/("""{"""),format.raw/*207.32*/("""
            """),format.raw/*208.13*/("""containerWidth += Number(tableObj[o].width);
            $(".table-fragment-" + tableName + "-column" + o).css("""),format.raw/*209.67*/("""{"""),format.raw/*209.68*/(""""width": tableObj[o].width"""),format.raw/*209.94*/("""}"""),format.raw/*209.95*/(""");
            $(".table-fragment-" + tableName + "-column" + o +" input").css("width",tableObj[o].width);
            $(".table-fragment-" + tableName + "-column" + o +" select").css("width",tableObj[o].width);
        """),format.raw/*212.9*/("""}"""),format.raw/*212.10*/("""
        """),format.raw/*213.9*/("""$(".table-fragment-"+tableName+"-container").width(containerWidth);
    """),format.raw/*214.5*/("""}"""),format.raw/*214.6*/("""

"""),format.raw/*216.1*/("""</script>
"""))
      }
    }
  }

  def render(form:Form[_$1] forSome { 
   type _$1 >: _root_.scala.Nothing <: _root_.scala.Any
},tableFragment:TableFragment): play.twirl.api.HtmlFormat.Appendable = apply(form,tableFragment)

  def f:((Form[_$1] forSome { 
   type _$1 >: _root_.scala.Nothing <: _root_.scala.Any
},TableFragment) => play.twirl.api.HtmlFormat.Appendable) = (form,tableFragment) => apply(form,tableFragment)

  def ref: this.type = this

}


}
}

/**/
object formtablefragment extends formtablefragment_Scope0.formtablefragment_Scope1.formtablefragment
              /*
                  -- GENERATED --
                  DATE: Wed Jun 19 09:50:52 UTC 2019
                  SOURCE: /app/app/views/dataUpload/sheets/formtablefragment.scala.html
                  HASH: 98439cc1121558be0f258fb82444e904607bbd20
                  MATRIX: 1026->118|1207->162|1234->207|1274->220|1367->286|1389->299|1420->309|1550->412|1619->465|1659->467|1704->484|1780->533|1802->546|1833->556|1868->564|1890->565|1937->585|1947->586|1979->597|2024->614|2075->634|2116->647|2232->736|2254->749|2285->759|2344->791|2413->844|2453->846|2498->864|2527->884|2567->886|2616->908|2850->1120|2900->1139|2945->1157|2974->1177|3014->1179|3063->1201|3252->1368|3301->1386|3346->1400|3387->1413|3461->1460|3483->1473|3514->1483|3663->1604|3692->1605|3728->1614|3772->1631|3800->1632|3832->1637|3885->1662|3914->1663|3950->1672|4044->1739|4072->1740|4104->1745|4163->1776|4192->1777|4228->1786|4272->1803|4300->1804|4332->1809|4391->1840|4420->1841|4456->1850|4500->1867|4528->1868|4560->1873|4609->1894|4638->1895|4674->1904|4850->2053|4878->2054|4910->2059|4939->2061|4967->2062|5003->2071|5087->2128|5115->2129|5147->2134|5183->2142|5212->2143|5248->2152|5299->2176|5327->2177|5359->2182|5388->2184|5416->2185|5452->2194|5567->2282|5595->2283|5627->2288|5676->2309|5705->2310|5741->2319|5860->2411|5888->2412|5920->2417|5975->2444|6004->2445|6040->2454|6248->2635|6276->2636|6308->2641|6364->2669|6393->2670|6429->2679|6659->2882|6687->2883|6720->2889|6767->2908|6796->2909|6832->2918|7182->3240|7211->3241|7242->3244|7298->3272|7321->3285|7353->3295|7390->3303|7420->3304|7466->3321|7496->3322|7535->3333|7558->3346|7590->3356|7637->3376|7707->3429|7748->3431|7785->3440|7824->3450|7854->3451|7884->3452|7959->3499|7970->3500|8002->3510|8061->3541|8072->3542|8104->3552|8158->3578|8169->3579|8202->3590|8242->3602|8272->3622|8312->3623|8354->3636|8410->3664|8448->3685|8488->3686|8534->3703|8572->3713|8595->3714|8677->3764|8719->3774|8757->3784|8788->3805|8828->3806|8865->3815|8918->3840|8941->3853|8973->3863|9019->3881|9042->3882|9089->3901|9100->3902|9133->3912|9186->3937|9197->3938|9229->3948|9273->3960|9312->3971|9335->3984|9367->3994|9429->4028|9463->4045|9503->4046|9549->4063|9592->4077|9622->4078|9652->4079|9715->4114|9738->4127|9770->4137|9854->4193|9930->4259|9970->4260|10020->4282|10097->4337|10147->4359|10170->4372|10202->4382|10247->4398|10277->4399|10311->4405|10351->4423|10391->4435|10455->4476|10486->4477|10517->4478|10569->4502|10592->4515|10624->4525|10666->4539|10689->4552|10721->4562|10772->4584|10796->4585|10840->4600|10864->4601|10899->4607|10923->4620|10956->4630|11018->4660|11064->4674|11101->4680|11135->4686|11192->4714|11222->4715|11259->4724|11299->4736|11322->4749|11354->4759|11394->4771|11417->4784|11449->4794|11510->4827|11533->4840|11565->4850|11610->4866|11640->4867|11682->4881|11705->4894|11737->4904|11780->4919|11810->4920|11847->4929|11889->4943|11912->4956|11944->4966|11984->4977|12014->4978|12056->4991|12093->5000|12116->5013|12148->5023|12199->5046|12222->5059|12254->5069|12312->5099|12342->5100|12380->5110|12428->5130|12451->5143|12483->5153|12536->5177|12566->5178|12608->5192|12631->5205|12663->5215|12758->5282|12788->5283|12823->5290|12852->5291|12893->5304|12916->5317|12948->5327|12995->5345|13025->5346|13062->5355|13106->5371|13129->5384|13161->5394|13225->5430|13248->5443|13280->5453|13339->5484|13362->5497|13394->5507|13554->5639|13577->5652|13609->5662|13669->5694|13698->5695|13732->5701|13799->5739|13829->5740|13866->5749|13956->5810|13986->5811|14028->5824|14202->5970|14232->5971|14269->5980|14317->6000|14346->6001|14380->6007|14452->6050|14482->6051|14519->6060|14904->6416|14934->6417|14976->6430|15094->6520|15124->6521|15161->6530|15260->6601|15289->6602|15323->6608|15392->6648|15422->6649|15459->6658|15830->7001|15859->7002|15893->7008|15963->7049|15993->7050|16030->7059|16114->7114|16144->7115|16186->7128|16326->7239|16356->7240|16411->7266|16441->7267|16689->7487|16719->7488|16756->7497|16856->7569|16885->7570|16915->7572
                  LINES: 35->5|40->5|41->7|46->12|47->13|47->13|47->13|49->15|49->15|49->15|50->16|50->16|50->16|50->16|50->16|50->16|51->17|51->17|51->17|52->18|53->19|54->20|56->22|56->22|56->22|57->23|57->23|57->23|58->24|58->24|58->24|59->25|59->25|61->27|62->28|62->28|62->28|63->29|63->29|64->30|65->31|66->32|67->33|67->33|67->33|72->38|72->38|73->39|74->40|74->40|75->41|75->41|75->41|76->42|79->45|79->45|80->46|80->46|80->46|81->47|82->48|82->48|83->49|83->49|83->49|84->50|85->51|85->51|86->52|86->52|86->52|87->53|93->59|93->59|94->60|94->60|94->60|95->61|97->63|97->63|98->64|98->64|98->64|99->65|100->66|100->66|101->67|101->67|101->67|102->68|105->71|105->71|106->72|106->72|106->72|107->73|111->77|111->77|112->78|112->78|112->78|113->79|120->86|120->86|121->87|121->87|121->87|122->88|130->96|130->96|132->98|132->98|132->98|133->99|145->111|145->111|148->114|151->117|151->117|151->117|151->117|151->117|151->117|151->117|152->118|152->118|152->118|153->119|153->119|153->119|154->120|154->120|154->120|154->120|157->123|157->123|157->123|158->124|158->124|158->124|159->125|159->125|159->125|160->126|160->126|160->126|161->127|162->128|162->128|162->128|163->129|163->129|163->129|165->131|166->132|167->133|167->133|167->133|168->134|168->134|168->134|168->134|168->134|168->134|168->134|168->134|168->134|169->135|169->135|169->135|170->136|172->138|172->138|172->138|174->140|174->140|174->140|175->141|175->141|175->141|175->141|176->142|176->142|176->142|179->145|179->145|179->145|180->146|180->146|181->147|181->147|181->147|181->147|181->147|181->147|181->147|181->147|181->147|181->147|181->147|182->148|182->148|182->148|182->148|182->148|182->148|182->148|182->148|182->148|182->148|182->148|182->148|182->148|184->150|185->151|186->152|188->154|188->154|188->154|189->155|189->155|189->155|189->155|189->155|189->155|189->155|191->157|191->157|191->157|191->157|191->157|192->158|192->158|192->158|193->159|193->159|194->160|194->160|194->160|194->160|194->160|194->160|195->161|195->161|195->161|195->161|195->161|195->161|195->161|196->162|196->162|198->164|198->164|198->164|198->164|198->164|198->164|199->165|199->165|199->165|201->167|201->167|202->168|202->168|204->170|204->170|204->170|204->170|204->170|205->171|205->171|205->171|205->171|206->172|206->172|206->172|207->173|207->173|207->173|209->175|209->175|209->175|210->176|210->176|212->178|212->178|212->178|213->179|214->180|214->180|215->181|217->183|217->183|218->184|219->185|219->185|221->187|221->187|221->187|222->188|226->192|226->192|227->193|228->194|228->194|229->195|230->196|230->196|232->198|232->198|232->198|233->199|237->203|237->203|239->205|239->205|239->205|240->206|241->207|241->207|242->208|243->209|243->209|243->209|243->209|246->212|246->212|247->213|248->214|248->214|250->216
                  -- GENERATED --
              */
          