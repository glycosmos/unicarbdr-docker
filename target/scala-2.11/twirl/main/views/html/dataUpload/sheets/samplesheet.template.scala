
package views.html.dataUpload.sheets

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object samplesheet_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

     object samplesheet_Scope1 {
import src.TableFragment
import service.{FormSample, FormMS}
import helper._

class samplesheet extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template4[Form[_$1] forSome { 
   type _$1 >: _root_.scala.Nothing <: _root_.scala.Any
},Form[_$2] forSome { 
   type _$2 >: _root_.scala.Nothing <: _root_.scala.Any
},Map[String, Map[String, String]],List[TableFragment],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*4.2*/(form: Form[_], tablesForm: Form[_], optionMaps: Map[String, Map[String,String]], sampleTables: List[TableFragment]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {

implicit def /*6.2*/implicitFieldConstructor/*6.26*/ = {{ b3.vertical.fieldConstructor }};
Seq[Any](format.raw/*4.118*/("""

"""),format.raw/*6.62*/("""


    """),format.raw/*9.5*/("""<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>


<div id="ms-table">

    <h3 class="ms-head">MIRAGE Sample Preparation Guidelines</h3>
    <h5>Guidelines for reporting sample preparation descriptors for glycomics experiments</h5>

    <br /> <br />
    <br /> <br />
    <h4>General features — (a) Global descriptors</h4>
    <br />
    <div class="ms-row" title="The date on which the work described was completed; given in the standard ‘YYYY-MM-DD’ format (with hyphens).">
        <div class="ms-label">
            <p>Date stamp</p>
        </div>
        <div class="ms-input">"""),_display_(/*26.32*/helper/*26.38*/.inputDate(form("s_date"), '_label -> null, '_class -> "search-field row-fluid date-stamp val v-sample")),format.raw/*26.142*/("""</div>
        <span class="red-star">*</span>
    </div>
    <div class="ms-row" title="The (stable) primary contact person for this data set; this could be the experimenter, lab head, line manager etc. Where responsibility rests with an institutional role (e.g., one of a number of duty officers) rather than a person, give the official name of the role rather than any one person.">
        <div class="ms-label">
            <p>Responsible person. Provide name,</p>
        </div>
        <div class="ms-input">"""),_display_(/*33.32*/helper/*33.38*/.inputText(form("s_responsiblePerson"), '_label -> null, '_class -> "search-field row-fluid responsible val v-sample")),format.raw/*33.156*/("""</div>
        <span class="red-star">*</span>
    </div>
    <div class="ms-row">
        <div class="ms-label">
            <p>Affiliation</p>
        </div>
        <div class="ms-input">"""),_display_(/*40.32*/helper/*40.38*/.inputText(form("s_affiliation"), '_label -> null, '_class -> "search-field row-fluid affiliation val v-sample")),format.raw/*40.150*/("""</div>
        <span class="red-star">*</span>
    </div>
    <div class="ms-row">
        <div class="ms-label">
            <p>Stable contact information</p>
        </div>
        <div class="ms-input">"""),_display_(/*47.32*/helper/*47.38*/.inputText(form("s_contactInformation"), '_label -> null, '_class -> "search-field row-fluid contactInformation val v-sample")),format.raw/*47.164*/("""</div>
        <span class="red-star">*</span>
    </div>
    <br /> <br /> <br />
    <h4>1. Sample origin</h4>
    <br />
    <div class="ms-area-label">
        <p>Here “sample” is defined as any carbohydrate, polysaccharide, oligosaccharide or glycoconjugate that originates from any given starting material. The starting material may be a compound, mixture or cell product used to produce the oligosaccharide sample of interest. The source and/or methods used to produce the starting sample material can vary considerably but minimum information that describes its origin is outlined.</p>
    </div>
    <br />
    <div title="Describe how original starting sample material was generated or where it was obtained. Starting material descriptions are further delineated by biologically or chemically derived material.">
    <div class="ms-area-label">
            <p>General information</p>
        </div>
        <div class="ms-input">"""),_display_(/*61.32*/helper/*61.38*/.textarea(form("s_generalInfo"), '_label -> null, '_class -> "search-field val v-sample")),format.raw/*61.127*/("""</div>
        <span class="red-star">*</span>
    </div>
<br />
    <h4 title="Biologically derived material includes recombinantly expressed protein cells, etc. As well as whole organisms or tissues">
        1.1 Biologically derived material
    </h4>
    <br />
    <h5>Biologically derived material - Recombinantly produced material</h5>
    <br />
    <div class="ms-row" title="e.g. CHO, HEK, NS0 etc.">
        <div class="ms-label">
            <p>Cell type</p>
        </div>
        <div class="ms-input">
            <select class="cell-type form-control" style="width:200px" name="cell-type"></select>
        </div>


    </div>
    <br /> <br />
    <div  title="growth/harvest conditions should be specified. Any modifications to cells that influence the characteristics of the starting material (e.g. genetic manipulations) should also be stated.">
        <div class="ms-area-label">
            <p>Growth/harvest conditions. Other modifications.</p>
        </div>
        <div class="ms-input">"""),_display_(/*86.32*/helper/*86.38*/.textarea(form("s_harvestConditions"), '_label -> null, '_class -> "search-field")),format.raw/*86.120*/("""</div>
    </div>

    <br />
    <h5>Biologically derived material - Biological origin of Material</h5>
    <br /><br />
    <div class="ms-row">
        <div class="ms-label"><p>Origin (biological fluids, tissue, etc)</p></div>
        <select class="origin form-control" style="width:200px" name="origin"></select>
    </div>
    <br />
    <div class="ms-row">
        <div class="ms-label"><p>Species</p></div>
        <select class="species form-control" style="width:200px" name="species"></select>
    </div>
    <br /> <br />
    <div>
        <div class="ms-area-label"><p>Describe treatments and/or storage conditions</p></div>
        <div class="ms-input">"""),_display_(/*104.32*/helper/*104.38*/.textarea(form("s_storageConditions"), '_label -> null, '_class -> "search-field")),format.raw/*104.120*/("""</div>
    </div>
    <div class="ms-row">
        <div class="ms-label">
            <p>Glycoprotein (Uniprot ID)</p>
        </div>
        <div class="ms-input">"""),_display_(/*110.32*/helper/*110.38*/.inputText(form("s_glycoprotein"), '_label -> null, '_class -> "search-field")),format.raw/*110.116*/("""</div>
    </div>

    <br /><br /><br />
    <h5>Biologically derived material - Purchased from commercial manufacturer</h5>
    <br /> <br />
    <div>
        <div class="ms-area-label"><p>Vendor and applicable item information</p></div>
        <div class="ms-input">"""),_display_(/*118.32*/helper/*118.38*/.textarea(form("s_vendor"), '_label -> null, '_class -> "search-field")),format.raw/*118.109*/("""</div>
    </div>
    <br />
    <h5>1.2 Chemically derived material</h5>
    <br />
    <br />
    <div title="If samples were synthetically derived, provide information detailing synthesis steps or specify where the equivalent reaction protocol is available.">
        <div class="ms-area-label">
            <p>Synthesis steps or specify where the equivalent reaction protocol is available.</p>
        </div>
        <div class="ms-input">"""),_display_(/*128.32*/helper/*128.38*/.textarea(form("s_SynthesisSteps"), '_label -> null, '_class -> "search-field")),format.raw/*128.117*/("""</div>
    </div>
    <div class="ms-row" title="Define the type of starting material used or produced that contains the oligosaccharide to be used/analysed in subsequent experiments. These may include glycoprotein(s), proteoglycan, glycolipid, GPI-anchored, free-oligosaccharides, sugar-nucleotides or synthetically derived material but are not limited to these definitions">
        <div class="ms-label">
            <p>Description of starting material</p>
        </div>
        <div class="ms-input">"""),_display_(/*134.32*/select(form("s_startingMaterial"), options(optionMaps.get("StartingMaterial")), '_label -> null)),format.raw/*134.128*/("""</div>
    </div>
    <br />
    <br />
    <br />
    <h4>2. Sample Processing</h4>
    <br />
    <h5>2.1 Sample Processing – Isolation</h5>
    <br />
    <br />
    <h6>Enzymatic treatments</h6>
    """),_display_(/*145.6*/views/*145.11*/.html.dataUpload.sheets.formtablefragment(tablesForm, sampleTables(0))),format.raw/*145.81*/("""
    """),format.raw/*146.5*/("""<br />
    <br />
    <h6>Chemical treatments</h6>
    """),_display_(/*149.6*/views/*149.11*/.html.dataUpload.sheets.formtablefragment(tablesForm, sampleTables(1))),format.raw/*149.81*/("""

    """),format.raw/*151.5*/("""<br />
    <br />
    <br />
    <h5 title="Isolated material may be subject to a wide variety of chemical or enzymatic modifications. The type of modification and reaction protocols should be documented. If new protocols are used provide a thorough description.">
        2.2 Sample Processing – Modification
    </h5>
    <br />
    <h6>Enzymatic modifications</h6>
    """),_display_(/*159.6*/views/*159.11*/.html.dataUpload.sheets.formtablefragment(tablesForm, sampleTables(2))),format.raw/*159.81*/("""
    """),format.raw/*160.5*/("""<br />
    <br />
    <h6>Chemical treatments</h6>
    """),_display_(/*163.6*/views/*163.11*/.html.dataUpload.sheets.formtablefragment(tablesForm, sampleTables(3))),format.raw/*163.81*/("""
    """),format.raw/*164.5*/("""<br />
    <br />
    <br />

    <h5 title="The processing steps encompass any type of refining or clean-up strategies used to produce the purified final sample product.">
        2.3 Sample Processing – Purification
    </h5>
    <br />
    <div title="Specify all steps used to purify starting material after isolation/modification steps. Examples of procedures include solid phase extraction (SPE), liquid-liquid extraction or other chromatographic methods. For each method describe the all experimental materials (e.g. stationary phase) and methods (e.g. flow rates, fractionation etc.).">
        <div class="ms-area-label"><p>Purification steps</p></div>
        <div class="ms-input">"""),_display_(/*174.32*/helper/*174.38*/.textarea(form("s_purification"), '_label -> null, '_class -> "search-field row-fluid")),format.raw/*174.125*/("""</div>
    </div>
    <br /> <br />


    <h4>3. Defined sample</h4>
    <div class="ms-row" title="Name or specify the type of sample material to be analysed or used in other experiments. These may include but are not limited to glycoconjugates, glycosaminoglycans, N- or O-glycans, glycopeptides, glycolipids, monosaccharides, poly- and oligosaccharides">
        <div class="ms-label"><p>Sample name</p></div>
        <div class="ms-input">"""),_display_(/*182.32*/select(form("s_sampleName"), options(optionMaps.get("SampleMaterial")), '_label -> null)),format.raw/*182.120*/("""</div>
    </div>
    <br /> <br />


    """),_display_(/*187.6*/inputText(form("s_cellType"),'style -> "display: none", '_label -> null)),format.raw/*187.78*/("""

    """),_display_(/*189.6*/inputText(form("s_origin"),'style -> "display: none", '_label -> null)),format.raw/*189.76*/("""

    """),_display_(/*191.6*/inputText(form("s_species"),'style -> "display: none", '_label -> null)),format.raw/*191.77*/("""

"""),format.raw/*193.1*/("""</div>



<script>
    var material = '"""),_display_(/*198.22*/form("s_startingMaterial")/*198.48*/.value),format.raw/*198.54*/("""';
    var sample = '"""),_display_(/*199.20*/form("s_sampleName")/*199.40*/.value),format.raw/*199.46*/("""';
    var fCell = '"""),_display_(/*200.19*/form("s_cellType")/*200.37*/.value),format.raw/*200.43*/("""';
    var fOrigin = '"""),_display_(/*201.21*/form("s_origin")/*201.37*/.value),format.raw/*201.43*/("""';
    var fSpecies = '"""),_display_(/*202.22*/form("s_species")/*202.39*/.value),format.raw/*202.45*/("""';

        $(document).ready(function()"""),format.raw/*204.37*/("""{"""),format.raw/*204.38*/("""

            """),format.raw/*206.13*/("""//$("#s_vendor").val("NA");
            //$("#s_vendor").attr('disabled', 'disabled');
            //$("#s_startingMaterial").attr('disabled', 'disabled');
            //$(".species").attr('disabled', 'disabled');

            // Switch border color
            if(material != "" && $("#s_startingMaterial").val() != material)"""),format.raw/*212.77*/("""{"""),format.raw/*212.78*/("""
                """),format.raw/*213.17*/("""$("#s_startingMaterial").css("border", "solid 1px #ffbb44");
            """),format.raw/*214.13*/("""}"""),format.raw/*214.14*/("""
            """),format.raw/*215.13*/("""$("#s_startingMaterial").change(function () """),format.raw/*215.57*/("""{"""),format.raw/*215.58*/("""
                """),format.raw/*216.17*/("""if($("#s_startingMaterial").val() != " ")"""),format.raw/*216.58*/("""{"""),format.raw/*216.59*/("""
                    """),format.raw/*217.21*/("""$("#s_startingMaterial").css("border", "");
                """),format.raw/*218.17*/("""}"""),format.raw/*218.18*/("""
            """),format.raw/*219.13*/("""}"""),format.raw/*219.14*/(""");
            if(sample != "" && $("#s_sampleName").val() != sample)"""),format.raw/*220.67*/("""{"""),format.raw/*220.68*/("""
                """),format.raw/*221.17*/("""$("#s_sampleName").css("border", "solid 1px #ffbb44");
            """),format.raw/*222.13*/("""}"""),format.raw/*222.14*/("""
            """),format.raw/*223.13*/("""$("#s_sampleName").change(function () """),format.raw/*223.51*/("""{"""),format.raw/*223.52*/("""
                """),format.raw/*224.17*/("""if($("#s_sampleName").val() != " ")"""),format.raw/*224.52*/("""{"""),format.raw/*224.53*/("""
                    """),format.raw/*225.21*/("""$("#s_sampleName").css("border", "");
                """),format.raw/*226.17*/("""}"""),format.raw/*226.18*/("""
            """),format.raw/*227.13*/("""}"""),format.raw/*227.14*/(""");

            $('.cell-type').select2("""),format.raw/*229.37*/("""{"""),format.raw/*229.38*/("""
                """),format.raw/*230.17*/("""placeholder: '',
                ajax: """),format.raw/*231.23*/("""{"""),format.raw/*231.24*/("""
                    """),format.raw/*232.21*/("""url: '/api/cell/json/',
                    dataType: 'json',
                    delay: 250,
                    processResults: function (data) """),format.raw/*235.53*/("""{"""),format.raw/*235.54*/("""
                        """),format.raw/*236.25*/("""return """),format.raw/*236.32*/("""{"""),format.raw/*236.33*/("""
                            """),format.raw/*237.29*/("""results: data
                        """),format.raw/*238.25*/("""}"""),format.raw/*238.26*/(""";
                    """),format.raw/*239.21*/("""}"""),format.raw/*239.22*/(""",
                    cache: true
                """),format.raw/*241.17*/("""}"""),format.raw/*241.18*/(""",
                minimumInputLength: 2
            """),format.raw/*243.13*/("""}"""),format.raw/*243.14*/(""");

            $('.species').select2("""),format.raw/*245.35*/("""{"""),format.raw/*245.36*/("""
                """),format.raw/*246.17*/("""placeholder: '',
                ajax: """),format.raw/*247.23*/("""{"""),format.raw/*247.24*/("""
                    """),format.raw/*248.21*/("""url: '/api/species/json/',
                    dataType: 'json',
                    delay: 250,
                    processResults: function (data) """),format.raw/*251.53*/("""{"""),format.raw/*251.54*/("""
                        """),format.raw/*252.25*/("""return """),format.raw/*252.32*/("""{"""),format.raw/*252.33*/("""
                            """),format.raw/*253.29*/("""results: data
                        """),format.raw/*254.25*/("""}"""),format.raw/*254.26*/(""";
                    """),format.raw/*255.21*/("""}"""),format.raw/*255.22*/(""",
                    cache: true
                """),format.raw/*257.17*/("""}"""),format.raw/*257.18*/(""",
                minimumInputLength: 2
            """),format.raw/*259.13*/("""}"""),format.raw/*259.14*/(""");

            $('.origin').select2("""),format.raw/*261.34*/("""{"""),format.raw/*261.35*/("""
                """),format.raw/*262.17*/("""placeholder: '',
                ajax: """),format.raw/*263.23*/("""{"""),format.raw/*263.24*/("""
                    """),format.raw/*264.21*/("""url: '/api/origin/json/',
                    dataType: 'json',
                    delay: 250,
                    processResults: function (data) """),format.raw/*267.53*/("""{"""),format.raw/*267.54*/("""
                        """),format.raw/*268.25*/("""return """),format.raw/*268.32*/("""{"""),format.raw/*268.33*/("""
                            """),format.raw/*269.29*/("""results: data
                        """),format.raw/*270.25*/("""}"""),format.raw/*270.26*/(""";
                    """),format.raw/*271.21*/("""}"""),format.raw/*271.22*/(""",
                    cache: true
                """),format.raw/*273.17*/("""}"""),format.raw/*273.18*/(""",
                minimumInputLength: 2
            """),format.raw/*275.13*/("""}"""),format.raw/*275.14*/(""");

            $(".cell-type").change(function () """),format.raw/*277.48*/("""{"""),format.raw/*277.49*/("""
                """),format.raw/*278.17*/("""$(".cell-type").next().children(".selection").children(".select2-selection").css("border","");
                $("#s_cellType").val($(".cell-type").val());
            """),format.raw/*280.13*/("""}"""),format.raw/*280.14*/(""");

            $(".origin").change(function () """),format.raw/*282.45*/("""{"""),format.raw/*282.46*/("""
                """),format.raw/*283.17*/("""$(".origin").next().children(".selection").children(".select2-selection").css("border","");
                $("#s_origin").val($(".origin").val());
            """),format.raw/*285.13*/("""}"""),format.raw/*285.14*/(""");

            $(".species").change(function () """),format.raw/*287.46*/("""{"""),format.raw/*287.47*/("""
                """),format.raw/*288.17*/("""$(".species").next().children(".selection").children(".select2-selection").css("border","");
                $("#s_species").val($(".species").val());
            """),format.raw/*290.13*/("""}"""),format.raw/*290.14*/(""");


            // Check the data against the db
             $.getJSON( '/api/cell/json/?q=' + $("#s_cellType").val(), function( data ) """),format.raw/*294.89*/("""{"""),format.raw/*294.90*/("""
                 """),format.raw/*295.18*/("""var items = [];
                 $.each( data, function( key, val ) """),format.raw/*296.53*/("""{"""),format.raw/*296.54*/("""
                     """),format.raw/*297.22*/("""items.push(val);
                 """),format.raw/*298.18*/("""}"""),format.raw/*298.19*/(""");
                 if(items.length == 0)"""),format.raw/*299.39*/("""{"""),format.raw/*299.40*/("""
                     """),format.raw/*300.22*/("""$(".cell-type").select2("trigger", "select", """),format.raw/*300.67*/("""{"""),format.raw/*300.68*/("""
                         """),format.raw/*301.26*/("""data: """),format.raw/*301.32*/("""{"""),format.raw/*301.33*/("""id: "", "text": """""),format.raw/*301.51*/("""}"""),format.raw/*301.52*/("""
                     """),format.raw/*302.22*/("""}"""),format.raw/*302.23*/(""");
                     $(".cell-type").next().children(".selection").children(".select2-selection").css("border","solid 1px #ffbb44");
                 """),format.raw/*304.18*/("""}"""),format.raw/*304.19*/("""else """),format.raw/*304.24*/("""{"""),format.raw/*304.25*/("""
                     """),format.raw/*305.22*/("""$(".cell-type").select2("trigger", "select", """),format.raw/*305.67*/("""{"""),format.raw/*305.68*/("""
                         """),format.raw/*306.26*/("""data: """),format.raw/*306.32*/("""{"""),format.raw/*306.33*/("""id: $("#s_cellType").val(), "text": $("#s_cellType").val()"""),format.raw/*306.91*/("""}"""),format.raw/*306.92*/("""
                     """),format.raw/*307.22*/("""}"""),format.raw/*307.23*/(""");
                 """),format.raw/*308.18*/("""}"""),format.raw/*308.19*/("""
             """),format.raw/*309.14*/("""}"""),format.raw/*309.15*/(""");

             $.getJSON( '/api/origin/json/?q=' + $("#s_origin").val(), function( data ) """),format.raw/*311.89*/("""{"""),format.raw/*311.90*/("""
                   """),format.raw/*312.20*/("""var items = [];
                   $.each( data, function( key, val ) """),format.raw/*313.55*/("""{"""),format.raw/*313.56*/("""
                       """),format.raw/*314.24*/("""items.push(val);
                   """),format.raw/*315.20*/("""}"""),format.raw/*315.21*/(""");
                   if(items.length == 0)"""),format.raw/*316.41*/("""{"""),format.raw/*316.42*/("""
                       """),format.raw/*317.24*/("""$(".origin").select2("trigger", "select", """),format.raw/*317.66*/("""{"""),format.raw/*317.67*/("""
                           """),format.raw/*318.28*/("""data: """),format.raw/*318.34*/("""{"""),format.raw/*318.35*/("""id: "", "text": """""),format.raw/*318.53*/("""}"""),format.raw/*318.54*/("""
                       """),format.raw/*319.24*/("""}"""),format.raw/*319.25*/(""");
                       $(".origin").next().children(".selection").children(".select2-selection").css("border","solid 1px #ffbb44");
                   """),format.raw/*321.20*/("""}"""),format.raw/*321.21*/("""else """),format.raw/*321.26*/("""{"""),format.raw/*321.27*/("""
                       """),format.raw/*322.24*/("""$(".origin").select2("trigger", "select", """),format.raw/*322.66*/("""{"""),format.raw/*322.67*/("""
                           """),format.raw/*323.28*/("""data: """),format.raw/*323.34*/("""{"""),format.raw/*323.35*/(""" """),format.raw/*323.36*/("""id:  $("#s_origin").val(), "text": $("#s_origin").val()"""),format.raw/*323.91*/("""}"""),format.raw/*323.92*/("""
                       """),format.raw/*324.24*/("""}"""),format.raw/*324.25*/(""");
                   """),format.raw/*325.20*/("""}"""),format.raw/*325.21*/("""
             """),format.raw/*326.14*/("""}"""),format.raw/*326.15*/(""");

             $.getJSON( '/api/species/json/?q=' + $("#s_species").val(), function( data ) """),format.raw/*328.91*/("""{"""),format.raw/*328.92*/("""
                """),format.raw/*329.17*/("""var items = [];
                $.each( data, function( key, val ) """),format.raw/*330.52*/("""{"""),format.raw/*330.53*/("""
                    """),format.raw/*331.21*/("""items.push(val);
                """),format.raw/*332.17*/("""}"""),format.raw/*332.18*/(""");
                if(items.length == 0)"""),format.raw/*333.38*/("""{"""),format.raw/*333.39*/("""
                    """),format.raw/*334.21*/("""$(".species").select2("trigger", "select", """),format.raw/*334.64*/("""{"""),format.raw/*334.65*/("""
                        """),format.raw/*335.25*/("""data: """),format.raw/*335.31*/("""{"""),format.raw/*335.32*/("""id: "", "text": """""),format.raw/*335.50*/("""}"""),format.raw/*335.51*/("""
                    """),format.raw/*336.21*/("""}"""),format.raw/*336.22*/(""");
                    $(".species").next().children(".selection").children(".select2-selection").css("border","solid 1px #ffbb44");
                """),format.raw/*338.17*/("""}"""),format.raw/*338.18*/("""else """),format.raw/*338.23*/("""{"""),format.raw/*338.24*/("""
                    """),format.raw/*339.21*/("""$(".species").select2("trigger", "select", """),format.raw/*339.64*/("""{"""),format.raw/*339.65*/("""
                        """),format.raw/*340.25*/("""data: """),format.raw/*340.31*/("""{"""),format.raw/*340.32*/("""id: $("#s_species").val(), "text": $("#s_species").val()"""),format.raw/*340.88*/("""}"""),format.raw/*340.89*/("""
                    """),format.raw/*341.21*/("""}"""),format.raw/*341.22*/(""");
                """),format.raw/*342.17*/("""}"""),format.raw/*342.18*/("""
                """),format.raw/*343.17*/("""document.activeElement.blur();
                document.body.scrollTop = 0; // For Chrome, Safari and Opera
                document.documentElement.scrollTop = 0; // For IE and Firefox
            """),format.raw/*346.13*/("""}"""),format.raw/*346.14*/(""");

        """),format.raw/*348.9*/("""}"""),format.raw/*348.10*/(""");
</script>

"""))
      }
    }
  }

  def render(form:Form[_$1] forSome { 
   type _$1 >: _root_.scala.Nothing <: _root_.scala.Any
},tablesForm:Form[_$2] forSome { 
   type _$2 >: _root_.scala.Nothing <: _root_.scala.Any
},optionMaps:Map[String, Map[String, String]],sampleTables:List[TableFragment]): play.twirl.api.HtmlFormat.Appendable = apply(form,tablesForm,optionMaps,sampleTables)

  def f:((Form[_$1] forSome { 
   type _$1 >: _root_.scala.Nothing <: _root_.scala.Any
},Form[_$2] forSome { 
   type _$2 >: _root_.scala.Nothing <: _root_.scala.Any
},Map[String, Map[String, String]],List[TableFragment]) => play.twirl.api.HtmlFormat.Appendable) = (form,tablesForm,optionMaps,sampleTables) => apply(form,tablesForm,optionMaps,sampleTables)

  def ref: this.type = this

}


}
}

/**/
object samplesheet extends samplesheet_Scope0.samplesheet_Scope1.samplesheet
              /*
                  -- GENERATED --
                  DATE: Wed Jun 19 09:50:51 UTC 2019
                  SOURCE: /app/app/views/dataUpload/sheets/samplesheet.scala.html
                  HASH: ac0141deabfc4d543bd3782721e957fb910229e7
                  MATRIX: 1090->81|1293->200|1325->224|1392->197|1421->260|1454->267|2223->1009|2238->1015|2364->1119|2907->1635|2922->1641|3062->1759|3280->1950|3295->1956|3429->2068|3662->2274|3677->2280|3825->2406|4792->3346|4807->3352|4918->3441|5960->4456|5975->4462|6079->4544|6777->5214|6793->5220|6898->5302|7091->5467|7107->5473|7208->5551|7508->5823|7524->5829|7618->5900|8090->6344|8106->6350|8208->6429|8742->6935|8861->7031|9092->7235|9107->7240|9199->7310|9232->7315|9315->7371|9330->7376|9422->7446|9456->7452|9856->7825|9871->7830|9963->7900|9996->7905|10079->7961|10094->7966|10186->8036|10219->8041|10940->8734|10956->8740|11066->8827|11538->9271|11649->9359|11719->9402|11813->9474|11847->9481|11939->9551|11973->9558|12066->9629|12096->9631|12164->9671|12200->9697|12228->9703|12278->9725|12308->9745|12336->9751|12385->9772|12413->9790|12441->9796|12492->9819|12518->9835|12546->9841|12598->9865|12625->9882|12653->9888|12722->9928|12752->9929|12795->9943|13150->10269|13180->10270|13226->10287|13328->10360|13358->10361|13400->10374|13473->10418|13503->10419|13549->10436|13619->10477|13649->10478|13699->10499|13788->10559|13818->10560|13860->10573|13890->10574|13988->10643|14018->10644|14064->10661|14160->10728|14190->10729|14232->10742|14299->10780|14329->10781|14375->10798|14439->10833|14469->10834|14519->10855|14602->10909|14632->10910|14674->10923|14704->10924|14773->10964|14803->10965|14849->10982|14917->11021|14947->11022|14997->11043|15172->11189|15202->11190|15256->11215|15292->11222|15322->11223|15380->11252|15447->11290|15477->11291|15528->11313|15558->11314|15637->11364|15667->11365|15748->11417|15778->11418|15845->11456|15875->11457|15921->11474|15989->11513|16019->11514|16069->11535|16247->11684|16277->11685|16331->11710|16367->11717|16397->11718|16455->11747|16522->11785|16552->11786|16603->11808|16633->11809|16712->11859|16742->11860|16823->11912|16853->11913|16919->11950|16949->11951|16995->11968|17063->12007|17093->12008|17143->12029|17320->12177|17350->12178|17404->12203|17440->12210|17470->12211|17528->12240|17595->12278|17625->12279|17676->12301|17706->12302|17785->12352|17815->12353|17896->12405|17926->12406|18006->12457|18036->12458|18082->12475|18279->12643|18309->12644|18386->12692|18416->12693|18462->12710|18651->12870|18681->12871|18759->12920|18789->12921|18835->12938|19027->13101|19057->13102|19224->13240|19254->13241|19301->13259|19398->13327|19428->13328|19479->13350|19542->13384|19572->13385|19642->13426|19672->13427|19723->13449|19797->13494|19827->13495|19882->13521|19917->13527|19947->13528|19994->13546|20024->13547|20075->13569|20105->13570|20287->13723|20317->13724|20351->13729|20381->13730|20432->13752|20506->13797|20536->13798|20591->13824|20626->13830|20656->13831|20743->13889|20773->13890|20824->13912|20854->13913|20903->13933|20933->13934|20976->13948|21006->13949|21127->14041|21157->14042|21206->14062|21305->14132|21335->14133|21388->14157|21453->14193|21483->14194|21555->14237|21585->14238|21638->14262|21709->14304|21739->14305|21796->14333|21831->14339|21861->14340|21908->14358|21938->14359|21991->14383|22021->14384|22204->14538|22234->14539|22268->14544|22298->14545|22351->14569|22422->14611|22452->14612|22509->14640|22544->14646|22574->14647|22604->14648|22688->14703|22718->14704|22771->14728|22801->14729|22852->14751|22882->14752|22925->14766|22955->14767|23078->14861|23108->14862|23154->14879|23250->14946|23280->14947|23330->14968|23392->15001|23422->15002|23491->15042|23521->15043|23571->15064|23643->15107|23673->15108|23727->15133|23762->15139|23792->15140|23839->15158|23869->15159|23919->15180|23949->15181|24127->15330|24157->15331|24191->15336|24221->15337|24271->15358|24343->15401|24373->15402|24427->15427|24462->15433|24492->15434|24577->15490|24607->15491|24657->15512|24687->15513|24735->15532|24765->15533|24811->15550|25038->15748|25068->15749|25108->15761|25138->15762
                  LINES: 36->4|40->6|40->6|41->4|43->6|46->9|63->26|63->26|63->26|70->33|70->33|70->33|77->40|77->40|77->40|84->47|84->47|84->47|98->61|98->61|98->61|123->86|123->86|123->86|141->104|141->104|141->104|147->110|147->110|147->110|155->118|155->118|155->118|165->128|165->128|165->128|171->134|171->134|182->145|182->145|182->145|183->146|186->149|186->149|186->149|188->151|196->159|196->159|196->159|197->160|200->163|200->163|200->163|201->164|211->174|211->174|211->174|219->182|219->182|224->187|224->187|226->189|226->189|228->191|228->191|230->193|235->198|235->198|235->198|236->199|236->199|236->199|237->200|237->200|237->200|238->201|238->201|238->201|239->202|239->202|239->202|241->204|241->204|243->206|249->212|249->212|250->213|251->214|251->214|252->215|252->215|252->215|253->216|253->216|253->216|254->217|255->218|255->218|256->219|256->219|257->220|257->220|258->221|259->222|259->222|260->223|260->223|260->223|261->224|261->224|261->224|262->225|263->226|263->226|264->227|264->227|266->229|266->229|267->230|268->231|268->231|269->232|272->235|272->235|273->236|273->236|273->236|274->237|275->238|275->238|276->239|276->239|278->241|278->241|280->243|280->243|282->245|282->245|283->246|284->247|284->247|285->248|288->251|288->251|289->252|289->252|289->252|290->253|291->254|291->254|292->255|292->255|294->257|294->257|296->259|296->259|298->261|298->261|299->262|300->263|300->263|301->264|304->267|304->267|305->268|305->268|305->268|306->269|307->270|307->270|308->271|308->271|310->273|310->273|312->275|312->275|314->277|314->277|315->278|317->280|317->280|319->282|319->282|320->283|322->285|322->285|324->287|324->287|325->288|327->290|327->290|331->294|331->294|332->295|333->296|333->296|334->297|335->298|335->298|336->299|336->299|337->300|337->300|337->300|338->301|338->301|338->301|338->301|338->301|339->302|339->302|341->304|341->304|341->304|341->304|342->305|342->305|342->305|343->306|343->306|343->306|343->306|343->306|344->307|344->307|345->308|345->308|346->309|346->309|348->311|348->311|349->312|350->313|350->313|351->314|352->315|352->315|353->316|353->316|354->317|354->317|354->317|355->318|355->318|355->318|355->318|355->318|356->319|356->319|358->321|358->321|358->321|358->321|359->322|359->322|359->322|360->323|360->323|360->323|360->323|360->323|360->323|361->324|361->324|362->325|362->325|363->326|363->326|365->328|365->328|366->329|367->330|367->330|368->331|369->332|369->332|370->333|370->333|371->334|371->334|371->334|372->335|372->335|372->335|372->335|372->335|373->336|373->336|375->338|375->338|375->338|375->338|376->339|376->339|376->339|377->340|377->340|377->340|377->340|377->340|378->341|378->341|379->342|379->342|380->343|383->346|383->346|385->348|385->348
                  -- GENERATED --
              */
          