
package views.html.dataUpload.sheets

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object structureitem_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

     object structureitem_Scope1 {
import service.StructureForm

class structureitem extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template2[StructureForm,Integer,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*2.2*/(st: StructureForm, index: Integer):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.37*/("""
"""),format.raw/*3.1*/("""<div style="border-bottom: solid #f0f0f0 1px;" onclick="itemExpander(this)">

    <div style="width: 88%; display: inline-block">
        <div style="display: inline-block; vertical-align: top; width: 220px; height: 160px">
            <br>
            <h5 style="margin-bottom: 8px">MS information</h5>
            Observed Precursor: """),_display_(/*9.34*/st/*9.36*/.getPrecursor),format.raw/*9.49*/(""" """),format.raw/*9.50*/("""<br>
        Theoretical Reducing Mass: """),_display_(/*10.37*/("%.2f".format(st.getReducingMass))),format.raw/*10.72*/(""" """),format.raw/*10.73*/("""<br>
        Retention Time: """),_display_(/*11.26*/st/*11.28*/.getRetentionTime),format.raw/*11.45*/(""" """),format.raw/*11.46*/("""<br> <br>
        </div>

        <div style="display: inline-block; width: 225px; vertical-align: top ">
             <img class="sugar_image" src="""),_display_(/*15.44*/{routes.GWPController.showImage(st.getStructure(), null, "extended")
            }),format.raw/*16.14*/(""" """),format.raw/*16.15*/("""width="200px" alt="" />
        </div>

        <div class="spec-container" style="height: 160px; width: 240px; display: inline-block; padding-top: 40px; cursor: pointer">
            <div id="stgraph"""),_display_(/*20.30*/index),format.raw/*20.35*/("""" style="display: inline-block; width: 200px; height: 100px"></div>
        </div>

    </div>

    <div class="down-arrow" style="display: inline-block; height: 40px; width: 10%; vertical-align: bottom; ">
        <img src="../assets/images/icons/icon_arrow.png" style="float: right; width: 26px; opacity: 0.4; cursor: pointer">
    </div>

    <div class="item-extension" style="display: none; width: 100%;">

        <div style="display: inline-block; width: 50%; vertical-align: top">

            <div>
                <br>
                Reducing End: """),_display_(/*35.32*/st/*35.34*/.getReducingEnd),format.raw/*35.49*/(""" """),format.raw/*35.50*/("""<br>
                Persubstitution: """),_display_(/*36.35*/st/*36.37*/.getPersubstitution),format.raw/*36.56*/(""" """),format.raw/*36.57*/("""<br>
                Scan MS: """),_display_(/*37.27*/st/*37.29*/.getScanMS),format.raw/*37.39*/(""" """),format.raw/*37.40*/("""<br>
                Scan MS/MS: """),_display_(/*38.30*/st/*38.32*/.getScanMSMS),format.raw/*38.44*/(""" """),format.raw/*38.45*/("""<br>
               """),format.raw/*40.61*/("""
                """),format.raw/*41.17*/("""Stability: """),_display_(/*41.29*/st/*41.31*/.getStability),format.raw/*41.44*/(""" """),format.raw/*41.45*/("""<br>
                <br>

                <h5>Orthogonal methods</h5>
                Orthogonal: """),_display_(/*45.30*/st/*45.32*/.getOrthogonal),format.raw/*45.46*/(""" """),format.raw/*45.47*/("""<br>
                <br>

                <h5> Quantitation </h5>
                Quantitation method: <br>
                Quantity: """),_display_(/*50.28*/st/*50.30*/.getQuantity),format.raw/*50.42*/(""" """),format.raw/*50.43*/("""<br>
                <br>
            </div>

        </div>

        <div style="display: inline-block; width: 49%;">

            <br>
            <h5>Database</h5>
            Database: """),_display_(/*60.24*/st/*60.26*/.getDatabase),format.raw/*60.38*/(""" """),format.raw/*60.39*/("""<br>
            Tax Restrictions: """),_display_(/*61.32*/st/*61.34*/.getTaxRestrictions),format.raw/*61.53*/(""" """),format.raw/*61.54*/("""<br>
            Other Restrictions: """),_display_(/*62.34*/st/*62.36*/.getOtherRestrictions),format.raw/*62.57*/(""" """),format.raw/*62.58*/("""<br>
            Allowed Cleavages: """),_display_(/*63.33*/st/*63.35*/.getAllowedCleavages),format.raw/*63.55*/(""" """),format.raw/*63.56*/("""<br>
            Parent Error: """),_display_(/*64.28*/st/*64.30*/.getParentError),format.raw/*64.45*/(""" """),format.raw/*64.46*/("""<br>
            Fragment Error: """),_display_(/*65.30*/st/*65.32*/.getFragmentError),format.raw/*65.49*/(""" """),format.raw/*65.50*/("""<br>
            Scoring Method: """),_display_(/*66.30*/st/*66.32*/.getScoringMethod),format.raw/*66.49*/(""" """),format.raw/*66.50*/("""<br>
            Scoring Algorithm: """),_display_(/*67.33*/st/*67.35*/.getScoringAlgorithm),format.raw/*67.55*/(""" """),format.raw/*67.56*/("""<br>
            Scoring Value Format: """),_display_(/*68.36*/st/*68.38*/.getScoringValueFormat),format.raw/*68.60*/(""" """),format.raw/*68.61*/("""<br>
            Scoring Result: """),_display_(/*69.30*/st/*69.32*/.getScoringResult),format.raw/*69.49*/(""" """),format.raw/*69.50*/("""<br>
            <br>

            <h5>Validation</h5>
            Validation Status: """),_display_(/*73.33*/st/*73.35*/.getValidationStatus),format.raw/*73.55*/(""" """),format.raw/*73.56*/("""<br>
            Validation Value Format: """),_display_(/*74.39*/st/*74.41*/.getValidationValueFormat),format.raw/*74.66*/(""" """),format.raw/*74.67*/("""<br>
            Validation Results: """),_display_(/*75.34*/st/*75.36*/.getValidationResults),format.raw/*75.57*/(""" """),format.raw/*75.58*/("""<br>
            Other: """),_display_(/*76.21*/st/*76.23*/.getOther),format.raw/*76.32*/(""" """),format.raw/*76.33*/("""<br>
        </div>

        <div style=" height: 40px; width: 93.5%;">
            <img src="../assets/images/icons/icon_uparrow.png" style="float: right; width: 26px; opacity: 0.4; cursor: pointer">
        </div>
    </div>

</div>


<script>

    """),_display_(/*89.6*/if(st.getPeakLabeledList.size > 1)/*89.40*/{_display_(Seq[Any](format.raw/*89.41*/("""
        """),format.raw/*90.9*/("""var chart;

        var jsonObject"""),_display_(/*92.24*/index),format.raw/*92.29*/(""" """),format.raw/*92.30*/("""= ﻿"""),format.raw/*92.33*/("""{"""),format.raw/*92.34*/(""""spectrumId": "TestID",
                "mzStart": 0,
                "mzStop": 1000,
                "peaks": [
            """),_display_(/*96.14*/for(pl <- st.getPeakLabeledList) yield /*96.46*/{_display_(Seq[Any](format.raw/*96.47*/("""
            """),format.raw/*97.13*/("""{"""),format.raw/*97.14*/(""""mz": """),_display_(/*97.21*/pl/*97.23*/.mz_value),format.raw/*97.32*/(""", "intensity": """),_display_(/*97.48*/pl/*97.50*/.intensity_value),format.raw/*97.66*/("""}"""),format.raw/*97.67*/(""",
            """)))}),format.raw/*98.14*/("""
        """),format.raw/*99.9*/("""]"""),format.raw/*99.10*/("""}"""),format.raw/*99.11*/(""";


        var yAxis = [
            """),_display_(/*103.14*/for(pl <- st.getPeakLabeledList) yield /*103.46*/{_display_(Seq[Any](format.raw/*103.47*/("""
            """),_display_(/*104.14*/pl/*104.16*/.intensity_value),format.raw/*104.32*/(""",
            """)))}),format.raw/*105.14*/("""
        """),format.raw/*106.9*/("""];


        var largest"""),_display_(/*109.21*/index),format.raw/*109.26*/(""" """),format.raw/*109.27*/("""= Math.max.apply(Math, yAxis);

        chart = st.chart
                .ms()
                .xlabel(" ")
                .ylabel(" ")
                .margins([0, 0, 0, 0]);
        chart.render("#stgraph"""),_display_(/*116.32*/index),format.raw/*116.37*/("""");           // render chart to id 'stgraph'


        var set = st.data.set()        // data type (set)
                .ylimits([0, largest"""),_display_(/*120.38*/index),format.raw/*120.43*/("""])        // y-axis limits
                .x("peaks.mz")             // x-accessor
                .y("peaks.intensity")      // y-accessor
                .title("spectrumId");      // id-accessor


        chart.load(set);               // bind the data set

        set.add([jsonObject"""),_display_(/*128.29*/index),format.raw/*128.34*/("""], []);

        $("#stgraph"""),_display_(/*130.21*/index),format.raw/*130.26*/("""").click(function(a) """),format.raw/*130.47*/("""{"""),format.raw/*130.48*/("""
          """),format.raw/*131.11*/("""specOverlay(jsonObject"""),_display_(/*131.34*/index),format.raw/*131.39*/(""", largest"""),_display_(/*131.49*/index),format.raw/*131.54*/(""");
        return false;
        """),format.raw/*133.9*/("""}"""),format.raw/*133.10*/(""");

        """)))}/*135.11*/else/*135.16*/{_display_(Seq[Any](format.raw/*135.17*/("""
        """),format.raw/*136.9*/("""$("#stgraph"""),_display_(/*136.21*/index),format.raw/*136.26*/("""").append("<br/>   <h4 style='padding-left: 16px'>No spectra available</h4>");
    """)))}),format.raw/*137.6*/("""
"""),format.raw/*138.1*/("""</script>"""))
      }
    }
  }

  def render(st:StructureForm,index:Integer): play.twirl.api.HtmlFormat.Appendable = apply(st,index)

  def f:((StructureForm,Integer) => play.twirl.api.HtmlFormat.Appendable) = (st,index) => apply(st,index)

  def ref: this.type = this

}


}
}

/**/
object structureitem extends structureitem_Scope0.structureitem_Scope1.structureitem
              /*
                  -- GENERATED --
                  DATE: Wed Jun 19 09:50:51 UTC 2019
                  SOURCE: /app/app/views/dataUpload/sheets/structureitem.scala.html
                  HASH: 5f19240f6ec83fd35a74f61514908c2f62b7903d
                  MATRIX: 859->31|989->66|1016->67|1379->404|1389->406|1422->419|1450->420|1518->461|1574->496|1603->497|1660->527|1671->529|1709->546|1738->547|1914->696|2017->778|2046->779|2274->980|2300->985|2887->1545|2898->1547|2934->1562|2963->1563|3029->1602|3040->1604|3080->1623|3109->1624|3167->1655|3178->1657|3209->1667|3238->1668|3299->1702|3310->1704|3343->1716|3372->1717|3420->1833|3465->1850|3504->1862|3515->1864|3549->1877|3578->1878|3705->1978|3716->1980|3751->1994|3780->1995|3943->2131|3954->2133|3987->2145|4016->2146|4233->2336|4244->2338|4277->2350|4306->2351|4369->2387|4380->2389|4420->2408|4449->2409|4514->2447|4525->2449|4567->2470|4596->2471|4660->2508|4671->2510|4712->2530|4741->2531|4800->2563|4811->2565|4847->2580|4876->2581|4937->2615|4948->2617|4986->2634|5015->2635|5076->2669|5087->2671|5125->2688|5154->2689|5218->2726|5229->2728|5270->2748|5299->2749|5366->2789|5377->2791|5420->2813|5449->2814|5510->2848|5521->2850|5559->2867|5588->2868|5702->2955|5713->2957|5754->2977|5783->2978|5853->3021|5864->3023|5910->3048|5939->3049|6004->3087|6015->3089|6057->3110|6086->3111|6138->3136|6149->3138|6179->3147|6208->3148|6486->3400|6529->3434|6568->3435|6604->3444|6666->3479|6692->3484|6721->3485|6752->3488|6781->3489|6934->3615|6982->3647|7021->3648|7062->3661|7091->3662|7125->3669|7136->3671|7166->3680|7209->3696|7220->3698|7257->3714|7286->3715|7332->3730|7368->3739|7397->3740|7426->3741|7493->3780|7542->3812|7582->3813|7624->3827|7636->3829|7674->3845|7721->3860|7758->3869|7811->3894|7838->3899|7868->3900|8104->4108|8131->4113|8302->4256|8329->4261|8647->4551|8674->4556|8731->4585|8758->4590|8808->4611|8838->4612|8878->4623|8929->4646|8956->4651|8994->4661|9021->4666|9082->4699|9112->4700|9145->4714|9159->4719|9199->4720|9236->4729|9276->4741|9303->4746|9418->4830|9447->4831
                  LINES: 30->2|35->2|36->3|42->9|42->9|42->9|42->9|43->10|43->10|43->10|44->11|44->11|44->11|44->11|48->15|49->16|49->16|53->20|53->20|68->35|68->35|68->35|68->35|69->36|69->36|69->36|69->36|70->37|70->37|70->37|70->37|71->38|71->38|71->38|71->38|72->40|73->41|73->41|73->41|73->41|73->41|77->45|77->45|77->45|77->45|82->50|82->50|82->50|82->50|92->60|92->60|92->60|92->60|93->61|93->61|93->61|93->61|94->62|94->62|94->62|94->62|95->63|95->63|95->63|95->63|96->64|96->64|96->64|96->64|97->65|97->65|97->65|97->65|98->66|98->66|98->66|98->66|99->67|99->67|99->67|99->67|100->68|100->68|100->68|100->68|101->69|101->69|101->69|101->69|105->73|105->73|105->73|105->73|106->74|106->74|106->74|106->74|107->75|107->75|107->75|107->75|108->76|108->76|108->76|108->76|121->89|121->89|121->89|122->90|124->92|124->92|124->92|124->92|124->92|128->96|128->96|128->96|129->97|129->97|129->97|129->97|129->97|129->97|129->97|129->97|129->97|130->98|131->99|131->99|131->99|135->103|135->103|135->103|136->104|136->104|136->104|137->105|138->106|141->109|141->109|141->109|148->116|148->116|152->120|152->120|160->128|160->128|162->130|162->130|162->130|162->130|163->131|163->131|163->131|163->131|163->131|165->133|165->133|167->135|167->135|167->135|168->136|168->136|168->136|169->137|170->138
                  -- GENERATED --
              */
          