
package views.html.dataUpload

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object index_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

     object index_Scope1 {
import service.UserProvider
import service.FormPubMed
import helper._

class index extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template3[UserProvider,models.User,Form[FormPubMed],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*4.2*/(userProvider: UserProvider, localUser: models.User = null, form: Form[FormPubMed]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*4.85*/("""

"""),_display_(/*6.2*/main(userProvider, "UniCarb-DB")/*6.34*/{_display_(Seq[Any](format.raw/*6.35*/("""
"""),format.raw/*7.1*/("""<script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.8.0/xlsx.full.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.8.0/jszip.js"></script>

<div id="loading-overlay" style="
    position: fixed;
    display: none;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-color: rgba(250,250,250, 1);
    z-index: 12;">
    <div id="loading-info" style="position: absolute;
        top: 50%;
        left: 50%;
        font-size: 12px;
        text-align: center;
        width: 680px;
        height: 200px;
        border-radius: 4px;
        transform: translate(-50%,-50%);
        -ms-transform: translate(-50%,-50%);">
        <img src="../assets/images/wait.gif" style="width: 400px; margin: 20px">
        <br><div><h5 style="font-size: 18px">Loading... Please Wait</h5></div>
    </div>
</div>

<div class="upload">
"""),_display_(/*37.2*/helper/*37.8*/.form(action = routes.UploadDataController.upload, 'enctype -> "multipart/form-data", 'onsubmit -> "return loadScreen();")/*37.130*/ {_display_(Seq[Any](format.raw/*37.132*/("""
    """),format.raw/*38.5*/("""<div class="container-fluid content">
        <div class="searchdiv">
            <h4 class="inline">Manuscript</h4>
            <div class="inline">
                """),_display_(/*42.18*/checkbox(form("manuscript"), '_label -> null)),format.raw/*42.63*/("""
            """),format.raw/*43.13*/("""</div>
            <br/>
            <br/>
            <br/>
            <br/>
            <div id="pubmed-container">
                <h4>PubMed ID</h4>
                <div class="search-field short">
                    """),_display_(/*51.22*/inputText(form("pubMedId"), '_label -> null, 'type -> "number", '_class -> "pid-input")),format.raw/*51.109*/("""
                """),format.raw/*52.17*/("""</div>
                <button type="button" class="btn btn-primary" id="fetch-details">
                    Fetch details from PubMed
                </button>
                <div id="authors-input" class="pubmed-info"></div>
                <div id="title-input" class="pubmed-info"></div>
                <div id="journal-input" class="pubmed-info"></div>
                <div id="year-input" class="pubmed-info"></div>
            </div>

            <div id="input-container">
                <div id="error-title" style="color: red; display: none">Missing title and/or authors!</div>
                <h5 class="inline-label">Title:</h5>
                <div class="search-field long inline">
                    """),_display_(/*66.22*/inputText(form("title"), '_label -> null)),format.raw/*66.63*/("""
                """),format.raw/*67.17*/("""</div>
                <br/>
                <h5 class="inline-label">Authors:</h5>
                <div class="search-field long inline">
                    """),_display_(/*71.22*/inputText(form("authors"), '_label -> null)),format.raw/*71.65*/("""
                """),format.raw/*72.17*/("""</div>
            </div>
            <br />
            <br />
            <br />

        </div>
        """),_display_(/*79.10*/views/*79.15*/.html.dataUpload.upload()),format.raw/*79.40*/("""
    """),format.raw/*80.5*/("""</div>
    """),_display_(/*81.6*/inputText(form("journal"),'style -> "display: none", '_label -> null)),format.raw/*81.75*/("""
    """),_display_(/*82.6*/inputText(form("year"),'style -> "display: none", '_label -> null)),format.raw/*82.72*/("""
    """),_display_(/*83.6*/inputText(form("volume"),'style -> "display: none", '_label -> null)),format.raw/*83.74*/("""
    """),_display_(/*84.6*/inputText(form("pages"),'style -> "display: none", '_label -> null)),format.raw/*84.73*/("""
    """),_display_(/*85.6*/inputText(form("abbrev"),'style -> "display: none", '_label -> null)),format.raw/*85.74*/("""
""")))}),format.raw/*86.2*/("""
"""),format.raw/*87.1*/("""</div>


<style>

    #manuscript_field dt label"""),format.raw/*92.31*/("""{"""),format.raw/*92.32*/("""
        """),format.raw/*93.9*/("""width: 0px;
        margin: 0px;
        padding: 0px;
    """),format.raw/*96.5*/("""}"""),format.raw/*96.6*/("""
    """),format.raw/*97.5*/("""#input-container"""),format.raw/*97.21*/("""{"""),format.raw/*97.22*/("""
        """),format.raw/*98.9*/("""display: none;
    """),format.raw/*99.5*/("""}"""),format.raw/*99.6*/("""
    """),format.raw/*100.5*/(""".inline"""),format.raw/*100.12*/("""{"""),format.raw/*100.13*/("""
        """),format.raw/*101.9*/("""display: inline-block;
    """),format.raw/*102.5*/("""}"""),format.raw/*102.6*/("""
    """),format.raw/*103.5*/(""".inline-label"""),format.raw/*103.18*/("""{"""),format.raw/*103.19*/("""
        """),format.raw/*104.9*/("""width: 100px;
        display: inline-block;
    """),format.raw/*106.5*/("""}"""),format.raw/*106.6*/("""
"""),format.raw/*107.1*/("""</style>

<script>

        var isManuscript = false;
        var submitClicked = false;

        function fetchDetails()"""),format.raw/*114.32*/("""{"""),format.raw/*114.33*/("""
                """),format.raw/*115.17*/("""// Example: 27456831
               var id = $("#pubMedId").val();

               if(id != "" && !isManuscript)"""),format.raw/*118.45*/("""{"""),format.raw/*118.46*/("""
                   """),format.raw/*119.20*/("""$.getJSON("http://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi", """),format.raw/*119.92*/("""{"""),format.raw/*119.93*/("""
                            """),format.raw/*120.29*/(""""db": "pubmed",
                            "retmode": "json",
                            "id": id,
                            "rettype": "abstract"
                        """),format.raw/*124.25*/("""}"""),format.raw/*124.26*/(""", function(data)"""),format.raw/*124.42*/("""{"""),format.raw/*124.43*/("""
                            """),format.raw/*125.29*/("""if(data.result.uids != undefined && data.result.uids.length>0) """),format.raw/*125.92*/("""{"""),format.raw/*125.93*/("""
                                """),format.raw/*126.33*/("""var authorArray = data.result[id].authors;
                                var authorStr = "";
                                console.log(data.result[id]);

                                $.each(authorArray, function(index, author)"""),format.raw/*130.76*/("""{"""),format.raw/*130.77*/("""
                                    """),format.raw/*131.37*/("""if(index == authorArray.length -1 && authorArray.length > 1)
                                        authorStr += " and "
                                    else if(authorStr != "") authorStr += ", ";

                                    authorStr += author.name;
                                """),format.raw/*136.33*/("""}"""),format.raw/*136.34*/(""");

                                $("#authors-input").html("<h5>Authors: </h5>" + authorStr + " <br /><br />");
                                $("#authors").val(authorStr);

                                var title = data.result[id].title;
                                $("#title-input").html("<h5>Title: </h5>" + title + " <br /><br />");
                                $("#title").val(title);

                                var journal = data.result[id].fulljournalname;
                                $("#journal-input").html("<h5>Journal: </h5>" + journal + " <br /><br />");
                                $("#journal").val(journal);

                                var year = data.result[id].pubdate.split(" ")[0];
                                $("#year-input").html("<h5>Year: </h5>" + year + " <br /><br />");
                                $("#year").val(year);

                                $("#volume").val(data.result[id].volume);
                                $("#pages").val(data.result[id].pages);
                                $("#abbrev").val(data.result[id].source);

                                if(submitClicked)"""),format.raw/*157.50*/("""{"""),format.raw/*157.51*/("""
                                """),format.raw/*158.33*/("""$('form').submit();
                                """),format.raw/*159.33*/("""}"""),format.raw/*159.34*/("""
                            """),format.raw/*160.29*/("""}"""),format.raw/*160.30*/(""" """),format.raw/*160.31*/("""else """),format.raw/*160.36*/("""{"""),format.raw/*160.37*/("""
                                """),format.raw/*161.33*/("""$("#title-input").html("<h5 style='color: red'>PubMed ID: </h5>" + "<span style='color: red'>" + $('#pubMedId').val() + " does not exist."+ "</span>");
                                $("#loading-overlay").hide();
                            """),format.raw/*163.29*/("""}"""),format.raw/*163.30*/("""
                        """),format.raw/*164.25*/("""}"""),format.raw/*164.26*/("""
                   """),format.raw/*165.20*/(""");
               """),format.raw/*166.16*/("""}"""),format.raw/*166.17*/("""
        """),format.raw/*167.9*/("""}"""),format.raw/*167.10*/("""

        """),format.raw/*169.9*/("""function loadScreen() """),format.raw/*169.31*/("""{"""),format.raw/*169.32*/("""
            """),format.raw/*170.13*/("""if($("#pubMedId").val() != "" || isManuscript)"""),format.raw/*170.59*/("""{"""),format.raw/*170.60*/("""
                """),format.raw/*171.17*/("""var submit = false;
                if($("#gwp-input").get(0).files.length === 0)"""),format.raw/*172.62*/("""{"""),format.raw/*172.63*/("""
                    """),format.raw/*173.21*/("""$("#error-gwp").show();
                """),format.raw/*174.17*/("""}"""),format.raw/*174.18*/(""" """),format.raw/*174.19*/("""else """),format.raw/*174.24*/("""{"""),format.raw/*174.25*/("""
                    """),format.raw/*175.21*/("""if(!isManuscript)"""),format.raw/*175.38*/("""{"""),format.raw/*175.39*/("""
                        """),format.raw/*176.25*/("""if($("#journal_field input").val() != "" && $("#year_field input").val() != "" && $("#volume_field input").val() != "" && $("#pages_field input").val() != "")"""),format.raw/*176.183*/("""{"""),format.raw/*176.184*/("""
                            """),format.raw/*177.29*/("""submit = true;
                        """),format.raw/*178.25*/("""}"""),format.raw/*178.26*/("""else """),format.raw/*178.31*/("""{"""),format.raw/*178.32*/("""
                            """),format.raw/*179.29*/("""submitClicked = true;
                            fetchDetails();
                        """),format.raw/*181.25*/("""}"""),format.raw/*181.26*/("""
                        """),format.raw/*182.25*/("""$("#loading-overlay").show();
                    """),format.raw/*183.21*/("""}"""),format.raw/*183.22*/(""" """),format.raw/*183.23*/("""else """),format.raw/*183.28*/("""{"""),format.raw/*183.29*/("""
                        """),format.raw/*184.25*/("""if ($("#authors").val() != "" && $("#title").val() != "")"""),format.raw/*184.82*/("""{"""),format.raw/*184.83*/("""
                           """),format.raw/*185.28*/("""submit = true;
                           $("#loading-overlay").show();
                        """),format.raw/*187.25*/("""}"""),format.raw/*187.26*/(""" """),format.raw/*187.27*/("""else """),format.raw/*187.32*/("""{"""),format.raw/*187.33*/("""
                            """),format.raw/*188.29*/("""$("#error-title").show();
                        """),format.raw/*189.25*/("""}"""),format.raw/*189.26*/("""

                    """),format.raw/*191.21*/("""}"""),format.raw/*191.22*/("""
                """),format.raw/*192.17*/("""}"""),format.raw/*192.18*/("""
                """),format.raw/*193.17*/("""return submit;
            """),format.raw/*194.13*/("""}"""),format.raw/*194.14*/("""else """),format.raw/*194.19*/("""{"""),format.raw/*194.20*/("""
                """),format.raw/*195.17*/("""return false;
            """),format.raw/*196.13*/("""}"""),format.raw/*196.14*/("""
        """),format.raw/*197.9*/("""}"""),format.raw/*197.10*/("""

        """),format.raw/*199.9*/("""$(document).ready(function()"""),format.raw/*199.37*/("""{"""),format.raw/*199.38*/("""
            """),format.raw/*200.13*/("""$('#manuscript').attr('checked', false);
            var gwpFieldCount = 1;


            $('#manuscript').click(function () """),format.raw/*204.48*/("""{"""),format.raw/*204.49*/("""
                """),format.raw/*205.17*/("""isManuscript = !isManuscript;

                if(isManuscript)"""),format.raw/*207.33*/("""{"""),format.raw/*207.34*/("""
                    """),format.raw/*208.21*/("""$("#input-container").show();
                    $("#pubmed-container").hide();
                """),format.raw/*210.17*/("""}"""),format.raw/*210.18*/(""" """),format.raw/*210.19*/("""else """),format.raw/*210.24*/("""{"""),format.raw/*210.25*/("""
                    """),format.raw/*211.21*/("""$("#input-container").hide();
                    $("#pubmed-container").show();
                """),format.raw/*213.17*/("""}"""),format.raw/*213.18*/("""
            """),format.raw/*214.13*/("""}"""),format.raw/*214.14*/(""");

            $("#fetch-details").click(function () """),format.raw/*216.51*/("""{"""),format.raw/*216.52*/("""
                """),format.raw/*217.17*/("""fetchDetails();
            """),format.raw/*218.13*/("""}"""),format.raw/*218.14*/(""");

            $("#gwp-input").change(function() """),format.raw/*220.47*/("""{"""),format.raw/*220.48*/("""
              """),format.raw/*221.15*/("""$("#error-gwp").hide();
            """),format.raw/*222.13*/("""}"""),format.raw/*222.14*/(""");
        """),format.raw/*223.9*/("""}"""),format.raw/*223.10*/(""");

"""),format.raw/*290.3*/("""
    """),format.raw/*291.5*/("""</script>
""")))}),format.raw/*292.2*/("""
"""))
      }
    }
  }

  def render(userProvider:UserProvider,localUser:models.User,form:Form[FormPubMed]): play.twirl.api.HtmlFormat.Appendable = apply(userProvider,localUser,form)

  def f:((UserProvider,models.User,Form[FormPubMed]) => play.twirl.api.HtmlFormat.Appendable) = (userProvider,localUser,form) => apply(userProvider,localUser,form)

  def ref: this.type = this

}


}
}

/**/
object index extends index_Scope0.index_Scope1.index
              /*
                  -- GENERATED --
                  DATE: Wed Jun 19 09:50:51 UTC 2019
                  SOURCE: /app/app/views/dataUpload/index.scala.html
                  HASH: 09969d899f2653c9b3d9340c98c0444d362a55e7
                  MATRIX: 889->74|1067->157|1095->160|1135->192|1173->193|1200->194|2133->1101|2147->1107|2279->1229|2320->1231|2352->1236|2546->1403|2612->1448|2653->1461|2904->1685|3013->1772|3058->1789|3805->2509|3867->2550|3912->2567|4099->2727|4163->2770|4208->2787|4343->2895|4357->2900|4403->2925|4435->2930|4473->2942|4563->3011|4595->3017|4682->3083|4714->3089|4803->3157|4835->3163|4923->3230|4955->3236|5044->3304|5076->3306|5104->3307|5180->3355|5209->3356|5245->3365|5331->3424|5359->3425|5391->3430|5435->3446|5464->3447|5500->3456|5546->3475|5574->3476|5607->3481|5643->3488|5673->3489|5710->3498|5765->3525|5794->3526|5827->3531|5869->3544|5899->3545|5936->3554|6013->3603|6042->3604|6071->3605|6221->3726|6251->3727|6297->3744|6438->3856|6468->3857|6517->3877|6618->3949|6648->3950|6706->3979|6910->4154|6940->4155|6985->4171|7015->4172|7073->4201|7165->4264|7195->4265|7257->4298|7519->4531|7549->4532|7615->4569|7941->4866|7971->4867|9157->6024|9187->6025|9249->6058|9330->6110|9360->6111|9418->6140|9448->6141|9478->6142|9512->6147|9542->6148|9604->6181|9875->6423|9905->6424|9959->6449|9989->6450|10038->6470|10085->6488|10115->6489|10152->6498|10182->6499|10220->6509|10271->6531|10301->6532|10343->6545|10418->6591|10448->6592|10494->6609|10604->6690|10634->6691|10684->6712|10753->6752|10783->6753|10813->6754|10847->6759|10877->6760|10927->6781|10973->6798|11003->6799|11057->6824|11245->6982|11276->6983|11334->7012|11402->7051|11432->7052|11466->7057|11496->7058|11554->7087|11673->7177|11703->7178|11757->7203|11836->7253|11866->7254|11896->7255|11930->7260|11960->7261|12014->7286|12100->7343|12130->7344|12187->7372|12312->7468|12342->7469|12372->7470|12406->7475|12436->7476|12494->7505|12573->7555|12603->7556|12654->7578|12684->7579|12730->7596|12760->7597|12806->7614|12862->7641|12892->7642|12926->7647|12956->7648|13002->7665|13057->7691|13087->7692|13124->7701|13154->7702|13192->7712|13249->7740|13279->7741|13321->7754|13475->7879|13505->7880|13551->7897|13643->7960|13673->7961|13723->7982|13849->8079|13879->8080|13909->8081|13943->8086|13973->8087|14023->8108|14149->8205|14179->8206|14221->8219|14251->8220|14334->8274|14364->8275|14410->8292|14467->8320|14497->8321|14576->8371|14606->8372|14650->8387|14715->8423|14745->8424|14784->8435|14814->8436|14846->10958|14879->10963|14921->10974
                  LINES: 32->4|37->4|39->6|39->6|39->6|40->7|70->37|70->37|70->37|70->37|71->38|75->42|75->42|76->43|84->51|84->51|85->52|99->66|99->66|100->67|104->71|104->71|105->72|112->79|112->79|112->79|113->80|114->81|114->81|115->82|115->82|116->83|116->83|117->84|117->84|118->85|118->85|119->86|120->87|125->92|125->92|126->93|129->96|129->96|130->97|130->97|130->97|131->98|132->99|132->99|133->100|133->100|133->100|134->101|135->102|135->102|136->103|136->103|136->103|137->104|139->106|139->106|140->107|147->114|147->114|148->115|151->118|151->118|152->119|152->119|152->119|153->120|157->124|157->124|157->124|157->124|158->125|158->125|158->125|159->126|163->130|163->130|164->131|169->136|169->136|190->157|190->157|191->158|192->159|192->159|193->160|193->160|193->160|193->160|193->160|194->161|196->163|196->163|197->164|197->164|198->165|199->166|199->166|200->167|200->167|202->169|202->169|202->169|203->170|203->170|203->170|204->171|205->172|205->172|206->173|207->174|207->174|207->174|207->174|207->174|208->175|208->175|208->175|209->176|209->176|209->176|210->177|211->178|211->178|211->178|211->178|212->179|214->181|214->181|215->182|216->183|216->183|216->183|216->183|216->183|217->184|217->184|217->184|218->185|220->187|220->187|220->187|220->187|220->187|221->188|222->189|222->189|224->191|224->191|225->192|225->192|226->193|227->194|227->194|227->194|227->194|228->195|229->196|229->196|230->197|230->197|232->199|232->199|232->199|233->200|237->204|237->204|238->205|240->207|240->207|241->208|243->210|243->210|243->210|243->210|243->210|244->211|246->213|246->213|247->214|247->214|249->216|249->216|250->217|251->218|251->218|253->220|253->220|254->221|255->222|255->222|256->223|256->223|258->290|259->291|260->292
                  -- GENERATED --
              */
          