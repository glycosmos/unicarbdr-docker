
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object about_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

     object about_Scope1 {
import service.UserProvider

class about extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[UserProvider,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*2.2*/(userProvider: UserProvider):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.30*/("""
"""),_display_(/*3.2*/main(userProvider, "About UniCarb-DR")/*3.40*/{_display_(Seq[Any](format.raw/*3.41*/("""
"""),format.raw/*4.1*/("""<script>
    $("#aboutLink").addClass("active");
</script>

<div class="about">
    <div class="container-fluid content">

        <div class="row-fluid">
            <div class="span12 centered">
                <h3 class="logo-text">
                    The Glycomic MS Database and Repository
                </h3>
            </div>
        </div>
        <div class="row-fluid info">
            <div class="span8 leftContent">
                <h3>Why did we initiate the UniCarb-DB Glycomic MS Database and Repository?</h3>
                <p>In the endeavour to make glycomics a part of mainstream life science research, we believe that it is important to assemble glycan structure databases together with the experimental attributes this data has generated. Mass spectrometry has become the key technology for characterising the complex glycome. The association of known glycan structures with individual MS fragmentation data provides reference points for the entire community and beyond. This allows the current version of UniCarb-DB to grow while an increasing research community contributes to its expansion. Eventually, this database will allow us to help deciphering the human glycome and understand how it relates and interacts with other molecules of life. This was the inspiration of a small number of dedicated researchers who launched this UniCarb-DB initiative and have gradually drawn more groups into their wake.</p>
                <p>If you think that this is a useful resource, have ideas of how it can be improved or want to contribute with data, please contact the people listed on the contact page. We are always looking for constructive partners that can help this initiative and support its longevity.</p>

                <h3>References</h3>

                <p>

                        Rojas Macias MA, Mariethoz J, Andersson P, Jin C, Horlacher O, Aoki NP, Shinmachi D, Levander F, Packer NH, Aoki-Kinoshita K, Lisacek F, Karlsson NG.
                        <b>An E-workflow for implementing reporting guidelines in glycomics mass spectrometry. </b>
                        2018,
                            doi: <a href="https://doi.org/10.1101/401141">10.1101/401141</a>
                    </p>
                    <p>
                        Lisacek F, Mariethoz J, Alocci D, Rudd PM, Abrahams JL, Campbell MP, Packer NH, Ståhle J, Widmalm G, Mullen E, Adamczyk B, Rojas-Macias MA, Jin C, Karlsson NG.
                        <b>Databases and Associated Tools for Glycomics and Glycoproteomics. </b>
                            Methods Mol Biol. 2017;1503:235-264. PubMed PMID: <a href="https://www.ncbi.nlm.nih.gov/pubmed/27743371">27743371</a>
                    </p>
                    <p>
                        Campbell MP, Nguyen-Khuong T, Hayes CA, Flowers SA, Alagesan K, Kolarich D,Packer NH, Karlsson NG.
                        <b>Validation of the curation pipeline of UniCarb-DB: building a global glycan reference MS/MS repository. </b>
                        Biochim Biophys Acta. 2014;1844(1 Pt A):108-16.
                            doi: <a href="https://doi.org/10.1016/j.bbapap.2013.04.018">10.1016/j.bbapap.2013.04.018</a>,
                            PubMed PMID: <a href="https://www.ncbi.nlm.nih.gov/pubmed/23624262">23624262</a>
                    </p>
                    <p>
                        Ali L, Kenny DT, Hayes CA, Karlsson NG.
                            <b>Structural Identification of O-Linked Oligosaccharides Using Exoglycosidases and MSn Together with UniCarb-DB Fragment Spectra Comparison. </b>
                        Metabolites. 2012 Oct 8;2(4):648-66.
                        doi: <a href="https://doi.org/10.3390/metabo2040648">10.3390/metabo2040648</a>,
                        PubMed PMID:  <a href="https://www.ncbi.nlm.nih.gov/pubmed/24957756">24957756</a>,
                            PubMed Central PMCID: <a href="https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3901228">PMC3901228</a>
                    </p>
                    <p>
                        Hayes CA, Karlsson NG, Struwe WB, Lisacek F, Rudd PM, Packer NH, Campbell MP.
                            <b>UniCarb-DB: a database resource for glycomic discovery.</b>
                            Bioinformatics. 2011 May 1;27(9):1343-4.
                            doi: <a href="https://doi.org/10.1093/bioinformatics/btr137">10.1093/bioinformatics/btr137</a>,
                            Epub 2011 Mar 12,
                            PubMed PMID:  <a href="https://www.ncbi.nlm.nih.gov/pubmed/21398669">21398669</a>
                    </p>



            </div>
            <div class="span4 sidebar supporters">
                <ul class="nav nav-pills nav-stacked clearfix">
                    <h3>Contacts</h3>
                    <li>
                        <a href='mailto:niclas.karlsson&#64;medkem.gu.se'>
                            Dr. Niclas Karlsson, Institute of Biomedicine, University of Gothenburg
                        </a>
                    </li>

                    <li>
                        <a href='mailto:Frederique.Lisacek&#64;isb-sib.ch'>
                            Dr. Frederique Lisacek,  Proteome Informatics, Swiss Institute of Bioinformatics, Geneva
                        </a>
                    </li>
                </ul>
                <ul class="nav nav-pills nav-stacked clearfix">
                    <h3>Supported By</h3>
                    <li>
                        <a href='http://www.stint.se/'>
                            <img src="../assets/images/logos/stint_logo.png"
                                 alt="Stiftelsen för internationalisering av högre utbildning och forskning"
                                 title="STINT - Stiftelsen för internationalisering av högre utbildning och forskning"
                                 class="img-responsive" />
                        </a>
                    </li>
                    <li>
                        <a href='http://expasy.org/'>
                            <img src="../assets/images/logos/expasy_logo.gif"
                                 alt="SIB ExPASy Bioninformatics Resource Portal"
                                 title="SIB ExPASy Bioninformatics Resource Portal"
                                 class="img-responsive inline-block" />
                        </a>
                    </li>
                    <li>
                        <a href='http://glycogastromics.biomedtrain.eu'>
                            <img src="../assets/images/logos/gge_logo.png"
                                 alt="GGE - Gastric Glyco Explorer"
                                 title="GGE - Gastric Glyco Explorer"
                                 class="img-responsive inline-block" />
                            <span>Gastric Glyco Explorer</span>
                        </a>
                    </li>
                </ul>
            </div>

        </div>
    </div>
</div>

""")))}),format.raw/*114.2*/("""
"""))
      }
    }
  }

  def render(userProvider:UserProvider): play.twirl.api.HtmlFormat.Appendable = apply(userProvider)

  def f:((UserProvider) => play.twirl.api.HtmlFormat.Appendable) = (userProvider) => apply(userProvider)

  def ref: this.type = this

}


}
}

/**/
object about extends about_Scope0.about_Scope1.about
              /*
                  -- GENERATED --
                  DATE: Wed Jun 19 09:50:47 UTC 2019
                  SOURCE: /app/app/views/about.scala.html
                  HASH: 90185187ccf655ac01b079ca1eafb0ba4002d390
                  MATRIX: 807->30|930->58|957->60|1003->98|1041->99|1068->100|8060->7061
                  LINES: 30->2|35->2|36->3|36->3|36->3|37->4|147->114
                  -- GENERATED --
              */
          