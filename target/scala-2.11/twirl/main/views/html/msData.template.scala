
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object msData_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

     object msData_Scope1 {
import service.UserProvider

class msData extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template12[DBviews.vReferences,MS.Scan,List[MS.PeakLabeled],Double,String,String,Map[src.PairWrapper, Double],DBviews.vBiologicalContext,DBviews.vLC,DBviews.vMassSpec,DBviews.vMirageStructures,UserProvider,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*2.2*/(journal: DBviews.vReferences,
sc: MS.Scan, pL: List[MS.PeakLabeled],
mass:Double, deriv:String, redEnd:String,
simSpectra: Map[src.PairWrapper, Double],
bio:DBviews.vBiologicalContext,
lc: DBviews.vLC,
massSpec: DBviews.vMassSpec,
mStruct: DBviews.vMirageStructures,
userProvider: UserProvider):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {

def /*16.2*/header/*16.8*/(key:String, title:String):play.twirl.api.HtmlFormat.Appendable = {_display_(

Seq[Any](format.raw/*16.38*/("""

""")))};
Seq[Any](format.raw/*10.28*/("""


"""),format.raw/*15.37*/("""
"""),format.raw/*18.2*/("""
"""),format.raw/*19.1*/("""<script src=""""),_display_(/*19.15*/routes/*19.21*/.Assets.versioned("javascripts/st.js")),format.raw/*19.59*/(""""></script>
<script src=""""),_display_(/*20.15*/routes/*20.21*/.Assets.versioned("javascripts/d3.js")),format.raw/*20.59*/(""""></script>


"""),_display_(/*23.2*/main(userProvider, "MIRAGE upload")/*23.37*/{_display_(Seq[Any](format.raw/*23.38*/("""

"""),format.raw/*25.1*/("""<div class="container-fluid content">
    <ul class="breadcrumb">
        <li><i class="icon-home" ></i><a href="/"> UniCarb-DB</a> <span class="divider">></span></li>
        <li class="active"><i class="icon-th" ></i> Structure<span class="divider"></span></li>
    </ul>


    <section id="structureLayout">
        <section id="layouts">

            <div class="page-header row-fluid">
                <h1 class="">Structure</h1>
            </div>


            <div class="row-fluid">
                <div class="span8">
                    <ul class='structures'>
                        <img class="sugar_image" src="""),_display_(/*43.55*/{routes.GWPController.showImage(sc.glycanSequence.sequence_gws,session.get("notation"),"extended")}),format.raw/*43.154*/(""" """),format.raw/*43.155*/("""height="25%", width="35%" alt=""/>

                    </ul>

                    <p><b>"""),_display_(/*47.28*/journal/*47.35*/.title),format.raw/*47.41*/("""</b></p>
                    """),_display_(/*48.22*/if(sc.note != null)/*48.41*/{_display_(Seq[Any](format.raw/*48.42*/("""
                    """),format.raw/*49.21*/("""<p class="evaluation">"""),_display_(/*49.44*/sc/*49.46*/.note),format.raw/*49.51*/("""</p>""")))}),format.raw/*49.56*/("""
                    """),format.raw/*50.21*/("""<p><a target="blank" href="http://www.ncbi.nlm.nih.gov/pubmed/"""),_display_(/*50.84*/journal/*50.91*/.pubmed_id),format.raw/*50.101*/("""">PubMed Entry</a></p>

                    """),_display_(/*52.22*/if(sc.glycanSequence.glytoucan_id != null)/*52.64*/{_display_(Seq[Any](format.raw/*52.65*/("""
                    """),format.raw/*53.21*/("""<p><img src=""""),_display_(/*53.35*/routes/*53.41*/.Assets.versioned("images/logo_toucan.png")),format.raw/*53.84*/(""""> GlyToucan Accession Number:<a target="blank" href="https://glytoucan.org/Structures/Glycans/"""),_display_(/*53.180*/sc/*53.182*/.glycanSequence.glytoucan_id),format.raw/*53.210*/(""""> """),_display_(/*53.214*/sc/*53.216*/.glycanSequence.glytoucan_id),format.raw/*53.244*/("""</a></p>""")))}),format.raw/*53.253*/("""

                    """),_display_(/*55.22*/views/*55.27*/.html.spectra.spectraTable(pL, simSpectra)),format.raw/*55.69*/("""

                """),format.raw/*57.17*/("""</div>

                <div class="span4 sidebar">
                    """),_display_(/*60.22*/views/*60.27*/.html.format.format()),format.raw/*60.48*/("""
                    """),format.raw/*61.71*/("""
                    """),_display_(/*62.22*/views/*62.27*/.html.additionalInfo.structureDetails(mass,deriv,redEnd)),format.raw/*62.83*/("""
                    """),_display_(/*63.22*/views/*63.27*/.html.additionalInfo.bioContext(bio)),format.raw/*63.63*/("""
                    """),_display_(/*64.22*/if(lc != null)/*64.36*/{_display_(Seq[Any](format.raw/*64.37*/("""
                        """),_display_(/*65.26*/views/*65.31*/.html.additionalInfo.hplc(lc)),format.raw/*65.60*/("""
                    """)))}),format.raw/*66.22*/("""


                    """),_display_(/*69.22*/views/*69.27*/.html.additionalInfo.massSpec(massSpec)),format.raw/*69.66*/("""

                    """),_display_(/*71.22*/if(mStruct != null)/*71.41*/ {_display_(Seq[Any](format.raw/*71.43*/("""
                        """),_display_(/*72.26*/views/*72.31*/.html.additionalInfo.mirageStructures(mStruct)),format.raw/*72.77*/("""
                    """)))}),format.raw/*73.22*/("""

                """),format.raw/*75.17*/("""</div>

            </div>
        </section>
    </section>
    <a href="https://web.expasy.org/contact.html?subject=UniCarb-DB" class='label label-dark'>Report errors in this entry</a></p>
</div>

"""),_display_(/*83.2*/views/*83.7*/.html.spectra.spectraViewer(pL)),format.raw/*83.38*/("""

""")))}),format.raw/*85.2*/("""



"""))
      }
    }
  }

  def render(journal:DBviews.vReferences,sc:MS.Scan,pL:List[MS.PeakLabeled],mass:Double,deriv:String,redEnd:String,simSpectra:Map[src.PairWrapper, Double],bio:DBviews.vBiologicalContext,lc:DBviews.vLC,massSpec:DBviews.vMassSpec,mStruct:DBviews.vMirageStructures,userProvider:UserProvider): play.twirl.api.HtmlFormat.Appendable = apply(journal,sc,pL,mass,deriv,redEnd,simSpectra,bio,lc,massSpec,mStruct,userProvider)

  def f:((DBviews.vReferences,MS.Scan,List[MS.PeakLabeled],Double,String,String,Map[src.PairWrapper, Double],DBviews.vBiologicalContext,DBviews.vLC,DBviews.vMassSpec,DBviews.vMirageStructures,UserProvider) => play.twirl.api.HtmlFormat.Appendable) = (journal,sc,pL,mass,deriv,redEnd,simSpectra,bio,lc,massSpec,mStruct,userProvider) => apply(journal,sc,pL,mass,deriv,redEnd,simSpectra,bio,lc,massSpec,mStruct,userProvider)

  def ref: this.type = this

}


}
}

/**/
object msData extends msData_Scope0.msData_Scope1.msData
              /*
                  -- GENERATED --
                  DATE: Wed Jun 19 09:50:47 UTC 2019
                  SOURCE: /app/app/views/msData.scala.html
                  HASH: 4b04aac0002f96dd5ac5d999dbffaeeb3e3df3f6
                  MATRIX: 993->30|1367->438|1381->444|1488->474|1531->325|1562->436|1590->477|1618->478|1659->492|1674->498|1733->536|1786->562|1801->568|1860->606|1901->621|1945->656|1984->657|2013->659|2666->1285|2787->1384|2817->1385|2934->1475|2950->1482|2977->1488|3034->1518|3062->1537|3101->1538|3150->1559|3200->1582|3211->1584|3237->1589|3273->1594|3322->1615|3412->1678|3428->1685|3460->1695|3532->1740|3583->1782|3622->1783|3671->1804|3712->1818|3727->1824|3791->1867|3915->1963|3927->1965|3977->1993|4009->1997|4021->1999|4071->2027|4112->2036|4162->2059|4176->2064|4239->2106|4285->2124|4385->2197|4399->2202|4441->2223|4490->2294|4539->2316|4553->2321|4630->2377|4679->2399|4693->2404|4750->2440|4799->2462|4822->2476|4861->2477|4914->2503|4928->2508|4978->2537|5031->2559|5082->2583|5096->2588|5156->2627|5206->2650|5234->2669|5274->2671|5327->2697|5341->2702|5408->2748|5461->2770|5507->2788|5733->2988|5746->2993|5798->3024|5831->3027
                  LINES: 30->2|42->16|42->16|44->16|47->10|50->15|51->18|52->19|52->19|52->19|52->19|53->20|53->20|53->20|56->23|56->23|56->23|58->25|76->43|76->43|76->43|80->47|80->47|80->47|81->48|81->48|81->48|82->49|82->49|82->49|82->49|82->49|83->50|83->50|83->50|83->50|85->52|85->52|85->52|86->53|86->53|86->53|86->53|86->53|86->53|86->53|86->53|86->53|86->53|86->53|88->55|88->55|88->55|90->57|93->60|93->60|93->60|94->61|95->62|95->62|95->62|96->63|96->63|96->63|97->64|97->64|97->64|98->65|98->65|98->65|99->66|102->69|102->69|102->69|104->71|104->71|104->71|105->72|105->72|105->72|106->73|108->75|116->83|116->83|116->83|118->85
                  -- GENERATED --
              */
          