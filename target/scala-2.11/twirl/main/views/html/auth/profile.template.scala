
package views.html.auth

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object profile_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

     object profile_Scope1 {
import service.UserProvider

class profile extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template3[com.feth.play.module.pa.PlayAuthenticate,UserProvider,models.User,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*2.2*/(playAuth: com.feth.play.module.pa.PlayAuthenticate, userProvider: UserProvider, localUser: models.User = null):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {
import com.feth.play.module.pa.views.html._

Seq[Any](format.raw/*2.113*/("""

"""),format.raw/*5.1*/("""
"""),_display_(/*6.2*/main(userProvider, "UniCarb-DB Profile")/*6.42*/{_display_(Seq[Any](format.raw/*6.43*/("""


"""),format.raw/*9.1*/("""<div class="container-fluid content">
    <ul class="breadcrumb">
        <li><i class="icon-home" ></i><a href="/"> UniCarb-DB</a> <span class="divider">></span></li>
        <li class="active"><i class="icon-download" ></i> Profile<span class="divider"></span></li>
    </ul>

    <div class="row-fluid info">
        <div class="row-fluid">
            <div class="page-header row-fluid">
                <h1 class="subheader span8">Your UniCarb-DB profile</h1>
            </div>

            <p>
                Your name is """),_display_(/*22.31*/localUser/*22.40*/.name),format.raw/*22.45*/(""" """),format.raw/*22.46*/("""and your email address is """),_display_(/*22.73*/if(!localUser.email)/*22.93*/ {_display_(Seq[Any](format.raw/*22.95*/("""<em>&lt;unknown&gt;</em>.""")))}/*22.122*/else/*22.127*/{_display_(Seq[Any](format.raw/*22.128*/("""
                """),_display_(/*23.18*/localUser/*23.27*/.email),format.raw/*23.33*/(""".
                <i>
                    """),_display_(/*25.22*/if(!localUser.emailValidated && localUser.email)/*25.70*/ {_display_(Seq[Any](format.raw/*25.72*/("""
                    """),format.raw/*26.21*/("""(<a href=""""),_display_(/*26.32*/routes/*26.38*/.Account.verifyEmail),format.raw/*26.58*/("""">unverified - click to verify</a>)
                    """)))}/*27.23*/else/*27.28*/{_display_(Seq[Any](format.raw/*27.29*/("""
                    """),format.raw/*28.21*/("""(verified)
                    """)))}),format.raw/*29.22*/("""</i>
                """)))}),format.raw/*30.18*/("""
                """),format.raw/*31.17*/("""<br />
                """),_display_(/*32.18*/if(localUser.firstName && localUser.lastName)/*32.63*/ {_display_(Seq[Any](format.raw/*32.65*/("""
                    """),format.raw/*33.21*/("""Your first name is """),_display_(/*33.41*/localUser/*33.50*/.firstName),format.raw/*33.60*/(""" """),format.raw/*33.61*/("""and your last name is """),_display_(/*33.84*/localUser/*33.93*/.lastName),format.raw/*33.102*/("""
                """),format.raw/*34.17*/("""<br/>
                """)))}),format.raw/*35.18*/("""
                """),_display_(/*36.18*/defining(localUser.getProviders())/*36.52*/ { providers =>_display_(Seq[Any](format.raw/*36.67*/("""
                    """),_display_(/*37.22*/if(providers.size() > 0)/*37.46*/ {_display_(Seq[Any](format.raw/*37.48*/("""
                        """),_display_(/*38.26*/if(providers.size() ==1)/*38.50*/ {_display_(Seq[Any](format.raw/*38.52*/("""
                            """),format.raw/*39.29*/("""There is one provider linked with your account:
                        """)))}/*40.27*/else/*40.32*/{_display_(Seq[Any](format.raw/*40.33*/("""
                            """),format.raw/*41.29*/("""There are """),format.raw/*41.39*/("""{"""),format.raw/*41.40*/("""0"""),format.raw/*41.41*/("""}"""),format.raw/*41.42*/(""" """),format.raw/*41.43*/("""providers linked with your account: """),_display_(/*41.80*/providers/*41.89*/.size().toString()),format.raw/*41.107*/("""
                        """)))}),format.raw/*42.26*/("""
                        """),_display_(/*43.26*/for(p <- providers) yield /*43.45*/ {_display_(Seq[Any](format.raw/*43.47*/("""
                            """),_display_(/*44.30*/_providerIcon(p)),format.raw/*44.46*/("""
                        """)))}),format.raw/*45.26*/("""
                    """),format.raw/*46.21*/("""<br/>
                    """)))}),format.raw/*47.22*/("""
                """)))}),format.raw/*48.18*/("""

                """),format.raw/*50.17*/("""<br/>
                """),_display_(/*51.18*/currentAuth(playAuth)/*51.39*/ { auth =>_display_(Seq[Any](format.raw/*51.49*/("""
                     """),format.raw/*52.22*/("""<br/>
                    """),_display_(/*53.22*/if(auth.expires() != -1)/*53.46*/{_display_(Seq[Any](format.raw/*53.47*/("""
                        """),format.raw/*54.25*/("""Your user ID is """),_display_(/*54.42*/auth/*54.46*/.getId()),format.raw/*54.54*/(""" """),format.raw/*54.55*/("""and your session will expire on """),_display_(/*54.88*/HomeController/*54.102*/.formatTimestamp(auth.expires())),format.raw/*54.134*/("""
                    """)))}/*55.23*/else/*55.28*/{_display_(Seq[Any](format.raw/*55.29*/("""
                        """),format.raw/*56.25*/("""Your user ID is  """),_display_(/*56.43*/auth/*56.47*/.getId()),format.raw/*56.55*/(""" """),format.raw/*56.56*/("""and your session will not expire, as it is endless
                    """)))}),format.raw/*57.22*/("""
                """)))}),format.raw/*58.18*/("""
                """),format.raw/*59.17*/("""<br/>
            </p>

<!--            <ul>
                <li><a href=""""),_display_(/*63.31*/routes/*63.37*/.Account.changePassword),format.raw/*63.60*/("""">"""),format.raw/*63.118*/("""</a></li>
            </ul>-->

        </div>
    </div>
</div>





""")))}),format.raw/*74.2*/("""
"""))
      }
    }
  }

  def render(playAuth:com.feth.play.module.pa.PlayAuthenticate,userProvider:UserProvider,localUser:models.User): play.twirl.api.HtmlFormat.Appendable = apply(playAuth,userProvider,localUser)

  def f:((com.feth.play.module.pa.PlayAuthenticate,UserProvider,models.User) => play.twirl.api.HtmlFormat.Appendable) = (playAuth,userProvider,localUser) => apply(playAuth,userProvider,localUser)

  def ref: this.type = this

}


}
}

/**/
object profile extends profile_Scope0.profile_Scope1.profile
              /*
                  -- GENERATED --
                  DATE: Wed Jun 19 09:50:50 UTC 2019
                  SOURCE: /app/app/views/auth/profile.scala.html
                  HASH: c6e2dd2fad830c37ddd57718e7b7716cfdfc14db
                  MATRIX: 871->30|1121->141|1149->188|1176->190|1224->230|1262->231|1291->234|1849->765|1867->774|1893->779|1922->780|1976->807|2005->827|2045->829|2091->856|2105->861|2145->862|2190->880|2208->889|2235->895|2305->938|2362->986|2402->988|2451->1009|2489->1020|2504->1026|2545->1046|2621->1104|2634->1109|2673->1110|2722->1131|2785->1163|2838->1185|2883->1202|2934->1226|2988->1271|3028->1273|3077->1294|3124->1314|3142->1323|3173->1333|3202->1334|3252->1357|3270->1366|3301->1375|3346->1392|3400->1415|3445->1433|3488->1467|3541->1482|3590->1504|3623->1528|3663->1530|3716->1556|3749->1580|3789->1582|3846->1611|3938->1685|3951->1690|3990->1691|4047->1720|4085->1730|4114->1731|4143->1732|4172->1733|4201->1734|4265->1771|4283->1780|4323->1798|4380->1824|4433->1850|4468->1869|4508->1871|4565->1901|4602->1917|4659->1943|4708->1964|4766->1991|4815->2009|4861->2027|4911->2050|4941->2071|4989->2081|5039->2103|5093->2130|5126->2154|5165->2155|5218->2180|5262->2197|5275->2201|5304->2209|5333->2210|5393->2243|5417->2257|5471->2289|5512->2312|5525->2317|5564->2318|5617->2343|5662->2361|5675->2365|5704->2373|5733->2374|5836->2446|5885->2464|5930->2481|6032->2556|6047->2562|6091->2585|6122->2643|6223->2714
                  LINES: 30->2|35->2|37->5|38->6|38->6|38->6|41->9|54->22|54->22|54->22|54->22|54->22|54->22|54->22|54->22|54->22|54->22|55->23|55->23|55->23|57->25|57->25|57->25|58->26|58->26|58->26|58->26|59->27|59->27|59->27|60->28|61->29|62->30|63->31|64->32|64->32|64->32|65->33|65->33|65->33|65->33|65->33|65->33|65->33|65->33|66->34|67->35|68->36|68->36|68->36|69->37|69->37|69->37|70->38|70->38|70->38|71->39|72->40|72->40|72->40|73->41|73->41|73->41|73->41|73->41|73->41|73->41|73->41|73->41|74->42|75->43|75->43|75->43|76->44|76->44|77->45|78->46|79->47|80->48|82->50|83->51|83->51|83->51|84->52|85->53|85->53|85->53|86->54|86->54|86->54|86->54|86->54|86->54|86->54|86->54|87->55|87->55|87->55|88->56|88->56|88->56|88->56|88->56|89->57|90->58|91->59|95->63|95->63|95->63|95->63|106->74
                  -- GENERATED --
              */
          