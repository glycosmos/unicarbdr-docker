
package views.html.auth

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object restricted_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

     object restricted_Scope1 {
import service.UserProvider

class restricted extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template2[UserProvider,models.User,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*2.2*/(userProvider: UserProvider, localUser: models.User = null):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.61*/("""

"""),_display_(/*4.2*/main(userProvider, "UniCarb-DB restricted page")/*4.50*/{_display_(Seq[Any](format.raw/*4.51*/("""

"""),format.raw/*6.1*/("""<h1>"""),_display_(/*6.6*/Messages("playauthenticate.navigation.restricted")),format.raw/*6.56*/("""</h1>
<p>
    """),_display_(/*8.6*/Messages("playauthenticate.restricted.secrets")),format.raw/*8.53*/("""
"""),format.raw/*9.1*/("""</p>
""")))}),format.raw/*10.2*/("""
"""))
      }
    }
  }

  def render(userProvider:UserProvider,localUser:models.User): play.twirl.api.HtmlFormat.Appendable = apply(userProvider,localUser)

  def f:((UserProvider,models.User) => play.twirl.api.HtmlFormat.Appendable) = (userProvider,localUser) => apply(userProvider,localUser)

  def ref: this.type = this

}


}
}

/**/
object restricted extends restricted_Scope0.restricted_Scope1.restricted
              /*
                  -- GENERATED --
                  DATE: Wed Jun 19 09:50:50 UTC 2019
                  SOURCE: /app/app/views/auth/restricted.scala.html
                  HASH: 986c84a5d6ef260d7cc6068002cb9b4d24e29ffd
                  MATRIX: 839->30|993->89|1021->92|1077->140|1115->141|1143->143|1173->148|1243->198|1283->213|1350->260|1377->261|1413->267
                  LINES: 30->2|35->2|37->4|37->4|37->4|39->6|39->6|39->6|41->8|41->8|42->9|43->10
                  -- GENERATED --
              */
          