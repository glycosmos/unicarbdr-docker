
package views.html.auth

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object _emailPartial_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class _emailPartial extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template2[Form[_$1] forSome { 
   type _$1 >: _root_.scala.Nothing <: _root_.scala.Any
},Boolean,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(f: Form[_], constraints: Boolean = false):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {
import helper._
implicit def /*4.2*/implicitFieldConstructor/*4.26*/ = {{ b3.vertical.fieldConstructor }};
Seq[Any](format.raw/*1.44*/("""

"""),format.raw/*4.62*/("""

"""),format.raw/*6.1*/("""<h4>Your e-mail address</h4>

"""),_display_(/*8.2*/inputText(
f("email"),
'_showConstraints -> constraints,
'_class -> "search-field",
'_label -> " "
)),format.raw/*13.2*/("""
"""))
      }
    }
  }

  def render(f:Form[_$1] forSome { 
   type _$1 >: _root_.scala.Nothing <: _root_.scala.Any
},constraints:Boolean): play.twirl.api.HtmlFormat.Appendable = apply(f,constraints)

  def f:((Form[_$1] forSome { 
   type _$1 >: _root_.scala.Nothing <: _root_.scala.Any
},Boolean) => play.twirl.api.HtmlFormat.Appendable) = (f,constraints) => apply(f,constraints)

  def ref: this.type = this

}


}

/**/
object _emailPartial extends _emailPartial_Scope0._emailPartial
              /*
                  -- GENERATED --
                  DATE: Wed Jun 19 09:50:50 UTC 2019
                  SOURCE: /app/app/views/auth/_emailPartial.scala.html
                  HASH: 501875a7852a902c95f99dcc6018bd128d44745a
                  MATRIX: 846->1|990->63|1022->87|1088->43|1117->123|1145->125|1201->156|1321->256
                  LINES: 29->1|33->4|33->4|34->1|36->4|38->6|40->8|45->13
                  -- GENERATED --
              */
          