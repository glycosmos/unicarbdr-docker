
package views.html.auth

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object signup_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

     object signup_Scope1 {
import service.UserProvider

class signup extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template3[com.feth.play.module.pa.PlayAuthenticate,UserProvider,Form[_$1] forSome { 
   type _$1 >: _root_.scala.Nothing <: _root_.scala.Any
},play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*2.2*/(playAuth: com.feth.play.module.pa.PlayAuthenticate, userProvider: UserProvider, signupForm: Form[_]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {
import helper._
implicit def /*5.2*/implicitFieldConstructor/*5.26*/ = {{ b3.vertical.fieldConstructor }};
Seq[Any](format.raw/*2.103*/("""

"""),format.raw/*5.62*/("""

"""),_display_(/*7.2*/main(userProvider, "UniCarb-DR Sign Up")/*7.42*/{_display_(Seq[Any](format.raw/*7.43*/("""


"""),_display_(/*10.2*/if(flash.containsKey("error"))/*10.32*/ {_display_(Seq[Any](format.raw/*10.34*/("""
"""),format.raw/*11.1*/("""<div class="error message">
    <h3>Restricted access</h3>
    <p>"""),_display_(/*13.9*/flash/*13.14*/.get("error")),format.raw/*13.27*/("""</p>
</div>
""")))}),format.raw/*15.2*/("""



"""),format.raw/*19.1*/("""<div class="container-fluid content">
    <ul class="breadcrumb">
        <li><i class="icon-home" ></i><a href="/"> UniCarb-DR</a> <span class="divider">></span></li>
        <li class="active"><i class="icon-download" ></i> Sign up<span class="divider"></span></li>
    </ul>


    <div class="row-fluid info">
        <div class="row-fluid">
            <div class="page-header row-fluid">
                <h1 class="subheader span8">Login</h1>
            </div>
            <div id="signup" class="row">


                <div class="col-xs-6 col-md-4">
                    """),format.raw/*35.57*/("""
                    """),_display_(/*36.22*/helper/*36.28*/.form(routes.HomeController.doSignup, 'class -> "form-horizontal", 'role -> "form")/*36.111*/ {_display_(Seq[Any](format.raw/*36.113*/("""

                        """),_display_(/*38.26*/if(signupForm.hasGlobalErrors)/*38.56*/ {_display_(Seq[Any](format.raw/*38.58*/("""
                        """),format.raw/*39.25*/("""<p class="alert alert-danger">
                            """),_display_(/*40.30*/signupForm/*40.40*/.globalError.message),format.raw/*40.60*/("""
                        """),format.raw/*41.25*/("""</p>
                        """)))}),format.raw/*42.26*/("""

                        """),format.raw/*44.25*/("""<h4>Your name</h4>
                        """),_display_(/*45.26*/inputText(signupForm("name"), '_label -> " ", '_class -> "search-field")),format.raw/*45.98*/("""

                        """),_display_(/*47.26*/_emailPartial(signupForm)),format.raw/*47.51*/("""

                        """),_display_(/*49.26*/_passwordPartial(signupForm)),format.raw/*49.54*/("""


                    """),format.raw/*52.21*/("""<div class="row-fluid">
                        """),_display_(/*53.26*/b3/*53.28*/.free()/*53.35*/{_display_(Seq[Any](format.raw/*53.36*/("""
                            """),format.raw/*54.29*/("""<div><strong>Click in the square next to "I'm not a robot"</strong></div>
                            <!-- <div class="g-recaptcha" data-sitekey=""""),_display_(/*55.74*/play/*55.78*/.Play.application().configuration().getString("gcaptcha.gsiteKey")),format.raw/*55.144*/(""""></div> -->
                            <br />
                            <div class="g-recaptcha" data-callback="testCallback" data-sitekey="6LcMCKcUAAAAAF52QEmC9yCvUKH070BLV8fVTGhj"></div>
                            <!-- To make the confirmation button whether the user is human or robott, there may need to be inputed code. -->
                            <button id="submit" type="submit" class="btn btn-primary" disabled="disabled">Sign up now</button>
                        """)))}),format.raw/*60.26*/("""
                    """),format.raw/*61.21*/("""</div>
                    """)))}),format.raw/*62.22*/("""
                """),format.raw/*63.17*/("""</div>

            </div>
        </div>
    </div>
</div>

""")))}),format.raw/*70.2*/("""
"""))
      }
    }
  }

  def render(playAuth:com.feth.play.module.pa.PlayAuthenticate,userProvider:UserProvider,signupForm:Form[_$1] forSome { 
   type _$1 >: _root_.scala.Nothing <: _root_.scala.Any
}): play.twirl.api.HtmlFormat.Appendable = apply(playAuth,userProvider,signupForm)

  def f:((com.feth.play.module.pa.PlayAuthenticate,UserProvider,Form[_$1] forSome { 
   type _$1 >: _root_.scala.Nothing <: _root_.scala.Any
}) => play.twirl.api.HtmlFormat.Appendable) = (playAuth,userProvider,signupForm) => apply(playAuth,userProvider,signupForm)

  def ref: this.type = this

}


}
}

/**/
object signup extends signup_Scope0.signup_Scope1.signup
              /*
                  -- GENERATED --
                  DATE: Wed Jun 19 09:50:50 UTC 2019
                  SOURCE: /app/app/views/auth/signup.scala.html
                  HASH: 02a7e64b121a16a90b78878f794a8e79710b3ef0
                  MATRIX: 935->30|1138->151|1170->175|1237->131|1266->211|1294->214|1342->254|1380->255|1410->259|1449->289|1489->291|1517->292|1610->359|1624->364|1658->377|1701->390|1732->394|2339->1009|2388->1031|2403->1037|2496->1120|2537->1122|2591->1149|2630->1179|2670->1181|2723->1206|2810->1266|2829->1276|2870->1296|2923->1321|2984->1351|3038->1377|3109->1421|3202->1493|3256->1520|3302->1545|3356->1572|3405->1600|3456->1623|3532->1672|3543->1674|3559->1681|3598->1682|3655->1711|3829->1858|3842->1862|3930->1928|4447->2414|4496->2435|4555->2463|4600->2480|4692->2542
                  LINES: 32->2|36->5|36->5|37->2|39->5|41->7|41->7|41->7|44->10|44->10|44->10|45->11|47->13|47->13|47->13|49->15|53->19|69->35|70->36|70->36|70->36|70->36|72->38|72->38|72->38|73->39|74->40|74->40|74->40|75->41|76->42|78->44|79->45|79->45|81->47|81->47|83->49|83->49|86->52|87->53|87->53|87->53|87->53|88->54|89->55|89->55|89->55|94->60|95->61|96->62|97->63|104->70
                  -- GENERATED --
              */
          