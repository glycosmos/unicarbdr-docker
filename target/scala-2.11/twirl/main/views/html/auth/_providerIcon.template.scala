
package views.html.auth

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object _providerIcon_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class _providerIcon extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(providerKey: String):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.23*/("""
"""),format.raw/*2.1*/("""<img alt=""""),_display_(/*2.12*/providerKey),format.raw/*2.23*/(""" """),format.raw/*2.24*/("""icon" title=""""),_display_(/*2.38*/providerKey),format.raw/*2.49*/("""" src=""""),_display_(/*2.57*/routes/*2.63*/.Assets.versioned("images/icons/"+providerKey+"-24x24.png")),format.raw/*2.122*/("""">"""))
      }
    }
  }

  def render(providerKey:String): play.twirl.api.HtmlFormat.Appendable = apply(providerKey)

  def f:((String) => play.twirl.api.HtmlFormat.Appendable) = (providerKey) => apply(providerKey)

  def ref: this.type = this

}


}

/**/
object _providerIcon extends _providerIcon_Scope0._providerIcon
              /*
                  -- GENERATED --
                  DATE: Wed Jun 19 09:50:50 UTC 2019
                  SOURCE: /app/app/views/auth/_providerIcon.scala.html
                  HASH: d9452efc9d8d4b5dd9a78e4e8709edbe17897a69
                  MATRIX: 766->1|882->22|909->23|946->34|977->45|1005->46|1045->60|1076->71|1110->79|1124->85|1204->144
                  LINES: 27->1|32->1|33->2|33->2|33->2|33->2|33->2|33->2|33->2|33->2|33->2
                  -- GENERATED --
              */
          