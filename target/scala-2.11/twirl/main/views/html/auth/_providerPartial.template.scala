
package views.html.auth

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object _providerPartial_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class _providerPartial extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template2[com.feth.play.module.pa.PlayAuthenticate,Boolean,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(playAuth: com.feth.play.module.pa.PlayAuthenticate, skipCurrent: Boolean = true):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {
import com.feth.play.module.pa.views.html._

Seq[Any](format.raw/*1.83*/("""

"""),format.raw/*4.1*/("""
"""),format.raw/*5.1*/("""<script type="text/javascript">
function askOpenID(url) """),format.raw/*6.25*/("""{"""),format.raw/*6.26*/("""
	"""),format.raw/*7.2*/("""var openid = prompt("Please enter your OpenID:", "yourname.myopenid.com");
	if(openid) """),format.raw/*8.13*/("""{"""),format.raw/*8.14*/("""
		"""),format.raw/*9.3*/("""window.location.href = url + "?p=" + encodeURIComponent(openid);
	"""),format.raw/*10.2*/("""}"""),format.raw/*10.3*/("""
"""),format.raw/*11.1*/("""}"""),format.raw/*11.2*/("""
"""),format.raw/*12.1*/("""</script>
<ul class="providers">
    """),_display_(/*14.6*/forProviders(playAuth, skipCurrent)/*14.41*/ { p =>_display_(Seq[Any](format.raw/*14.48*/("""
    """),format.raw/*15.5*/("""<li>
        """),_display_(/*16.10*/if(p.getKey() == "openid")/*16.36*/ {_display_(Seq[Any](format.raw/*16.38*/("""
        """),format.raw/*17.9*/("""<a href="javascript:void(0);" onclick="askOpenID('"""),_display_(/*17.60*/p/*17.61*/.getUrl()),format.raw/*17.70*/("""');">
            """)))}/*18.15*/else/*18.20*/{_display_(Seq[Any](format.raw/*18.21*/("""
            """),format.raw/*19.13*/("""<a href=""""),_display_(/*19.23*/p/*19.24*/.getUrl()),format.raw/*19.33*/("""">
                """)))}),format.raw/*20.18*/("""
                """),_display_(/*21.18*/_providerIcon(p.getKey())),format.raw/*21.43*/("""</a>
    </li>
    """)))}),format.raw/*23.6*/("""
"""),format.raw/*24.1*/("""</ul>"""))
      }
    }
  }

  def render(playAuth:com.feth.play.module.pa.PlayAuthenticate,skipCurrent:Boolean): play.twirl.api.HtmlFormat.Appendable = apply(playAuth,skipCurrent)

  def f:((com.feth.play.module.pa.PlayAuthenticate,Boolean) => play.twirl.api.HtmlFormat.Appendable) = (playAuth,skipCurrent) => apply(playAuth,skipCurrent)

  def ref: this.type = this

}


}

/**/
object _providerPartial extends _providerPartial_Scope0._providerPartial
              /*
                  -- GENERATED --
                  DATE: Wed Jun 19 09:50:50 UTC 2019
                  SOURCE: /app/app/views/auth/_providerPartial.scala.html
                  HASH: 1e84b6c599f5fe6c189a3fdc9599d036a2ca224e
                  MATRIX: 814->1|1033->82|1061->129|1088->130|1171->186|1199->187|1227->189|1341->276|1369->277|1398->280|1491->346|1519->347|1547->348|1575->349|1603->350|1667->388|1711->423|1756->430|1788->435|1829->449|1864->475|1904->477|1940->486|2018->537|2028->538|2058->547|2096->567|2109->572|2148->573|2189->586|2226->596|2236->597|2266->606|2317->626|2362->644|2408->669|2458->689|2486->690
                  LINES: 27->1|32->1|34->4|35->5|36->6|36->6|37->7|38->8|38->8|39->9|40->10|40->10|41->11|41->11|42->12|44->14|44->14|44->14|45->15|46->16|46->16|46->16|47->17|47->17|47->17|47->17|48->18|48->18|48->18|49->19|49->19|49->19|49->19|50->20|51->21|51->21|53->23|54->24
                  -- GENERATED --
              */
          