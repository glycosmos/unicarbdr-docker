
package views.html.auth

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object _passwordPartial_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class _passwordPartial extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[Form[_$1] forSome { 
   type _$1 >: _root_.scala.Nothing <: _root_.scala.Any
},play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(f: Form[_]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {
import helper._
implicit def /*4.2*/implicitFieldConstructor/*4.26*/ = {{ b3.vertical.fieldConstructor }};
Seq[Any](format.raw/*1.14*/("""

"""),format.raw/*4.62*/("""

"""),format.raw/*6.1*/("""<h4>Choose a password</h4>
"""),_display_(/*7.2*/inputPassword(
f("password"),
'_label -> " ",
'_class -> "search-field"
)),format.raw/*11.2*/("""

"""),format.raw/*13.1*/("""<h4>Repeat chosen password</h4>
"""),_display_(/*14.2*/inputPassword(
f("repeatPassword"),
'_label -> " ",
'_class -> "search-field",
'_showConstraints -> false,
'_error -> f.error("password")
)),format.raw/*20.2*/("""
"""))
      }
    }
  }

  def render(f:Form[_$1] forSome { 
   type _$1 >: _root_.scala.Nothing <: _root_.scala.Any
}): play.twirl.api.HtmlFormat.Appendable = apply(f)

  def f:((Form[_$1] forSome { 
   type _$1 >: _root_.scala.Nothing <: _root_.scala.Any
}) => play.twirl.api.HtmlFormat.Appendable) = (f) => apply(f)

  def ref: this.type = this

}


}

/**/
object _passwordPartial extends _passwordPartial_Scope0._passwordPartial
              /*
                  -- GENERATED --
                  DATE: Wed Jun 19 09:50:50 UTC 2019
                  SOURCE: /app/app/views/auth/_passwordPartial.scala.html
                  HASH: 8d1a0c4e8535678a69277d5e711c20d33921d6c0
                  MATRIX: 844->1|958->33|990->57|1056->13|1085->93|1113->95|1166->123|1259->196|1288->198|1347->231|1506->370
                  LINES: 29->1|33->4|33->4|34->1|36->4|38->6|39->7|43->11|45->13|46->14|52->20
                  -- GENERATED --
              */
          