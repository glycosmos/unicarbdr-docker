
package views.html.auth

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object login_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

     object login_Scope1 {
import service.UserProvider

class login extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template3[com.feth.play.module.pa.PlayAuthenticate,UserProvider,Form[_$1] forSome { 
   type _$1 >: _root_.scala.Nothing <: _root_.scala.Any
},play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*2.2*/(playAuth: com.feth.play.module.pa.PlayAuthenticate, userProvider: UserProvider, loginForm: Form[_]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {
import helper._
import com.feth.play.module.pa.views.html._
implicit def /*5.2*/implicitFieldConstructor/*5.26*/ = {{ b3.vertical.fieldConstructor }};
Seq[Any](format.raw/*2.102*/("""

"""),format.raw/*5.62*/("""
"""),format.raw/*7.1*/("""
"""),_display_(/*8.2*/main(userProvider, "UniCarb-DR Login")/*8.40*/{_display_(Seq[Any](format.raw/*8.41*/("""

"""),format.raw/*10.1*/("""<div class="container-fluid content">
    <ul class="breadcrumb">
        <li><i class="icon-home" ></i><a href="/"> UniCarb-DR</a> <span class="divider">></span></li>
        <li class="active"><i class="icon-download" ></i> Login<span class="divider"></span></li>
    </ul>


    <div class="row-fluid info">
        <div class="row-fluid">
            <div class="page-header row-fluid">
                <h1 class="subheader span8">Login</h1>
            </div>
            <p>Fill in information below</p>

            <div class="col-xs-6 col-sm-4">
                """),format.raw/*25.53*/("""
                """),_display_(/*26.18*/helper/*26.24*/.form(routes.HomeController.doLogin, 'class -> "form-horizontal", 'role -> "form")/*26.106*/ {_display_(Seq[Any](format.raw/*26.108*/("""

                """),_display_(/*28.18*/if(loginForm.hasGlobalErrors)/*28.47*/ {_display_(Seq[Any](format.raw/*28.49*/("""
                """),format.raw/*29.17*/("""<p class="alert alert-danger">
                    """),_display_(/*30.22*/loginForm/*30.31*/.globalError.message),format.raw/*30.51*/("""
                """),format.raw/*31.17*/("""</p>
                """)))}),format.raw/*32.18*/("""


                """),_display_(/*35.18*/_emailPartial(loginForm)),format.raw/*35.42*/("""

                """),format.raw/*37.17*/("""<h4>Password</h4>
                """),_display_(/*38.18*/inputPassword(
                    loginForm("password"),
                    '_showConstraints -> false,
                    '_label -> "",
                    '_class -> "search-field"
                )),format.raw/*43.18*/("""


                """),format.raw/*46.17*/("""<div class="row-fluid">
                    """),_display_(/*47.22*/b3/*47.24*/.free()/*47.31*/{_display_(Seq[Any](format.raw/*47.32*/("""
                    """),format.raw/*48.21*/("""<!--                            <div class="g-recaptcha" data-sitekey=""""),_display_(/*48.93*/play/*48.97*/.Play.application().configuration().getString("play-authenticate.gcaptcha.gsiteKey")),format.raw/*48.181*/(""""></div>-->
                    <br />
                    <button type="submit" class="btn btn-primary">Login now</button>
                    """)))}),format.raw/*51.22*/("""
                """),format.raw/*52.17*/("""</div>

                """)))}),format.raw/*54.18*/("""
                """),format.raw/*55.17*/("""<br><br>
                """),_display_(/*56.18*/b3/*56.20*/.form(routes.Signup.doForgotPassword)/*56.57*/ {_display_(Seq[Any](format.raw/*56.59*/("""
                    """),_display_(/*57.22*/b3/*57.24*/.free()/*57.31*/{_display_(Seq[Any](format.raw/*57.32*/("""
                        """),format.raw/*58.25*/("""<input type="submit" value="Forgot your password?" class="btn btn-primary u-form-group">
                    """)))}),format.raw/*59.22*/("""
                """)))}),format.raw/*60.18*/("""

            """),format.raw/*62.13*/("""</div>


        </div>
    </div>
</div>



""")))}),format.raw/*71.2*/("""
"""))
      }
    }
  }

  def render(playAuth:com.feth.play.module.pa.PlayAuthenticate,userProvider:UserProvider,loginForm:Form[_$1] forSome { 
   type _$1 >: _root_.scala.Nothing <: _root_.scala.Any
}): play.twirl.api.HtmlFormat.Appendable = apply(playAuth,userProvider,loginForm)

  def f:((com.feth.play.module.pa.PlayAuthenticate,UserProvider,Form[_$1] forSome { 
   type _$1 >: _root_.scala.Nothing <: _root_.scala.Any
}) => play.twirl.api.HtmlFormat.Appendable) = (playAuth,userProvider,loginForm) => apply(playAuth,userProvider,loginForm)

  def ref: this.type = this

}


}
}

/**/
object login extends login_Scope0.login_Scope1.login
              /*
                  -- GENERATED --
                  DATE: Wed Jun 19 09:50:50 UTC 2019
                  SOURCE: /app/app/views/auth/login.scala.html
                  HASH: aac377375a73499534c94bb340aff3206354a95b
                  MATRIX: 932->30|1178->150|1210->174|1277->130|1306->210|1333->256|1360->258|1406->296|1444->297|1473->299|2072->906|2117->924|2132->930|2224->1012|2265->1014|2311->1033|2349->1062|2389->1064|2434->1081|2513->1133|2531->1142|2572->1162|2617->1179|2670->1201|2717->1221|2762->1245|2808->1263|2870->1298|3095->1502|3142->1521|3214->1566|3225->1568|3241->1575|3280->1576|3329->1597|3428->1669|3441->1673|3547->1757|3723->1902|3768->1919|3824->1944|3869->1961|3922->1987|3933->1989|3979->2026|4019->2028|4068->2050|4079->2052|4095->2059|4134->2060|4187->2085|4328->2195|4377->2213|4419->2227|4495->2273
                  LINES: 32->2|37->5|37->5|38->2|40->5|41->7|42->8|42->8|42->8|44->10|59->25|60->26|60->26|60->26|60->26|62->28|62->28|62->28|63->29|64->30|64->30|64->30|65->31|66->32|69->35|69->35|71->37|72->38|77->43|80->46|81->47|81->47|81->47|81->47|82->48|82->48|82->48|82->48|85->51|86->52|88->54|89->55|90->56|90->56|90->56|90->56|91->57|91->57|91->57|91->57|92->58|93->59|94->60|96->62|105->71
                  -- GENERATED --
              */
          