
package views.html.search

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object searchIndex_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

     object searchIndex_Scope1 {
import service.UserProvider

class searchIndex extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template8[Map[String, String],Map[String, String],Map[String, String],Map[String, String],List[String],Form[DBviews.vAdvancedSearch],Form[SearchMSMSForm],UserProvider,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*2.2*/(  persubs: Map[String,String],
    redEnd: Map[String,String],
    taxon: Map[String,String],
    tissue: Map[String,String],
    refs: List[String],
    frAdvanced: Form[DBviews.vAdvancedSearch],
    frMSMS: Form[SearchMSMSForm],
    userProvider: UserProvider
):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*10.2*/("""


"""),_display_(/*13.2*/main(userProvider, "MIRAGE search")/*13.37*/{_display_(Seq[Any](format.raw/*13.38*/("""

    """),format.raw/*15.5*/("""<link rel="stylesheet" type="text/css" media="screen" href=""""),_display_(/*15.66*/routes/*15.72*/.Assets.versioned("stylesheets/select2.css")),format.raw/*15.116*/("""">
    <link rel="stylesheet" type="text/css" media="screen" href=""""),_display_(/*16.66*/routes/*16.72*/.Assets.versioned("stylesheets/select2-bootstrap.css")),format.raw/*16.126*/("""">
    <script src=""""),_display_(/*17.19*/routes/*17.25*/.Assets.versioned("javascripts/select2.js")),format.raw/*17.68*/(""""></script>

<script>
    $("#searchLink").addClass("active");
</script>

<div class="container-fluid content">
    <ul class="breadcrumb">
        <li><i class="icon-home" ></i><a href="/"> UniCarb-DR</a> <span class="divider">></span></li>
        <li class="active"><i class="icon-search" ></i> Search<span class="divider"></span></li>
    </ul>


    <section id="structureLayout">
        <section id="layouts">

            <div class="page-header row-fluid">
<!--                <h1>Search UniCarb-DR</h1>-->
                <h1>Search UniCarb-DR</h1>
                <h4 class="pull-left">Retrieve records from UniCarb-DR</h4>
                <!--<h4 class="pull-left">Retrieve records from UniCarb-DR</h4>-->
            </div>


            <div class="row-fluid">
                <div class="span3 search">
                    <div class="row-fluid">

                        <div class="filterbar tabbable clearfix">

                            <ul class="nav nav-tabs" id="myTabBar">
                                <li class="title">Select a query: </li>
                                <li class="active"><a href="#basic" data-toggle="tab">Basic Search<span class="pull-right count"></span></a></li>
                                <li><a href="#advanced" data-toggle="tab">Advanced Search<span class="pull-right count"></span></a></li>
                                <li><a href="#msms" data-toggle="tab">MS/MS<span class="pull-right count"></span></a></li>
                            </ul>
                        </div>
                    </div>
                </div>


                <div class="span9 rightpanel">
                    <div class="tabbable"> <!-- Only required for left/right tabs -->
                        <div class="tab-pane" id="tab1">
                            <div id="basic" class="control-group row-fluid glycosuitedb active">
                                <div id="tab7" class="span12" >
                                   """),_display_(/*63.37*/views/*63.42*/.html.search.searchMultiCriteria(tissue, taxon, refs, frAdvanced)),format.raw/*63.107*/("""
                                 """),format.raw/*64.34*/("""</div>
                            </div>

                            <div id="advanced" class="tab-pane control-group row-fluid basic">
                                <div id="tab1" class="span12" >

                                    """),_display_(/*70.38*/views/*70.43*/.html.search.searchAdvanced(frAdvanced, persubs, redEnd, tissue, taxon)),format.raw/*70.114*/("""
                                """),format.raw/*71.33*/("""</div>
                            </div>

                            <div id="msms" class="tab-pane control-group row-fluid basic">
                                <div id="tab3" class="span12" >
                                    """),_display_(/*76.38*/views/*76.43*/.html.search.searchMSMS(frMSMS)),format.raw/*76.74*/("""
                                """),format.raw/*77.33*/("""</div>
                            </div>


                        </div>
                    </div>

                    <div class="tab-content results">
                        """),format.raw/*85.44*/("""
                        """),format.raw/*86.42*/("""
                    """),format.raw/*87.21*/("""</div>

                    <script>
                        // Javascript to enable link to tab
                        var hash = document.location.hash;
                        var prefix = "tab_";
                        if (hash) """),format.raw/*93.35*/("""{"""),format.raw/*93.36*/("""
                            """),format.raw/*94.29*/("""$('.nav-tabs a[href="'+hash.replace(prefix,"")+'"]').tab('show');
                        """),format.raw/*95.25*/("""}"""),format.raw/*95.26*/("""

                        """),format.raw/*97.25*/("""// Change hash for page-reload
                        $('.nav-tabs a').on('shown', function (e) """),format.raw/*98.67*/("""{"""),format.raw/*98.68*/("""
                            """),format.raw/*99.29*/("""window.location.hash = e.target.hash.replace("#", "#" + prefix);
                        """),format.raw/*100.25*/("""}"""),format.raw/*100.26*/(""");
                    </script>

                </div>
            </div>
        </section>
    </section>
</div>





""")))}),format.raw/*113.2*/("""
"""))
      }
    }
  }

  def render(persubs:Map[String, String],redEnd:Map[String, String],taxon:Map[String, String],tissue:Map[String, String],refs:List[String],frAdvanced:Form[DBviews.vAdvancedSearch],frMSMS:Form[SearchMSMSForm],userProvider:UserProvider): play.twirl.api.HtmlFormat.Appendable = apply(persubs,redEnd,taxon,tissue,refs,frAdvanced,frMSMS,userProvider)

  def f:((Map[String, String],Map[String, String],Map[String, String],Map[String, String],List[String],Form[DBviews.vAdvancedSearch],Form[SearchMSMSForm],UserProvider) => play.twirl.api.HtmlFormat.Appendable) = (persubs,redEnd,taxon,tissue,refs,frAdvanced,frMSMS,userProvider) => apply(persubs,redEnd,taxon,tissue,refs,frAdvanced,frMSMS,userProvider)

  def ref: this.type = this

}


}
}

/**/
object searchIndex extends searchIndex_Scope0.searchIndex_Scope1.searchIndex
              /*
                  -- GENERATED --
                  DATE: Wed Jun 19 09:50:51 UTC 2019
                  SOURCE: /app/app/views/search/searchIndex.scala.html
                  HASH: 03826cd140317f9ba29488a80a787061cd682334
                  MATRIX: 976->30|1335->294|1365->298|1409->333|1448->334|1481->340|1569->401|1584->407|1650->451|1745->519|1760->525|1836->579|1884->600|1899->606|1963->649|3970->2629|3984->2634|4071->2699|4133->2733|4400->2973|4414->2978|4507->3049|4568->3082|4830->3317|4844->3322|4896->3353|4957->3386|5166->3586|5219->3628|5268->3649|5531->3884|5560->3885|5617->3914|5735->4004|5764->4005|5818->4031|5943->4128|5972->4129|6029->4158|6147->4247|6177->4248|6331->4371
                  LINES: 30->2|43->10|46->13|46->13|46->13|48->15|48->15|48->15|48->15|49->16|49->16|49->16|50->17|50->17|50->17|96->63|96->63|96->63|97->64|103->70|103->70|103->70|104->71|109->76|109->76|109->76|110->77|118->85|119->86|120->87|126->93|126->93|127->94|128->95|128->95|130->97|131->98|131->98|132->99|133->100|133->100|146->113
                  -- GENERATED --
              */
          