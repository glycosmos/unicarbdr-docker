
package views.html.search

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object searchComposition_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class searchComposition extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template3[Form[DBviews.vAdvancedSearch],Map[String, String],Map[String, String],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(frAdvanced: Form[DBviews.vAdvancedSearch], persubs: Map[String,String], redEnd: Map[String,String]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {
import helper._
implicit def /*4.2*/implicitFieldConstructor/*4.26*/ = {{ b3.horizontal.fieldConstructor("col-md-8", "col-md-4") }};
Seq[Any](format.raw/*1.102*/("""

"""),format.raw/*4.88*/("""



"""),_display_(/*8.2*/if(frAdvanced.hasGlobalErrors)/*8.32*/ {_display_(Seq[Any](format.raw/*8.34*/("""
"""),format.raw/*9.1*/("""<p class="error">
    """),_display_(/*10.6*/for(error <- frAdvanced.globalErrors) yield /*10.43*/ {_display_(Seq[Any](format.raw/*10.45*/("""
"""),format.raw/*11.1*/("""<p class="coral">"""),_display_(/*11.19*/Messages(error.messages, error.arguments.toArray: _*)),format.raw/*11.72*/("""</p>
""")))}),format.raw/*12.2*/("""
"""),format.raw/*13.1*/("""</p>
""")))}),format.raw/*14.2*/("""






"""),format.raw/*21.1*/("""<div class="composition-div searchdiv">
    <h4>Mass</h4>
    <div class="mass-div">
        """),_display_(/*24.10*/b3/*24.12*/.number(frAdvanced("mass"),
        'min -> 0,
        'step -> "any",
        '_class -> "search-field short",
        'placeholder -> "Mass")),format.raw/*28.32*/("""

        """),_display_(/*30.10*/b3/*30.12*/.radio(frAdvanced("massType"), '_class -> "search-radio")/*30.69*/{ implicit extraInfo =>_display_(Seq[Any](format.raw/*30.92*/("""
        """),_display_(/*31.10*/b3/*31.12*/.radioOption("r", Html("<span>Recorded</span>"), 'checked -> true)),format.raw/*31.78*/("""
        """),_display_(/*32.10*/b3/*32.12*/.radioOption("c", Html("<span>Calculated (Monoisotopic, underivatised, uncharged)</span>"))),format.raw/*32.103*/("""
        """)))}),format.raw/*33.10*/("""

        """),format.raw/*35.9*/("""<script>
            $("input[name='massType']").on('change', function() """),format.raw/*36.65*/("""{"""),format.raw/*36.66*/("""
                """),format.raw/*37.17*/("""var val = $(this).val();
                $("#persId").prop("disabled", val != "r");
                $("#redEndId").prop("disabled", val != "r");

            """),format.raw/*41.13*/("""}"""),format.raw/*41.14*/(""");
        </script>

    </div>
    <div class="error-div">
        """),_display_(/*46.10*/b3/*46.12*/.number(frAdvanced("error"),
        'min -> 0,
        'step -> "any",
        '_class -> "search-field short",
        'placeholder -> "Error")),format.raw/*50.33*/("""

        """),_display_(/*52.10*/b3/*52.12*/.radio(frAdvanced("errorType"), '_class -> "search-radio")/*52.70*/ { implicit extraInfo =>_display_(Seq[Any](format.raw/*52.94*/("""
        """),_display_(/*53.10*/b3/*53.12*/.radioOption("m", Html("<span>mz</span>"), 'checked -> true)),format.raw/*53.72*/("""
        """),_display_(/*54.10*/b3/*54.12*/.radioOption("p", Html("<span>ppm</span>"))),format.raw/*54.55*/("""
        """),_display_(/*55.10*/b3/*55.12*/.radioOption("p", Html("<span>dalton</span>"))),format.raw/*55.58*/("""
        """)))}),format.raw/*56.10*/("""

    """),format.raw/*58.5*/("""</div>
    <div class="pers-div">
            Persubstitution
            """),_display_(/*61.14*/b3/*61.16*/.select(
            frAdvanced("persId"),
            options(persubs),
            '_default -> "Not specified",
            '_showConstraints -> false,
            'multiple -> false
            )),format.raw/*67.14*/("""

            """),format.raw/*69.13*/("""Reducing End
            """),_display_(/*70.14*/b3/*70.16*/.select(
            frAdvanced("redEndId"),
            options(redEnd),
            '_default -> "Not specified",
            '_showConstraints -> false,
            'multiple -> false
            )),format.raw/*76.14*/("""

    """),format.raw/*78.5*/("""</div>
    <div class="charge-div">
        Charge
        """),_display_(/*81.10*/b3/*81.12*/.number(frAdvanced("charge"), '_class -> "hidden-charge-input")),format.raw/*81.75*/("""
        """),_display_(/*82.10*/b3/*82.12*/.text(frAdvanced("chargeMode"), '_class -> "hidden-charge-input")),format.raw/*82.77*/("""

        """),format.raw/*84.9*/("""<div id="chargeContainer">
            <!-- TODO: Read limits of this slider in from database if possible -->
            <output id="chargeDisplay">Not specified</output>
            <input type="range" max="2" min="-3" id="chargeSlider"/>
        </div>
        <script>
        $(document).on('input', '#chargeSlider', function() """),format.raw/*90.61*/("""{"""),format.raw/*90.62*/("""
            """),format.raw/*91.13*/("""var value = $(this).val();
            if(value > 0) """),format.raw/*92.27*/("""{"""),format.raw/*92.28*/("""
                """),format.raw/*93.17*/("""$('#chargeMode').val("+");
                $('#chargeDisplay').val("+" + value);
            """),format.raw/*95.13*/("""}"""),format.raw/*95.14*/(""" """),format.raw/*95.15*/("""else if(value < 0 ) """),format.raw/*95.35*/("""{"""),format.raw/*95.36*/("""
                """),format.raw/*96.17*/("""$('#chargeMode').val("-");
                $('#chargeDisplay').val(value);
            """),format.raw/*98.13*/("""}"""),format.raw/*98.14*/(""" """),format.raw/*98.15*/("""else """),format.raw/*98.20*/("""{"""),format.raw/*98.21*/("""
                """),format.raw/*99.17*/("""$('#chargeMode').val( "" );
                $('#chargeDisplay').val("Not specified");
            """),format.raw/*101.13*/("""}"""),format.raw/*101.14*/("""
            """),format.raw/*102.13*/("""$('#charge').val( value );
        """),format.raw/*103.9*/("""}"""),format.raw/*103.10*/(""");
        </script>


    </div>

</div>


























    <div class="composition-div searchdiv">
        <h4>Composition</h4>
        <div class="hex-div">
            <h5>Hex</h5>
            """),_display_(/*140.14*/b3/*140.16*/.radio( frAdvanced("selHex"),
                '_class -> "search-radio" )/*141.44*/ { implicit extraInfo =>_display_(Seq[Any](format.raw/*141.68*/("""
                    """),_display_(/*142.22*/b3/*142.24*/.radioOption("p", Html("<span>Include</span>"), 'checked -> true)),format.raw/*142.89*/("""
                    """),_display_(/*143.22*/b3/*143.24*/.radioOption("n", Html("<span>Exclude</span>"))),format.raw/*143.71*/("""
                    """),_display_(/*144.22*/b3/*144.24*/.radioOption("y", Html("<span>Include within range</span>"))),format.raw/*144.84*/("""
            """)))}),format.raw/*145.14*/("""
            """),format.raw/*146.13*/("""<div class="range-picker">
                """),_display_(/*147.18*/b3/*147.20*/.number(frAdvanced("hex"),
                    '_class -> "range-input range-input-start",
                    'min -> 0,
                    'disabled -> true)),format.raw/*150.39*/("""
                """),format.raw/*151.17*/("""<span class="range-input-to">to</span>
                """),_display_(/*152.18*/b3/*152.20*/.number(frAdvanced("hex2"),
                    '_class -> "range-input range-input-end",
                    'min -> 0,
                    'disabled -> true)),format.raw/*155.39*/("""
            """),format.raw/*156.13*/("""</div>

            <script>
            $("input[name='selHex']").on('change', function() """),format.raw/*159.63*/("""{"""),format.raw/*159.64*/("""
                """),format.raw/*160.17*/("""var val = $(this).val();
                $("#hex").prop("disabled", val != "y");
                $("#hex2").prop("disabled", val != "y");

                if(val == "y")"""),format.raw/*164.31*/("""{"""),format.raw/*164.32*/("""
                    """),format.raw/*165.21*/("""$(".hex-div .range-input-to").addClass("to-active");
                """),format.raw/*166.17*/("""}"""),format.raw/*166.18*/(""" """),format.raw/*166.19*/("""else """),format.raw/*166.24*/("""{"""),format.raw/*166.25*/("""
                    """),format.raw/*167.21*/("""$(".hex-div .range-input-to").removeClass("to-active");
                """),format.raw/*168.17*/("""}"""),format.raw/*168.18*/("""
            """),format.raw/*169.13*/("""}"""),format.raw/*169.14*/(""");
            </script>
        </div>

        <div class="dhex-div">
            <h5>dHex</h5>
            """),_display_(/*175.14*/b3/*175.16*/.radio( frAdvanced("selDHex"),
                '_class -> "search-radio" )/*176.44*/ { implicit extraInfo =>_display_(Seq[Any](format.raw/*176.68*/("""
                    """),_display_(/*177.22*/b3/*177.24*/.radioOption("p", Html("<span>Include</span>"), 'checked -> true)),format.raw/*177.89*/("""
                    """),_display_(/*178.22*/b3/*178.24*/.radioOption("n", Html("<span>Exclude</span>"))),format.raw/*178.71*/("""
                    """),_display_(/*179.22*/b3/*179.24*/.radioOption("y", Html("<span>Include within range</span>"))),format.raw/*179.84*/("""
                """)))}),format.raw/*180.18*/("""
            """),format.raw/*181.13*/("""<div class="range-picker">
                """),_display_(/*182.18*/b3/*182.20*/.number(frAdvanced("dhex"),
                    '_class -> "range-input range-input-start",
                    'min -> 0,
                    'disabled -> true)),format.raw/*185.39*/("""
                """),format.raw/*186.17*/("""<span class="range-input-to">to</span>
                """),_display_(/*187.18*/b3/*187.20*/.number(frAdvanced("dhex2"),
                    '_class -> "range-input range-input-end",
                    'min -> 0,
                    'disabled -> true)),format.raw/*190.39*/("""
            """),format.raw/*191.13*/("""</div>
            <script>
            $("input[name='selDHex']").on('change', function() """),format.raw/*193.64*/("""{"""),format.raw/*193.65*/("""
                """),format.raw/*194.17*/("""var val = $(this).val();
                $("#dhex").prop("disabled", $(this).val() != "y");
                $("#dhex2").prop("disabled", $(this).val() != "y");

                if(val == "y")"""),format.raw/*198.31*/("""{"""),format.raw/*198.32*/("""
                    """),format.raw/*199.21*/("""$(".dhex-div .range-input-to").addClass("to-active");
                """),format.raw/*200.17*/("""}"""),format.raw/*200.18*/(""" """),format.raw/*200.19*/("""else """),format.raw/*200.24*/("""{"""),format.raw/*200.25*/("""
                    """),format.raw/*201.21*/("""$(".dhex-div .range-input-to").removeClass("to-active");
                """),format.raw/*202.17*/("""}"""),format.raw/*202.18*/("""
            """),format.raw/*203.13*/("""}"""),format.raw/*203.14*/(""");
            </script>
        </div>

        <div class="hexnac-div">
            <h5>HexNAc</h5>
            """),_display_(/*209.14*/b3/*209.16*/.radio( frAdvanced("selHexNac"),
                '_class -> "search-radio" )/*210.44*/ { implicit extraInfo =>_display_(Seq[Any](format.raw/*210.68*/("""
                    """),_display_(/*211.22*/b3/*211.24*/.radioOption("p", Html("<span>Include</span>"), 'checked -> true)),format.raw/*211.89*/("""
                    """),_display_(/*212.22*/b3/*212.24*/.radioOption("n", Html("<span>Exclude</span>"))),format.raw/*212.71*/("""
                    """),_display_(/*213.22*/b3/*213.24*/.radioOption("y", Html("<span>Include within range</span>"))),format.raw/*213.84*/("""
            """)))}),format.raw/*214.14*/("""
            """),format.raw/*215.13*/("""<div class="range-picker">
                """),_display_(/*216.18*/b3/*216.20*/.number(frAdvanced("hexnac"),
                    '_class -> "range-input range-input-start",
                    'min -> 0,
                    'disabled -> true)),format.raw/*219.39*/("""
                """),format.raw/*220.17*/("""<span class="range-input-to">to</span>
                """),_display_(/*221.18*/b3/*221.20*/.number(frAdvanced("hexnac2"),
                    '_class -> "range-input range-input-end",
                    'min -> 0,
                    'disabled -> true)),format.raw/*224.39*/("""
            """),format.raw/*225.13*/("""</div>

            <script>
            $("input[name='selHexNac']").on('change', function() """),format.raw/*228.66*/("""{"""),format.raw/*228.67*/("""
                """),format.raw/*229.17*/("""var val = $(this).val();
                $("#hexnac").prop("disabled", $(this).val() != "y");
                $("#hexnac2").prop("disabled", $(this).val() != "y");

                if(val == "y")"""),format.raw/*233.31*/("""{"""),format.raw/*233.32*/("""
                    """),format.raw/*234.21*/("""$(".hexnac-div .range-input-to").addClass("to-active");
                """),format.raw/*235.17*/("""}"""),format.raw/*235.18*/(""" """),format.raw/*235.19*/("""else """),format.raw/*235.24*/("""{"""),format.raw/*235.25*/("""
                    """),format.raw/*236.21*/("""$(".hexnac-div .range-input-to").removeClass("to-active");
                """),format.raw/*237.17*/("""}"""),format.raw/*237.18*/("""
            """),format.raw/*238.13*/("""}"""),format.raw/*238.14*/(""");
            </script>
        </div>

        <div class="neuac-div">
            <h5>NeuAc</h5>
            """),_display_(/*244.14*/b3/*244.16*/.radio( frAdvanced("selNeuAc"),
                '_class -> "search-radio" )/*245.44*/ { implicit extraInfo =>_display_(Seq[Any](format.raw/*245.68*/("""
                    """),_display_(/*246.22*/b3/*246.24*/.radioOption("p", Html("<span>Include</span>"), 'checked -> true)),format.raw/*246.89*/("""
                    """),_display_(/*247.22*/b3/*247.24*/.radioOption("n", Html("<span>Exclude</span>"))),format.raw/*247.71*/("""
                    """),_display_(/*248.22*/b3/*248.24*/.radioOption("y", Html("<span>Include within range</span>"))),format.raw/*248.84*/("""
            """)))}),format.raw/*249.14*/("""
            """),format.raw/*250.13*/("""<div class="range-picker">
                """),_display_(/*251.18*/b3/*251.20*/.number(frAdvanced("neuAc"),
                    '_class -> "range-input range-input-start",
                    'min -> 0,
                    'disabled -> true)),format.raw/*254.39*/("""
                """),format.raw/*255.17*/("""<span class="range-input-to">to</span>
                """),_display_(/*256.18*/b3/*256.20*/.number(frAdvanced("neuAc2"),
                    '_class -> "range-input range-input-end",
                    'min -> 0,
                    'disabled -> true)),format.raw/*259.39*/("""
            """),format.raw/*260.13*/("""</div>

            <script>
            $("input[name='selNeuAc']").on('change', function() """),format.raw/*263.65*/("""{"""),format.raw/*263.66*/("""
                """),format.raw/*264.17*/("""var val = $(this).val();
                $("#neuAc").prop("disabled", $(this).val() != "y");
                $("#neuAc2").prop("disabled", $(this).val() != "y");

                if(val == "y")"""),format.raw/*268.31*/("""{"""),format.raw/*268.32*/("""
                    """),format.raw/*269.21*/("""$(".neuac-div .range-input-to").addClass("to-active");
                """),format.raw/*270.17*/("""}"""),format.raw/*270.18*/(""" """),format.raw/*270.19*/("""else """),format.raw/*270.24*/("""{"""),format.raw/*270.25*/("""
                    """),format.raw/*271.21*/("""$(".neuac-div .range-input-to").removeClass("to-active");
                """),format.raw/*272.17*/("""}"""),format.raw/*272.18*/("""
            """),format.raw/*273.13*/("""}"""),format.raw/*273.14*/(""");
            </script>
        </div>

        <div class="neugc-div">
            <h5>NeuGc</h5>
                """),_display_(/*279.18*/b3/*279.20*/.radio( frAdvanced("selNeuGc"),
                '_class -> "search-radio" )/*280.44*/ { implicit extraInfo =>_display_(Seq[Any](format.raw/*280.68*/("""
                    """),_display_(/*281.22*/b3/*281.24*/.radioOption("p", Html("<span>Include</span>"), 'checked -> true)),format.raw/*281.89*/("""
                    """),_display_(/*282.22*/b3/*282.24*/.radioOption("n", Html("<span>Exclude</span>"))),format.raw/*282.71*/("""
                    """),_display_(/*283.22*/b3/*283.24*/.radioOption("y", Html("<span>Include within range</span>"))),format.raw/*283.84*/("""
            """)))}),format.raw/*284.14*/("""
            """),format.raw/*285.13*/("""<div class="range-picker">
                """),_display_(/*286.18*/b3/*286.20*/.number(frAdvanced("neuGc"),
                    '_class -> "range-input range-input-start",
                    'min -> 0,
                    'disabled -> true)),format.raw/*289.39*/("""
                """),format.raw/*290.17*/("""<span class="range-input-to">to</span>
                """),_display_(/*291.18*/b3/*291.20*/.number(frAdvanced("neuGc2"),
                    '_class -> "range-input range-input-end",
                    'min -> 0,
                    'disabled -> true)),format.raw/*294.39*/("""
            """),format.raw/*295.13*/("""</div>
            <script>
                $("input[name='selNeuGc']").on('change', function() """),format.raw/*297.69*/("""{"""),format.raw/*297.70*/("""
                    """),format.raw/*298.21*/("""var val = $(this).val();
                    $("#neuGc").prop("disabled", $(this).val() != "y");
                    $("#neuGc2").prop("disabled", $(this).val() != "y");

                    if(val == "y")"""),format.raw/*302.35*/("""{"""),format.raw/*302.36*/("""
                        """),format.raw/*303.25*/("""$(".neugc-div .range-input-to").addClass("to-active");
                    """),format.raw/*304.21*/("""}"""),format.raw/*304.22*/(""" """),format.raw/*304.23*/("""else """),format.raw/*304.28*/("""{"""),format.raw/*304.29*/("""
                        """),format.raw/*305.25*/("""$(".neugc-div .range-input-to").removeClass("to-active");
                    """),format.raw/*306.21*/("""}"""),format.raw/*306.22*/("""
                """),format.raw/*307.17*/("""}"""),format.raw/*307.18*/(""");
            </script>
        </div>
    </div>

    """),format.raw/*317.9*/("""

"""))
      }
    }
  }

  def render(frAdvanced:Form[DBviews.vAdvancedSearch],persubs:Map[String, String],redEnd:Map[String, String]): play.twirl.api.HtmlFormat.Appendable = apply(frAdvanced,persubs,redEnd)

  def f:((Form[DBviews.vAdvancedSearch],Map[String, String],Map[String, String]) => play.twirl.api.HtmlFormat.Appendable) = (frAdvanced,persubs,redEnd) => apply(frAdvanced,persubs,redEnd)

  def ref: this.type = this

}


}

/**/
object searchComposition extends searchComposition_Scope0.searchComposition
              /*
                  -- GENERATED --
                  DATE: Wed Jun 19 09:50:51 UTC 2019
                  SOURCE: /app/app/views/search/searchComposition.scala.html
                  HASH: b6ac1852dbc32b41ff345493566588230ba3dc02
                  MATRIX: 839->1|1041->121|1073->145|1166->101|1195->207|1225->212|1263->242|1302->244|1329->245|1378->268|1431->305|1471->307|1499->308|1544->326|1618->379|1654->385|1682->386|1718->392|1752->399|1873->493|1884->495|2048->638|2086->649|2097->651|2163->708|2224->731|2261->741|2272->743|2359->809|2396->819|2407->821|2520->912|2561->922|2598->932|2699->1005|2728->1006|2773->1023|2959->1181|2988->1182|3085->1252|3096->1254|3262->1399|3300->1410|3311->1412|3378->1470|3440->1494|3477->1504|3488->1506|3569->1566|3606->1576|3617->1578|3681->1621|3718->1631|3729->1633|3796->1679|3837->1689|3870->1695|3972->1770|3983->1772|4203->1971|4245->1985|4298->2011|4309->2013|4530->2213|4563->2219|4650->2279|4661->2281|4745->2344|4782->2354|4793->2356|4879->2421|4916->2431|5277->2764|5306->2765|5347->2778|5428->2831|5457->2832|5502->2849|5623->2942|5652->2943|5681->2944|5729->2964|5758->2965|5803->2982|5918->3069|5947->3070|5976->3071|6009->3076|6038->3077|6083->3094|6210->3192|6240->3193|6282->3206|6345->3241|6375->3242|6612->3451|6624->3453|6707->3526|6770->3550|6820->3572|6832->3574|6919->3639|6969->3661|6981->3663|7050->3710|7100->3732|7112->3734|7194->3794|7240->3808|7282->3821|7354->3865|7366->3867|7548->4027|7594->4044|7678->4100|7690->4102|7871->4261|7913->4274|8033->4365|8063->4366|8109->4383|8307->4552|8337->4553|8387->4574|8485->4643|8515->4644|8545->4645|8579->4650|8609->4651|8659->4672|8760->4744|8790->4745|8832->4758|8862->4759|9001->4870|9013->4872|9097->4946|9160->4970|9210->4992|9222->4994|9309->5059|9359->5081|9371->5083|9440->5130|9490->5152|9502->5154|9584->5214|9634->5232|9676->5245|9748->5289|9760->5291|9943->5452|9989->5469|10073->5525|10085->5527|10267->5687|10309->5700|10429->5791|10459->5792|10505->5809|10725->6000|10755->6001|10805->6022|10904->6092|10934->6093|10964->6094|10998->6099|11028->6100|11078->6121|11180->6194|11210->6195|11252->6208|11282->6209|11425->6324|11437->6326|11523->6402|11586->6426|11636->6448|11648->6450|11735->6515|11785->6537|11797->6539|11866->6586|11916->6608|11928->6610|12010->6670|12056->6684|12098->6697|12170->6741|12182->6743|12367->6906|12413->6923|12497->6979|12509->6981|12693->7143|12735->7156|12858->7250|12888->7251|12934->7268|13158->7463|13188->7464|13238->7485|13339->7557|13369->7558|13399->7559|13433->7564|13463->7565|13513->7586|13617->7661|13647->7662|13689->7675|13719->7676|13860->7789|13872->7791|13957->7866|14020->7890|14070->7912|14082->7914|14169->7979|14219->8001|14231->8003|14300->8050|14350->8072|14362->8074|14444->8134|14490->8148|14532->8161|14604->8205|14616->8207|14800->8369|14846->8386|14930->8442|14942->8444|15125->8605|15167->8618|15289->8711|15319->8712|15365->8729|15587->8922|15617->8923|15667->8944|15767->9015|15797->9016|15827->9017|15861->9022|15891->9023|15941->9044|16044->9118|16074->9119|16116->9132|16146->9133|16291->9250|16303->9252|16388->9327|16451->9351|16501->9373|16513->9375|16600->9440|16650->9462|16662->9464|16731->9511|16781->9533|16793->9535|16875->9595|16921->9609|16963->9622|17035->9666|17047->9668|17231->9830|17277->9847|17361->9903|17373->9905|17556->10066|17598->10079|17723->10175|17753->10176|17803->10197|18037->10402|18067->10403|18121->10428|18225->10503|18255->10504|18285->10505|18319->10510|18349->10511|18403->10536|18510->10614|18540->10615|18586->10632|18616->10633|18700->10865
                  LINES: 27->1|31->4|31->4|32->1|34->4|38->8|38->8|38->8|39->9|40->10|40->10|40->10|41->11|41->11|41->11|42->12|43->13|44->14|51->21|54->24|54->24|58->28|60->30|60->30|60->30|60->30|61->31|61->31|61->31|62->32|62->32|62->32|63->33|65->35|66->36|66->36|67->37|71->41|71->41|76->46|76->46|80->50|82->52|82->52|82->52|82->52|83->53|83->53|83->53|84->54|84->54|84->54|85->55|85->55|85->55|86->56|88->58|91->61|91->61|97->67|99->69|100->70|100->70|106->76|108->78|111->81|111->81|111->81|112->82|112->82|112->82|114->84|120->90|120->90|121->91|122->92|122->92|123->93|125->95|125->95|125->95|125->95|125->95|126->96|128->98|128->98|128->98|128->98|128->98|129->99|131->101|131->101|132->102|133->103|133->103|170->140|170->140|171->141|171->141|172->142|172->142|172->142|173->143|173->143|173->143|174->144|174->144|174->144|175->145|176->146|177->147|177->147|180->150|181->151|182->152|182->152|185->155|186->156|189->159|189->159|190->160|194->164|194->164|195->165|196->166|196->166|196->166|196->166|196->166|197->167|198->168|198->168|199->169|199->169|205->175|205->175|206->176|206->176|207->177|207->177|207->177|208->178|208->178|208->178|209->179|209->179|209->179|210->180|211->181|212->182|212->182|215->185|216->186|217->187|217->187|220->190|221->191|223->193|223->193|224->194|228->198|228->198|229->199|230->200|230->200|230->200|230->200|230->200|231->201|232->202|232->202|233->203|233->203|239->209|239->209|240->210|240->210|241->211|241->211|241->211|242->212|242->212|242->212|243->213|243->213|243->213|244->214|245->215|246->216|246->216|249->219|250->220|251->221|251->221|254->224|255->225|258->228|258->228|259->229|263->233|263->233|264->234|265->235|265->235|265->235|265->235|265->235|266->236|267->237|267->237|268->238|268->238|274->244|274->244|275->245|275->245|276->246|276->246|276->246|277->247|277->247|277->247|278->248|278->248|278->248|279->249|280->250|281->251|281->251|284->254|285->255|286->256|286->256|289->259|290->260|293->263|293->263|294->264|298->268|298->268|299->269|300->270|300->270|300->270|300->270|300->270|301->271|302->272|302->272|303->273|303->273|309->279|309->279|310->280|310->280|311->281|311->281|311->281|312->282|312->282|312->282|313->283|313->283|313->283|314->284|315->285|316->286|316->286|319->289|320->290|321->291|321->291|324->294|325->295|327->297|327->297|328->298|332->302|332->302|333->303|334->304|334->304|334->304|334->304|334->304|335->305|336->306|336->306|337->307|337->307|342->317
                  -- GENERATED --
              */
          