
package views.html.search.results

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object resMSMS_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

     object resMSMS_Scope1 {
import service.UserProvider

class resMSMS extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template2[Map[src.PairWrapper, Double],UserProvider,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*2.2*/(resultMap: Map[src.PairWrapper, Double], userProvider: UserProvider):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.71*/("""

"""),_display_(/*4.2*/main(userProvider, "MIRAGE upload")/*4.37*/{_display_(Seq[Any](format.raw/*4.38*/("""

"""),format.raw/*6.1*/("""<div class="container-fluid content">

    <h1>UniCarb-DB Reference Collection</h1>
    """),_display_(/*9.6*/if(resultMap.size() == 0)/*9.31*/ {_display_(Seq[Any](format.raw/*9.33*/("""

    """),format.raw/*11.5*/("""<div class="well">
        <em>There are no results to display</em>
    </div>

    """)))}/*15.7*/else/*15.12*/{_display_(Seq[Any](format.raw/*15.13*/("""


    """),format.raw/*18.5*/("""<div class="accordion" id="accordion2">
        <div class="accordion-group">
            <div class="accordion-heading">
                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
                    Structures matching experimental mass ("""),_display_(/*22.61*/resultMap/*22.70*/.size()),format.raw/*22.77*/(""" """),format.raw/*22.78*/("""results )
                </a>
            </div>
            <div class="accordion-inner">
                <table class="computers table table-striped">
                    <col width="60%"/>
                    <col width="40%"/>
                    <thead>

                    <tr>
                        <td><a><b>Structure</b></a></td>
                        <td><a><b>Score</b></a></td>
                    </tr>
                    </thead>
                    <tbody>

                    """),_display_(/*38.22*/for((key, value) <- resultMap) yield /*38.52*/ {_display_(Seq[Any](format.raw/*38.54*/("""
                    """),format.raw/*39.21*/("""<td><a href="/msData/"""),_display_(/*39.43*/key/*39.46*/.getid()),format.raw/*39.54*/("""">
                        <img class="sugar_image" src="""),_display_(/*40.55*/{routes.GWPController.showImageId(key.getGlycanSeqId(),session.get("notation"),"extended")}),format.raw/*40.146*/(""" """),format.raw/*40.147*/("""height="30%", width="30%" alt=""/>
                    </a>
                    <td>"""),_display_(/*42.26*/("%.2f".format(value))),format.raw/*42.48*/("""</td></tr>
                    """)))}),format.raw/*43.22*/("""

                    """),format.raw/*45.21*/("""</tbody>
                </table>

            </div>

        </div>

    </div>

    """)))}),format.raw/*54.6*/("""

    """),format.raw/*56.5*/("""<a href=""""),_display_(/*56.15*/routes/*56.21*/.SearchController.search()),format.raw/*56.47*/("""">
        <input type="button" value="Search Again" href=""""),_display_(/*57.58*/routes/*57.64*/.SearchController.search()),format.raw/*57.90*/("""" class="btn btn-primary pull-right">
    </a>

</div>

""")))}),format.raw/*62.2*/("""




"""))
      }
    }
  }

  def render(resultMap:Map[src.PairWrapper, Double],userProvider:UserProvider): play.twirl.api.HtmlFormat.Appendable = apply(resultMap,userProvider)

  def f:((Map[src.PairWrapper, Double],UserProvider) => play.twirl.api.HtmlFormat.Appendable) = (resultMap,userProvider) => apply(resultMap,userProvider)

  def ref: this.type = this

}


}
}

/**/
object resMSMS extends resMSMS_Scope0.resMSMS_Scope1.resMSMS
              /*
                  -- GENERATED --
                  DATE: Wed Jun 19 09:50:51 UTC 2019
                  SOURCE: /app/app/views/search/results/resMSMS.scala.html
                  HASH: 3816336328c7551f1c7d095e051ab2d9ad0327f1
                  MATRIX: 857->30|1021->99|1049->102|1092->137|1130->138|1158->140|1272->229|1305->254|1344->256|1377->262|1480->348|1493->353|1532->354|1566->361|1899->667|1917->676|1945->683|1974->684|2502->1185|2548->1215|2588->1217|2637->1238|2686->1260|2698->1263|2727->1271|2811->1328|2924->1419|2954->1420|3066->1505|3109->1527|3172->1559|3222->1581|3340->1669|3373->1675|3410->1685|3425->1691|3472->1717|3559->1777|3574->1783|3621->1809|3708->1866
                  LINES: 30->2|35->2|37->4|37->4|37->4|39->6|42->9|42->9|42->9|44->11|48->15|48->15|48->15|51->18|55->22|55->22|55->22|55->22|71->38|71->38|71->38|72->39|72->39|72->39|72->39|73->40|73->40|73->40|75->42|75->42|76->43|78->45|87->54|89->56|89->56|89->56|89->56|90->57|90->57|90->57|95->62
                  -- GENERATED --
              */
          