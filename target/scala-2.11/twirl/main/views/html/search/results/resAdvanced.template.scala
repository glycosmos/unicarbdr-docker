
package views.html.search.results

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object resAdvanced_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

     object resAdvanced_Scope1 {
import service.UserProvider

class resAdvanced extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template2[List[DBviews.vAdvancedSearch],UserProvider,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*2.2*/(res:List[DBviews.vAdvancedSearch], userProvider: UserProvider):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.65*/("""

"""),_display_(/*4.2*/main(userProvider, "MIRAGE upload")/*4.37*/{_display_(Seq[Any](format.raw/*4.38*/("""

"""),format.raw/*6.1*/("""<div class="container-fluid content">

    <h1>UniCarb-DB Reference Collection</h1>
    """),_display_(/*9.6*/if(res.size() == 0)/*9.25*/ {_display_(Seq[Any](format.raw/*9.27*/("""

    """),format.raw/*11.5*/("""<div class="well">
        <em>There are no results to display</em>
    </div>

    """)))}/*15.7*/else/*15.12*/{_display_(Seq[Any](format.raw/*15.13*/("""


    """),format.raw/*18.5*/("""<div class="accordion" id="accordion2">
        <div class="accordion-group">
            <div class="accordion-heading">
                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
                    Structures matching experimental mass ("""),_display_(/*22.61*/res/*22.64*/.size()),format.raw/*22.71*/(""" """),format.raw/*22.72*/("""results )
                </a>
            </div>
            <div class="accordion-inner">
                <table class="computers table table-striped">
                    <col width="30%"/>
                    <col width="10%"/>
                    <col width="10%"/>
                    <col width="15%"/>
                    <col width="15%"/>
                    <col width="20%"/>
                    <thead>

                    <tr>
                        <td><a><b>Structure</b></a></td>
                        <td><a><b>Calculated Mass</b></a></td>
                        <td><a><b>Precursor Mass</b></a></td>
                        <td><a><b>Taxonomy</b></a></td>
                        <td><a><b>Tissue</b></a></td>
                        <td><a><b>Reference</b></a></td>
                    </tr>
                    </thead>
                    <tbody>

                    """),_display_(/*46.22*/for(r <- res) yield /*46.35*/{_display_(Seq[Any](format.raw/*46.36*/("""
                        """),format.raw/*47.25*/("""<tr>
                            <td><a href="/msData/"""),_display_(/*48.51*/r/*48.52*/.scan_id),format.raw/*48.60*/(""""><img class="sugar_image" src="""),_display_(/*48.92*/{routes.GWPController.showImage(r.sequence_gws,session.get("notation"),"extended")}),format.raw/*48.175*/(""" """),format.raw/*48.176*/("""height="50%", width="50%" alt=""/></a></td>
                            <td>"""),_display_(/*49.34*/("%.2f".format(r.mass))),format.raw/*49.57*/("""</td>
                            <td>"""),_display_(/*50.34*/r/*50.35*/.base_peak_mz),format.raw/*50.48*/("""</td>
                            <td>"""),_display_(/*51.34*/r/*51.35*/.taxon),format.raw/*51.41*/("""</td>
                            <td>"""),_display_(/*52.34*/r/*52.35*/.tissue_taxon),format.raw/*52.48*/("""</td>
                            <td>"""),_display_(/*53.34*/r/*53.35*/.title),format.raw/*53.41*/("""</td>
                        </tr>
                    """)))}),format.raw/*55.22*/("""

                    """),format.raw/*57.21*/("""</tbody>
                </table>

            </div>

        </div>

    </div>

    """)))}),format.raw/*66.6*/("""

    """),format.raw/*68.5*/("""<a href=""""),_display_(/*68.15*/routes/*68.21*/.SearchController.search()),format.raw/*68.47*/("""">
        <input type="button" value="Search Again" href=""""),_display_(/*69.58*/routes/*69.64*/.SearchController.search()),format.raw/*69.90*/("""" class="btn btn-primary pull-right">
    </a>

</div>

""")))}))
      }
    }
  }

  def render(res:List[DBviews.vAdvancedSearch],userProvider:UserProvider): play.twirl.api.HtmlFormat.Appendable = apply(res,userProvider)

  def f:((List[DBviews.vAdvancedSearch],UserProvider) => play.twirl.api.HtmlFormat.Appendable) = (res,userProvider) => apply(res,userProvider)

  def ref: this.type = this

}


}
}

/**/
object resAdvanced extends resAdvanced_Scope0.resAdvanced_Scope1.resAdvanced
              /*
                  -- GENERATED --
                  DATE: Wed Jun 19 09:50:51 UTC 2019
                  SOURCE: /app/app/views/search/results/resAdvanced.scala.html
                  HASH: bdecf95cb504021feb66d5b05a38b167e5022f05
                  MATRIX: 870->30|1028->93|1056->96|1099->131|1137->132|1165->134|1279->223|1306->242|1345->244|1378->250|1481->336|1494->341|1533->342|1567->349|1900->655|1912->658|1940->665|1969->666|2892->1562|2921->1575|2960->1576|3013->1601|3095->1656|3105->1657|3134->1665|3193->1697|3298->1780|3328->1781|3432->1858|3476->1881|3542->1920|3552->1921|3586->1934|3652->1973|3662->1974|3689->1980|3755->2019|3765->2020|3799->2033|3865->2072|3875->2073|3902->2079|3990->2136|4040->2158|4158->2246|4191->2252|4228->2262|4243->2268|4290->2294|4377->2354|4392->2360|4439->2386
                  LINES: 30->2|35->2|37->4|37->4|37->4|39->6|42->9|42->9|42->9|44->11|48->15|48->15|48->15|51->18|55->22|55->22|55->22|55->22|79->46|79->46|79->46|80->47|81->48|81->48|81->48|81->48|81->48|81->48|82->49|82->49|83->50|83->50|83->50|84->51|84->51|84->51|85->52|85->52|85->52|86->53|86->53|86->53|88->55|90->57|99->66|101->68|101->68|101->68|101->68|102->69|102->69|102->69
                  -- GENERATED --
              */
          