
package views.html.search

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object searchTissueTaxonomy_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class searchTissueTaxonomy extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template3[Form[DBviews.vAdvancedSearch],Map[String, String],Map[String, String],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(frAdvanced:Form[DBviews.vAdvancedSearch], taxon: Map[String,String], tissue: Map[String,String]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {
import helper._
implicit def /*4.2*/implicitFieldConstructor/*4.26*/ = {{ b3.inline.fieldConstructor }};
Seq[Any](format.raw/*1.99*/("""

"""),format.raw/*4.60*/("""


"""),format.raw/*7.58*/("""


"""),format.raw/*10.1*/("""<div class="taxonomy-div searchdiv">
    <h4>Biological Context</h4>
    <div class="bio-div">
        Taxonomy
        """),_display_(/*14.10*/b3/*14.12*/.select(
        frAdvanced("taxonomy_id"),
        options(taxon),
        '_default -> "Not specified",
        '_showConstraints -> false,
        'multiple -> false
        )),format.raw/*20.10*/("""

        """),format.raw/*22.9*/("""Tissue
        """),_display_(/*23.10*/b3/*23.12*/.select(
        frAdvanced("tissue_taxonomy_id"),
        options(tissue),
        '_default -> "Not specified",
        '_showConstraints -> false,
        'multiple -> false
        )),format.raw/*29.10*/("""
    """),format.raw/*30.5*/("""</div>
</div>
"""))
      }
    }
  }

  def render(frAdvanced:Form[DBviews.vAdvancedSearch],taxon:Map[String, String],tissue:Map[String, String]): play.twirl.api.HtmlFormat.Appendable = apply(frAdvanced,taxon,tissue)

  def f:((Form[DBviews.vAdvancedSearch],Map[String, String],Map[String, String]) => play.twirl.api.HtmlFormat.Appendable) = (frAdvanced,taxon,tissue) => apply(frAdvanced,taxon,tissue)

  def ref: this.type = this

}


}

/**/
object searchTissueTaxonomy extends searchTissueTaxonomy_Scope0.searchTissueTaxonomy
              /*
                  -- GENERATED --
                  DATE: Wed Jun 19 09:50:50 UTC 2019
                  SOURCE: /app/app/views/search/searchTissueTaxonomy.scala.html
                  HASH: fb77173e2a5d42b6caf9d4cb7dff1d0e8c3cbc49
                  MATRIX: 845->1|1044->118|1076->142|1140->98|1169->176|1199->236|1229->239|1377->360|1388->362|1587->540|1624->550|1667->566|1678->568|1885->754|1917->759
                  LINES: 27->1|31->4|31->4|32->1|34->4|37->7|40->10|44->14|44->14|50->20|52->22|53->23|53->23|59->29|60->30
                  -- GENERATED --
              */
          