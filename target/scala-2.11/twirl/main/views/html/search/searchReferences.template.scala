
package views.html.search

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object searchReferences_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class searchReferences extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[Form[DBviews.vAdvancedSearch],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(frAdvanced: Form[DBviews.vAdvancedSearch]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {

implicit def /*3.2*/implicitFieldConstructor/*3.26*/ = {{ b3.inline.fieldConstructor }};
Seq[Any](format.raw/*1.45*/("""

"""),format.raw/*3.60*/("""

"""),format.raw/*5.1*/("""<div class="searchdiv">
    <h4>Reference</h4>
    """),_display_(/*7.6*/b3/*7.8*/.text(frAdvanced("authors"), '_class -> "search-field", 'placeholder -> "Author")),format.raw/*7.89*/("""
    """),_display_(/*8.6*/b3/*8.8*/.text(frAdvanced("title"), '_class -> "search-field", 'placeholder -> "Title")),format.raw/*8.86*/("""
    """),_display_(/*9.6*/b3/*9.8*/.number(frAdvanced("year"), '_class -> "search-field short", 'max -> 9999, 'placeholder -> "Year")),format.raw/*9.106*/("""
    """),_display_(/*10.6*/b3/*10.8*/.text(frAdvanced("pubmed"), '_class -> "search-field short", 'placeholder -> "Pubmed")),format.raw/*10.94*/("""
"""),format.raw/*11.1*/("""</div>


"""))
      }
    }
  }

  def render(frAdvanced:Form[DBviews.vAdvancedSearch]): play.twirl.api.HtmlFormat.Appendable = apply(frAdvanced)

  def f:((Form[DBviews.vAdvancedSearch]) => play.twirl.api.HtmlFormat.Appendable) = (frAdvanced) => apply(frAdvanced)

  def ref: this.type = this

}


}

/**/
object searchReferences extends searchReferences_Scope0.searchReferences
              /*
                  -- GENERATED --
                  DATE: Wed Jun 19 09:50:51 UTC 2019
                  SOURCE: /app/app/views/search/searchReferences.scala.html
                  HASH: 6c0e365284dac9f8f172ed94835698044922ef9f
                  MATRIX: 797->1|927->47|959->71|1023->44|1052->105|1080->107|1157->159|1166->161|1267->242|1298->248|1307->250|1405->328|1436->334|1445->336|1564->434|1596->440|1606->442|1713->528|1741->529
                  LINES: 27->1|31->3|31->3|32->1|34->3|36->5|38->7|38->7|38->7|39->8|39->8|39->8|40->9|40->9|40->9|41->10|41->10|41->10|42->11
                  -- GENERATED --
              */
          