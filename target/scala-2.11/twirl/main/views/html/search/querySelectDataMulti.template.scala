
package views.html.search

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object querySelectDataMulti_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class querySelectDataMulti extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template3[Map[String, String],Map[String, String],List[String],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(tissues: Map[String,String], taxonomies: Map[String,String], refs: List[String]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.83*/("""

"""),format.raw/*3.1*/("""<script>
    $(function() """),format.raw/*4.18*/("""{"""),format.raw/*4.19*/("""
        """),format.raw/*5.9*/("""options = '';

        var myTissues = new Array();
        """),_display_(/*8.10*/for((key, value) <- tissues ) yield /*8.39*/{_display_(Seq[Any](format.raw/*8.40*/("""
            """),format.raw/*9.13*/("""myTissues.push(""""),_display_(/*9.30*/value),format.raw/*9.35*/("""");
        """)))}),format.raw/*10.10*/("""
        """),format.raw/*11.9*/("""myTissues.sort();
        options += '<optgroup label="Tissue">';
        for (var i = 0; i < myTissues.length; i++) """),format.raw/*13.52*/("""{"""),format.raw/*13.53*/("""
            """),format.raw/*14.13*/("""options += '<option value="' + myTissues[i] + '">' + myTissues[i] + '</option>';
        """),format.raw/*15.9*/("""}"""),format.raw/*15.10*/("""
        """),format.raw/*16.9*/("""options += '</optgroup>';

        var myTaxonomies = new Array();
        """),_display_(/*19.10*/for((key, value) <- taxonomies ) yield /*19.42*/{_display_(Seq[Any](format.raw/*19.43*/("""
            """),format.raw/*20.13*/("""myTaxonomies.push(""""),_display_(/*20.33*/value),format.raw/*20.38*/("""");
        """)))}),format.raw/*21.10*/("""
        """),format.raw/*22.9*/("""myTaxonomies.sort();
        options += '<optgroup label="Taxonomies">';
        for (var i = 0; i < myTaxonomies.length; i++) """),format.raw/*24.55*/("""{"""),format.raw/*24.56*/("""
            """),format.raw/*25.13*/("""options += '<option value="' + myTaxonomies[i] + '">' + myTaxonomies[i] + '</option>';
        """),format.raw/*26.9*/("""}"""),format.raw/*26.10*/("""
        """),format.raw/*27.9*/("""options += '</optgroup>';

        var myReferences = new Array();
        """),_display_(/*30.10*/for(r <- refs ) yield /*30.25*/{_display_(Seq[Any](format.raw/*30.26*/("""
            """),format.raw/*31.13*/("""myReferences.push(""""),_display_(/*31.33*/r),format.raw/*31.34*/("""");
        """)))}),format.raw/*32.10*/("""
        """),format.raw/*33.9*/("""myReferences.sort();
        options += '<optgroup label="References">';
        for (var i = 0; i < myReferences.length; i++) """),format.raw/*35.55*/("""{"""),format.raw/*35.56*/("""
            """),format.raw/*36.13*/("""options += '<option value="' + myReferences[i] + '">' + myReferences[i] + '</option>';
        """),format.raw/*37.9*/("""}"""),format.raw/*37.10*/("""
        """),format.raw/*38.9*/("""options += '</optgroup>';

        $(document).ready(function()"""),format.raw/*40.37*/("""{"""),format.raw/*40.38*/("""
            """),format.raw/*41.13*/("""$("#e20").select2("""),format.raw/*41.31*/("""{"""),format.raw/*41.32*/("""
                """),format.raw/*42.17*/("""placeholder: "Start typing...",
                allowClear: true,
                minimumInputLength: 1,
                width: '100%'
            """),format.raw/*46.13*/("""}"""),format.raw/*46.14*/(""");
        """),format.raw/*47.9*/("""}"""),format.raw/*47.10*/(""");


        $('#e20').html(options); //SB
    """),format.raw/*51.5*/("""}"""),format.raw/*51.6*/(""");
</script>

"""))
      }
    }
  }

  def render(tissues:Map[String, String],taxonomies:Map[String, String],refs:List[String]): play.twirl.api.HtmlFormat.Appendable = apply(tissues,taxonomies,refs)

  def f:((Map[String, String],Map[String, String],List[String]) => play.twirl.api.HtmlFormat.Appendable) = (tissues,taxonomies,refs) => apply(tissues,taxonomies,refs)

  def ref: this.type = this

}


}

/**/
object querySelectDataMulti extends querySelectDataMulti_Scope0.querySelectDataMulti
              /*
                  -- GENERATED --
                  DATE: Wed Jun 19 09:50:51 UTC 2019
                  SOURCE: /app/app/views/search/querySelectDataMulti.scala.html
                  HASH: d348d87f612cb3fe9f5cd1a2fa33d5a32bf5f087
                  MATRIX: 828->1|1004->82|1032->84|1085->110|1113->111|1148->120|1235->181|1279->210|1317->211|1357->224|1400->241|1425->246|1469->259|1505->268|1650->385|1679->386|1720->399|1836->488|1865->489|1901->498|2004->574|2052->606|2091->607|2132->620|2179->640|2205->645|2249->658|2285->667|2440->794|2469->795|2510->808|2632->903|2661->904|2697->913|2800->989|2831->1004|2870->1005|2911->1018|2958->1038|2980->1039|3024->1052|3060->1061|3215->1188|3244->1189|3285->1202|3407->1297|3436->1298|3472->1307|3563->1370|3592->1371|3633->1384|3679->1402|3708->1403|3753->1420|3928->1567|3957->1568|3995->1579|4024->1580|4098->1627|4126->1628
                  LINES: 27->1|32->1|34->3|35->4|35->4|36->5|39->8|39->8|39->8|40->9|40->9|40->9|41->10|42->11|44->13|44->13|45->14|46->15|46->15|47->16|50->19|50->19|50->19|51->20|51->20|51->20|52->21|53->22|55->24|55->24|56->25|57->26|57->26|58->27|61->30|61->30|61->30|62->31|62->31|62->31|63->32|64->33|66->35|66->35|67->36|68->37|68->37|69->38|71->40|71->40|72->41|72->41|72->41|73->42|77->46|77->46|78->47|78->47|82->51|82->51
                  -- GENERATED --
              */
          