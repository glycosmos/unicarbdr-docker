
package views.html.search

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object searchMSMS_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class searchMSMS extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[Form[SearchMSMSForm],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(frMSMS: Form[SearchMSMSForm]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {
import helper._
implicit def /*3.2*/implicitFieldConstructor/*3.26*/ = {{ b3.vertical.fieldConstructor }};
Seq[Any](format.raw/*1.32*/("""
"""),format.raw/*3.62*/("""


"""),_display_(/*6.2*/if(frMSMS.hasGlobalErrors)/*6.28*/ {_display_(Seq[Any](format.raw/*6.30*/("""
    """),format.raw/*7.5*/("""<p class="error">
        """),_display_(/*8.10*/for(error <- frMSMS.globalErrors) yield /*8.43*/ {_display_(Seq[Any](format.raw/*8.45*/("""
            """),format.raw/*9.13*/("""<p class="coral">"""),_display_(/*9.31*/Messages(error.messages, error.arguments.toArray: _*)),format.raw/*9.84*/("""</p>
        """)))}),format.raw/*10.10*/("""
    """),format.raw/*11.5*/("""</p>
""")))}),format.raw/*12.2*/("""


"""),_display_(/*15.2*/b3/*15.4*/.form(routes.SearchController.searchMSMS())/*15.47*/ {_display_(Seq[Any](format.raw/*15.49*/("""
    """),format.raw/*16.5*/("""<div class="searchdiv">
        <h4>Precursor Mass</h4>
        """),_display_(/*18.10*/b3/*18.12*/.number(frMSMS("precursor"),
        'step -> "any",
        '_class -> "search-field short")),format.raw/*20.41*/("""
    """),format.raw/*21.5*/("""</div>

    <div class="searchdiv">
        <h4>Error</h4>
        """),_display_(/*25.10*/b3/*25.12*/.number(frMSMS("error"),
            'step -> "any",
            '_class -> "search-field short")),format.raw/*27.45*/("""

        """),_display_(/*29.10*/b3/*29.12*/.radio(frMSMS("errorType"), '_class -> "search-radio")/*29.66*/{ implicit extraInfo =>_display_(Seq[Any](format.raw/*29.89*/("""
            """),_display_(/*30.14*/b3/*30.16*/.radioOption("m", Html("<span>mz</span>"), 'checked -> true)),format.raw/*30.76*/("""
            """),_display_(/*31.14*/b3/*31.16*/.radioOption("p", Html("<span>ppm</span>"))),format.raw/*31.59*/("""
            """),_display_(/*32.14*/b3/*32.16*/.radioOption("d", Html("<span>dalton</span>"))),format.raw/*32.62*/("""
        """)))}),format.raw/*33.10*/("""
    """),format.raw/*34.5*/("""</div>

    <div class="searchdiv">
        <h4>Fragmentation Error</h4>
        """),_display_(/*38.10*/b3/*38.12*/.number(frMSMS("frError"),
        'step -> "any",
        '_class -> "search-field short")),format.raw/*40.41*/("""

        """),_display_(/*42.10*/b3/*42.12*/.radio(frMSMS("frErrorType"), '_class -> "search-radio")/*42.68*/{ implicit extraInfo =>_display_(Seq[Any](format.raw/*42.91*/("""
        """),_display_(/*43.10*/b3/*43.12*/.radioOption("m", Html("<span>mz</span>"), 'checked -> true)),format.raw/*43.72*/("""
        """),_display_(/*44.10*/b3/*44.12*/.radioOption("p", Html("<span>ppm</span>"))),format.raw/*44.55*/("""
        """),_display_(/*45.10*/b3/*45.12*/.radioOption("d", Html("<span>dalton</span>"))),format.raw/*45.58*/("""
        """)))}),format.raw/*46.10*/("""
    """),format.raw/*47.5*/("""</div>

    <div class="searchdiv">
        <h4>Peak List</h4>
        """),_display_(/*51.10*/b3/*51.12*/.textarea(frMSMS("peaks"), 'rows -> 15,  'placeholder -> "Paste your peak list here: one column for the mz/value and a second column for the respective intensities. The columns must be tab separated." )),format.raw/*51.214*/("""
        """),_display_(/*52.10*/b3/*52.12*/.select(frMSMS("method"), options = Seq("d"->"dot product"), '_label -> "Select type" )),format.raw/*52.99*/("""
    """),format.raw/*53.5*/("""</div>



    """),_display_(/*57.6*/b3/*57.8*/.free()/*57.15*/{_display_(Seq[Any](format.raw/*57.16*/("""
        """),format.raw/*58.9*/("""<div class="row-fluid">
            <button type="submit" class="btn btn-primary pull-right">Search</button>
        </div>
    """)))}),format.raw/*61.6*/("""
""")))}),format.raw/*62.2*/("""






"""))
      }
    }
  }

  def render(frMSMS:Form[SearchMSMSForm]): play.twirl.api.HtmlFormat.Appendable = apply(frMSMS)

  def f:((Form[SearchMSMSForm]) => play.twirl.api.HtmlFormat.Appendable) = (frMSMS) => apply(frMSMS)

  def ref: this.type = this

}


}

/**/
object searchMSMS extends searchMSMS_Scope0.searchMSMS
              /*
                  -- GENERATED --
                  DATE: Wed Jun 19 09:50:50 UTC 2019
                  SOURCE: /app/app/views/search/searchMSMS.scala.html
                  HASH: bad10fcdd77cef1cbdcfc4a98813fdca1b7baf05
                  MATRIX: 776->1|908->50|940->74|1006->31|1034->110|1063->114|1097->140|1136->142|1167->147|1220->174|1268->207|1307->209|1347->222|1391->240|1464->293|1509->307|1541->312|1577->318|1607->322|1617->324|1669->367|1709->369|1741->374|1833->439|1844->441|1958->534|1990->539|2085->607|2096->609|2214->706|2252->717|2263->719|2326->773|2387->796|2428->810|2439->812|2520->872|2561->886|2572->888|2636->931|2677->945|2688->947|2755->993|2796->1003|2828->1008|2937->1090|2948->1092|3060->1183|3098->1194|3109->1196|3174->1252|3235->1275|3272->1285|3283->1287|3364->1347|3401->1357|3412->1359|3476->1402|3513->1412|3524->1414|3591->1460|3632->1470|3664->1475|3763->1547|3774->1549|3998->1751|4035->1761|4046->1763|4154->1850|4186->1855|4227->1870|4237->1872|4253->1879|4292->1880|4328->1889|4487->2018|4519->2020
                  LINES: 27->1|31->3|31->3|32->1|33->3|36->6|36->6|36->6|37->7|38->8|38->8|38->8|39->9|39->9|39->9|40->10|41->11|42->12|45->15|45->15|45->15|45->15|46->16|48->18|48->18|50->20|51->21|55->25|55->25|57->27|59->29|59->29|59->29|59->29|60->30|60->30|60->30|61->31|61->31|61->31|62->32|62->32|62->32|63->33|64->34|68->38|68->38|70->40|72->42|72->42|72->42|72->42|73->43|73->43|73->43|74->44|74->44|74->44|75->45|75->45|75->45|76->46|77->47|81->51|81->51|81->51|82->52|82->52|82->52|83->53|87->57|87->57|87->57|87->57|88->58|91->61|92->62
                  -- GENERATED --
              */
          