
package views.html.search

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object searchAdvanced_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class searchAdvanced extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template5[Form[DBviews.vAdvancedSearch],Map[String, String],Map[String, String],Map[String, String],Map[String, String],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(frAdvanced: Form[DBviews.vAdvancedSearch], persubs: Map[String,String], redEnd: Map[String,String], taxon: Map[String,String], tissue: Map[String,String]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {
import helper._
implicit def /*4.2*/implicitFC/*4.12*/ = {{ b3.horizontal.fieldConstructor("col-md-6", "col-md-6") }};
Seq[Any](format.raw/*1.157*/("""

"""),format.raw/*4.74*/("""

"""),_display_(/*6.2*/if(frAdvanced.hasGlobalErrors)/*6.32*/ {_display_(Seq[Any](format.raw/*6.34*/("""
    """),format.raw/*7.5*/("""<p class="error">
        """),_display_(/*8.10*/for(error <- frAdvanced.globalErrors) yield /*8.47*/ {_display_(Seq[Any](format.raw/*8.49*/("""
            """),format.raw/*9.13*/("""<p class="coral">"""),_display_(/*9.31*/Messages(error.messages, error.arguments.toArray: _*)),format.raw/*9.84*/("""</p>
        """)))}),format.raw/*10.10*/("""
    """),format.raw/*11.5*/("""</p>
""")))}),format.raw/*12.2*/("""


"""),_display_(/*15.2*/b3/*15.4*/.form(routes.SearchController.searchAdvanced())/*15.51*/ {_display_(Seq[Any](format.raw/*15.53*/("""

    """),_display_(/*17.6*/views/*17.11*/.html.search.searchTissueTaxonomy(frAdvanced, tissue, taxon)),format.raw/*17.71*/("""
"""),format.raw/*18.1*/("""<span id="pruebata">
    """),_display_(/*19.6*/views/*19.11*/.html.search.searchReferences(frAdvanced)),format.raw/*19.52*/("""
"""),format.raw/*20.1*/("""</span>
    """),_display_(/*21.6*/views/*21.11*/.html.search.searchComposition(frAdvanced, persubs, redEnd)),format.raw/*21.70*/("""



    """),_display_(/*25.6*/b3/*25.8*/.free()/*25.15*/{_display_(Seq[Any](format.raw/*25.16*/("""
    """),format.raw/*26.5*/("""<div class="row-fluid">
        <button type="submit" class="btn btn-primary pull-right">Search</button>
    </div>
    """)))}),format.raw/*29.6*/("""



""")))}))
      }
    }
  }

  def render(frAdvanced:Form[DBviews.vAdvancedSearch],persubs:Map[String, String],redEnd:Map[String, String],taxon:Map[String, String],tissue:Map[String, String]): play.twirl.api.HtmlFormat.Appendable = apply(frAdvanced,persubs,redEnd,taxon,tissue)

  def f:((Form[DBviews.vAdvancedSearch],Map[String, String],Map[String, String],Map[String, String],Map[String, String]) => play.twirl.api.HtmlFormat.Appendable) = (frAdvanced,persubs,redEnd,taxon,tissue) => apply(frAdvanced,persubs,redEnd,taxon,tissue)

  def ref: this.type = this

}


}

/**/
object searchAdvanced extends searchAdvanced_Scope0.searchAdvanced
              /*
                  -- GENERATED --
                  DATE: Wed Jun 19 09:50:51 UTC 2019
                  SOURCE: /app/app/views/search/searchAdvanced.scala.html
                  HASH: 20d1f49dd3c818161ae205fb7ae16e692502cb5c
                  MATRIX: 873->1|1130->176|1148->186|1241->156|1270->248|1298->251|1336->281|1375->283|1406->288|1459->315|1511->352|1550->354|1590->367|1634->385|1707->438|1752->452|1784->457|1820->463|1850->467|1860->469|1916->516|1956->518|1989->525|2003->530|2084->590|2112->591|2164->617|2178->622|2240->663|2268->664|2307->677|2321->682|2401->741|2436->750|2446->752|2462->759|2501->760|2533->765|2684->886
                  LINES: 27->1|31->4|31->4|32->1|34->4|36->6|36->6|36->6|37->7|38->8|38->8|38->8|39->9|39->9|39->9|40->10|41->11|42->12|45->15|45->15|45->15|45->15|47->17|47->17|47->17|48->18|49->19|49->19|49->19|50->20|51->21|51->21|51->21|55->25|55->25|55->25|55->25|56->26|59->29
                  -- GENERATED --
              */
          