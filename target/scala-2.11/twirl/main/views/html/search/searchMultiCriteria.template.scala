
package views.html.search

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object searchMultiCriteria_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class searchMultiCriteria extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template4[Map[String, String],Map[String, String],List[String],Form[DBviews.vAdvancedSearch],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(tissue: Map[String,String], taxon: Map[String,String], refs: List[String], frAdvanced: Form[DBviews.vAdvancedSearch]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {
import helper._
implicit def /*4.2*/implicitFieldConstructor/*4.26*/ = {{ b3.vertical.fieldConstructor }};
Seq[Any](format.raw/*1.120*/("""

"""),format.raw/*4.62*/("""


"""),_display_(/*7.2*/views/*7.7*/.html.search.querySelectDataMulti(tissue, taxon, refs)),format.raw/*7.61*/("""


"""),format.raw/*10.1*/("""<div id="unicarbCriteria" class="controls">
    <span>The Basic Search allows you to search UniCarb-DR using a combination of fields: Taxonomy, Tissue and References.
        Choose one or several options from the list below and click Search</span>
    <label class="checkbox">
            <div id="selection" class="row-fluid;">

                """),_display_(/*16.18*/b3/*16.20*/.form(routes.SearchController.searchBasic())/*16.64*/ {_display_(Seq[Any](format.raw/*16.66*/("""
                    """),_display_(/*17.22*/b3/*17.24*/.select(frAdvanced("e20"),
                    options(),
                    '_class -> "span10",
                    'multiple -> true)),format.raw/*20.39*/("""

                    """),_display_(/*22.22*/b3/*22.24*/.free()/*22.31*/{_display_(Seq[Any](format.raw/*22.32*/("""
                        """),format.raw/*23.25*/("""<button type="submit" class="btn btn-primary pull-right">Search</button>
                    """)))}),format.raw/*24.22*/("""
               """)))}),format.raw/*25.17*/("""

            """),format.raw/*27.13*/("""</div>
    </label>
</div>
"""))
      }
    }
  }

  def render(tissue:Map[String, String],taxon:Map[String, String],refs:List[String],frAdvanced:Form[DBviews.vAdvancedSearch]): play.twirl.api.HtmlFormat.Appendable = apply(tissue,taxon,refs,frAdvanced)

  def f:((Map[String, String],Map[String, String],List[String],Form[DBviews.vAdvancedSearch]) => play.twirl.api.HtmlFormat.Appendable) = (tissue,taxon,refs,frAdvanced) => apply(tissue,taxon,refs,frAdvanced)

  def ref: this.type = this

}


}

/**/
object searchMultiCriteria extends searchMultiCriteria_Scope0.searchMultiCriteria
              /*
                  -- GENERATED --
                  DATE: Wed Jun 19 09:50:51 UTC 2019
                  SOURCE: /app/app/views/search/searchMultiCriteria.scala.html
                  HASH: f443b47239bdff5ea98633c9813aec3423cdab94
                  MATRIX: 856->1|1076->139|1108->163|1175->119|1204->199|1233->203|1245->208|1319->262|1349->265|1724->613|1735->615|1788->659|1828->661|1877->683|1888->685|2046->822|2096->845|2107->847|2123->854|2162->855|2215->880|2340->974|2388->991|2430->1005
                  LINES: 27->1|31->4|31->4|32->1|34->4|37->7|37->7|37->7|40->10|46->16|46->16|46->16|46->16|47->17|47->17|50->20|52->22|52->22|52->22|52->22|53->23|54->24|55->25|57->27
                  -- GENERATED --
              */
          