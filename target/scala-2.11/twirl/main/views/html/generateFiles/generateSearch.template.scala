
package views.html.generateFiles

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object generateSearch_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

     object generateSearch_Scope1 {
import service.GenerateMirageForm

class generateSearch extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[Form[GenerateMirageForm],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*2.2*/(mirageForm: Form[GenerateMirageForm]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {

implicit def /*4.2*/implicitFieldConstructor/*4.26*/ = {{ b3.vertical.fieldConstructor }};
Seq[Any](format.raw/*2.40*/("""

"""),format.raw/*4.62*/("""



"""),format.raw/*8.1*/("""<div id="unicarbCriteria" class="controls">
    <span></span>
    <label class="checkbox">
        <div id="selection" class="row-fluid;">
        <br />

            """),format.raw/*14.109*/("""
            """),_display_(/*15.14*/helper/*15.20*/.form(action = routes.GenerateController.generateForm(), 'onsubmit -> "return loadScreen();")/*15.113*/{_display_(Seq[Any](format.raw/*15.114*/("""
            """),format.raw/*16.70*/("""
            """),format.raw/*25.21*/("""

            """),format.raw/*27.13*/("""<h4>Sheets</h4>
            <br />
                <h6 id="error-message" style="color: red; display: none">You must select at least one sheet.</h6>

            """),_display_(/*31.14*/b3/*31.16*/.multiCheckbox(
                (mirageForm("genSamplePrep"), Seq('_text -> "MIRAGE-Sample Preparation")),
                (mirageForm("genLCMS"), Seq('_text -> "LC as part of Mass Spectrometry")),
                (mirageForm("genMSMirage"), Seq('_text -> "MIRAGE-MS"))
            )/*35.14*/('_label -> null, 'class -> "multi-checkbox-list")),format.raw/*35.64*/("""
            """),format.raw/*36.13*/("""<br />
            <br />
                """),format.raw/*59.3*/("""
            """),format.raw/*60.13*/("""<br />
            """),format.raw/*70.17*/("""

            """),format.raw/*72.13*/("""<br />
            <br />
            <br />
            <button type="submit" class="btn btn-primary">Next</button>
        """)))}),format.raw/*76.10*/("""

        """),format.raw/*78.9*/("""</div>
    </label>
</div>
<div id="loading-overlay" style="
    position: fixed;
    display: none;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-color: rgba(200,200,200, 1);
    z-index: 12;">
  <div id="loading-info" style="position: absolute;
      top: 50%;
      left: 50%;
      font-size: 12px;
      text-align: center;
      width: 680px;
      height: 200px;
      border-radius: 4px;
      transform: translate(-50%,-50%);
      -ms-transform: translate(-50%,-50%);">
      <img src="../assets/images/wait.gif" style="width: 160px; margin: 20px"> <span style="font-size: 80px; vertical-align: middle">UniCarb-DB</span>
  </div>
</div>

<script>
        function loadScreen() """),format.raw/*107.31*/("""{"""),format.raw/*107.32*/("""
            """),format.raw/*108.13*/("""var submit = false;

            if($("#genSamplePrep").prop("checked") || $("#genLCMS").prop("checked") || $("#genMSMirage").prop("checked"))"""),format.raw/*110.122*/("""{"""),format.raw/*110.123*/("""
                """),format.raw/*111.17*/("""submit = true;
            """),format.raw/*112.13*/("""}"""),format.raw/*112.14*/(""" """),format.raw/*112.15*/("""else """),format.raw/*112.20*/("""{"""),format.raw/*112.21*/("""
                """),format.raw/*113.17*/("""$("#error-message").show();
            """),format.raw/*114.13*/("""}"""),format.raw/*114.14*/("""

            """),format.raw/*116.13*/("""return submit;
        """),format.raw/*117.9*/("""}"""),format.raw/*117.10*/("""
        """),format.raw/*118.9*/("""$(document).ready(function()"""),format.raw/*118.37*/("""{"""),format.raw/*118.38*/("""
            """),format.raw/*119.13*/("""$(".checkbox").change(function () """),format.raw/*119.47*/("""{"""),format.raw/*119.48*/("""
                """),format.raw/*120.17*/("""$("#error-message").hide();
            """),format.raw/*121.13*/("""}"""),format.raw/*121.14*/(""");
        """),format.raw/*122.9*/("""}"""),format.raw/*122.10*/(""");
</script>
"""))
      }
    }
  }

  def render(mirageForm:Form[GenerateMirageForm]): play.twirl.api.HtmlFormat.Appendable = apply(mirageForm)

  def f:((Form[GenerateMirageForm]) => play.twirl.api.HtmlFormat.Appendable) = (mirageForm) => apply(mirageForm)

  def ref: this.type = this

}


}
}

/**/
object generateSearch extends generateSearch_Scope0.generateSearch_Scope1.generateSearch
              /*
                  -- GENERATED --
                  DATE: Wed Jun 19 09:50:52 UTC 2019
                  SOURCE: /app/app/views/generateFiles/generateSearch.scala.html
                  HASH: b3351c42ee12fdd06a46759b4a01d47878b28f15
                  MATRIX: 866->36|991->77|1023->101|1089->74|1118->137|1148->141|1344->404|1385->418|1400->424|1503->517|1543->518|1584->588|1625->1014|1667->1028|1857->1191|1868->1193|2160->1476|2231->1526|2272->1539|2341->2598|2382->2611|2429->3080|2471->3094|2628->3220|2665->3230|3439->3975|3469->3976|3511->3989|3683->4131|3714->4132|3760->4149|3816->4176|3846->4177|3876->4178|3910->4183|3940->4184|3986->4201|4055->4241|4085->4242|4128->4256|4179->4279|4209->4280|4246->4289|4303->4317|4333->4318|4375->4331|4438->4365|4468->4366|4514->4383|4583->4423|4613->4424|4652->4435|4682->4436
                  LINES: 30->2|34->4|34->4|35->2|37->4|41->8|47->14|48->15|48->15|48->15|48->15|49->16|50->25|52->27|56->31|56->31|60->35|60->35|61->36|63->59|64->60|65->70|67->72|71->76|73->78|102->107|102->107|103->108|105->110|105->110|106->111|107->112|107->112|107->112|107->112|107->112|108->113|109->114|109->114|111->116|112->117|112->117|113->118|113->118|113->118|114->119|114->119|114->119|115->120|116->121|116->121|117->122|117->122
                  -- GENERATED --
              */
          