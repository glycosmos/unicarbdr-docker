
package views.html.generateFiles

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object generateMirage_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

     object generateMirage_Scope1 {
import service.UserProvider
import service.GenerateMirageForm

class generateMirage extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template3[String,Form[GenerateMirageForm],UserProvider,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*3.2*/(sub: String, mirageForm: Form[GenerateMirageForm], userProvider: UserProvider):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*3.81*/("""

"""),_display_(/*5.2*/main(userProvider, "MIRAGE files")/*5.36*/{_display_(Seq[Any](format.raw/*5.37*/("""
    """),format.raw/*6.5*/("""<div class="generate">
        <div class="container-fluid content">
            <ul class="breadcrumb">
                <li><i class="icon-home" ></i><a href="/"> UniCarb-DBR/a> <span class="divider">></span></li>
                <li class="active"><i class="icon-download" ></i>MIRAGE<span class="divider"></span></li>
            </ul>

            <div class="row-fluid info">
                <p class="row-fluid">
                    <div class="page-header row-fluid">
                        <h1 class="subheader span8">MIRAGE data submission requirements</h1>
                    </div>
                    <p>The Minimum Information Required for a Glycomics Experiment (MIRAGE) guidelines seek to facilitate the reproduction of experimental work
                        in glycomics by improving the quality of published data. UniCarb-DB is the first database to implement a framework for the support of
                        <a href="http://www.beilstein-institut.de/en/projects/mirage/guidelines#mass_spectrometric_analysis" target="_blank">MS</a> and
                        <a href="http://www.beilstein-institut.de/en/projects/mirage/guidelines#sample_preparations" target="_blank">Sample preparation</a>
                        compliant data. </p>

                    <p>Besides sample preparation and MS guidelines, UniCarb-DB also collects HPLC settings. The submission process allows to capture all these data
                        online. However, in order to facilitate data submission, we have designed an excel file that can be uploaded when completed. The collection of data
                        specific for each structure follows a different workflow. In this case, it is necessary to create a Glycoworkbench file (extension gwp). </p>


                    <p>The submission process differentiates two types of data that can be described as a) general for the experiment and b) specific for each structure in the experiment.
                        A file format has been implemented for both types of data, a Glycoworkbench file (extension gwp) for the former and an excel file for the latter. These files can
                        be uploaded to UniCarb-DB through the submission system. For each structure, this file includes a peak list, a scan name, and a series of attributes that can be
                        described in the notes section <a href="#" target="_blank">(See image)</a>. The submission process cannot be completed if the gwp file is not provided.</p>

                    <p>Below both files are available to download. If you have any question, please do not hesitate to contact us at admin at unicarb-db.org.</p>

                    <br/><br/>
                    <h4>GlycoWorkbench file.</h4>
                    <hr>
                    <p>This file contains a single structure that can be used as example for the addition of new data. More than one file can be used when the number of structures is too big.</p>

                    <br />
                    <a href=""""),_display_(/*42.31*/routes/*42.37*/.Assets.versioned("samplefiles/UniCarb-DB-Sample.gwp")),format.raw/*42.91*/("""" target="_blank" style="font-size: 15px; margin-left: 20px">
                    Download sample Glycoworkbench file
                    </a>

                    <br/><br/>
                    <br/><br/>
                    <h4>Excel file.</h4>
                    <hr>
                    <p>This file can be personalized to better fit your experiment. Please, start by choosing your setup.
                    </p>

                    """),_display_(/*53.22*/views/*53.27*/.html.generateFiles.generateSearch(mirageForm)),format.raw/*53.73*/("""

                """),format.raw/*55.17*/("""</div>
            </div>
        </div>
    </div>
""")))}),format.raw/*59.2*/("""
"""))
      }
    }
  }

  def render(sub:String,mirageForm:Form[GenerateMirageForm],userProvider:UserProvider): play.twirl.api.HtmlFormat.Appendable = apply(sub,mirageForm,userProvider)

  def f:((String,Form[GenerateMirageForm],UserProvider) => play.twirl.api.HtmlFormat.Appendable) = (sub,mirageForm,userProvider) => apply(sub,mirageForm,userProvider)

  def ref: this.type = this

}


}
}

/**/
object generateMirage extends generateMirage_Scope0.generateMirage_Scope1.generateMirage
              /*
                  -- GENERATED --
                  DATE: Wed Jun 19 09:50:52 UTC 2019
                  SOURCE: /app/app/views/generateFiles/generateMirage.scala.html
                  HASH: 31f142d1406cf70df725af1e22ef06878f499685
                  MATRIX: 914->65|1088->144|1116->147|1158->181|1196->182|1227->187|4289->3222|4304->3228|4379->3282|4847->3723|4861->3728|4928->3774|4974->3792|5057->3845
                  LINES: 31->3|36->3|38->5|38->5|38->5|39->6|75->42|75->42|75->42|86->53|86->53|86->53|88->55|92->59
                  -- GENERATED --
              */
          