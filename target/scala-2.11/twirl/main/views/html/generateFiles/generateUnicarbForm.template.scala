
package views.html.generateFiles

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object generateUnicarbForm_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

     object generateUnicarbForm_Scope1 {
import service.UserProvider
import service.{FormSample, StructureForm, StructureForms, FormLC, FormMS, FormStructure}
import src.TableFragment

class generateUnicarbForm extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template4[Map[String, Form[_$1] forSome { 
   type _$1 >: _root_.scala.Nothing <: _root_.scala.Any
}],Map[String, Map[String, String]],List[TableFragment],UserProvider,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*4.2*/(
        forms: Map[String,Form[_]],
        sampleOptions: Map[String, Map[String, String]],
        tables: List[TableFragment], userProvider: UserProvider):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*7.65*/("""

"""),_display_(/*9.2*/main(userProvider, "MIRAGE files")/*9.36*/{_display_(Seq[Any](format.raw/*9.37*/("""

    """),format.raw/*11.5*/("""<link rel="stylesheet" type="text/css" media="screen" href='"""),_display_(/*11.66*/routes/*11.72*/.Assets.versioned("stylesheets/mirageForms.css")),format.raw/*11.120*/("""'>

    <div id="loading-overlay" style="
        position: fixed;
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: rgba(250,250,250, 1);
        z-index: 12;">
    </div>
adding
    <div class="container-fluid content">
        <ul class="breadcrumb">
            <li><i class="icon-home" ></i><a href="/"> UniCarb-DR</a> <span class="divider">></span></li>
            <li class="active"><i class="icon-upload" ></i>Generate<span class="divider"></span></li>
        </ul>
        <div class="page-header row-fluid">
            <h1 class="subheader span8">Excel file generation.</h1>
        </div>
        <p>The forms below mirror the structure of the excel file. The number of tabs is dependent on the options selected in the previous step, and
            all the data captured in the forms will be transcribed to the excel file. You can also click on the "Generate" button without filling in the forms. This action will generate an empty excel file.
            However, it is recommended to use the interfaces since some of the fields have been validated (and the list boxes use a defined language), which prevents
            validation errors later during the submission process.</p>
        <p>The fields marked with an * are required. However, if these data is not relevant for your experiment, you can use the value NA (Not applicable).</p>
        <br />
        <br />
        """),_display_(/*40.10*/helper/*40.16*/.form(action = routes.GenerateController.uploadForm())/*40.70*/ {_display_(Seq[Any](format.raw/*40.72*/("""
            """),format.raw/*41.13*/("""<button type="submit" class="btn btn-primary">Generate</button>
            <div id="sheet-container" >

                <div id="sheet-btns">
                    <div id="sheet-btns-container"><!--
                        """),_display_(/*46.26*/if(forms.get("Sample") != null)/*46.57*/{_display_(Seq[Any](format.raw/*46.58*/("""
                        """),format.raw/*47.25*/("""--><div id="sample-btn" class="sheet-btn">Sample Preparation</div><!--
                            """)))}),_display_(/*48.31*/if(forms.get("LC") != null)/*48.58*/{_display_(Seq[Any](format.raw/*48.59*/("""
                        """),format.raw/*49.25*/("""--><div id="lc-btn" class="sheet-btn">LC settings</div><!--
                            """)))}),_display_(/*50.31*/if(forms.get("MS") != null)/*50.58*/{_display_(Seq[Any](format.raw/*50.59*/("""
                        """),format.raw/*51.25*/("""--><div id="ms-one-btn" class="sheet-btn">MS part 1</div><!--
                        --><div id="ms-two-btn" class="sheet-btn">MS part 2</div><!--
                        """)))}),_display_(/*53.27*/if(forms.get("Sample") == null)/*53.58*/{_display_(Seq[Any](format.raw/*53.59*/("""
                        """),format.raw/*54.25*/("""--><div id="sample-btn-line" class="sheet-btn-line"></div><!--
                        """)))}),_display_(/*55.27*/if(forms.get("LC") == null)/*55.54*/ {_display_(Seq[Any](format.raw/*55.56*/("""
                        """),format.raw/*56.25*/("""--><div id="sample-btn-line" class="sheet-btn-line"></div><!--
                        """)))}),_display_(/*57.27*/if(forms.get("MS") == null)/*57.54*/ {_display_(Seq[Any](format.raw/*57.56*/("""
                        """),format.raw/*58.25*/("""--><div id="sample-btn-line" class="sheet-btn-line"></div><!--
                    --><div id="sample-btn-line" class="sheet-btn-line"></div><!--
                    """)))}),format.raw/*60.22*/("""--></div><!--
                --><div id="folder-end"></div>
                </div>
                <div class="sheet">


                    <div id="sample-sheet" style="display: none">
                   """),_display_(/*67.21*/if(forms.get("Sample") != null)/*67.52*/{_display_(Seq[Any](format.raw/*67.53*/(""" """),_display_(/*67.55*/views/*67.60*/.html.dataUpload.sheets.samplesheet(forms.get("Sample"),forms.get("Tables"),sampleOptions,tables)),format.raw/*67.157*/(""" """)))}),format.raw/*67.159*/("""
                    """),format.raw/*68.21*/("""</div>
                    <div id="lc-sheet">
                    """),_display_(/*70.22*/if(forms.get("LC") != null)/*70.49*/{_display_(Seq[Any](format.raw/*70.50*/(""" """),_display_(/*70.52*/views/*70.57*/.html.dataUpload.sheets.lcsheet(forms.get("LC"), sampleOptions)),format.raw/*70.120*/(""" """)))}),format.raw/*70.122*/("""
                    """),format.raw/*71.21*/("""</div>
                    <div id="ms-one-sheet">
                    """),_display_(/*73.22*/if(forms.get("MS") != null)/*73.49*/{_display_(Seq[Any](format.raw/*73.50*/(""" """),_display_(/*73.52*/views/*73.57*/.html.dataUpload.sheets.mssheet(forms.get("MS"),forms.get("Tables"), tables, sampleOptions)),format.raw/*73.148*/(""" """)))}),format.raw/*73.150*/("""
                    """),format.raw/*74.21*/("""</div>
                    <div id="ms-two-sheet">
                    """),_display_(/*76.22*/if(forms.get("MS") != null)/*76.49*/{_display_(Seq[Any](format.raw/*76.50*/(""" """),_display_(/*76.52*/views/*76.57*/.html.dataUpload.sheets.mssheet2(forms.get("MS"),forms.get("Tables"), tables, sampleOptions)),format.raw/*76.149*/(""" """)))}),format.raw/*76.151*/("""
                    """),format.raw/*77.21*/("""</div>

                </div>
            </div>
        """)))}),format.raw/*81.10*/("""
    """),format.raw/*82.5*/("""</div>
    <style>
            select::-ms-expand """),format.raw/*84.32*/("""{"""),format.raw/*84.33*/("""
                """),format.raw/*85.17*/("""width:12px;
                border:none;
                background:#fff;
            """),format.raw/*88.13*/("""}"""),format.raw/*88.14*/("""
            """),format.raw/*89.13*/(""".sheet"""),format.raw/*89.19*/("""{"""),format.raw/*89.20*/("""
                """),format.raw/*90.17*/("""box-sizing: border-box;
                padding: 40px;
                border-bottom: solid 1px #DDDDDD;
                border-left: solid 1px #DDDDDD;
                border-right: solid 1px #DDDDDD;
                width: 100%;
                margin-top: -0px;
            """),format.raw/*97.13*/("""}"""),format.raw/*97.14*/("""
            """),format.raw/*98.13*/(""".sheet-btn"""),format.raw/*98.23*/("""{"""),format.raw/*98.24*/("""
                """),format.raw/*99.17*/("""box-sizing: border-box;
                display: inline-block;
                margin: 0px;
                text-align: center;
                padding: 10px;
                width: 25%;
                border: solid 1px #DDDDDD;
                cursor: pointer;
                border-radius: 30px 30px 0px 0px;
                -webkit-margin-start: 0px;
                -webkit-margin-after: 0px;
                -moz-margin-start: 0px;
                overflow: hidden;
            """),format.raw/*112.13*/("""}"""),format.raw/*112.14*/("""
            """),format.raw/*113.13*/(""".sheet-btn-line"""),format.raw/*113.28*/("""{"""),format.raw/*113.29*/("""
                """),format.raw/*114.17*/("""box-sizing: border-box;
                display: inline-block;
                margin: 0px;
                text-align: center;
                padding: 10px;
                width: 25%;
                border-bottom: solid 1px #DDDDDD;
                -webkit-margin-start: 0px;
                -webkit-margin-after: 0px;
                -moz-margin-start: 0px;
                overflow: hidden;
            """),format.raw/*125.13*/("""}"""),format.raw/*125.14*/("""
            """),format.raw/*126.13*/("""#sheet-btns"""),format.raw/*126.24*/("""{"""),format.raw/*126.25*/("""
                """),format.raw/*127.17*/("""width: 100%;
                overflow: hidden;
                -webkit-touch-callout: none; /* iOS Safari */
                -webkit-user-select: none; /* Safari */
                -khtml-user-select: none; /* Konqueror HTML */
                -moz-user-select: none; /* Firefox */
                -ms-user-select: none; /* Internet Explorer/Edge */
                user-select: none; /* Non-prefixed version, currently
                                  supported by Chrome and Opera */
            """),format.raw/*136.13*/("""}"""),format.raw/*136.14*/("""
            """),format.raw/*137.13*/("""#sheet-btns-container"""),format.raw/*137.34*/("""{"""),format.raw/*137.35*/("""
                """),format.raw/*138.17*/("""width: 70%;
                min-width: 600px;
                display: inline-block;
            """),format.raw/*141.13*/("""}"""),format.raw/*141.14*/("""
            """),format.raw/*142.13*/("""#sheet-container"""),format.raw/*142.29*/("""{"""),format.raw/*142.30*/("""
                """),format.raw/*143.17*/("""display: inline-block;
                margin-top: 60px;
                width: 100%;
            """),format.raw/*146.13*/("""}"""),format.raw/*146.14*/("""
            """),format.raw/*147.13*/(""".footer"""),format.raw/*147.20*/("""{"""),format.raw/*147.21*/("""
                """),format.raw/*148.17*/("""visibility: hidden;
            """),format.raw/*149.13*/("""}"""),format.raw/*149.14*/("""
            """),format.raw/*150.13*/("""#folder-end"""),format.raw/*150.24*/("""{"""),format.raw/*150.25*/("""
                """),format.raw/*151.17*/("""width: 30%;
                box-sizing: border-box;
                display: inline-block;
                margin: 0px;
                padding: 17px;
                border: none;
                border-bottom: solid 1px #DDDDDD;
            """),format.raw/*158.13*/("""}"""),format.raw/*158.14*/("""
    """),format.raw/*159.5*/("""</style>
    <style>
            .red-star"""),format.raw/*161.22*/("""{"""),format.raw/*161.23*/("""
                """),format.raw/*162.17*/("""color: #ff0000;
                font-size: 16px;
                padding-left: 4px;
            """),format.raw/*165.13*/("""}"""),format.raw/*165.14*/("""
    """),format.raw/*166.5*/("""</style>

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
            $(document).ready(function()"""),format.raw/*171.41*/("""{"""),format.raw/*171.42*/("""
                """),format.raw/*172.17*/("""$("#loading-overlay").hide(100);

                $(".val input, .val textarea").each(function () """),format.raw/*174.65*/("""{"""),format.raw/*174.66*/("""
                    """),format.raw/*175.21*/("""setStars($(this));
                """),format.raw/*176.17*/("""}"""),format.raw/*176.18*/(""");

                $(".val input, .val textarea").focus(function () """),format.raw/*178.66*/("""{"""),format.raw/*178.67*/("""
                    """),format.raw/*179.21*/("""$(this).css("""),format.raw/*179.33*/("""{"""),format.raw/*179.34*/(""" """),format.raw/*179.35*/("""'border': 'solid 1px #018FD5'"""),format.raw/*179.64*/("""}"""),format.raw/*179.65*/(""");
                """),format.raw/*180.17*/("""}"""),format.raw/*180.18*/(""");
                $(".val input, .val textarea").focusout(function () """),format.raw/*181.69*/("""{"""),format.raw/*181.70*/("""
                    """),format.raw/*182.21*/("""validate($(this));
                """),format.raw/*183.17*/("""}"""),format.raw/*183.18*/(""");


                function setStars(element) """),format.raw/*186.44*/("""{"""),format.raw/*186.45*/("""
                    """),format.raw/*187.21*/("""if($(element).val() == "")"""),format.raw/*187.47*/("""{"""),format.raw/*187.48*/("""
                        """),format.raw/*188.25*/("""el = $(element).parent().parent().parent();
                        $(el).siblings(".red-star").css('color', '#ff0000');
                    """),format.raw/*190.21*/("""}"""),format.raw/*190.22*/("""else """),format.raw/*190.27*/("""{"""),format.raw/*190.28*/("""
                        """),format.raw/*191.25*/("""el = $(element).parent().parent().parent();
                        $(el).siblings(".red-star").css('color', '#aaaaaa');
                    """),format.raw/*193.21*/("""}"""),format.raw/*193.22*/("""
                """),format.raw/*194.17*/("""}"""),format.raw/*194.18*/("""

                """),format.raw/*196.17*/("""function validate(element) """),format.raw/*196.44*/("""{"""),format.raw/*196.45*/("""
                    """),format.raw/*197.21*/("""if($(element).val() == "")"""),format.raw/*197.47*/("""{"""),format.raw/*197.48*/("""
                        """),format.raw/*198.25*/("""$(element).css("""),format.raw/*198.40*/("""{"""),format.raw/*198.41*/(""" """),format.raw/*198.42*/("""'border': 'solid 1px #ff0000'"""),format.raw/*198.71*/("""}"""),format.raw/*198.72*/(""");
                        el = $(element).parent().parent().parent();
                        $(el).siblings(".red-star").css('color', '#ff0000');
                    """),format.raw/*201.21*/("""}"""),format.raw/*201.22*/("""else """),format.raw/*201.27*/("""{"""),format.raw/*201.28*/("""
                        """),format.raw/*202.25*/("""$(element).css("""),format.raw/*202.40*/("""{"""),format.raw/*202.41*/(""" """),format.raw/*202.42*/("""'border': ''"""),format.raw/*202.54*/("""}"""),format.raw/*202.55*/(""");
                        el = $(element).parent().parent().parent();
                        $(el).siblings(".red-star").css('color', '#aaaaaa');
                    """),format.raw/*205.21*/("""}"""),format.raw/*205.22*/("""
                """),format.raw/*206.17*/("""}"""),format.raw/*206.18*/("""

                """),format.raw/*208.17*/("""var fileList = [];
                var sheets = [
                    """),format.raw/*210.21*/("""{"""),format.raw/*210.22*/("""element: $("#sample-sheet"), btn: $("#sample-btn")"""),format.raw/*210.72*/("""}"""),format.raw/*210.73*/(""",
                    """),format.raw/*211.21*/("""{"""),format.raw/*211.22*/("""element: $("#lc-sheet"),btn: $("#lc-btn")"""),format.raw/*211.63*/("""}"""),format.raw/*211.64*/(""",
                    """),format.raw/*212.21*/("""{"""),format.raw/*212.22*/("""element: $("#ms-one-sheet"),btn: $("#ms-one-btn")"""),format.raw/*212.71*/("""}"""),format.raw/*212.72*/(""",
                    """),format.raw/*213.21*/("""{"""),format.raw/*213.22*/("""element: $("#ms-two-sheet"),btn: $("#ms-two-btn")"""),format.raw/*213.71*/("""}"""),format.raw/*213.72*/("""
                """),format.raw/*214.17*/("""];

                var sheetClick = function(element)"""),format.raw/*216.51*/("""{"""),format.raw/*216.52*/("""
                    """),format.raw/*217.21*/("""for(var e in sheets)"""),format.raw/*217.41*/("""{"""),format.raw/*217.42*/("""
                        """),format.raw/*218.25*/("""if(sheets[e].btn.text() == element.text())"""),format.raw/*218.67*/("""{"""),format.raw/*218.68*/("""
                            """),format.raw/*219.29*/("""sheets[e].element.css("display","block");
                            sheets[e].btn.css("""),format.raw/*220.47*/("""{"""),format.raw/*220.48*/(""""border-bottom": "hidden", "background-color": "inherit""""),format.raw/*220.104*/("""}"""),format.raw/*220.105*/(""");
                        """),format.raw/*221.25*/("""}"""),format.raw/*221.26*/(""" """),format.raw/*221.27*/("""else """),format.raw/*221.32*/("""{"""),format.raw/*221.33*/("""
                            """),format.raw/*222.29*/("""sheets[e].element.css("display", "none");
                            sheets[e].btn.css("""),format.raw/*223.47*/("""{"""),format.raw/*223.48*/(""""border-bottom": "solid 1px #dddddd", "background-color": "#f0f0f0""""),format.raw/*223.115*/("""}"""),format.raw/*223.116*/(""");
                        """),format.raw/*224.25*/("""}"""),format.raw/*224.26*/("""
                    """),format.raw/*225.21*/("""}"""),format.raw/*225.22*/("""
                """),format.raw/*226.17*/("""}"""),format.raw/*226.18*/(""";

                $(".sheet-btn").click(function (e) """),format.raw/*228.52*/("""{"""),format.raw/*228.53*/("""
                    """),format.raw/*229.21*/("""sheetClick($(e.target));
                """),format.raw/*230.17*/("""}"""),format.raw/*230.18*/(""");

                if ($("#sample-btn").length)"""),format.raw/*232.45*/("""{"""),format.raw/*232.46*/("""
                    """),format.raw/*233.21*/("""sheetClick($("#sample-btn"));
                """),format.raw/*234.17*/("""}"""),format.raw/*234.18*/(""" """),format.raw/*234.19*/("""else if ($("#lc-btn").length)"""),format.raw/*234.48*/("""{"""),format.raw/*234.49*/("""
                    """),format.raw/*235.21*/("""sheetClick($("#lc-btn"));
                """),format.raw/*236.17*/("""}"""),format.raw/*236.18*/(""" """),format.raw/*236.19*/("""else if ($("#ms-one-btn").length)"""),format.raw/*236.52*/("""{"""),format.raw/*236.53*/("""
                    """),format.raw/*237.21*/("""sheetClick($("#ms-one-btn"));
                """),format.raw/*238.17*/("""}"""),format.raw/*238.18*/("""


                """),format.raw/*241.17*/("""$(window).keydown(function(event)"""),format.raw/*241.50*/("""{"""),format.raw/*241.51*/("""
                    """),format.raw/*242.21*/("""if(event.keyCode == 13) """),format.raw/*242.45*/("""{"""),format.raw/*242.46*/("""
                        """),format.raw/*243.25*/("""event.preventDefault();
                        return false;
                    """),format.raw/*245.21*/("""}"""),format.raw/*245.22*/("""
                """),format.raw/*246.17*/("""}"""),format.raw/*246.18*/(""");

                // Check if browser is Firefox, if so add date picker
                if(typeof InstallTrigger !== 'undefined')"""),format.raw/*249.58*/("""{"""),format.raw/*249.59*/("""
                    """),format.raw/*250.21*/("""$( function() """),format.raw/*250.35*/("""{"""),format.raw/*250.36*/("""
                        """),format.raw/*251.25*/("""$(".date-stamp dd input").datepicker();
                        $(".date-stamp dd input").datepicker("option", "dateFormat", "yy-mm-dd");
                    """),format.raw/*253.21*/("""}"""),format.raw/*253.22*/(""" """),format.raw/*253.23*/(""");
                """),format.raw/*254.17*/("""}"""),format.raw/*254.18*/("""

                """),format.raw/*256.17*/("""$(".date-stamp dd input").change(function (e) """),format.raw/*256.63*/("""{"""),format.raw/*256.64*/("""
                    """),format.raw/*257.21*/("""var text = $(e.target).val();
                    $(".date-stamp dd input").each(function() """),format.raw/*258.63*/("""{"""),format.raw/*258.64*/("""
                        """),format.raw/*259.25*/("""$(this).val(text);
                        setStars($(this));
                    """),format.raw/*261.21*/("""}"""),format.raw/*261.22*/(""");
                """),format.raw/*262.17*/("""}"""),format.raw/*262.18*/(""");
                $(".responsible dd input").change(function (e) """),format.raw/*263.64*/("""{"""),format.raw/*263.65*/("""
                    """),format.raw/*264.21*/("""var text = $(e.target).val();
                    $(".responsible dd input").each(function() """),format.raw/*265.64*/("""{"""),format.raw/*265.65*/("""
                        """),format.raw/*266.25*/("""$(this).val(text);
                        setStars($(this));
                    """),format.raw/*268.21*/("""}"""),format.raw/*268.22*/(""");
                """),format.raw/*269.17*/("""}"""),format.raw/*269.18*/(""");
                $(".affiliation dd input").change(function (e) """),format.raw/*270.64*/("""{"""),format.raw/*270.65*/("""
                    """),format.raw/*271.21*/("""var text = $(e.target).val();
                    $(".affiliation dd input").each(function() """),format.raw/*272.64*/("""{"""),format.raw/*272.65*/("""
                        """),format.raw/*273.25*/("""$(this).val(text);
                        setStars($(this));
                    """),format.raw/*275.21*/("""}"""),format.raw/*275.22*/(""");
                """),format.raw/*276.17*/("""}"""),format.raw/*276.18*/(""");
                $(".contactInformation dd input").change(function (e) """),format.raw/*277.71*/("""{"""),format.raw/*277.72*/("""
                    """),format.raw/*278.21*/("""var text = $(e.target).val();
                    $(".contactInformation dd input").each(function() """),format.raw/*279.71*/("""{"""),format.raw/*279.72*/("""
                        """),format.raw/*280.25*/("""$(this).val(text);
                        setStars($(this));
                    """),format.raw/*282.21*/("""}"""),format.raw/*282.22*/(""");
                """),format.raw/*283.17*/("""}"""),format.raw/*283.18*/(""");

                $(".table-fragment-softwareTwo-input-container").click(function()"""),format.raw/*285.82*/("""{"""),format.raw/*285.83*/("""
                    """),format.raw/*286.21*/("""setUpSoftwareList($(".table-fragment-softwareTwo-column0 select"), $(".table-fragment-softwareThree-column0 select"));
                """),format.raw/*287.17*/("""}"""),format.raw/*287.18*/(""");
                $(".table-fragment-softwareTwo-add").click(function()"""),format.raw/*288.70*/("""{"""),format.raw/*288.71*/("""
                    """),format.raw/*289.21*/("""setUpSoftwareList($(".table-fragment-softwareTwo-column0 select"), $(".table-fragment-softwareThree-column0 select"));
                """),format.raw/*290.17*/("""}"""),format.raw/*290.18*/(""");
                $(".table-fragment-softwareThree-add").click(function()"""),format.raw/*291.72*/("""{"""),format.raw/*291.73*/("""
                    """),format.raw/*292.21*/("""setUpSoftwareList($(".table-fragment-softwareTwo-column0 select"), $(".table-fragment-softwareThree-column0 select"), true);
                """),format.raw/*293.17*/("""}"""),format.raw/*293.18*/(""");
                $(".table-fragment-softwareFour-input-container").click(function()"""),format.raw/*294.83*/("""{"""),format.raw/*294.84*/("""
                    """),format.raw/*295.21*/("""setUpSoftwareList($(".table-fragment-softwareFour-column0 select"), $(".table-fragment-softwareFive-column0 select"));
                """),format.raw/*296.17*/("""}"""),format.raw/*296.18*/(""");
                $(".table-fragment-softwareFour-add").click(function()"""),format.raw/*297.71*/("""{"""),format.raw/*297.72*/("""
                    """),format.raw/*298.21*/("""setUpSoftwareList($(".table-fragment-softwareFour-column0 select"), $(".table-fragment-softwareFive-column0 select"));
                """),format.raw/*299.17*/("""}"""),format.raw/*299.18*/(""");
                $(".table-fragment-softwareFive-add").click(function()"""),format.raw/*300.71*/("""{"""),format.raw/*300.72*/("""
                    """),format.raw/*301.21*/("""setUpSoftwareList($(".table-fragment-softwareFour-column0 select"), $(".table-fragment-softwareFive-column0 select"), true);
                """),format.raw/*302.17*/("""}"""),format.raw/*302.18*/(""");
                $(".table-fragment-softwareSix-input-container").click(function()"""),format.raw/*303.82*/("""{"""),format.raw/*303.83*/("""
                    """),format.raw/*304.21*/("""setUpSoftwareList($(".table-fragment-softwareSix-column0 select"), $(".table-fragment-softwareSeven-column0 select"));
                """),format.raw/*305.17*/("""}"""),format.raw/*305.18*/(""");
                $(".table-fragment-softwareSix-add").click(function()"""),format.raw/*306.70*/("""{"""),format.raw/*306.71*/("""
                    """),format.raw/*307.21*/("""setUpSoftwareList($(".table-fragment-softwareSix-column0 select"), $(".table-fragment-softwareSeven-column0 select"));
                """),format.raw/*308.17*/("""}"""),format.raw/*308.18*/(""");
                $(".table-fragment-softwareSeven-add").click(function()"""),format.raw/*309.72*/("""{"""),format.raw/*309.73*/("""
                    """),format.raw/*310.21*/("""setUpSoftwareList($(".table-fragment-softwareSix-column0 select"), $(".table-fragment-softwareSeven-column0 select"), true);
                """),format.raw/*311.17*/("""}"""),format.raw/*311.18*/(""");

                function setUpSoftwareList(fromElement, toElement, last) """),format.raw/*313.74*/("""{"""),format.raw/*313.75*/("""
                    """),format.raw/*314.21*/("""var opList = [];
                    fromElement.each(function () """),format.raw/*315.50*/("""{"""),format.raw/*315.51*/("""
                        """),format.raw/*316.25*/("""opList.push($(this).val());
                    """),format.raw/*317.21*/("""}"""),format.raw/*317.22*/(""");

                    if(last)"""),format.raw/*319.29*/("""{"""),format.raw/*319.30*/("""
                        """),format.raw/*320.25*/("""toElement.last().find('option').remove();
                        for (var o in opList)"""),format.raw/*321.46*/("""{"""),format.raw/*321.47*/("""
                            """),format.raw/*322.29*/("""toElement.last().append('<option value="' + opList[o] + '">' + opList[o] + '</option>');
                        """),format.raw/*323.25*/("""}"""),format.raw/*323.26*/("""
                    """),format.raw/*324.21*/("""}"""),format.raw/*324.22*/(""" """),format.raw/*324.23*/("""else """),format.raw/*324.28*/("""{"""),format.raw/*324.29*/("""
                        """),format.raw/*325.25*/("""toElement.each(function () """),format.raw/*325.52*/("""{"""),format.raw/*325.53*/("""
                            """),format.raw/*326.29*/("""if ($.inArray($(this).val(), opList) != -1)"""),format.raw/*326.72*/("""{"""),format.raw/*326.73*/("""
                                """),format.raw/*327.33*/("""var selected = $(this).val();
                                $(this).find('option').remove();
                                for (var o in opList)"""),format.raw/*329.54*/("""{"""),format.raw/*329.55*/("""
                                    """),format.raw/*330.37*/("""$(this).append('<option value="' + opList[o] + '">' + opList[o] + '</option>');
                                """),format.raw/*331.33*/("""}"""),format.raw/*331.34*/("""
                                """),format.raw/*332.33*/("""$(this).val(selected);
                            """),format.raw/*333.29*/("""}"""),format.raw/*333.30*/(""" """),format.raw/*333.31*/("""else """),format.raw/*333.36*/("""{"""),format.raw/*333.37*/("""
                                """),format.raw/*334.33*/("""$(this).find('option').remove();
                                for (var o in opList)"""),format.raw/*335.54*/("""{"""),format.raw/*335.55*/("""
                                    """),format.raw/*336.37*/("""$(this).append('<option value="' + opList[o] + '">' + opList[o] + '</option>');
                                """),format.raw/*337.33*/("""}"""),format.raw/*337.34*/("""
                            """),format.raw/*338.29*/("""}"""),format.raw/*338.30*/("""

                            """),format.raw/*340.29*/("""console.log($(this).val());
                        """),format.raw/*341.25*/("""}"""),format.raw/*341.26*/(""");
                    """),format.raw/*342.21*/("""}"""),format.raw/*342.22*/("""

                """),format.raw/*344.17*/("""}"""),format.raw/*344.18*/("""

            """),format.raw/*346.13*/("""}"""),format.raw/*346.14*/(""");
    </script>
""")))}),format.raw/*348.2*/("""
"""))
      }
    }
  }

  def render(forms:Map[String, Form[_$1] forSome { 
   type _$1 >: _root_.scala.Nothing <: _root_.scala.Any
}],sampleOptions:Map[String, Map[String, String]],tables:List[TableFragment],userProvider:UserProvider): play.twirl.api.HtmlFormat.Appendable = apply(forms,sampleOptions,tables,userProvider)

  def f:((Map[String, Form[_$1] forSome { 
   type _$1 >: _root_.scala.Nothing <: _root_.scala.Any
}],Map[String, Map[String, String]],List[TableFragment],UserProvider) => play.twirl.api.HtmlFormat.Appendable) = (forms,sampleOptions,tables,userProvider) => apply(forms,sampleOptions,tables,userProvider)

  def ref: this.type = this

}


}
}

/**/
object generateUnicarbForm extends generateUnicarbForm_Scope0.generateUnicarbForm_Scope1.generateUnicarbForm
              /*
                  -- GENERATED --
                  DATE: Wed Jun 19 09:50:52 UTC 2019
                  SOURCE: /app/app/views/generateFiles/generateUnicarbForm.scala.html
                  HASH: 7d4e5d8a66812a737fec400a662ae68fc385e4e5
                  MATRIX: 1123->147|1377->306|1405->309|1447->343|1485->344|1518->350|1606->411|1621->417|1691->465|3202->1949|3217->1955|3280->2009|3320->2011|3361->2024|3612->2248|3652->2279|3691->2280|3744->2305|3875->2406|3911->2433|3950->2434|4003->2459|4123->2549|4159->2576|4198->2577|4251->2602|4455->2776|4495->2807|4534->2808|4587->2833|4706->2922|4742->2949|4782->2951|4835->2976|4954->3065|4990->3092|5030->3094|5083->3119|5281->3286|5516->3494|5556->3525|5595->3526|5624->3528|5638->3533|5757->3630|5791->3632|5840->3653|5935->3721|5971->3748|6010->3749|6039->3751|6053->3756|6138->3819|6172->3821|6221->3842|6320->3914|6356->3941|6395->3942|6424->3944|6438->3949|6551->4040|6585->4042|6634->4063|6733->4135|6769->4162|6808->4163|6837->4165|6851->4170|6965->4262|6999->4264|7048->4285|7138->4344|7170->4349|7248->4399|7277->4400|7322->4417|7436->4503|7465->4504|7506->4517|7540->4523|7569->4524|7614->4541|7919->4818|7948->4819|7989->4832|8027->4842|8056->4843|8101->4860|8615->5345|8645->5346|8687->5359|8731->5374|8761->5375|8807->5392|9245->5801|9275->5802|9317->5815|9357->5826|9387->5827|9433->5844|9961->6343|9991->6344|10033->6357|10083->6378|10113->6379|10159->6396|10285->6493|10315->6494|10357->6507|10402->6523|10432->6524|10478->6541|10605->6639|10635->6640|10677->6653|10713->6660|10743->6661|10789->6678|10850->6710|10880->6711|10922->6724|10962->6735|10992->6736|11038->6753|11310->6996|11340->6997|11373->7002|11444->7044|11474->7045|11520->7062|11645->7158|11675->7159|11708->7164|11964->7391|11994->7392|12040->7409|12167->7507|12197->7508|12247->7529|12311->7564|12341->7565|12439->7634|12469->7635|12519->7656|12560->7668|12590->7669|12620->7670|12678->7699|12708->7700|12756->7719|12786->7720|12886->7791|12916->7792|12966->7813|13030->7848|13060->7849|13137->7897|13167->7898|13217->7919|13272->7945|13302->7946|13356->7971|13526->8112|13556->8113|13590->8118|13620->8119|13674->8144|13844->8285|13874->8286|13920->8303|13950->8304|13997->8322|14053->8349|14083->8350|14133->8371|14188->8397|14218->8398|14272->8423|14316->8438|14346->8439|14376->8440|14434->8469|14464->8470|14661->8638|14691->8639|14725->8644|14755->8645|14809->8670|14853->8685|14883->8686|14913->8687|14954->8699|14984->8700|15181->8868|15211->8869|15257->8886|15287->8887|15334->8905|15433->8975|15463->8976|15542->9026|15572->9027|15623->9049|15653->9050|15723->9091|15753->9092|15804->9114|15834->9115|15912->9164|15942->9165|15993->9187|16023->9188|16101->9237|16131->9238|16177->9255|16260->9309|16290->9310|16340->9331|16389->9351|16419->9352|16473->9377|16544->9419|16574->9420|16632->9449|16749->9537|16779->9538|16865->9594|16896->9595|16952->9622|16982->9623|17012->9624|17046->9629|17076->9630|17134->9659|17251->9747|17281->9748|17378->9815|17409->9816|17465->9843|17495->9844|17545->9865|17575->9866|17621->9883|17651->9884|17734->9938|17764->9939|17814->9960|17884->10001|17914->10002|17991->10050|18021->10051|18071->10072|18146->10118|18176->10119|18206->10120|18264->10149|18294->10150|18344->10171|18415->10213|18445->10214|18475->10215|18537->10248|18567->10249|18617->10270|18692->10316|18722->10317|18770->10336|18832->10369|18862->10370|18912->10391|18965->10415|18995->10416|19049->10441|19160->10523|19190->10524|19236->10541|19266->10542|19426->10673|19456->10674|19506->10695|19549->10709|19579->10710|19633->10735|19820->10893|19850->10894|19880->10895|19928->10914|19958->10915|20005->10933|20080->10979|20110->10980|20160->11001|20281->11093|20311->11094|20365->11119|20476->11201|20506->11202|20554->11221|20584->11222|20679->11288|20709->11289|20759->11310|20881->11403|20911->11404|20965->11429|21076->11511|21106->11512|21154->11531|21184->11532|21279->11598|21309->11599|21359->11620|21481->11713|21511->11714|21565->11739|21676->11821|21706->11822|21754->11841|21784->11842|21886->11915|21916->11916|21966->11937|22095->12037|22125->12038|22179->12063|22290->12145|22320->12146|22368->12165|22398->12166|22512->12251|22542->12252|22592->12273|22756->12408|22786->12409|22887->12481|22917->12482|22967->12503|23131->12638|23161->12639|23264->12713|23294->12714|23344->12735|23514->12876|23544->12877|23658->12962|23688->12963|23738->12984|23902->13119|23932->13120|24034->13193|24064->13194|24114->13215|24278->13350|24308->13351|24410->13424|24440->13425|24490->13446|24660->13587|24690->13588|24803->13672|24833->13673|24883->13694|25047->13829|25077->13830|25178->13902|25208->13903|25258->13924|25422->14059|25452->14060|25555->14134|25585->14135|25635->14156|25805->14297|25835->14298|25941->14375|25971->14376|26021->14397|26116->14463|26146->14464|26200->14489|26277->14537|26307->14538|26368->14570|26398->14571|26452->14596|26568->14683|26598->14684|26656->14713|26798->14826|26828->14827|26878->14848|26908->14849|26938->14850|26972->14855|27002->14856|27056->14881|27112->14908|27142->14909|27200->14938|27272->14981|27302->14982|27364->15015|27541->15163|27571->15164|27637->15201|27778->15313|27808->15314|27870->15347|27950->15398|27980->15399|28010->15400|28044->15405|28074->15406|28136->15439|28251->15525|28281->15526|28347->15563|28488->15675|28518->15676|28576->15705|28606->15706|28665->15736|28746->15788|28776->15789|28828->15812|28858->15813|28905->15831|28935->15832|28978->15846|29008->15847|29057->15865
                  LINES: 34->4|42->7|44->9|44->9|44->9|46->11|46->11|46->11|46->11|75->40|75->40|75->40|75->40|76->41|81->46|81->46|81->46|82->47|83->48|83->48|83->48|84->49|85->50|85->50|85->50|86->51|88->53|88->53|88->53|89->54|90->55|90->55|90->55|91->56|92->57|92->57|92->57|93->58|95->60|102->67|102->67|102->67|102->67|102->67|102->67|102->67|103->68|105->70|105->70|105->70|105->70|105->70|105->70|105->70|106->71|108->73|108->73|108->73|108->73|108->73|108->73|108->73|109->74|111->76|111->76|111->76|111->76|111->76|111->76|111->76|112->77|116->81|117->82|119->84|119->84|120->85|123->88|123->88|124->89|124->89|124->89|125->90|132->97|132->97|133->98|133->98|133->98|134->99|147->112|147->112|148->113|148->113|148->113|149->114|160->125|160->125|161->126|161->126|161->126|162->127|171->136|171->136|172->137|172->137|172->137|173->138|176->141|176->141|177->142|177->142|177->142|178->143|181->146|181->146|182->147|182->147|182->147|183->148|184->149|184->149|185->150|185->150|185->150|186->151|193->158|193->158|194->159|196->161|196->161|197->162|200->165|200->165|201->166|206->171|206->171|207->172|209->174|209->174|210->175|211->176|211->176|213->178|213->178|214->179|214->179|214->179|214->179|214->179|214->179|215->180|215->180|216->181|216->181|217->182|218->183|218->183|221->186|221->186|222->187|222->187|222->187|223->188|225->190|225->190|225->190|225->190|226->191|228->193|228->193|229->194|229->194|231->196|231->196|231->196|232->197|232->197|232->197|233->198|233->198|233->198|233->198|233->198|233->198|236->201|236->201|236->201|236->201|237->202|237->202|237->202|237->202|237->202|237->202|240->205|240->205|241->206|241->206|243->208|245->210|245->210|245->210|245->210|246->211|246->211|246->211|246->211|247->212|247->212|247->212|247->212|248->213|248->213|248->213|248->213|249->214|251->216|251->216|252->217|252->217|252->217|253->218|253->218|253->218|254->219|255->220|255->220|255->220|255->220|256->221|256->221|256->221|256->221|256->221|257->222|258->223|258->223|258->223|258->223|259->224|259->224|260->225|260->225|261->226|261->226|263->228|263->228|264->229|265->230|265->230|267->232|267->232|268->233|269->234|269->234|269->234|269->234|269->234|270->235|271->236|271->236|271->236|271->236|271->236|272->237|273->238|273->238|276->241|276->241|276->241|277->242|277->242|277->242|278->243|280->245|280->245|281->246|281->246|284->249|284->249|285->250|285->250|285->250|286->251|288->253|288->253|288->253|289->254|289->254|291->256|291->256|291->256|292->257|293->258|293->258|294->259|296->261|296->261|297->262|297->262|298->263|298->263|299->264|300->265|300->265|301->266|303->268|303->268|304->269|304->269|305->270|305->270|306->271|307->272|307->272|308->273|310->275|310->275|311->276|311->276|312->277|312->277|313->278|314->279|314->279|315->280|317->282|317->282|318->283|318->283|320->285|320->285|321->286|322->287|322->287|323->288|323->288|324->289|325->290|325->290|326->291|326->291|327->292|328->293|328->293|329->294|329->294|330->295|331->296|331->296|332->297|332->297|333->298|334->299|334->299|335->300|335->300|336->301|337->302|337->302|338->303|338->303|339->304|340->305|340->305|341->306|341->306|342->307|343->308|343->308|344->309|344->309|345->310|346->311|346->311|348->313|348->313|349->314|350->315|350->315|351->316|352->317|352->317|354->319|354->319|355->320|356->321|356->321|357->322|358->323|358->323|359->324|359->324|359->324|359->324|359->324|360->325|360->325|360->325|361->326|361->326|361->326|362->327|364->329|364->329|365->330|366->331|366->331|367->332|368->333|368->333|368->333|368->333|368->333|369->334|370->335|370->335|371->336|372->337|372->337|373->338|373->338|375->340|376->341|376->341|377->342|377->342|379->344|379->344|381->346|381->346|383->348
                  -- GENERATED --
              */
          