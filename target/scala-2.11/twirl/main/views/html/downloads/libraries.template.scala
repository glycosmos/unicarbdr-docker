
package views.html.downloads

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object libraries_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

     object libraries_Scope1 {
import service.UserProvider

class libraries extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[UserProvider,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*2.2*/(userProvider: UserProvider):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.30*/("""
"""),_display_(/*3.2*/main(userProvider, "Libraries UniCarb-DB")/*3.44*/{_display_(Seq[Any](format.raw/*3.45*/("""

"""),format.raw/*5.1*/("""<script>
    $("#libLink").addClass("active");
</script>

<div class="container-fluid content">
    <ul class="breadcrumb">
        <li><i class="icon-home" ></i><a href="/"> UniCarb-DR</a> <span class="divider">></span></li>
<!--        <li><i class="icon-home" ></i><a href="/"> UniCarb-DR</a> <span class="divider">></span></li>-->
        <li class="active"><i class="icon-download" ></i> Downloads<span class="divider"></span></li>
    </ul>

    <div class="row-fluid info">
        <div class="row-fluid">
            <div class="page-header row-fluid">
                <h1 class="subheader span8">The UniCarb-DR library</h1>
            </div>
            <p>The first version of the UniCarb-DR library contains 1069 negative ion spectra (704 unique structures)</p>

            <p>You can download the library in either zip or tar formats. </p>
            <ul class='nav nav-pills nav-stacked clearfix'>
                <li>
                    <a href=""""),_display_(/*26.31*/routes/*26.37*/.Assets.versioned("library/UniCarbMSP.zip")),format.raw/*26.80*/("""">
                    Msp format (zip)
                    </a>
                    <a href=""""),_display_(/*29.31*/routes/*29.37*/.Assets.versioned("library/UniCarbMSP.msp.tar.gz")),format.raw/*29.87*/("""">
                    Msp format (tar)
                    </a>
                    <a href=""""),_display_(/*32.31*/routes/*32.37*/.Assets.versioned("library/UniCarbNIST.zip")),format.raw/*32.81*/("""">
                    NIST format (zip)
                    </a>
                    <a href=""""),_display_(/*35.31*/routes/*35.37*/.Assets.versioned("library/UniCarbNIST.tar.gz")),format.raw/*35.84*/("""">
                    NIST format (tar)
                    </a>

                </li>
            </ul>
            <p>Release date: 19-08-2016</p>


        </div>
    </div>
</div>

""")))}),format.raw/*48.2*/("""
"""))
      }
    }
  }

  def render(userProvider:UserProvider): play.twirl.api.HtmlFormat.Appendable = apply(userProvider)

  def f:((UserProvider) => play.twirl.api.HtmlFormat.Appendable) = (userProvider) => apply(userProvider)

  def ref: this.type = this

}


}
}

/**/
object libraries extends libraries_Scope0.libraries_Scope1.libraries
              /*
                  -- GENERATED --
                  DATE: Wed Jun 19 09:50:52 UTC 2019
                  SOURCE: /app/app/views/downloads/libraries.scala.html
                  HASH: 1f6f3a66e66662f8f4dd2c04275ebc67c7e46c0b
                  MATRIX: 829->30|952->58|979->60|1029->102|1067->103|1095->105|2087->1070|2102->1076|2166->1119|2288->1214|2303->1220|2374->1270|2496->1365|2511->1371|2576->1415|2699->1511|2714->1517|2782->1564|3000->1752
                  LINES: 30->2|35->2|36->3|36->3|36->3|38->5|59->26|59->26|59->26|62->29|62->29|62->29|65->32|65->32|65->32|68->35|68->35|68->35|81->48
                  -- GENERATED --
              */
          