
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object main_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

     object main_Scope1 {
import service.UserProvider

class main extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template3[UserProvider,String,Html,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*2.2*/(userProvider: UserProvider, title: String)(content: Html):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.60*/("""

"""),format.raw/*4.1*/("""<html>
<head>
    """),format.raw/*6.58*/("""
    """),format.raw/*7.5*/("""<title>"""),_display_(/*7.13*/title),format.raw/*7.18*/("""</title>
    <link rel="stylesheet" type="text/css" media="screen" href=""""),_display_(/*8.66*/routes/*8.72*/.Assets.versioned("stylesheets/bootstrap.min.css")),format.raw/*8.122*/(""""/>
    <link rel="stylesheet" type="text/css" media="screen" href=""""),_display_(/*9.66*/routes/*9.72*/.Assets.versioned("stylesheets/unicarbkb.css")),format.raw/*9.118*/(""""/>
    <link rel="stylesheet" type="text/css" media="screen" href=""""),_display_(/*10.66*/routes/*10.72*/.Assets.versioned("stylesheets/styles.css")),format.raw/*10.115*/(""""/>
    <link rel="stylesheet" type="text/css" media="screen" href=""""),_display_(/*11.66*/routes/*11.72*/.Assets.versioned("stylesheets/newUnicarb.css")),format.raw/*11.119*/(""""/>

    <link rel="shortcut icon" type="image/png" href=""""),_display_(/*13.55*/routes/*13.61*/.Assets.versioned("images/favicon.ico")),format.raw/*13.100*/("""">

    <script src=""""),_display_(/*15.19*/routes/*15.25*/.Assets.versioned("javascripts/jquery.js")),format.raw/*15.67*/(""""></script>
    <script type="text/javascript" src=""""),_display_(/*16.42*/routes/*16.48*/.Assets.versioned("javascripts/bootstrap.js")),format.raw/*16.93*/(""""></script>
    <script src=""""),_display_(/*17.19*/routes/*17.25*/.Assets.versioned("javascripts/ganalytics.js")),format.raw/*17.71*/(""""></script>
    <script src=""""),_display_(/*18.19*/routes/*18.25*/.Assets.versioned("javascripts/application.js")),format.raw/*18.72*/(""""></script>
    <script src="https://www.google.com/recaptcha/api.js?hl=ja" async="async" defer="defer"></script>
    <script type="text/javascript">
      var testCallback = function(code) """),format.raw/*21.41*/("""{"""),format.raw/*21.42*/("""
             """),format.raw/*22.14*/("""if(code != "")"""),format.raw/*22.28*/("""{"""),format.raw/*22.29*/("""
                      """),format.raw/*23.23*/("""$('#submit').attr('disabled',false);
             """),format.raw/*24.14*/("""}"""),format.raw/*24.15*/("""
      """),format.raw/*25.7*/("""}"""),format.raw/*25.8*/(""";
    </script>


    <script type="text/javascript">
        var switchTo5x=true;
    </script>
    <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
    <script type="text/javascript">stLight.options("""),format.raw/*33.52*/("""{"""),format.raw/*33.53*/("""publisher: "966b5d99-38c2-4ba6-905f-41929669e974", doNotHash: false, doNotCopy: false, hashAddressBar: false"""),format.raw/*33.161*/("""}"""),format.raw/*33.162*/(""");</script>

    <script language="JavaScript" type="text/javascript">
        $(function()"""),format.raw/*36.21*/("""{"""),format.raw/*36.22*/("""
            """),format.raw/*37.13*/("""$('#slideCarousel').carousel("""),format.raw/*37.42*/("""{"""),format.raw/*37.43*/("""
              """),format.raw/*38.15*/("""interval: 4000
            """),format.raw/*39.13*/("""}"""),format.raw/*39.14*/(""")
          """),format.raw/*40.11*/("""}"""),format.raw/*40.12*/(""");
    </script>

</head>
<body>
"""),_display_(/*45.2*/views/*45.7*/.html.headerfooter.mainheader(userProvider)),format.raw/*45.50*/("""
"""),_display_(/*46.2*/content),format.raw/*46.9*/("""
"""),_display_(/*47.2*/views/*47.7*/.html.headerfooter.mainfooter()),format.raw/*47.38*/("""
"""),format.raw/*48.1*/("""</body>
</html>
"""))
      }
    }
  }

  def render(userProvider:UserProvider,title:String,content:Html): play.twirl.api.HtmlFormat.Appendable = apply(userProvider,title)(content)

  def f:((UserProvider,String) => (Html) => play.twirl.api.HtmlFormat.Appendable) = (userProvider,title) => (content) => apply(userProvider,title)(content)

  def ref: this.type = this

}


}
}

/**/
object main extends main_Scope0.main_Scope1.main
              /*
                  -- GENERATED --
                  DATE: Wed Jun 19 09:50:47 UTC 2019
                  SOURCE: /app/app/views/main.scala.html
                  HASH: f798a6c20cb41be7c54ba39bf36e64f83a4bd1d3
                  MATRIX: 816->30|969->88|997->90|1042->161|1073->166|1107->174|1132->179|1232->253|1246->259|1317->309|1412->378|1426->384|1493->430|1589->499|1604->505|1669->548|1765->617|1780->623|1849->670|1935->729|1950->735|2011->774|2060->796|2075->802|2138->844|2218->897|2233->903|2299->948|2356->978|2371->984|2438->1030|2495->1060|2510->1066|2578->1113|2796->1303|2825->1304|2867->1318|2909->1332|2938->1333|2989->1356|3067->1406|3096->1407|3130->1414|3158->1415|3426->1655|3455->1656|3592->1764|3622->1765|3741->1856|3770->1857|3811->1870|3868->1899|3897->1900|3940->1915|3995->1942|4024->1943|4064->1955|4093->1956|4153->1990|4166->1995|4230->2038|4258->2040|4285->2047|4313->2049|4326->2054|4378->2085|4406->2086
                  LINES: 30->2|35->2|37->4|39->6|40->7|40->7|40->7|41->8|41->8|41->8|42->9|42->9|42->9|43->10|43->10|43->10|44->11|44->11|44->11|46->13|46->13|46->13|48->15|48->15|48->15|49->16|49->16|49->16|50->17|50->17|50->17|51->18|51->18|51->18|54->21|54->21|55->22|55->22|55->22|56->23|57->24|57->24|58->25|58->25|66->33|66->33|66->33|66->33|69->36|69->36|70->37|70->37|70->37|71->38|72->39|72->39|73->40|73->40|78->45|78->45|78->45|79->46|79->46|80->47|80->47|80->47|81->48
                  -- GENERATED --
              */
          