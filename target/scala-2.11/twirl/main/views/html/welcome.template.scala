
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object welcome_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class welcome extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template5[Int,Int,Int,Int,Int,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(referenceCount: Int, spectraCount: Int, structureCount: Int, tissueCount: Int, taxonomyCount: Int):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.101*/("""

"""),format.raw/*3.1*/("""<script>
    $("#homeLink").addClass("active");
</script>
<div class="home">
    <div class="container-fluid content">
        <div class="row-fluid">
            <div class="span12 centered">
                <h3 class="logo-text">
                    The Glycomic MS Database and Repository
                </h3>
            </div>

            <div class="span12 centered">
                <h3 class="subheader">
                    <a href=""""),_display_(/*17.31*/routes/*17.37*/.SearchController.search()),format.raw/*17.63*/("""">
                        <span class="start">Search UniCarb-DR</span>
                    <!-- <span class="start">Search UniCarb-DR</span>  -->
                    </a>
                </h3>
            </div>
            <div class="span12 centered">
                <p>
                    Carbohydrates are the most common but less known biological material on earth. This deficit can partly be explained by the lack of glycodatabases and glycobioinformatics.
                    UniCarb-DR is an example of what can be achieved with a lot of dedication, cross-disciplinary collaborations and lack of monetary resources.
                    In 2009 the UniCarb-DB was initiated in order to meet in-house need to store structural and MS-glycomic data, and has now grown to be one of the largest experimental glycomic MS databases,
                    generously supported by several national and European funding for continued growth and development.
                </p>
                <p>
                    Glycans nomenclature is available in <a href="https://www.ncbi.nlm.nih.gov/books/NBK310273" target="_blank">Essentials of Glycobiology</a>.
                </p>
                <h3>
                    Let's explore the glycans!
                </h3>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span8 leftContent">
                <div class="row-fluid">

                    <div class="widget">
                        <h3 class="lead">
                            Search categories
                            <small>by following a link below</small>
                        </h3>
                        <div class="row-fluid">
                            <div class="span2 taxonomy-count">
                                <a href="/search#tab_advanced">
                                    <span class="count">"""),_display_(/*50.58*/taxonomyCount),format.raw/*50.71*/("""</span>
                                    <span class="desc">Taxonomies</span>
                                </a>
                            </div>
                            <div class="span2 tissue-count">
                                <a href="/search#tab_advanced">
                                    <span class="count">"""),_display_(/*56.58*/tissueCount),format.raw/*56.69*/("""</span>
                                    <span class="desc">Tissues</span>
                                </a>
                            </div>
                            <div class="span2 references-count">
                                <a href="/search#tab_advanced">
                                    <span class="count">"""),_display_(/*62.58*/referenceCount),format.raw/*62.72*/("""</span>
                                    <span class="desc">References</span>
                                </a>
                            </div>
                            <div class="span2 structures-count">
                                <a href="/search#tab_advanced">
                                    <span class="count">"""),_display_(/*68.58*/structureCount),format.raw/*68.72*/("""</span>
                                    <span class="desc">Structures</span>
                                </a>
                            </div>
                            <div class="span2 spectra-count">
                                <a href="/search#tab_msms">
                                    <span class="count">"""),_display_(/*74.58*/spectraCount),format.raw/*74.70*/("""</span>
                                    <span class="desc">Spectra</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="slideCarousel" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#slideCarousel" data-slide-to="0" class="active"></li>
                        <li data-target="#slideCarousel" data-slide-to="1"></li>
                        <li data-target="#slideCarousel" data-slide-to="2"></li>
                        <li data-target="#slideCarousel" data-slide-to="3"></li>
                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        <div class="item active">
                            <img src="../assets/images/slides/structures.png"
                                 alt="Structures">
                            <div class="carousel-caption">
                                <h3>Structures</h3>
                                <p>Our LC MS/MS library contains both N- and O- linked glycans released from
                                    glycoproteins, and each entry is supported by published references.</p>
                            </div>
                        </div>

                        <div class="item">
                            <img src="../assets/images/slides/spectrum.png"
                                 alt="Spectrum">
                            <div class="carousel-caption">
                                <h3>Spectra</h3>
                                <p>UniCarb-DB provides access to a collection of manually annotated spectra that have
                                    been experimentally verified in order to increase the speed and accuracy of glycan
                                    assignment. </p>
                            </div>
                        </div>

                        <div class="item">
                            <img src="../assets/images/slides/gwb.png"
                                 alt="GlycoWorkbench">
                            <div class="carousel-caption">
                                <h3>GlycoWorkbench</h3>
                                <p>UniCarb-DB supports annotated
                                    <a href="http://glycomics.ccrc.uga.edu/GlycomicsPortal/showEntry.action?id=1">Glycoworkbench</a>
                                    files for data submission.</p>
                            </div>
                        </div>

                        <div class="item">
                            <img src="../assets/images/slides/NIST.png"
                                 alt="NIST">
                            <div class="carousel-caption">
                                <h3>NIST</h3>
                                <p>In order to promote the use and development of software tools for data analysis,
                                    UniCarb-DB has been made available as an <a href="/library">external glycomics library</a>.
                                    (in the <a href="http://chemdata.nist.gov/dokuwiki/doku.php?id=chemdata:glycan-library">
                                        NIST format</a>).</p>
                            </div>
                        </div>

                    </div>

                    <!-- Left and right controls -->
                    <a class="left carousel-control" href="#slideCarousel" role="button" data-slide="prev">
                        &lsaquo;
                    </a>
                    <a class="right carousel-control" href="#slideCarousel" role="button" data-slide="next">
                        &rsaquo;
                    </a>
                </div>
            </div>

            <!-- <div class="span4 sidebar">
                <div class="info">
                    <h3>Tweets by &#64;unicarb-db
                        <a href="https://twitter.com/unicarb_db"
                           class="twitter-follow-button"
                           data-show-screen-name="false"
                           data-size="large"
                           data-show-count="false">Follow &#64;unicarb-db</a>
                        <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
                    </h3>
                    <a class="twitter-timeline"
                       data-chrome="transparent nofooter noheader"
                       data-width="100%"
                       data-height="600"
                       href="https://twitter.com/NG_Karlsson">Tweets by UniCarb-DB</a>
                    <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
                </div>
            </div> -->

        </div>
    </div>

    <div id="welcome-footer" class="container-gray">
        <div class="container">
            <!-- <div class="row-fluid">
                <h3>Please cite:</h3>
                <p>
                    Campbell MP, Nguyen-Khuong T, Hayes CA, Flowers SA, Alagesan K, Kolarich D, Packer NH, Karlsson NG :
                    <b>Validation of the curation pipeline of UniCarb-DB: building a global glycan reference MS/MS repository.</b>
                    Biochim Biophys Acta. 2014 Jan. 1844(1 Pt A):108-16.
                </p>
            </div> -->

            <div class="row-fluid" id="sponsours">
                <h3>In partnership with</h3>
                <div class="center">
                    <ul>
                        <li>
                          <a href="https://www.gu.se/english"><img src="../assets/images/logos/gu_logo.gif"
                               class="img-responsive inline-block"
                               title="University of Gothenburg"
                               alt="University of Gothenburg"/></a>

                        </li>
                        <li>
                          <a href="https://www.soka.ac.jp/en/"><img src="../assets/images/logos/soka_logo.png"
                               class="img-responsive inline-block"
                               title="Soka University"
                               alt="Soka University"/></a>

                        </li>
                        <li><a href="https://www.sib.swiss"><img src="../assets/images/logos/sib_logo.png"
                             class="img-responsive inline-block"
                             title="Swiss Institute of Bioinformatics"
                             alt="Swiss Institute of Bioinformatics"/></a>

                        </li>
                        <li><a href="https://www.mq.edu.au"><img src="../assets/images/logos/macquarie_logo.png"
                             class="img-responsive inline-block"
                             title="Macquarie University"
                             alt="Macquarie University"/></a>

                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="row-fluid">
            <div class="span12 centered">
                <span class='st_sharethis_large' displayText='ShareThis'></span>
                <span class='st_twitter_large' displayText='Tweet'></span>
                <span class='st_linkedin_large' displayText='LinkedIn'></span>
                <span class='st__large' displayText=''></span>
                <span class='st_googleplus_large' displayText='Google+'></span>
            </div>
        </div>
    </div>

</div>
"""))
      }
    }
  }

  def render(referenceCount:Int,spectraCount:Int,structureCount:Int,tissueCount:Int,taxonomyCount:Int): play.twirl.api.HtmlFormat.Appendable = apply(referenceCount,spectraCount,structureCount,tissueCount,taxonomyCount)

  def f:((Int,Int,Int,Int,Int) => play.twirl.api.HtmlFormat.Appendable) = (referenceCount,spectraCount,structureCount,tissueCount,taxonomyCount) => apply(referenceCount,spectraCount,structureCount,tissueCount,taxonomyCount)

  def ref: this.type = this

}


}

/**/
object welcome extends welcome_Scope0.welcome
              /*
                  -- GENERATED --
                  DATE: Wed Jun 19 09:50:47 UTC 2019
                  SOURCE: /app/app/views/welcome.scala.html
                  HASH: ba6f45556cbfd7ac9e2288cf2f2850bd7ea1dda3
                  MATRIX: 762->1|957->100|985->102|1457->547|1472->553|1519->579|3425->2458|3459->2471|3821->2806|3853->2817|4216->3153|4251->3167|4617->3506|4652->3520|5011->3852|5044->3864
                  LINES: 27->1|32->1|34->3|48->17|48->17|48->17|81->50|81->50|87->56|87->56|93->62|93->62|99->68|99->68|105->74|105->74
                  -- GENERATED --
              */
          