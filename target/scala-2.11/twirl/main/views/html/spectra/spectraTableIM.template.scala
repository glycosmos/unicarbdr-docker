
package views.html.spectra

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object spectraTableIM_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class spectraTableIM extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template2[List[MS.PeakLabeled],Map[src.PairWrapper, Double],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(pLabeled: List[MS.PeakLabeled], simSpectra: Map[src.PairWrapper, Double]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.76*/("""


"""),_display_(/*4.2*/if(pLabeled.size()>2)/*4.23*/{_display_(Seq[Any](format.raw/*4.24*/("""

"""),format.raw/*6.1*/("""<ul class="nav nav-tabs" data-tabs="tabs">
    <li class="active"><a data-toggle="tab" href="#rplot">Spectra</a></li>
    <li><a data-toggle="tab" href="#complete">Complete Peak List</a></li>
    <li><a data-toggle="tab" href="#more">List of spectra for this structure</a></li>
</ul>


<div class="tab-content">
    <div class="tab-pane active" id="rplot">
        <div id="stgraph" style="width: 600px; height: 450px;"></div>
    </div>

    <div class="tab-pane" id="complete">
        <table class="computers table table-striped">
            <thead><tr>
                <th class="title header">Peak</th>
                <th class="title header">Intensity</th>
                <th class="title header">CCS</th>
            <tbody>
            for(p <- pLabeled)"""),format.raw/*25.31*/("""{"""),format.raw/*25.32*/("""
            """),format.raw/*26.13*/("""<tr>
                <td>p.mz_value</td>
                <td>p.intensity_value</td>
                <td>p.cSectionLabeled.ccs_value</td>
            </tr>
            if(p.pAnnotatedLabeled.size()>0)"""),format.raw/*31.45*/("""{"""),format.raw/*31.46*/("""
            """),format.raw/*32.13*/("""<tr>
                <td colspan="2">
                    <p>Annotations:</p>
                    for(q <- p.pAnnotatedLabeled)"""),format.raw/*35.50*/("""{"""),format.raw/*35.51*/("""
                    """),format.raw/*36.21*/("""q.fragmentation_type
                    <img class="sugar_image" src= """),format.raw/*37.51*/("""{"""),format.raw/*37.52*/("""routes.GWPController.showImage(q.sequence_gws,session.get("notation"),"small")"""),format.raw/*37.130*/("""}"""),format.raw/*37.131*/(""" """),format.raw/*37.132*/("""height="18%", width="18%" alt=""/>
                    """),format.raw/*38.21*/("""}"""),format.raw/*38.22*/("""
                """),format.raw/*39.17*/("""</td>
            </tr>
            """),format.raw/*41.13*/("""}"""),format.raw/*41.14*/("""
            """),format.raw/*42.13*/("""}"""),format.raw/*42.14*/("""
            """),format.raw/*43.13*/("""</tbody>
            </tr></thead>
        </table>
    </div>

    views.html.spectra.spectraSimilar(simSpectra)


</div>

""")))}/*53.2*/else/*53.6*/{_display_(Seq[Any](format.raw/*53.7*/("""
"""),format.raw/*54.1*/("""<div class="tab-content">
    <h4>No spectra available</h4>
</div>
""")))}),format.raw/*57.2*/("""

"""))
      }
    }
  }

  def render(pLabeled:List[MS.PeakLabeled],simSpectra:Map[src.PairWrapper, Double]): play.twirl.api.HtmlFormat.Appendable = apply(pLabeled,simSpectra)

  def f:((List[MS.PeakLabeled],Map[src.PairWrapper, Double]) => play.twirl.api.HtmlFormat.Appendable) = (pLabeled,simSpectra) => apply(pLabeled,simSpectra)

  def ref: this.type = this

}


}

/**/
object spectraTableIM extends spectraTableIM_Scope0.spectraTableIM
              /*
                  -- GENERATED --
                  DATE: Wed Jun 19 09:50:50 UTC 2019
                  SOURCE: /app/app/views/spectra/spectraTableIM.scala.html
                  HASH: c021d2731c87ed3065c9223fe6cd4d55ab7862a4
                  MATRIX: 814->1|983->75|1012->79|1041->100|1079->101|1107->103|1900->868|1929->869|1970->882|2197->1081|2226->1082|2267->1095|2422->1222|2451->1223|2500->1244|2599->1315|2628->1316|2735->1394|2765->1395|2795->1396|2878->1451|2907->1452|2952->1469|3016->1505|3045->1506|3086->1519|3115->1520|3156->1533|3299->1658|3311->1662|3349->1663|3377->1664|3475->1732
                  LINES: 27->1|32->1|35->4|35->4|35->4|37->6|56->25|56->25|57->26|62->31|62->31|63->32|66->35|66->35|67->36|68->37|68->37|68->37|68->37|68->37|69->38|69->38|70->39|72->41|72->41|73->42|73->42|74->43|84->53|84->53|84->53|85->54|88->57
                  -- GENERATED --
              */
          