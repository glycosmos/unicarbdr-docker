
package views.html.spectra

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object spectraTable_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class spectraTable extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template2[List[MS.PeakLabeled],Map[src.PairWrapper, Double],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(pLabeled: List[MS.PeakLabeled], simSpectra: Map[src.PairWrapper, Double]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.76*/("""


"""),_display_(/*4.2*/if(pLabeled.size()>2)/*4.23*/{_display_(Seq[Any](format.raw/*4.24*/("""

"""),format.raw/*6.1*/("""<ul class="nav nav-tabs" data-tabs="tabs">
    <li class="active"><a data-toggle="tab" href="#rplot">Spectra</a></li>
    <li><a data-toggle="tab" href="#complete">Complete Peak List</a></li>
    <li><a data-toggle="tab" href="#more">List of spectra for this structure</a></li>
</ul>


<div class="tab-content">
    <div class="tab-pane active" id="rplot">
        <div id="stgraph" style="width: 600px; height: 450px;"></div>
    </div>

    <div class="tab-pane" id="complete">
        <table class="computers table table-striped">
            <thead><tr>
                <th class="title header">Peak</th>
                <th class="title header">Intensity</th>
            <tbody>
            """),_display_(/*24.14*/for(p <- pLabeled) yield /*24.32*/{_display_(Seq[Any](format.raw/*24.33*/("""
                """),format.raw/*25.17*/("""<tr>
                    <td>"""),_display_(/*26.26*/p/*26.27*/.mz_value),format.raw/*26.36*/("""</td>
                    <td>"""),_display_(/*27.26*/p/*27.27*/.intensity_value),format.raw/*27.43*/("""</td>

                </tr>
                """),_display_(/*30.18*/if(p.pAnnotLab.size()>0)/*30.42*/{_display_(Seq[Any](format.raw/*30.43*/("""
                    """),format.raw/*31.21*/("""<tr>
                        <td colspan="2">
                            <p>Annotations:</p>
                            """),_display_(/*34.30*/for(q <- p.pAnnotLab) yield /*34.51*/{_display_(Seq[Any](format.raw/*34.52*/("""
                                """),_display_(/*35.34*/q/*35.35*/.fragmentation_type),format.raw/*35.54*/("""
                               """),format.raw/*36.32*/("""<img class="sugar_image" src="""),_display_(/*36.62*/{routes.GWPController.showImage(q.sequence_gws,session.get("notation"),"small")}),format.raw/*36.142*/(""" """),format.raw/*36.143*/("""height="18%", width="18%" alt=""/>
                            """)))}),format.raw/*37.30*/("""
                        """),format.raw/*38.25*/("""</td>
                    </tr>
                """)))}),format.raw/*40.18*/("""
            """)))}),format.raw/*41.14*/("""
            """),format.raw/*42.13*/("""</tbody>
            </tr></thead>
        </table>
    </div>

    """),_display_(/*47.6*/views/*47.11*/.html.spectra.spectraSimilar(simSpectra)),format.raw/*47.51*/("""


"""),format.raw/*50.1*/("""</div>

""")))}/*52.2*/else/*52.6*/{_display_(Seq[Any](format.raw/*52.7*/("""
"""),format.raw/*53.1*/("""<div class="tab-content">
    <h4>No spectra available</h4>
</div>
""")))}),format.raw/*56.2*/("""

"""))
      }
    }
  }

  def render(pLabeled:List[MS.PeakLabeled],simSpectra:Map[src.PairWrapper, Double]): play.twirl.api.HtmlFormat.Appendable = apply(pLabeled,simSpectra)

  def f:((List[MS.PeakLabeled],Map[src.PairWrapper, Double]) => play.twirl.api.HtmlFormat.Appendable) = (pLabeled,simSpectra) => apply(pLabeled,simSpectra)

  def ref: this.type = this

}


}

/**/
object spectraTable extends spectraTable_Scope0.spectraTable
              /*
                  -- GENERATED --
                  DATE: Wed Jun 19 09:50:50 UTC 2019
                  SOURCE: /app/app/views/spectra/spectraTable.scala.html
                  HASH: adfb8830223f2f60a048b2a8c9a37e0cbd5e76cf
                  MATRIX: 810->1|979->75|1008->79|1037->100|1075->101|1103->103|1828->801|1862->819|1901->820|1946->837|2003->867|2013->868|2043->877|2101->908|2111->909|2148->925|2221->971|2254->995|2293->996|2342->1017|2492->1140|2529->1161|2568->1162|2629->1196|2639->1197|2679->1216|2739->1248|2796->1278|2898->1358|2928->1359|3023->1423|3076->1448|3156->1497|3201->1511|3242->1524|3337->1593|3351->1598|3412->1638|3442->1641|3469->1650|3481->1654|3519->1655|3547->1656|3645->1724
                  LINES: 27->1|32->1|35->4|35->4|35->4|37->6|55->24|55->24|55->24|56->25|57->26|57->26|57->26|58->27|58->27|58->27|61->30|61->30|61->30|62->31|65->34|65->34|65->34|66->35|66->35|66->35|67->36|67->36|67->36|67->36|68->37|69->38|71->40|72->41|73->42|78->47|78->47|78->47|81->50|83->52|83->52|83->52|84->53|87->56
                  -- GENERATED --
              */
          