
package views.html.spectra

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object spectraViewer_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class spectraViewer extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[List[MS.PeakLabeled],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(pLabeled: List[MS.PeakLabeled]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.34*/("""

"""),format.raw/*3.1*/("""<script>

    var chart;

    var jsonObject = ﻿"""),format.raw/*7.23*/("""{"""),format.raw/*7.24*/(""""spectrumId": "TestID",
			 "mzStart": 0,
			 "mzStop": 1000,
			 "peaks": [
             """),_display_(/*11.15*/for(pl <- pLabeled) yield /*11.34*/{_display_(Seq[Any](format.raw/*11.35*/("""
                """),format.raw/*12.17*/("""{"""),format.raw/*12.18*/(""""mz": """),_display_(/*12.25*/pl/*12.27*/.mz_value),format.raw/*12.36*/(""", "intensity": """),_display_(/*12.52*/pl/*12.54*/.intensity_value),format.raw/*12.70*/("""}"""),format.raw/*12.71*/(""",
             """)))}),format.raw/*13.15*/("""
		"""),format.raw/*14.3*/("""]"""),format.raw/*14.4*/("""}"""),format.raw/*14.5*/(""";


    var yAxis = [
    """),_display_(/*18.6*/for(pl <- pLabeled) yield /*18.25*/{_display_(Seq[Any](format.raw/*18.26*/("""
        """),_display_(/*19.10*/pl/*19.12*/.intensity_value),format.raw/*19.28*/(""",
    """)))}),format.raw/*20.6*/("""
    """),format.raw/*21.5*/("""];


    var largest = Math.max.apply(Math, yAxis);

    chart = st.chart
            .ms()
            .legend(false)
            .xlabel("m/z")
            .ylabel("Abundance")
            .labels(false)
            .margins([50, 80, 80, 120]);
        chart.render("#stgraph");           // render chart to id 'stgraph'


    var set = st.data.set()        // data type (set)
            .ylimits([0, largest])        // y-axis limits
            .x("peaks.mz")             // x-accessor
            .y("peaks.intensity")      // y-accessor
            .title("spectrumId");      // id-accessor


    chart.load(set);               // bind the data set
    set.add([jsonObject], []);



</script>

"""))
      }
    }
  }

  def render(pLabeled:List[MS.PeakLabeled]): play.twirl.api.HtmlFormat.Appendable = apply(pLabeled)

  def f:((List[MS.PeakLabeled]) => play.twirl.api.HtmlFormat.Appendable) = (pLabeled) => apply(pLabeled)

  def ref: this.type = this

}


}

/**/
object spectraViewer extends spectraViewer_Scope0.spectraViewer
              /*
                  -- GENERATED --
                  DATE: Wed Jun 19 09:50:50 UTC 2019
                  SOURCE: /app/app/views/spectra/spectraViewer.scala.html
                  HASH: 5ebf1b5b4027a8280316a4040058754ffe4a6982
                  MATRIX: 783->1|910->33|938->35|1013->83|1041->84|1159->175|1194->194|1233->195|1278->212|1307->213|1341->220|1352->222|1382->231|1425->247|1436->249|1473->265|1502->266|1549->282|1579->285|1607->286|1635->287|1688->314|1723->333|1762->334|1799->344|1810->346|1847->362|1884->369|1916->374
                  LINES: 27->1|32->1|34->3|38->7|38->7|42->11|42->11|42->11|43->12|43->12|43->12|43->12|43->12|43->12|43->12|43->12|43->12|44->13|45->14|45->14|45->14|49->18|49->18|49->18|50->19|50->19|50->19|51->20|52->21
                  -- GENERATED --
              */
          