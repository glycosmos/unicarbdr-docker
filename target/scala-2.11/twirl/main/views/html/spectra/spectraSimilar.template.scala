
package views.html.spectra

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object spectraSimilar_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class spectraSimilar extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[Map[src.PairWrapper, Double],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(simSpectra: Map[src.PairWrapper, Double]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.44*/("""

"""),format.raw/*3.1*/("""<div class="tab-pane" id="more">

    """),_display_(/*5.6*/if(simSpectra.size != 0)/*5.30*/{_display_(Seq[Any](format.raw/*5.31*/("""
    """),format.raw/*6.5*/("""<p>There are """),_display_(/*6.19*/simSpectra/*6.29*/.size()),format.raw/*6.36*/(""" """),format.raw/*6.37*/("""similar spectra: </p>

    <div class="tab-pane" id="complete">
        <table class="computers table table-striped">
            <thead><tr>
                <th class="title header">Id</th>
                <th class="title header">Structure</th>
                <th class="title header">Score</th>
            </tr></thead>
            <tbody>
            """),_display_(/*16.14*/for((key, value) <- simSpectra) yield /*16.45*/ {_display_(Seq[Any](format.raw/*16.47*/("""
            """),format.raw/*17.13*/("""<tr><td><p>"""),_display_(/*17.25*/key/*17.28*/.getid()),format.raw/*17.36*/("""</p></td><td><a href="/msData/"""),_display_(/*17.67*/key/*17.70*/.getid()),format.raw/*17.78*/(""""><img class="sugar_image" src="""),_display_(/*17.110*/{routes.GWPController.showImageId(key.getGlycanSeqId(), session.get("notation"), "extended" )}),format.raw/*17.204*/(""" """),format.raw/*17.205*/("""height="15%", width="15%" alt=""></a></td>
                <td>"""),_display_(/*18.22*/("%.2f".format(value))),format.raw/*18.44*/("""</td></tr>
            """)))}),format.raw/*19.14*/("""
            """),format.raw/*20.13*/("""</tbody>

        </table>
    </div>

    """)))}/*25.6*/else/*25.10*/{_display_(Seq[Any](format.raw/*25.11*/("""

    """),format.raw/*27.5*/("""<p>There are not related spectra</p>
    """)))}),format.raw/*28.6*/("""
"""),format.raw/*29.1*/("""</div>
"""))
      }
    }
  }

  def render(simSpectra:Map[src.PairWrapper, Double]): play.twirl.api.HtmlFormat.Appendable = apply(simSpectra)

  def f:((Map[src.PairWrapper, Double]) => play.twirl.api.HtmlFormat.Appendable) = (simSpectra) => apply(simSpectra)

  def ref: this.type = this

}


}

/**/
object spectraSimilar extends spectraSimilar_Scope0.spectraSimilar
              /*
                  -- GENERATED --
                  DATE: Wed Jun 19 09:50:50 UTC 2019
                  SOURCE: /app/app/views/spectra/spectraSimilar.scala.html
                  HASH: 9661e48070cd6edd65ac73c1ea2c21fcc325936e
                  MATRIX: 793->1|930->43|958->45|1022->84|1054->108|1092->109|1123->114|1163->128|1181->138|1208->145|1236->146|1621->504|1668->535|1708->537|1749->550|1788->562|1800->565|1829->573|1887->604|1899->607|1928->615|1988->647|2104->741|2134->742|2225->806|2268->828|2323->852|2364->865|2426->909|2439->913|2478->914|2511->920|2583->962|2611->963
                  LINES: 27->1|32->1|34->3|36->5|36->5|36->5|37->6|37->6|37->6|37->6|37->6|47->16|47->16|47->16|48->17|48->17|48->17|48->17|48->17|48->17|48->17|48->17|48->17|48->17|49->18|49->18|50->19|51->20|56->25|56->25|56->25|58->27|59->28|60->29
                  -- GENERATED --
              */
          