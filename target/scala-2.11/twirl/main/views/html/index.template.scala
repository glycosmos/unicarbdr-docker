
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object index_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

     object index_Scope1 {
import service.UserProvider

class index extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template6[Int,Int,Int,Int,Int,UserProvider,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*2.2*/(referenceCount: Int, spectraCount: Int, structureCount: Int, tissueCount: Int, taxonomyCount: Int, userProvider: UserProvider):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.129*/("""
"""),_display_(/*3.2*/main(userProvider, "UniCarb-DR")/*3.34*/{_display_(Seq[Any](format.raw/*3.35*/("""
"""),_display_(/*4.2*/welcome(referenceCount, spectraCount, structureCount, tissueCount, taxonomyCount)),format.raw/*4.83*/("""
""")))}),format.raw/*5.2*/("""
"""))
      }
    }
  }

  def render(referenceCount:Int,spectraCount:Int,structureCount:Int,tissueCount:Int,taxonomyCount:Int,userProvider:UserProvider): play.twirl.api.HtmlFormat.Appendable = apply(referenceCount,spectraCount,structureCount,tissueCount,taxonomyCount,userProvider)

  def f:((Int,Int,Int,Int,Int,UserProvider) => play.twirl.api.HtmlFormat.Appendable) = (referenceCount,spectraCount,structureCount,tissueCount,taxonomyCount,userProvider) => apply(referenceCount,spectraCount,structureCount,tissueCount,taxonomyCount,userProvider)

  def ref: this.type = this

}


}
}

/**/
object index extends index_Scope0.index_Scope1.index
              /*
                  -- GENERATED --
                  DATE: Wed Jun 19 09:50:47 UTC 2019
                  SOURCE: /app/app/views/index.scala.html
                  HASH: af31874c67a6b8388c247109849cee1983d6f0e3
                  MATRIX: 827->30|1050->157|1077->159|1117->191|1155->192|1182->194|1283->275|1314->277
                  LINES: 30->2|35->2|36->3|36->3|36->3|37->4|37->4|38->5
                  -- GENERATED --
              */
          