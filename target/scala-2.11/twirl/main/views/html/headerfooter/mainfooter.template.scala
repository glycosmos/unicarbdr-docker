
package views.html.headerfooter

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object mainfooter_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class mainfooter extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template0[play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply():play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.1*/("""<div class="footer row-fluid">
    <div class="span12">
        <p class="pull-left">UniCarb-DR <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/3.0/deed.en_US"><img alt="Creative Commons License" style="border-width:0" src=""""),_display_(/*3.190*/routes/*3.196*/.Assets.versioned("images/cc_80x15.png")),format.raw/*3.236*/("""" /></a></p>
        <p class="pull-right">Supported by
            <a href="http://www.nectar.org.au">Nectar</a> &nbsp;|&nbsp;
            <a href="http://www.ands.org.au">ANDS</a> &nbsp;|&nbsp;
            <a href="http://www.stint.se">STINT</a> &nbsp;|&nbsp;
            <a href="http://expasy.org/">SIB ExPASy</a> &nbsp;|&nbsp;
            <a href="http://glycogastromics.biomedtrain.eu">GGE</a>
        </p>
    </div>
</div>
"""))
      }
    }
  }

  def render(): play.twirl.api.HtmlFormat.Appendable = apply()

  def f:(() => play.twirl.api.HtmlFormat.Appendable) = () => apply()

  def ref: this.type = this

}


}

/**/
object mainfooter extends mainfooter_Scope0.mainfooter
              /*
                  -- GENERATED --
                  DATE: Wed Jun 19 09:50:52 UTC 2019
                  SOURCE: /app/app/views/headerfooter/mainfooter.scala.html
                  HASH: 4dd8fc3680ee0d588db99620a71539ea7d585562
                  MATRIX: 850->0|1122->245|1137->251|1198->291
                  LINES: 32->1|34->3|34->3|34->3
                  -- GENERATED --
              */
          