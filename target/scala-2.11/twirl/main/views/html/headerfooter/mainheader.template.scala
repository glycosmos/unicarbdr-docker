
package views.html.headerfooter

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object mainheader_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

     object mainheader_Scope1 {
import service.UserProvider

class mainheader extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[UserProvider,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*2.2*/(userProvider: UserProvider):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.30*/("""

"""),format.raw/*4.1*/("""<div id = "topNavBar" class="navbar" >
    <div class="navbar-inner">
        <div class="container-fluid">
            <ul class="nav">
                <li><img src='"""),_display_(/*8.32*/routes/*8.38*/.Assets.versioned("images/UniCarbDB_Logo_white.svg")),format.raw/*8.90*/("""'/></li>
                <li id="homeLink"><a href=""""),_display_(/*9.45*/routes/*9.51*/.HomeController.index()),format.raw/*9.74*/("""" style="font-size: 18px;">UniCarb-DR</a></li>

                <li class="navbar-text">&nbsp | &nbsp </li>
                <li id="MIRAGELink"><a href=""""),_display_(/*12.47*/routes/*12.53*/.GenerateController.generate()),format.raw/*12.83*/("""">MIRAGE</a></li>
                <!-- above code should be in the code but it does not work due to argument error -->
                <li class="navbar-text">&nbsp | &nbsp </li>
                <li id="aboutLink"><a href=""""),_display_(/*15.46*/routes/*15.52*/.HomeController.about()),format.raw/*15.75*/("""">About</a></li>
            </ul>


<!--                <li class="navbar-text">&nbsp | &nbsp </li>
                <li id="searchLink"><a href=""""),_display_(/*20.47*/routes/*20.53*/.HomeController.signup()),format.raw/*20.77*/("""">Register</a></li>
                <li class="navbar-text">&nbsp | &nbsp </li>
                <li id="searchLink"><a href=""""),_display_(/*22.47*/routes/*22.53*/.GenerateController.generate()),format.raw/*22.83*/("""">Mirage</a></li>-->



            <div id="headersearch" class="pull-right">
                <ul class='nav'>
                    <li id="loginlink"><a href=""""),_display_(/*28.50*/routes/*28.56*/.HomeController.login()),format.raw/*28.79*/("""">Login</a></li>
                    <li id="libLink"><a href=""""),_display_(/*29.48*/routes/*29.54*/.HomeController.signup()),format.raw/*29.78*/("""">Register</a></li>
                    """),format.raw/*30.72*/("""
                """),format.raw/*31.17*/("""</ul>
            </div>

            <script>
                $("#topNavBar .nav a").on("click", function()"""),format.raw/*35.62*/("""{"""),format.raw/*35.63*/("""
                    """),format.raw/*36.21*/("""$(".nav").find(".active").removeClass("active");
                    $(this).parent().addClass("active");
                """),format.raw/*38.17*/("""}"""),format.raw/*38.18*/(""");
            </script>

        </div>
    </div>
</div>
"""))
      }
    }
  }

  def render(userProvider:UserProvider): play.twirl.api.HtmlFormat.Appendable = apply(userProvider)

  def f:((UserProvider) => play.twirl.api.HtmlFormat.Appendable) = (userProvider) => apply(userProvider)

  def ref: this.type = this

}


}
}

/**/
object mainheader extends mainheader_Scope0.mainheader_Scope1.mainheader
              /*
                  -- GENERATED --
                  DATE: Wed Jun 19 09:50:52 UTC 2019
                  SOURCE: /app/app/views/headerfooter/mainheader.scala.html
                  HASH: dc69b31c39d20e80211379e7a68c8d33b1d64fcb
                  MATRIX: 835->30|958->58|986->60|1180->228|1194->234|1266->286|1345->339|1359->345|1402->368|1583->522|1598->528|1649->558|1900->782|1915->788|1959->811|2133->958|2148->964|2193->988|2346->1114|2361->1120|2412->1150|2600->1311|2615->1317|2659->1340|2750->1404|2765->1410|2810->1434|2878->1525|2923->1542|3059->1650|3088->1651|3137->1672|3287->1794|3316->1795
                  LINES: 30->2|35->2|37->4|41->8|41->8|41->8|42->9|42->9|42->9|45->12|45->12|45->12|48->15|48->15|48->15|53->20|53->20|53->20|55->22|55->22|55->22|61->28|61->28|61->28|62->29|62->29|62->29|63->30|64->31|68->35|68->35|69->36|71->38|71->38
                  -- GENERATED --
              */
          