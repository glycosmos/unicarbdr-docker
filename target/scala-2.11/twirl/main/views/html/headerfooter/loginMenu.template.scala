
package views.html.headerfooter

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object loginMenu_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

     object loginMenu_Scope1 {
import service.UserProvider
import be.objectify.deadbolt.java.views.html._

class loginMenu extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[UserProvider,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*4.2*/(userProvider: UserProvider):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*4.30*/("""




"""),_display_(/*9.2*/subjectPresentOr()/*9.20*/ {_display_(Seq[Any](format.raw/*9.22*/("""
    """),_display_(/*10.6*/defining(userProvider.getUser(session()))/*10.47*/ { user =>_display_(Seq[Any](format.raw/*10.57*/("""
    """),format.raw/*11.5*/("""<li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-user"></i> """),_display_(/*12.110*/user/*12.114*/.name),format.raw/*12.119*/(""" """),format.raw/*12.120*/("""<span class="caret"></span></a>

        <ul class="dropdown-menu" role="menu">
<!--          <li><a href=""""),_display_(/*15.29*/routes/*15.35*/.HomeController.signup()),format.raw/*15.59*/("""">Profile</a></li>   -->
            <li><a href=""""),_display_(/*16.27*/routes/*16.33*/.HomeController.profile()),format.raw/*16.58*/("""">Profile</a></li>
            <li class="divider"></li>
            <li><a href=""""),_display_(/*18.27*/routes/*18.33*/.HomeController.uploadIndex()),format.raw/*18.62*/("""" >Submit data</a></li>
            <li class="divider"></li>
            <li><a href=""""),_display_(/*20.27*/com/*20.30*/.feth.play.module.pa.controllers.routes.Authenticate.logout()),format.raw/*20.91*/(""""><i class="icon-off"></i>Logout</a></li>
        </ul>
    </li>
    """)))}),format.raw/*23.6*/("""
""")))}/*24.2*/ {_display_(Seq[Any](format.raw/*24.4*/("""
    """),format.raw/*25.5*/("""<li id="loginLink"><a href=""""),_display_(/*25.34*/routes/*25.40*/.HomeController.login()),format.raw/*25.63*/("""">Login</a></li>
""")))}))
      }
    }
  }

  def render(userProvider:UserProvider): play.twirl.api.HtmlFormat.Appendable = apply(userProvider)

  def f:((UserProvider) => play.twirl.api.HtmlFormat.Appendable) = (userProvider) => apply(userProvider)

  def ref: this.type = this

}


}
}

/**/
object loginMenu extends loginMenu_Scope0.loginMenu_Scope1.loginMenu
              /*
                  -- GENERATED --
                  DATE: Wed Jun 19 09:50:52 UTC 2019
                  SOURCE: /app/app/views/headerfooter/loginMenu.scala.html
                  HASH: 02ea61f84b5f721292515d46c1f174ff3035c280
                  MATRIX: 879->79|1002->107|1033->113|1059->131|1098->133|1130->139|1180->180|1228->190|1260->195|1419->326|1433->330|1460->335|1490->336|1625->444|1640->450|1685->474|1763->525|1778->531|1824->556|1934->639|1949->645|1999->674|2114->762|2126->765|2208->826|2309->897|2329->899|2368->901|2400->906|2456->935|2471->941|2515->964
                  LINES: 31->4|36->4|41->9|41->9|41->9|42->10|42->10|42->10|43->11|44->12|44->12|44->12|44->12|47->15|47->15|47->15|48->16|48->16|48->16|50->18|50->18|50->18|52->20|52->20|52->20|55->23|56->24|56->24|57->25|57->25|57->25|57->25
                  -- GENERATED --
              */
          