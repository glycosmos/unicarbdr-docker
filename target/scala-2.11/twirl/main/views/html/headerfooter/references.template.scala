
package views.html.headerfooter

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object references_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

     object references_Scope1 {
import service.UserProvider
import be.objectify.deadbolt.java.views.html._

class references extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[UserProvider,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*4.2*/(userProvider: UserProvider):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*4.30*/("""




"""),_display_(/*9.2*/subjectPresentOr()/*9.20*/ {_display_(Seq[Any](format.raw/*9.22*/("""
"""),_display_(/*10.2*/defining(userProvider.getUser(session()))/*10.43*/ { user =>_display_(Seq[Any](format.raw/*10.53*/("""
    """),format.raw/*11.5*/("""<li id="refsLink">
        <a href=""""),_display_(/*12.19*/routes/*12.25*/.HomeController.listReferences()),format.raw/*12.57*/("""" title=""""),_display_(/*12.67*/DBviews/*12.74*/.vReferences.getToolTipText()),format.raw/*12.103*/("""">References</a>
    </li>
""")))}),format.raw/*14.2*/("""
""")))}/*15.2*/ {_display_(Seq[Any](format.raw/*15.4*/("""

""")))}))
      }
    }
  }

  def render(userProvider:UserProvider): play.twirl.api.HtmlFormat.Appendable = apply(userProvider)

  def f:((UserProvider) => play.twirl.api.HtmlFormat.Appendable) = (userProvider) => apply(userProvider)

  def ref: this.type = this

}


}
}

/**/
object references extends references_Scope0.references_Scope1.references
              /*
                  -- GENERATED --
                  DATE: Wed Jun 19 09:50:52 UTC 2019
                  SOURCE: /app/app/views/headerfooter/references.scala.html
                  HASH: 4b13e1412fd56a17d98c463d13987b2b4e02611b
                  MATRIX: 882->79|1005->107|1036->113|1062->131|1101->133|1129->135|1179->176|1227->186|1259->191|1323->228|1338->234|1391->266|1428->276|1444->283|1495->312|1553->340|1573->342|1612->344
                  LINES: 31->4|36->4|41->9|41->9|41->9|42->10|42->10|42->10|43->11|44->12|44->12|44->12|44->12|44->12|44->12|46->14|47->15|47->15
                  -- GENERATED --
              */
          