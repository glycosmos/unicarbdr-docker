
package views.html.references

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object listReferences_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

     object listReferences_Scope1 {
import service.UserProvider

class listReferences extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template5[com.avaje.ebean.PagedList[models.DBviews.vReferences],String,String,String,UserProvider,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*2.2*/(currentPage: com.avaje.ebean.PagedList[models.DBviews.vReferences], currentSortBy: String, currentOrder: String, currentFilter: String, userProvider: UserProvider):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {

def /*30.2*/header/*30.8*/(key:String, title:String):play.twirl.api.HtmlFormat.Appendable = {_display_(

Seq[Any](format.raw/*30.38*/("""
"""),format.raw/*31.1*/("""<!--
~ Copyright (c) 2014. Copyright (c) 2014. Matthew Campbell
~
~ The program can not be distributed to others with out the consent of the copyright holder, until such a time that the copyright holder has released the code for public use.
-->

<th class=""""),_display_(/*37.13*/key/*37.16*/.replace(".","_")),format.raw/*37.33*/(""" """),format.raw/*37.34*/("""header """),_display_(/*37.42*/if(currentSortBy == key){/*37.68*/{if(currentOrder == "asc") "headerSortDown" else "headerSortUp"}}),format.raw/*37.132*/("""">
<a href=""""),_display_(/*38.11*/link(0, key)),format.raw/*38.23*/("""">"""),_display_(/*38.26*/title),format.raw/*38.31*/("""</a>
</th>
""")))};def /*4.2*/link/*4.6*/(newPage:Int, newSortBy:String) = {{

    var sortBy = currentSortBy
    var order = currentOrder

    if(newSortBy != null) {
        sortBy = newSortBy
        if(currentSortBy == newSortBy) {
            if(currentOrder == "asc") {
                order = "desc"
            } else {
            order = "asc"
            }
        } else {
            order = "asc"
        }
    }

    // Generate the link
    routes.HomeController.listReferences(newPage, sortBy, order, currentFilter)

}};
Seq[Any](format.raw/*2.166*/("""

"""),format.raw/*25.2*/("""

"""),format.raw/*29.37*/("""
"""),format.raw/*40.2*/("""


"""),_display_(/*43.2*/main(userProvider, "UniCarb-DR")/*43.34*/{_display_(Seq[Any](format.raw/*43.35*/("""

"""),format.raw/*45.1*/("""<script>
    $("#refsLink").addClass("active");
</script>

<div class="container-fluid content">
    <ul class="breadcrumb">
        <li><i class="icon-home" ></i><a href="/"> UniCarb-DR</a> <span class="divider">></span></li>
        <li class="active"><i class="icon-book" ></i> References<span class="divider"></span></li>
    </ul>

    <h1>UniCarb-DR Reference Collection</h1>
<!--    <h1>UniCarb-DR Reference Collection</h1>-->


    """),_display_(/*59.6*/if(currentPage.getTotalRowCount > 0)/*59.42*/ {_display_(Seq[Any](format.raw/*59.44*/("""

            """),format.raw/*61.13*/("""<table class="computers table table-striped">
                <thead>
                <tr>
                    """),_display_(/*64.22*/header("title", "Title")),format.raw/*64.46*/("""
                    """),_display_(/*65.22*/header("publication_year", "Year")),format.raw/*65.56*/("""
                    """),_display_(/*66.22*/header("authors", "Authors")),format.raw/*66.50*/("""
                    """),_display_(/*67.22*/header("journal_title", "Journal")),format.raw/*67.56*/("""
                    """),_display_(/*68.22*/header("pubmed_id", "Pubmed")),format.raw/*68.51*/("""

                """),format.raw/*70.17*/("""</tr>
                </thead>
                <tbody>

                """),_display_(/*74.18*/for(reference <- currentPage.getList) yield /*74.55*/ {_display_(Seq[Any](format.raw/*74.57*/("""
                """),format.raw/*75.17*/("""<tr>
                    <td>
                        """),_display_(/*77.26*/if(reference.title == null)/*77.53*/ {_display_(Seq[Any](format.raw/*77.55*/("""
                        """),format.raw/*78.25*/("""<em>Not available</em>
                        """)))}/*79.27*/else/*79.32*/{_display_(Seq[Any](format.raw/*79.33*/("""
                        """),format.raw/*80.25*/("""<a href="references/"""),_display_(/*80.46*/reference/*80.55*/.journal_reference_id),format.raw/*80.76*/("""">"""),_display_(/*80.79*/reference/*80.88*/.title),format.raw/*80.94*/("""</a>
                        """)))}),format.raw/*81.26*/("""
                    """),format.raw/*82.21*/("""</td>
                    <td>
                        """),_display_(/*84.26*/if(reference.publication_year == null)/*84.64*/ {_display_(Seq[Any](format.raw/*84.66*/("""
                        """),format.raw/*85.25*/("""<em>-</em>
                        """)))}/*86.27*/else/*86.32*/{_display_(Seq[Any](format.raw/*86.33*/("""
                        """),_display_(/*87.26*/reference/*87.35*/.publication_year),format.raw/*87.52*/("""
                        """)))}),format.raw/*88.26*/("""
                    """),format.raw/*89.21*/("""</td>
                    <td>
                        """),_display_(/*91.26*/if(reference.authors == null)/*91.55*/ {_display_(Seq[Any](format.raw/*91.57*/("""
                        """),format.raw/*92.25*/("""<em>-</em>
                        """)))}/*93.27*/else/*93.32*/{_display_(Seq[Any](format.raw/*93.33*/("""
                        """),_display_(/*94.26*/reference/*94.35*/.authors),format.raw/*94.43*/("""
                        """)))}),format.raw/*95.26*/("""
                    """),format.raw/*96.21*/("""</td>
                    <td>
                        """),_display_(/*98.26*/if(reference.journal_title == null)/*98.61*/ {_display_(Seq[Any](format.raw/*98.63*/("""
                        """),format.raw/*99.25*/("""<em>-</em>
                        """)))}/*100.27*/else/*100.32*/{_display_(Seq[Any](format.raw/*100.33*/("""
                        """),_display_(/*101.26*/reference/*101.35*/.journal_title),format.raw/*101.49*/("""
                        """)))}),format.raw/*102.26*/("""
                    """),format.raw/*103.21*/("""</td>
                    <td>
                        """),_display_(/*105.26*/if(reference.pubmed_id == null)/*105.57*/ {_display_(Seq[Any](format.raw/*105.59*/("""
                        """),format.raw/*106.25*/("""<em>-</em>
                        """)))}/*107.27*/else/*107.32*/{_display_(Seq[Any](format.raw/*107.33*/("""
                        """),_display_(/*108.26*/reference/*108.35*/.pubmed_id),format.raw/*108.45*/("""
                        """)))}),format.raw/*109.26*/("""
                    """),format.raw/*110.21*/("""</td>
                </tr>
                """)))}),format.raw/*112.18*/("""
                """),format.raw/*113.17*/("""</tbody>
            </table>


            <div id="pagination" class="pagination">
                <ul>
                    """),_display_(/*119.22*/if(currentPage.hasPrev)/*119.45*/ {_display_(Seq[Any](format.raw/*119.47*/("""
                    """),format.raw/*120.21*/("""<li class="prev">
                        <a href=""""),_display_(/*121.35*/link(currentPage.getPageIndex - 1, null)),format.raw/*121.75*/("""">&larr; Previous</a>
                    </li>
                    """)))}/*123.23*/else/*123.28*/{_display_(Seq[Any](format.raw/*123.29*/("""
                    """),format.raw/*124.21*/("""<li class="previous disabled">
                        <a>&larr; Previous</a>
                    </li>
                    """)))}),format.raw/*127.22*/("""
                    """),format.raw/*128.21*/("""<li class="current">
                        <a>Displaying """),_display_(/*129.40*/currentPage/*129.51*/.getDisplayXtoYofZ(" to "," of ")),format.raw/*129.84*/("""</a>
                    </li>
                    """),_display_(/*131.22*/if(currentPage.hasNext)/*131.45*/ {_display_(Seq[Any](format.raw/*131.47*/("""
                    """),format.raw/*132.21*/("""<li class="next">
                        <a href=""""),_display_(/*133.35*/link(currentPage.getPageIndex + 1, null)),format.raw/*133.75*/("""">Next &rarr;</a>
                    </li>
                    """)))}/*135.23*/else/*135.28*/{_display_(Seq[Any](format.raw/*135.29*/("""
                    """),format.raw/*136.21*/("""<li class="next disabled">
                        <a>Next &rarr;</a>
                    </li>
                    """)))}),format.raw/*139.22*/("""
                """),format.raw/*140.17*/("""</ul>
            </div>

        </div>

    """)))}/*145.7*/else/*145.12*/{_display_(Seq[Any](format.raw/*145.13*/("""

        """),format.raw/*147.9*/("""<div class="well">
            <em>There are no references to display</em>
        </div>
    """)))}),format.raw/*150.6*/("""

""")))}),format.raw/*152.2*/("""
"""))
      }
    }
  }

  def render(currentPage:com.avaje.ebean.PagedList[models.DBviews.vReferences],currentSortBy:String,currentOrder:String,currentFilter:String,userProvider:UserProvider): play.twirl.api.HtmlFormat.Appendable = apply(currentPage,currentSortBy,currentOrder,currentFilter,userProvider)

  def f:((com.avaje.ebean.PagedList[models.DBviews.vReferences],String,String,String,UserProvider) => play.twirl.api.HtmlFormat.Appendable) = (currentPage,currentSortBy,currentOrder,currentFilter,userProvider) => apply(currentPage,currentSortBy,currentOrder,currentFilter,userProvider)

  def ref: this.type = this

}


}
}

/**/
object listReferences extends listReferences_Scope0.listReferences_Scope1.listReferences
              /*
                  -- GENERATED --
                  DATE: Wed Jun 19 09:50:50 UTC 2019
                  SOURCE: /app/app/views/references/listReferences.scala.html
                  HASH: 4665586d821941fe8ade2eeb20f96a2e43c958c4
                  MATRIX: 920->30|1163->807|1177->813|1284->843|1312->844|1597->1102|1609->1105|1647->1122|1676->1123|1711->1131|1745->1157|1832->1221|1872->1234|1905->1246|1935->1249|1961->1254|1995->197|2006->201|2531->194|2560->695|2590->805|2618->1266|2648->1270|2689->1302|2728->1303|2757->1305|3224->1746|3269->1782|3309->1784|3351->1798|3490->1910|3535->1934|3584->1956|3639->1990|3688->2012|3737->2040|3786->2062|3841->2096|3890->2118|3940->2147|3986->2165|4086->2238|4139->2275|4179->2277|4224->2294|4306->2349|4342->2376|4382->2378|4435->2403|4502->2452|4515->2457|4554->2458|4607->2483|4655->2504|4673->2513|4715->2534|4745->2537|4763->2546|4790->2552|4851->2582|4900->2603|4983->2659|5030->2697|5070->2699|5123->2724|5178->2761|5191->2766|5230->2767|5283->2793|5301->2802|5339->2819|5396->2845|5445->2866|5528->2922|5566->2951|5606->2953|5659->2978|5714->3015|5727->3020|5766->3021|5819->3047|5837->3056|5866->3064|5923->3090|5972->3111|6055->3167|6099->3202|6139->3204|6192->3229|6248->3266|6262->3271|6302->3272|6356->3298|6375->3307|6411->3321|6469->3347|6519->3368|6603->3424|6644->3455|6685->3457|6739->3482|6795->3519|6809->3524|6849->3525|6903->3551|6922->3560|6954->3570|7012->3596|7062->3617|7139->3662|7185->3679|7340->3806|7373->3829|7414->3831|7464->3852|7544->3904|7606->3944|7695->4014|7709->4019|7749->4020|7799->4041|7956->4166|8006->4187|8094->4247|8115->4258|8170->4291|8250->4343|8283->4366|8324->4368|8374->4389|8454->4441|8516->4481|8601->4547|8615->4552|8655->4553|8705->4574|8854->4691|8900->4708|8966->4756|8980->4761|9020->4762|9058->4772|9184->4867|9218->4870
                  LINES: 30->2|34->30|34->30|36->30|37->31|43->37|43->37|43->37|43->37|43->37|43->37|43->37|44->38|44->38|44->38|44->38|46->4|46->4|68->2|70->25|72->29|73->40|76->43|76->43|76->43|78->45|92->59|92->59|92->59|94->61|97->64|97->64|98->65|98->65|99->66|99->66|100->67|100->67|101->68|101->68|103->70|107->74|107->74|107->74|108->75|110->77|110->77|110->77|111->78|112->79|112->79|112->79|113->80|113->80|113->80|113->80|113->80|113->80|113->80|114->81|115->82|117->84|117->84|117->84|118->85|119->86|119->86|119->86|120->87|120->87|120->87|121->88|122->89|124->91|124->91|124->91|125->92|126->93|126->93|126->93|127->94|127->94|127->94|128->95|129->96|131->98|131->98|131->98|132->99|133->100|133->100|133->100|134->101|134->101|134->101|135->102|136->103|138->105|138->105|138->105|139->106|140->107|140->107|140->107|141->108|141->108|141->108|142->109|143->110|145->112|146->113|152->119|152->119|152->119|153->120|154->121|154->121|156->123|156->123|156->123|157->124|160->127|161->128|162->129|162->129|162->129|164->131|164->131|164->131|165->132|166->133|166->133|168->135|168->135|168->135|169->136|172->139|173->140|178->145|178->145|178->145|180->147|183->150|185->152
                  -- GENERATED --
              */
          