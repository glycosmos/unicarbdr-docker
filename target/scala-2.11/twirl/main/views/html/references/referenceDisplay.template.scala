
package views.html.references

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object referenceDisplay_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

     object referenceDisplay_Scope1 {
import service.UserProvider

class referenceDisplay extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template6[DBviews.vReferences,List[DBviews.vReferenceDisplay],DBviews.vLC,List[DBviews.vBCToJournal],DBviews.vMassSpec,UserProvider,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*2.2*/(journal: DBviews.vReferences, struc: List[DBviews.vReferenceDisplay], lc: DBviews.vLC, bio: List[DBviews.vBCToJournal], massSpec: DBviews.vMassSpec, userProvider: UserProvider):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.179*/("""

"""),_display_(/*4.2*/main(userProvider, "UniCarb-DB")/*4.34*/{_display_(Seq[Any](format.raw/*4.35*/("""

"""),format.raw/*6.1*/("""<div class="container-fluid content">

    <ul class="breadcrumb">
        <li><i class="icon-home"></i><a href="/"> UniCarb-DR</a> <span class="divider">></span></li>
        <li><i class="icon-book"></i><a href="/references"> References</a> <span class="divider">></span></li>
        <li class="active"><i class="icon-file"></i> """),_display_(/*11.55*/journal/*11.62*/.pubmed_id),format.raw/*11.72*/("""<span class="divider"></span></li>
    </ul>


    <section id="structureLayout">
        <section id="layouts">

            <div class="page-header row-fluid">
                <h1 class="">"""),_display_(/*19.31*/journal/*19.38*/.title),format.raw/*19.44*/("""</h1>
            </div>


            <div class="row-fluid">
                <div class="span8">

                    <p><b>"""),_display_(/*26.28*/journal/*26.35*/.authors),format.raw/*26.43*/(""", """),_display_(/*26.46*/journal/*26.53*/.journal_title),format.raw/*26.67*/(""", """),_display_(/*26.70*/journal/*26.77*/.pages),format.raw/*26.83*/("""</b></p>


                    <p><a href="http://www.ncbi.nlm.nih.gov/pubmed/"""),_display_(/*29.69*/journal/*29.76*/.pubmed_id),format.raw/*29.86*/("""" target="_blank">PubMed Entry:</a></p>

                    <table class="computers table table-striped">
                        <col width="15%"/>
                        <col width="25%"/>
                        <col width="60%"/>
                        <thead>
                        <tr>
                            <td><a><b>Retention Time</b></a></td>
                            <td><a><b>Recorded Precursor Mass</b></a></td>
                            <td><a><b>Structure</b></a></td>
                        </tr>
                        </thead>
                        <tbody>

                        """),_display_(/*44.26*/for(st <- struc) yield /*44.42*/{_display_(Seq[Any](format.raw/*44.43*/("""
                        """),format.raw/*45.25*/("""<tr>
                            <td>"""),_display_(/*46.34*/if(st.retention_time > 1)/*46.59*/ {_display_(_display_(/*46.62*/st/*46.64*/.retention_time))}/*46.81*/else/*46.86*/{_display_(Seq[Any](format.raw/*46.87*/(""" """),format.raw/*46.88*/("""Not recorded """)))}),format.raw/*46.102*/("""</td>
                            <td>"""),_display_(/*47.34*/st/*47.36*/.base_peak_mz),format.raw/*47.49*/(""" """),_display_(/*47.51*/Html(st.charges)),format.raw/*47.67*/("""
                            """),format.raw/*48.29*/("""</td>

                            <td><a href="/msData/"""),_display_(/*50.51*/st/*50.53*/.scan_id),format.raw/*50.61*/("""">
                                <img class="sugar_image" src="""),_display_(/*51.63*/{routes.GWPController.showImage(st.sequence_gws,session.get("notation"),"normal")}),format.raw/*51.145*/(""" """),format.raw/*51.146*/("""height="25%", width="35%" alt=""/>
                            </a></td>
                        </tr>
                        """)))}),format.raw/*54.26*/("""


                        """),format.raw/*57.25*/("""</tbody>
                    </table>
                </div>



                <div class="span4 sidebar">
                    """),_display_(/*64.22*/views/*64.27*/.html.format.format()),format.raw/*64.48*/("""
                    """),_display_(/*65.22*/views/*65.27*/.html.additionalInfo.bioContextList(bio)),format.raw/*65.67*/("""
                    """),_display_(/*66.22*/if(lc != null)/*66.36*/{_display_(Seq[Any](format.raw/*66.37*/("""
                        """),_display_(/*67.26*/views/*67.31*/.html.additionalInfo.hplc(lc)),format.raw/*67.60*/("""
                    """)))}),format.raw/*68.22*/("""
                    """),_display_(/*69.22*/views/*69.27*/.html.additionalInfo.massSpec(massSpec)),format.raw/*69.66*/("""
                    """),format.raw/*70.87*/("""
                """),format.raw/*71.17*/("""</div>




            </div>

        </section>
    </section>



</div>

""")))}),format.raw/*85.2*/("""
"""))
      }
    }
  }

  def render(journal:DBviews.vReferences,struc:List[DBviews.vReferenceDisplay],lc:DBviews.vLC,bio:List[DBviews.vBCToJournal],massSpec:DBviews.vMassSpec,userProvider:UserProvider): play.twirl.api.HtmlFormat.Appendable = apply(journal,struc,lc,bio,massSpec,userProvider)

  def f:((DBviews.vReferences,List[DBviews.vReferenceDisplay],DBviews.vLC,List[DBviews.vBCToJournal],DBviews.vMassSpec,UserProvider) => play.twirl.api.HtmlFormat.Appendable) = (journal,struc,lc,bio,massSpec,userProvider) => apply(journal,struc,lc,bio,massSpec,userProvider)

  def ref: this.type = this

}


}
}

/**/
object referenceDisplay extends referenceDisplay_Scope0.referenceDisplay_Scope1.referenceDisplay
              /*
                  -- GENERATED --
                  DATE: Wed Jun 19 09:50:50 UTC 2019
                  SOURCE: /app/app/views/references/referenceDisplay.scala.html
                  HASH: 5c02a08622c8b31c3fa45d87b4abb816d5d448c2
                  MATRIX: 960->30|1233->207|1261->210|1301->242|1339->243|1367->245|1727->578|1743->585|1774->595|1993->787|2009->794|2036->800|2190->927|2206->934|2235->942|2265->945|2281->952|2316->966|2346->969|2362->976|2389->982|2495->1061|2511->1068|2542->1078|3189->1698|3221->1714|3260->1715|3313->1740|3378->1778|3412->1803|3443->1806|3454->1808|3481->1825|3494->1830|3533->1831|3562->1832|3608->1846|3674->1885|3685->1887|3719->1900|3748->1902|3785->1918|3842->1947|3926->2004|3937->2006|3966->2014|4058->2079|4162->2161|4192->2162|4351->2290|4406->2317|4562->2446|4576->2451|4618->2472|4667->2494|4681->2499|4742->2539|4791->2561|4814->2575|4853->2576|4906->2602|4920->2607|4970->2636|5023->2658|5072->2680|5086->2685|5146->2724|5195->2811|5240->2828|5347->2905
                  LINES: 30->2|35->2|37->4|37->4|37->4|39->6|44->11|44->11|44->11|52->19|52->19|52->19|59->26|59->26|59->26|59->26|59->26|59->26|59->26|59->26|59->26|62->29|62->29|62->29|77->44|77->44|77->44|78->45|79->46|79->46|79->46|79->46|79->46|79->46|79->46|79->46|79->46|80->47|80->47|80->47|80->47|80->47|81->48|83->50|83->50|83->50|84->51|84->51|84->51|87->54|90->57|97->64|97->64|97->64|98->65|98->65|98->65|99->66|99->66|99->66|100->67|100->67|100->67|101->68|102->69|102->69|102->69|103->70|104->71|118->85
                  -- GENERATED --
              */
          