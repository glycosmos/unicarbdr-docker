
package views.html.additionalInfo

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object structureDetails_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class structureDetails extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template3[Double,String,String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(mass:Double, deriv:String, redEnd:String):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.44*/("""
"""),format.raw/*2.1*/("""<!--
<div class="info">
    <h3>Structure details</h3>

    <div class='taxonomy'>
        <p><b>Reducing Mass</b>if(sd.mass>0)"""),format.raw/*7.45*/("""{"""),format.raw/*7.46*/(""" """),format.raw/*7.47*/("""("%.2f".format(sd.mass))"""),format.raw/*7.71*/("""}"""),format.raw/*7.72*/("""
            """),format.raw/*8.13*/("""<br>(Calculated Monoisotopic)</p>
        <p><b>Reducing End </b>if(sd.reducing_name != null)"""),format.raw/*9.60*/("""{"""),format.raw/*9.61*/("""sd.reducing_name """),format.raw/*9.78*/("""}"""),format.raw/*9.79*/(""" """),format.raw/*9.80*/("""else """),format.raw/*9.85*/("""{"""),format.raw/*9.86*/("""Not available"""),format.raw/*9.99*/("""}"""),format.raw/*9.100*/("""  """),format.raw/*9.102*/("""</p>
        <p><b>Persubstitution </b>if(sd.pers_name != null)"""),format.raw/*10.59*/("""{"""),format.raw/*10.60*/("""sd.pers_name"""),format.raw/*10.72*/("""}"""),format.raw/*10.73*/(""" """),format.raw/*10.74*/("""else """),format.raw/*10.79*/("""{"""),format.raw/*10.80*/("""Not available"""),format.raw/*10.93*/("""}"""),format.raw/*10.94*/("""  """),format.raw/*10.96*/("""</p>
    </div>
</div>

-->

<div class="info">
    <h3>Structure details</h3>

    <div class='taxonomy'>
        <p><b>Reducing Mass</b>"""),_display_(/*20.33*/if(mass>0)/*20.43*/{_display_(Seq[Any](format.raw/*20.44*/(""" """),_display_(/*20.46*/("%.2f".format(mass)))))}),format.raw/*20.68*/("""
            """),format.raw/*21.13*/("""<br>(Calculated Monoisotopic)</p>
        <p><b>Reducing End </b>"""),_display_(/*22.33*/if(redEnd != null)/*22.51*/{_display_(Seq[Any](_display_(/*22.53*/redEnd),format.raw/*22.59*/(""" """)))}/*22.62*/else/*22.67*/{_display_(Seq[Any](format.raw/*22.68*/("""Not available""")))}),format.raw/*22.82*/("""  """),format.raw/*22.84*/("""</p>
        <p><b>Persubstitution </b>"""),_display_(/*23.36*/if(deriv != null)/*23.53*/{_display_(_display_(/*23.55*/deriv))}/*23.62*/else/*23.67*/{_display_(Seq[Any](format.raw/*23.68*/("""Not available""")))}),format.raw/*23.82*/("""  """),format.raw/*23.84*/("""</p>
    </div>
</div>

"""))
      }
    }
  }

  def render(mass:Double,deriv:String,redEnd:String): play.twirl.api.HtmlFormat.Appendable = apply(mass,deriv,redEnd)

  def f:((Double,String,String) => play.twirl.api.HtmlFormat.Appendable) = (mass,deriv,redEnd) => apply(mass,deriv,redEnd)

  def ref: this.type = this

}


}

/**/
object structureDetails extends structureDetails_Scope0.structureDetails
              /*
                  -- GENERATED --
                  DATE: Wed Jun 19 09:50:50 UTC 2019
                  SOURCE: /app/app/views/additionalInfo/structureDetails.scala.html
                  HASH: f012eb717d43434e7dfc4b087911f9f9cd6179a4
                  MATRIX: 796->1|933->43|960->44|1114->171|1142->172|1170->173|1221->197|1249->198|1289->211|1409->304|1437->305|1481->322|1509->323|1537->324|1569->329|1597->330|1637->343|1666->344|1696->346|1787->409|1816->410|1856->422|1885->423|1914->424|1947->429|1976->430|2017->443|2046->444|2076->446|2242->585|2261->595|2300->596|2329->598|2375->620|2416->633|2509->699|2536->717|2575->719|2602->725|2623->728|2636->733|2675->734|2720->748|2750->750|2817->790|2843->807|2873->809|2890->816|2903->821|2942->822|2987->836|3017->838
                  LINES: 27->1|32->1|33->2|38->7|38->7|38->7|38->7|38->7|39->8|40->9|40->9|40->9|40->9|40->9|40->9|40->9|40->9|40->9|40->9|41->10|41->10|41->10|41->10|41->10|41->10|41->10|41->10|41->10|41->10|51->20|51->20|51->20|51->20|51->20|52->21|53->22|53->22|53->22|53->22|53->22|53->22|53->22|53->22|53->22|54->23|54->23|54->23|54->23|54->23|54->23|54->23|54->23
                  -- GENERATED --
              */
          