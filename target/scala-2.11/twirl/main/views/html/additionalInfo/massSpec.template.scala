
package views.html.additionalInfo

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object massSpec_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class massSpec extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[DBviews.vMassSpec,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(massSpec: DBviews.vMassSpec):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.31*/("""

"""),format.raw/*3.1*/("""<div class="info">
    <h3>Mass Spec</h3>

    <div class='taxonomy'>
        <p>"""),_display_(/*7.13*/massSpec/*7.21*/.manufacturer_name),format.raw/*7.39*/(""" """),_display_(/*7.41*/massSpec/*7.49*/.model),format.raw/*7.55*/(""" """),_display_(/*7.57*/massSpec/*7.65*/.ionisation_type),format.raw/*7.81*/("""</p>
    </div>
</div>
"""))
      }
    }
  }

  def render(massSpec:DBviews.vMassSpec): play.twirl.api.HtmlFormat.Appendable = apply(massSpec)

  def f:((DBviews.vMassSpec) => play.twirl.api.HtmlFormat.Appendable) = (massSpec) => apply(massSpec)

  def ref: this.type = this

}


}

/**/
object massSpec extends massSpec_Scope0.massSpec
              /*
                  -- GENERATED --
                  DATE: Wed Jun 19 09:50:50 UTC 2019
                  SOURCE: /app/app/views/additionalInfo/massSpec.scala.html
                  HASH: bd722d299cf81f093e366456b745c7c0cf835b8b
                  MATRIX: 777->1|901->30|929->32|1037->114|1053->122|1091->140|1119->142|1135->150|1161->156|1189->158|1205->166|1241->182
                  LINES: 27->1|32->1|34->3|38->7|38->7|38->7|38->7|38->7|38->7|38->7|38->7|38->7
                  -- GENERATED --
              */
          