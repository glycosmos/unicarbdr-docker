
package views.html.additionalInfo

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object mirage_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class mirage extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[Long,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(id:Long):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.11*/("""

"""),format.raw/*3.1*/("""<div class="info">
    <h3>MIRAGE</h3>
    <div class='taxonomy'>
        <a href="/mirageReport/"""),_display_(/*6.33*/id),format.raw/*6.35*/(""""><span class='label label-success'>See report</span></a>
    </div>
</div>

"""))
      }
    }
  }

  def render(id:Long): play.twirl.api.HtmlFormat.Appendable = apply(id)

  def f:((Long) => play.twirl.api.HtmlFormat.Appendable) = (id) => apply(id)

  def ref: this.type = this

}


}

/**/
object mirage extends mirage_Scope0.mirage
              /*
                  -- GENERATED --
                  DATE: Wed Jun 19 09:50:50 UTC 2019
                  SOURCE: /app/app/views/additionalInfo/mirage.scala.html
                  HASH: a8380253c26873f60c82d841bfaad5499e5c434a
                  MATRIX: 760->1|864->10|892->12|1016->110|1038->112
                  LINES: 27->1|32->1|34->3|37->6|37->6
                  -- GENERATED --
              */
          