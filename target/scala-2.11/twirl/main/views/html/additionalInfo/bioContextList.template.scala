
package views.html.additionalInfo

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object bioContextList_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class bioContextList extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[List[DBviews.vBCToJournal],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(bio: List[DBviews.vBCToJournal]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.35*/("""

"""),format.raw/*3.1*/("""<div class="info">
    <h3>Biological Context</h3>

    """),_display_(/*6.6*/for(bc <- bio) yield /*6.20*/{_display_(Seq[Any](format.raw/*6.21*/("""
        """),format.raw/*7.9*/("""<div class='taxonomy'>
            <a href="https://www.ncbi.nlm.nih.gov/taxonomy/?term="""),_display_(/*8.67*/bc/*8.69*/.ncbi_id),format.raw/*8.77*/("""" target="_blank"><span class='label label-important'> Taxonomy - """),_display_(/*8.144*/bc/*8.146*/.taxon),format.raw/*8.152*/(""" """),format.raw/*8.153*/("""</span></a>
        </div>
        <div class='source'>
            <a href="https://www.ncbi.nlm.nih.gov/mesh/?term="""),_display_(/*11.63*/bc/*11.65*/.mesh_id),format.raw/*11.73*/("""" target="_blank"><span class='label label-important'> Tissue - """),_display_(/*11.138*/bc/*11.140*/.tissue_taxon),format.raw/*11.153*/(""" """),format.raw/*11.154*/("""</span></a>
        </div>


        """),_display_(/*15.10*/if(bc.glycoprotein_name  != null)/*15.43*/{_display_(Seq[Any](format.raw/*15.44*/("""
                """),format.raw/*16.17*/("""<div class='source'>
                    <a><span class='label label-important'> Glycoprotein - """),_display_(/*17.77*/bc/*17.79*/.glycoprotein_name),format.raw/*17.97*/(""" """),format.raw/*17.98*/("""</span></a>
                </div>
        """)))}),format.raw/*19.10*/("""

        """),_display_(/*21.10*/if(bc.cell_line_name != null)/*21.39*/{_display_(Seq[Any](format.raw/*21.40*/("""
            """),format.raw/*22.13*/("""<div class='source'>
                <a><span class='label label-important'> Cell line - """),_display_(/*23.70*/bc/*23.72*/.cell_line_name),format.raw/*23.87*/(""" """),format.raw/*23.88*/("""</span></a>
            </div>
        """)))}),format.raw/*25.10*/("""


    """)))}),format.raw/*28.6*/("""
"""),format.raw/*29.1*/("""</div>

"""))
      }
    }
  }

  def render(bio:List[DBviews.vBCToJournal]): play.twirl.api.HtmlFormat.Appendable = apply(bio)

  def f:((List[DBviews.vBCToJournal]) => play.twirl.api.HtmlFormat.Appendable) = (bio) => apply(bio)

  def ref: this.type = this

}


}

/**/
object bioContextList extends bioContextList_Scope0.bioContextList
              /*
                  -- GENERATED --
                  DATE: Wed Jun 19 09:50:50 UTC 2019
                  SOURCE: /app/app/views/additionalInfo/bioContextList.scala.html
                  HASH: 53aad6a39f739218eb3ad9e8c045e6645b7232c5
                  MATRIX: 798->1|926->34|954->36|1036->93|1065->107|1103->108|1138->117|1253->206|1263->208|1291->216|1385->283|1396->285|1423->291|1452->292|1597->410|1608->412|1637->420|1730->485|1742->487|1777->500|1807->501|1872->539|1914->572|1953->573|1998->590|2122->687|2133->689|2172->707|2201->708|2276->752|2314->763|2352->792|2391->793|2432->806|2549->896|2560->898|2596->913|2625->914|2696->954|2734->962|2762->963
                  LINES: 27->1|32->1|34->3|37->6|37->6|37->6|38->7|39->8|39->8|39->8|39->8|39->8|39->8|39->8|42->11|42->11|42->11|42->11|42->11|42->11|42->11|46->15|46->15|46->15|47->16|48->17|48->17|48->17|48->17|50->19|52->21|52->21|52->21|53->22|54->23|54->23|54->23|54->23|56->25|59->28|60->29
                  -- GENERATED --
              */
          