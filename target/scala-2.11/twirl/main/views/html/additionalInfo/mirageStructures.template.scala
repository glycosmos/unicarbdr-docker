
package views.html.additionalInfo

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object mirageStructures_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class mirageStructures extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[DBviews.vMirageStructures,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(mStruct: DBviews.vMirageStructures):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.38*/("""



"""),format.raw/*5.1*/("""<div class="info">

    <a onclick="toggle_visibility('foo');"><h3>MIRAGE Structural data <img src=""""),_display_(/*7.82*/routes/*7.88*/.Assets.versioned("images/icons/icon_arrow.png")),format.raw/*7.136*/(""""></h3></a>

    <div class='taxonomy'>
        <div id="foo" style="display:none;">
            <p><b>Precursor </b>"""),_display_(/*11.34*/mStruct/*11.41*/.base_peak_mz),format.raw/*11.54*/("""</p>
            <p><b>Retention time </b>"""),_display_(/*12.39*/mStruct/*12.46*/.retention_time),format.raw/*12.61*/("""</p>
            <p><b>Stability </b>"""),_display_(/*13.34*/mStruct/*13.41*/.stability),format.raw/*13.51*/("""</p>
            <p><b>Orthogonal approaches </b>"""),_display_(/*14.46*/mStruct/*14.53*/.orthogonal_approaches),format.raw/*14.75*/("""</p>
            <p><b>Quantity </b>"""),_display_(/*15.33*/mStruct/*15.40*/.quantity),format.raw/*15.49*/("""</p>
            <br>
            <p><b>Database </b>"""),_display_(/*17.33*/mStruct/*17.40*/.name_database),format.raw/*17.54*/("""</p>
            <p><b>Taxonomical restrictions </b>"""),_display_(/*18.49*/mStruct/*18.56*/.taxonomical_restrictions),format.raw/*18.81*/("""</p>
            <p><b>Other restrictions </b>"""),_display_(/*19.43*/mStruct/*19.50*/.other_restrictions),format.raw/*19.69*/("""</p>
            <p><b>Allowed cleavages </b>"""),_display_(/*20.42*/mStruct/*20.49*/.allowed_cleavages),format.raw/*20.67*/("""</p>
            <p><b>Parent error </b>"""),_display_(/*21.37*/mStruct/*21.44*/.parent_error),format.raw/*21.57*/("""</p>
            <p><b>Fragment error </b>"""),_display_(/*22.39*/mStruct/*22.46*/.fragment_error),format.raw/*22.61*/("""</p>
            <p><b>Mass accuracy </b>"""),_display_(/*23.38*/mStruct/*23.45*/.mass_accuracy),format.raw/*23.59*/("""</p>
            <p><b>Scoring method </b>"""),_display_(/*24.39*/mStruct/*24.46*/.scoring_method),format.raw/*24.61*/("""</p>
            <p><b>Scoring algorithm </b>"""),_display_(/*25.42*/mStruct/*25.49*/.scoring_algorithm),format.raw/*25.67*/("""</p>
            <p><b>Scoring result </b>"""),_display_(/*26.39*/mStruct/*26.46*/.scoring_result),format.raw/*26.61*/("""</p>
            <p><b>Scoring value format </b>"""),_display_(/*27.45*/mStruct/*27.52*/.scoring_format),format.raw/*27.67*/("""</p>
            <br>
            <p><b>Validation status </b>"""),_display_(/*29.42*/mStruct/*29.49*/.status),format.raw/*29.56*/("""</p>
            <p><b>Validation value format </b>"""),_display_(/*30.48*/mStruct/*30.55*/.validation_format),format.raw/*30.73*/("""</p>
            <p><b>Validation result </b>"""),_display_(/*31.42*/mStruct/*31.49*/.validation_result),format.raw/*31.67*/("""</p>
            <p><b>Validation level </b>"""),_display_(/*32.41*/mStruct/*32.48*/.validation_level),format.raw/*32.65*/("""</p>
        </div>
    </div>
</div>


<script type="text/javascript">
function toggle_visibility(id) """),format.raw/*39.32*/("""{"""),format.raw/*39.33*/("""
    """),format.raw/*40.5*/("""var e = document.getElementById(id);
    if(e.style.display == 'none')
        e.style.display = 'block';
    else
        e.style.display = 'none';
"""),format.raw/*45.1*/("""}"""),format.raw/*45.2*/("""

"""),format.raw/*47.1*/("""</script>
"""))
      }
    }
  }

  def render(mStruct:DBviews.vMirageStructures): play.twirl.api.HtmlFormat.Appendable = apply(mStruct)

  def f:((DBviews.vMirageStructures) => play.twirl.api.HtmlFormat.Appendable) = (mStruct) => apply(mStruct)

  def ref: this.type = this

}


}

/**/
object mirageStructures extends mirageStructures_Scope0.mirageStructures
              /*
                  -- GENERATED --
                  DATE: Wed Jun 19 09:50:50 UTC 2019
                  SOURCE: /app/app/views/additionalInfo/mirageStructures.scala.html
                  HASH: e65f1241d35ed97a8d937e2d23047b3d5897a496
                  MATRIX: 801->1|932->37|962->41|1089->142|1103->148|1172->196|1317->314|1333->321|1367->334|1437->377|1453->384|1489->399|1554->437|1570->444|1601->454|1678->504|1694->511|1737->533|1801->570|1817->577|1847->586|1928->640|1944->647|1979->661|2059->714|2075->721|2121->746|2195->793|2211->800|2251->819|2324->865|2340->872|2379->890|2447->931|2463->938|2497->951|2567->994|2583->1001|2619->1016|2688->1058|2704->1065|2739->1079|2809->1122|2825->1129|2861->1144|2934->1190|2950->1197|2989->1215|3059->1258|3075->1265|3111->1280|3187->1329|3203->1336|3239->1351|3329->1414|3345->1421|3373->1428|3452->1480|3468->1487|3507->1505|3580->1551|3596->1558|3635->1576|3707->1621|3723->1628|3761->1645|3892->1748|3921->1749|3953->1754|4129->1903|4157->1904|4186->1906
                  LINES: 27->1|32->1|36->5|38->7|38->7|38->7|42->11|42->11|42->11|43->12|43->12|43->12|44->13|44->13|44->13|45->14|45->14|45->14|46->15|46->15|46->15|48->17|48->17|48->17|49->18|49->18|49->18|50->19|50->19|50->19|51->20|51->20|51->20|52->21|52->21|52->21|53->22|53->22|53->22|54->23|54->23|54->23|55->24|55->24|55->24|56->25|56->25|56->25|57->26|57->26|57->26|58->27|58->27|58->27|60->29|60->29|60->29|61->30|61->30|61->30|62->31|62->31|62->31|63->32|63->32|63->32|70->39|70->39|71->40|76->45|76->45|78->47
                  -- GENERATED --
              */
          