
package views.html.additionalInfo

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object bioContext_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class bioContext extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[DBviews.vBiologicalContext,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(bio: DBviews.vBiologicalContext):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.35*/("""

"""),format.raw/*3.1*/("""<div class="info">
    <h3>Biological Context</h3>

        <div class='taxonomy'>
            <a href="https://www.ncbi.nlm.nih.gov/taxonomy/?term="""),_display_(/*7.67*/bio/*7.70*/.ncbi_id),format.raw/*7.78*/("""" target="_blank"><span class='label label-important'>Taxonomy - """),_display_(/*7.144*/bio/*7.147*/.taxon),format.raw/*7.153*/(""" """),format.raw/*7.154*/("""</span></a>
        </div>
        <div class='source'>
            <a href="https://www.ncbi.nlm.nih.gov/mesh/?term="""),_display_(/*10.63*/bio/*10.66*/.mesh_id),format.raw/*10.74*/("""" target="_blank"><span class='label label-important'> Tissue - """),_display_(/*10.139*/bio/*10.142*/.tissue_taxon),format.raw/*10.155*/(""" """),format.raw/*10.156*/("""</span></a>
        </div>

        """),_display_(/*13.10*/if(bio.glycoprotein_name  != null)/*13.44*/{_display_(Seq[Any](format.raw/*13.45*/("""
            """),format.raw/*14.13*/("""<div class='source'>
                <a><span class='label label-important'> Glycoprotein - """),_display_(/*15.73*/bio/*15.76*/.glycoprotein_name),format.raw/*15.94*/(""" """),format.raw/*15.95*/("""</span></a>
            </div>
        """)))}),format.raw/*17.10*/("""


"""),format.raw/*20.1*/("""</div>
"""))
      }
    }
  }

  def render(bio:DBviews.vBiologicalContext): play.twirl.api.HtmlFormat.Appendable = apply(bio)

  def f:((DBviews.vBiologicalContext) => play.twirl.api.HtmlFormat.Appendable) = (bio) => apply(bio)

  def ref: this.type = this

}


}

/**/
object bioContext extends bioContext_Scope0.bioContext
              /*
                  -- GENERATED --
                  DATE: Wed Jun 19 09:50:50 UTC 2019
                  SOURCE: /app/app/views/additionalInfo/bioContext.scala.html
                  HASH: a91de25d680dcf4b2aa43b216e52fdd699ee942c
                  MATRIX: 790->1|918->34|946->36|1121->185|1132->188|1160->196|1253->262|1265->265|1292->271|1321->272|1466->390|1478->393|1507->401|1600->466|1613->469|1648->482|1678->483|1742->520|1785->554|1824->555|1865->568|1985->661|1997->664|2036->682|2065->683|2136->723|2166->726
                  LINES: 27->1|32->1|34->3|38->7|38->7|38->7|38->7|38->7|38->7|38->7|41->10|41->10|41->10|41->10|41->10|41->10|41->10|44->13|44->13|44->13|45->14|46->15|46->15|46->15|46->15|48->17|51->20
                  -- GENERATED --
              */
          