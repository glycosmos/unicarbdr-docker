
package views.html.additionalInfo

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object hplc_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class hplc extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[DBviews.vLC,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(lc: DBviews.vLC):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.19*/("""

"""),format.raw/*3.1*/("""<div class="info">
    <h3>HPLC</h3>

    <div class='taxonomy'>
        <p><b>Column </b>"""),_display_(/*7.27*/if(lc.model != null)/*7.47*/{_display_(Seq[Any](_display_(/*7.49*/lc/*7.51*/.model),format.raw/*7.57*/(""" """)))}/*7.60*/else/*7.65*/{_display_(Seq[Any](format.raw/*7.66*/("""Not recorded""")))}),format.raw/*7.79*/("""  """),format.raw/*7.81*/("""</p>
    </div>
    <div class='source'>
        <p><b>Method </b>
        <ul>
            <li style="list-style-type:circle;">"""),_display_(/*12.50*/if(lc.runrate != null)/*12.72*/ {_display_(_display_(/*12.75*/lc/*12.77*/.runrate))}/*12.87*/else/*12.92*/{_display_(Seq[Any](format.raw/*12.93*/(""" """),format.raw/*12.94*/("""Run time not recorded """)))}),format.raw/*12.117*/("""</li>
            """),_display_(/*13.14*/if(lc.solvent_a != null )/*13.39*/{_display_(Seq[Any](format.raw/*13.40*/("""<li style="list-style-type:circle;">"""),_display_(/*13.77*/lc/*13.79*/.solvent_a),format.raw/*13.89*/("""</li> """)))}),format.raw/*13.96*/("""
            """),_display_(/*14.14*/if(lc.solvent_b != null )/*14.39*/{_display_(Seq[Any](format.raw/*14.40*/("""<li style="list-style-type:circle;">"""),_display_(/*14.77*/lc/*14.79*/.solvent_b),format.raw/*14.89*/("""</li> """)))}),format.raw/*14.96*/("""
        """),format.raw/*15.9*/("""</ul>
    </div>

</div>
"""))
      }
    }
  }

  def render(lc:DBviews.vLC): play.twirl.api.HtmlFormat.Appendable = apply(lc)

  def f:((DBviews.vLC) => play.twirl.api.HtmlFormat.Appendable) = (lc) => apply(lc)

  def ref: this.type = this

}


}

/**/
object hplc extends hplc_Scope0.hplc
              /*
                  -- GENERATED --
                  DATE: Wed Jun 19 09:50:50 UTC 2019
                  SOURCE: /app/app/views/additionalInfo/hplc.scala.html
                  HASH: 36a427e089b29f73965a80737aeeac81414af931
                  MATRIX: 763->1|875->18|903->20|1020->111|1048->131|1086->133|1096->135|1122->141|1142->144|1154->149|1192->150|1235->163|1264->165|1420->294|1451->316|1482->319|1493->321|1513->331|1526->336|1565->337|1594->338|1649->361|1695->380|1729->405|1768->406|1832->443|1843->445|1874->455|1912->462|1953->476|1987->501|2026->502|2090->539|2101->541|2132->551|2170->558|2206->567
                  LINES: 27->1|32->1|34->3|38->7|38->7|38->7|38->7|38->7|38->7|38->7|38->7|38->7|38->7|43->12|43->12|43->12|43->12|43->12|43->12|43->12|43->12|43->12|44->13|44->13|44->13|44->13|44->13|44->13|44->13|45->14|45->14|45->14|45->14|45->14|45->14|45->14|46->15
                  -- GENERATED --
              */
          