
package views.html.mirageReport

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object reportSearchable_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class reportSearchable extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template4[List[DBviews.vMirageSample],DBviews.vMirageLc,DBviews.vMirageMs,List[DBviews.vMirageSoftware],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(vSample: List[DBviews.vMirageSample], vLc: DBviews.vMirageLc, vMs: DBviews.vMirageMs, vSoft:List[DBviews.vMirageSoftware]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.125*/("""

"""),format.raw/*3.1*/("""<div class="info">


    <h3>Sample preparation</h3>
    """),_display_(/*7.6*/for(vS <- vSample) yield /*7.24*/{_display_(Seq[Any](format.raw/*7.25*/("""
        """),format.raw/*8.9*/("""<h5>Species</h5>
        """),_display_(/*9.10*/for(item <- vS.getTaxonomies()) yield /*9.41*/{_display_(Seq[Any](format.raw/*9.42*/("""
            """),format.raw/*10.13*/("""<a href="#">"""),_display_(/*10.26*/item),format.raw/*10.30*/(""" """),format.raw/*10.31*/("""</a> <br>
        """)))}),format.raw/*11.10*/("""

        """),format.raw/*13.9*/("""<h5>Tissue</h5>
        """),_display_(/*14.10*/for(item <- vS.getTissues()) yield /*14.38*/{_display_(Seq[Any](format.raw/*14.39*/("""
            """),format.raw/*15.13*/("""<a href="#">"""),_display_(/*15.26*/item),format.raw/*15.30*/(""" """),format.raw/*15.31*/("""</a> <br>
        """)))}),format.raw/*16.10*/("""
        """),_display_(/*17.10*/if(vS.getGlycoproteins().size >0)/*17.43*/{_display_(Seq[Any](format.raw/*17.44*/("""
            """),format.raw/*18.13*/("""<h5>Glycoprotein</h5>
            """),_display_(/*19.14*/for(item <- vS.getGlycoproteins()) yield /*19.48*/{_display_(Seq[Any](format.raw/*19.49*/("""
                """),format.raw/*20.17*/("""<a href="#">"""),_display_(/*20.30*/item),format.raw/*20.34*/(""" """),format.raw/*20.35*/("""</a> <br>
            """)))}),format.raw/*21.14*/("""
        """)))}),format.raw/*22.10*/("""
        """),_display_(/*23.10*/if(vS.getCellLines().size >0)/*23.39*/{_display_(Seq[Any](format.raw/*23.40*/("""
            """),format.raw/*24.13*/("""<h5>Cell Line</h5>
            """),_display_(/*25.14*/for(item <- vS.getCellLines()) yield /*25.44*/{_display_(Seq[Any](format.raw/*25.45*/("""
                """),format.raw/*26.17*/("""<a href="#">"""),_display_(/*26.30*/item),format.raw/*26.34*/(""" """),format.raw/*26.35*/("""</a> <br>
            """)))}),format.raw/*27.14*/("""
        """)))}),format.raw/*28.10*/("""
        """),_display_(/*29.10*/if(vS.getTreatments().size >0)/*29.40*/{_display_(Seq[Any](format.raw/*29.41*/("""
            """),format.raw/*30.13*/("""<h5>Treatments</h5>
            """),_display_(/*31.14*/for(item <- vS.getTreatments()) yield /*31.45*/{_display_(Seq[Any](format.raw/*31.46*/("""
                """),_display_(/*32.18*/if(item !=null)/*32.33*/{_display_(Seq[Any](format.raw/*32.34*/("""
                    """),format.raw/*33.21*/("""<a href="#">"""),_display_(/*33.34*/item),format.raw/*33.38*/(""" """),format.raw/*33.39*/("""</a> <br>
                """)))}),format.raw/*34.18*/("""
            """)))}),format.raw/*35.14*/("""
        """)))}),format.raw/*36.10*/("""


        """),_display_(/*39.10*/if(vS.starting_material != null)/*39.42*/{_display_(Seq[Any](format.raw/*39.43*/("""
            """),format.raw/*40.13*/("""<h5>Starting material</h5>
            <a href="#">"""),_display_(/*41.26*/vS/*41.28*/.starting_material),format.raw/*41.46*/(""" """),format.raw/*41.47*/("""</a> <br>
        """)))}),format.raw/*42.10*/("""
    """)))}),format.raw/*43.6*/("""

    """),format.raw/*45.5*/("""<hr>
    <h3>HPLC</h3>
    <h5>Instrument:</h5><a href="#">"""),_display_(/*47.38*/vLc/*47.41*/.inst_name),format.raw/*47.51*/(""" """),format.raw/*47.52*/("""</a><a href="#">"""),_display_(/*47.69*/vLc/*47.72*/.inst_model),format.raw/*47.83*/(""" """),format.raw/*47.84*/("""</a> <br>
    <hr>
    <h3>MS</h3>
    <h5>Instrument:</h5><a href="#">"""),_display_(/*50.38*/vMs/*50.41*/.manufacturer_name),format.raw/*50.59*/(""" """),format.raw/*50.60*/("""</a><a href="#">"""),_display_(/*50.77*/vMs/*50.80*/.device_model),format.raw/*50.93*/(""" """),format.raw/*50.94*/("""</a><a href="#">"""),_display_(/*50.111*/vMs/*50.114*/.ionisation_type),format.raw/*50.130*/(""" """),format.raw/*50.131*/("""</a> <br>
    <hr>
    <h3>Software</h3>
    """),_display_(/*53.6*/for(vSft <- vSoft) yield /*53.24*/{_display_(Seq[Any](format.raw/*53.25*/("""
        """),format.raw/*54.9*/("""<h5>"""),_display_(/*54.14*/vSft/*54.18*/.task),format.raw/*54.23*/("""</h5>
        ("""),_display_(/*55.11*/vSft/*55.15*/.manufacturer),format.raw/*55.28*/(""") <a href="#">"""),_display_(/*55.43*/vSft/*55.47*/.name_software),format.raw/*55.61*/(""" """),_display_(/*55.63*/vSft/*55.67*/.version_software),format.raw/*55.84*/("""</a><br>

        """),_display_(/*57.10*/if(vSft.getFiles().size >0)/*57.37*/{_display_(Seq[Any](format.raw/*57.38*/("""
            """),_display_(/*58.14*/for(item <- vSft.getFiles()) yield /*58.42*/{_display_(Seq[Any](format.raw/*58.43*/("""
                """),format.raw/*59.17*/("""- """),_display_(/*59.20*/item),format.raw/*59.24*/(""" """),format.raw/*59.25*/("""<br>
            """)))}),format.raw/*60.14*/("""
        """)))}),format.raw/*61.10*/("""
    """)))}),format.raw/*62.6*/("""


"""),format.raw/*65.1*/("""</div>
</div>

"""))
      }
    }
  }

  def render(vSample:List[DBviews.vMirageSample],vLc:DBviews.vMirageLc,vMs:DBviews.vMirageMs,vSoft:List[DBviews.vMirageSoftware]): play.twirl.api.HtmlFormat.Appendable = apply(vSample,vLc,vMs,vSoft)

  def f:((List[DBviews.vMirageSample],DBviews.vMirageLc,DBviews.vMirageMs,List[DBviews.vMirageSoftware]) => play.twirl.api.HtmlFormat.Appendable) = (vSample,vLc,vMs,vSoft) => apply(vSample,vLc,vMs,vSoft)

  def ref: this.type = this

}


}

/**/
object reportSearchable extends reportSearchable_Scope0.reportSearchable
              /*
                  -- GENERATED --
                  DATE: Wed Jun 19 09:50:50 UTC 2019
                  SOURCE: /app/app/views/mirageReport/reportSearchable.scala.html
                  HASH: 724495f25f0a4b55328befae950a935d314df5d0
                  MATRIX: 867->1|1086->124|1114->126|1197->184|1230->202|1268->203|1303->212|1355->238|1401->269|1439->270|1480->283|1520->296|1545->300|1574->301|1624->320|1661->330|1713->355|1757->383|1796->384|1837->397|1877->410|1902->414|1931->415|1981->434|2018->444|2060->477|2099->478|2140->491|2202->526|2252->560|2291->561|2336->578|2376->591|2401->595|2430->596|2484->619|2525->629|2562->639|2600->668|2639->669|2680->682|2739->714|2785->744|2824->745|2869->762|2909->775|2934->779|2963->780|3017->803|3058->813|3095->823|3134->853|3173->854|3214->867|3274->900|3321->931|3360->932|3405->950|3429->965|3468->966|3517->987|3557->1000|3582->1004|3611->1005|3669->1032|3714->1046|3755->1056|3794->1068|3835->1100|3874->1101|3915->1114|3994->1166|4005->1168|4044->1186|4073->1187|4123->1206|4159->1212|4192->1218|4279->1278|4291->1281|4322->1291|4351->1292|4395->1309|4407->1312|4439->1323|4468->1324|4567->1396|4579->1399|4618->1417|4647->1418|4691->1435|4703->1438|4737->1451|4766->1452|4811->1469|4824->1472|4862->1488|4892->1489|4964->1535|4998->1553|5037->1554|5073->1563|5105->1568|5118->1572|5144->1577|5187->1593|5200->1597|5234->1610|5276->1625|5289->1629|5324->1643|5353->1645|5366->1649|5404->1666|5450->1685|5486->1712|5525->1713|5566->1727|5610->1755|5649->1756|5694->1773|5724->1776|5749->1780|5778->1781|5827->1799|5868->1809|5904->1815|5934->1818
                  LINES: 27->1|32->1|34->3|38->7|38->7|38->7|39->8|40->9|40->9|40->9|41->10|41->10|41->10|41->10|42->11|44->13|45->14|45->14|45->14|46->15|46->15|46->15|46->15|47->16|48->17|48->17|48->17|49->18|50->19|50->19|50->19|51->20|51->20|51->20|51->20|52->21|53->22|54->23|54->23|54->23|55->24|56->25|56->25|56->25|57->26|57->26|57->26|57->26|58->27|59->28|60->29|60->29|60->29|61->30|62->31|62->31|62->31|63->32|63->32|63->32|64->33|64->33|64->33|64->33|65->34|66->35|67->36|70->39|70->39|70->39|71->40|72->41|72->41|72->41|72->41|73->42|74->43|76->45|78->47|78->47|78->47|78->47|78->47|78->47|78->47|78->47|81->50|81->50|81->50|81->50|81->50|81->50|81->50|81->50|81->50|81->50|81->50|81->50|84->53|84->53|84->53|85->54|85->54|85->54|85->54|86->55|86->55|86->55|86->55|86->55|86->55|86->55|86->55|86->55|88->57|88->57|88->57|89->58|89->58|89->58|90->59|90->59|90->59|90->59|91->60|92->61|93->62|96->65
                  -- GENERATED --
              */
          