
package views.html.mirageReport

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object report_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

     object report_Scope1 {
import service.UserProvider

class report extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template6[List[DBviews.vMirageSample],DBviews.vJournalResponsible,DBviews.vMirageLc,DBviews.vMirageMs,List[DBviews.vMirageSoftware],UserProvider,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*2.2*/(vSample: List[DBviews.vMirageSample], journalResp: DBviews.vJournalResponsible, vLc: DBviews.vMirageLc, vMs: DBviews.vMirageMs, vSoft:List[DBviews.vMirageSoftware], userProvider: UserProvider):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.195*/("""

"""),format.raw/*4.172*/("""

"""),_display_(/*6.2*/main(userProvider, "MIRAGE report")/*6.37*/{_display_(Seq[Any](format.raw/*6.38*/("""
"""),format.raw/*7.1*/("""<div class="about">
    <div class="container-fluid content">

        <div class="row-fluid">
            <div class="span12 centered">
                <h3 class="logo-text">
                    The Glycomic MS Database and Repository
                </h3>
            </div>
        </div>
        <div class="page-header row-fluid">
            <h1 class="">"""),_display_(/*18.27*/journalResp/*18.38*/.title),format.raw/*18.44*/("""</h1>
        </div>
        <div class="row-fluid info">
            <div class="span8 leftContent">
                <h3>Responsible: </h3>

                    """),_display_(/*24.22*/journalResp/*24.33*/.responsible_name),format.raw/*24.50*/(""" """),format.raw/*24.51*/(""". """),_display_(/*24.54*/journalResp/*24.65*/.affiliation),format.raw/*24.77*/(""" """),format.raw/*24.78*/(""". """),_display_(/*24.81*/journalResp/*24.92*/.contact_information),format.raw/*24.112*/("""

                """),format.raw/*26.17*/("""<br/><br/>
                <h3>Sample preparation</h3>
                <hr>
                """),_display_(/*29.18*/for(vS <- vSample) yield /*29.36*/{_display_(Seq[Any](format.raw/*29.37*/("""
                    """),format.raw/*30.21*/("""<h5>1.Sample Origin</h5><br/>
                    """),_display_(/*31.22*/if(vS.general_info !=null)/*31.48*/{_display_(Seq[Any](format.raw/*31.49*/("""
                        """),format.raw/*32.25*/("""<p><b class="report">General information: </b><br/>"""),_display_(/*32.77*/vS/*32.79*/.general_info),format.raw/*32.92*/("""</p>
                    """)))}/*33.22*/else/*33.26*/{_display_(Seq[Any](format.raw/*33.27*/("""
                        """),format.raw/*34.25*/("""<p><b class="report">General information: </b><br/>NA</p>
                    """)))}),format.raw/*35.22*/("""
                    """),_display_(/*36.22*/if(vS.growth_harvest != null)/*36.51*/{_display_(Seq[Any](format.raw/*36.52*/("""
                        """),format.raw/*37.25*/("""<p><b class="report">Growth/harvest conditions for recombinantly produced material: </b><br/>"""),_display_(/*37.119*/vS/*37.121*/.growth_harvest),format.raw/*37.136*/("""</p>
                    """)))}/*38.22*/else/*38.26*/{_display_(Seq[Any](format.raw/*38.27*/("""
                        """),format.raw/*39.25*/("""<p><b class="report">Growth/harvest conditions for recombinantly produced material: </b><br/>NA</p>
                    """)))}),format.raw/*40.22*/("""
                    """),_display_(/*41.22*/if(vS.treatment_storage != null)/*41.54*/{_display_(Seq[Any](format.raw/*41.55*/("""
                        """),format.raw/*42.25*/("""<p><b class="report">Treatments and/or storage conditions:</b><br/>"""),_display_(/*42.93*/vS/*42.95*/.treatment_storage),format.raw/*42.113*/("""</p>
                    """)))}/*43.22*/else/*43.26*/{_display_(Seq[Any](format.raw/*43.27*/("""
                        """),format.raw/*44.25*/("""<p><b class="report">Treatments and/or storage conditions:</b><br/>NA</p>
                    """)))}),format.raw/*45.22*/("""
                    """),_display_(/*46.22*/if(vS.vendor_item_info != null)/*46.53*/{_display_(Seq[Any](format.raw/*46.54*/("""
                        """),format.raw/*47.25*/("""<p><b class="report">For commercial material, vendor and applicable item information:</b><br/>"""),_display_(/*47.120*/vS/*47.122*/.vendor_item_info),format.raw/*47.139*/("""</p>
                    """)))}/*48.22*/else/*48.26*/{_display_(Seq[Any](format.raw/*48.27*/("""
                        """),format.raw/*49.25*/("""<p><b class="report">For commercial material, vendor and applicable item information:</b><br/>NA</p>
                    """)))}),format.raw/*50.22*/("""
                    """),_display_(/*51.22*/if(vS.generation_material !=null)/*51.55*/{_display_(Seq[Any](format.raw/*51.56*/("""
                        """),format.raw/*52.25*/("""<p><b class="report">Generation of chemically derived material: </b><br/>"""),_display_(/*52.99*/vS/*52.101*/.generation_material),format.raw/*52.121*/("""</p>
                    """)))}/*53.22*/else/*53.26*/{_display_(Seq[Any](format.raw/*53.27*/("""
                        """),format.raw/*54.25*/("""<p><b class="report">Generation of chemically derived material:</b><br/>NA</p>
                    """)))}),format.raw/*55.22*/("""


                    """),format.raw/*58.21*/("""<br/><h5>2. Sample Processing</h5><br/>

                    """),_display_(/*60.22*/if(vS.purification !=null)/*60.48*/{_display_(Seq[Any](format.raw/*60.49*/("""
                        """),format.raw/*61.25*/("""<p><b class="report">Purification steps: </b><br/>"""),_display_(/*61.76*/vS/*61.78*/.purification),format.raw/*61.91*/("""</p>
                    """)))}/*62.22*/else/*62.26*/{_display_(Seq[Any](format.raw/*62.27*/("""
                        """),format.raw/*63.25*/("""<p><b class="report">Purification steps: </b><br/>NA</p>
                    """)))}),format.raw/*64.22*/("""


                    """),format.raw/*67.21*/("""<br/><h5>2. Defined Sample</h5><br/>

                    """),_display_(/*69.22*/if(vS.sample_Name !=null)/*69.47*/{_display_(Seq[Any](format.raw/*69.48*/("""
                        """),format.raw/*70.25*/("""<p><b class="report">Sample name: </b><br/>"""),_display_(/*70.69*/vS/*70.71*/.sample_Name),format.raw/*70.83*/("""</p>
                    """)))}/*71.22*/else/*71.26*/{_display_(Seq[Any](format.raw/*71.27*/("""
                        """),format.raw/*72.25*/("""<p><b class="report">Sample name: </b><br/>NA</p>
                    """)))}),format.raw/*73.22*/("""
                """)))}),format.raw/*74.18*/("""

                """),format.raw/*76.17*/("""<br/><br/>
                <h3>HPLC</h3>
                <hr>


                <h5>1. General settings</h5><br/>



                """),_display_(/*85.18*/if(vLc.inst_name !=null)/*85.42*/{_display_(Seq[Any](format.raw/*85.43*/("""
                    """),format.raw/*86.21*/("""<p><b class="report">Instrument: </b>"""),_display_(/*86.59*/vLc/*86.62*/.inst_name),format.raw/*86.72*/(""" """),_display_(/*86.74*/vLc/*86.77*/.inst_model),format.raw/*86.88*/("""</p>
                """)))}/*87.18*/else/*87.22*/{_display_(Seq[Any](format.raw/*87.23*/("""
                    """),format.raw/*88.21*/("""<p><b class="report">Instrument: </b>NA</p>
                """)))}),format.raw/*89.18*/("""

                """),_display_(/*91.18*/if(vLc.inst_url !=null)/*91.41*/{_display_(Seq[Any](format.raw/*91.42*/("""
                    """),format.raw/*92.21*/("""<p><b class="report">Manufacturer url: </b><a href="http://"""),_display_(/*92.81*/vLc/*92.84*/.inst_url),format.raw/*92.93*/("""" target="_blank">"""),_display_(/*92.112*/vLc/*92.115*/.inst_url),format.raw/*92.124*/("""</a></p>
                """)))}/*93.18*/else/*93.22*/{_display_(Seq[Any](format.raw/*93.23*/("""
                    """),format.raw/*94.21*/("""<p><b class="report">Manufacturer url: </b>NA</p>
                """)))}),format.raw/*95.18*/("""

                """),_display_(/*97.18*/if(vLc.injector_type !=null)/*97.46*/{_display_(Seq[Any](format.raw/*97.47*/("""
                    """),format.raw/*98.21*/("""<p><b class="report">Injector: </b>"""),_display_(/*98.57*/vLc/*98.60*/.injector_type),format.raw/*98.74*/("""</p>
                """)))}/*99.18*/else/*99.22*/{_display_(Seq[Any](format.raw/*99.23*/("""
                    """),format.raw/*100.21*/("""<p><b class="report">Injector: </b>NA</p>
                """)))}),format.raw/*101.18*/("""

                """),_display_(/*103.18*/if(vLc.injector_settings !=null)/*103.50*/{_display_(Seq[Any](format.raw/*103.51*/("""
                    """),format.raw/*104.21*/("""<p><b class="report">Injector settings: </b>"""),_display_(/*104.66*/vLc/*104.69*/.injector_settings),format.raw/*104.87*/("""</p>
                """)))}/*105.18*/else/*105.22*/{_display_(Seq[Any](format.raw/*105.23*/("""
                    """),format.raw/*106.21*/("""<p><b class="report">Injector settings: </b>NA</p>
                """)))}),format.raw/*107.18*/("""



                """),format.raw/*111.17*/("""<br/><h5>2. Column</h5><br/>

                """),_display_(/*113.18*/if(vLc.man_name !=null)/*113.41*/{_display_(Seq[Any](format.raw/*113.42*/("""
                    """),format.raw/*114.21*/("""<p><b class="report">Manufacturer: </b> """),_display_(/*114.62*/vLc/*114.65*/.man_name),format.raw/*114.74*/("""</p>
                """)))}/*115.18*/else/*115.22*/{_display_(Seq[Any](format.raw/*115.23*/("""
                    """),format.raw/*116.21*/("""<p><b class="report">Manufacturer: </b> NA</p>
                """)))}),format.raw/*117.18*/("""

                """),_display_(/*119.18*/if(vLc.column_model !=null)/*119.45*/{_display_(Seq[Any](format.raw/*119.46*/("""
                    """),format.raw/*120.21*/("""<p><b class="report">Model: </b> """),_display_(/*120.55*/vLc/*120.58*/.column_model),format.raw/*120.71*/("""</p>
                """)))}/*121.18*/else/*121.22*/{_display_(Seq[Any](format.raw/*121.23*/("""
                    """),format.raw/*122.21*/("""<p><b class="report">Model: </b> NA</p>
                """)))}),format.raw/*123.18*/("""

                """),_display_(/*125.18*/if(vLc.type_chromatography !=null)/*125.52*/{_display_(Seq[Any](format.raw/*125.53*/("""
                    """),format.raw/*126.21*/("""<p><b class="report">Type of Chromatography: </b>"""),_display_(/*126.71*/vLc/*126.74*/.type_chromatography),format.raw/*126.94*/("""</p>
                """)))}/*127.18*/else/*127.22*/{_display_(Seq[Any](format.raw/*127.23*/("""
                    """),format.raw/*128.21*/("""<p><b class="report">Type of Chromatography: </b> NA</p>
                """)))}),format.raw/*129.18*/("""

                """),_display_(/*131.18*/if(vLc.packing_material !=null)/*131.49*/{_display_(Seq[Any](format.raw/*131.50*/("""
                    """),format.raw/*132.21*/("""<p><b class="report">Type of Material: </b>"""),_display_(/*132.65*/vLc/*132.68*/.packing_material),format.raw/*132.85*/("""</p>
                """)))}/*133.18*/else/*133.22*/{_display_(Seq[Any](format.raw/*133.23*/("""
                    """),format.raw/*134.21*/("""<p><b class="report">Type of Material: </b> NA</p>
                """)))}),format.raw/*135.18*/("""


                """),_display_(/*138.18*/if(vLc.column_size_width !=null)/*138.50*/{_display_(Seq[Any](format.raw/*138.51*/("""
                    """),format.raw/*139.21*/("""<p><b class="report">Column diameter: </b> """),_display_(/*139.65*/vLc/*139.68*/.column_size_width),format.raw/*139.86*/("""</p>
                """)))}/*140.18*/else/*140.22*/{_display_(Seq[Any](format.raw/*140.23*/("""
                    """),format.raw/*141.21*/("""<p><b class="report">Column diameter: </b> NA</p>
                """)))}),format.raw/*142.18*/("""


                """),_display_(/*145.18*/if(vLc.column_size_length !=null)/*145.51*/{_display_(Seq[Any](format.raw/*145.52*/("""
                    """),format.raw/*146.21*/("""<p><b class="report">Column length: </b>"""),_display_(/*146.62*/vLc/*146.65*/.column_size_length),format.raw/*146.84*/("""</p>
                """)))}/*147.18*/else/*147.22*/{_display_(Seq[Any](format.raw/*147.23*/("""
                    """),format.raw/*148.21*/("""<p><b class="report">Column length: </b> NA</p>
                """)))}),format.raw/*149.18*/("""

                """),_display_(/*151.18*/if(vLc.particle_size !=null)/*151.46*/{_display_(Seq[Any](format.raw/*151.47*/("""
                    """),format.raw/*152.21*/("""<p><b class="report">Particle size: </b>"""),_display_(/*152.62*/vLc/*152.65*/.particle_size),format.raw/*152.79*/("""</p>
                """)))}/*153.18*/else/*153.22*/{_display_(Seq[Any](format.raw/*153.23*/("""
                    """),format.raw/*154.21*/("""<p><b class="report">Particle size: </b> NA</p>
                """)))}),format.raw/*155.18*/("""

                """),_display_(/*157.18*/if(vLc.col_url !=null)/*157.40*/{_display_(Seq[Any](format.raw/*157.41*/("""
                    """),format.raw/*158.21*/("""<p><b class="report">Manufacturer url: </b><a href="http://"""),_display_(/*158.81*/vLc/*158.84*/.col_url),format.raw/*158.92*/("""" target="_blank">"""),_display_(/*158.111*/vLc/*158.114*/.col_url),format.raw/*158.122*/("""</a></p>
                """)))}/*159.18*/else/*159.22*/{_display_(Seq[Any](format.raw/*159.23*/("""
                    """),format.raw/*160.21*/("""<p><b class="report">Manufacturer url: </b>NA</p>
                """)))}),format.raw/*161.18*/("""



                """),format.raw/*165.17*/("""<br/><h5>3. Method run</h5><br/>

                """),_display_(/*167.18*/if(vLc.temperature !=null)/*167.44*/{_display_(Seq[Any](format.raw/*167.45*/("""
                    """),format.raw/*168.21*/("""<p><b class="report">Temperature: </b>"""),_display_(/*168.60*/vLc/*168.63*/.temperature),format.raw/*168.75*/("""</p>
                """)))}/*169.18*/else/*169.22*/{_display_(Seq[Any](format.raw/*169.23*/("""
                    """),format.raw/*170.21*/("""<p><b class="report">Temperature: </b>NA</p>
                """)))}),format.raw/*171.18*/("""
                """),_display_(/*172.18*/if(vLc.solvent_a !=null)/*172.42*/{_display_(Seq[Any](format.raw/*172.43*/("""
                    """),format.raw/*173.21*/("""<p><b class="report">Solvent a:</b> """),_display_(/*173.58*/vLc/*173.61*/.solvent_a),format.raw/*173.71*/("""</p>
                """)))}/*174.18*/else/*174.22*/{_display_(Seq[Any](format.raw/*174.23*/("""
                    """),format.raw/*175.21*/("""<p><b class="report">Solvent a:</b> NA</p>
                """)))}),format.raw/*176.18*/("""
                """),_display_(/*177.18*/if(vLc.solvent_b !=null)/*177.42*/{_display_(Seq[Any](format.raw/*177.43*/("""
                    """),format.raw/*178.21*/("""<p><b class="report">Solvent b: </b>"""),_display_(/*178.58*/vLc/*178.61*/.solvent_b),format.raw/*178.71*/("""</p>
                """)))}/*179.18*/else/*179.22*/{_display_(Seq[Any](format.raw/*179.23*/("""
                    """),format.raw/*180.21*/("""<p><b class="report">Solvent b: </b>NA</p>
                """)))}),format.raw/*181.18*/("""
                """),_display_(/*182.18*/if(vLc.other_solvent !=null)/*182.46*/{_display_(Seq[Any](format.raw/*182.47*/("""
                    """),format.raw/*183.21*/("""<p><b class="report">Other solvent: </b>"""),_display_(/*183.62*/vLc/*183.65*/.other_solvent),format.raw/*183.79*/("""</p>
                """)))}/*184.18*/else/*184.22*/{_display_(Seq[Any](format.raw/*184.23*/("""
                    """),format.raw/*185.21*/("""<p><b class="report">Other solvent: </b>NA</p>
                """)))}),format.raw/*186.18*/("""
                """),_display_(/*187.18*/if(vLc.flow_rate !=null)/*187.42*/{_display_(Seq[Any](format.raw/*187.43*/("""
                    """),format.raw/*188.21*/("""<p><b class="report">Flow rate: </b>"""),_display_(/*188.58*/vLc/*188.61*/.flow_rate),format.raw/*188.71*/("""</p>
                """)))}/*189.18*/else/*189.22*/{_display_(Seq[Any](format.raw/*189.23*/("""
                    """),format.raw/*190.21*/("""<p><b class="report">Flow rate: </b>NA</p>
                """)))}),format.raw/*191.18*/("""
                """),_display_(/*192.18*/if(vLc.flow_gradient !=null)/*192.46*/{_display_(Seq[Any](format.raw/*192.47*/("""
                    """),format.raw/*193.21*/("""<p><b class="report">Flow gradient: </b>"""),_display_(/*193.62*/vLc/*193.65*/.flow_gradient),format.raw/*193.79*/("""</p>
                """)))}/*194.18*/else/*194.22*/{_display_(Seq[Any](format.raw/*194.23*/("""
                    """),format.raw/*195.21*/("""<p><b class="report">Flow gradient: </b>NA</p>
                """)))}),format.raw/*196.18*/("""
                """),_display_(/*197.18*/if(vLc.run_time !=null)/*197.41*/{_display_(Seq[Any](format.raw/*197.42*/("""
                    """),format.raw/*198.21*/("""<p><b class="report">Run time: </b>"""),_display_(/*198.57*/vLc/*198.60*/.run_time),format.raw/*198.69*/("""</p>
                """)))}/*199.18*/else/*199.22*/{_display_(Seq[Any](format.raw/*199.23*/("""
                    """),format.raw/*200.21*/("""<p><b class="report">Run time: </b>NA</p>
                """)))}),format.raw/*201.18*/("""
                """),_display_(/*202.18*/if(vLc.phase !=null)/*202.38*/{_display_(Seq[Any](format.raw/*202.39*/("""
                    """),format.raw/*203.21*/("""<p><b class="report">Phase: </b>"""),_display_(/*203.54*/vLc/*203.57*/.phase),format.raw/*203.63*/("""</p>
                """)))}/*204.18*/else/*204.22*/{_display_(Seq[Any](format.raw/*204.23*/("""
                    """),format.raw/*205.21*/("""<p><b class="report">Phase: </b>NA</p>
                """)))}),format.raw/*206.18*/("""


                """),format.raw/*209.17*/("""<br/><br/>
                <h3>MS</h3>
                <hr>

                <br/><h5>1. General features</h5><br/>

                """),_display_(/*215.18*/if(vMs.date_completed !=null)/*215.47*/{_display_(Seq[Any](format.raw/*215.48*/("""
                    """),format.raw/*216.21*/("""<p><b class="report">Date stamp: </b> """),_display_(/*216.60*/vMs/*216.63*/.date_completed),format.raw/*216.78*/("""</p>
                """)))}/*217.18*/else/*217.22*/{_display_(Seq[Any](format.raw/*217.23*/("""
                    """),format.raw/*218.21*/("""<p><b class="report">Date stamp: </b>NA</p>
                """)))}),format.raw/*219.18*/("""


                """),_display_(/*222.18*/if(vMs.manufacturer_name !=null)/*222.50*/{_display_(Seq[Any](format.raw/*222.51*/("""
                    """),format.raw/*223.21*/("""<p><b class="report">Instrument: </b> """),_display_(/*223.60*/vMs/*223.63*/.manufacturer_name),format.raw/*223.81*/("""  """),_display_(/*223.84*/vMs/*223.87*/.device_model),format.raw/*223.100*/("""  """),_display_(/*223.103*/vMs/*223.106*/.ionisation_type),format.raw/*223.122*/(""" """),format.raw/*223.123*/("""</p>
                """)))}/*224.18*/else/*224.22*/{_display_(Seq[Any](format.raw/*224.23*/("""
                    """),format.raw/*225.21*/("""<p><b class="report">Instrument: </b> NA</p>
                """)))}),format.raw/*226.18*/("""


                """),_display_(/*229.18*/if(vMs.ms_customizations !=null)/*229.50*/{_display_(Seq[Any](format.raw/*229.51*/("""
                    """),format.raw/*230.21*/("""<p><b class="report"> Customizations from the manufacturer's specification: </b><br/> """),_display_(/*230.108*/vMs/*230.111*/.ms_customizations),format.raw/*230.129*/("""</p>
                """)))}/*231.18*/else/*231.22*/{_display_(Seq[Any](format.raw/*231.23*/("""
                    """),format.raw/*232.21*/("""<p><b class="report"> Customizations from the manufacturer's specification: </b><br/> NA</p>
                """)))}),format.raw/*233.18*/("""

                """),_display_(/*235.18*/if(vMs.ion_mode !=null)/*235.41*/{_display_(Seq[Any](format.raw/*235.42*/("""
                    """),format.raw/*236.21*/("""<p><b class="report"> ion_mode: </b> """),_display_(/*236.59*/vMs/*236.62*/.ion_mode),format.raw/*236.71*/("""</p>
                """)))}/*237.18*/else/*237.22*/{_display_(Seq[Any](format.raw/*237.23*/("""
                    """),format.raw/*238.21*/("""<p><b class="report"> ion_mode: </b>NA</p>
                """)))}),format.raw/*239.18*/("""


                """),format.raw/*242.17*/("""<br/><h5>2. Ion sources</h5><br/>
                <h5>(a) Electrospray ionisation - ESI</h5><br/>

                """),_display_(/*245.18*/if(vLc.phase !=null)/*245.38*/{_display_(Seq[Any](format.raw/*245.39*/("""
                    """),format.raw/*246.21*/("""<p><b class="report">Sprayer type : </b> """),_display_(/*246.63*/vMs/*246.66*/.esi_supply_type),format.raw/*246.82*/("""</p>
                """)))}/*247.18*/else/*247.22*/{_display_(Seq[Any](format.raw/*247.23*/("""
                    """),format.raw/*248.21*/("""<p><b class="report">Sprayer type: </b> NA</p>
                """)))}),format.raw/*249.18*/("""

                """),_display_(/*251.18*/if(vMs.esi_interface_name !=null)/*251.51*/{_display_(Seq[Any](format.raw/*251.52*/("""
                    """),format.raw/*252.21*/("""<p><b class="report"> Interface name: </b> """),_display_(/*252.65*/vMs/*252.68*/.esi_interface_name),format.raw/*252.87*/("""</p>
                """)))}/*253.18*/else/*253.22*/{_display_(Seq[Any](format.raw/*253.23*/("""
                    """),format.raw/*254.21*/("""<p><b class="report"> Interface name: </b>NA</p>
                """)))}),format.raw/*255.18*/("""

                """),_display_(/*257.18*/if(vMs.esi_interface_details !=null)/*257.54*/{_display_(Seq[Any](format.raw/*257.55*/("""
                    """),format.raw/*258.21*/("""<p><b class="report"> Other data for the Interface: </b>"""),_display_(/*258.78*/vMs/*258.81*/.esi_interface_details),format.raw/*258.103*/("""</p>
                """)))}/*259.18*/else/*259.22*/{_display_(Seq[Any](format.raw/*259.23*/("""
                    """),format.raw/*260.21*/("""<p><b class="report"> Other data for the Interface: </b>NA</p>
                """)))}),format.raw/*261.18*/("""

                """),_display_(/*263.18*/if(vMs.esi_sprayer_name !=null)/*263.49*/{_display_(Seq[Any](format.raw/*263.50*/("""
                    """),format.raw/*264.21*/("""<p><b class="report"> Sprayer name: </b> """),_display_(/*264.63*/vMs/*264.66*/.esi_sprayer_name),format.raw/*264.83*/("""</p>
                """)))}/*265.18*/else/*265.22*/{_display_(Seq[Any](format.raw/*265.23*/("""
                    """),format.raw/*266.21*/("""<p><b class="report"> Sprayer name: </b>NA</p>
                """)))}),format.raw/*267.18*/("""

                """),_display_(/*269.18*/if(vMs.esi_sprayer_details !=null)/*269.52*/{_display_(Seq[Any](format.raw/*269.53*/("""
                    """),format.raw/*270.21*/("""<p><b class="report"> Other data for the sprayer: </b> """),_display_(/*270.77*/vMs/*270.80*/.esi_sprayer_details),format.raw/*270.100*/("""</p>
                """)))}/*271.18*/else/*271.22*/{_display_(Seq[Any](format.raw/*271.23*/("""
                    """),format.raw/*272.21*/("""<p><b class="report"> Other data for the sprayer: </b>NA</p>
                """)))}),format.raw/*273.18*/("""

                """),_display_(/*275.18*/if(vMs.esi_voltages !=null)/*275.45*/{_display_(Seq[Any](format.raw/*275.46*/("""
                    """),format.raw/*276.21*/("""<p><b class="report"> Relevant voltages: </b> """),_display_(/*276.68*/vMs/*276.71*/.esi_voltages),format.raw/*276.84*/("""</p>
                """)))}/*277.18*/else/*277.22*/{_display_(Seq[Any](format.raw/*277.23*/("""
                    """),format.raw/*278.21*/("""<p><b class="report"> Relevant voltages: </b>NA</p>
                """)))}),format.raw/*279.18*/("""
                """),_display_(/*280.18*/if(vMs.esi_prompt !=null)/*280.43*/{_display_(Seq[Any](format.raw/*280.44*/("""
                    """),format.raw/*281.21*/("""<p><b class="report"> Degree of prompt fragmentation evaluated: </b> """),_display_(/*281.91*/vMs/*281.94*/.esi_prompt),format.raw/*281.105*/("""</p>
                """)))}/*282.18*/else/*282.22*/{_display_(Seq[Any](format.raw/*282.23*/("""
                    """),format.raw/*283.21*/("""<p><b class="report"> Degree of prompt fragmentation evaluated: </b>NA</p>
                """)))}),format.raw/*284.18*/("""

                """),_display_(/*286.18*/if(vMs.esi_in_source !=null)/*286.46*/{_display_(Seq[Any](format.raw/*286.47*/("""
                    """),format.raw/*287.21*/("""<p><b class="report"> In-source dissociation performed: </b> """),_display_(/*287.83*/vMs/*287.86*/.esi_in_source),format.raw/*287.100*/("""</p>
                """)))}/*288.18*/else/*288.22*/{_display_(Seq[Any](format.raw/*288.23*/("""
                    """),format.raw/*289.21*/("""<p><b class="report"> In-source dissociation performed: </b>NA</p>
                """)))}),format.raw/*290.18*/("""

                """),_display_(/*292.18*/if(vMs.esi_other_param !=null)/*292.48*/{_display_(Seq[Any](format.raw/*292.49*/("""
                    """),format.raw/*293.21*/("""<p><b class="report"> Other parameters if discriminant for the experiment: </b> """),_display_(/*293.102*/vMs/*293.105*/.esi_other_param),format.raw/*293.121*/("""</p>
                """)))}/*294.18*/else/*294.22*/{_display_(Seq[Any](format.raw/*294.23*/("""
                    """),format.raw/*295.21*/("""<p><b class="report"> Other parameters if discriminant for the experiment: </b>NA</p>
                """)))}),format.raw/*296.18*/("""



                """),format.raw/*300.17*/("""<br/><h5>(b) MALDI</h5><br/>

                """),_display_(/*302.18*/if(vMs.plate_composition !=null)/*302.50*/{_display_(Seq[Any](format.raw/*302.51*/("""
                """),format.raw/*303.17*/("""<p><b class="report"> Plate composition (or type): </b> """),_display_(/*303.74*/vMs/*303.77*/.plate_composition),format.raw/*303.95*/("""</p>
                """)))}/*304.18*/else/*304.22*/{_display_(Seq[Any](format.raw/*304.23*/("""
                """),format.raw/*305.17*/("""<p><b class="report"> Plate composition (or type): </b>NA</p>
                """)))}),format.raw/*306.18*/("""

                """),_display_(/*308.18*/if(vMs.matrix_composition !=null)/*308.51*/{_display_(Seq[Any](format.raw/*308.52*/("""
                """),format.raw/*309.17*/("""<p><b class="report"> Matrix composition: </b> """),_display_(/*309.65*/vMs/*309.68*/.matrix_composition),format.raw/*309.87*/("""</p>
                """)))}/*310.18*/else/*310.22*/{_display_(Seq[Any](format.raw/*310.23*/("""
                """),format.raw/*311.17*/("""<p><b class="report"> Matrix composition: </b>NA</p>
                """)))}),format.raw/*312.18*/("""

                """),_display_(/*314.18*/if(vMs.deposition_technique !=null)/*314.53*/{_display_(Seq[Any](format.raw/*314.54*/("""
                """),format.raw/*315.17*/("""<p><b class="report"> Deposition technique: </b> """),_display_(/*315.67*/vMs/*315.70*/.deposition_technique),format.raw/*315.91*/("""</p>
                """)))}/*316.18*/else/*316.22*/{_display_(Seq[Any](format.raw/*316.23*/("""
                """),format.raw/*317.17*/("""<p><b class="report"> Deposition technique: </b>NA</p>
                """)))}),format.raw/*318.18*/("""

                """),_display_(/*320.18*/if(vMs.maldi_voltages !=null)/*320.47*/{_display_(Seq[Any](format.raw/*320.48*/("""
                """),format.raw/*321.17*/("""<p><b class="report"> Relevant voltages: </b> """),_display_(/*321.64*/vMs/*321.67*/.maldi_voltages),format.raw/*321.82*/("""</p>
                """)))}/*322.18*/else/*322.22*/{_display_(Seq[Any](format.raw/*322.23*/("""
                """),format.raw/*323.17*/("""<p><b class="report"> Relevant voltages: </b> NA</p>
                """)))}),format.raw/*324.18*/("""

                """),_display_(/*326.18*/if(vMs.maldi_prompt !=null)/*326.45*/{_display_(Seq[Any](format.raw/*326.46*/("""
                """),format.raw/*327.17*/("""<p><b class="report"> Degree of prompt fragmentation evaluated: </b> """),_display_(/*327.87*/vMs/*327.90*/.maldi_prompt),format.raw/*327.103*/("""</p>
                """)))}/*328.18*/else/*328.22*/{_display_(Seq[Any](format.raw/*328.23*/("""
                """),format.raw/*329.17*/("""<p><b class="report"> Degree of prompt fragmentation evaluated: </b>NA</p>
                """)))}),format.raw/*330.18*/("""

                """),_display_(/*332.18*/if(vMs.psd_lid_summary !=null)/*332.48*/{_display_(Seq[Any](format.raw/*332.49*/("""
                """),format.raw/*333.17*/("""<p><b class="report"> PSD (or LID/ISD) summary: </b> """),_display_(/*333.71*/vMs/*333.74*/.psd_lid_summary),format.raw/*333.90*/("""</p>
                """)))}/*334.18*/else/*334.22*/{_display_(Seq[Any](format.raw/*334.23*/("""
                """),format.raw/*335.17*/("""<p><b class="report"> PSD (or LID/ISD) summary: </b> NA</p>
                """)))}),format.raw/*336.18*/("""
                """),_display_(/*337.18*/if(vMs.delayed_extraction !=null)/*337.51*/{_display_(Seq[Any](format.raw/*337.52*/("""
                """),format.raw/*338.17*/("""<p><b class="report"> Operation with or without delayed extraction: </b> """),_display_(/*338.91*/vMs/*338.94*/.delayed_extraction),format.raw/*338.113*/("""</p>
                """)))}/*339.18*/else/*339.22*/{_display_(Seq[Any](format.raw/*339.23*/("""
                """),format.raw/*340.17*/("""<p><b class="report"> Operation with or without delayed extraction: </b>NA</p>
                """)))}),format.raw/*341.18*/("""

                """),_display_(/*343.18*/if(vMs.laser_type !=null)/*343.43*/{_display_(Seq[Any](format.raw/*343.44*/("""
                """),format.raw/*344.17*/("""<p><b class="report"> Laser type: </b> """),_display_(/*344.57*/vMs/*344.60*/.laser_type),format.raw/*344.71*/("""</p>
                """)))}/*345.18*/else/*345.22*/{_display_(Seq[Any](format.raw/*345.23*/("""
                """),format.raw/*346.17*/("""<p><b class="report"> Laser type: </b>NA</p>
                """)))}),format.raw/*347.18*/("""

                """),_display_(/*349.18*/if(vMs.laser_details !=null)/*349.46*/{_display_(Seq[Any](format.raw/*349.47*/("""
                """),format.raw/*350.17*/("""<p><b class="report"> Other laser related parameters: </b> """),_display_(/*350.77*/vMs/*350.80*/.laser_details),format.raw/*350.94*/("""</p>
                """)))}/*351.18*/else/*351.22*/{_display_(Seq[Any](format.raw/*351.23*/("""
                """),format.raw/*352.17*/("""<p><b class="report"> Other laser related parameters: </b>NA</p>
                """)))}),format.raw/*353.18*/("""



                """),_display_(/*357.18*/if(vMs.url !=null)/*357.36*/{_display_(Seq[Any](format.raw/*357.37*/("""
                """),format.raw/*358.17*/("""<p><b class="report"> url: </b> """),_display_(/*358.50*/vMs/*358.53*/.url),format.raw/*358.57*/("""</p>
                """)))}/*359.18*/else/*359.22*/{_display_(Seq[Any](format.raw/*359.23*/("""
                """),format.raw/*360.17*/("""<p><b class="report"> url: </b> NA</p>
                """)))}),format.raw/*361.18*/("""


                """),format.raw/*364.17*/("""<br/><h5>3. Ion transfer optics</h5><br/>

                """),_display_(/*366.18*/if(vMs.hardware_options !=null)/*366.49*/{_display_(Seq[Any](format.raw/*366.50*/("""
                """),format.raw/*367.17*/("""<p><b class="report">Hardware options: </b> """),_display_(/*367.62*/vMs/*367.65*/.hardware_options),format.raw/*367.82*/("""</p>
                """)))}/*368.18*/else/*368.22*/{_display_(Seq[Any](format.raw/*368.23*/("""
                """),format.raw/*369.17*/("""<p><b class="report">Hardware options: </b>NA</p>
                """)))}),format.raw/*370.18*/("""


                """),format.raw/*373.17*/("""<br/><h5>3. Post-source componentry</h5><br/>

                <h5>Collision-Induced Dissociation - CID</h5><br/>


                """),_display_(/*378.18*/if(vMs.cid_gas_composition !=null)/*378.52*/{_display_(Seq[Any](format.raw/*378.53*/("""
                    """),format.raw/*379.21*/("""<p><b class="report">CID. Gas composition: </b> """),_display_(/*379.70*/vMs/*379.73*/.cid_gas_composition),format.raw/*379.93*/("""</p>
                """)))}/*380.18*/else/*380.22*/{_display_(Seq[Any](format.raw/*380.23*/("""
                    """),format.raw/*381.21*/("""<p><b class="report">CID. Gas composition: </b>NA</p>
                """)))}),format.raw/*382.18*/("""


                """),_display_(/*385.18*/if(vMs.cid_gas_pressure !=null)/*385.49*/{_display_(Seq[Any](format.raw/*385.50*/("""
                    """),format.raw/*386.21*/("""<p><b class="report">CID. Gas pressure: </b> """),_display_(/*386.67*/vMs/*386.70*/.cid_gas_pressure),format.raw/*386.87*/("""</p>
                """)))}/*387.18*/else/*387.22*/{_display_(Seq[Any](format.raw/*387.23*/("""
                    """),format.raw/*388.21*/("""<p><b class="report">CID. Gas pressure: </b>NA</p>
                """)))}),format.raw/*389.18*/("""


                """),_display_(/*392.18*/if(vMs.cid_collision_energy !=null)/*392.53*/{_display_(Seq[Any](format.raw/*392.54*/("""
                    """),format.raw/*393.21*/("""<p><b class="report">CID. Collision energy: </b> """),_display_(/*393.71*/vMs/*393.74*/.cid_collision_energy),format.raw/*393.95*/("""</p>
                """)))}/*394.18*/else/*394.22*/{_display_(Seq[Any](format.raw/*394.23*/("""
                    """),format.raw/*395.21*/("""<p><b class="report">CID. Collision energy: </b>NA</p>
                """)))}),format.raw/*396.18*/("""



                """),format.raw/*400.17*/("""<h5>Electron Transfer Dissociation - ETD</h5><br/>

                """),_display_(/*402.18*/if(vMs.etd_reagent_gas !=null)/*402.48*/{_display_(Seq[Any](format.raw/*402.49*/("""
                    """),format.raw/*403.21*/("""<p><b class="report">ETD. Reagent gas: </b> """),_display_(/*403.66*/vMs/*403.69*/.etd_reagent_gas),format.raw/*403.85*/("""</p>
                """)))}/*404.18*/else/*404.22*/{_display_(Seq[Any](format.raw/*404.23*/("""
                    """),format.raw/*405.21*/("""<p><b class="report">ETD. Reagent gas: </b>NA</p>
                """)))}),format.raw/*406.18*/("""

                """),_display_(/*408.18*/if(vMs.etd_pressure !=null)/*408.45*/{_display_(Seq[Any](format.raw/*408.46*/("""
                """),format.raw/*409.17*/("""<p><b class="report">ETD. Pressure: </b> """),_display_(/*409.59*/vMs/*409.62*/.etd_pressure),format.raw/*409.75*/("""</p>
                """)))}/*410.18*/else/*410.22*/{_display_(Seq[Any](format.raw/*410.23*/("""
                """),format.raw/*411.17*/("""<p><b class="report">ETD. Pressure: </b>NA</p>
                """)))}),format.raw/*412.18*/("""

                """),_display_(/*414.18*/if(vMs.etd_reaction_time !=null)/*414.50*/{_display_(Seq[Any](format.raw/*414.51*/("""
                """),format.raw/*415.17*/("""<p><b class="report">ETD. Reaction time: </b> """),_display_(/*415.64*/vMs/*415.67*/.etd_reaction_time),format.raw/*415.85*/("""</p>
                """)))}/*416.18*/else/*416.22*/{_display_(Seq[Any](format.raw/*416.23*/("""
                """),format.raw/*417.17*/("""<p><b class="report">ETD. Reaction time: </b>NA</p>
                """)))}),format.raw/*418.18*/("""

                """),_display_(/*420.18*/if(vMs.etd_reagent_atoms !=null)/*420.50*/{_display_(Seq[Any](format.raw/*420.51*/("""
                """),format.raw/*421.17*/("""<p><b class="report">ETD. Reagent atoms: </b> """),_display_(/*421.64*/vMs/*421.67*/.etd_reagent_atoms),format.raw/*421.85*/("""</p>
                """)))}/*422.18*/else/*422.22*/{_display_(Seq[Any](format.raw/*422.23*/("""
                """),format.raw/*423.17*/("""<p><b class="report">ETD. Reagent atoms: </b>NA</p>
                """)))}),format.raw/*424.18*/("""

                """),format.raw/*426.17*/("""<h5>Electron Capture Dissociation - ECD</h5><br/>


                """),_display_(/*429.18*/if(vMs.ecd_emitter_type !=null)/*429.49*/{_display_(Seq[Any](format.raw/*429.50*/("""
                """),format.raw/*430.17*/("""<p><b class="report">ECD. Emitter type: </b> """),_display_(/*430.63*/vMs/*430.66*/.ecd_emitter_type),format.raw/*430.83*/("""</p>
                """)))}/*431.18*/else/*431.22*/{_display_(Seq[Any](format.raw/*431.23*/("""
                """),format.raw/*432.17*/("""<p><b class="report">ECD. Emitter type: </b>NA</p>
                """)))}),format.raw/*433.18*/("""


                """),_display_(/*436.18*/if(vMs.ecd_voltage !=null)/*436.44*/{_display_(Seq[Any](format.raw/*436.45*/("""
                """),format.raw/*437.17*/("""<p><b class="report">ECD. Voltage: </b> """),_display_(/*437.58*/vMs/*437.61*/.ecd_voltage),format.raw/*437.73*/("""</p>
                """)))}/*438.18*/else/*438.22*/{_display_(Seq[Any](format.raw/*438.23*/("""
                """),format.raw/*439.17*/("""<p><b class="report">ECD. Voltage: </b>NA</p>
                """)))}),format.raw/*440.18*/("""


                """),_display_(/*443.18*/if(vMs.ecd_current !=null)/*443.44*/{_display_(Seq[Any](format.raw/*443.45*/("""
                """),format.raw/*444.17*/("""<p><b class="report">ECD. Current: </b> """),_display_(/*444.58*/vMs/*444.61*/.ecd_current),format.raw/*444.73*/("""</p>
                """)))}/*445.18*/else/*445.22*/{_display_(Seq[Any](format.raw/*445.23*/("""
                """),format.raw/*446.17*/("""<p><b class="report">ECD. Current: </b>NA</p>
                """)))}),format.raw/*447.18*/("""


                """),format.raw/*450.17*/("""<h5>TOF drift tube</h5><br/>

                """),_display_(/*452.18*/if(vMs.reflectron_status !=null)/*452.50*/{_display_(Seq[Any](format.raw/*452.51*/("""
                """),format.raw/*453.17*/("""<p><b class="report">TOF. Reflectron status: </b> """),_display_(/*453.68*/vMs/*453.71*/.reflectron_status),format.raw/*453.89*/("""</p>
                """)))}/*454.18*/else/*454.22*/{_display_(Seq[Any](format.raw/*454.23*/("""
                """),format.raw/*455.17*/("""<p><b class="report">TOF. Reflectron status: </b>NA</p>
                """)))}),format.raw/*456.18*/("""

                """),format.raw/*458.17*/("""<h5>Ion Trap</h5><br/>

                """),_display_(/*460.18*/if(vMs.ms_achieved !=null)/*460.44*/{_display_(Seq[Any](format.raw/*460.45*/("""
                """),format.raw/*461.17*/("""<p><b class="report"> Ion trap. Final MS stage achieved: </b> """),_display_(/*461.80*/vMs/*461.83*/.ms_achieved),format.raw/*461.95*/("""</p>
                """)))}/*462.18*/else/*462.22*/{_display_(Seq[Any](format.raw/*462.23*/("""
                """),format.raw/*463.17*/("""<p><b class="report"> Ion trap. Final MS stage achieved: </b>NA</p>
                """)))}),format.raw/*464.18*/("""


                """),format.raw/*467.17*/("""<h5>Ion mobility</h5><br/>

                """),_display_(/*469.18*/if(vMs.gas !=null)/*469.36*/{_display_(Seq[Any](format.raw/*469.37*/("""
                """),format.raw/*470.17*/("""<p><b class="report"> Ion mobility. Gas type : </b> """),_display_(/*470.70*/vMs/*470.73*/.gas),format.raw/*470.77*/("""</p>
                """)))}/*471.18*/else/*471.22*/{_display_(Seq[Any](format.raw/*471.23*/("""
                """),format.raw/*472.17*/("""<p><b class="report"> Ion mobility. Gas type : </b> NA</p>
                """)))}),format.raw/*473.18*/("""

                """),_display_(/*475.18*/if(vMs.pressure !=null)/*475.41*/{_display_(Seq[Any](format.raw/*475.42*/("""
                """),format.raw/*476.17*/("""<p><b class="report"> Ion mobility. Pressure: </b> """),_display_(/*476.69*/vMs/*476.72*/.pressure),format.raw/*476.81*/("""</p>
                """)))}/*477.18*/else/*477.22*/{_display_(Seq[Any](format.raw/*477.23*/("""
                """),format.raw/*478.17*/("""<p><b class="report"> Ion mobility. Pressure: </b> NA</p>
                """)))}),format.raw/*479.18*/("""

                """),_display_(/*481.18*/if(vMs.instrument_parameters !=null)/*481.54*/{_display_(Seq[Any](format.raw/*481.55*/("""
                """),format.raw/*482.17*/("""<p><b class="report"> Ion mobility. Instrument parameters: </b> """),_display_(/*482.82*/vMs/*482.85*/.instrument_parameters),format.raw/*482.107*/("""</p>
                """)))}/*483.18*/else/*483.22*/{_display_(Seq[Any](format.raw/*483.23*/("""
                """),format.raw/*484.17*/("""<p><b class="report"> Ion mobility. Instrument parameters: </b>NA</p>
                """)))}),format.raw/*485.18*/("""


                """),format.raw/*488.17*/("""<h5>FT-ICR</h5><br/>

                """),_display_(/*490.18*/if(vMs.peak_selection !=null)/*490.47*/{_display_(Seq[Any](format.raw/*490.48*/("""
                    """),format.raw/*491.21*/("""<p><b class="report"> Peak selection: </b> """),_display_(/*491.65*/vMs/*491.68*/.peak_selection),format.raw/*491.83*/("""</p>
                """)))}/*492.18*/else/*492.22*/{_display_(Seq[Any](format.raw/*492.23*/("""
                    """),format.raw/*493.21*/("""<p><b class="report"> Peak selection: </b> NA</p>
                """)))}),format.raw/*494.18*/("""

                """),_display_(/*496.18*/if(vMs.pulse !=null)/*496.38*/{_display_(Seq[Any](format.raw/*496.39*/("""
                """),format.raw/*497.17*/("""<p><b class="report"> FT-ICR. Pulse: </b> """),_display_(/*497.60*/vMs/*497.63*/.pulse),format.raw/*497.69*/("""</p>
                """)))}/*498.18*/else/*498.22*/{_display_(Seq[Any](format.raw/*498.23*/("""
                """),format.raw/*499.17*/("""<p><b class="report"> FT-ICR. Pulse: </b> NA</p>
                """)))}),format.raw/*500.18*/("""

                """),_display_(/*502.18*/if(vMs.width !=null)/*502.38*/{_display_(Seq[Any](format.raw/*502.39*/("""
                """),format.raw/*503.17*/("""<p><b class="report"> FT-ICR. Width: </b> """),_display_(/*503.60*/vMs/*503.63*/.width),format.raw/*503.69*/("""</p>
                """)))}/*504.18*/else/*504.22*/{_display_(Seq[Any](format.raw/*504.23*/("""
                """),format.raw/*505.17*/("""<p><b class="report"> FT-ICR. Width: </b> NA</p>
                """)))}),format.raw/*506.18*/("""

                """),_display_(/*508.18*/if(vMs.decay_time !=null)/*508.43*/{_display_(Seq[Any](format.raw/*508.44*/("""
                """),format.raw/*509.17*/("""<p><b class="report"> FT-ICR. IR: Decay time: </b> """),_display_(/*509.69*/vMs/*509.72*/.decay_time),format.raw/*509.83*/("""</p>
                """)))}/*510.18*/else/*510.22*/{_display_(Seq[Any](format.raw/*510.23*/("""
                """),format.raw/*511.17*/("""<p><b class="report"> FT-ICR. IR: Decay time: </b>NA</p>
                """)))}),format.raw/*512.18*/("""

                """),_display_(/*514.18*/if(vMs.ir !=null)/*514.35*/{_display_(Seq[Any](format.raw/*514.36*/("""
                """),format.raw/*515.17*/("""<p><b class="report"> FT-ICR. IR: </b> """),_display_(/*515.57*/vMs/*515.60*/.ir),format.raw/*515.63*/("""</p>
                """)))}/*516.18*/else/*516.22*/{_display_(Seq[Any](format.raw/*516.23*/("""
                """),format.raw/*517.17*/("""<p><b class="report"> FT-ICR. IR: </b>NA</p>
                """)))}),format.raw/*518.18*/("""

                """),_display_(/*520.18*/if(vMs.ft_other !=null)/*520.41*/{_display_(Seq[Any](format.raw/*520.42*/("""
                """),format.raw/*521.17*/("""<p><b class="report"> FT-ICR. Other parameters: </b> """),_display_(/*521.71*/vMs/*521.74*/.ft_other),format.raw/*521.83*/("""</p>
                """)))}/*522.18*/else/*522.22*/{_display_(Seq[Any](format.raw/*522.23*/("""
                """),format.raw/*523.17*/("""<p><b class="report"> FT-ICR. Other parameters: </b>NA</p>
                """)))}),format.raw/*524.18*/("""

                """),format.raw/*526.17*/("""<h5>Detectors</h5><br/>

                """),_display_(/*528.18*/if(vMs.detector_type !=null)/*528.46*/{_display_(Seq[Any](format.raw/*528.47*/("""
                """),format.raw/*529.17*/("""<p><b class="report"> Detector type: </b> """),_display_(/*529.60*/vMs/*529.63*/.detector_type),format.raw/*529.77*/("""</p>
                """)))}/*530.18*/else/*530.22*/{_display_(Seq[Any](format.raw/*530.23*/("""
                """),format.raw/*531.17*/("""<p><b class="report"> Detector type: </b>NA</p>
                """)))}),format.raw/*532.18*/("""




            """),format.raw/*537.13*/("""</div>
            <div class="span4 sidebar supporters">
                """),_display_(/*539.18*/views/*539.23*/.html.mirageReport.reportSearchable(vSample, vLc, vMs, vSoft)),format.raw/*539.84*/("""
            """),format.raw/*540.13*/("""</div>

            <div class="searchagain">
            <a href=""""),_display_(/*543.23*/routes/*543.29*/.HomeController.listReferences()),format.raw/*543.61*/("""" class='btn btn-primary pull-right'>View references</a>
            </div>
        </div>
    </div>
</div>

""")))}),format.raw/*549.2*/("""

"""))
      }
    }
  }

  def render(vSample:List[DBviews.vMirageSample],journalResp:DBviews.vJournalResponsible,vLc:DBviews.vMirageLc,vMs:DBviews.vMirageMs,vSoft:List[DBviews.vMirageSoftware],userProvider:UserProvider): play.twirl.api.HtmlFormat.Appendable = apply(vSample,journalResp,vLc,vMs,vSoft,userProvider)

  def f:((List[DBviews.vMirageSample],DBviews.vJournalResponsible,DBviews.vMirageLc,DBviews.vMirageMs,List[DBviews.vMirageSoftware],UserProvider) => play.twirl.api.HtmlFormat.Appendable) = (vSample,journalResp,vLc,vMs,vSoft,userProvider) => apply(vSample,journalResp,vLc,vMs,vSoft,userProvider)

  def ref: this.type = this

}


}
}

/**/
object report extends report_Scope0.report_Scope1.report
              /*
                  -- GENERATED --
                  DATE: Wed Jun 19 09:50:50 UTC 2019
                  SOURCE: /app/app/views/mirageReport/report.scala.html
                  HASH: b0365afbec23104b1c097e7f77322747b01c11e8
                  MATRIX: 945->30|1234->223|1264->396|1292->399|1335->434|1373->435|1400->436|1789->798|1809->809|1836->815|2026->978|2046->989|2084->1006|2113->1007|2143->1010|2163->1021|2196->1033|2225->1034|2255->1037|2275->1048|2317->1068|2363->1086|2483->1179|2517->1197|2556->1198|2605->1219|2683->1270|2718->1296|2757->1297|2810->1322|2889->1374|2900->1376|2934->1389|2979->1415|2992->1419|3031->1420|3084->1445|3194->1524|3243->1546|3281->1575|3320->1576|3373->1601|3495->1695|3507->1697|3544->1712|3589->1738|3602->1742|3641->1743|3694->1768|3846->1889|3895->1911|3936->1943|3975->1944|4028->1969|4123->2037|4134->2039|4174->2057|4219->2083|4232->2087|4271->2088|4324->2113|4450->2208|4499->2230|4539->2261|4578->2262|4631->2287|4754->2382|4766->2384|4805->2401|4850->2427|4863->2431|4902->2432|4955->2457|5108->2579|5157->2601|5199->2634|5238->2635|5291->2660|5392->2734|5404->2736|5446->2756|5491->2782|5504->2786|5543->2787|5596->2812|5727->2912|5778->2935|5867->2997|5902->3023|5941->3024|5994->3049|6072->3100|6083->3102|6117->3115|6162->3141|6175->3145|6214->3146|6267->3171|6376->3249|6427->3272|6513->3331|6547->3356|6586->3357|6639->3382|6710->3426|6721->3428|6754->3440|6799->3466|6812->3470|6851->3471|6904->3496|7006->3567|7055->3585|7101->3603|7262->3737|7295->3761|7334->3762|7383->3783|7448->3821|7460->3824|7491->3834|7520->3836|7532->3839|7564->3850|7605->3872|7618->3876|7657->3877|7706->3898|7798->3959|7844->3978|7876->4001|7915->4002|7964->4023|8051->4083|8063->4086|8093->4095|8140->4114|8153->4117|8184->4126|8229->4152|8242->4156|8281->4157|8330->4178|8428->4245|8474->4264|8511->4292|8550->4293|8599->4314|8662->4350|8674->4353|8709->4367|8750->4389|8763->4393|8802->4394|8852->4415|8943->4474|8990->4493|9032->4525|9072->4526|9122->4547|9195->4592|9208->4595|9248->4613|9290->4635|9304->4639|9344->4640|9394->4661|9494->4729|9543->4749|9618->4796|9651->4819|9691->4820|9741->4841|9810->4882|9823->4885|9854->4894|9896->4916|9910->4920|9950->4921|10000->4942|10096->5006|10143->5025|10180->5052|10220->5053|10270->5074|10332->5108|10345->5111|10380->5124|10422->5146|10436->5150|10476->5151|10526->5172|10615->5229|10662->5248|10706->5282|10746->5283|10796->5304|10874->5354|10887->5357|10929->5377|10971->5399|10985->5403|11025->5404|11075->5425|11181->5499|11228->5518|11269->5549|11309->5550|11359->5571|11431->5615|11444->5618|11483->5635|11525->5657|11539->5661|11579->5662|11629->5683|11729->5751|11777->5771|11819->5803|11859->5804|11909->5825|11981->5869|11994->5872|12034->5890|12076->5912|12090->5916|12130->5917|12180->5938|12279->6005|12327->6025|12370->6058|12410->6059|12460->6080|12529->6121|12542->6124|12583->6143|12625->6165|12639->6169|12679->6170|12729->6191|12826->6256|12873->6275|12911->6303|12951->6304|13001->6325|13070->6366|13083->6369|13119->6383|13161->6405|13175->6409|13215->6410|13265->6431|13362->6496|13409->6515|13441->6537|13481->6538|13531->6559|13619->6619|13632->6622|13662->6630|13710->6649|13724->6652|13755->6660|13801->6686|13815->6690|13855->6691|13905->6712|14004->6779|14053->6799|14132->6850|14168->6876|14208->6877|14258->6898|14325->6937|14338->6940|14372->6952|14414->6974|14428->6978|14468->6979|14518->7000|14612->7062|14658->7080|14692->7104|14732->7105|14782->7126|14847->7163|14860->7166|14892->7176|14934->7198|14948->7202|14988->7203|15038->7224|15130->7284|15176->7302|15210->7326|15250->7327|15300->7348|15365->7385|15378->7388|15410->7398|15452->7420|15466->7424|15506->7425|15556->7446|15648->7506|15694->7524|15732->7552|15772->7553|15822->7574|15891->7615|15904->7618|15940->7632|15982->7654|15996->7658|16036->7659|16086->7680|16182->7744|16228->7762|16262->7786|16302->7787|16352->7808|16417->7845|16430->7848|16462->7858|16504->7880|16518->7884|16558->7885|16608->7906|16700->7966|16746->7984|16784->8012|16824->8013|16874->8034|16943->8075|16956->8078|16992->8092|17034->8114|17048->8118|17088->8119|17138->8140|17234->8204|17280->8222|17313->8245|17353->8246|17403->8267|17467->8303|17480->8306|17511->8315|17553->8337|17567->8341|17607->8342|17657->8363|17748->8422|17794->8440|17824->8460|17864->8461|17914->8482|17975->8515|17988->8518|18016->8524|18058->8546|18072->8550|18112->8551|18162->8572|18250->8628|18298->8647|18460->8781|18499->8810|18539->8811|18589->8832|18656->8871|18669->8874|18706->8889|18748->8911|18762->8915|18802->8916|18852->8937|18945->8998|18993->9018|19035->9050|19075->9051|19125->9072|19192->9111|19205->9114|19245->9132|19276->9135|19289->9138|19325->9151|19357->9154|19371->9157|19410->9173|19441->9174|19483->9196|19497->9200|19537->9201|19587->9222|19681->9284|19729->9304|19771->9336|19811->9337|19861->9358|19977->9445|19991->9448|20032->9466|20074->9488|20088->9492|20128->9493|20178->9514|20320->9624|20367->9643|20400->9666|20440->9667|20490->9688|20556->9726|20569->9729|20600->9738|20642->9760|20656->9764|20696->9765|20746->9786|20838->9846|20886->9865|21030->9981|21060->10001|21100->10002|21150->10023|21220->10065|21233->10068|21271->10084|21313->10106|21327->10110|21367->10111|21417->10132|21513->10196|21560->10215|21603->10248|21643->10249|21693->10270|21765->10314|21778->10317|21819->10336|21861->10358|21875->10362|21915->10363|21965->10384|22063->10450|22110->10469|22156->10505|22196->10506|22246->10527|22331->10584|22344->10587|22389->10609|22431->10631|22445->10635|22485->10636|22535->10657|22647->10737|22694->10756|22735->10787|22775->10788|22825->10809|22895->10851|22908->10854|22947->10871|22989->10893|23003->10897|23043->10898|23093->10919|23189->10983|23236->11002|23280->11036|23320->11037|23370->11058|23454->11114|23467->11117|23510->11137|23552->11159|23566->11163|23606->11164|23656->11185|23766->11263|23813->11282|23850->11309|23890->11310|23940->11331|24015->11378|24028->11381|24063->11394|24105->11416|24119->11420|24159->11421|24209->11442|24310->11511|24356->11529|24391->11554|24431->11555|24481->11576|24579->11646|24592->11649|24626->11660|24668->11682|24682->11686|24722->11687|24772->11708|24896->11800|24943->11819|24981->11847|25021->11848|25071->11869|25161->11931|25174->11934|25211->11948|25253->11970|25267->11974|25307->11975|25357->11996|25473->12080|25520->12099|25560->12129|25600->12130|25650->12151|25760->12232|25774->12235|25813->12251|25855->12273|25869->12277|25909->12278|25959->12299|26094->12402|26143->12422|26218->12469|26260->12501|26300->12502|26346->12519|26431->12576|26444->12579|26484->12597|26526->12619|26540->12623|26580->12624|26626->12641|26737->12720|26784->12739|26827->12772|26867->12773|26913->12790|26989->12838|27002->12841|27043->12860|27085->12882|27099->12886|27139->12887|27185->12904|27287->12974|27334->12993|27379->13028|27419->13029|27465->13046|27543->13096|27556->13099|27599->13120|27641->13142|27655->13146|27695->13147|27741->13164|27845->13236|27892->13255|27931->13284|27971->13285|28017->13302|28092->13349|28105->13352|28142->13367|28184->13389|28198->13393|28238->13394|28284->13411|28386->13481|28433->13500|28470->13527|28510->13528|28556->13545|28654->13615|28667->13618|28703->13631|28745->13653|28759->13657|28799->13658|28845->13675|28969->13767|29016->13786|29056->13816|29096->13817|29142->13834|29224->13888|29237->13891|29275->13907|29317->13929|29331->13933|29371->13934|29417->13951|29526->14028|29572->14046|29615->14079|29655->14080|29701->14097|29803->14171|29816->14174|29858->14193|29900->14215|29914->14219|29954->14220|30000->14237|30128->14333|30175->14352|30210->14377|30250->14378|30296->14395|30364->14435|30377->14438|30410->14449|30452->14471|30466->14475|30506->14476|30552->14493|30646->14555|30693->14574|30731->14602|30771->14603|30817->14620|30905->14680|30918->14683|30954->14697|30996->14719|31010->14723|31050->14724|31096->14741|31210->14823|31259->14844|31287->14862|31327->14863|31373->14880|31434->14913|31447->14916|31473->14920|31515->14942|31529->14946|31569->14947|31615->14964|31703->15020|31751->15039|31839->15099|31880->15130|31920->15131|31966->15148|32039->15193|32052->15196|32091->15213|32133->15235|32147->15239|32187->15240|32233->15257|32332->15324|32380->15343|32541->15476|32585->15510|32625->15511|32675->15532|32752->15581|32765->15584|32807->15604|32849->15626|32863->15630|32903->15631|32953->15652|33056->15723|33104->15743|33145->15774|33185->15775|33235->15796|33309->15842|33322->15845|33361->15862|33403->15884|33417->15888|33457->15889|33507->15910|33607->15978|33655->15998|33700->16033|33740->16034|33790->16055|33868->16105|33881->16108|33924->16129|33966->16151|33980->16155|34020->16156|34070->16177|34174->16249|34223->16269|34320->16338|34360->16368|34400->16369|34450->16390|34523->16435|34536->16438|34574->16454|34616->16476|34630->16480|34670->16481|34720->16502|34819->16569|34866->16588|34903->16615|34943->16616|34989->16633|35059->16675|35072->16678|35107->16691|35149->16713|35163->16717|35203->16718|35249->16735|35345->16799|35392->16818|35434->16850|35474->16851|35520->16868|35595->16915|35608->16918|35648->16936|35690->16958|35704->16962|35744->16963|35790->16980|35891->17049|35938->17068|35980->17100|36020->17101|36066->17118|36141->17165|36154->17168|36194->17186|36236->17208|36250->17212|36290->17213|36336->17230|36437->17299|36484->17317|36581->17386|36622->17417|36662->17418|36708->17435|36782->17481|36795->17484|36834->17501|36876->17523|36890->17527|36930->17528|36976->17545|37076->17613|37124->17633|37160->17659|37200->17660|37246->17677|37315->17718|37328->17721|37362->17733|37404->17755|37418->17759|37458->17760|37504->17777|37599->17840|37647->17860|37683->17886|37723->17887|37769->17904|37838->17945|37851->17948|37885->17960|37927->17982|37941->17986|37981->17987|38027->18004|38122->18067|38170->18086|38245->18133|38287->18165|38327->18166|38373->18183|38452->18234|38465->18237|38505->18255|38547->18277|38561->18281|38601->18282|38647->18299|38752->18372|38799->18390|38868->18431|38904->18457|38944->18458|38990->18475|39081->18538|39094->18541|39128->18553|39170->18575|39184->18579|39224->18580|39270->18597|39387->18682|39435->18701|39508->18746|39536->18764|39576->18765|39622->18782|39703->18835|39716->18838|39742->18842|39784->18864|39798->18868|39838->18869|39884->18886|39992->18962|40039->18981|40072->19004|40112->19005|40158->19022|40238->19074|40251->19077|40282->19086|40324->19108|40338->19112|40378->19113|40424->19130|40531->19205|40578->19224|40624->19260|40664->19261|40710->19278|40803->19343|40816->19346|40861->19368|40903->19390|40917->19394|40957->19395|41003->19412|41122->19499|41170->19518|41237->19557|41276->19586|41316->19587|41366->19608|41438->19652|41451->19655|41488->19670|41530->19692|41544->19696|41584->19697|41634->19718|41733->19785|41780->19804|41810->19824|41850->19825|41896->19842|41967->19885|41980->19888|42008->19894|42050->19916|42064->19920|42104->19921|42150->19938|42248->20004|42295->20023|42325->20043|42365->20044|42411->20061|42482->20104|42495->20107|42523->20113|42565->20135|42579->20139|42619->20140|42665->20157|42763->20223|42810->20242|42845->20267|42885->20268|42931->20285|43011->20337|43024->20340|43057->20351|43099->20373|43113->20377|43153->20378|43199->20395|43305->20469|43352->20488|43379->20505|43419->20506|43465->20523|43533->20563|43546->20566|43571->20569|43613->20591|43627->20595|43667->20596|43713->20613|43807->20675|43854->20694|43887->20717|43927->20718|43973->20735|44055->20789|44068->20792|44099->20801|44141->20823|44155->20827|44195->20828|44241->20845|44349->20921|44396->20939|44466->20981|44504->21009|44544->21010|44590->21027|44661->21070|44674->21073|44710->21087|44752->21109|44766->21113|44806->21114|44852->21131|44949->21196|44995->21213|45098->21288|45113->21293|45196->21354|45238->21367|45334->21435|45350->21441|45404->21473|45546->21584
                  LINES: 30->2|35->2|37->4|39->6|39->6|39->6|40->7|51->18|51->18|51->18|57->24|57->24|57->24|57->24|57->24|57->24|57->24|57->24|57->24|57->24|57->24|59->26|62->29|62->29|62->29|63->30|64->31|64->31|64->31|65->32|65->32|65->32|65->32|66->33|66->33|66->33|67->34|68->35|69->36|69->36|69->36|70->37|70->37|70->37|70->37|71->38|71->38|71->38|72->39|73->40|74->41|74->41|74->41|75->42|75->42|75->42|75->42|76->43|76->43|76->43|77->44|78->45|79->46|79->46|79->46|80->47|80->47|80->47|80->47|81->48|81->48|81->48|82->49|83->50|84->51|84->51|84->51|85->52|85->52|85->52|85->52|86->53|86->53|86->53|87->54|88->55|91->58|93->60|93->60|93->60|94->61|94->61|94->61|94->61|95->62|95->62|95->62|96->63|97->64|100->67|102->69|102->69|102->69|103->70|103->70|103->70|103->70|104->71|104->71|104->71|105->72|106->73|107->74|109->76|118->85|118->85|118->85|119->86|119->86|119->86|119->86|119->86|119->86|119->86|120->87|120->87|120->87|121->88|122->89|124->91|124->91|124->91|125->92|125->92|125->92|125->92|125->92|125->92|125->92|126->93|126->93|126->93|127->94|128->95|130->97|130->97|130->97|131->98|131->98|131->98|131->98|132->99|132->99|132->99|133->100|134->101|136->103|136->103|136->103|137->104|137->104|137->104|137->104|138->105|138->105|138->105|139->106|140->107|144->111|146->113|146->113|146->113|147->114|147->114|147->114|147->114|148->115|148->115|148->115|149->116|150->117|152->119|152->119|152->119|153->120|153->120|153->120|153->120|154->121|154->121|154->121|155->122|156->123|158->125|158->125|158->125|159->126|159->126|159->126|159->126|160->127|160->127|160->127|161->128|162->129|164->131|164->131|164->131|165->132|165->132|165->132|165->132|166->133|166->133|166->133|167->134|168->135|171->138|171->138|171->138|172->139|172->139|172->139|172->139|173->140|173->140|173->140|174->141|175->142|178->145|178->145|178->145|179->146|179->146|179->146|179->146|180->147|180->147|180->147|181->148|182->149|184->151|184->151|184->151|185->152|185->152|185->152|185->152|186->153|186->153|186->153|187->154|188->155|190->157|190->157|190->157|191->158|191->158|191->158|191->158|191->158|191->158|191->158|192->159|192->159|192->159|193->160|194->161|198->165|200->167|200->167|200->167|201->168|201->168|201->168|201->168|202->169|202->169|202->169|203->170|204->171|205->172|205->172|205->172|206->173|206->173|206->173|206->173|207->174|207->174|207->174|208->175|209->176|210->177|210->177|210->177|211->178|211->178|211->178|211->178|212->179|212->179|212->179|213->180|214->181|215->182|215->182|215->182|216->183|216->183|216->183|216->183|217->184|217->184|217->184|218->185|219->186|220->187|220->187|220->187|221->188|221->188|221->188|221->188|222->189|222->189|222->189|223->190|224->191|225->192|225->192|225->192|226->193|226->193|226->193|226->193|227->194|227->194|227->194|228->195|229->196|230->197|230->197|230->197|231->198|231->198|231->198|231->198|232->199|232->199|232->199|233->200|234->201|235->202|235->202|235->202|236->203|236->203|236->203|236->203|237->204|237->204|237->204|238->205|239->206|242->209|248->215|248->215|248->215|249->216|249->216|249->216|249->216|250->217|250->217|250->217|251->218|252->219|255->222|255->222|255->222|256->223|256->223|256->223|256->223|256->223|256->223|256->223|256->223|256->223|256->223|256->223|257->224|257->224|257->224|258->225|259->226|262->229|262->229|262->229|263->230|263->230|263->230|263->230|264->231|264->231|264->231|265->232|266->233|268->235|268->235|268->235|269->236|269->236|269->236|269->236|270->237|270->237|270->237|271->238|272->239|275->242|278->245|278->245|278->245|279->246|279->246|279->246|279->246|280->247|280->247|280->247|281->248|282->249|284->251|284->251|284->251|285->252|285->252|285->252|285->252|286->253|286->253|286->253|287->254|288->255|290->257|290->257|290->257|291->258|291->258|291->258|291->258|292->259|292->259|292->259|293->260|294->261|296->263|296->263|296->263|297->264|297->264|297->264|297->264|298->265|298->265|298->265|299->266|300->267|302->269|302->269|302->269|303->270|303->270|303->270|303->270|304->271|304->271|304->271|305->272|306->273|308->275|308->275|308->275|309->276|309->276|309->276|309->276|310->277|310->277|310->277|311->278|312->279|313->280|313->280|313->280|314->281|314->281|314->281|314->281|315->282|315->282|315->282|316->283|317->284|319->286|319->286|319->286|320->287|320->287|320->287|320->287|321->288|321->288|321->288|322->289|323->290|325->292|325->292|325->292|326->293|326->293|326->293|326->293|327->294|327->294|327->294|328->295|329->296|333->300|335->302|335->302|335->302|336->303|336->303|336->303|336->303|337->304|337->304|337->304|338->305|339->306|341->308|341->308|341->308|342->309|342->309|342->309|342->309|343->310|343->310|343->310|344->311|345->312|347->314|347->314|347->314|348->315|348->315|348->315|348->315|349->316|349->316|349->316|350->317|351->318|353->320|353->320|353->320|354->321|354->321|354->321|354->321|355->322|355->322|355->322|356->323|357->324|359->326|359->326|359->326|360->327|360->327|360->327|360->327|361->328|361->328|361->328|362->329|363->330|365->332|365->332|365->332|366->333|366->333|366->333|366->333|367->334|367->334|367->334|368->335|369->336|370->337|370->337|370->337|371->338|371->338|371->338|371->338|372->339|372->339|372->339|373->340|374->341|376->343|376->343|376->343|377->344|377->344|377->344|377->344|378->345|378->345|378->345|379->346|380->347|382->349|382->349|382->349|383->350|383->350|383->350|383->350|384->351|384->351|384->351|385->352|386->353|390->357|390->357|390->357|391->358|391->358|391->358|391->358|392->359|392->359|392->359|393->360|394->361|397->364|399->366|399->366|399->366|400->367|400->367|400->367|400->367|401->368|401->368|401->368|402->369|403->370|406->373|411->378|411->378|411->378|412->379|412->379|412->379|412->379|413->380|413->380|413->380|414->381|415->382|418->385|418->385|418->385|419->386|419->386|419->386|419->386|420->387|420->387|420->387|421->388|422->389|425->392|425->392|425->392|426->393|426->393|426->393|426->393|427->394|427->394|427->394|428->395|429->396|433->400|435->402|435->402|435->402|436->403|436->403|436->403|436->403|437->404|437->404|437->404|438->405|439->406|441->408|441->408|441->408|442->409|442->409|442->409|442->409|443->410|443->410|443->410|444->411|445->412|447->414|447->414|447->414|448->415|448->415|448->415|448->415|449->416|449->416|449->416|450->417|451->418|453->420|453->420|453->420|454->421|454->421|454->421|454->421|455->422|455->422|455->422|456->423|457->424|459->426|462->429|462->429|462->429|463->430|463->430|463->430|463->430|464->431|464->431|464->431|465->432|466->433|469->436|469->436|469->436|470->437|470->437|470->437|470->437|471->438|471->438|471->438|472->439|473->440|476->443|476->443|476->443|477->444|477->444|477->444|477->444|478->445|478->445|478->445|479->446|480->447|483->450|485->452|485->452|485->452|486->453|486->453|486->453|486->453|487->454|487->454|487->454|488->455|489->456|491->458|493->460|493->460|493->460|494->461|494->461|494->461|494->461|495->462|495->462|495->462|496->463|497->464|500->467|502->469|502->469|502->469|503->470|503->470|503->470|503->470|504->471|504->471|504->471|505->472|506->473|508->475|508->475|508->475|509->476|509->476|509->476|509->476|510->477|510->477|510->477|511->478|512->479|514->481|514->481|514->481|515->482|515->482|515->482|515->482|516->483|516->483|516->483|517->484|518->485|521->488|523->490|523->490|523->490|524->491|524->491|524->491|524->491|525->492|525->492|525->492|526->493|527->494|529->496|529->496|529->496|530->497|530->497|530->497|530->497|531->498|531->498|531->498|532->499|533->500|535->502|535->502|535->502|536->503|536->503|536->503|536->503|537->504|537->504|537->504|538->505|539->506|541->508|541->508|541->508|542->509|542->509|542->509|542->509|543->510|543->510|543->510|544->511|545->512|547->514|547->514|547->514|548->515|548->515|548->515|548->515|549->516|549->516|549->516|550->517|551->518|553->520|553->520|553->520|554->521|554->521|554->521|554->521|555->522|555->522|555->522|556->523|557->524|559->526|561->528|561->528|561->528|562->529|562->529|562->529|562->529|563->530|563->530|563->530|564->531|565->532|570->537|572->539|572->539|572->539|573->540|576->543|576->543|576->543|582->549
                  -- GENERATED --
              */
          