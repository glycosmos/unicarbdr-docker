
package views.html.tests

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object databaseFile_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

     object databaseFile_Scope1 {
import service.UserProvider

class databaseFile extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[UserProvider,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*2.2*/(userProvider: UserProvider):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.30*/("""
"""),_display_(/*3.2*/main(userProvider, "UniCarb-DB")/*3.34*/{_display_(Seq[Any](format.raw/*3.35*/("""

"""),format.raw/*5.1*/("""<script>
    $("#aboutLink").addClass("active");
</script>

<div class="about">
    <div class="container-fluid content">

        <div class="row-fluid">
            <div class="span12 centered">
                <h3 class="logo-text">
                    The Glycomic MS Database and Repository
                </h3>
            </div>
        </div>
        <div class="row-fluid info">


            <div id="data">
                <table id="algo">
                    <tr><td>Database</td><td><input type="text" id="databasename" value="" /></td>
                        <td>Taxonomical restrictions</td><td><input type="text" id="taxRest" value="" /></td>
                    </tr>
                    <tr>
                        <td>Other restrictions</td><td><input type="text" id="otherRestr" value="" /></td>
                        <td>Allowed cleavages</td><td><input type="text" id="otherRestr" value="" /></td>
                    </tr>
                    <tr>
                        <td>Scoring method</td><td><input type="text" id="otherRestr" value="" /></td>
                        <td>Scoring algorithm</td><td><input type="text" id="otherRestr" value="" /></td>
                    </tr>
                </table>
                    <br><br>
                <input type="button" value="Add New" class="addNew" name="add"/>
                <br><br>
            </div>

            <table width="50%" border="1" cellspacing="0" cellpadding="0" class="table">
                <tr>
                    <td>ACC_CODE</td>
                    <td>ACC_NAME</td>
                    <td>ACTION</td>
                </tr>
                <tr>
                    <td>&nbsp;<input type="text" class="acc_code" name="acc_code" id="acc_code"/></td>
                    <td>&nbsp;<input type="text" class="click" name="parentinput"  id="parentinput" /></td>
                    <td>&nbsp;<input type="button" value="Add New" class="addNew" name="addNew" /></td>
                </tr>
            </table>

        </div>
    </div>
</div>

<script>
$(document).ready(function() """),format.raw/*59.30*/("""{"""),format.raw/*59.31*/("""
  """),format.raw/*60.3*/("""$('.table').on('click', '.addNew',  function() """),format.raw/*60.50*/("""{"""),format.raw/*60.51*/("""
    """),format.raw/*61.5*/("""$('.table tr:last').after('<tr><td><input type="text" class="acc_code" name="acc_codes" id="acc_codes"/></td><td><input type="text" class="click" name="parentinputs"  id="parentinputs" /></td><td><input type="button" value="Add New" class="addNew" name="add" /></td></tr>');
    $("#acc_code").prop( "disabled", false );
  """),format.raw/*63.3*/("""}"""),format.raw/*63.4*/(""");
"""),format.raw/*64.1*/("""}"""),format.raw/*64.2*/(""");
</script>



""")))}),format.raw/*69.2*/("""


"""))
      }
    }
  }

  def render(userProvider:UserProvider): play.twirl.api.HtmlFormat.Appendable = apply(userProvider)

  def f:((UserProvider) => play.twirl.api.HtmlFormat.Appendable) = (userProvider) => apply(userProvider)

  def ref: this.type = this

}


}
}

/**/
object databaseFile extends databaseFile_Scope0.databaseFile_Scope1.databaseFile
              /*
                  -- GENERATED --
                  DATE: Wed Jun 19 09:50:50 UTC 2019
                  SOURCE: /app/app/views/tests/databaseFile.scala.html
                  HASH: 918326f98c38eeb2911363eeabbdb28abd4b6767
                  MATRIX: 834->30|957->58|984->60|1024->92|1062->93|1090->95|3206->2183|3235->2184|3265->2187|3340->2234|3369->2235|3401->2240|3751->2563|3779->2564|3809->2567|3837->2568|3884->2585
                  LINES: 30->2|35->2|36->3|36->3|36->3|38->5|92->59|92->59|93->60|93->60|93->60|94->61|96->63|96->63|97->64|97->64|102->69
                  -- GENERATED --
              */
          