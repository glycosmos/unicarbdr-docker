
package views.html.tests

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object autoComplete_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

     object autoComplete_Scope1 {
import service.UserProvider

class autoComplete extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[UserProvider,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*2.2*/(userProvider: UserProvider):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.30*/("""
"""),_display_(/*3.2*/main(userProvider, "UniCarb-DB")/*3.34*/{_display_(Seq[Any](format.raw/*3.35*/("""

"""),format.raw/*5.1*/("""<script>
    $("#aboutLink").addClass("active");
</script>

<div class="about">
    <div class="container-fluid content">

        <div class="row-fluid">
            <div class="span12 centered">
                <h3 class="logo-text">
                    The Glycomic MS Database and Repository
                </h3>
            </div>
        </div>
        <div class="row-fluid info">

            <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
            <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
            <script>
                $(document).ready(function() """),format.raw/*24.46*/("""{"""),format.raw/*24.47*/("""
                    """),format.raw/*25.21*/("""$(function() """),format.raw/*25.34*/("""{"""),format.raw/*25.35*/("""
                        """),format.raw/*26.25*/("""$("#search").autocomplete("""),format.raw/*26.51*/("""{"""),format.raw/*26.52*/("""
                            """),format.raw/*27.29*/("""minLength: 3,
                            source : function(request, response) """),format.raw/*28.66*/("""{"""),format.raw/*28.67*/("""
                                """),format.raw/*29.33*/("""$.ajax("""),format.raw/*29.40*/("""{"""),format.raw/*29.41*/("""
                                    """),format.raw/*30.37*/("""url : "/api/taxonomy/json/" + request.term,
                                    type : "GET",
                                    data : """),format.raw/*32.44*/("""{"""),format.raw/*32.45*/("""
                                        """),format.raw/*33.41*/("""term : request.term
                                    """),format.raw/*34.37*/("""}"""),format.raw/*34.38*/(""",
                                    dataType : "json",
                                    success : function(data) """),format.raw/*36.62*/("""{"""),format.raw/*36.63*/("""
                                        """),format.raw/*37.41*/("""response(data);
                                    """),format.raw/*38.37*/("""}"""),format.raw/*38.38*/("""
                                """),format.raw/*39.33*/("""}"""),format.raw/*39.34*/(""");
                            """),format.raw/*40.29*/("""}"""),format.raw/*40.30*/("""
                        """),format.raw/*41.25*/("""}"""),format.raw/*41.26*/(""");
                    """),format.raw/*42.21*/("""}"""),format.raw/*42.22*/(""");
                """),format.raw/*43.17*/("""}"""),format.raw/*43.18*/(""");
            </script>

            <div class="search-container">
                <div class="ui-widget">
                    <input type="text" id="search" name="search" class="search" />
                </div>
            </div>


        </div>
    </div>
</div>
""")))}),format.raw/*56.2*/("""

"""))
      }
    }
  }

  def render(userProvider:UserProvider): play.twirl.api.HtmlFormat.Appendable = apply(userProvider)

  def f:((UserProvider) => play.twirl.api.HtmlFormat.Appendable) = (userProvider) => apply(userProvider)

  def ref: this.type = this

}


}
}

/**/
object autoComplete extends autoComplete_Scope0.autoComplete_Scope1.autoComplete
              /*
                  -- GENERATED --
                  DATE: Wed Jun 19 09:50:50 UTC 2019
                  SOURCE: /app/app/views/tests/autoComplete.scala.html
                  HASH: 8d43ee396b0ea881cde3bb97c87fee55981e2c9d
                  MATRIX: 834->30|957->58|984->60|1024->92|1062->93|1090->95|1734->711|1763->712|1812->733|1853->746|1882->747|1935->772|1989->798|2018->799|2075->828|2182->907|2211->908|2272->941|2307->948|2336->949|2401->986|2566->1123|2595->1124|2664->1165|2748->1221|2777->1222|2923->1340|2952->1341|3021->1382|3101->1434|3130->1435|3191->1468|3220->1469|3279->1500|3308->1501|3361->1526|3390->1527|3441->1550|3470->1551|3517->1570|3546->1571|3846->1841
                  LINES: 30->2|35->2|36->3|36->3|36->3|38->5|57->24|57->24|58->25|58->25|58->25|59->26|59->26|59->26|60->27|61->28|61->28|62->29|62->29|62->29|63->30|65->32|65->32|66->33|67->34|67->34|69->36|69->36|70->37|71->38|71->38|72->39|72->39|73->40|73->40|74->41|74->41|75->42|75->42|76->43|76->43|89->56
                  -- GENERATED --
              */
          