# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table sampleprep.biological_context (
  biological_context_id         bigserial not null,
  constraint pk_biological_context primary key (biological_context_id)
);

create table ms.cid (
  cid_id                        bigserial not null,
  gas_composition               varchar(255),
  gas_pressure                  varchar(255),
  collision_energy              varchar(255),
  collision_cell_id             bigint,
  constraint uq_cid_collision_cell_id unique (collision_cell_id),
  constraint pk_cid primary key (cid_id)
);

create table sampleprep.cell_line (
  cell_line_id                  bigserial not null,
  cell_line_name                varchar(255),
  owl_id                        varchar(255),
  comment                       varchar(255),
  constraint pk_cell_line primary key (cell_line_id)
);

create table ms.collision_cell (
  collision_cell_id             bigserial not null,
  ms_id                         bigint,
  constraint uq_collision_cell_ms_id unique (ms_id),
  constraint pk_collision_cell primary key (collision_cell_id)
);

create table hplc.column (
  column_id                     bigserial not null,
  model                         varchar(255),
  type_chromatography           varchar(255),
  packing_material              varchar(255),
  column_size_width             varchar(255),
  column_size_length            varchar(255),
  particle_size                 varchar(255),
  manufacturer_id               bigint,
  constraint uq_column_manufacturer_id unique (manufacturer_id),
  constraint pk_column primary key (column_id)
);

create table im.cross_section (
  cross_section_id              bigserial not null,
  peak_labeled_id               bigint,
  ccs_value                     float,
  constraint uq_cross_section_peak_labeled_id unique (peak_labeled_id),
  constraint pk_cross_section primary key (cross_section_id)
);

create table ms.database (
  database_id                   bigserial not null,
  name_database                 varchar(255),
  constraint pk_database primary key (database_id)
);

create table ms.detector (
  detector_id                   bigserial not null,
  detector_type                 varchar(255),
  ms_id                         bigint,
  constraint uq_detector_ms_id unique (ms_id),
  constraint pk_detector primary key (detector_id)
);

create table ms.device (
  device_id                     bigserial not null,
  manufacturer_id               bigint,
  model                         varchar(255),
  ionisation_type               varchar(255),
  constraint pk_device primary key (device_id)
);

create table ms.ecd (
  ecd_id                        bigserial not null,
  emitter_type                  varchar(255),
  voltage                       varchar(255),
  current                       varchar(255),
  collision_cell_id             bigint,
  constraint uq_ecd_collision_cell_id unique (collision_cell_id),
  constraint pk_ecd primary key (ecd_id)
);

create table ms.esi (
  esi_id                        bigserial not null,
  supply_type                   varchar(255),
  interface_name                varchar(255),
  interface_details             varchar(255),
  sprayer_name                  varchar(255),
  sprayer_details               varchar(255),
  voltages                      varchar(255),
  prompt_fragmentation          varchar(255),
  in_source_dissociation        varchar(255),
  other_parameters              varchar(255),
  ms_id                         bigint,
  constraint uq_esi_ms_id unique (ms_id),
  constraint pk_esi primary key (esi_id)
);

create table ms.etd (
  etd_id                        bigserial not null,
  reagent_gas                   varchar(255),
  pressure                      varchar(255),
  reaction_time                 varchar(255),
  reagent_atoms                 varchar(255),
  collision_cell_id             bigint,
  constraint uq_etd_collision_cell_id unique (collision_cell_id),
  constraint pk_etd primary key (etd_id)
);

create table ms.ft_icr (
  ft_icr_id                     bigserial not null,
  peak_selection                varchar(255),
  pulse                         varchar(255),
  width                         varchar(255),
  voltage                       varchar(255),
  decay_time                    varchar(255),
  ir                            varchar(255),
  other_parameters              varchar(255),
  ms_id                         bigint,
  constraint uq_ft_icr_ms_id unique (ms_id),
  constraint pk_ft_icr primary key (ft_icr_id)
);

create table ms.file (
  file_id                       bigserial not null,
  file_name                     varchar(255),
  format                        varchar(255),
  url                           varchar(255),
  identification_target_area    varchar(255),
  ms_to_software_id             bigint,
  scan_to_database_id           bigint,
  constraint uq_file_scan_to_database_id unique (scan_to_database_id),
  constraint pk_file primary key (file_id)
);

create table core.glycan_composition (
  glycan_composition_id         bigserial not null,
  hexnac                        integer,
  hex                           integer,
  dhex                          integer,
  neuac                         integer,
  neugc                         integer,
  sulf                          integer,
  gluro                         integer,
  redend                        float,
  pento                         float,
  methyl                        integer,
  pen                           integer,
  mass                          float,
  constraint pk_glycan_composition primary key (glycan_composition_id)
);

create table core.glycan_sequence (
  glycan_sequence_id            bigserial not null,
  sequence_iupac                varchar(255),
  sequence_ct                   varchar(255),
  sequence_ct_condensed         varchar(255),
  sequence_gws                  varchar(255),
  glytoucan_id                  varchar(255),
  sequence_kcf                  varchar(255),
  contributor_id                integer,
  glycan_composition_id         bigint,
  constraint uq_glycan_sequence_glycan_composition_id unique (glycan_composition_id),
  constraint pk_glycan_sequence primary key (glycan_sequence_id)
);

create table sampleprep.glycoprotein (
  glycoprotein_id               bigserial not null,
  uniprot_id                    varchar(255),
  glycoprotein_name             varchar(255),
  description                   varchar(255),
  native_species                bigint,
  expressed_species             bigint,
  constraint pk_glycoprotein primary key (glycoprotein_id)
);

create table hplc.injector (
  injector_id                   bigserial not null,
  injector_type                 varchar(255),
  injector_settings             varchar(255),
  constraint pk_injector primary key (injector_id)
);

create table hplc.instrument (
  instrument_id                 bigserial not null,
  model                         varchar(255),
  manufacturer_id               bigint,
  constraint pk_instrument primary key (instrument_id)
);

create table ms.ion (
  ion_id                        bigserial not null,
  ion_type                      varchar(255),
  constraint pk_ion primary key (ion_id)
);

create table ms.ion_mobility (
  ion_mobility_id               bigserial not null,
  gas                           varchar(255),
  pressure                      varchar(255),
  instrument_parameters         varchar(255),
  ms_id                         bigint,
  constraint uq_ion_mobility_ms_id unique (ms_id),
  constraint pk_ion_mobility primary key (ion_mobility_id)
);

create table ms.ion_trap (
  ion_trap_id                   bigserial not null,
  ms_achieved                   varchar(255),
  ms_id                         bigint,
  constraint uq_ion_trap_ms_id unique (ms_id),
  constraint pk_ion_trap primary key (ion_trap_id)
);

create table core.journal (
  journal_id                    bigserial not null,
  journal_title                 varchar(255),
  journal_abbrev                varchar(255),
  constraint pk_journal primary key (journal_id)
);

create table core.journal_reference (
  journal_reference_id          bigserial not null,
  pubmed_id                     integer,
  authors                       varchar(255),
  title                         varchar(255),
  publication_year              integer,
  journal_volume                bigint,
  journal_start_page            bigint,
  journal_end_page              bigint,
  show                          boolean,
  user_id                       integer,
  journal_id                    bigint,
  responsible_id                bigint,
  constraint pk_journal_reference primary key (journal_reference_id)
);

create table hplc.lc (
  lc_id                         bigserial not null,
  method_run_id                 bigint,
  column_id                     bigint,
  instrument_id                 bigint,
  injector_id                   bigint,
  journal_reference_id          bigint,
  constraint uq_lc_method_run_id unique (method_run_id),
  constraint uq_lc_column_id unique (column_id),
  constraint uq_lc_instrument_id unique (instrument_id),
  constraint uq_lc_injector_id unique (injector_id),
  constraint uq_lc_journal_reference_id unique (journal_reference_id),
  constraint pk_lc primary key (lc_id)
);

create table hplc.lcmucin (
  lcmucin_id                    bigserial not null,
  gwpname                       varchar(255),
  ionization_method             varchar(255),
  retention_time                float,
  note                          varchar(255),
  constraint pk_lcmucin primary key (lcmucin_id)
);

create table linked_account (
  id                            bigserial not null,
  user_id                       bigint,
  provider_user_id              varchar(255),
  provider_key                  varchar(255),
  constraint pk_linked_account primary key (id)
);

create table ms.maldi (
  maldi_id                      bigserial not null,
  plate_composition             varchar(255),
  matrix_composition            varchar(255),
  deposition_technique          varchar(255),
  voltages                      varchar(255),
  prompt_fragmentation          varchar(255),
  psd_lid_summary               varchar(255),
  delayed_extraction            varchar(255),
  laser_type                    varchar(255),
  laser_details                 varchar(255),
  ms_id                         bigint,
  constraint uq_maldi_ms_id unique (ms_id),
  constraint pk_maldi primary key (maldi_id)
);

create table ms.ms (
  ms_id                         bigserial not null,
  customizations                varchar(255),
  ion_mode                      varchar(255),
  hardware_options              varchar(255),
  date_completed                timestamp,
  device_id                     bigint,
  journal_reference_id          bigint,
  constraint uq_ms_journal_reference_id unique (journal_reference_id),
  constraint pk_ms primary key (ms_id)
);

create table ms.ms_to_software (
  ms_to_software_id             bigserial not null,
  version_software              varchar(255),
  customizations                varchar(255),
  software_settings             varchar(255),
  upgrades                      varchar(255),
  type_processing               varchar(255),
  task                          varchar(255),
  switching_criteria            varchar(255),
  isolation_width               varchar(255),
  location_file                 varchar(255),
  software_id                   bigint,
  ms_id                         bigint,
  constraint pk_ms_to_software primary key (ms_to_software_id)
);

create table core.manufacturer (
  manufacturer_id               bigserial not null,
  manufacturer_name             varchar(255),
  url                           varchar(255),
  hplc                          boolean,
  column                        boolean,
  spectrometer                  boolean,
  constraint pk_manufacturer primary key (manufacturer_id)
);

create table sampleprep.material (
  material_id                   bigserial not null,
  description                   varchar(255),
  material_type                 varchar(255),
  constraint pk_material primary key (material_id)
);

create table hplc.method_run (
  method_run_id                 bigserial not null,
  profile                       integer,
  temperature                   varchar(255),
  solvent_a                     varchar(255),
  solvent_b                     varchar(255),
  other_solvent                 varchar(255),
  flow_rate                     varchar(255),
  flow_gradient                 varchar(255),
  run_time                      varchar(255),
  phase                         varchar(255),
  constraint pk_method_run primary key (method_run_id)
);

create table ms.peak_annotated (
  peak_annotated_id             bigserial not null,
  peak_labeled_id               bigint,
  sequence_gws                  varchar(255),
  calculated_mass               float,
  fragmentation_type            varchar(255),
  constraint pk_peak_annotated primary key (peak_annotated_id)
);

create table ms.peak_labeled (
  peak_labeled_id               bigserial not null,
  mz_value                      float,
  intensity_value               float,
  peak_list_id                  bigint,
  constraint pk_peak_labeled primary key (peak_labeled_id)
);

create table ms.peak_list (
  peak_list_id                  bigserial not null,
  base_peak_mz                  float,
  base_peak_intensity           float,
  retention_time                float,
  acquisition_number            varchar(255),
  parameters_generation         varchar(255),
  raw_data_scoring              varchar(255),
  smoothing                     varchar(255),
  threshold_algorithm           varchar(255),
  signal_to_noise               varchar(255),
  peak_height                   varchar(255),
  scan_id                       bigint,
  constraint uq_peak_list_scan_id unique (scan_id),
  constraint pk_peak_list primary key (peak_list_id)
);

create table ms.persubstitution (
  persubstitution_id            bigserial not null,
  abbreviation                  varchar(255),
  name                          varchar(255),
  constraint pk_persubstitution primary key (persubstitution_id)
);

create table ms.reducing_end (
  reducing_end_id               bigserial not null,
  abbreviation                  varchar(255),
  name                          varchar(255),
  constraint pk_reducing_end primary key (reducing_end_id)
);

create table core.responsible (
  responsible_id                bigserial not null,
  responsible_name              varchar(255),
  affiliation                   varchar(255),
  contact_information           varchar(255),
  constraint pk_responsible primary key (responsible_id)
);

create table sampleprep.sample (
  sample_id                     bigserial not null,
  general_info                  varchar(255),
  growth_harvest                varchar(255),
  treatment_storage             varchar(255),
  vendor_item_info              varchar(255),
  generation_material           varchar(255),
  purification                  varchar(255),
  taxonomy_id                   bigint,
  tissue_taxonomy_id            bigint,
  glycoprotein_id               bigint,
  sample_name_id                bigint,
  starting_material_id          bigint,
  cell_line_id                  bigint,
  constraint pk_sample primary key (sample_id)
);

create table sampleprep.sample_to_journal_reference (
  sample_to_journal_reference_id bigserial not null,
  sample_id                     bigint,
  journal_reference_id          bigint,
  constraint pk_sample_to_journal_reference primary key (sample_to_journal_reference_id)
);

create table sampleprep.sample_to_treatment (
  sample_to_treatment_id        bigserial not null,
  treatment_id                  bigint,
  sample_id                     bigint,
  purpose                       varchar(255),
  reaction_conditions           varchar(255),
  vendor_expression             varchar(255),
  origin_glycosidase            varchar(255),
  type_modification             varchar(255),
  constraint pk_sample_to_treatment primary key (sample_to_treatment_id)
);

create table ms.scan (
  scan_id                       bigserial not null,
  ms_exponent                   integer,
  persubstitution_id            bigint,
  reducing_end_id               bigint,
  glycan_sequence_id            bigint,
  show                          boolean,
  note                          varchar(255),
  stability                     varchar(255),
  orthogonal_approaches         varchar(255),
  quantity                      varchar(255),
  sample_to_journal_reference_id bigint,
  constraint uq_scan_glycan_sequence_id unique (glycan_sequence_id),
  constraint pk_scan primary key (scan_id)
);

create table ms.scan_to_database (
  scan_to_database_id           bigserial not null,
  taxonomical_restrictions      varchar(255),
  other_restrictions            varchar(255),
  allowed_cleavages             varchar(255),
  mass_accuracy                 varchar(255),
  parent_error                  varchar(255),
  fragment_error                varchar(255),
  scoring_method                varchar(255),
  scoring_algorithm             varchar(255),
  scoring_result                varchar(255),
  scoring_format                varchar(255),
  scan_id                       bigint,
  database_id                   bigint,
  constraint uq_scan_to_database_scan_id unique (scan_id),
  constraint pk_scan_to_database primary key (scan_to_database_id)
);

create table ms.scan_to_ion (
  scan_to_ion_id                bigserial not null,
  scan_id                       bigint,
  ion_id                        bigint,
  charge                        integer,
  gain                          boolean,
  neutralexchange               varchar(255),
  constraint pk_scan_to_ion primary key (scan_to_ion_id)
);

create table ms.scan_to_validation (
  scan_to_validation_id         bigserial not null,
  status                        varchar(255),
  validation_result             varchar(255),
  validation_id                 bigint,
  scan_id                       bigint,
  constraint uq_scan_to_validation_scan_id unique (scan_id),
  constraint pk_scan_to_validation primary key (scan_to_validation_id)
);

create table public.security_role (
  id                            bigserial not null,
  role_name                     varchar(255),
  constraint pk_security_role primary key (id)
);

create table ms.software (
  software_id                   bigserial not null,
  name_software                 varchar(255),
  manufacturer                  varchar(255),
  availability                  varchar(255),
  acquisition                   boolean,
  analysis                      boolean,
  processing                    boolean,
  quantitation                  boolean,
  constraint pk_software primary key (software_id)
);

create table mirage.starting_material (
  starting_material_id          bigserial not null,
  description                   varchar(255),
  constraint pk_starting_material primary key (starting_material_id)
);

create table ms.tof (
  tof_id                        bigserial not null,
  reflectron_status             varchar(255),
  ms_id                         bigint,
  constraint uq_tof_ms_id unique (ms_id),
  constraint pk_tof primary key (tof_id)
);

create table sampleprep.taxonomy (
  taxonomy_id                   bigserial not null,
  ncbi_id                       integer,
  rank                          varchar(255),
  taxon                         varchar(255),
  constraint pk_taxonomy primary key (taxonomy_id)
);

create table sampleprep.tissue_taxonomy (
  tissue_taxonomy_id            bigserial not null,
  tissue_taxon                  varchar(255),
  mesh_id                       varchar(255),
  description                   varchar(255),
  date_last_modified            timestamp,
  constraint pk_tissue_taxonomy primary key (tissue_taxonomy_id)
);

create table token_action (
  id                            bigserial not null,
  token                         varchar(255),
  target_user_id                bigint,
  type                          varchar(2),
  created                       timestamp,
  expires                       timestamp,
  constraint ck_token_action_type check (type in ('PR','EV')),
  constraint uq_token_action_token unique (token),
  constraint pk_token_action primary key (id)
);

create table sampleprep.treatment (
  treatment_id                  bigserial not null,
  treatment_name                varchar(255),
  treatment_type                varchar(255),
  constraint pk_treatment primary key (treatment_id)
);

create table public.users (
  id                            bigserial not null,
  login                         varchar(255),
  password                      varchar(255),
  last_name                     varchar(255),
  email                         varchar(255),
  created_at                    timestamp,
  updated_at                    timestamp,
  name                          varchar(255),
  first_name                    varchar(255),
  last_login                    timestamp,
  active                        boolean,
  email_validated               boolean,
  constraint pk_users primary key (id)
);

create table public.users_security_role (
  users_id                      bigint not null,
  security_role_id              bigint not null,
  constraint pk_users_security_role primary key (users_id,security_role_id)
);

create table public.users_user_permission (
  users_id                      bigint not null,
  user_permission_id            bigint not null,
  constraint pk_users_user_permission primary key (users_id,user_permission_id)
);

create table user_permission (
  id                            bigserial not null,
  value                         varchar(255),
  constraint pk_user_permission primary key (id)
);

create table ms.validation (
  validation_id                 bigserial not null,
  validation_level              varchar(255),
  constraint pk_validation primary key (validation_id)
);

create table web.v_advanced_search (
  scan_id                       bigserial not null,
  base_peak_mz                  float,
  mass                          float,
  charge                        integer,
  authors                       varchar(255),
  title                         varchar(255),
  publication_year              integer,
  pubmed_id                     integer,
  glycan_sequence_id            integer,
  sequence_gws                  varchar(255),
  hex                           integer,
  dhex                          integer,
  hexnac                        integer,
  neuac                         integer,
  neugc                         integer,
  persubstitution_id            integer,
  reducing_end_id               integer,
  taxonomy_id                   integer,
  taxon                         varchar(255),
  tissue_taxonomy_id            integer,
  tissue_taxon                  varchar(255),
  constraint pk_v_advanced_search primary key (scan_id)
);

create table web.v_bc_to_journal (
  journal_reference_id          bigint,
  ncbi_id                       varchar(255),
  mesh_id                       varchar(255),
  taxon                         varchar(255),
  tissue_taxon                  varchar(255),
  glycoprotein_name             varchar(255),
  cell_line_name                varchar(255)
);

create table web.v_biological_context (
  scan_id                       bigserial not null,
  taxon                         varchar(255),
  ncbi_id                       integer,
  tissue_taxon                  varchar(255),
  mesh_id                       varchar(255),
  glycoprotein_name             varchar(255),
  cell_line_name                varchar(255),
  journal_reference_id          integer,
  constraint pk_v_biological_context primary key (scan_id)
);

create table web.v_journal_responsible (
  journal_reference_id          bigserial not null,
  title                         varchar(255),
  responsible_name              varchar(255),
  affiliation                   varchar(255),
  contact_information           varchar(255),
  constraint pk_v_journal_responsible primary key (journal_reference_id)
);

create table web.v_lc (
  journal_reference_id          bigserial not null,
  model                         varchar(255),
  runrate                       varchar(255),
  solvent_a                     varchar(255),
  solvent_b                     varchar(255),
  manufacturer_name             varchar(255),
  constraint pk_v_lc primary key (journal_reference_id)
);

create table web.v_mass_spec (
  journal_reference_id          bigserial not null,
  manufacturer_name             varchar(255),
  model                         varchar(255),
  ionisation_type               varchar(255),
  constraint pk_v_mass_spec primary key (journal_reference_id)
);

create table web.v_miragereport_lc (
  journal_reference_id          bigserial not null,
  column_model                  varchar(255),
  packing_material              varchar(255),
  column_size_width             varchar(255),
  column_size_length            varchar(255),
  type_chromatography           varchar(255),
  particle_size                 varchar(255),
  profile                       integer,
  temperature                   varchar(255),
  solvent_a                     varchar(255),
  solvent_b                     varchar(255),
  other_solvent                 varchar(255),
  flow_rate                     varchar(255),
  flow_gradient                 varchar(255),
  run_time                      varchar(255),
  phase                         varchar(255),
  injector_type                 varchar(255),
  injector_settings             varchar(255),
  inst_model                    varchar(255),
  man_name                      varchar(255),
  col_url                       varchar(255),
  inst_name                     varchar(255),
  inst_url                      varchar(255),
  constraint pk_v_miragereport_lc primary key (journal_reference_id)
);

create table web.v_miragereport_ms (
  journal_reference_id          bigserial not null,
  ms_customizations             varchar(255),
  ion_mode                      varchar(255),
  date_completed                timestamp,
  hardware_options              varchar(255),
  device_model                  varchar(255),
  ionisation_type               varchar(255),
  manufacturer_name             varchar(255),
  url                           varchar(255),
  esi_supply_type               varchar(255),
  esi_interface_name            varchar(255),
  esi_interface_details         varchar(255),
  esi_sprayer_name              varchar(255),
  esi_sprayer_details           varchar(255),
  esi_voltages                  varchar(255),
  esi_prompt                    varchar(255),
  esi_in_source                 varchar(255),
  esi_other_param               varchar(255),
  plate_composition             varchar(255),
  matrix_composition            varchar(255),
  deposition_technique          varchar(255),
  maldi_voltages                varchar(255),
  maldi_prompt                  varchar(255),
  psd_lid_summary               varchar(255),
  delayed_extraction            varchar(255),
  laser_type                    varchar(255),
  laser_details                 varchar(255),
  detector_type                 varchar(255),
  gas                           varchar(255),
  pressure                      varchar(255),
  instrument_parameters         varchar(255),
  reflectron_status             varchar(255),
  peak_selection                varchar(255),
  pulse                         varchar(255),
  width                         varchar(255),
  maldi_voltage                 varchar(255),
  decay_time                    varchar(255),
  ir                            varchar(255),
  ft_other                      varchar(255),
  ms_achieved                   varchar(255),
  ecd_emitter_type              varchar(255),
  ecd_voltage                   varchar(255),
  ecd_current                   varchar(255),
  cid_gas_composition           varchar(255),
  cid_gas_pressure              varchar(255),
  cid_collision_energy          varchar(255),
  etd_reagent_gas               varchar(255),
  etd_pressure                  varchar(255),
  etd_reaction_time             varchar(255),
  etd_reagent_atoms             varchar(255),
  constraint pk_v_miragereport_ms primary key (journal_reference_id)
);

create table web.v_miragereport_sample (
  sample_id                     bigserial not null,
  general_info                  varchar(255),
  growth_harvest                varchar(255),
  treatment_storage             varchar(255),
  vendor_item_info              varchar(255),
  generation_material           varchar(255),
  purification                  varchar(255),
  journal_reference_id          varchar(255),
  sample_name                   varchar(255),
  starting_material             varchar(255),
  treatments                    varchar(255),
  tissues                       varchar(255),
  taxonomies                    varchar(255),
  cell_lines                    varchar(255),
  glycoproteins                 varchar(255),
  constraint pk_v_miragereport_sample primary key (sample_id)
);

create table web.v_miragereport_software (
  ms_to_software_id             bigserial not null,
  journal_reference_id          varchar(255),
  software_id                   varchar(255),
  name_software                 varchar(255),
  version_software              varchar(255),
  manufacturer                  varchar(255),
  task                          varchar(255),
  files                         varchar(255),
  constraint pk_v_miragereport_software primary key (ms_to_software_id)
);

create table web.v_mirage_structures (
  scan_id                       bigserial not null,
  base_peak_mz                  float,
  retention_time                float,
  stability                     varchar(255),
  orthogonal_approaches         varchar(255),
  quantity                      varchar(255),
  status                        varchar(255),
  validation_format             varchar(255),
  validation_level              varchar(255),
  validation_result             varchar(255),
  name_database                 varchar(255),
  taxonomical_restrictions      varchar(255),
  other_restrictions            varchar(255),
  allowed_cleavages             varchar(255),
  mass_accuracy                 varchar(255),
  parent_error                  varchar(255),
  fragment_error                varchar(255),
  scoring_method                varchar(255),
  scoring_algorithm             varchar(255),
  scoring_result                varchar(255),
  scoring_format                varchar(255),
  constraint pk_v_mirage_structures primary key (scan_id)
);

create table web.v_reference_display (
  scan_id                       bigserial not null,
  base_peak_mz                  float,
  retention_time                float,
  sequence_gws                  varchar(255),
  journal_reference_id          integer,
  charges                       varchar(255),
  constraint pk_v_reference_display primary key (scan_id)
);

create table web.v_reference (
  journal_reference_id          bigserial not null,
  title                         varchar(255),
  journal_title                 varchar(255),
  publication_year              varchar(255),
  authors                       varchar(255),
  pubmed_id                     varchar(255),
  pages                         varchar(255),
  constraint pk_v_reference primary key (journal_reference_id)
);

create table web.v_similar_spectra (
  scan_id                       bigint,
  glycan_sequence_id            bigint,
  sequence_gws                  varchar(255),
  base_peak_mz                  float,
  base_peak_intensity           float,
  show                          boolean,
  peaks                         varchar(255)
);

create table web.v_structure_details (
  scan_id                       bigserial not null,
  mass                          float,
  reducing_name                 varchar(255),
  pers_name                     varchar(255),
  constraint pk_v_structure_details primary key (scan_id)
);

create table web.v_structure_json (
  scan_id                       bigserial not null,
  sequence_gws                  varchar(255),
  sequence_ct                   varchar(255),
  base_peak_mz                  float,
  taxon                         varchar(255),
  tissue_taxon                  varchar(255),
  charge                        integer,
  reducing_end                  varchar(255),
  constraint pk_v_structure_json primary key (scan_id)
);

alter table ms.cid add constraint fk_cid_collision_cell_id foreign key (collision_cell_id) references ms.collision_cell (collision_cell_id) on delete restrict on update restrict;

alter table ms.collision_cell add constraint fk_collision_cell_ms_id foreign key (ms_id) references ms.ms (ms_id) on delete restrict on update restrict;

alter table hplc.column add constraint fk_column_manufacturer_id foreign key (manufacturer_id) references core.manufacturer (manufacturer_id) on delete restrict on update restrict;

alter table im.cross_section add constraint fk_cross_section_peak_labeled_id foreign key (peak_labeled_id) references ms.peak_labeled (peak_labeled_id) on delete restrict on update restrict;

alter table ms.detector add constraint fk_detector_ms_id foreign key (ms_id) references ms.ms (ms_id) on delete restrict on update restrict;

alter table ms.device add constraint fk_device_manufacturer_id foreign key (manufacturer_id) references core.manufacturer (manufacturer_id) on delete restrict on update restrict;
create index ix_device_manufacturer_id on ms.device (manufacturer_id);

alter table ms.ecd add constraint fk_ecd_collision_cell_id foreign key (collision_cell_id) references ms.collision_cell (collision_cell_id) on delete restrict on update restrict;

alter table ms.esi add constraint fk_esi_ms_id foreign key (ms_id) references ms.ms (ms_id) on delete restrict on update restrict;

alter table ms.etd add constraint fk_etd_collision_cell_id foreign key (collision_cell_id) references ms.collision_cell (collision_cell_id) on delete restrict on update restrict;

alter table ms.ft_icr add constraint fk_ft_icr_ms_id foreign key (ms_id) references ms.ms (ms_id) on delete restrict on update restrict;

alter table ms.file add constraint fk_file_ms_to_software_id foreign key (ms_to_software_id) references ms.ms_to_software (ms_to_software_id) on delete restrict on update restrict;
create index ix_file_ms_to_software_id on ms.file (ms_to_software_id);

alter table ms.file add constraint fk_file_scan_to_database_id foreign key (scan_to_database_id) references ms.scan_to_database (scan_to_database_id) on delete restrict on update restrict;

alter table core.glycan_sequence add constraint fk_glycan_sequence_glycan_composition_id foreign key (glycan_composition_id) references core.glycan_composition (glycan_composition_id) on delete restrict on update restrict;

alter table sampleprep.glycoprotein add constraint fk_glycoprotein_native_species foreign key (native_species) references sampleprep.taxonomy (taxonomy_id) on delete restrict on update restrict;
create index ix_glycoprotein_native_species on sampleprep.glycoprotein (native_species);

alter table sampleprep.glycoprotein add constraint fk_glycoprotein_expressed_species foreign key (expressed_species) references sampleprep.taxonomy (taxonomy_id) on delete restrict on update restrict;
create index ix_glycoprotein_expressed_species on sampleprep.glycoprotein (expressed_species);

alter table hplc.instrument add constraint fk_instrument_manufacturer_id foreign key (manufacturer_id) references core.manufacturer (manufacturer_id) on delete restrict on update restrict;
create index ix_instrument_manufacturer_id on hplc.instrument (manufacturer_id);

alter table ms.ion_mobility add constraint fk_ion_mobility_ms_id foreign key (ms_id) references ms.ms (ms_id) on delete restrict on update restrict;

alter table ms.ion_trap add constraint fk_ion_trap_ms_id foreign key (ms_id) references ms.ms (ms_id) on delete restrict on update restrict;

alter table core.journal_reference add constraint fk_journal_reference_journal_id foreign key (journal_id) references core.journal (journal_id) on delete restrict on update restrict;
create index ix_journal_reference_journal_id on core.journal_reference (journal_id);

alter table core.journal_reference add constraint fk_journal_reference_responsible_id foreign key (responsible_id) references core.responsible (responsible_id) on delete restrict on update restrict;
create index ix_journal_reference_responsible_id on core.journal_reference (responsible_id);

alter table hplc.lc add constraint fk_lc_method_run_id foreign key (method_run_id) references hplc.method_run (method_run_id) on delete restrict on update restrict;

alter table hplc.lc add constraint fk_lc_column_id foreign key (column_id) references hplc.column (column_id) on delete restrict on update restrict;

alter table hplc.lc add constraint fk_lc_instrument_id foreign key (instrument_id) references hplc.instrument (instrument_id) on delete restrict on update restrict;

alter table hplc.lc add constraint fk_lc_injector_id foreign key (injector_id) references hplc.injector (injector_id) on delete restrict on update restrict;

alter table hplc.lc add constraint fk_lc_journal_reference_id foreign key (journal_reference_id) references core.journal_reference (journal_reference_id) on delete restrict on update restrict;

alter table linked_account add constraint fk_linked_account_user_id foreign key (user_id) references public.users (id) on delete restrict on update restrict;
create index ix_linked_account_user_id on linked_account (user_id);

alter table ms.maldi add constraint fk_maldi_ms_id foreign key (ms_id) references ms.ms (ms_id) on delete restrict on update restrict;

alter table ms.ms add constraint fk_ms_device_id foreign key (device_id) references ms.device (device_id) on delete restrict on update restrict;
create index ix_ms_device_id on ms.ms (device_id);

alter table ms.ms add constraint fk_ms_journal_reference_id foreign key (journal_reference_id) references core.journal_reference (journal_reference_id) on delete restrict on update restrict;

alter table ms.ms_to_software add constraint fk_ms_to_software_software_id foreign key (software_id) references ms.software (software_id) on delete restrict on update restrict;
create index ix_ms_to_software_software_id on ms.ms_to_software (software_id);

alter table ms.ms_to_software add constraint fk_ms_to_software_ms_id foreign key (ms_id) references ms.ms (ms_id) on delete restrict on update restrict;
create index ix_ms_to_software_ms_id on ms.ms_to_software (ms_id);

alter table ms.peak_annotated add constraint fk_peak_annotated_peak_labeled_id foreign key (peak_labeled_id) references ms.peak_labeled (peak_labeled_id) on delete restrict on update restrict;
create index ix_peak_annotated_peak_labeled_id on ms.peak_annotated (peak_labeled_id);

alter table ms.peak_labeled add constraint fk_peak_labeled_peak_list_id foreign key (peak_list_id) references ms.peak_list (peak_list_id) on delete restrict on update restrict;
create index ix_peak_labeled_peak_list_id on ms.peak_labeled (peak_list_id);

alter table ms.peak_list add constraint fk_peak_list_scan_id foreign key (scan_id) references ms.scan (scan_id) on delete restrict on update restrict;

alter table sampleprep.sample add constraint fk_sample_taxonomy_id foreign key (taxonomy_id) references sampleprep.taxonomy (taxonomy_id) on delete restrict on update restrict;
create index ix_sample_taxonomy_id on sampleprep.sample (taxonomy_id);

alter table sampleprep.sample add constraint fk_sample_tissue_taxonomy_id foreign key (tissue_taxonomy_id) references sampleprep.tissue_taxonomy (tissue_taxonomy_id) on delete restrict on update restrict;
create index ix_sample_tissue_taxonomy_id on sampleprep.sample (tissue_taxonomy_id);

alter table sampleprep.sample add constraint fk_sample_glycoprotein_id foreign key (glycoprotein_id) references sampleprep.glycoprotein (glycoprotein_id) on delete restrict on update restrict;
create index ix_sample_glycoprotein_id on sampleprep.sample (glycoprotein_id);

alter table sampleprep.sample add constraint fk_sample_sample_name_id foreign key (sample_name_id) references sampleprep.material (material_id) on delete restrict on update restrict;
create index ix_sample_sample_name_id on sampleprep.sample (sample_name_id);

alter table sampleprep.sample add constraint fk_sample_starting_material_id foreign key (starting_material_id) references sampleprep.material (material_id) on delete restrict on update restrict;
create index ix_sample_starting_material_id on sampleprep.sample (starting_material_id);

alter table sampleprep.sample add constraint fk_sample_cell_line_id foreign key (cell_line_id) references sampleprep.cell_line (cell_line_id) on delete restrict on update restrict;
create index ix_sample_cell_line_id on sampleprep.sample (cell_line_id);

alter table sampleprep.sample_to_journal_reference add constraint fk_sample_to_journal_reference_sample_id foreign key (sample_id) references sampleprep.sample (sample_id) on delete restrict on update restrict;
create index ix_sample_to_journal_reference_sample_id on sampleprep.sample_to_journal_reference (sample_id);

alter table sampleprep.sample_to_journal_reference add constraint fk_sample_to_journal_reference_journal_reference_id foreign key (journal_reference_id) references core.journal_reference (journal_reference_id) on delete restrict on update restrict;
create index ix_sample_to_journal_reference_journal_reference_id on sampleprep.sample_to_journal_reference (journal_reference_id);

alter table sampleprep.sample_to_treatment add constraint fk_sample_to_treatment_treatment_id foreign key (treatment_id) references sampleprep.treatment (treatment_id) on delete restrict on update restrict;
create index ix_sample_to_treatment_treatment_id on sampleprep.sample_to_treatment (treatment_id);

alter table sampleprep.sample_to_treatment add constraint fk_sample_to_treatment_sample_id foreign key (sample_id) references sampleprep.sample (sample_id) on delete restrict on update restrict;
create index ix_sample_to_treatment_sample_id on sampleprep.sample_to_treatment (sample_id);

alter table ms.scan add constraint fk_scan_persubstitution_id foreign key (persubstitution_id) references ms.persubstitution (persubstitution_id) on delete restrict on update restrict;
create index ix_scan_persubstitution_id on ms.scan (persubstitution_id);

alter table ms.scan add constraint fk_scan_reducing_end_id foreign key (reducing_end_id) references ms.reducing_end (reducing_end_id) on delete restrict on update restrict;
create index ix_scan_reducing_end_id on ms.scan (reducing_end_id);

alter table ms.scan add constraint fk_scan_glycan_sequence_id foreign key (glycan_sequence_id) references core.glycan_sequence (glycan_sequence_id) on delete restrict on update restrict;

alter table ms.scan add constraint fk_scan_sample_to_journal_reference_id foreign key (sample_to_journal_reference_id) references sampleprep.sample_to_journal_reference (sample_to_journal_reference_id) on delete restrict on update restrict;
create index ix_scan_sample_to_journal_reference_id on ms.scan (sample_to_journal_reference_id);

alter table ms.scan_to_database add constraint fk_scan_to_database_scan_id foreign key (scan_id) references ms.scan (scan_id) on delete restrict on update restrict;

alter table ms.scan_to_database add constraint fk_scan_to_database_database_id foreign key (database_id) references ms.database (database_id) on delete restrict on update restrict;
create index ix_scan_to_database_database_id on ms.scan_to_database (database_id);

alter table ms.scan_to_ion add constraint fk_scan_to_ion_scan_id foreign key (scan_id) references ms.scan (scan_id) on delete restrict on update restrict;
create index ix_scan_to_ion_scan_id on ms.scan_to_ion (scan_id);

alter table ms.scan_to_ion add constraint fk_scan_to_ion_ion_id foreign key (ion_id) references ms.ion (ion_id) on delete restrict on update restrict;
create index ix_scan_to_ion_ion_id on ms.scan_to_ion (ion_id);

alter table ms.scan_to_validation add constraint fk_scan_to_validation_validation_id foreign key (validation_id) references ms.validation (validation_id) on delete restrict on update restrict;
create index ix_scan_to_validation_validation_id on ms.scan_to_validation (validation_id);

alter table ms.scan_to_validation add constraint fk_scan_to_validation_scan_id foreign key (scan_id) references ms.scan (scan_id) on delete restrict on update restrict;

alter table ms.tof add constraint fk_tof_ms_id foreign key (ms_id) references ms.ms (ms_id) on delete restrict on update restrict;

alter table token_action add constraint fk_token_action_target_user_id foreign key (target_user_id) references public.users (id) on delete restrict on update restrict;
create index ix_token_action_target_user_id on token_action (target_user_id);

alter table public.users_security_role add constraint fk_users_security_role_users foreign key (users_id) references public.users (id) on delete restrict on update restrict;
create index ix_users_security_role_users on public.users_security_role (users_id);

alter table public.users_security_role add constraint fk_users_security_role_security_role foreign key (security_role_id) references public.security_role (id) on delete restrict on update restrict;
create index ix_users_security_role_security_role on public.users_security_role (security_role_id);

alter table public.users_user_permission add constraint fk_users_user_permission_users foreign key (users_id) references public.users (id) on delete restrict on update restrict;
create index ix_users_user_permission_users on public.users_user_permission (users_id);

alter table public.users_user_permission add constraint fk_users_user_permission_user_permission foreign key (user_permission_id) references user_permission (id) on delete restrict on update restrict;
create index ix_users_user_permission_user_permission on public.users_user_permission (user_permission_id);


# --- !Downs

alter table if exists ms.cid drop constraint if exists fk_cid_collision_cell_id;

alter table if exists ms.collision_cell drop constraint if exists fk_collision_cell_ms_id;

alter table if exists hplc.column drop constraint if exists fk_column_manufacturer_id;

alter table if exists im.cross_section drop constraint if exists fk_cross_section_peak_labeled_id;

alter table if exists ms.detector drop constraint if exists fk_detector_ms_id;

alter table if exists ms.device drop constraint if exists fk_device_manufacturer_id;
drop index if exists ix_device_manufacturer_id;

alter table if exists ms.ecd drop constraint if exists fk_ecd_collision_cell_id;

alter table if exists ms.esi drop constraint if exists fk_esi_ms_id;

alter table if exists ms.etd drop constraint if exists fk_etd_collision_cell_id;

alter table if exists ms.ft_icr drop constraint if exists fk_ft_icr_ms_id;

alter table if exists ms.file drop constraint if exists fk_file_ms_to_software_id;
drop index if exists ix_file_ms_to_software_id;

alter table if exists ms.file drop constraint if exists fk_file_scan_to_database_id;

alter table if exists core.glycan_sequence drop constraint if exists fk_glycan_sequence_glycan_composition_id;

alter table if exists sampleprep.glycoprotein drop constraint if exists fk_glycoprotein_native_species;
drop index if exists ix_glycoprotein_native_species;

alter table if exists sampleprep.glycoprotein drop constraint if exists fk_glycoprotein_expressed_species;
drop index if exists ix_glycoprotein_expressed_species;

alter table if exists hplc.instrument drop constraint if exists fk_instrument_manufacturer_id;
drop index if exists ix_instrument_manufacturer_id;

alter table if exists ms.ion_mobility drop constraint if exists fk_ion_mobility_ms_id;

alter table if exists ms.ion_trap drop constraint if exists fk_ion_trap_ms_id;

alter table if exists core.journal_reference drop constraint if exists fk_journal_reference_journal_id;
drop index if exists ix_journal_reference_journal_id;

alter table if exists core.journal_reference drop constraint if exists fk_journal_reference_responsible_id;
drop index if exists ix_journal_reference_responsible_id;

alter table if exists hplc.lc drop constraint if exists fk_lc_method_run_id;

alter table if exists hplc.lc drop constraint if exists fk_lc_column_id;

alter table if exists hplc.lc drop constraint if exists fk_lc_instrument_id;

alter table if exists hplc.lc drop constraint if exists fk_lc_injector_id;

alter table if exists hplc.lc drop constraint if exists fk_lc_journal_reference_id;

alter table if exists linked_account drop constraint if exists fk_linked_account_user_id;
drop index if exists ix_linked_account_user_id;

alter table if exists ms.maldi drop constraint if exists fk_maldi_ms_id;

alter table if exists ms.ms drop constraint if exists fk_ms_device_id;
drop index if exists ix_ms_device_id;

alter table if exists ms.ms drop constraint if exists fk_ms_journal_reference_id;

alter table if exists ms.ms_to_software drop constraint if exists fk_ms_to_software_software_id;
drop index if exists ix_ms_to_software_software_id;

alter table if exists ms.ms_to_software drop constraint if exists fk_ms_to_software_ms_id;
drop index if exists ix_ms_to_software_ms_id;

alter table if exists ms.peak_annotated drop constraint if exists fk_peak_annotated_peak_labeled_id;
drop index if exists ix_peak_annotated_peak_labeled_id;

alter table if exists ms.peak_labeled drop constraint if exists fk_peak_labeled_peak_list_id;
drop index if exists ix_peak_labeled_peak_list_id;

alter table if exists ms.peak_list drop constraint if exists fk_peak_list_scan_id;

alter table if exists sampleprep.sample drop constraint if exists fk_sample_taxonomy_id;
drop index if exists ix_sample_taxonomy_id;

alter table if exists sampleprep.sample drop constraint if exists fk_sample_tissue_taxonomy_id;
drop index if exists ix_sample_tissue_taxonomy_id;

alter table if exists sampleprep.sample drop constraint if exists fk_sample_glycoprotein_id;
drop index if exists ix_sample_glycoprotein_id;

alter table if exists sampleprep.sample drop constraint if exists fk_sample_sample_name_id;
drop index if exists ix_sample_sample_name_id;

alter table if exists sampleprep.sample drop constraint if exists fk_sample_starting_material_id;
drop index if exists ix_sample_starting_material_id;

alter table if exists sampleprep.sample drop constraint if exists fk_sample_cell_line_id;
drop index if exists ix_sample_cell_line_id;

alter table if exists sampleprep.sample_to_journal_reference drop constraint if exists fk_sample_to_journal_reference_sample_id;
drop index if exists ix_sample_to_journal_reference_sample_id;

alter table if exists sampleprep.sample_to_journal_reference drop constraint if exists fk_sample_to_journal_reference_journal_reference_id;
drop index if exists ix_sample_to_journal_reference_journal_reference_id;

alter table if exists sampleprep.sample_to_treatment drop constraint if exists fk_sample_to_treatment_treatment_id;
drop index if exists ix_sample_to_treatment_treatment_id;

alter table if exists sampleprep.sample_to_treatment drop constraint if exists fk_sample_to_treatment_sample_id;
drop index if exists ix_sample_to_treatment_sample_id;

alter table if exists ms.scan drop constraint if exists fk_scan_persubstitution_id;
drop index if exists ix_scan_persubstitution_id;

alter table if exists ms.scan drop constraint if exists fk_scan_reducing_end_id;
drop index if exists ix_scan_reducing_end_id;

alter table if exists ms.scan drop constraint if exists fk_scan_glycan_sequence_id;

alter table if exists ms.scan drop constraint if exists fk_scan_sample_to_journal_reference_id;
drop index if exists ix_scan_sample_to_journal_reference_id;

alter table if exists ms.scan_to_database drop constraint if exists fk_scan_to_database_scan_id;

alter table if exists ms.scan_to_database drop constraint if exists fk_scan_to_database_database_id;
drop index if exists ix_scan_to_database_database_id;

alter table if exists ms.scan_to_ion drop constraint if exists fk_scan_to_ion_scan_id;
drop index if exists ix_scan_to_ion_scan_id;

alter table if exists ms.scan_to_ion drop constraint if exists fk_scan_to_ion_ion_id;
drop index if exists ix_scan_to_ion_ion_id;

alter table if exists ms.scan_to_validation drop constraint if exists fk_scan_to_validation_validation_id;
drop index if exists ix_scan_to_validation_validation_id;

alter table if exists ms.scan_to_validation drop constraint if exists fk_scan_to_validation_scan_id;

alter table if exists ms.tof drop constraint if exists fk_tof_ms_id;

alter table if exists token_action drop constraint if exists fk_token_action_target_user_id;
drop index if exists ix_token_action_target_user_id;

alter table if exists public.users_security_role drop constraint if exists fk_users_security_role_users;
drop index if exists ix_users_security_role_users;

alter table if exists public.users_security_role drop constraint if exists fk_users_security_role_security_role;
drop index if exists ix_users_security_role_security_role;

alter table if exists public.users_user_permission drop constraint if exists fk_users_user_permission_users;
drop index if exists ix_users_user_permission_users;

alter table if exists public.users_user_permission drop constraint if exists fk_users_user_permission_user_permission;
drop index if exists ix_users_user_permission_user_permission;

drop table if exists sampleprep.biological_context cascade;

drop table if exists ms.cid cascade;

drop table if exists sampleprep.cell_line cascade;

drop table if exists ms.collision_cell cascade;

drop table if exists hplc.column cascade;

drop table if exists im.cross_section cascade;

drop table if exists ms.database cascade;

drop table if exists ms.detector cascade;

drop table if exists ms.device cascade;

drop table if exists ms.ecd cascade;

drop table if exists ms.esi cascade;

drop table if exists ms.etd cascade;

drop table if exists ms.ft_icr cascade;

drop table if exists ms.file cascade;

drop table if exists core.glycan_composition cascade;

drop table if exists core.glycan_sequence cascade;

drop table if exists sampleprep.glycoprotein cascade;

drop table if exists hplc.injector cascade;

drop table if exists hplc.instrument cascade;

drop table if exists ms.ion cascade;

drop table if exists ms.ion_mobility cascade;

drop table if exists ms.ion_trap cascade;

drop table if exists core.journal cascade;

drop table if exists core.journal_reference cascade;

drop table if exists hplc.lc cascade;

drop table if exists hplc.lcmucin cascade;

drop table if exists linked_account cascade;

drop table if exists ms.maldi cascade;

drop table if exists ms.ms cascade;

drop table if exists ms.ms_to_software cascade;

drop table if exists core.manufacturer cascade;

drop table if exists sampleprep.material cascade;

drop table if exists hplc.method_run cascade;

drop table if exists ms.peak_annotated cascade;

drop table if exists ms.peak_labeled cascade;

drop table if exists ms.peak_list cascade;

drop table if exists ms.persubstitution cascade;

drop table if exists ms.reducing_end cascade;

drop table if exists core.responsible cascade;

drop table if exists sampleprep.sample cascade;

drop table if exists sampleprep.sample_to_journal_reference cascade;

drop table if exists sampleprep.sample_to_treatment cascade;

drop table if exists ms.scan cascade;

drop table if exists ms.scan_to_database cascade;

drop table if exists ms.scan_to_ion cascade;

drop table if exists ms.scan_to_validation cascade;

drop table if exists public.security_role cascade;

drop table if exists ms.software cascade;

drop table if exists mirage.starting_material cascade;

drop table if exists ms.tof cascade;

drop table if exists sampleprep.taxonomy cascade;

drop table if exists sampleprep.tissue_taxonomy cascade;

drop table if exists token_action cascade;

drop table if exists sampleprep.treatment cascade;

drop table if exists public.users cascade;

drop table if exists public.users_security_role cascade;

drop table if exists public.users_user_permission cascade;

drop table if exists user_permission cascade;

drop table if exists ms.validation cascade;

drop table if exists web.v_advanced_search cascade;

drop table if exists web.v_bc_to_journal cascade;

drop table if exists web.v_biological_context cascade;

drop table if exists web.v_journal_responsible cascade;

drop table if exists web.v_lc cascade;

drop table if exists web.v_mass_spec cascade;

drop table if exists web.v_miragereport_lc cascade;

drop table if exists web.v_miragereport_ms cascade;

drop table if exists web.v_miragereport_sample cascade;

drop table if exists web.v_miragereport_software cascade;

drop table if exists web.v_mirage_structures cascade;

drop table if exists web.v_reference_display cascade;

drop table if exists web.v_reference cascade;

drop table if exists web.v_similar_spectra cascade;

drop table if exists web.v_structure_details cascade;

drop table if exists web.v_structure_json cascade;

