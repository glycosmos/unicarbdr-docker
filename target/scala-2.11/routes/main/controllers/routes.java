
// @GENERATOR:play-routes-compiler
// @SOURCE:/app/conf/routes
// @DATE:Wed Jun 19 09:50:46 UTC 2019

package controllers;

import router.RoutesPrefix;

public class routes {
  
  public static final controllers.ReverseAssets Assets = new controllers.ReverseAssets(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseUploadDataController UploadDataController = new controllers.ReverseUploadDataController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseGWPController GWPController = new controllers.ReverseGWPController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseHomeController HomeController = new controllers.ReverseHomeController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseSignup Signup = new controllers.ReverseSignup(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseGenerateController GenerateController = new controllers.ReverseGenerateController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseSearchController SearchController = new controllers.ReverseSearchController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseAccount Account = new controllers.ReverseAccount(RoutesPrefix.byNamePrefix());

  public static class javascript {
    
    public static final controllers.javascript.ReverseAssets Assets = new controllers.javascript.ReverseAssets(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseUploadDataController UploadDataController = new controllers.javascript.ReverseUploadDataController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseGWPController GWPController = new controllers.javascript.ReverseGWPController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseHomeController HomeController = new controllers.javascript.ReverseHomeController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseSignup Signup = new controllers.javascript.ReverseSignup(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseGenerateController GenerateController = new controllers.javascript.ReverseGenerateController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseSearchController SearchController = new controllers.javascript.ReverseSearchController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseAccount Account = new controllers.javascript.ReverseAccount(RoutesPrefix.byNamePrefix());
  }

}
