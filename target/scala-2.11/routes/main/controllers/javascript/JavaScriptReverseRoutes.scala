
// @GENERATOR:play-routes-compiler
// @SOURCE:/app/conf/routes
// @DATE:Wed Jun 19 09:50:46 UTC 2019

import play.api.routing.JavaScriptReverseRoute
import play.api.mvc.{ QueryStringBindable, PathBindable, Call, JavascriptLiteral }
import play.core.routing.{ HandlerDef, ReverseRouteContext, queryString, dynamicString }


import _root_.controllers.Assets.Asset
import _root_.play.libs.F

// @LINE:6
package controllers.javascript {
  import ReverseRouteContext.empty

  // @LINE:50
  class ReverseAssets(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:61
    def at: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Assets.at",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "favicon.ico"})
        }
      """
    )
  
    // @LINE:50
    def versioned: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Assets.versioned",
      """
        function(file1) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "assets/" + (""" + implicitly[PathBindable[Asset]].javascriptUnbind + """)("file", file1)})
        }
      """
    )
  
  }

  // @LINE:58
  class ReverseUploadDataController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:58
    def upload: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.UploadDataController.upload",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "upload"})
        }
      """
    )
  
    // @LINE:59
    def uploadData: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.UploadDataController.uploadData",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "uploaddata"})
        }
      """
    )
  
  }

  // @LINE:27
  class ReverseGWPController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:28
    def showImageId: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.GWPController.showImageId",
      """
        function(id0,notation1,style2) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "showImageId" + _qS([(""" + implicitly[QueryStringBindable[Long]].javascriptUnbind + """)("id", id0), (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("notation", notation1), (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("style", style2)])})
        }
      """
    )
  
    // @LINE:27
    def showImage: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.GWPController.showImage",
      """
        function(structure0,notation1,style2) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "showImage" + _qS([(""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("structure", structure0), (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("notation", notation1), (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("style", style2)])})
        }
      """
    )
  
  }

  // @LINE:6
  class ReverseHomeController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:85
    def doSignup: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.HomeController.doSignup",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "signup"})
        }
      """
    )
  
    // @LINE:76
    def profile: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.HomeController.profile",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "profile"})
        }
      """
    )
  
    // @LINE:20
    def library: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.HomeController.library",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "library"})
        }
      """
    )
  
    // @LINE:30
    def allStructuresToJson: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.HomeController.allStructuresToJson",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/structures/json"})
        }
      """
    )
  
    // @LINE:84
    def signup: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.HomeController.signup",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "signup"})
        }
      """
    )
  
    // @LINE:11
    def about: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.HomeController.about",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "about"})
        }
      """
    )
  
    // @LINE:79
    def doLogin: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.HomeController.doLogin",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "login"})
        }
      """
    )
  
    // @LINE:73
    def uploadIndex: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.HomeController.uploadIndex",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "uploadData"})
        }
      """
    )
  
    // @LINE:41
    def mirageReport: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.HomeController.mirageReport",
      """
        function(id0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "mirageReport/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id0)})
        }
      """
    )
  
    // @LINE:16
    def msData: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.HomeController.msData",
      """
        function(id0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "msData/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id0)})
        }
      """
    )
  
    // @LINE:13
    def listReferences: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.HomeController.listReferences",
      """
        function(p0,s1,o2,f3) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "references" + _qS([(p0 == null ? null : (""" + implicitly[QueryStringBindable[Int]].javascriptUnbind + """)("p", p0)), (s1 == null ? null : (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("s", s1)), (o2 == null ? null : (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("o", o2)), (f3 == null ? null : (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("f", f3))])})
        }
      """
    )
  
    // @LINE:6
    def index: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.HomeController.index",
      """
        function() {
        
          if (true) {
            return _wA({method:"GET", url:"""" + _prefix + """"})
          }
        
        }
      """
    )
  
    // @LINE:18
    def format: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.HomeController.format",
      """
        function(s0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "format/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("s", encodeURIComponent(s0))})
        }
      """
    )
  
    // @LINE:78
    def login: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.HomeController.login",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "login"})
        }
      """
    )
  
    // @LINE:14
    def referenceDisplay: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.HomeController.referenceDisplay",
      """
        function(id0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "references/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id0)})
        }
      """
    )
  
  }

  // @LINE:87
  class ReverseSignup(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:91
    def exists: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Signup.exists",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "accounts/exists"})
        }
      """
    )
  
    // @LINE:90
    def verify: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Signup.verify",
      """
        function(token0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "accounts/verify/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("token", encodeURIComponent(token0))})
        }
      """
    )
  
    // @LINE:87
    def unverified: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Signup.unverified",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "accounts/unverified"})
        }
      """
    )
  
    // @LINE:88
    def oAuthDenied: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Signup.oAuthDenied",
      """
        function(provider0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "authenticate/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("provider", encodeURIComponent(provider0)) + "/denied"})
        }
      """
    )
  
    // @LINE:93
    def resetPassword: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Signup.resetPassword",
      """
        function(token0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "accounts/password/reset/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("token", encodeURIComponent(token0))})
        }
      """
    )
  
    // @LINE:94
    def doResetPassword: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Signup.doResetPassword",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "accounts/password/reset"})
        }
      """
    )
  
    // @LINE:110
    def doForgotPassword: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Signup.doForgotPassword",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "login/password/forgot"})
        }
      """
    )
  
    // @LINE:109
    def forgotPassword: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Signup.forgotPassword",
      """
        function(email0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "login/password/forgot" + _qS([(email0 == null ? null : (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("email", email0))])})
        }
      """
    )
  
  }

  // @LINE:33
  class ReverseGenerateController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:35
    def speciesToJson: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.GenerateController.speciesToJson",
      """
        function(q0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/species/json/" + _qS([(""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("q", q0)])})
        }
      """
    )
  
    // @LINE:52
    def generate: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.GenerateController.generate",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "generate"})
        }
      """
    )
  
    // @LINE:56
    def uploadForm: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.GenerateController.uploadForm",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "generateformupload"})
        }
      """
    )
  
    // @LINE:53
    def generateMirage: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.GenerateController.generateMirage",
      """
        function(setup0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "generate/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("setup", encodeURIComponent(setup0))})
        }
      """
    )
  
    // @LINE:54
    def generateFile: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.GenerateController.generateFile",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "generatefile"})
        }
      """
    )
  
    // @LINE:55
    def generateForm: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.GenerateController.generateForm",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "generateform"})
        }
      """
    )
  
    // @LINE:33
    def cellToJson: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.GenerateController.cellToJson",
      """
        function(q0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/cell/json/" + _qS([(""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("q", q0)])})
        }
      """
    )
  
    // @LINE:34
    def originToJson: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.GenerateController.originToJson",
      """
        function(q0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/origin/json/" + _qS([(""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("q", q0)])})
        }
      """
    )
  
  }

  // @LINE:22
  class ReverseSearchController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:24
    def searchBasic: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.SearchController.searchBasic",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "basic"})
        }
      """
    )
  
    // @LINE:31
    def searchStructures: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.SearchController.searchStructures",
      """
        function(id0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "searchStructures/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id0)})
        }
      """
    )
  
    // @LINE:25
    def searchMSMS: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.SearchController.searchMSMS",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "peakMatching"})
        }
      """
    )
  
    // @LINE:22
    def search: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.SearchController.search",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "search"})
        }
      """
    )
  
    // @LINE:23
    def searchAdvanced: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.SearchController.searchAdvanced",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "advanced"})
        }
      """
    )
  
  }

  // @LINE:96
  class ReverseAccount(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:106
    def askMerge: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Account.askMerge",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "accounts/merge"})
        }
      """
    )
  
    // @LINE:107
    def doMerge: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Account.doMerge",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "accounts/merge"})
        }
      """
    )
  
    // @LINE:99
    def verifyEmail: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Account.verifyEmail",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "accounts/verify"})
        }
      """
    )
  
    // @LINE:103
    def askLink: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Account.askLink",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "accounts/link"})
        }
      """
    )
  
    // @LINE:104
    def doLink: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Account.doLink",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "accounts/link"})
        }
      """
    )
  
    // @LINE:96
    def changePassword: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Account.changePassword",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "accounts/password/change"})
        }
      """
    )
  
    // @LINE:101
    def link: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Account.link",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "accounts/add"})
        }
      """
    )
  
    // @LINE:97
    def doChangePassword: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Account.doChangePassword",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "accounts/password/change"})
        }
      """
    )
  
  }


}
