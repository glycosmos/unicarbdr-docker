
// @GENERATOR:play-routes-compiler
// @SOURCE:/app/conf/routes
// @DATE:Wed Jun 19 09:50:46 UTC 2019

package router

import play.core.routing._
import play.core.routing.HandlerInvokerFactory._
import play.core.j._

import play.api.mvc._

import _root_.controllers.Assets.Asset
import _root_.play.libs.F

class Routes(
  override val errorHandler: play.api.http.HttpErrorHandler, 
  // @LINE:6
  HomeController_2: controllers.HomeController,
  // @LINE:22
  SearchController_0: controllers.SearchController,
  // @LINE:27
  GWPController_1: controllers.GWPController,
  // @LINE:33
  GenerateController_5: controllers.GenerateController,
  // @LINE:50
  Assets_7: controllers.Assets,
  // @LINE:58
  UploadDataController_6: controllers.UploadDataController,
  // @LINE:81
  Authenticate_8: com.feth.play.module.pa.controllers.Authenticate,
  // @LINE:87
  Signup_3: controllers.Signup,
  // @LINE:96
  Account_4: controllers.Account,
  val prefix: String
) extends GeneratedRouter {

   @javax.inject.Inject()
   def this(errorHandler: play.api.http.HttpErrorHandler,
    // @LINE:6
    HomeController_2: controllers.HomeController,
    // @LINE:22
    SearchController_0: controllers.SearchController,
    // @LINE:27
    GWPController_1: controllers.GWPController,
    // @LINE:33
    GenerateController_5: controllers.GenerateController,
    // @LINE:50
    Assets_7: controllers.Assets,
    // @LINE:58
    UploadDataController_6: controllers.UploadDataController,
    // @LINE:81
    Authenticate_8: com.feth.play.module.pa.controllers.Authenticate,
    // @LINE:87
    Signup_3: controllers.Signup,
    // @LINE:96
    Account_4: controllers.Account
  ) = this(errorHandler, HomeController_2, SearchController_0, GWPController_1, GenerateController_5, Assets_7, UploadDataController_6, Authenticate_8, Signup_3, Account_4, "/")

  import ReverseRouteContext.empty

  def withPrefix(prefix: String): Routes = {
    router.RoutesPrefix.setPrefix(prefix)
    new Routes(errorHandler, HomeController_2, SearchController_0, GWPController_1, GenerateController_5, Assets_7, UploadDataController_6, Authenticate_8, Signup_3, Account_4, prefix)
  }

  private[this] val defaultPrefix: String = {
    if (this.prefix.endsWith("/")) "" else "/"
  }

  def documentation = List(
    ("""GET""", this.prefix, """controllers.HomeController.index()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """unicarb-db""", """controllers.HomeController.index()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """unicarb-db/""", """controllers.HomeController.index()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """about""", """controllers.HomeController.about()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """references""", """controllers.HomeController.listReferences(p:Int ?= 0, s:String ?= "publication_year", o:String ?= "desc", f:String ?= "")"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """references/""" + "$" + """id<[^/]+>""", """controllers.HomeController.referenceDisplay(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """msData/""" + "$" + """id<[^/]+>""", """controllers.HomeController.msData(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """format/""" + "$" + """s<[^/]+>""", """controllers.HomeController.format(s:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """library""", """controllers.HomeController.library()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """search""", """controllers.SearchController.search()"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """advanced""", """controllers.SearchController.searchAdvanced()"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """basic""", """controllers.SearchController.searchBasic()"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """peakMatching""", """controllers.SearchController.searchMSMS()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """showImage""", """controllers.GWPController.showImage(structure:String, notation:String, style:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """showImageId""", """controllers.GWPController.showImageId(id:Long, notation:String, style:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/structures/json""", """controllers.HomeController.allStructuresToJson()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """searchStructures/""" + "$" + """id<[^/]+>""", """controllers.SearchController.searchStructures(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/cell/json/""", """controllers.GenerateController.cellToJson(q:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/origin/json/""", """controllers.GenerateController.originToJson(q:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/species/json/""", """controllers.GenerateController.speciesToJson(q:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """mirageReport/""" + "$" + """id<[^/]+>""", """controllers.HomeController.mirageReport(id:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """assets/""" + "$" + """file<.+>""", """controllers.Assets.versioned(path:String = "/public", file:Asset)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """generate""", """controllers.GenerateController.generate()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """generate/""" + "$" + """setup<[^/]+>""", """controllers.GenerateController.generateMirage(setup:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """generatefile""", """controllers.GenerateController.generateFile()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """generateform""", """controllers.GenerateController.generateForm()"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """generateformupload""", """controllers.GenerateController.uploadForm()"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """upload""", """controllers.UploadDataController.upload()"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """uploaddata""", """controllers.UploadDataController.uploadData()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """favicon.ico""", """controllers.Assets.at(path:String = "/public/images", file:String = "favicon.ico")"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """uploadData""", """controllers.HomeController.uploadIndex()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """profile""", """controllers.HomeController.profile"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """login""", """controllers.HomeController.login"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """login""", """controllers.HomeController.doLogin"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """logout""", """com.feth.play.module.pa.controllers.Authenticate.logout"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """authenticate/""" + "$" + """provider<[^/]+>""", """com.feth.play.module.pa.controllers.Authenticate.authenticate(provider:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """signup""", """controllers.HomeController.signup"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """signup""", """controllers.HomeController.doSignup"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """accounts/unverified""", """controllers.Signup.unverified"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """authenticate/""" + "$" + """provider<[^/]+>/denied""", """controllers.Signup.oAuthDenied(provider:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """accounts/verify/""" + "$" + """token<[^/]+>""", """controllers.Signup.verify(token:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """accounts/exists""", """controllers.Signup.exists"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """accounts/password/reset/""" + "$" + """token<[^/]+>""", """controllers.Signup.resetPassword(token:String)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """accounts/password/reset""", """controllers.Signup.doResetPassword"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """accounts/password/change""", """controllers.Account.changePassword"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """accounts/password/change""", """controllers.Account.doChangePassword"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """accounts/verify""", """controllers.Account.verifyEmail"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """accounts/add""", """controllers.Account.link"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """accounts/link""", """controllers.Account.askLink"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """accounts/link""", """controllers.Account.doLink"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """accounts/merge""", """controllers.Account.askMerge"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """accounts/merge""", """controllers.Account.doMerge"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """login/password/forgot""", """controllers.Signup.forgotPassword(email:String ?= "")"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """login/password/forgot""", """controllers.Signup.doForgotPassword"""),
    Nil
  ).foldLeft(List.empty[(String,String,String)]) { (s,e) => e.asInstanceOf[Any] match {
    case r @ (_,_,_) => s :+ r.asInstanceOf[(String,String,String)]
    case l => s ++ l.asInstanceOf[List[(String,String,String)]]
  }}


  // @LINE:6
  private[this] lazy val controllers_HomeController_index0_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix)))
  )
  private[this] lazy val controllers_HomeController_index0_invoker = createInvoker(
    HomeController_2.index(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.HomeController",
      "index",
      Nil,
      "GET",
      """ An example controller showing a sample home page""",
      this.prefix + """"""
    )
  )

  // @LINE:8
  private[this] lazy val controllers_HomeController_index1_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("unicarb-db")))
  )
  private[this] lazy val controllers_HomeController_index1_invoker = createInvoker(
    HomeController_2.index(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.HomeController",
      "index",
      Nil,
      "GET",
      """GET     /                           controllers.Application.index()""",
      this.prefix + """unicarb-db"""
    )
  )

  // @LINE:9
  private[this] lazy val controllers_HomeController_index2_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("unicarb-db/")))
  )
  private[this] lazy val controllers_HomeController_index2_invoker = createInvoker(
    HomeController_2.index(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.HomeController",
      "index",
      Nil,
      "GET",
      """""",
      this.prefix + """unicarb-db/"""
    )
  )

  // @LINE:11
  private[this] lazy val controllers_HomeController_about3_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("about")))
  )
  private[this] lazy val controllers_HomeController_about3_invoker = createInvoker(
    HomeController_2.about(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.HomeController",
      "about",
      Nil,
      "GET",
      """""",
      this.prefix + """about"""
    )
  )

  // @LINE:13
  private[this] lazy val controllers_HomeController_listReferences4_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("references")))
  )
  private[this] lazy val controllers_HomeController_listReferences4_invoker = createInvoker(
    HomeController_2.listReferences(fakeValue[Int], fakeValue[String], fakeValue[String], fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.HomeController",
      "listReferences",
      Seq(classOf[Int], classOf[String], classOf[String], classOf[String]),
      "GET",
      """""",
      this.prefix + """references"""
    )
  )

  // @LINE:14
  private[this] lazy val controllers_HomeController_referenceDisplay5_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("references/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_HomeController_referenceDisplay5_invoker = createInvoker(
    HomeController_2.referenceDisplay(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.HomeController",
      "referenceDisplay",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """references/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:16
  private[this] lazy val controllers_HomeController_msData6_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("msData/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_HomeController_msData6_invoker = createInvoker(
    HomeController_2.msData(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.HomeController",
      "msData",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """msData/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:18
  private[this] lazy val controllers_HomeController_format7_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("format/"), DynamicPart("s", """[^/]+""",true)))
  )
  private[this] lazy val controllers_HomeController_format7_invoker = createInvoker(
    HomeController_2.format(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.HomeController",
      "format",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """format/""" + "$" + """s<[^/]+>"""
    )
  )

  // @LINE:20
  private[this] lazy val controllers_HomeController_library8_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("library")))
  )
  private[this] lazy val controllers_HomeController_library8_invoker = createInvoker(
    HomeController_2.library(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.HomeController",
      "library",
      Nil,
      "GET",
      """""",
      this.prefix + """library"""
    )
  )

  // @LINE:22
  private[this] lazy val controllers_SearchController_search9_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("search")))
  )
  private[this] lazy val controllers_SearchController_search9_invoker = createInvoker(
    SearchController_0.search(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.SearchController",
      "search",
      Nil,
      "GET",
      """""",
      this.prefix + """search"""
    )
  )

  // @LINE:23
  private[this] lazy val controllers_SearchController_searchAdvanced10_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("advanced")))
  )
  private[this] lazy val controllers_SearchController_searchAdvanced10_invoker = createInvoker(
    SearchController_0.searchAdvanced(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.SearchController",
      "searchAdvanced",
      Nil,
      "POST",
      """""",
      this.prefix + """advanced"""
    )
  )

  // @LINE:24
  private[this] lazy val controllers_SearchController_searchBasic11_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("basic")))
  )
  private[this] lazy val controllers_SearchController_searchBasic11_invoker = createInvoker(
    SearchController_0.searchBasic(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.SearchController",
      "searchBasic",
      Nil,
      "POST",
      """""",
      this.prefix + """basic"""
    )
  )

  // @LINE:25
  private[this] lazy val controllers_SearchController_searchMSMS12_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("peakMatching")))
  )
  private[this] lazy val controllers_SearchController_searchMSMS12_invoker = createInvoker(
    SearchController_0.searchMSMS(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.SearchController",
      "searchMSMS",
      Nil,
      "POST",
      """""",
      this.prefix + """peakMatching"""
    )
  )

  // @LINE:27
  private[this] lazy val controllers_GWPController_showImage13_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("showImage")))
  )
  private[this] lazy val controllers_GWPController_showImage13_invoker = createInvoker(
    GWPController_1.showImage(fakeValue[String], fakeValue[String], fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.GWPController",
      "showImage",
      Seq(classOf[String], classOf[String], classOf[String]),
      "GET",
      """""",
      this.prefix + """showImage"""
    )
  )

  // @LINE:28
  private[this] lazy val controllers_GWPController_showImageId14_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("showImageId")))
  )
  private[this] lazy val controllers_GWPController_showImageId14_invoker = createInvoker(
    GWPController_1.showImageId(fakeValue[Long], fakeValue[String], fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.GWPController",
      "showImageId",
      Seq(classOf[Long], classOf[String], classOf[String]),
      "GET",
      """""",
      this.prefix + """showImageId"""
    )
  )

  // @LINE:30
  private[this] lazy val controllers_HomeController_allStructuresToJson15_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/structures/json")))
  )
  private[this] lazy val controllers_HomeController_allStructuresToJson15_invoker = createInvoker(
    HomeController_2.allStructuresToJson(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.HomeController",
      "allStructuresToJson",
      Nil,
      "GET",
      """""",
      this.prefix + """api/structures/json"""
    )
  )

  // @LINE:31
  private[this] lazy val controllers_SearchController_searchStructures16_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("searchStructures/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_SearchController_searchStructures16_invoker = createInvoker(
    SearchController_0.searchStructures(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.SearchController",
      "searchStructures",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """searchStructures/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:33
  private[this] lazy val controllers_GenerateController_cellToJson17_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/cell/json/")))
  )
  private[this] lazy val controllers_GenerateController_cellToJson17_invoker = createInvoker(
    GenerateController_5.cellToJson(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.GenerateController",
      "cellToJson",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """api/cell/json/"""
    )
  )

  // @LINE:34
  private[this] lazy val controllers_GenerateController_originToJson18_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/origin/json/")))
  )
  private[this] lazy val controllers_GenerateController_originToJson18_invoker = createInvoker(
    GenerateController_5.originToJson(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.GenerateController",
      "originToJson",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """api/origin/json/"""
    )
  )

  // @LINE:35
  private[this] lazy val controllers_GenerateController_speciesToJson19_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/species/json/")))
  )
  private[this] lazy val controllers_GenerateController_speciesToJson19_invoker = createInvoker(
    GenerateController_5.speciesToJson(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.GenerateController",
      "speciesToJson",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """api/species/json/"""
    )
  )

  // @LINE:41
  private[this] lazy val controllers_HomeController_mirageReport20_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("mirageReport/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_HomeController_mirageReport20_invoker = createInvoker(
    HomeController_2.mirageReport(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.HomeController",
      "mirageReport",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """mirageReport/""" + "$" + """id<[^/]+>"""
    )
  )

  // @LINE:50
  private[this] lazy val controllers_Assets_versioned21_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("assets/"), DynamicPart("file", """.+""",false)))
  )
  private[this] lazy val controllers_Assets_versioned21_invoker = createInvoker(
    Assets_7.versioned(fakeValue[String], fakeValue[Asset]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Assets",
      "versioned",
      Seq(classOf[String], classOf[Asset]),
      "GET",
      """ Map static resources from the /public folder to the /assets URL path""",
      this.prefix + """assets/""" + "$" + """file<.+>"""
    )
  )

  // @LINE:52
  private[this] lazy val controllers_GenerateController_generate22_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("generate")))
  )
  private[this] lazy val controllers_GenerateController_generate22_invoker = createInvoker(
    GenerateController_5.generate(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.GenerateController",
      "generate",
      Nil,
      "GET",
      """""",
      this.prefix + """generate"""
    )
  )

  // @LINE:53
  private[this] lazy val controllers_GenerateController_generateMirage23_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("generate/"), DynamicPart("setup", """[^/]+""",true)))
  )
  private[this] lazy val controllers_GenerateController_generateMirage23_invoker = createInvoker(
    GenerateController_5.generateMirage(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.GenerateController",
      "generateMirage",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """generate/""" + "$" + """setup<[^/]+>"""
    )
  )

  // @LINE:54
  private[this] lazy val controllers_GenerateController_generateFile24_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("generatefile")))
  )
  private[this] lazy val controllers_GenerateController_generateFile24_invoker = createInvoker(
    GenerateController_5.generateFile(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.GenerateController",
      "generateFile",
      Nil,
      "GET",
      """""",
      this.prefix + """generatefile"""
    )
  )

  // @LINE:55
  private[this] lazy val controllers_GenerateController_generateForm25_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("generateform")))
  )
  private[this] lazy val controllers_GenerateController_generateForm25_invoker = createInvoker(
    GenerateController_5.generateForm(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.GenerateController",
      "generateForm",
      Nil,
      "GET",
      """""",
      this.prefix + """generateform"""
    )
  )

  // @LINE:56
  private[this] lazy val controllers_GenerateController_uploadForm26_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("generateformupload")))
  )
  private[this] lazy val controllers_GenerateController_uploadForm26_invoker = createInvoker(
    GenerateController_5.uploadForm(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.GenerateController",
      "uploadForm",
      Nil,
      "POST",
      """""",
      this.prefix + """generateformupload"""
    )
  )

  // @LINE:58
  private[this] lazy val controllers_UploadDataController_upload27_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("upload")))
  )
  private[this] lazy val controllers_UploadDataController_upload27_invoker = createInvoker(
    UploadDataController_6.upload(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.UploadDataController",
      "upload",
      Nil,
      "POST",
      """""",
      this.prefix + """upload"""
    )
  )

  // @LINE:59
  private[this] lazy val controllers_UploadDataController_uploadData28_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("uploaddata")))
  )
  private[this] lazy val controllers_UploadDataController_uploadData28_invoker = createInvoker(
    UploadDataController_6.uploadData(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.UploadDataController",
      "uploadData",
      Nil,
      "POST",
      """""",
      this.prefix + """uploaddata"""
    )
  )

  // @LINE:61
  private[this] lazy val controllers_Assets_at29_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("favicon.ico")))
  )
  private[this] lazy val controllers_Assets_at29_invoker = createInvoker(
    Assets_7.at(fakeValue[String], fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Assets",
      "at",
      Seq(classOf[String], classOf[String]),
      "GET",
      """""",
      this.prefix + """favicon.ico"""
    )
  )

  // @LINE:73
  private[this] lazy val controllers_HomeController_uploadIndex30_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("uploadData")))
  )
  private[this] lazy val controllers_HomeController_uploadIndex30_invoker = createInvoker(
    HomeController_2.uploadIndex(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.HomeController",
      "uploadIndex",
      Nil,
      "GET",
      """""",
      this.prefix + """uploadData"""
    )
  )

  // @LINE:76
  private[this] lazy val controllers_HomeController_profile31_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("profile")))
  )
  private[this] lazy val controllers_HomeController_profile31_invoker = createInvoker(
    HomeController_2.profile,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.HomeController",
      "profile",
      Nil,
      "GET",
      """""",
      this.prefix + """profile"""
    )
  )

  // @LINE:78
  private[this] lazy val controllers_HomeController_login32_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("login")))
  )
  private[this] lazy val controllers_HomeController_login32_invoker = createInvoker(
    HomeController_2.login,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.HomeController",
      "login",
      Nil,
      "GET",
      """""",
      this.prefix + """login"""
    )
  )

  // @LINE:79
  private[this] lazy val controllers_HomeController_doLogin33_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("login")))
  )
  private[this] lazy val controllers_HomeController_doLogin33_invoker = createInvoker(
    HomeController_2.doLogin,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.HomeController",
      "doLogin",
      Nil,
      "POST",
      """""",
      this.prefix + """login"""
    )
  )

  // @LINE:81
  private[this] lazy val com_feth_play_module_pa_controllers_Authenticate_logout34_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("logout")))
  )
  private[this] lazy val com_feth_play_module_pa_controllers_Authenticate_logout34_invoker = createInvoker(
    Authenticate_8.logout,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "com.feth.play.module.pa.controllers.Authenticate",
      "logout",
      Nil,
      "GET",
      """""",
      this.prefix + """logout"""
    )
  )

  // @LINE:82
  private[this] lazy val com_feth_play_module_pa_controllers_Authenticate_authenticate35_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("authenticate/"), DynamicPart("provider", """[^/]+""",true)))
  )
  private[this] lazy val com_feth_play_module_pa_controllers_Authenticate_authenticate35_invoker = createInvoker(
    Authenticate_8.authenticate(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "com.feth.play.module.pa.controllers.Authenticate",
      "authenticate",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """authenticate/""" + "$" + """provider<[^/]+>"""
    )
  )

  // @LINE:84
  private[this] lazy val controllers_HomeController_signup36_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("signup")))
  )
  private[this] lazy val controllers_HomeController_signup36_invoker = createInvoker(
    HomeController_2.signup,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.HomeController",
      "signup",
      Nil,
      "GET",
      """""",
      this.prefix + """signup"""
    )
  )

  // @LINE:85
  private[this] lazy val controllers_HomeController_doSignup37_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("signup")))
  )
  private[this] lazy val controllers_HomeController_doSignup37_invoker = createInvoker(
    HomeController_2.doSignup,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.HomeController",
      "doSignup",
      Nil,
      "POST",
      """""",
      this.prefix + """signup"""
    )
  )

  // @LINE:87
  private[this] lazy val controllers_Signup_unverified38_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("accounts/unverified")))
  )
  private[this] lazy val controllers_Signup_unverified38_invoker = createInvoker(
    Signup_3.unverified,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Signup",
      "unverified",
      Nil,
      "GET",
      """""",
      this.prefix + """accounts/unverified"""
    )
  )

  // @LINE:88
  private[this] lazy val controllers_Signup_oAuthDenied39_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("authenticate/"), DynamicPart("provider", """[^/]+""",true), StaticPart("/denied")))
  )
  private[this] lazy val controllers_Signup_oAuthDenied39_invoker = createInvoker(
    Signup_3.oAuthDenied(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Signup",
      "oAuthDenied",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """authenticate/""" + "$" + """provider<[^/]+>/denied"""
    )
  )

  // @LINE:90
  private[this] lazy val controllers_Signup_verify40_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("accounts/verify/"), DynamicPart("token", """[^/]+""",true)))
  )
  private[this] lazy val controllers_Signup_verify40_invoker = createInvoker(
    Signup_3.verify(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Signup",
      "verify",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """accounts/verify/""" + "$" + """token<[^/]+>"""
    )
  )

  // @LINE:91
  private[this] lazy val controllers_Signup_exists41_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("accounts/exists")))
  )
  private[this] lazy val controllers_Signup_exists41_invoker = createInvoker(
    Signup_3.exists,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Signup",
      "exists",
      Nil,
      "GET",
      """""",
      this.prefix + """accounts/exists"""
    )
  )

  // @LINE:93
  private[this] lazy val controllers_Signup_resetPassword42_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("accounts/password/reset/"), DynamicPart("token", """[^/]+""",true)))
  )
  private[this] lazy val controllers_Signup_resetPassword42_invoker = createInvoker(
    Signup_3.resetPassword(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Signup",
      "resetPassword",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """accounts/password/reset/""" + "$" + """token<[^/]+>"""
    )
  )

  // @LINE:94
  private[this] lazy val controllers_Signup_doResetPassword43_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("accounts/password/reset")))
  )
  private[this] lazy val controllers_Signup_doResetPassword43_invoker = createInvoker(
    Signup_3.doResetPassword,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Signup",
      "doResetPassword",
      Nil,
      "POST",
      """""",
      this.prefix + """accounts/password/reset"""
    )
  )

  // @LINE:96
  private[this] lazy val controllers_Account_changePassword44_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("accounts/password/change")))
  )
  private[this] lazy val controllers_Account_changePassword44_invoker = createInvoker(
    Account_4.changePassword,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Account",
      "changePassword",
      Nil,
      "GET",
      """""",
      this.prefix + """accounts/password/change"""
    )
  )

  // @LINE:97
  private[this] lazy val controllers_Account_doChangePassword45_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("accounts/password/change")))
  )
  private[this] lazy val controllers_Account_doChangePassword45_invoker = createInvoker(
    Account_4.doChangePassword,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Account",
      "doChangePassword",
      Nil,
      "POST",
      """""",
      this.prefix + """accounts/password/change"""
    )
  )

  // @LINE:99
  private[this] lazy val controllers_Account_verifyEmail46_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("accounts/verify")))
  )
  private[this] lazy val controllers_Account_verifyEmail46_invoker = createInvoker(
    Account_4.verifyEmail,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Account",
      "verifyEmail",
      Nil,
      "GET",
      """""",
      this.prefix + """accounts/verify"""
    )
  )

  // @LINE:101
  private[this] lazy val controllers_Account_link47_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("accounts/add")))
  )
  private[this] lazy val controllers_Account_link47_invoker = createInvoker(
    Account_4.link,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Account",
      "link",
      Nil,
      "GET",
      """""",
      this.prefix + """accounts/add"""
    )
  )

  // @LINE:103
  private[this] lazy val controllers_Account_askLink48_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("accounts/link")))
  )
  private[this] lazy val controllers_Account_askLink48_invoker = createInvoker(
    Account_4.askLink,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Account",
      "askLink",
      Nil,
      "GET",
      """""",
      this.prefix + """accounts/link"""
    )
  )

  // @LINE:104
  private[this] lazy val controllers_Account_doLink49_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("accounts/link")))
  )
  private[this] lazy val controllers_Account_doLink49_invoker = createInvoker(
    Account_4.doLink,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Account",
      "doLink",
      Nil,
      "POST",
      """""",
      this.prefix + """accounts/link"""
    )
  )

  // @LINE:106
  private[this] lazy val controllers_Account_askMerge50_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("accounts/merge")))
  )
  private[this] lazy val controllers_Account_askMerge50_invoker = createInvoker(
    Account_4.askMerge,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Account",
      "askMerge",
      Nil,
      "GET",
      """""",
      this.prefix + """accounts/merge"""
    )
  )

  // @LINE:107
  private[this] lazy val controllers_Account_doMerge51_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("accounts/merge")))
  )
  private[this] lazy val controllers_Account_doMerge51_invoker = createInvoker(
    Account_4.doMerge,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Account",
      "doMerge",
      Nil,
      "POST",
      """""",
      this.prefix + """accounts/merge"""
    )
  )

  // @LINE:109
  private[this] lazy val controllers_Signup_forgotPassword52_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("login/password/forgot")))
  )
  private[this] lazy val controllers_Signup_forgotPassword52_invoker = createInvoker(
    Signup_3.forgotPassword(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Signup",
      "forgotPassword",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """login/password/forgot"""
    )
  )

  // @LINE:110
  private[this] lazy val controllers_Signup_doForgotPassword53_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("login/password/forgot")))
  )
  private[this] lazy val controllers_Signup_doForgotPassword53_invoker = createInvoker(
    Signup_3.doForgotPassword,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Signup",
      "doForgotPassword",
      Nil,
      "POST",
      """""",
      this.prefix + """login/password/forgot"""
    )
  )


  def routes: PartialFunction[RequestHeader, Handler] = {
  
    // @LINE:6
    case controllers_HomeController_index0_route(params) =>
      call { 
        controllers_HomeController_index0_invoker.call(HomeController_2.index())
      }
  
    // @LINE:8
    case controllers_HomeController_index1_route(params) =>
      call { 
        controllers_HomeController_index1_invoker.call(HomeController_2.index())
      }
  
    // @LINE:9
    case controllers_HomeController_index2_route(params) =>
      call { 
        controllers_HomeController_index2_invoker.call(HomeController_2.index())
      }
  
    // @LINE:11
    case controllers_HomeController_about3_route(params) =>
      call { 
        controllers_HomeController_about3_invoker.call(HomeController_2.about())
      }
  
    // @LINE:13
    case controllers_HomeController_listReferences4_route(params) =>
      call(params.fromQuery[Int]("p", Some(0)), params.fromQuery[String]("s", Some("publication_year")), params.fromQuery[String]("o", Some("desc")), params.fromQuery[String]("f", Some(""))) { (p, s, o, f) =>
        controllers_HomeController_listReferences4_invoker.call(HomeController_2.listReferences(p, s, o, f))
      }
  
    // @LINE:14
    case controllers_HomeController_referenceDisplay5_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_HomeController_referenceDisplay5_invoker.call(HomeController_2.referenceDisplay(id))
      }
  
    // @LINE:16
    case controllers_HomeController_msData6_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_HomeController_msData6_invoker.call(HomeController_2.msData(id))
      }
  
    // @LINE:18
    case controllers_HomeController_format7_route(params) =>
      call(params.fromPath[String]("s", None)) { (s) =>
        controllers_HomeController_format7_invoker.call(HomeController_2.format(s))
      }
  
    // @LINE:20
    case controllers_HomeController_library8_route(params) =>
      call { 
        controllers_HomeController_library8_invoker.call(HomeController_2.library())
      }
  
    // @LINE:22
    case controllers_SearchController_search9_route(params) =>
      call { 
        controllers_SearchController_search9_invoker.call(SearchController_0.search())
      }
  
    // @LINE:23
    case controllers_SearchController_searchAdvanced10_route(params) =>
      call { 
        controllers_SearchController_searchAdvanced10_invoker.call(SearchController_0.searchAdvanced())
      }
  
    // @LINE:24
    case controllers_SearchController_searchBasic11_route(params) =>
      call { 
        controllers_SearchController_searchBasic11_invoker.call(SearchController_0.searchBasic())
      }
  
    // @LINE:25
    case controllers_SearchController_searchMSMS12_route(params) =>
      call { 
        controllers_SearchController_searchMSMS12_invoker.call(SearchController_0.searchMSMS())
      }
  
    // @LINE:27
    case controllers_GWPController_showImage13_route(params) =>
      call(params.fromQuery[String]("structure", None), params.fromQuery[String]("notation", None), params.fromQuery[String]("style", None)) { (structure, notation, style) =>
        controllers_GWPController_showImage13_invoker.call(GWPController_1.showImage(structure, notation, style))
      }
  
    // @LINE:28
    case controllers_GWPController_showImageId14_route(params) =>
      call(params.fromQuery[Long]("id", None), params.fromQuery[String]("notation", None), params.fromQuery[String]("style", None)) { (id, notation, style) =>
        controllers_GWPController_showImageId14_invoker.call(GWPController_1.showImageId(id, notation, style))
      }
  
    // @LINE:30
    case controllers_HomeController_allStructuresToJson15_route(params) =>
      call { 
        controllers_HomeController_allStructuresToJson15_invoker.call(HomeController_2.allStructuresToJson())
      }
  
    // @LINE:31
    case controllers_SearchController_searchStructures16_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_SearchController_searchStructures16_invoker.call(SearchController_0.searchStructures(id))
      }
  
    // @LINE:33
    case controllers_GenerateController_cellToJson17_route(params) =>
      call(params.fromQuery[String]("q", None)) { (q) =>
        controllers_GenerateController_cellToJson17_invoker.call(GenerateController_5.cellToJson(q))
      }
  
    // @LINE:34
    case controllers_GenerateController_originToJson18_route(params) =>
      call(params.fromQuery[String]("q", None)) { (q) =>
        controllers_GenerateController_originToJson18_invoker.call(GenerateController_5.originToJson(q))
      }
  
    // @LINE:35
    case controllers_GenerateController_speciesToJson19_route(params) =>
      call(params.fromQuery[String]("q", None)) { (q) =>
        controllers_GenerateController_speciesToJson19_invoker.call(GenerateController_5.speciesToJson(q))
      }
  
    // @LINE:41
    case controllers_HomeController_mirageReport20_route(params) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_HomeController_mirageReport20_invoker.call(HomeController_2.mirageReport(id))
      }
  
    // @LINE:50
    case controllers_Assets_versioned21_route(params) =>
      call(Param[String]("path", Right("/public")), params.fromPath[Asset]("file", None)) { (path, file) =>
        controllers_Assets_versioned21_invoker.call(Assets_7.versioned(path, file))
      }
  
    // @LINE:52
    case controllers_GenerateController_generate22_route(params) =>
      call { 
        controllers_GenerateController_generate22_invoker.call(GenerateController_5.generate())
      }
  
    // @LINE:53
    case controllers_GenerateController_generateMirage23_route(params) =>
      call(params.fromPath[String]("setup", None)) { (setup) =>
        controllers_GenerateController_generateMirage23_invoker.call(GenerateController_5.generateMirage(setup))
      }
  
    // @LINE:54
    case controllers_GenerateController_generateFile24_route(params) =>
      call { 
        controllers_GenerateController_generateFile24_invoker.call(GenerateController_5.generateFile())
      }
  
    // @LINE:55
    case controllers_GenerateController_generateForm25_route(params) =>
      call { 
        controllers_GenerateController_generateForm25_invoker.call(GenerateController_5.generateForm())
      }
  
    // @LINE:56
    case controllers_GenerateController_uploadForm26_route(params) =>
      call { 
        controllers_GenerateController_uploadForm26_invoker.call(GenerateController_5.uploadForm())
      }
  
    // @LINE:58
    case controllers_UploadDataController_upload27_route(params) =>
      call { 
        controllers_UploadDataController_upload27_invoker.call(UploadDataController_6.upload())
      }
  
    // @LINE:59
    case controllers_UploadDataController_uploadData28_route(params) =>
      call { 
        controllers_UploadDataController_uploadData28_invoker.call(UploadDataController_6.uploadData())
      }
  
    // @LINE:61
    case controllers_Assets_at29_route(params) =>
      call(Param[String]("path", Right("/public/images")), Param[String]("file", Right("favicon.ico"))) { (path, file) =>
        controllers_Assets_at29_invoker.call(Assets_7.at(path, file))
      }
  
    // @LINE:73
    case controllers_HomeController_uploadIndex30_route(params) =>
      call { 
        controllers_HomeController_uploadIndex30_invoker.call(HomeController_2.uploadIndex())
      }
  
    // @LINE:76
    case controllers_HomeController_profile31_route(params) =>
      call { 
        controllers_HomeController_profile31_invoker.call(HomeController_2.profile)
      }
  
    // @LINE:78
    case controllers_HomeController_login32_route(params) =>
      call { 
        controllers_HomeController_login32_invoker.call(HomeController_2.login)
      }
  
    // @LINE:79
    case controllers_HomeController_doLogin33_route(params) =>
      call { 
        controllers_HomeController_doLogin33_invoker.call(HomeController_2.doLogin)
      }
  
    // @LINE:81
    case com_feth_play_module_pa_controllers_Authenticate_logout34_route(params) =>
      call { 
        com_feth_play_module_pa_controllers_Authenticate_logout34_invoker.call(Authenticate_8.logout)
      }
  
    // @LINE:82
    case com_feth_play_module_pa_controllers_Authenticate_authenticate35_route(params) =>
      call(params.fromPath[String]("provider", None)) { (provider) =>
        com_feth_play_module_pa_controllers_Authenticate_authenticate35_invoker.call(Authenticate_8.authenticate(provider))
      }
  
    // @LINE:84
    case controllers_HomeController_signup36_route(params) =>
      call { 
        controllers_HomeController_signup36_invoker.call(HomeController_2.signup)
      }
  
    // @LINE:85
    case controllers_HomeController_doSignup37_route(params) =>
      call { 
        controllers_HomeController_doSignup37_invoker.call(HomeController_2.doSignup)
      }
  
    // @LINE:87
    case controllers_Signup_unverified38_route(params) =>
      call { 
        controllers_Signup_unverified38_invoker.call(Signup_3.unverified)
      }
  
    // @LINE:88
    case controllers_Signup_oAuthDenied39_route(params) =>
      call(params.fromPath[String]("provider", None)) { (provider) =>
        controllers_Signup_oAuthDenied39_invoker.call(Signup_3.oAuthDenied(provider))
      }
  
    // @LINE:90
    case controllers_Signup_verify40_route(params) =>
      call(params.fromPath[String]("token", None)) { (token) =>
        controllers_Signup_verify40_invoker.call(Signup_3.verify(token))
      }
  
    // @LINE:91
    case controllers_Signup_exists41_route(params) =>
      call { 
        controllers_Signup_exists41_invoker.call(Signup_3.exists)
      }
  
    // @LINE:93
    case controllers_Signup_resetPassword42_route(params) =>
      call(params.fromPath[String]("token", None)) { (token) =>
        controllers_Signup_resetPassword42_invoker.call(Signup_3.resetPassword(token))
      }
  
    // @LINE:94
    case controllers_Signup_doResetPassword43_route(params) =>
      call { 
        controllers_Signup_doResetPassword43_invoker.call(Signup_3.doResetPassword)
      }
  
    // @LINE:96
    case controllers_Account_changePassword44_route(params) =>
      call { 
        controllers_Account_changePassword44_invoker.call(Account_4.changePassword)
      }
  
    // @LINE:97
    case controllers_Account_doChangePassword45_route(params) =>
      call { 
        controllers_Account_doChangePassword45_invoker.call(Account_4.doChangePassword)
      }
  
    // @LINE:99
    case controllers_Account_verifyEmail46_route(params) =>
      call { 
        controllers_Account_verifyEmail46_invoker.call(Account_4.verifyEmail)
      }
  
    // @LINE:101
    case controllers_Account_link47_route(params) =>
      call { 
        controllers_Account_link47_invoker.call(Account_4.link)
      }
  
    // @LINE:103
    case controllers_Account_askLink48_route(params) =>
      call { 
        controllers_Account_askLink48_invoker.call(Account_4.askLink)
      }
  
    // @LINE:104
    case controllers_Account_doLink49_route(params) =>
      call { 
        controllers_Account_doLink49_invoker.call(Account_4.doLink)
      }
  
    // @LINE:106
    case controllers_Account_askMerge50_route(params) =>
      call { 
        controllers_Account_askMerge50_invoker.call(Account_4.askMerge)
      }
  
    // @LINE:107
    case controllers_Account_doMerge51_route(params) =>
      call { 
        controllers_Account_doMerge51_invoker.call(Account_4.doMerge)
      }
  
    // @LINE:109
    case controllers_Signup_forgotPassword52_route(params) =>
      call(params.fromQuery[String]("email", Some(""))) { (email) =>
        controllers_Signup_forgotPassword52_invoker.call(Signup_3.forgotPassword(email))
      }
  
    // @LINE:110
    case controllers_Signup_doForgotPassword53_route(params) =>
      call { 
        controllers_Signup_doForgotPassword53_invoker.call(Signup_3.doForgotPassword)
      }
  }
}
