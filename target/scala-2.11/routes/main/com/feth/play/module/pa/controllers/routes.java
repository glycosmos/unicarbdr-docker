
// @GENERATOR:play-routes-compiler
// @SOURCE:/app/conf/routes
// @DATE:Wed Jun 19 09:50:46 UTC 2019

package com.feth.play.module.pa.controllers;

import router.RoutesPrefix;

public class routes {
  
  public static final com.feth.play.module.pa.controllers.ReverseAuthenticate Authenticate = new com.feth.play.module.pa.controllers.ReverseAuthenticate(RoutesPrefix.byNamePrefix());

  public static class javascript {
    
    public static final com.feth.play.module.pa.controllers.javascript.ReverseAuthenticate Authenticate = new com.feth.play.module.pa.controllers.javascript.ReverseAuthenticate(RoutesPrefix.byNamePrefix());
  }

}
