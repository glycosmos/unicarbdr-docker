$(document).ready(function(){

    $("#unicarbTissue").show();
    $("#unicarbTaxonomy").hide();

  $("input[name='unicarbSearch']").change(function() {
    if ($(this).val() === "Tissue") { $("#unicarbTissue").show(); } else { $("#unicarbTissue").hide(); }
    if ($(this).val() === "Taxonomy") { $("#unicarbTaxonomy").show(); } else { $("#unicarbTaxonomy").hide(); }
  });


   function clickHandler(e) {

        // click is on the span elem
        var elem, evt = e ? e : event;
        if (evt.srcElement)  elem = evt.srcElement; else if (evt.target) elem = evt.target;


        // parent id the <a> with id
        var parent = elem.parentNode;
        var parentClass = parent.className;
        var elemClass = elem.className;

        if (parentClass === "toggle-info") {
            toggle(parent);
        } else {
            closeToggle();
        }
        return true;
   }

   function toggle(elem) {
        closeToggle();
        //        console.log(elem);

        var name = (elem.id).replace('toggle', 'more');
        //        console.log('element to toggle : ');
        //        console.log(name);

        var matchToToggle = document.querySelector("#" + name);
        //        console.log(matchToToggle);
        $(matchToToggle).toggle();

        return true;
   }

   function closeToggle() {

        var toClose = document.querySelectorAll("ul.more-toggle")


        //        console.log('element to hide : ');
        //hide all matches with a for loop
        for (var i = 0, len = toClose.length; i < len; i++) {
            $(toClose[i]).hide();
            //            console.log(toClose[i]);
        }
        return true;
   }

    document.onclick = clickHandler;

});