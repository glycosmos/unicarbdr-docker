name := """UniCarb-DB"""

version := "1.0-SNAPSHOT"

scalaVersion := "2.11.11"
//scalaVersion := "2.12.8"

lazy val root = (project in file(".")).enablePlugins(PlayJava)
lazy val myProject = (project in file(".")).enablePlugins(PlayJava, PlayEbean)


libraryDependencies ++= Seq(
  javaJdbc,
  cache,
  javaWs,
  "com.adrianhurt" %% "play-bootstrap" % "1.0-P25-B3",
  "be.objectify" %% "deadbolt-java" % "2.5.4",
  "com.feth" %% "play-authenticate" % "0.8.1",
  "com.feth" %% "play-easymail" % "0.8.1",
  "org.hibernate" % "hibernate-entitymanager" % "4.3.8.Final",
  "org.apache.poi" % "poi" % "3.8",
  "org.apache.poi" % "poi-ooxml" % "3.9",
  "org.postgresql" % "postgresql" % "9.4-1206-jdbc42"
)

//libraryDependencies += "postgresql" %  "postgresql" % "9.1-901-1.jdbc4"

routesGenerator := InjectedRoutesGenerator
fork in run := true

//mainClass in (Compile, run) := Some("main.scala.html")
