package src;

import models.MS.Software;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import service.*;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;


//__________________
import org.apache.poi.xssf.usermodel.*;

import org.apache.poi.openxml4j.opc.OPCPackage;


public class FileGenerator {

   /* public XSSFWorkbook createMirageWorkbook(GenerateMirageForm form) throws IOException, InvalidFormatException {

        // Fetch the Mirage file
        OPCPackage pkg = OPCPackage.open(new File("public/samplefiles/MIRAGE.xlsx"));
        XSSFWorkbook wb = new XSSFWorkbook(pkg);

        // Get the sheets
        List<Sheet> sheets = Arrays.asList(
                wb.getSheet("SamplePrep"),
                wb.getSheet("Software"),
                wb.getSheet("settingsForLCMS"),
                wb.getSheet("MS"),
                wb.getSheet("PostProcessing"),
                wb.getSheet("Annotation"),
                wb.getSheet("Structures")
        );

        // Get MS sheet
        Sheet ms = wb.getSheet("MS");

        // Get row, cell and set value for Instrument
        Row msInstrumentRow = ms.getRow(9);
        Cell manufacturerCell = msInstrumentRow.getCell(2);
        if (manufacturerCell == null)
            manufacturerCell = msInstrumentRow.createCell(2);
        //manufacturerCell.setCellValue(form.getManufacturer());

        // Get row, cell and set value for Model
        Row msModelRow = ms.getRow(11);
        Cell modelCell = msModelRow.getCell(2);
        if (modelCell == null)
            modelCell = msModelRow.createCell(2);
        //modelCell.setCellValue(form.getModel());

        // Hide none used dissociation types
        if (!form.isGenCID()){
            removeRows(wb, ms, 57,63);
        }
        if(!form.isGenECD()){
            removeRows(wb, ms,64, 69);
        }
        if (!form.isGenETD()){
            removeRows(wb, ms, 70, 74);
        }

        // Protect sheet
        for(Sheet sheet: sheets){
            sheet.protectSheet("UniCarb-DB");
        }

        return wb;
    }

    private void removeRows(XSSFWorkbook wb, Sheet ms, int startPos, int endPos){
        CellStyle lockStyle = wb.createCellStyle();
        lockStyle.setLocked(true);
        for (int i = startPos; i <= endPos; i++){
            Row row = ms.getRow(i);
            row.createCell(2).setCellStyle(lockStyle);
            row.setZeroHeight(true);
        }

    }*/

    public XSSFWorkbook createUniCarbFile(FormSample sampleForm, FormLC lcForm, FormMS msForm, FormTables tablesForm) throws InvalidFormatException, IOException {


        // Fetch the Mirage file
        OPCPackage pkg = OPCPackage.open(new File("public/samplefiles/MIRAGE.xlsx"));
        XSSFWorkbook wb = new XSSFWorkbook(pkg);

        // Get the sheets
        List<Sheet> sheets = new ArrayList<>();


        // Fill in the sheets
        if (sampleForm.getS_sampleName() != null){
            fillInSample(wb.getSheet("MIRAGE-SamplePrep"), sampleForm);
            sheets.add(wb.getSheet("MIRAGE-SamplePrep"));
        } else {
            int index = 0;
            Sheet sheet = wb.getSheet("MIRAGE-SamplePrep");
            if(sheet != null)   {
                index = wb.getSheetIndex(sheet);
                wb.removeSheetAt(index);
                wb.setActiveSheet(index);
            }
        }
        if (lcForm.getL_date() != null){
            fillInLC(wb.getSheet("LC-PartOfMS"), lcForm);
            sheets.add(wb.getSheet("LC-PartOfMS"));
        } else {
            int index = 0;
            Sheet sheet = wb.getSheet("LC-PartOfMS");
            if(sheet != null)   {
                index = wb.getSheetIndex(sheet);
                wb.removeSheetAt(index);
                wb.setActiveSheet(index);
            }
        }
        if (msForm.getGwpFiles() != null){
            fillInMS(wb.getSheet("MIRAGE-MS"), msForm);
            sheets.add(wb.getSheet("MIRAGE-MS"));
            sheets.add(wb.getSheet("MIRAGE-MSn"));
            if (!msForm.getM_databaseQueried().equals("")){
                fillInMSn(wb.getSheet("MIRAGE-MSn"), msForm);
            }
        } else {
            int index = 0;
            Sheet sheet = wb.getSheet("MIRAGE-MS");
            if(sheet != null)   {
                index = wb.getSheetIndex(sheet);
                wb.removeSheetAt(index);
                wb.setActiveSheet(index);
            }
            sheet = wb.getSheet("MIRAGE-MSn");
            if(sheet != null)   {
                wb.setSheetOrder("MIRAGE-MSn", 0);
                index = wb.getSheetIndex(sheet);
                wb.removeSheetAt(index);
                wb.setActiveSheet(index);
            }
        }


        // Protect sheets
        for(Sheet sheet: sheets){
            sheet.protectSheet("UniCarb-DB");
        }

        return wb;
    }


    private void fillInSample(Sheet sheet, FormSample form){

        sheet.getRow(7).getCell(2).setCellValue(form.getS_date());
        sheet.getRow(8).getCell(2).setCellValue(form.getS_responsiblePerson());
        sheet.getRow(9).getCell(2).setCellValue(form.getS_affiliation());
        sheet.getRow(10).getCell(2).setCellValue(form.getS_contactInformation());
        sheet.getRow(13).getCell(2).setCellValue(form.getS_generalInfo());
        sheet.getRow(16).getCell(2).setCellValue(form.getS_cellType());
        sheet.getRow(17).getCell(2).setCellValue(form.getS_harvestConditions());
        sheet.getRow(19).getCell(2).setCellValue(form.getS_origin());
        sheet.getRow(20).getCell(2).setCellValue(form.getS_species());
        sheet.getRow(21).getCell(2).setCellValue(form.getS_storageConditions());
        sheet.getRow(22).getCell(2).setCellValue(form.getS_glycoprotein());
        sheet.getRow(24).getCell(2).setCellValue(form.getS_vendor());
        sheet.getRow(26).getCell(2).setCellValue(form.getS_SynthesisSteps());
        sheet.getRow(27).getCell(2).setCellValue(form.getS_startingMaterial());
        sheet.getRow(47).getCell(2).setCellValue(form.getS_purification());
        sheet.getRow(49).getCell(2).setCellValue(form.getS_sampleName());

        setValuesFromList(sheet,31,form.getIsoOneEnzymes());
        setValuesFromList(sheet,32,form.getIsoOneVendor());
        setValuesFromList(sheet,33,form.getIsoOneConditions());

        setValuesFromList(sheet,35,form.getIsoTwoChemicals());
        setValuesFromList(sheet,36,form.getIsoTwoConditions());

        setValuesFromList(sheet,39,form.getModOneEnzymes());
        setValuesFromList(sheet,40,form.getModOneConditions());
        setValuesFromList(sheet,41,form.getModOneCOrigin());

        setValuesFromList(sheet,43,form.getModTwoChemicals());
        setValuesFromList(sheet,44,form.getModTwoModification());
        setValuesFromList(sheet,45,form.getModTwoConditions());
    }


    private void fillInLC(Sheet sheet, FormLC form){

        sheet.getRow(7).getCell(2).setCellValue(form.getL_date());
        sheet.getRow(8).getCell(2).setCellValue(form.getL_responsiblePerson());
        sheet.getRow(9).getCell(2).setCellValue(form.getL_affiliation());
        sheet.getRow(10).getCell(2).setCellValue(form.getL_contactInformation());

        sheet.getRow(12).getCell(2).setCellValue(form.getL_hplcManufacturer());
        sheet.getRow(13).getCell(2).setCellValue(form.getL_hplcBrand());
        sheet.getRow(14).getCell(2).setCellValue(form.getL_injector());
        sheet.getRow(15).getCell(2).setCellValue(form.getL_injectorSettings());

        sheet.getRow(17).getCell(2).setCellValue(form.getL_temp());
        sheet.getRow(18).getCell(2).setCellValue(form.getL_solventA());
        sheet.getRow(19).getCell(2).setCellValue(form.getL_solventB());
        sheet.getRow(20).getCell(2).setCellValue(form.getL_solventD());
        sheet.getRow(21).getCell(2).setCellValue(form.getL_flowRate());
        sheet.getRow(22).getCell(2).setCellValue(form.getL_flowGradient());
        sheet.getRow(23).getCell(2).setCellValue(form.getL_runtime());
        sheet.getRow(24).getCell(2).setCellValue(form.getL_phase());

        sheet.getRow(26).getCell(2).setCellValue(form.getL_manufacturer());
        sheet.getRow(27).getCell(2).setCellValue(form.getL_model());
        sheet.getRow(28).getCell(2).setCellValue(form.getL_chromatography());
        sheet.getRow(29).getCell(2).setCellValue(form.getL_material());
        sheet.getRow(30).getCell(2).setCellValue(form.getL_columnDiameter());
        sheet.getRow(31).getCell(2).setCellValue(form.getL_columnLength());
        sheet.getRow(32).getCell(2).setCellValue(form.getL_particleSize());
    }


    private void fillInMS(Sheet sheet, FormMS form){

        sheet.getRow(8).getCell(2).setCellValue(form.getM_date());
        sheet.getRow(9).getCell(2).setCellValue(form.getM_responsiblePerson());
        sheet.getRow(10).getCell(2).setCellValue(form.getM_affiliation());
        sheet.getRow(11).getCell(2).setCellValue(form.getM_contactInformation());
        sheet.getRow(12).getCell(2).setCellValue(form.getM_manufacturer());
        sheet.getRow(13).getCell(2).setCellValue(form.getM_model());
        sheet.getRow(14).getCell(2).setCellValue(form.getM_customizations());
        sheet.getRow(15).getCell(2).setCellValue(form.getM_ionMode());

        setValuesFromList(sheet,17,form.getSoftwareOneName());
        setValuesFromList(sheet,18,form.getSoftwareOneVersion());
        //setValuesFromList(sheet,19,form.getSoftwareOneUpgrades());
        setManufacturerAndAvailabilitySoftwareOne(sheet, form.getSoftwareOneName());
        //setValuesFromList(sheet,20,form.getSoftwareOneSwitchingCriteria());
        setValuesFromList(sheet,21,form.getSoftwareOneUpgrades());
        setValuesFromList(sheet,22,form.getSoftwareOneSwitchingCriteria());
        setValuesFromList(sheet,23,form.getSoftwareOneIsolation());
        setValuesFromList(sheet,24,form.getSoftwareOneFile());

        sheet.getRow(27).getCell(2).setCellValue(form.getM_supplyTypeESI());
        sheet.getRow(28).getCell(2).setCellValue(form.getM_interfaceNameESI());
        sheet.getRow(29).getCell(2).setCellValue(form.getM_interfaceDescriptionESI());
        sheet.getRow(30).getCell(2).setCellValue(form.getM_sprayerNameESI());
        sheet.getRow(31).getCell(2).setCellValue(form.getM_sprayerDescriptionESI());
        sheet.getRow(32).getCell(2).setCellValue(form.getM_voltagesESI());
        sheet.getRow(33).getCell(2).setCellValue(form.getM_evaluatedFragmentationESI());
        sheet.getRow(34).getCell(2).setCellValue(form.getM_inSourceDissociationESI());
        sheet.getRow(35).getCell(2).setCellValue(form.getM_otherParametersESI());

        sheet.getRow(37).getCell(2).setCellValue(form.getM_PlateCompositionMALDI());
        sheet.getRow(38).getCell(2).setCellValue(form.getM_matrixCompositionMALDI());
        sheet.getRow(39).getCell(2).setCellValue(form.getM_depositionTechniqueMALDI());
        sheet.getRow(40).getCell(2).setCellValue(form.getM_voltagesMALDI());
        sheet.getRow(41).getCell(2).setCellValue(form.getM_evaluatedFragmentationMALDI());
        sheet.getRow(42).getCell(2).setCellValue(form.getM_psdMALDI());
        sheet.getRow(43).getCell(2).setCellValue(form.getM_delayedExtractionMALDI());
        sheet.getRow(44).getCell(2).setCellValue(form.getM_laserMALDI());
        sheet.getRow(45).getCell(2).setCellValue(form.getM_otherParmasMALDI());

        sheet.getRow(47).getCell(2).setCellValue(form.getM_hardwareOptions());

        sheet.getRow(50).getCell(2).setCellValue(form.getM_cidGasComposition());
        sheet.getRow(51).getCell(2).setCellValue(form.getM_cidGasPressure());
        sheet.getRow(52).getCell(2).setCellValue(form.getM_cidCollisionEnergy());

        sheet.getRow(54).getCell(2).setCellValue(form.getM_etdReagentGas());
        sheet.getRow(55).getCell(2).setCellValue(form.getM_etdPressure());
        sheet.getRow(56).getCell(2).setCellValue(form.getM_etdReactionTime());
        sheet.getRow(57).getCell(2).setCellValue(form.getM_etdNumberOfAtoms());

        sheet.getRow(59).getCell(2).setCellValue(form.getM_ecdEmitterType());
        sheet.getRow(60).getCell(2).setCellValue(form.getM_ecdVoltage());
        sheet.getRow(61).getCell(2).setCellValue(form.getM_ecdCurrent());

        sheet.getRow(63).getCell(2).setCellValue(form.getM_reflectronStatus());

        sheet.getRow(65).getCell(2).setCellValue(form.getM_finalMsStage());

        sheet.getRow(67).getCell(2).setCellValue(form.getM_ionMobilityGas());
        sheet.getRow(68).getCell(2).setCellValue(form.getM_ionMobilityPressure());
        sheet.getRow(69).getCell(2).setCellValue(form.getM_ionMobilityInstrumentParameters());


        sheet.getRow(71).getCell(2).setCellValue(form.getM_ftIcrPeakSelection());
        sheet.getRow(72).getCell(2).setCellValue(form.getM_ftIcrPulse());
        sheet.getRow(73).getCell(2).setCellValue(form.getM_ftIcrWidth());
        sheet.getRow(74).getCell(2).setCellValue(form.getM_ftIcrVoltage());
        sheet.getRow(75).getCell(2).setCellValue(form.getM_ftIcrDecayTime());
        sheet.getRow(76).getCell(2).setCellValue(form.getM_ftIcrIR());
        sheet.getRow(77).getCell(2).setCellValue(form.getM_ftIcrOtherParameters());

        sheet.getRow(79).getCell(2).setCellValue(form.getM_detectorType());

        List<String> s1ManufactureList = new ArrayList<>();
        List<String> s1AvailabilityList = new ArrayList<>();
        for (String software : form.getSoftwareTwoName()){
            Software s = Software.find.where().ieq("name_software",software).findUnique();
            s1ManufactureList.add(s.manufacturer);
            s1AvailabilityList.add(s.availability);
        }
        setSoftwareListValues(sheet,82, form.getSoftwareTwoName(), form.getSoftwareThreeSoftware(),Arrays.asList(form.getSoftwareTwoVersion(),s1ManufactureList, s1AvailabilityList));


        setValuesFromList(sheet,88,form.getSoftwareThreeName());
        setValuesFromList(sheet,89,form.getSoftwareThreeFormat());
        setValuesFromList(sheet,90,form.getSoftwareThreeArea());
        setValuesFromList(sheet,91,form.getSoftwareThreeURL());

        List<String> s2ManufactureList = new ArrayList<>();
        List<String> s2AvailabilityList = new ArrayList<>();
        for (String software : form.getSoftwareFourSoftware()){
            Software s = Software.find.where().ieq("name_software",software).findUnique();
            s2ManufactureList.add(s.manufacturer);
            s2AvailabilityList.add(s.availability);
        }
        setSoftwareListValues(sheet, 94, form.getSoftwareFourSoftware(), form.getSoftwareFiveSoftware(), Arrays.asList(form.getSoftwareFourVersion(),s2ManufactureList,s2AvailabilityList,form.getSoftwareFourCustomizations(),form.getSoftwareFourSettings()));


        setValuesFromList(sheet,101,form.getSoftwareFiveName());
        setValuesFromList(sheet,102,form.getSoftwareFiveFormat());
        setValuesFromList(sheet,103,form.getSoftwareFiveURL());


        sheet.getRow(104).getCell(2).setCellValue(form.getM_acquisitionNumber());
        sheet.getRow(105).getCell(2).setCellValue(form.getM_peakLists());
        sheet.getRow(106).getCell(2).setCellValue(form.getM_rawDataScoring());
        sheet.getRow(107).getCell(2).setCellValue(form.getM_smoothing());
        sheet.getRow(108).getCell(2).setCellValue(form.getM_backgroundThreshold());
        sheet.getRow(109).getCell(2).setCellValue(form.getM_signalToNoise());
        sheet.getRow(110).getCell(2).setCellValue(form.getM_peakHeight());
        sheet.getRow(111).getCell(2).setCellValue(form.getM_retentionTimes());
        sheet.getRow(112).getCell(2).setCellValue(form.getM_intensityValues());


        List<String> s3ManufactureList = new ArrayList<>();
        List<String> s3AvailabilityList = new ArrayList<>();
        for (String software : form.getSoftwareFourSoftware()){
            Software s = Software.find.where().ieq("name_software",software).findUnique();
            s3ManufactureList.add(s.manufacturer);
            s3AvailabilityList.add(s.availability);
        }
        setSoftwareListValues(sheet, 115, form.getSoftwareSixName(), form.getSoftwareSevenSoftware(), Arrays.asList(form.getSoftwareSixVersion(),s3ManufactureList,s3AvailabilityList,form.getSoftwareSixType(),form.getSoftwareSixCustomizations(), form.getSoftwareSixSettings()));

        setValuesFromList(sheet,123,form.getSoftwareSevenName());
        setValuesFromList(sheet,124,form.getSoftwareSevenFormat());
        setValuesFromList(sheet,125,form.getSoftwareSevenURL());

        sheet.getRow(128).getCell(2).setCellValue(form.getM_databaseQueried());
        sheet.getRow(129).getCell(2).setCellValue(form.getM_taxonomicalRestrictions());
        sheet.getRow(130).getCell(2).setCellValue(form.getM_otherRestrictions());
        sheet.getRow(131).getCell(2).setCellValue(form.getM_allowedCleavages());
        sheet.getRow(132).getCell(2).setCellValue(form.getM_massAccuracy());
        sheet.getRow(133).getCell(2).setCellValue(form.getM_parentError());
        sheet.getRow(134).getCell(2).setCellValue(form.getM_fragmentError());
        sheet.getRow(135).getCell(2).setCellValue(form.getM_scoringMethod());
        sheet.getRow(136).getCell(2).setCellValue(form.getM_scoringValueFormat());
        sheet.getRow(137).getCell(2).setCellValue(form.getM_scoringAlgorithm());
        sheet.getRow(138).getCell(2).setCellValue(form.getM_scoringResult());

        sheet.getRow(140).getCell(2).setCellValue(form.getM_validationStatus());
        sheet.getRow(141).getCell(2).setCellValue(form.getM_validationMethod());
        sheet.getRow(142).getCell(2).setCellValue(form.getM_validationResult());
    }


    private void fillInMSn(Sheet sheet, FormMS form){
        for (int i = 8; i < 109; i++){
            Row row = sheet.getRow(i);

            if (row.getCell(8) == null) {
                row.createCell(8).setCellValue(form.getM_databaseQueried());
            } else {
                row.getCell(8).setCellValue(form.getM_databaseQueried());
            }
            if (row.getCell(9) == null) {
                row.createCell(9).setCellValue(form.getM_taxonomicalRestrictions());
            } else {
                row.getCell(9).setCellValue(form.getM_taxonomicalRestrictions());
            }
            if (row.getCell(10) == null) {
                row.createCell(10).setCellValue(form.getM_otherRestrictions());
            } else {
                row.getCell(10).setCellValue(form.getM_otherRestrictions());
            }
            if (row.getCell(11) == null) {
                row.createCell(11).setCellValue(form.getM_allowedCleavages());
            } else {
                row.getCell(11).setCellValue(form.getM_allowedCleavages());
            }
            if (row.getCell(12) == null) {
                row.createCell(12).setCellValue(form.getM_parentError());
            } else {
                row.getCell(12).setCellValue(form.getM_parentError());
            }
            if (row.getCell(13) == null) {
                row.createCell(13).setCellValue(form.getM_fragmentError());
            } else {
                row.getCell(13).setCellValue(form.getM_fragmentError());
            }
            if (row.getCell(14) == null) {
                row.createCell(14).setCellValue(form.getM_scoringMethod());
            } else {
                row.getCell(14).setCellValue(form.getM_scoringMethod());
            }
            if (row.getCell(15) == null) {
                row.createCell(15).setCellValue(form.getM_scoringValueFormat());
            } else {
                row.getCell(15).setCellValue(form.getM_scoringValueFormat());
            }
            if (row.getCell(16) == null) {
                row.createCell(16).setCellValue(form.getM_scoringAlgorithm());
            } else {
                row.getCell(16).setCellValue(form.getM_scoringAlgorithm());
            }
            if (row.getCell(17) == null) {
                row.createCell(17).setCellValue(form.getM_scoringResult());
            } else {
                row.getCell(17).setCellValue(form.getM_scoringResult());
            }
            if (row.getCell(18) == null) {
                row.createCell(18).setCellValue(form.getM_validationStatus());
            } else {
                row.getCell(18).setCellValue(form.getM_validationStatus());
            }
            if (row.getCell(19) == null) {
                row.createCell(19).setCellValue(form.getM_validationMethod());
            } else {
                row.getCell(19).setCellValue(form.getM_validationMethod());
            }
            if (row.getCell(20) == null) {
                row.createCell(20).setCellValue(form.getM_validationResult());
            } else {
                row.getCell(20).setCellValue(form.getM_validationResult());
            }
        }
    }

    // Method for adding a list of values to a row
    private void setValuesFromList(Sheet sheet, int row, List<String> list){
        for(int i = 0; i < list.size(); i++){
            sheet.getRow(row).getCell(2 + i).setCellValue(list.get(i));
        }
    }

    // Method for adding software lists
    private void setSoftwareListValues(Sheet sheet, int row, List<String> softwareList, List<String> fileList, List<List<String>> lists){
        if(fileList != null) {
            for (int i = 0; i < fileList.size(); i++) {
                for (int s = 0; s < softwareList.size(); s++) {
                    if (softwareList.get(s).equals(fileList.get(i))) {
//                        sheet.getRow(row).getCell(2 + i).setCellValue(softwareList.get(s));
                        if(sheet.getRow(row).getCell(2 + i)!=null)
                            sheet.getRow(row).getCell(2 + i).setCellValue(softwareList.get(s));
                        else
                            sheet.getRow(row).createCell(2 + i).setCellValue(softwareList.get(s));



                        for (int l = 0; l < lists.size(); l++) {
                            if (lists.get(l).size() > s) {
//                                sheet.getRow(row + 1 + l).getCell(2 + i).setCellValue(lists.get(l).get(s));
                                if(sheet.getRow(row + 1 + l).getCell(2 + i)!=null)
                                    sheet.getRow(row + 1 + l).getCell(2 + i).setCellValue(lists.get(l).get(s));
                                else
                                    sheet.getRow(row + 1 + l).createCell(2 + i).setCellValue(lists.get(l).get(s));


                            }
                        }
                    }
                }
            }


            int col = fileList.size();
            for (int i = 0; i < softwareList.size(); i++) {
                if (!fileList.contains(softwareList.get(i))) {
//                    sheet.getRow(row).getCell(2 + col).setCellValue(softwareList.get(i));
                    if(sheet.getRow(row).getCell(2 + col)!=null)
                        sheet.getRow(row).getCell(2 + col).setCellValue(softwareList.get(i));
                    else
                        sheet.getRow(row).createCell(2 + col).setCellValue(softwareList.get(i));


                    for (int l = 0; l < lists.size(); l++) {
                        if (lists.get(l).size() > i) {
//                            sheet.getRow(row + 1 + l).getCell(2 + col).setCellValue(lists.get(l).get(i));
                            if(sheet.getRow(row + 1 + l).getCell(2 + col)!=null)
                                sheet.getRow(row + 1 + l).getCell(2 + col).setCellValue(lists.get(l).get(i));
                            else
                                sheet.getRow(row + 1 + l).createCell(2 + col).setCellValue(lists.get(l).get(i));


                        }
                    }
                    col++;
                }
            }
        }
    }

    private void setManufacturerAndAvailabilitySoftwareOne(Sheet sheet, List<String> list){
        for(int i = 0; i < list.size(); i++){
            sheet.getRow(19).getCell(2 + i).setCellValue(Software.find.where().ieq("name_software", list.get(i)).findUnique().manufacturer);
            sheet.getRow(20).getCell(2 + i).setCellValue(Software.find.where().ieq("name_software", list.get(i)).findUnique().availability);
        }
    }

    // Method for adding data to tables
    private void tableValues(Sheet sheet, int startRow, int endRow, int columns, List<List<String>> valueList){

        int index = 0;
        for (int i = startRow; i < endRow; i++){
            Row row = sheet.getRow(i);

            for (int c = 1; c <= columns; c++){
                if(valueList.get(c - 1) != null && index < valueList.get(c - 1).size()){
                    row.getCell(c).setCellValue(valueList.get(c - 1).get(index));
                }
            }
            index++;
        }
    }
}
