package src;


public class PairWrapper {

    private Integer id;
    private Long glycanSeqId;


    public PairWrapper(Integer id,Long gSeqId) {
        this.id = id;
        this.glycanSeqId = gSeqId;
    }

    public Integer getid() { return this.id; }
    public Long getGlycanSeqId() { return this.glycanSeqId; }
}
