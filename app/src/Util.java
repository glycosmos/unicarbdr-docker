package src;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Util {

    public static <T> List<T> unique (List<T> listItems)
    {
        Set<T> hset = new HashSet<>(listItems);
        List<T> list = new ArrayList<>(hset);
        return list;
    }

    private static boolean isNumeric(String str)
    {
        try
        {
            double d = Double.parseDouble(str);
        }
        catch(NumberFormatException nfe)
        {
            return false;
        }
        return true;
    }


    public static Double getDoubleParam(String param){
        Double newParam=-.01;
        if(!param.isEmpty() && Util.isNumeric(param))
            newParam=Double.valueOf(param);

        return newParam;
    }

    public static double formatError(double parentMass, String typeError, double error) {
        switch (typeError) {
            case "ppm":
                return parentMass*(error/(10^6));
            default:
                return error;
        }
    }


    



}
