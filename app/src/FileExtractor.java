
package src;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.*;
import org.apache.poi.openxml4j.opc.OPCPackage;
import service.*;

import java.io.File;
import java.io.IOException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.*;

public class FileExtractor {
    private String filePath;
    XSSFWorkbook wb;
    Format formatter;

    public FileExtractor(){}

    public FileExtractor(String filePath) throws InvalidFormatException, IOException {
        this.filePath = filePath;

        // Fetch the Mirage file
        OPCPackage pkg = OPCPackage.open(new File(filePath));
        wb = new XSSFWorkbook(pkg);
        formatter = new SimpleDateFormat("yyyy-MM-dd");
    }



    // --- Methods for moving data from Excel sheets to form classes ---

    public FormSample getSampleForm(){
        FormSample form = new FormSample();
        Sheet sheet = wb.getSheet("MIRAGE-SamplePrep");

        if (sheet != null) {

            if (sheet.getRow(7).getCell(2) != null && Cell.CELL_TYPE_NUMERIC == sheet.getRow(7).getCell(2).getCellType()) {
                form.setS_date(formatter.format(sheet.getRow(7).getCell(2).getDateCellValue()));
            } else {
                form.setS_date(sheet.getRow(7).getCell(2).getStringCellValue());
            }
            form.setS_responsiblePerson(getCellValue(sheet, 8));
            form.setS_affiliation(getCellValue(sheet, 9));
            form.setS_contactInformation(getCellValue(sheet, 10));
            form.setS_generalInfo(getCellValue(sheet, 13));
            form.setS_cellType(getCellValue(sheet, 16));
            form.setS_harvestConditions(getCellValue(sheet, 17));
            form.setS_origin(getCellValue(sheet, 19));
            form.setS_species(getCellValue(sheet, 20));

            form.setS_storageConditions(getCellValue(sheet, 21));
            form.setS_glycoprotein(getCellValue(sheet, 22));
            form.setS_vendor(getCellValue(sheet, 24));
            form.setS_SynthesisSteps(getCellValue(sheet, 26));
            form.setS_startingMaterial(getCellValue(sheet, 27));
            form.setS_purification(getCellValue(sheet, 47));
            form.setS_sampleName(getCellValue(sheet, 49));

            form.setIsoOneEnzymes(multiValueRow(sheet, 31));
            form.setIsoOneVendor(multiValueRow(sheet, 32, form.getIsoOneEnzymes().size()));
            form.setIsoOneConditions(multiValueRow(sheet, 33, form.getIsoOneEnzymes().size()));

            form.setIsoTwoChemicals(multiValueRow(sheet, 35));
            form.setIsoTwoConditions(multiValueRow(sheet, 36, form.getIsoTwoChemicals().size()));

            form.setModOneEnzymes(multiValueRow(sheet, 39));
            form.setModOneConditions(multiValueRow(sheet, 40, form.getModOneEnzymes().size()));
            form.setModOneCOrigin(multiValueRow(sheet, 41, form.getModOneEnzymes().size()));

            form.setModTwoChemicals(multiValueRow(sheet, 43));
            form.setModTwoModification(multiValueRow(sheet, 44, form.getModTwoChemicals().size()));
            form.setModTwoConditions(multiValueRow(sheet, 45, form.getModTwoChemicals().size()));
        }
        return form;
    }


    public FormLC getLC(){
        FormLC form = new FormLC();
        Sheet sheet = wb.getSheet("LC-PartOfMS");
        if (sheet != null) {

            if (sheet.getRow(7).getCell(2) != null && Cell.CELL_TYPE_NUMERIC == sheet.getRow(7).getCell(2).getCellType()) {
                form.setL_date(formatter.format(sheet.getRow(7).getCell(2).getDateCellValue()));
            } else {
                form.setL_date(sheet.getRow(7).getCell(2).getStringCellValue());
            }
            form.setL_responsiblePerson(getCellValue(sheet, 8));
            form.setL_affiliation(getCellValue(sheet, 9));
            form.setL_contactInformation(getCellValue(sheet, 10));

            form.setL_hplcManufacturer(getCellValue(sheet, 12));
            form.setL_hplcBrand(getCellValue(sheet, 13));
            form.setL_injector(getCellValue(sheet, 14));
            form.setL_injectorSettings(getCellValue(sheet, 15));

            form.setL_temp(getCellValue(sheet, 17));
            form.setL_solventA(getCellValue(sheet, 18));
            form.setL_solventB(getCellValue(sheet, 19));
            form.setL_solventD(getCellValue(sheet, 20));
            form.setL_flowRate(getCellValue(sheet, 21));
            form.setL_flowGradient(getCellValue(sheet, 22));
            form.setL_runtime(getCellValue(sheet, 23));
            form.setL_phase(getCellValue(sheet, 24));

            form.setL_manufacturer(getCellValue(sheet, 26));
            form.setL_model(getCellValue(sheet, 27));
            form.setL_chromatography(getCellValue(sheet, 28));
            form.setL_material(getCellValue(sheet, 29));
            form.setL_columnDiameter(getCellValue(sheet, 30));
            form.setL_columnLength(getCellValue(sheet, 31));
            form.setL_particleSize(getCellValue(sheet, 32));
        }
        return form;
    }


    public FormMS getMS(){
        FormMS form = new FormMS();
        Sheet sheet = wb.getSheet("MIRAGE-MS");
        if (sheet != null) {

            if (sheet.getRow(8).getCell(2) != null && Cell.CELL_TYPE_NUMERIC == sheet.getRow(8).getCell(2).getCellType()) {
                form.setM_date(formatter.format(sheet.getRow(8).getCell(2).getDateCellValue()));
            } else {
                form.setM_date(sheet.getRow(8).getCell(2).getStringCellValue());
            }

            form.setM_responsiblePerson(getCellValue(sheet, 9));
            form.setM_affiliation(getCellValue(sheet, 10));
            form.setM_contactInformation(getCellValue(sheet, 11));
            form.setM_manufacturer(getCellValue(sheet, 12));
            form.setM_model(getCellValue(sheet, 13));
            form.setM_customizations(getCellValue(sheet, 14));
            form.setM_ionMode(getCellValue(sheet, 15));

            form.setSoftwareOneName(multiValueRow(sheet, 17));
            form.setSoftwareOneVersion(multiValueRow(sheet, 18, form.getSoftwareOneName().size()));
            form.setSoftwareOneUpgrades(multiValueRow(sheet, 21, form.getSoftwareOneName().size()));
            form.setSoftwareOneSwitchingCriteria(multiValueRow(sheet, 22, form.getSoftwareOneName().size()));
            form.setSoftwareOneIsolation(multiValueRow(sheet, 23, form.getSoftwareOneName().size()));
            form.setSoftwareOneFile(multiValueRow(sheet, 24, form.getSoftwareOneName().size()));

            form.setM_supplyTypeESI(getCellValue(sheet, 27));
            form.setM_interfaceNameESI(getCellValue(sheet, 28));
            form.setM_interfaceDescriptionESI(getCellValue(sheet, 29));
            form.setM_sprayerNameESI(getCellValue(sheet, 30));
            form.setM_sprayerDescriptionESI(getCellValue(sheet, 31));
            form.setM_voltagesESI(getCellValue(sheet, 32));
            form.setM_evaluatedFragmentationESI(getCellValue(sheet, 33));
            form.setM_inSourceDissociationESI(getCellValue(sheet, 34));
            form.setM_otherParametersESI(getCellValue(sheet, 35));
            form.setM_CheckESI(
                    !form.getM_supplyTypeESI().toLowerCase().equals("na")
                    || !form.getM_interfaceNameESI().toLowerCase().equals("na")
                    || !form.getM_interfaceDescriptionESI().toLowerCase().equals("na")
                    || !form.getM_sprayerNameESI().toLowerCase().equals("na")
                    || !form.getM_sprayerDescriptionESI().toLowerCase().equals("na")
                    || !form.getM_voltagesESI().toLowerCase().equals("na")
                    || !form.getM_otherParametersESI().toLowerCase().equals("na")
            );

            form.setM_PlateCompositionMALDI(getCellValue(sheet, 37));
            form.setM_matrixCompositionMALDI(getCellValue(sheet, 38));
            form.setM_depositionTechniqueMALDI(getCellValue(sheet, 39));
            form.setM_voltagesMALDI(getCellValue(sheet, 40));
            form.setM_evaluatedFragmentationMALDI(getCellValue(sheet, 41));
            form.setM_psdMALDI(getCellValue(sheet, 42));
            form.setM_delayedExtractionMALDI(getCellValue(sheet, 43));
            form.setM_laserMALDI(getCellValue(sheet, 44));
            form.setM_otherParmasMALDI(getCellValue(sheet, 45));
            form.setM_CheckMALDI(
                    !form.getM_PlateCompositionMALDI().toLowerCase().equals("na")
                    || !form.getM_matrixCompositionMALDI().toLowerCase().equals("na")
                    || !form.getM_depositionTechniqueMALDI().toLowerCase().equals("na")
                    || !form.getM_voltagesMALDI().toLowerCase().equals("na")
                    || !form.getM_evaluatedFragmentationMALDI().toLowerCase().equals("na")
                    || !form.getM_psdMALDI().toLowerCase().equals("na")
                    || !form.getM_delayedExtractionMALDI().toLowerCase().equals("na")
                    || !form.getM_laserMALDI().toLowerCase().equals("na")
                    || !form.getM_otherParmasMALDI().toLowerCase().equals("na")
            );

            form.setM_hardwareOptions(getCellValue(sheet, 47));

            form.setM_cidGasComposition(getCellValue(sheet, 50));
            form.setM_cidGasPressure(getCellValue(sheet, 51));
            form.setM_cidCollisionEnergy(getCellValue(sheet, 52));
            form.setM_CheckCID(
                    !form.getM_cidGasComposition().toLowerCase().equals("na")
                    || !form.getM_cidGasPressure().toLowerCase().equals("na")
                    || !form.getM_cidCollisionEnergy().toLowerCase().equals("na")
            );

            form.setM_etdReagentGas(getCellValue(sheet, 54));
            form.setM_etdPressure(getCellValue(sheet, 55));
            form.setM_etdReactionTime(getCellValue(sheet, 56));
            form.setM_etdNumberOfAtoms(getCellValue(sheet, 57));
            form.setM_CheckETD(
                    !form.getM_etdReagentGas().toLowerCase().equals("na")
                    || !form.getM_etdPressure().toLowerCase().equals("na")
                    || !form.getM_etdReactionTime().toLowerCase().equals("na")
                    || !form.getM_etdNumberOfAtoms().toLowerCase().equals("na")
            );

            form.setM_ecdEmitterType(getCellValue(sheet, 59));
            form.setM_ecdVoltage(getCellValue(sheet, 60));
            form.setM_ecdCurrent(getCellValue(sheet, 61));
            form.setM_CheckECD(
                    !form.getM_ecdEmitterType().toLowerCase().equals("na")
                    || !form.getM_ecdVoltage().toLowerCase().equals("na")
                    || !form.getM_ecdCurrent().toLowerCase().equals("na")
            );

            form.setM_reflectronStatus(getCellValue(sheet, 63));
            form.setM_CheckTOF(!form.getM_reflectronStatus().toLowerCase().equals("na"));

            form.setM_finalMsStage(getCellValue(sheet, 65));
            form.setM_CheckIonTrap(!form.getM_finalMsStage().toLowerCase().equals("na"));

            form.setM_ionMobilityGas(getCellValue(sheet, 67));
            form.setM_ionMobilityPressure(getCellValue(sheet, 68));
            form.setM_ionMobilityInstrumentParameters(getCellValue(sheet, 69));
            form.setM_CheckIonMobility(
                    !form.getM_ionMobilityGas().toLowerCase().equals("na")
                    || !form.getM_ionMobilityPressure().toLowerCase().equals("na")
                    || !form.getM_ionMobilityInstrumentParameters().toLowerCase().equals("na")
            );

            form.setM_ftIcrPeakSelection(getCellValue(sheet, 71));
            form.setM_ftIcrPulse(getCellValue(sheet, 72));
            form.setM_ftIcrWidth(getCellValue(sheet, 73));
            form.setM_ftIcrVoltage(getCellValue(sheet, 74));
            form.setM_ftIcrDecayTime(getCellValue(sheet, 75));
            form.setM_ftIcrIR(getCellValue(sheet, 76));
            form.setM_ftIcrOtherParameters(getCellValue(sheet, 77));
            form.setM_CheckFTICR(
                    !form.getM_ftIcrPeakSelection().toLowerCase().equals("na")
                    || !form.getM_ftIcrPulse().toLowerCase().equals("na")
                    || !form.getM_ftIcrWidth().toLowerCase().equals("na")
                    || !form.getM_ftIcrVoltage().toLowerCase().equals("na")
                    || !form.getM_ftIcrDecayTime().toLowerCase().equals("na")
                    || !form.getM_ftIcrIR().toLowerCase().equals("na")
                    || !form.getM_ftIcrOtherParameters().toLowerCase().equals("na")
            );

            form.setM_detectorType(getCellValue(sheet, 79));
            form.setM_CheckDetectors(!form.getM_detectorType().toLowerCase().equals("na"));

            form.setSoftwareTwoName(new ArrayList<>());
            form.setSoftwareTwoVersion(new ArrayList<>());
            setSoftwareInfo(sheet,82, Arrays.asList(form.getSoftwareTwoName(),form.getSoftwareTwoVersion()));

            form.setSoftwareThreeSoftware(new ArrayList<>());
            form.setSoftwareThreeName(new ArrayList<>());
            form.setSoftwareThreeFormat(new ArrayList<>());
            form.setSoftwareThreeArea(new ArrayList<>());
            form.setSoftwareThreeURL(new ArrayList<>());
            setFilesInfo(sheet,88,82,Arrays.asList(form.getSoftwareThreeName(),form.getSoftwareThreeFormat(),form.getSoftwareThreeArea(), form.getSoftwareThreeURL()),form.getSoftwareThreeSoftware());

            form.setSoftwareFourSoftware(new ArrayList<>());
            form.setSoftwareFourVersion(new ArrayList<>());
            form.setSoftwareFourCustomizations(new ArrayList<>());
            form.setSoftwareFourSettings(new ArrayList<>());
            setSoftwareInfo(sheet,94, Arrays.asList(form.getSoftwareFourSoftware(),form.getSoftwareFourVersion(),form.getSoftwareFourCustomizations(),form.getSoftwareFourSettings()));

            form.setSoftwareFiveSoftware(new ArrayList<>());
            form.setSoftwareFiveName(new ArrayList<>());
            form.setSoftwareFiveFormat(new ArrayList<>());
            form.setSoftwareFiveURL(new ArrayList<>());
            setFilesInfo(sheet,101,94,Arrays.asList(form.getSoftwareFiveName(),form.getSoftwareFiveFormat(), form.getSoftwareFiveURL()),form.getSoftwareFiveSoftware());

            form.setM_acquisitionNumber(getCellValue(sheet, 104));
            form.setM_peakLists(getCellValue(sheet, 105));
            form.setM_rawDataScoring(getCellValue(sheet, 106));
            form.setM_smoothing(getCellValue(sheet, 107));
            form.setM_backgroundThreshold(getCellValue(sheet, 108));
            form.setM_signalToNoise(getCellValue(sheet, 109));
            form.setM_peakHeight(getCellValue(sheet, 110));
            form.setM_retentionTimes(getCellValue(sheet, 111));
            form.setM_intensityValues(getCellValue(sheet, 112));

            form.setSoftwareSixName(new ArrayList<>());
            form.setSoftwareSixVersion(new ArrayList<>());
            form.setSoftwareSixType(new ArrayList<>());
            form.setSoftwareSixCustomizations(new ArrayList<>());
            form.setSoftwareSixSettings(new ArrayList<>());
            setSoftwareInfo(sheet,115, Arrays.asList(form.getSoftwareSixName(),form.getSoftwareSixVersion(),form.getSoftwareSixType(),form.getSoftwareSixCustomizations(),form.getSoftwareSixSettings()));

            form.setSoftwareSevenSoftware(new ArrayList<>());
            form.setSoftwareSevenName(new ArrayList<>());
            form.setSoftwareSevenFormat(new ArrayList<>());
            form.setSoftwareSevenURL(new ArrayList<>());
            setFilesInfo(sheet,123,115,Arrays.asList(form.getSoftwareSevenName(),form.getSoftwareSevenFormat(), form.getSoftwareSevenURL()),form.getSoftwareSevenSoftware());

            form.setM_databaseQueried(getCellValue(sheet, 128));
            form.setM_taxonomicalRestrictions(getCellValue(sheet, 129));
            form.setM_otherRestrictions(getCellValue(sheet, 130));
            form.setM_allowedCleavages(getCellValue(sheet, 131));
            form.setM_massAccuracy(getCellValue(sheet, 132));
            form.setM_parentError(getCellValue(sheet, 133));
            form.setM_fragmentError(getCellValue(sheet, 134));
            form.setM_scoringMethod(getCellValue(sheet, 135));
            form.setM_scoringValueFormat(getCellValue(sheet, 136));
            form.setM_scoringAlgorithm(getCellValue(sheet, 137));
            form.setM_scoringResult(getCellValue(sheet, 138));

            form.setM_validationStatus(getCellValue(sheet, 140));
            form.setM_validationMethod(getCellValue(sheet, 141));
            form.setM_validationResult(getCellValue(sheet, 142));
        }
        return form;
    }

    public FormStructure getStructures(){
        FormStructure form = new FormStructure();
        /*Sheet sheet = wb.getSheet("Structures");

        List<List<String>> structureList = tableValues(sheet,6,10,7);
        form.setScanNumber(structureList.get(0));
        form.setDb(structureList.get(1));
        form.setIonMode(structureList.get(2));
        form.setMsLevel(structureList.get(3));
        form.setValidation(structureList.get(4));
        form.setOrthogonalApproaches(structureList.get(6));*/

        return form;
    }

    // Method for extracting data as String from cell
    private String getCellValue(Sheet sheet, int row){
        Cell cell = sheet.getRow(row).getCell(2);
        cell.setCellType(Cell.CELL_TYPE_STRING);

        return cell.getStringCellValue();
    }

    // Method for extracting data from rows with multiple fields
    private List<String> multiValueRow(Sheet sheet, int rowNumber){
        List<String> list = new ArrayList<>();
        Row row = sheet.getRow(rowNumber);
        int column = 2;

        while (row.getCell(column) != null && row.getCell(column).getCellType() != Cell.CELL_TYPE_BLANK){
            Cell cell = row.getCell(column);
            cell.setCellType(Cell.CELL_TYPE_STRING);
            list.add(cell.getStringCellValue());
            column++;
        }

        return list;
    }

    // Method for extracting data from rows with multiple fields with size
    private List<String> multiValueRow(Sheet sheet, int rowNumber, int size){
        List<String> list = new ArrayList<>();
        Row row = sheet.getRow(rowNumber);
        int end = size + 2;

        for (int i = 2; i <= end; i++){
            Cell cell = null == row.getCell(i) ? row.createCell(i) : row.getCell(i);
            cell.setCellType(Cell.CELL_TYPE_STRING);
            list.add(cell.getStringCellValue());
        }

        return list;
    }

    // Method for getting the file info
    private void setFilesInfo(Sheet sheet, int nameRowNumber, int softwareRowNumber, List<List<String>> lists, List<String> softwareList){
        Row row = sheet.getRow(nameRowNumber);
        Row softwareRow = sheet.getRow(softwareRowNumber);
        int column = 2;

        while (row.getCell(column).getCellType() != Cell.CELL_TYPE_BLANK && softwareRow.getCell(column).getCellType() != Cell.CELL_TYPE_BLANK){
            Cell cell = row.getCell(column);
            cell.setCellType(Cell.CELL_TYPE_STRING);
            if (!cell.getStringCellValue().equals("")){
                int rNum = nameRowNumber;
                for (List<String> l : lists){
                    Cell c = sheet.getRow(rNum++).getCell(column);
                    c.setCellType(Cell.CELL_TYPE_STRING);
                    l.add(c.getStringCellValue());
                }
            }
            Cell softwareCell = softwareRow.getCell(column);
            softwareCell.setCellType(Cell.CELL_TYPE_STRING);
            softwareList.add(softwareCell.getStringCellValue());
            column++;
        }
    }

    // Method for getting the Software info
    private void setSoftwareInfo(Sheet sheet, int rowNumber, List<List<String>> lists){
        Row rowName = sheet.getRow(rowNumber);
        Row rowVersion = sheet.getRow(rowNumber + 1);
        int column = 2;

        while ((rowName.getCell(column) != null && rowName.getCell(column).getCellType() != Cell.CELL_TYPE_BLANK) && (rowVersion.getCell(column) != null && rowVersion.getCell(column).getCellType() != Cell.CELL_TYPE_BLANK)){
            rowName.getCell(column).setCellType(Cell.CELL_TYPE_STRING);
            rowVersion.getCell(column).setCellType(Cell.CELL_TYPE_STRING);
            if (!(lists.get(0).contains(rowName.getCell(column).getStringCellValue()) && lists.get(1).contains(rowVersion.getCell(column).getStringCellValue()))){
                int field = 0;
                for (List<String> l : lists){
                    Cell c = sheet.getRow(rowNumber + field).getCell(column);
                    c.setCellType(Cell.CELL_TYPE_STRING);
                    l.add(c.getStringCellValue());
                    field += field == 1 ? 3 : 1;
                }
            }
            column++;
        }
    }


    // Method for extracting data from tables
    private List<List<String>> tableValues(Sheet sheet, int startRow, int endRow, int columns){
        List<List<String>> valueList = new ArrayList<>();

        for (int i = 0; i < columns; i++){
            valueList.add(new ArrayList<>());
        }

        for (int i = startRow; i < endRow; i++){
            Row row = sheet.getRow(i);

            for (int c = 1; c <= columns; c++){
                String value = null;
                if(row.getCell(c) != null){
                    if (row.getCell(c).getCellType() == 1){
                        value = row.getCell(c).getStringCellValue();
                    }
                    if(value != null && value.length() > 0){
                        valueList.get(c - 1).add(value);
                    }
                }
            }
        }
        return valueList;
    }

    // Method for parsing the "notes" String from a gwp file returns a map
    public Map<String, String> getGwpNotes(String notes){
        Map<String,String> map = new HashMap<>();
        String lineBreak = "" + (char) 10;
        String[] notesRows = notes.split(lineBreak);

        for (String s : notesRows) {
            String[] type = s.split(":");
            if (type.length > 1){

                switch (type[0].toLowerCase().trim()) {
                    case "scan ms":
                        map.put("scan ms",type[1].trim());
                        break;
                    case "scan ms/ms":
                        map.put("scan ms/ms",type[1].trim());
                        break;
                    case "precursor":
                        map.put("precursor", type[1].trim());
                        break;
                    case "retention time":
                        map.put("retention time", type[1].trim());
                        break;
                    case "stability":
                        map.put("stability", type[1].trim());
                        break;
                    case "orthogonal":
                        map.put("orthogonal", type[1].trim());
                        break;
                    case "quantity":
                        map.put("quantity", type[1].trim());
                        break;
                    case "Quantitation method":
                        map.put("Quantitation method", type[1].trim());
                        break;
                    case "database":
                        map.put("database", type[1].trim());
                        break;
                    case "taxonomical restrictions":
                        map.put("taxonomical restrictions", type[1].trim());
                        break;
                    case "other restrictions":
                        map.put("other restrictions", type[1].trim());
                        break;
                    case "mass accuracy":
                        map.put("mass accuracy", type[1].trim());
                        break;
                    case "allowed cleavages":
                        map.put("allowed cleavages", type[1].trim());
                        break;
                    case "parent error":
                        map.put("parent error", type[1].trim());
                        break;
                    case "fragment error":
                        map.put("fragment error", type[1].trim());
                        break;
                    case "scoring method":
                        map.put("scoring method", type[1].trim());
                        break;
                    case "scoring value format":
                        map.put("scoring value format", type[1].trim());
                        break;
                    case "scoring algorithm":
                        map.put("scoring algorithm", type[1].trim());
                        break;
                    case "scoring result":
                        map.put("scoring result", type[1].trim());
                        break;
                    case "output data file":
                        map.put("output data file", type[1].trim());
                        break;
                    case "output data file format":
                        map.put("output data file format", type[1].trim());
                        break;
                    case "output data file uri":
                        map.put("output data file uri", type[1].trim());
                        break;
                    case "validation status":
                        map.put("validation status", type[1].trim());
                        break;
                    case "validation value format":
                        map.put("validation value format", type[1].trim());
                        break;
                    case "validation result":
                        map.put("validation result", type[1].trim());
                        break;
                    case "validation level":
                        map.put("validation level", type[1].trim());
                        break;
                    case "other":
                        map.put("other", type[1].trim());
                        break;
                }
            }
        }
        return map;
    }
/*
    public List<StructureForm> gwpParser(List<String> gwpPathList) {
        GlycanWorkspace theWorkspace = new GlycanWorkspace("/tmp/conf", true, new GlycanRendererAWT());
        List<StructureForm> structureList = new ArrayList<>();

        for(String f : gwpPathList) {
            theWorkspace.open(f, false, false);

            for (Scan scan : theWorkspace.getAllScans()) {

                StructureForm structureForm = new StructureForm();
                String notes = scan.getNotes().getText();

                if (!scan.getStructures().isEmpty()){
                    org.eurocarbdb.application.glycanbuilder.Glycan glycan = org.eurocarbdb.application.glycanbuilder.Glycan.fromString(scan.getStructures().toString());

                    structureForm.setReducingMass(getComposition(glycan.toGlycoCTCondensed()).mass);
                    structureForm.setReducingEnd(getReducingEnd(scan.getStructures().toString()));
                    structureForm.setPersubstitution(getPersubstitution(scan.getStructures().toString()));
                    structureForm.setPeakLabeledList(labeledPeaks(scan.getPeakList()));
                    structureForm.setStructure(scan.getStructures().toString());
                    structureForm.setPeaks(peaksToString(scan.getPeakList()));
                    structureList.add(structureForm);
                }
            }

        }
        return structureList;
    }

    private String getReducingEnd(String structure){
        String[] parts = structure.split(",");
        ReducingEnd reducingEnd = ReducingEnd.find.where().eq("abbreviation", parts[parts.length -1]).findUnique();
        return reducingEnd.name;
    }

    private String getPersubstitution(String structure){
        String[] parts = structure.split(",");
        Persubstitution persubstitution = Persubstitution.find.where().eq("abbreviation", parts[parts.length -4]).findUnique();
        return persubstitution.name;
    }

    private List<PeakLabeled> labeledPeaks(PeakList pl){
        List<PeakLabeled> list = new ArrayList<>();

        for (int i =0;i<pl.size();i++) {

            if(pl.getIntensity(i) != 0){

                PeakLabeled p = new PeakLabeled();
                p.intensity_value = pl.getIntensity(i);
                p.mz_value = pl.getMZ(i);

                list.add(p);
            }
        }
        return list;
    }

    public String peaksToString(PeakList pl){

        StringBuilder stringBuilder = new StringBuilder();

        for (int i =0;i<pl.size();i++) {
            stringBuilder.append(String.valueOf(pl.getMZ(i)));
            stringBuilder.append("-");
            stringBuilder.append(String.valueOf(pl.getIntensity(i)));
            stringBuilder.append("!");
        }
        return stringBuilder.toString();
    }

    public GlycanComposition getComposition(String sequenceCTCond) {
        String[] arrayCT = sequenceCTCond.split("\\n");
        GlycanComposition gC = new GlycanComposition();

        int sulf =0;
        int hexNac =0;
        int hex =0;
        int dHex =0;
        int totalHex=0;
        int neuGc=0;

        int glycolyl=0;
        int gluronic=0;
        int neuAc =0;
        int pento=0;
//        double mass =0.0;


        for(int i=0;i<arrayCT.length;i++){

            if (arrayCT[i].indexOf("HEX") != -1) {
                hex++;
            }

            if (arrayCT[i].indexOf("acetyl") != -1) {
                hexNac++;
            }

            if (arrayCT[i].indexOf("sulfate") != -1) {
                sulf++;
            }

            if (arrayCT[i].indexOf("lgal") != -1) {
                dHex++;
            }

            if (arrayCT[i].indexOf("dgro-dgal-NON") != -1) {
                neuGc++;
            }

            if (arrayCT[i].indexOf("glycolyl") != -1) {
                glycolyl++;
            }

            if (arrayCT[i].indexOf("HEX-1:5|6:a") != -1) {   *//**Achtung**//*
                gluronic++;
            }

            if (arrayCT[i].indexOf("dxyl") != -1) {
                pento++;
            }


        }
        neuAc = (neuGc-glycolyl);
        totalHex = ((hex - hexNac)-dHex)-gluronic + neuAc;
        hexNac= hexNac - neuAc;

        gC.pen=pento;
        gC.dhex=dHex;
        gC.hex=totalHex;
        gC.hexnac=hexNac;
        gC.neuac=neuAc;
        gC.neugc=glycolyl;
        gC.sulf=sulf;
        gC.gluro=gluronic;
        gC.setMass();

        return gC;
    }*/
}
