package src;

import models.HPLC.*;
import models.MS.*;
import models.core.*;
import models.samplePrep.*;
import org.eurocarbdb.application.glycanbuilder.FragmentEntry;
import org.eurocarbdb.application.glycanbuilder.GlycanRendererAWT;
import org.eurocarbdb.application.glycoworkbench.Annotation;
import org.eurocarbdb.application.glycoworkbench.GlycanWorkspace;
import org.eurocarbdb.application.glycoworkbench.PeakAnnotationMultiple;
import org.eurocarbdb.application.glycoworkbench.Scan;
import service.*;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//this class uploads form data to the DB
public class DataUploader {

    private Responsible responsible = null;
    private final Logger datauploaderLogger = LoggerFactory.getLogger(this.getClass());

    // Generates persistence objects from the PubMed form
    public Long persistPubMed(FormPubMed formPubMed, int user_id){
        JournalReference journalReference = new JournalReference();

        journalReference.authors = formPubMed.getAuthors();
        journalReference.title = formPubMed.getTitle();
        journalReference.show = true;
        journalReference.user_id=user_id;

        if(formPubMed.isManuscript()){

            journalReference.journal = Journal.findByTitle("Manuscript");
//            journalReference.publication_year = new Date().getYear();
            journalReference.publication_year =  LocalDate.parse(new SimpleDateFormat("yyyy-MM-dd").format(new Date())).getYear();



        } else {
            if (null == Journal.findByTitle(formPubMed.getJournal())){
                Journal journal = new Journal();
                journal.journal_title = formPubMed.getJournal();
                journal.journal_abbrev = formPubMed.getAbbrev();
                journal.save();
            }

            journalReference.journal = Journal.findByTitle(formPubMed.getJournal());
            journalReference.pubmed_id = Integer.parseInt(formPubMed.getPubMedId());
            journalReference.publication_year = Integer.parseInt(formPubMed.getYear());
            journalReference.journal_volume = Long.parseLong(formPubMed.getVolume());

            int endPage;
            int startPage;

            if (formPubMed.getPages().contains("-")) {
                int breakPoint = formPubMed.getPages().indexOf('-');
                startPage = Integer.parseInt(formPubMed.getPages().substring(0, breakPoint));
                endPage = startPage + Integer.parseInt(formPubMed.getPages().substring(breakPoint + 1));
            } else {
                endPage = Integer.parseInt(formPubMed.getPages());
                startPage = endPage;
            }
            journalReference.journal_start_page = Long.valueOf(startPage);
            journalReference.journal_end_page = Long.valueOf(endPage);
        }

        journalReference.save();

        return journalReference.journal_reference_id;
    }


    // Generates persistence objects from the sample form
    public long persistSampleForm(FormSample formSample, Long journalRef){
        long idSamToJRef=1;
//        System.out.println("Date: " + formSample.getS_date());
        if (!formSample.getS_generalInfo().toLowerCase().equals("na")) {
            // Glycoprotein glycoprotein = new Glycoprotein(); ?????????
//            System.out.println("responsive: " + responsible);
            if (responsible == null){
                responsible = new Responsible();
                responsible.responsible_name = formSample.getS_responsiblePerson();
                responsible.affiliation = formSample.getS_affiliation();
                responsible.contact_information = formSample.getS_contactInformation();
                responsible.save();
                JournalReference journalReference = JournalReference.find.byId(journalRef);
                journalReference.responsible = responsible;
                journalReference.update();
            }

            Sample sample = new Sample();
            sample.general_info = formSample.getS_generalInfo();
            sample.growth_harvest = formSample.getS_harvestConditions();
            sample.treatment_storage = formSample.getS_storageConditions();
            sample.vendor_item_info = formSample.getS_vendor();
            sample.purification = formSample.getS_purification();
            sample.generation_material = formSample.getS_SynthesisSteps();

            sample.cellLine = CellLine.find.where().ieq("cell_line_name", formSample.getS_cellType()).findUnique();
            sample.sampleName = Material.find.where().eq("material_type", 2).ieq("description", formSample.getS_sampleName()).findUnique();
            sample.startingMaterial = Material.find.where().eq("material_type", 1).ieq("description", formSample.getS_startingMaterial()).findUnique();
            sample.taxonomy = Taxonomy.find.where().ieq("taxon", formSample.getS_species()).findUnique();
            sample.tissue = Tissue.find.where().ieq("tissue_taxon", formSample.getS_origin()).findUnique();

            if(!formSample.getS_glycoprotein().equals("")){
                Glycoprotein glycoprotein = new Glycoprotein();
                glycoprotein.uniprot_id = formSample.getS_glycoprotein();
                glycoprotein.save();

                sample.glycoprotein = glycoprotein;
            }

//            sample.journalReference = JournalReference.find.byId(journalRef);
            sample.save();






            SampleToJournalReference sampleToJRef=new SampleToJournalReference();
            sampleToJRef.sample=sample;
            sampleToJRef.journalReference = JournalReference.find.byId(journalRef);
            sampleToJRef.save();

            idSamToJRef= sampleToJRef.sample_to_journal_reference_id;


            for (int i = 0; i < formSample.getIsoOneEnzymes().size(); i++) {
                if (!formSample.getIsoOneEnzymes().get(i).equals(" ")) {
                    SampleToTreatment sampleToTreatment = new SampleToTreatment();
                    sampleToTreatment.sample = sample;
//                    sampleToTreatment.treatment = Treatment.find.where().ieq("treatment_name", formSample.getIsoOneEnzymes().get(i)).eq("treatment_type", 1).findUnique();
                    sampleToTreatment.treatment = Treatment.find.where().ieq("treatment_name", formSample.getIsoOneEnzymes().get(i)).findUnique();
                    sampleToTreatment.reaction_conditions = formSample.getIsoOneConditions().get(i);
                    sampleToTreatment.vendor_expression = formSample.getIsoOneVendor().get(i);
                    sampleToTreatment.purpose = "Isolation";
                    sampleToTreatment.save();
                }
            }

            for (int i = 0; i < formSample.getIsoTwoChemicals().size(); i++) {
                if (!formSample.getIsoTwoChemicals().get(i).equals(" ")) {
                    SampleToTreatment sampleToTreatment = new SampleToTreatment();
                    sampleToTreatment.sample = sample;
//                    sampleToTreatment.treatment = Treatment.find.where().ieq("treatment_name", formSample.getIsoTwoChemicals().get(i)).eq("treatment_type", 2).findUnique();
                    sampleToTreatment.treatment = Treatment.find.where().ieq("treatment_name", formSample.getIsoTwoChemicals().get(i)).findUnique();
                    sampleToTreatment.reaction_conditions = formSample.getIsoTwoConditions().get(i);
                    sampleToTreatment.vendor_expression = "null";
                    sampleToTreatment.purpose = "Isolation";
                    sampleToTreatment.save();
                }
            }

            for (int i = 0; i < formSample.getModOneEnzymes().size(); i++) {
                if (!formSample.getModOneEnzymes().get(i).equals(" ")) {
                    SampleToTreatment sampleToTreatment = new SampleToTreatment();
                    sampleToTreatment.sample = sample;
//                    sampleToTreatment.treatment = Treatment.find.where().ieq("treatment_name", formSample.getModOneEnzymes().get(i)).eq("treatment_type", 3).findUnique();
                    sampleToTreatment.treatment = Treatment.find.where().ieq("treatment_name", formSample.getModOneEnzymes().get(i)).findUnique();
                    sampleToTreatment.reaction_conditions = formSample.getModOneConditions().get(i);
                    sampleToTreatment.origin_glycosidase = formSample.getModOneCOrigin().get(i);
                    sampleToTreatment.purpose = "Modification";
                    sampleToTreatment.save();
                }
            }

            for (int i = 0; i < formSample.getModTwoChemicals().size(); i++) {
                if (!formSample.getModTwoChemicals().get(i).equals(" ")) {
                    SampleToTreatment sampleToTreatment = new SampleToTreatment();
                    sampleToTreatment.sample = sample;
//                    sampleToTreatment.treatment = Treatment.find.where().ieq("treatment_name", formSample.getModTwoChemicals().get(i)).eq("treatment_type", 4).findUnique();
                    sampleToTreatment.treatment = Treatment.find.where().ieq("treatment_name", formSample.getModTwoChemicals().get(i)).findUnique();
                    sampleToTreatment.reaction_conditions = formSample.getModTwoConditions().get(i);
                    sampleToTreatment.type_modification = formSample.getModTwoModification().get(i);
                    sampleToTreatment.purpose = "Modification";
                    sampleToTreatment.save();
                }
            }
        }
        return idSamToJRef;
    }

    public void persistLCFrom(FormLC formLC, Long journalRef){
        if(!formLC.getL_solventA().toLowerCase().equals("na")) {

            if (responsible == null){
                responsible = new Responsible();
                responsible.responsible_name = formLC.getL_responsiblePerson();
                responsible.affiliation = formLC.getL_affiliation();
                responsible.contact_information = formLC.getL_contactInformation();
                responsible.save();
                JournalReference journalReference = JournalReference.find.byId(journalRef);
                journalReference.responsible = responsible;
                journalReference.update();
            }

            Instrument instrument = new Instrument();
            if (!formLC.getL_hplcManufacturer().equals("")) {
                instrument.manufacturer = Manufacturer.find.where().ieq("manufacturer_name", formLC.getL_hplcManufacturer()).findUnique();
                instrument.model = formLC.getL_hplcBrand();
                instrument.save();
            }

            MethodRun methodRun = new MethodRun();
            methodRun.temperature = formLC.getL_temp();
            methodRun.solvent_a = formLC.getL_solventA();
            methodRun.solvent_b = formLC.getL_solventB();
            methodRun.other_solvent = formLC.getL_solventD();
            methodRun.flow_rate = formLC.getL_flowRate();
            methodRun.flow_gradient = formLC.getL_flowGradient();
            methodRun.run_time = formLC.getL_runtime();
            methodRun.phase = formLC.getL_phase();
            methodRun.save();

            Injector injector = new Injector();
            if (!formLC.getL_injector().equals("") && !formLC.getL_injector().toLowerCase().equals("NA")) {
                injector.injector_type = formLC.getL_injector();
                injector.injector_settings = formLC.getL_injectorSettings();
                injector.save();
            }

            Column column = new Column();
            column.manufacturer = Manufacturer.find.where().ieq("manufacturer_name", formLC.getL_manufacturer()).findUnique();
            column.model = formLC.getL_model();
            column.type_chromatography = formLC.getL_chromatography();
            column.packing_material = formLC.getL_material();
            column.particle_size = formLC.getL_particleSize();
            column.column_size_length = formLC.getL_columnLength();
            column.column_size_width = formLC.getL_columnDiameter();
            column.save();

            LC lc = new LC();
            lc.journalReference = JournalReference.find.byId(journalRef);
            lc.instrument = instrument.model == null ? null : instrument;
            lc.methodRun = methodRun;
            lc.injector = injector.injector_type == null ? null : injector;
            lc.column = column;
            lc.save();
        }
    }

    public void persistMSFrom(FormMS formMS){

        if(!(formMS.getM_PlateCompositionMALDI().toLowerCase().equals("na") && formMS.getM_supplyTypeESI().toLowerCase().equals("na"))) {

            if (responsible == null){
                responsible = new Responsible();
                responsible.responsible_name = formMS.getM_responsiblePerson();
                responsible.affiliation = formMS.getM_affiliation();
                responsible.contact_information = formMS.getM_contactInformation();
                responsible.save();
                JournalReference journalReference = JournalReference.find.byId(formMS.getJournalReferenceID());
                journalReference.responsible = responsible;
                journalReference.update();
            }

            MS ms = new MS();
            ms.device = Device.find.where().ieq("model", formMS.getM_model()).findUnique();

            SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd");
            try {
                java.util.Date date = dt.parse(formMS.getM_date());
                ms.date_completed = date;
                System.out.println(date.toString());
            } catch (Exception e){

            }

            ms.customizations = formMS.getM_customizations();
            ms.journalReference = JournalReference.find.byId(formMS.getJournalReferenceID());
            ms.ion_mode = formMS.getM_ionMode().equals("Positive") ? "+" : "-";
            ms.hardware_options = formMS.getM_hardwareOptions();

            ms.save();

            MALDI maldi = new MALDI();
            if (!formMS.getM_PlateCompositionMALDI().toLowerCase().equals("na")) {
                maldi.plate_composition = formMS.getM_PlateCompositionMALDI();
                maldi.matrix_composition = formMS.getM_matrixCompositionMALDI();
                maldi.deposition_technique = formMS.getM_depositionTechniqueMALDI();
                maldi.voltages = formMS.getM_voltagesMALDI();
                maldi.prompt_fragmentation = formMS.getM_evaluatedFragmentationMALDI();
                maldi.psd_lid_summary = formMS.getM_psdMALDI();
                maldi.delayed_extraction = formMS.getM_delayedExtractionMALDI();
                maldi.laser_type = formMS.getM_laserMALDI();
                maldi.laser_details = formMS.getM_otherParmasMALDI();
                maldi.ms = ms;
                maldi.save();
            }

            ESI esi = new ESI();
            if (!formMS.getM_supplyTypeESI().toLowerCase().equals("na")) {
                esi.supply_type = formMS.getM_supplyTypeESI();
                esi.interface_name = formMS.getM_interfaceNameESI();
                esi.interface_details = formMS.getM_interfaceDescriptionESI();
                esi.sprayer_name = formMS.getM_sprayerNameESI();
                esi.sprayer_details = formMS.getM_sprayerDescriptionESI();
                esi.voltages = formMS.getM_voltagesESI();
                esi.prompt_fragmentation = formMS.getM_evaluatedFragmentationESI();
                esi.in_source_dissociation = formMS.getM_inSourceDissociationESI();
                esi.other_parameters = formMS.getM_otherParametersESI();
                esi.ms = ms;
                esi.save();
            }

            Detector detector = new Detector();
            if (!formMS.getM_detectorType().equals("") && !formMS.getM_detectorType().toLowerCase().equals("na")) {
                detector.detector_type = formMS.getM_detectorType();
                detector.ms = ms;
                detector.save();
            }

            IonMobility ionMobility = new IonMobility();
            if (!formMS.getM_ionMobilityGas().toLowerCase().equals("na")) {
                ionMobility.gas = formMS.getM_ionMobilityGas();
                ionMobility.pressure = formMS.getM_ionMobilityPressure();
                ionMobility.instrument_parameters = formMS.getM_ionMobilityInstrumentParameters();
                ionMobility.ms = ms;
                ionMobility.save();
            }

            TOF tof = new TOF();
            if (!formMS.getM_reflectronStatus().toLowerCase().equals("na")) {
                tof.reflectron_status = formMS.getM_reflectronStatus();
                tof.ms = ms;
                tof.save();
            }

            CollisionCell collisionCell = new CollisionCell();
            collisionCell.ms = ms;
            collisionCell.save();

            CID cid = new CID();
            if (!formMS.getM_cidGasComposition().toLowerCase().equals("na")) {
                cid.gas_composition = formMS.getM_cidGasComposition();
                cid.gas_pressure = formMS.getM_cidGasPressure();
                cid.collision_energy = formMS.getM_cidCollisionEnergy();
                cid.collisionCell = collisionCell;
                cid.save();
            }

            ETD etd = new ETD();
            if (
                    !(formMS.getM_etdReagentGas().equals("") || formMS.getM_etdReagentGas().toLowerCase().equals("na"))
                            && !(formMS.getM_etdPressure().equals("") || formMS.getM_etdPressure().toLowerCase().equals("na"))
                            && !(formMS.getM_etdReactionTime().equals("") || formMS.getM_etdReactionTime().toLowerCase().equals("na"))
                            && !(formMS.getM_etdNumberOfAtoms().equals("") || formMS.getM_etdNumberOfAtoms().toLowerCase().equals("na"))
                    ) {
                etd.reagent_gas = formMS.getM_etdReagentGas();
                etd.pressure = formMS.getM_etdPressure();
                etd.reaction_time = formMS.getM_etdReactionTime();
                etd.reagent_atoms = formMS.getM_etdNumberOfAtoms();
                etd.collisionCell = collisionCell;
                etd.save();
            }

            ECD ecd = new ECD();
            if (!formMS.getM_ecdEmitterType().toLowerCase().equals("na")) {
                ecd.emitter_type = formMS.getM_ecdEmitterType();
                ecd.voltage = formMS.getM_ecdVoltage();
                ecd.current = formMS.getM_ecdCurrent();
                ecd.collisionCell = collisionCell;
                ecd.save();
            }

            IonTrap ionTrap = new IonTrap();
            if (!formMS.getM_finalMsStage().equals("") && !formMS.getM_finalMsStage().toLowerCase().equals("na")) {
                ionTrap.ms_achieved = formMS.getM_finalMsStage();
                ionTrap.ms = ms;
                ionTrap.save();
            }

            FTICR fticr = new FTICR();
            if (!(formMS.getM_ftIcrPeakSelection().equals("") || formMS.getM_ftIcrPeakSelection().toLowerCase().equals("na"))
                    && !(formMS.getM_ftIcrPulse().equals("") || formMS.getM_ftIcrPulse().toLowerCase().equals("na"))
                    && !(formMS.getM_ftIcrWidth().equals("") || formMS.getM_ftIcrWidth().toLowerCase().equals("na"))
                    && !(formMS.getM_ftIcrVoltage().equals("") || formMS.getM_ftIcrVoltage().toLowerCase().equals("na"))
                    && !(formMS.getM_ftIcrDecayTime().equals("") || formMS.getM_ftIcrDecayTime().toLowerCase().equals("na"))
                    && !(formMS.getM_ftIcrIR().equals("") || formMS.getM_ftIcrIR().toLowerCase().equals("na"))
                    && !(formMS.getM_ftIcrOtherParameters().equals("") || formMS.getM_ftIcrOtherParameters().toLowerCase().equals("na"))) {
                fticr.peak_selection = formMS.getM_ftIcrPeakSelection();
                fticr.pulse = formMS.getM_ftIcrPulse();
                fticr.width = formMS.getM_ftIcrWidth();
                fticr.voltage = formMS.getM_ftIcrVoltage();
                fticr.decay_time = formMS.getM_ftIcrDecayTime();
                fticr.ir = formMS.getM_ftIcrIR();
                fticr.other_parameters = formMS.getM_ftIcrOtherParameters();
                fticr.ms = ms;
                fticr.save();
            }

            for (int i = 0; i < formMS.getSoftwareOneName().size(); i++) {
                if (!formMS.getSoftwareOneName().equals("")) {
                    MSToSoftware msToSoftware = new MSToSoftware();
                    msToSoftware.task = "Control";
                    msToSoftware.software = Software.find.where().ieq("name_software", formMS.getSoftwareOneName().get(i)).findUnique();
                    msToSoftware.version_software = formMS.getSoftwareOneVersion().get(i);
                    msToSoftware.upgrades = formMS.getSoftwareOneUpgrades().get(i);
                    msToSoftware.ms = ms;
                    msToSoftware.switching_criteria = formMS.getSoftwareOneSwitchingCriteria().get(i);
                    msToSoftware.isolation_width = formMS.getSoftwareOneIsolation().get(i);
                    msToSoftware.location_file = formMS.getSoftwareOneFile().get(i);
                    msToSoftware.save();
                }
            }

            List<MSToSoftware> softwareList = new ArrayList<>();
            for (int i = 0; i < formMS.getSoftwareTwoName().size(); i++) {
                if (!formMS.getSoftwareTwoName().get(i).equals("")) {
                    MSToSoftware msToSoftware = new MSToSoftware();
                    msToSoftware.task = "Analysis";
                    msToSoftware.software = Software.find.where().ieq("name_software", formMS.getSoftwareTwoName().get(i)).findUnique();
                    msToSoftware.version_software = formMS.getSoftwareTwoVersion().get(i);
                    msToSoftware.ms = ms;

                    msToSoftware.save();
                    softwareList.add(msToSoftware);
                }
            }

            for (int i = 0; i < formMS.getSoftwareThreeName().size(); i++) {
                if (!formMS.getSoftwareThreeName().get(i).equals("")) {
                    File file = new File();
                    file.file_name = formMS.getSoftwareThreeName().get(i);
                    file.format = formMS.getSoftwareThreeFormat().get(i);
                    file.identification_target_area = formMS.getSoftwareThreeArea().get(i);
                    file.url = formMS.getSoftwareThreeURL().get(i);

                    for (int l = 0; l < softwareList.size(); l++) {
//                        System.out.println("softwareList: " + softwareList.get(l).software.name_software + "    Form: " + formMS.getSoftwareThreeSoftware().get(i));
                        if (softwareList.get(l).software.name_software.equals(formMS.getSoftwareThreeSoftware().get(i))) {
                            file.msToSoftware = softwareList.get(l);
//                            System.out.println("file.msToSoftware" + file.msToSoftware.software.name_software);
                            break;
                        }
                    }
                    file.save();
                }
            }

            softwareList = new ArrayList<>();
            for (int i = 0; i < formMS.getSoftwareFourSoftware().size(); i++) {
                if (!formMS.getSoftwareFourSoftware().get(i).equals("")) {
                    MSToSoftware msToSoftware = new MSToSoftware();
                    msToSoftware.task = "Processing";
                    msToSoftware.software = Software.find.where().ieq("name_software", formMS.getSoftwareFourSoftware().get(i)).findUnique();
                    msToSoftware.version_software = formMS.getSoftwareFourVersion().get(i);
                    msToSoftware.customizations = formMS.getSoftwareFourCustomizations().get(i);
                    msToSoftware.software_settings = formMS.getSoftwareFourSettings().get(i);
                    msToSoftware.ms = ms;

                    msToSoftware.save();
                    softwareList.add(msToSoftware);
                }
            }

            for (int i = 0; i < formMS.getSoftwareFiveName().size(); i++) {
                if (!formMS.getSoftwareFiveName().get(i).equals("")) {
                    File file = new File();
                    file.file_name = formMS.getSoftwareFiveName().get(i);
                    file.format = formMS.getSoftwareFiveFormat().get(i);
                    file.url = formMS.getSoftwareFiveURL().get(i);

                    for (int l = 0; l < softwareList.size(); l++) {
                        if (softwareList.get(l).software.name_software.equals(formMS.getSoftwareFiveSoftware().get(i))) {
                            file.msToSoftware = softwareList.get(l);
                            break;
                        }
                    }
                    file.save();
                }
            }

            softwareList = new ArrayList<>();
            for (int i = 0; i < formMS.getSoftwareSixName().size(); i++) {
                if (!formMS.getSoftwareSixName().get(i).equals("")) {
                    MSToSoftware msToSoftware = new MSToSoftware();
                    msToSoftware.task = "Annotation";
                    msToSoftware.software = Software.find.where().ieq("name_software", formMS.getSoftwareSixName().get(i)).findUnique();
                    msToSoftware.version_software = formMS.getSoftwareSixVersion().get(i);
                    msToSoftware.type_processing = formMS.getSoftwareSixType().get(i);
                    msToSoftware.customizations = formMS.getSoftwareSixCustomizations().get(i);
                    msToSoftware.software_settings = formMS.getSoftwareSixSettings().get(i);
                    msToSoftware.ms = ms;

                    msToSoftware.save();
                    softwareList.add(msToSoftware);
                }
            }

            for (int i = 0; i < formMS.getSoftwareSevenName().size(); i++) {
                if (!formMS.getSoftwareSevenName().get(i).equals("")) {
                    File file = new File();
                    file.file_name = formMS.getSoftwareSevenName().get(i);
                    file.format = formMS.getSoftwareSevenFormat().get(i);
                    file.url = formMS.getSoftwareSevenURL().get(i);

                    for (int l = 0; l < softwareList.size(); l++) {
                        if (softwareList.get(l).software.name_software.equals(formMS.getSoftwareSevenSoftware().get(i))) {
                            file.msToSoftware = softwareList.get(l);
                            break;
                        }
                    }
                    file.save();
                }
            }
        }
    }


    public void saveStructures(FormMS formMS, long id){
        FileExtractor extractor = new FileExtractor();

        List<String> list = new ArrayList<>();
        for(String s : formMS.getGwpFiles()){
            if(!s.equals("")){
                list.add(s);
            }
        }

        GlycanWorkspace theWorkspace = new GlycanWorkspace("/tmp/conf", true, new GlycanRendererAWT());
        List<StructureForm> structureList = new ArrayList<>();

        for(String f : list) {
            theWorkspace.open(f, false, false);

            for (Scan scan : theWorkspace.getAllScans()) {

                if (scan.getStructures() != null) {

                    Map<String, String> notes = extractor.getGwpNotes(scan.getNotes().toString());
//                    org.eurocarbdb.application.glycanbuilder.Glycan glycan = org.eurocarbdb.application.glycanbuilder.Glycan.fromString(scan.getStructures().toString());

                    GlycanSequence gSeq= saveGlycanSequence(scan.getStructures().toString(), scan.getStructures().toGlycoCT().toString(),scan.getStructures().toGlycoCTCondensed().toString());

                    models.MS.Scan dbScan = new models.MS.Scan();
                    String[] parts = scan.getStructures().toString().split(",");
                    dbScan.reducingEnd = ReducingEnd.find.where().eq("abbreviation", parts[parts.length - 1]).findUnique();
                    dbScan.persubstitution = Persubstitution.find.where().eq("abbreviation", parts[parts.length - 4]).findUnique();


                    dbScan.show = true;
                    dbScan.note = notes.get("other");
                    dbScan.stability = notes.get("stability");
                    dbScan.orthogonal_approaches = notes.get("orthogonal");
                    dbScan.quantity = notes.get("quantity");
//                    dbScan.journalReference = JournalReference.find.byId(formMS.getJournalReferenceID());
                    dbScan.glycanSequence=gSeq;
                    dbScan.sampleToJournalReference = SampleToJournalReference.find.byId(id);
                    dbScan.save();

                    // Create Database if necessary
/*                    if (notes.get("database") == null && null == Database.find.where().ieq("name_database", "Unspecified").findUnique()) {
                        Database database = new Database();

                        database.name_database = "Unspecified";

                        database.save();
                    }*/



                    //Create scan to database only when the database in the gwp file has been specified
                    ScanToDatabase scanToDatabase = new models.MS.ScanToDatabase();

                    if (notes.get("database") != null) {
                        if (null == Database.find.where().ieq("name_database", notes.get("database")).findUnique()) {

                            Database database = new Database();
                            database.name_database = notes.get("database");
                            database.save();
                        }

                        // Create Scan to Database
                        scanToDatabase.taxonomical_restrictions = notes.get("taxonomical restrictions");
                        scanToDatabase.other_restrictions = notes.get("other restrictions");
                        scanToDatabase.allowed_cleavages = notes.get("allowed cleavages");
                        scanToDatabase.mass_accuracy = notes.get("mass accuracy");
                        scanToDatabase.parent_error = notes.get("parent error");
                        scanToDatabase.fragment_error = notes.get("fragment error");
                        scanToDatabase.scoring_method = notes.get("scoring method");
                        scanToDatabase.scoring_algorithm = notes.get("scoring algorithm");
                        scanToDatabase.scoring_result = notes.get("scoring result");
                        scanToDatabase.scoring_format = notes.get("scoring value format");

                        scanToDatabase.database = Database.find.where().ieq("name_database", notes.get("database")).findUnique();




//                    }else
//                        scanToDatabase.database =Database.find.where().ieq("name_database", "Unspecified").findUnique();

//                    scanToDatabase.database = (notes.get("database") == null) ? Database.find.where().ieq("name_database", notes.get("Unspecified")).findUnique() : Database.find.where().ieq("name_database", notes.get("database")).findUnique();


                        scanToDatabase.scan = dbScan;
                        scanToDatabase.save();

                        //dbScan.scanToDatScan.add(scanToDatabase);

                        if (notes.get("output data file") != null && notes.get("output data file format") != null && notes.get("output data file uri") != null) {
                            if(notes.get("output data file").length() > 1 && notes.get("output data file format").length() > 1 && notes.get("output data file uri").length() > 1) {
                                File file = new File();
                                file.file_name = notes.get("output data file");
                                file.format = notes.get("output data file format");
                                file.url = notes.get("output data file uri");
                                file.scanToDatabase = scanToDatabase;
                                file.save();
                            }
                        }
                    }



                    // Create Validation if necessary
                    // Create validation only when the corresponding values in the gwp are not empty
                    if(notes.get("validation status")!=null || notes.get("validation value format")!=null || notes.get("validation result")!=null || notes.get("validation level")!=null){
                        if(notes.get("validation level")!=null){
                            Validation validation;
                            validation= Validation.find.where().ieq("validation_level", notes.get("validation level")).findUnique();
//                        if (null == Validation.find.where().ieq("validation_level", notes.get("validation level")).findUnique()) {
                            if (validation==null) {
                                validation = new Validation();
                                validation.validation_level = notes.get("validation level");
                                validation.save();
                            }

                            // Create Scan to Validation
                            ScanToValidation scanToValidation = new ScanToValidation();
                            scanToValidation.status = notes.get("validation status");
                            scanToValidation.validation = validation;
                            scanToValidation.validation_result = notes.get("validation result");
                            scanToValidation.scan = dbScan;

                            scanToValidation.save();
                            //dbScan.scanToValScan.add(scanToValidation);
                        }
                    }



/*
                    // Create the glycan sequence
                    org.eurocarbdb.application.glycanbuilder.Glycan glycan = org.eurocarbdb.application.glycanbuilder.Glycan.fromString(scan.getStructures().toString());
                    GlycanSequence glycanSequence;
//                    if(null == GlycanSequence.find.where().eq("sequence_gws", scan.getStructures().toString()).findUnique()) {
                    if(null == GlycanSequence.findByStructure(scan.getStructures().toString())) {

                        glycanSequence = new GlycanSequence();
                        glycanSequence.sequence_gws = scan.getStructures().toString();
                        glycanSequence.sequence_ct = glycan.toGlycoCT();
                        glycanSequence.sequence_ct_condensed = glycan.toGlycoCTCondensed();
                        // ..... and more....
                        glycanSequence.save();
                    } else {
//                        glycanSequence = GlycanSequence.find.where().eq("sequence_gws", scan.getStructures().toString()).findUnique();
                        glycanSequence = GlycanSequence.findByStructure(scan.getStructures().toString());

                    }
                    dbScan.glycanSequence = glycanSequence;
                    dbScan.update();
*/


                    // Create the PeakList
                    PeakList peakList = new PeakList();

		    datauploaderLogger.error("creating peaklist for scan={}", scan);
		    datauploaderLogger.error("scan.precursorMZ={}", scan.getPrecursorMZ());
		    datauploaderLogger.error("peakList={}", peakList);
		    datauploaderLogger.error("peakList.base_peak_mz={}", peakList.base_peak_mz);

                    if(notes.get("precursor")!= null)
                        peakList.base_peak_mz = Double.parseDouble(notes.get("precursor"));
                    else
                        peakList.base_peak_mz = scan.getPrecursorMZ();

                    //peakList.base_peak_intensity = ???
                    peakList.base_peak_intensity = 0;

                    if(notes.get("retention time")!= null)
                        peakList.retention_time = Double.parseDouble(notes.get("retention time"));
                    else
                        peakList.retention_time = 0;

                    peakList.acquisition_number = formMS.getM_acquisitionNumber();
                    peakList.parameters_generation = formMS.getM_peakLists();
                    peakList.raw_data_scoring = formMS.getM_rawDataScoring();
                    peakList.smoothing = formMS.getM_smoothing();
                    peakList.threshold_algorithm = formMS.getM_backgroundThreshold();
                    peakList.signal_to_noise = formMS.getM_signalToNoise();
                    peakList.peak_height = formMS.getM_peakHeight();
                    peakList.scan = dbScan;
                    peakList.save();


                    if(scan.getAnnotatedPeakList().getAnnotations().size()>0){

                        for (PeakAnnotationMultiple peakAnnotations : scan.getAnnotatedPeakList().getAnnotations()) {

                            // Create all PeakLabeled
                            PeakLabeled peakLabeled = new PeakLabeled();
                            peakLabeled.mz_value = peakAnnotations.getPeak().getMZ();
                            peakLabeled.intensity_value = peakAnnotations.getPeak().getIntensity();
                            peakLabeled.peakList = peakList;
			    peakLabeled.pAnnotLab = new ArrayList<PeakAnnotated>();
                            peakLabeled.save();

                            for (Vector<Annotation> structureToAnnotations : peakAnnotations.getAnnotations()) {
                                for (Annotation annotation : structureToAnnotations) {

                                    // Create all PeakAnnotated
                                    PeakAnnotated peakAnnotated = new PeakAnnotated();
                                    FragmentEntry fragmentEntry = annotation.getFragmentEntry();
                                    peakAnnotated.sequence_gws = fragmentEntry.fragment.toString();
                                    peakAnnotated.calculated_mass = fragmentEntry.mass;
                                    peakAnnotated.fragmentation_type = fragmentEntry.name;
                                    peakAnnotated.peakLabeled = peakLabeled;
                                    peakAnnotated.save();

				    datauploaderLogger.error("saving annotation={}", annotation);
				    datauploaderLogger.error("peakLabeled.pAnnotLab={}", peakLabeled.pAnnotLab);

                                    peakLabeled.pAnnotLab.add(peakAnnotated);
                                }
                            }
                            peakLabeled.save();
                            //peakList.peakLabList.add(peakLabeled);
                        }
                        peakList.save();
                        dbScan.peakListScan = peakList;
                    }else{ ////ESTO ES NUEVO *****************************************************

                        for (int p=0;p< scan.getPeakList().size();p++) {
                            PeakLabeled peakLabeled = new PeakLabeled();
                            peakLabeled.mz_value = scan.getPeakList().getMZ(p);
                            peakLabeled.intensity_value = scan.getPeakList().getIntensity(p);
                            peakLabeled.peakList = peakList;
                            peakLabeled.save();
                        }
                    }
                }
            }
        }

    }



    public GlycanSequence saveGlycanSequence (String gwsSeq, String ctSeq, String ctCondSeq){

        GlycanSequence gSeqExist = GlycanSequence.findByStructure(gwsSeq);

        if (gSeqExist == null) {
            gSeqExist = new GlycanSequence();
            gSeqExist.sequence_gws = gwsSeq;
            gSeqExist.sequence_ct = ctSeq;
            gSeqExist.sequence_ct_condensed = ctCondSeq;
            gSeqExist.contributor_id = 4191;

            gSeqExist.save();

        }
        return gSeqExist;
    }


}
