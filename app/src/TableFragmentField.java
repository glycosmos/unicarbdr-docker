package src;


import java.util.List;

public class TableFragmentField {
    private int type;
    private String name;
    private String title;
    private String info;
    private List<String> list;
    private int width = 200;

    public TableFragmentField() {
    }

    public TableFragmentField(int type, String name, String title) {
        this.type = type;
        this.name = name;
        this.title = title;
    }

    public TableFragmentField(int type, String name, String title, int width) {
        this.type = type;
        this.name = name;
        this.title = title;
        this.width = width;
    }

    public TableFragmentField(int type, String name, String title, String info) {
        this.type = type;
        this.name = name;
        this.title = title;
        this.info = info;
    }

    public TableFragmentField(int type, String name, String title, String info, int width) {
        this.type = type;
        this.name = name;
        this.title = title;
        this.info = info;
        this.width = width;
    }

    public TableFragmentField(int type, String name, String title, List<String> list) {
        this.type = type;
        this.name = name;
        this.title = title;
        this.list = list;
    }

    public TableFragmentField(int type, String name, String title, List<String> list, int width) {
        this.type = type;
        this.name = name;
        this.title = title;
        this.list = list;
        this.width = width;
    }

    public TableFragmentField(int type, String name, String title, List<String> list, String info) {
        this.type = type;
        this.name = name;
        this.title = title;
        this.info = info;
        this.list = list;
    }

    public TableFragmentField(int type, String name, String title, List<String> list, String info, int width) {
        this.type = type;
        this.name = name;
        this.title = title;
        this.info = info;
        this.list = list;
        this.width = width;
    }


    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getList() {
        return list;
    }

    public void setList(List<String> list) {
        this.list = list;
    }
}
