package src;

import java.util.List;

public class TableFragment {
    //private List<String> titles;
    private List<TableFragmentField> fields;
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /*
    public List<String> getTitles() {
        return titles;
    }

    public void setTitles(List<String> titles) {
        this.titles = titles;
    }
    */
    public List<TableFragmentField> getFields() {
        return fields;
    }

    public void setFields(List<TableFragmentField> fields) {
        this.fields = fields;
    }
}
