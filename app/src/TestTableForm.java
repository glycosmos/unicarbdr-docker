package src;

import java.util.List;

public class TestTableForm {
    private List<String> inputOne;
    private List<String> inputTwo;
    private List<String> inputThree;
    private List<String> inputFour;
    private String hello;

    public String getHello() {
        return hello;
    }

    public void setHello(String hello) {
        this.hello = hello;
    }

    public List<String> getInputOne() {
        return inputOne;
    }

    public void setInputOne(List<String> inputOne) {
        this.inputOne = inputOne;
    }

    public List<String> getInputTwo() {
        return inputTwo;
    }

    public void setInputTwo(List<String> inputTwo) {
        this.inputTwo = inputTwo;
    }

    public List<String> getInputThree() {
        return inputThree;
    }

    public void setInputThree(List<String> inputThree) {
        this.inputThree = inputThree;
    }

    public List<String> getInputFour() {
        return inputFour;
    }

    public void setInputFour(List<String> inputFour) {
        this.inputFour = inputFour;
    }
}
