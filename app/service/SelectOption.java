package service;

public class SelectOption {
    public String text;
    public String id;

    public SelectOption(String text, String id) {
        this.text = text;
        this.id = id;
    }
}
