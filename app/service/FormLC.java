package service;


import com.avaje.ebeaninternal.server.lib.util.Str;

public class FormLC {
    //General features
    private String l_date;
    private String l_responsiblePerson;
    private String l_affiliation;
    private String l_contactInformation;

    private String l_hplcManufacturer;
    private String l_hplcBrand;
    private String l_injector;
    private String l_injectorSettings;

    private String l_temp;
    private String l_solventA;
    private String l_solventB;
    private String l_solventC;
    private String l_solventD;
    private String l_flowRate;
    private String l_flowGradient;
    private String l_runtime;
    private String l_phase;

    private String l_manufacturer;
    private String l_model;
    private String l_chromatography;
    private String l_material;
    private String l_columnDiameter;
    private String l_columnLength;
    private String l_particleSize;


    public String getL_date() {
        return l_date;
    }

    public void setL_date(String l_date) {
        this.l_date = l_date;
    }

    public String getL_responsiblePerson() {
        return l_responsiblePerson;
    }

    public void setL_responsiblePerson(String l_responsiblePerson) {
        this.l_responsiblePerson = l_responsiblePerson;
    }

    public String getL_affiliation() {
        return l_affiliation;
    }

    public void setL_affiliation(String l_affiliation) {
        this.l_affiliation = l_affiliation;
    }

    public String getL_contactInformation() {
        return l_contactInformation;
    }

    public void setL_contactInformation(String l_contactInformation) {
        this.l_contactInformation = l_contactInformation;
    }

    public String getL_hplcManufacturer() {
        return l_hplcManufacturer;
    }

    public void setL_hplcManufacturer(String l_hplcManufacturer) {
        this.l_hplcManufacturer = l_hplcManufacturer;
    }

    public String getL_hplcBrand() {
        return l_hplcBrand;
    }

    public void setL_hplcBrand(String l_hplcBrand) {
        this.l_hplcBrand = l_hplcBrand;
    }

    public String getL_injector() {
        return l_injector;
    }

    public void setL_injector(String l_injector) {
        this.l_injector = l_injector;
    }

    public String getL_injectorSettings() {
        return l_injectorSettings;
    }

    public void setL_injectorSettings(String l_injectorSettings) {
        this.l_injectorSettings = l_injectorSettings;
    }

    public String getL_temp() {
        return l_temp;
    }

    public void setL_temp(String l_temp) {
        this.l_temp = l_temp;
    }

    public String getL_solventA() {
        return l_solventA;
    }

    public void setL_solventA(String l_solventA) {
        this.l_solventA = l_solventA;
    }

    public String getL_solventB() {
        return l_solventB;
    }

    public void setL_solventB(String l_solventB) {
        this.l_solventB = l_solventB;
    }

    public String getL_solventC() {
        return l_solventC;
    }

    public void setL_solventC(String l_solventC) {
        this.l_solventC = l_solventC;
    }

    public String getL_solventD() {
        return l_solventD;
    }

    public void setL_solventD(String l_solventD) {
        this.l_solventD = l_solventD;
    }

    public String getL_flowRate() {
        return l_flowRate;
    }

    public void setL_flowRate(String l_flowRate) {
        this.l_flowRate = l_flowRate;
    }

    public String getL_flowGradient() {
        return l_flowGradient;
    }

    public void setL_flowGradient(String l_flowGradient) {
        this.l_flowGradient = l_flowGradient;
    }

    public String getL_runtime() {
        return l_runtime;
    }

    public void setL_runtime(String l_runtime) {
        this.l_runtime = l_runtime;
    }

    public String getL_phase() {
        return l_phase;
    }

    public void setL_phase(String l_phase) {
        this.l_phase = l_phase;
    }

    public String getL_manufacturer() {
        return l_manufacturer;
    }

    public void setL_manufacturer(String l_manufacturer) {
        this.l_manufacturer = l_manufacturer;
    }

    public String getL_model() {
        return l_model;
    }

    public void setL_model(String l_model) {
        this.l_model = l_model;
    }

    public String getL_chromatography() {
        return l_chromatography;
    }

    public void setL_chromatography(String l_chromatography) {
        this.l_chromatography = l_chromatography;
    }

    public String getL_material() {
        return l_material;
    }

    public void setL_material(String l_material) {
        this.l_material = l_material;
    }

    public String getL_columnDiameter() {
        return l_columnDiameter;
    }

    public void setL_columnDiameter(String l_columnDiameter) {
        this.l_columnDiameter = l_columnDiameter;
    }

    public String getL_columnLength() {
        return l_columnLength;
    }

    public void setL_columnLength(String l_columnLength) {
        this.l_columnLength = l_columnLength;
    }

    public String getL_particleSize() {
        return l_particleSize;
    }

    public void setL_particleSize(String l_particleSize) {
        this.l_particleSize = l_particleSize;
    }
}
