package service;


import java.util.List;

public class StructureForms {
    private List<StructureForm> list;

    public StructureForms() {
    }

    public StructureForms(List<StructureForm> list) {
        this.list = list;
    }

    public List<StructureForm> getList() {
        return list;
    }

    public void setList(List<StructureForm> list) {
        this.list = list;
    }
}
