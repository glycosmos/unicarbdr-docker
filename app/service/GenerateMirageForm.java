package service;


import models.MS.Device;
import models.core.Manufacturer;
import play.data.validation.Constraints;

import java.util.*;

public class GenerateMirageForm {

    // not used
    private int genComponentry = 1;
    private int genSheets;

    private String genIonSource;
    private boolean genSamplePrep;
    private boolean genLCMS;
    private boolean genMSMirage;

    private boolean genCID;
    private boolean genETD;
    private boolean genECD;
    private boolean genTOF;
    private boolean genIonTrap;
    private boolean genIonMobility;
    private boolean genFTICR;
    private boolean genDetectors;


/*    public String validator(){
        String s =null;
        for (String diss: getGenComponentry()){
            s += diss;
        }
        return s;
    }*/


    public int getGenSheets() {
        return genSheets;
    }

    public void setGenSheets(int genSheets) {
        this.genSheets = genSheets;
    }

    public String getGenIonSource() {
        return genIonSource;
    }

    public void setGenIonSource(String genIonSource) {
        this.genIonSource = genIonSource;
    }

    public int getGenComponentry() {
        return genComponentry;
    }

    public void setGenComponentry(int genComponentry) {
        this.genComponentry = genComponentry;
    }

    public boolean isCid() {
        return genCID;
    }


    public boolean isGenSamplePrep() {
        return genSamplePrep;
    }

    public void setGenSamplePrep(boolean genSamplePrep) {
        this.genSamplePrep = genSamplePrep;
        genSheets += genSamplePrep ? 1 : -1;
    }

    public boolean isGenLCMS() {
        return genLCMS;
    }

    public void setGenLCMS(boolean genLCMS) {
        this.genLCMS = genLCMS;
        genSheets += genLCMS ? 1 : -1;
    }

    public boolean isGenMSMirage() {
        return genMSMirage;
    }

    public void setGenMSMirage(boolean genMSMirage) {
        this.genMSMirage = genMSMirage;
        genSheets += genMSMirage ? 1 : -1;
        genComponentry += genMSMirage ? -1 : 1;
    }

    public boolean isGenCID() {
        return genCID;
    }

    public void setGenCID(boolean genCID) {
        this.genCID = genCID;
        genComponentry += genCID ? 1 : -1;
    }

    public boolean isGenETD() {
        return genETD;
    }

    public void setGenETD(boolean genETD) {
        this.genETD = genETD;
        genComponentry += genETD ? 1 : -1;
    }

    public boolean isGenECD() {
        return genECD;
    }

    public void setGenECD(boolean genECD) {
        this.genECD = genECD;
        genComponentry += genECD ? 1 : -1;
    }

    public boolean isGenTOF() {
        return genTOF;
    }

    public void setGenTOF(boolean genTOF) {
        this.genTOF = genTOF;
        genComponentry += genTOF ? 1 : -1;
    }

    public boolean isGenIonTrap() {
        return genIonTrap;
    }

    public void setGenIonTrap(boolean genIonTrap) {
        this.genIonTrap = genIonTrap;
        genComponentry += genIonTrap ? 1 : -1;
    }

    public boolean isGenIonMobility() {
        return genIonMobility;
    }

    public void setGenIonMobility(boolean genIonMobility) {
        this.genIonMobility = genIonMobility;
        genComponentry += genIonMobility ? 1 : -1;
    }

    public boolean isGenFTICR() {
        return genFTICR;
    }

    public void setGenFTICR(boolean genFTICR) {
        this.genFTICR = genFTICR;
        genComponentry += genFTICR ? 1 : -1;
    }

    public boolean isGenDetectors() {
        return genDetectors;
    }

    public void setGenDetectors(boolean genDetectors) {
        this.genDetectors = genDetectors;
        genComponentry += genDetectors ? 1 : -1;
    }

    public static List<String> lcMaterialOptions() {
        return Arrays.asList("Graphitized", "HILIC", "C18");
    }

    public static Map<String, String> InstrumentOptions(){
        LinkedHashMap<String,String> options = new LinkedHashMap<String,String>();
        int index = 0;
        for(Manufacturer d: Manufacturer.find.orderBy("model").findList()) {
            if(!options.containsValue(d.manufacturer_name)){
                options.put("" + index++, d.manufacturer_name);
            }
        }
        return options;
    }
/*
    public static Map<String, List<String>> modelList() {
        LinkedHashMap<String, List<String>> map = new LinkedHashMap<>();

        for(Device d: Device.find.orderBy("model").findList()) {
            if(map.size() != 0 && map.containsKey(d.manufacturer.manufacturer_name)){
                if(!map.get(d.manufacturer.manufacturer_name).contains(d.model)){
                    map.get(d.manufacturer.manufacturer_name).add(d.model);
                }
            } else {
                List<String> newList = new ArrayList<>();
                newList.add(d.model);
                map.put(d.manufacturer.manufacturer_name, newList);
            }
        }
        return map;
    }*/

}