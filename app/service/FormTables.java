package service;

import java.util.Arrays;
import java.util.List;

public class FormTables {

    // isoOne
    private List<String> isoOneEnzymes;
    private List<String> isoOneVendor;
    private List<String> isoOneConditions;

    // isoTwo
    private List<String> isoTwoChemicals;
    private List<String> isoTwoConditions;

    // modOne
    private List<String> modOneEnzymes;
    private List<String> modOneConditions;
    private List<String> modOneCOrigin;

    // modTwo
    private List<String> modTwoChemicals;
    private List<String> modTwoModification;
    private List<String> modTwoConditions;

    // softwareOne
    private List<String> softwareOneName;
    private List<String> softwareOneVersion;
    private List<String> softwareOneUpgrades;
    private List<String> softwareOneSwitchingCriteria;
    private List<String> softwareOneIsolation;
    private List<String> softwareOneFile;

    // softwareTwo
    private List<String> softwareTwoName;
    private List<String> softwareTwoVersion;

    // softwareThree
    private List<String> softwareThreeSoftware;
    private List<String> softwareThreeName;
    private List<String> softwareThreeFormat;
    private List<String> softwareThreeArea;
    private List<String> softwareThreeURL;

    // softwareFour
    private List<String> softwareFourSoftware;
    private List<String> softwareFourVersion;
    private List<String> softwareFourCustomizations;
    private List<String> softwareFourSettings;

    // softwareFive
    private List<String> softwareFiveSoftware;
    private List<String> softwareFiveName;
    private List<String> softwareFiveFormat;
    private List<String> softwareFiveURL;

    // softwareSix
    private List<String> softwareSixName;
    private List<String> softwareSixVersion;
    private List<String> softwareSixType;
    private List<String> softwareSixCustomizations;
    private List<String> softwareSixSettings;

    // softwareSeven
    private List<String> softwareSevenSoftware;
    private List<String> softwareSevenName;
    private List<String> softwareSevenFormat;
    private List<String> softwareSevenURL;

    // softwareEight
    private List<String> softwareEightSoftware;
    private List<String> softwareEightName;
    private List<String> softwareEightFormat;
    private List<String> softwareEightURI;


    public FormTables() {
    }

    public FormTables(List<String> isoOneEnzymes, List<String> isoOneVendor, List<String> isoOneConditions, List<String> isoTwoChemicals, List<String> isoTwoConditions, List<String> modOneEnzymes, List<String> modOneConditions, List<String> modOneCOrigin, List<String> modTwoChemicals, List<String> modTwoModification, List<String> modTwoConditions, List<String> softwareOneName, List<String> softwareOneVersion, List<String> softwareOneUpgrades, List<String> softwareOneSwitchingCriteria, List<String> softwareOneIsolation, List<String> softwareOneFile, List<String> softwareTwoName, List<String> softwareTwoVersion, List<String> softwareThreeSoftware, List<String> softwareThreeName, List<String> softwareThreeFormat, List<String> softwareThreeArea, List<String> softwareThreeURL, List<String> softwareFourSoftware, List<String> softwareFourVersion, List<String> softwareFourCustomizations, List<String> softwareFourSettings, List<String> softwareFiveSoftware, List<String> softwareFiveName, List<String> softwareFiveFormat, List<String> softwareFiveURL, List<String> softwareSixName, List<String> softwareSixVersion, List<String> softwareSixType, List<String> softwareSixCustomizations, List<String> softwareSixSettings, List<String> softwareSevenSoftware, List<String> softwareSevenName, List<String> softwareSevenFormat, List<String> softwareSevenURL, List<String> softwareEightSoftware, List<String> softwareEightName, List<String> softwareEightFormat, List<String> softwareEightURI) {
        this.isoOneEnzymes = isoOneEnzymes;
        this.isoOneVendor = isoOneVendor;
        this.isoOneConditions = isoOneConditions;
        this.isoTwoChemicals = isoTwoChemicals;
        this.isoTwoConditions = isoTwoConditions;
        this.modOneEnzymes = modOneEnzymes;
        this.modOneConditions = modOneConditions;
        this.modOneCOrigin = modOneCOrigin;
        this.modTwoChemicals = modTwoChemicals;
        this.modTwoModification = modTwoModification;
        this.modTwoConditions = modTwoConditions;
        this.softwareOneName = softwareOneName;
        this.softwareOneVersion = softwareOneVersion;
        this.softwareOneUpgrades = softwareOneUpgrades;
        this.softwareOneSwitchingCriteria = softwareOneSwitchingCriteria;
        this.softwareOneIsolation = softwareOneIsolation;
        this.softwareOneFile = softwareOneFile;
        this.softwareTwoName = softwareTwoName;
        this.softwareTwoVersion = softwareTwoVersion;
        this.softwareThreeSoftware = softwareThreeSoftware;
        this.softwareThreeName = softwareThreeName;
        this.softwareThreeFormat = softwareThreeFormat;
        this.softwareThreeArea = softwareThreeArea;
        this.softwareThreeURL = softwareThreeURL;
        this.softwareFourSoftware = softwareFourSoftware;
        this.softwareFourVersion = softwareFourVersion;
        this.softwareFourCustomizations = softwareFourCustomizations;
        this.softwareFourSettings = softwareFourSettings;
        this.softwareFiveSoftware = softwareFiveSoftware;
        this.softwareFiveName = softwareFiveName;
        this.softwareFiveFormat = softwareFiveFormat;
        this.softwareFiveURL = softwareFiveURL;
        this.softwareSixName = softwareSixName;
        this.softwareSixVersion = softwareSixVersion;
        this.softwareSixType = softwareSixType;
        this.softwareSixCustomizations = softwareSixCustomizations;
        this.softwareSixSettings = softwareSixSettings;
        this.softwareSevenSoftware = softwareSevenSoftware;
        this.softwareSevenName = softwareSevenName;
        this.softwareSevenFormat = softwareSevenFormat;
        this.softwareSevenURL = softwareSevenURL;
        this.softwareEightSoftware = softwareEightSoftware;
        this.softwareEightName = softwareEightName;
        this.softwareEightFormat = softwareEightFormat;
        this.softwareEightURI = softwareEightURI;
    }

    public List<String> getIsoOneEnzymes() {
        return isoOneEnzymes;
    }

    public void setIsoOneEnzymes(List<String> isoOneEnzymes) {
        this.isoOneEnzymes = isoOneEnzymes;
    }

    public List<String> getIsoOneVendor() {
        return isoOneVendor;
    }

    public void setIsoOneVendor(List<String> isoOneVendor) {
        this.isoOneVendor = isoOneVendor;
    }

    public List<String> getIsoOneConditions() {
        return isoOneConditions;
    }

    public void setIsoOneConditions(List<String> isoOneConditions) {
        this.isoOneConditions = isoOneConditions;
    }

    public List<String> getIsoTwoChemicals() {
        return isoTwoChemicals;
    }

    public void setIsoTwoChemicals(List<String> isoTwoChemicals) {
        this.isoTwoChemicals = isoTwoChemicals;
    }

    public List<String> getIsoTwoConditions() {
        return isoTwoConditions;
    }

    public void setIsoTwoConditions(List<String> isoTwoConditions) {
        this.isoTwoConditions = isoTwoConditions;
    }

    public List<String> getModOneEnzymes() {
        return modOneEnzymes;
    }

    public void setModOneEnzymes(List<String> modOneEnzymes) {
        this.modOneEnzymes = modOneEnzymes;
    }

    public List<String> getModOneConditions() {
        return modOneConditions;
    }

    public void setModOneConditions(List<String> modOneConditions) {
        this.modOneConditions = modOneConditions;
    }

    public List<String> getModOneCOrigin() {
        return modOneCOrigin;
    }

    public void setModOneCOrigin(List<String> modOneCOrigin) {
        this.modOneCOrigin = modOneCOrigin;
    }

    public List<String> getModTwoChemicals() {
        return modTwoChemicals;
    }

    public void setModTwoChemicals(List<String> modTwoChemicals) {
        this.modTwoChemicals = modTwoChemicals;
    }

    public List<String> getModTwoModification() {
        return modTwoModification;
    }

    public void setModTwoModification(List<String> modTwoModification) {
        this.modTwoModification = modTwoModification;
    }

    public List<String> getModTwoConditions() {
        return modTwoConditions;
    }

    public void setModTwoConditions(List<String> modTwoConditions) {
        this.modTwoConditions = modTwoConditions;
    }

    public List<String> getSoftwareOneName() {
        return softwareOneName;
    }

    public void setSoftwareOneName(List<String> softwareOneName) {
        this.softwareOneName = softwareOneName;
    }

    public List<String> getSoftwareOneVersion() {
        return softwareOneVersion;
    }

    public void setSoftwareOneVersion(List<String> softwareOneVersion) {
        this.softwareOneVersion = softwareOneVersion;
    }

    public List<String> getSoftwareOneUpgrades() {
        return softwareOneUpgrades;
    }

    public void setSoftwareOneUpgrades(List<String> softwareOneUpgrades) {
        this.softwareOneUpgrades = softwareOneUpgrades;
    }

    public List<String> getSoftwareOneSwitchingCriteria() {
        return softwareOneSwitchingCriteria;
    }

    public void setSoftwareOneSwitchingCriteria(List<String> softwareOneSwitchingCriteria) {
        this.softwareOneSwitchingCriteria = softwareOneSwitchingCriteria;
    }

    public List<String> getSoftwareOneIsolation() {
        return softwareOneIsolation;
    }

    public void setSoftwareOneIsolation(List<String> softwareOneIsolation) {
        this.softwareOneIsolation = softwareOneIsolation;
    }

    public List<String> getSoftwareOneFile() {
        return softwareOneFile;
    }

    public void setSoftwareOneFile(List<String> softwareOneFile) {
        this.softwareOneFile = softwareOneFile;
    }

    public List<String> getSoftwareTwoName() {
        return softwareTwoName;
    }

    public void setSoftwareTwoName(List<String> softwareTwoName) {
        this.softwareTwoName = softwareTwoName;
    }

    public List<String> getSoftwareTwoVersion() {
        return softwareTwoVersion;
    }

    public void setSoftwareTwoVersion(List<String> softwareTwoVersion) {
        this.softwareTwoVersion = softwareTwoVersion;
    }

    public List<String> getSoftwareThreeSoftware() {
        return softwareThreeSoftware;
    }

    public void setSoftwareThreeSoftware(List<String> softwareThreeSoftware) {
        this.softwareThreeSoftware = softwareThreeSoftware;
    }

    public List<String> getSoftwareThreeName() {
        return softwareThreeName;
    }

    public void setSoftwareThreeName(List<String> softwareThreeName) {
        this.softwareThreeName = softwareThreeName;
    }

    public List<String> getSoftwareThreeFormat() {
        return softwareThreeFormat;
    }

    public void setSoftwareThreeFormat(List<String> softwareThreeFormat) {
        this.softwareThreeFormat = softwareThreeFormat;
    }

    public List<String> getSoftwareThreeArea() {
        return softwareThreeArea;
    }

    public void setSoftwareThreeArea(List<String> softwareThreeArea) {
        this.softwareThreeArea = softwareThreeArea;
    }

    public List<String> getSoftwareThreeURL() {
        return softwareThreeURL;
    }

    public void setSoftwareThreeURL(List<String> softwareThreeURL) {
        this.softwareThreeURL = softwareThreeURL;
    }

    public List<String> getSoftwareFourSoftware() {
        return softwareFourSoftware;
    }

    public void setSoftwareFourSoftware(List<String> softwareFourSoftware) {
        this.softwareFourSoftware = softwareFourSoftware;
    }

    public List<String> getSoftwareFourVersion() {
        return softwareFourVersion;
    }

    public void setSoftwareFourVersion(List<String> softwareFourVersion) {
        this.softwareFourVersion = softwareFourVersion;
    }

    public List<String> getSoftwareFourCustomizations() {
        return softwareFourCustomizations;
    }

    public void setSoftwareFourCustomizations(List<String> softwareFourCustomizations) {
        this.softwareFourCustomizations = softwareFourCustomizations;
    }

    public List<String> getSoftwareFourSettings() {
        return softwareFourSettings;
    }

    public void setSoftwareFourSettings(List<String> softwareFourSettings) {
        this.softwareFourSettings = softwareFourSettings;
    }

    public List<String> getSoftwareFiveSoftware() {
        return softwareFiveSoftware;
    }

    public void setSoftwareFiveSoftware(List<String> softwareFiveSoftware) {
        this.softwareFiveSoftware = softwareFiveSoftware;
    }

    public List<String> getSoftwareFiveName() {
        return softwareFiveName;
    }

    public void setSoftwareFiveName(List<String> softwareFiveName) {
        this.softwareFiveName = softwareFiveName;
    }

    public List<String> getSoftwareFiveFormat() {
        return softwareFiveFormat;
    }

    public void setSoftwareFiveFormat(List<String> softwareFiveFormat) {
        this.softwareFiveFormat = softwareFiveFormat;
    }

    public List<String> getSoftwareFiveURL() {
        return softwareFiveURL;
    }

    public void setSoftwareFiveURL(List<String> softwareFiveURL) {
        this.softwareFiveURL = softwareFiveURL;
    }

    public List<String> getSoftwareSixName() {
        return softwareSixName;
    }

    public void setSoftwareSixName(List<String> softwareSixName) {
        this.softwareSixName = softwareSixName;
    }

    public List<String> getSoftwareSixVersion() {
        return softwareSixVersion;
    }

    public void setSoftwareSixVersion(List<String> softwareSixVersion) {
        this.softwareSixVersion = softwareSixVersion;
    }

    public List<String> getSoftwareSixType() {
        return softwareSixType;
    }

    public void setSoftwareSixType(List<String> softwareSixType) {
        this.softwareSixType = softwareSixType;
    }

    public List<String> getSoftwareSixCustomizations() {
        return softwareSixCustomizations;
    }

    public void setSoftwareSixCustomizations(List<String> softwareSixCustomizations) {
        this.softwareSixCustomizations = softwareSixCustomizations;
    }

    public List<String> getSoftwareSixSettings() {
        return softwareSixSettings;
    }

    public void setSoftwareSixSettings(List<String> softwareSixSettings) {
        this.softwareSixSettings = softwareSixSettings;
    }

    public List<String> getSoftwareSevenSoftware() {
        return softwareSevenSoftware;
    }

    public void setSoftwareSevenSoftware(List<String> softwareSevenSoftware) {
        this.softwareSevenSoftware = softwareSevenSoftware;
    }

    public List<String> getSoftwareSevenName() {
        return softwareSevenName;
    }

    public void setSoftwareSevenName(List<String> softwareSevenName) {
        this.softwareSevenName = softwareSevenName;
    }

    public List<String> getSoftwareSevenFormat() {
        return softwareSevenFormat;
    }

    public void setSoftwareSevenFormat(List<String> softwareSevenFormat) {
        this.softwareSevenFormat = softwareSevenFormat;
    }

    public List<String> getSoftwareSevenURL() {
        return softwareSevenURL;
    }

    public void setSoftwareSevenURL(List<String> softwareSevenURL) {
        this.softwareSevenURL = softwareSevenURL;
    }

    public List<String> getSoftwareEightSoftware() {
        return softwareEightSoftware;
    }

    public void setSoftwareEightSoftware(List<String> softwareEightSoftware) {
        this.softwareEightSoftware = softwareEightSoftware;
    }

    public List<String> getSoftwareEightName() {
        return softwareEightName;
    }

    public void setSoftwareEightName(List<String> softwareEightName) {
        this.softwareEightName = softwareEightName;
    }

    public List<String> getSoftwareEightFormat() {
        return softwareEightFormat;
    }

    public void setSoftwareEightFormat(List<String> softwareEightFormat) {
        this.softwareEightFormat = softwareEightFormat;
    }

    public List<String> getSoftwareEightURI() {
        return softwareEightURI;
    }

    public void setSoftwareEightURI(List<String> softwareEightURI) {
        this.softwareEightURI = softwareEightURI;
    }
}
