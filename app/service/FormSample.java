package service;

import java.util.List;

public class FormSample {

    //General features
    private String s_date;
    private String s_responsiblePerson;
    private String s_affiliation;
    private String s_contactInformation;

    // Sample origin
    private String s_generalInfo;
    private String s_cellType;
    private String s_harvestConditions;
    private String s_origin;
    private String s_species;
    private String s_storageConditions;
    private String s_glycoprotein;
    private String s_vendor;
    private String s_SynthesisSteps;
    private String s_startingMaterial;

    // Sample processing
    // -Data in separate table form-   TODO check if possible (and desirable) to extract lists in this file
    // isoOne
    private List<String> isoOneEnzymes;
    private List<String> isoOneVendor;
    private List<String> isoOneConditions;

    // isoTwo
    private List<String> isoTwoChemicals;
    private List<String> isoTwoConditions;

    // modOne
    private List<String> modOneEnzymes;
    private List<String> modOneConditions;
    private List<String> modOneCOrigin;

    // modTwo
    private List<String> modTwoChemicals;
    private List<String> modTwoModification;
    private List<String> modTwoConditions;

    private String s_purification;

    // Defined sample
    private String s_sampleName;



    // Lists

    public List<String> getIsoOneEnzymes() {
        return isoOneEnzymes;
    }

    public void setIsoOneEnzymes(List<String> isoOneEnzymes) {
        this.isoOneEnzymes = isoOneEnzymes;
    }

    public List<String> getIsoOneVendor() {
        return isoOneVendor;
    }

    public void setIsoOneVendor(List<String> isoOneVendor) {
        this.isoOneVendor = isoOneVendor;
    }

    public List<String> getIsoOneConditions() {
        return isoOneConditions;
    }

    public void setIsoOneConditions(List<String> isoOneConditions) {
        this.isoOneConditions = isoOneConditions;
    }

    public List<String> getIsoTwoChemicals() {
        return isoTwoChemicals;
    }

    public void setIsoTwoChemicals(List<String> isoTwoChemicals) {
        this.isoTwoChemicals = isoTwoChemicals;
    }

    public List<String> getIsoTwoConditions() {
        return isoTwoConditions;
    }

    public void setIsoTwoConditions(List<String> isoTwoConditions) {
        this.isoTwoConditions = isoTwoConditions;
    }

    public List<String> getModOneEnzymes() {
        return modOneEnzymes;
    }

    public void setModOneEnzymes(List<String> modOneEnzymes) {
        this.modOneEnzymes = modOneEnzymes;
    }

    public List<String> getModOneConditions() {
        return modOneConditions;
    }

    public void setModOneConditions(List<String> modOneConditions) {
        this.modOneConditions = modOneConditions;
    }

    public List<String> getModOneCOrigin() {
        return modOneCOrigin;
    }

    public void setModOneCOrigin(List<String> modOneCOrigin) {
        this.modOneCOrigin = modOneCOrigin;
    }

    public List<String> getModTwoChemicals() {
        return modTwoChemicals;
    }

    public void setModTwoChemicals(List<String> modTwoChemicals) {
        this.modTwoChemicals = modTwoChemicals;
    }

    public List<String> getModTwoModification() {
        return modTwoModification;
    }

    public void setModTwoModification(List<String> modTwoModification) {
        this.modTwoModification = modTwoModification;
    }

    public List<String> getModTwoConditions() {
        return modTwoConditions;
    }

    public void setModTwoConditions(List<String> modTwoConditions) {
        this.modTwoConditions = modTwoConditions;
    }


    // properties

    public String getS_date() {
        return s_date;
    }

    public void setS_date(String s_date) {
        this.s_date = s_date;
    }

    public String getS_responsiblePerson() {
        return s_responsiblePerson;
    }

    public void setS_responsiblePerson(String s_responsiblePerson) {
        this.s_responsiblePerson = s_responsiblePerson;
    }

    public String getS_affiliation() {
        return s_affiliation;
    }

    public void setS_affiliation(String s_affiliation) {
        this.s_affiliation = s_affiliation;
    }

    public String getS_contactInformation() {
        return s_contactInformation;
    }

    public void setS_contactInformation(String s_contactInformation) {
        this.s_contactInformation = s_contactInformation;
    }

    public String getS_generalInfo() {
        return s_generalInfo;
    }

    public void setS_generalInfo(String s_generalInfo) {
        this.s_generalInfo = s_generalInfo;
    }

    public String getS_cellType() {
        return s_cellType;
    }

    public void setS_cellType(String s_cellType) {
        this.s_cellType = s_cellType;
    }

    public String getS_harvestConditions() {
        return s_harvestConditions;
    }

    public void setS_harvestConditions(String s_harvestConditions) {
        this.s_harvestConditions = s_harvestConditions;
    }

    public String getS_origin() {
        return s_origin;
    }

    public void setS_origin(String s_origin) {
        this.s_origin = s_origin;
    }

    public String getS_species() {
        return s_species;
    }

    public void setS_species(String s_species) {
        this.s_species = s_species;
    }

    public String getS_storageConditions() {
        return s_storageConditions;
    }

    public void setS_storageConditions(String s_storageConditions) {
        this.s_storageConditions = s_storageConditions;
    }

    public String getS_glycoprotein() {
        return s_glycoprotein;
    }

    public void setS_glycoprotein(String s_glycoprotein) {
        this.s_glycoprotein = s_glycoprotein;
    }

    public String getS_vendor() {
        return s_vendor;
    }

    public void setS_vendor(String s_vendor) {
        this.s_vendor = s_vendor;
    }

    public String getS_SynthesisSteps() {
        return s_SynthesisSteps;
    }

    public void setS_SynthesisSteps(String s_SynthesisSteps) {
        this.s_SynthesisSteps = s_SynthesisSteps;
    }

    public String getS_startingMaterial() {
        return s_startingMaterial;
    }

    public void setS_startingMaterial(String s_startingMaterial) {
        this.s_startingMaterial = s_startingMaterial;
    }

    public String getS_purification() {
        return s_purification;
    }

    public void setS_purification(String s_purification) {
        this.s_purification = s_purification;
    }

    public String getS_sampleName() {
        return s_sampleName;
    }

    public void setS_sampleName(String s_sampleName) {
        this.s_sampleName = s_sampleName;
    }
}
