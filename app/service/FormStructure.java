package service;

import java.util.List;

public class FormStructure {
    private List<String> scanNumber;
    private List<String> db;
    private List<String> ionMode;
    private List<String> msLevel;
    private List<String> validation;
    private List<String> orthogonalApproaches;

    public List<String> getScanNumber() {
        return scanNumber;
    }

    public void setScanNumber(List<String> scanNumber) {
        this.scanNumber = scanNumber;
    }

    public List<String> getDb() {
        return db;
    }

    public void setDb(List<String> db) {
        this.db = db;
    }

    public List<String> getIonMode() {
        return ionMode;
    }

    public void setIonMode(List<String> ionMode) {
        this.ionMode = ionMode;
    }

    public List<String> getMsLevel() {
        return msLevel;
    }

    public void setMsLevel(List<String> msLevel) {
        this.msLevel = msLevel;
    }

    public List<String> getValidation() {
        return validation;
    }

    public void setValidation(List<String> validation) {
        this.validation = validation;
    }

    public List<String> getOrthogonalApproaches() {
        return orthogonalApproaches;
    }

    public void setOrthogonalApproaches(List<String> orthogonalApproaches) {
        this.orthogonalApproaches = orthogonalApproaches;
    }
}
