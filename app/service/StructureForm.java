package service;


import controllers.routes;
import models.MS.PeakLabeled;

import java.util.List;

public class StructureForm {

    private String scanMS;
    private String scanMSMS;
    private String precursor;
    private String retentionTime;
    private String stability;

    // Orthogonal methods
    private String orthogonal;

    //Quantitation
    private String quantitationMethod;
    private String quantity;

    // Database
    private String database;
    private String taxRestrictions;
    private String otherRestrictions;
    private String massAccuracy;
    private String allowedCleavages;
    private String parentError;
    private String fragmentError;
    private String scoringMethod;
    private String scoringAlgorithm;
    private String scoringResult;
    private String scoringValueFormat;

    // Validation
    private String validationStatus;
    private String validationValueFormat;
    private String validationResults;
    private String other;

    // Structure
    private String structure;
    private String peaks;

    // PeakLabeled
    private List<PeakLabeled> peakLabeledList;

    private String reducingEnd;
    private String persubstitution;
    private double reducingMass;


    public StructureForm(){}
/*    public StructureForm(String structure, String retentionTime, String precursorTime, String peaks) {
        this.structure = structure;
        this.retentionTime = retentionTime;
        this.precursorMass = precursorTime;
        this.peaks = peaks;
    }

    public String getReducingMass(){
        return precursorMass;
        //return controllers.routes. TestsController.getComposition(structure);
    }
*/

    public double getReducingMass() {
        return reducingMass;
    }

    public void setReducingMass(double reducingMass) {
        this.reducingMass = reducingMass;
    }

    public String getPersubstitution() {
        return persubstitution;
    }

    public void setPersubstitution(String persubstitution) {
        this.persubstitution = persubstitution;
    }

    public String getReducingEnd() {
        return reducingEnd;
    }

    public void setReducingEnd(String reducingEnd) {
        this.reducingEnd = reducingEnd;
    }

    public List<PeakLabeled> getPeakLabeledList() {
        return peakLabeledList;
    }

    public void setPeakLabeledList(List<PeakLabeled> peakLabeledList) {
        this.peakLabeledList = peakLabeledList;
    }

    public String getPeaks() {
        return peaks;
    }

    public void setPeaks(String peaks) {
        this.peaks = peaks;
    }

    public boolean hasRetentionTime(){
        return !(retentionTime == "");
    }

    public String getStructure() {
        return structure;
    }

    public void setStructure(String structure) {
        this.structure = structure;
    }

    public String getRetentionTime() {
        return retentionTime;
    }

    public void setRetentionTime(String retentionTime) {
        this.retentionTime = retentionTime;
    }

    public String getScanMS() {
        return scanMS;
    }

    public void setScanMS(String scanMS) {
        this.scanMS = scanMS;
    }

    public String getScanMSMS() {
        return scanMSMS;
    }

    public void setScanMSMS(String scanMSMS) {
        this.scanMSMS = scanMSMS;
    }

    public String getPrecursor() {
        return precursor;
    }

    public void setPrecursor(String precursor) {
        this.precursor = precursor;
    }

    public String getStability() {
        return stability;
    }

    public void setStability(String stability) {
        this.stability = stability;
    }

    public String getOrthogonal() {
        return orthogonal;
    }

    public void setOrthogonal(String orthogonal) {
        this.orthogonal = orthogonal;
    }

    public String getQuantitationMethod() {
        return quantitationMethod;
    }

    public void setQuantitationMethod(String quantitationMethod) {
        this.quantitationMethod = quantitationMethod;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getDatabase() {
        return database;
    }

    public void setDatabase(String database) {
        this.database = database;
    }

    public String getTaxRestrictions() {
        return taxRestrictions;
    }

    public void setTaxRestrictions(String taxRestrictions) {
        this.taxRestrictions = taxRestrictions;
    }

    public String getOtherRestrictions() {
        return otherRestrictions;
    }

    public void setOtherRestrictions(String otherRestrictions) {
        this.otherRestrictions = otherRestrictions;
    }

    public String getMassAccuracy() {
        return massAccuracy;
    }

    public void setMassAccuracy(String massAccuracy) {
        this.massAccuracy = massAccuracy;
    }

    public String getAllowedCleavages() {
        return allowedCleavages;
    }

    public void setAllowedCleavages(String allowedCleavages) {
        this.allowedCleavages = allowedCleavages;
    }

    public String getParentError() {
        return parentError;
    }

    public void setParentError(String parentError) {
        this.parentError = parentError;
    }

    public String getFragmentError() {
        return fragmentError;
    }

    public void setFragmentError(String fragmentError) {
        this.fragmentError = fragmentError;
    }

    public String getScoringMethod() {
        return scoringMethod;
    }

    public void setScoringMethod(String scoringMethod) {
        this.scoringMethod = scoringMethod;
    }

    public String getScoringValueFormat() {
        return scoringValueFormat;
    }

    public void setScoringValueFormat(String scoringValueFormat) {
        this.scoringValueFormat = scoringValueFormat;
    }

    public String getScoringAlgorithm() {
        return scoringAlgorithm;
    }

    public void setScoringAlgorithm(String scoringAlgorithm) {
        this.scoringAlgorithm = scoringAlgorithm;
    }

    public String getScoringResult() {
        return scoringResult;
    }

    public void setScoringResult(String scoringResult) {
        this.scoringResult = scoringResult;
    }

    public String getValidationStatus() {
        return validationStatus;
    }

    public void setValidationStatus(String validationStatus) {
        this.validationStatus = validationStatus;
    }

    public String getValidationValueFormat() {
        return validationValueFormat;
    }

    public void setValidationValueFormat(String validationValueFormat) {
        this.validationValueFormat = validationValueFormat;
    }

    public String getValidationResults() {
        return validationResults;
    }

    public void setValidationResults(String validationResults) {
        this.validationResults = validationResults;
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }

    @Override
    public String toString() {
        return "StructureForm{" +
                "scanMS='" + scanMS + '\'' +
                ", scanMSMS='" + scanMSMS + '\'' +
                ", precursor='" + precursor + '\'' +
                ", retentionTime='" + retentionTime + '\'' +
                ", stability='" + stability + '\'' +
                ", orthogonal='" + orthogonal + '\'' +
                ", quantity='" + quantity + '\'' +
                ", taxRestrictions='" + taxRestrictions + '\'' +
                ", otherRestrictions='" + otherRestrictions + '\'' +
                ", allowedCleavages='" + allowedCleavages + '\'' +
                ", parentError='" + parentError + '\'' +
                ", fragmentError='" + fragmentError + '\'' +
                ", scoringMethod='" + scoringMethod + '\'' +
                ", scoringValueFormat='" + scoringValueFormat + '\'' +
                ", scoringAlgorithm='" + scoringAlgorithm + '\'' +
                ", scoringResult='" + scoringResult + '\'' +
                ", validationStatus='" + validationStatus + '\'' +
                ", validationValueFormat='" + validationValueFormat + '\'' +
                ", validationResults='" + validationResults + '\'' +
                ", structure='" + structure + '\'' +
                ", peaks='" + peaks + '\'' +
                '}';
    }
}
