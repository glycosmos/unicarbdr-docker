package service;

import com.feth.play.module.pa.PlayAuthenticate;
import com.feth.play.module.pa.user.AuthUser;
import models.User;
import org.jetbrains.annotations.Nullable;
import play.mvc.Http.Session;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Service layer for User DB entity
 */
public class UserProvider {

    private final PlayAuthenticate auth;
    private final Logger userLogger = LoggerFactory.getLogger(this.getClass());

    @Inject
    public UserProvider(final PlayAuthenticate auth) {
	userLogger.error("Constructing user with auth={}", auth);
        this.auth = auth;
    }

    @Nullable
    public User getUser(Session session) {
	userLogger.error("getUser for session={}", session);
	
        final AuthUser currentAuthUser = this.auth.getUser(session);
	userLogger.error("currentUser={}", currentAuthUser);
        final User localUser = User.findByAuthUserIdentity(currentAuthUser);
	userLogger.error("localUser={}", localUser);
        return localUser;
    }
}
