package service;

import play.data.validation.Constraints;

import java.util.List;

public class FormMS {

    // Journal reference ID
    private Long journalReferenceID;

    // Gwp file path
    private List<String> gwpFiles;

    // General features
    private String m_date;
    private String m_responsiblePerson;
    private String m_affiliation;
    private String m_contactInformation;
    private String m_manufacturer;
    private String m_model;
    private String m_customizations;
    private String m_ionMode;

    // SoftwareOne
    private List<String> softwareOneName;
    private List<String> softwareOneVersion;
    private List<String> softwareOneUpgrades;
    private List<String> softwareOneSwitchingCriteria;
    private List<String> softwareOneIsolation;
    private List<String> softwareOneFile;

    // Ion Sources
    // Ion Sources — (a) Electrospray Ionisation (ESI)
    private String m_supplyTypeESI;
    private String m_interfaceNameESI;
    private String m_interfaceDescriptionESI;
    private String m_sprayerNameESI;
    private String m_sprayerDescriptionESI;
    private String m_voltagesESI;
    private String m_evaluatedFragmentationESI;
    private String m_inSourceDissociationESI;
    private String m_otherParametersESI;

    // Ion sources — (b) MALDI
    private String m_PlateCompositionMALDI;
    private String m_matrixCompositionMALDI;
    private String m_depositionTechniqueMALDI;
    private String m_voltagesMALDI;
    private String m_evaluatedFragmentationMALDI;
    private String m_psdMALDI;
    private String m_delayedExtractionMALDI;
    private String m_laserMALDI;
    private String m_otherParmasMALDI;

    // Ion transfer optics
    private String m_hardwareOptions;

    // Collision-Induced Dissociation (CID)
    private String m_cidGasComposition;
    private String m_cidGasPressure;
    private String m_cidCollisionEnergy;

    // Electron Transfer Dissociation (ETD)
    private String m_etdReagentGas;
    private String m_etdPressure;
    private String m_etdReactionTime;
    private String m_etdNumberOfAtoms;

    // Electron Capture Dissociation (ECD)
    private String m_ecdEmitterType;
    private String m_ecdVoltage;
    private String m_ecdCurrent;

    //Post-source componentry — (b) TOF drift tube
    private String m_reflectronStatus;

    //Post-source componentry — (c) Ion trap
    private String m_finalMsStage;

    //Post-source componentry — (d) Ion mobility
    private String m_ionMobilityGas;
    private String m_ionMobilityPressure;
    private String m_ionMobilityInstrumentParameters;

    //Post-source componentry — (e) FT-ICR
    private String m_ftIcrPeakSelection;
    private String m_ftIcrPulse;
    private String m_ftIcrWidth;
    private String m_ftIcrVoltage;
    private String m_ftIcrDecayTime;
    private String m_ftIcrIR;
    private String m_ftIcrOtherParameters;

    //Post-source componentry — (f) Detectors
    private String m_detectorType;


    // Spectrum and peak list generation and annotation
    private String m_acquisitionNumber;
    private String m_peakLists;
    private String m_rawDataScoring;
    private String m_smoothing;
    private String m_backgroundThreshold;
    private String m_signalToNoise;
    private String m_peakHeight;
    private String m_retentionTimes;
    private String m_intensityValues;

    private String m_softwareFile;
    private String m_databaseMatching;
    private String m_databaseSettings;
    private String m_databaseQueried;
    private String m_taxonomicalRestrictions;
    private String m_otherRestrictions;
    private String m_allowedCleavages;
    private String m_massAccuracy;
    private String m_parentError;
    private String m_fragmentError;
    private String m_scoringMethod;
    private String m_scoringAlgorithm;
    private String m_scoringResult;
    private String m_scoringValueFormat;
    private String m_validationStatus;
    private String m_validationMethod;
    private String m_validationResult;

    // softwareTwo
    private List<String> softwareTwoName;
    private List<String> softwareTwoVersion;

    // softwareThree
    private List<String> softwareThreeSoftware;
    private List<String> softwareThreeName;
    private List<String> softwareThreeFormat;
    private List<String> softwareThreeArea;
    private List<String> softwareThreeURL;

    // softwareFour
    private List<String> softwareFourSoftware;
    private List<String> softwareFourVersion;
    private List<String> softwareFourCustomizations;
    private List<String> softwareFourSettings;

    // softwareFive
    private List<String> softwareFiveSoftware;
    private List<String> softwareFiveName;
    private List<String> softwareFiveFormat;
    private List<String> softwareFiveURL;

    // softwareSix
    private List<String> softwareSixName;
    private List<String> softwareSixVersion;
    private List<String> softwareSixType;
    private List<String> softwareSixCustomizations;
    private List<String> softwareSixSettings;

    // softwareSeven
    private List<String> softwareSevenSoftware;
    private List<String> softwareSevenName;
    private List<String> softwareSevenFormat;
    private List<String> softwareSevenURL;

    // DB matching file
    private String m_fileName;
    private String m_fileFormat;
    private String m_fileURI;

    // Checkboxes
    private boolean m_CheckESI;
    private boolean m_CheckMALDI;
    private boolean m_CheckCID;
    private boolean m_CheckECD;
    private boolean m_CheckETD;
    private boolean m_CheckTOF;
    private boolean m_CheckIonTrap;
    private boolean m_CheckIonMobility;
    private boolean m_CheckFTICR;
    private boolean m_CheckDetectors;

    public String getM_scoringValueFormat() {
        return m_scoringValueFormat;
    }

    public void setM_scoringValueFormat(String m_scoringValueFormat) {
        this.m_scoringValueFormat = m_scoringValueFormat;
    }

    public boolean isM_CheckESI() {
        return m_CheckESI;
    }

    public void setM_CheckESI(boolean m_CheckESI) {
        this.m_CheckESI = m_CheckESI;
    }

    public boolean isM_CheckMALDI() {
        return m_CheckMALDI;
    }

    public void setM_CheckMALDI(boolean m_CheckMALDI) {
        this.m_CheckMALDI = m_CheckMALDI;
    }

    public boolean isM_CheckCID() {
        return m_CheckCID;
    }

    public void setM_CheckCID(boolean m_CheckCID) {
        this.m_CheckCID = m_CheckCID;
    }

    public boolean isM_CheckECD() {
        return m_CheckECD;
    }

    public void setM_CheckECD(boolean m_CheckECD) {
        this.m_CheckECD = m_CheckECD;
    }

    public boolean isM_CheckETD() {
        return m_CheckETD;
    }

    public void setM_CheckETD(boolean m_CheckETD) {
        this.m_CheckETD = m_CheckETD;
    }

    public boolean isM_CheckTOF() {
        return m_CheckTOF;
    }

    public void setM_CheckTOF(boolean m_CheckTOF) {
        this.m_CheckTOF = m_CheckTOF;
    }

    public boolean isM_CheckIonTrap() {
        return m_CheckIonTrap;
    }

    public void setM_CheckIonTrap(boolean m_CheckIonTrap) {
        this.m_CheckIonTrap = m_CheckIonTrap;
    }

    public boolean isM_CheckIonMobility() {
        return m_CheckIonMobility;
    }

    public void setM_CheckIonMobility(boolean m_CheckIonMobility) {
        this.m_CheckIonMobility = m_CheckIonMobility;
    }

    public boolean isM_CheckFTICR() {
        return m_CheckFTICR;
    }

    public void setM_CheckFTICR(boolean m_CheckFTICR) {
        this.m_CheckFTICR = m_CheckFTICR;
    }

    public boolean isM_CheckDetectors() {
        return m_CheckDetectors;
    }

    public void setM_CheckDetectors(boolean m_CheckDetectors) {
        this.m_CheckDetectors = m_CheckDetectors;
    }

    public String getM_scoringResult() {
        return m_scoringResult;
    }

    public void setM_scoringResult(String m_scoringResult) {
        this.m_scoringResult = m_scoringResult;
    }

    public String getM_validationStatus() {
        return m_validationStatus;
    }

    public void setM_validationStatus(String m_validationStatus) {
        this.m_validationStatus = m_validationStatus;
    }

    public String getM_validationResult() {
        return m_validationResult;
    }

    public void setM_validationResult(String m_validationResult) {
        this.m_validationResult = m_validationResult;
    }

    public List<String> getGwpFiles() {
        return gwpFiles;
    }

    public void setGwpFiles(List<String> gwpFiles) {
        this.gwpFiles = gwpFiles;
    }

    public Long getJournalReferenceID() {
        return journalReferenceID;
    }

    public void setJournalReferenceID(Long journalReferenceID) {
        this.journalReferenceID = journalReferenceID;
    }

    public List<String> getSoftwareFiveSoftware() {
        return softwareFiveSoftware;
    }

    public void setSoftwareFiveSoftware(List<String> softwareFiveSoftware) {
        this.softwareFiveSoftware = softwareFiveSoftware;
    }

    public List<String> getSoftwareSevenSoftware() {
        return softwareSevenSoftware;
    }

    public void setSoftwareSevenSoftware(List<String> softwareSevenSoftware) {
        this.softwareSevenSoftware = softwareSevenSoftware;
    }

    public String getM_date() {
        return m_date;
    }

    public void setM_date(String m_date) {
        this.m_date = m_date;
    }

    public String getM_responsiblePerson() {
        return m_responsiblePerson;
    }

    public void setM_responsiblePerson(String m_responsiblePerson) {
        this.m_responsiblePerson = m_responsiblePerson;
    }

    public String getM_affiliation() {
        return m_affiliation;
    }

    public void setM_affiliation(String m_affiliation) {
        this.m_affiliation = m_affiliation;
    }

    public String getM_contactInformation() {
        return m_contactInformation;
    }

    public void setM_contactInformation(String m_contactInformation) {
        this.m_contactInformation = m_contactInformation;
    }

    public String getM_manufacturer() {
        return m_manufacturer;
    }

    public void setM_manufacturer(String m_manufacturer) {
        this.m_manufacturer = m_manufacturer;
    }

    public String getM_model() {
        return m_model;
    }

    public void setM_model(String m_model) {
        this.m_model = m_model;
    }

    public String getM_customizations() {
        return m_customizations;
    }

    public void setM_customizations(String m_customizations) {
        this.m_customizations = m_customizations;
    }

    public String getM_ionMode() {
        return m_ionMode;
    }

    public void setM_ionMode(String m_ionMode) {
        this.m_ionMode = m_ionMode;
    }

    public List<String> getSoftwareOneName() {
        return softwareOneName;
    }

    public void setSoftwareOneName(List<String> softwareOneName) {
        this.softwareOneName = softwareOneName;
    }

    public List<String> getSoftwareOneVersion() {
        return softwareOneVersion;
    }

    public void setSoftwareOneVersion(List<String> softwareOneVersion) {
        this.softwareOneVersion = softwareOneVersion;
    }

    public List<String> getSoftwareOneUpgrades() {
        return softwareOneUpgrades;
    }

    public void setSoftwareOneUpgrades(List<String> softwareOneUpgrades) {
        this.softwareOneUpgrades = softwareOneUpgrades;
    }

    public List<String> getSoftwareOneSwitchingCriteria() {
        return softwareOneSwitchingCriteria;
    }

    public void setSoftwareOneSwitchingCriteria(List<String> softwareOneSwitchingCriteria) {
        this.softwareOneSwitchingCriteria = softwareOneSwitchingCriteria;
    }

    public List<String> getSoftwareOneIsolation() {
        return softwareOneIsolation;
    }

    public void setSoftwareOneIsolation(List<String> softwareOneIsolation) {
        this.softwareOneIsolation = softwareOneIsolation;
    }

    public List<String> getSoftwareOneFile() {
        return softwareOneFile;
    }

    public void setSoftwareOneFile(List<String> softwareOneFile) {
        this.softwareOneFile = softwareOneFile;
    }

    public String getM_supplyTypeESI() {
        return m_supplyTypeESI;
    }

    public void setM_supplyTypeESI(String m_supplyTypeESI) {
        this.m_supplyTypeESI = m_supplyTypeESI;
    }

    public String getM_interfaceNameESI() {
        return m_interfaceNameESI;
    }

    public void setM_interfaceNameESI(String m_interfaceNameESI) {
        this.m_interfaceNameESI = m_interfaceNameESI;
    }

    public String getM_interfaceDescriptionESI() {
        return m_interfaceDescriptionESI;
    }

    public void setM_interfaceDescriptionESI(String m_interfaceDescriptionESI) {
        this.m_interfaceDescriptionESI = m_interfaceDescriptionESI;
    }

    public String getM_sprayerNameESI() {
        return m_sprayerNameESI;
    }

    public void setM_sprayerNameESI(String m_sprayerNameESI) {
        this.m_sprayerNameESI = m_sprayerNameESI;
    }

    public String getM_sprayerDescriptionESI() {
        return m_sprayerDescriptionESI;
    }

    public void setM_sprayerDescriptionESI(String m_sprayerDescriptionESI) {
        this.m_sprayerDescriptionESI = m_sprayerDescriptionESI;
    }

    public String getM_voltagesESI() {
        return m_voltagesESI;
    }

    public void setM_voltagesESI(String m_voltagesESI) {
        this.m_voltagesESI = m_voltagesESI;
    }

    public String getM_evaluatedFragmentationESI() {
        return m_evaluatedFragmentationESI;
    }

    public void setM_evaluatedFragmentationESI(String m_evaluatedFragmentationESI) {
        this.m_evaluatedFragmentationESI = m_evaluatedFragmentationESI;
    }

    public String getM_inSourceDissociationESI() {
        return m_inSourceDissociationESI;
    }

    public void setM_inSourceDissociationESI(String m_inSourceDissociationESI) {
        this.m_inSourceDissociationESI = m_inSourceDissociationESI;
    }

    public String getM_otherParametersESI() {
        return m_otherParametersESI;
    }

    public void setM_otherParametersESI(String m_otherParametersESI) {
        this.m_otherParametersESI = m_otherParametersESI;
    }

    public String getM_PlateCompositionMALDI() {
        return m_PlateCompositionMALDI;
    }

    public void setM_PlateCompositionMALDI(String m_PlateCompositionMALDI) {
        this.m_PlateCompositionMALDI = m_PlateCompositionMALDI;
    }

    public String getM_matrixCompositionMALDI() {
        return m_matrixCompositionMALDI;
    }

    public void setM_matrixCompositionMALDI(String m_matrixCompositionMALDI) {
        this.m_matrixCompositionMALDI = m_matrixCompositionMALDI;
    }

    public String getM_depositionTechniqueMALDI() {
        return m_depositionTechniqueMALDI;
    }

    public void setM_depositionTechniqueMALDI(String m_depositionTechniqueMALDI) {
        this.m_depositionTechniqueMALDI = m_depositionTechniqueMALDI;
    }

    public String getM_voltagesMALDI() {
        return m_voltagesMALDI;
    }

    public void setM_voltagesMALDI(String m_voltagesMALDI) {
        this.m_voltagesMALDI = m_voltagesMALDI;
    }

    public String getM_evaluatedFragmentationMALDI() {
        return m_evaluatedFragmentationMALDI;
    }

    public void setM_evaluatedFragmentationMALDI(String m_evaluatedFragmentationMALDI) {
        this.m_evaluatedFragmentationMALDI = m_evaluatedFragmentationMALDI;
    }

    public String getM_psdMALDI() {
        return m_psdMALDI;
    }

    public void setM_psdMALDI(String m_psdMALDI) {
        this.m_psdMALDI = m_psdMALDI;
    }

    public String getM_delayedExtractionMALDI() {
        return m_delayedExtractionMALDI;
    }

    public void setM_delayedExtractionMALDI(String m_delayedExtractionMALDI) {
        this.m_delayedExtractionMALDI = m_delayedExtractionMALDI;
    }

    public String getM_laserMALDI() {
        return m_laserMALDI;
    }

    public void setM_laserMALDI(String m_laserMALDI) {
        this.m_laserMALDI = m_laserMALDI;
    }

    public String getM_otherParmasMALDI() {
        return m_otherParmasMALDI;
    }

    public void setM_otherParmasMALDI(String m_otherParmasMALDI) {
        this.m_otherParmasMALDI = m_otherParmasMALDI;
    }

    public String getM_hardwareOptions() {
        return m_hardwareOptions;
    }

    public void setM_hardwareOptions(String m_hardwareOptions) {
        this.m_hardwareOptions = m_hardwareOptions;
    }

    public String getM_cidGasComposition() {
        return m_cidGasComposition;
    }

    public void setM_cidGasComposition(String m_cidGasComposition) {
        this.m_cidGasComposition = m_cidGasComposition;
    }

    public String getM_cidGasPressure() {
        return m_cidGasPressure;
    }

    public void setM_cidGasPressure(String m_cidGasPressure) {
        this.m_cidGasPressure = m_cidGasPressure;
    }

    public String getM_cidCollisionEnergy() {
        return m_cidCollisionEnergy;
    }

    public void setM_cidCollisionEnergy(String m_cidCollisionEnergy) {
        this.m_cidCollisionEnergy = m_cidCollisionEnergy;
    }

    public String getM_etdReagentGas() {
        return m_etdReagentGas;
    }

    public void setM_etdReagentGas(String m_etdReagentGas) {
        this.m_etdReagentGas = m_etdReagentGas;
    }

    public String getM_etdPressure() {
        return m_etdPressure;
    }

    public void setM_etdPressure(String m_etdPressure) {
        this.m_etdPressure = m_etdPressure;
    }

    public String getM_etdReactionTime() {
        return m_etdReactionTime;
    }

    public void setM_etdReactionTime(String m_etdReactionTime) {
        this.m_etdReactionTime = m_etdReactionTime;
    }

    public String getM_etdNumberOfAtoms() {
        return m_etdNumberOfAtoms;
    }

    public void setM_etdNumberOfAtoms(String m_etdNumberOfAtoms) {
        this.m_etdNumberOfAtoms = m_etdNumberOfAtoms;
    }

    public String getM_ecdEmitterType() {
        return m_ecdEmitterType;
    }

    public void setM_ecdEmitterType(String m_ecdEmitterType) {
        this.m_ecdEmitterType = m_ecdEmitterType;
    }

    public String getM_ecdVoltage() {
        return m_ecdVoltage;
    }

    public void setM_ecdVoltage(String m_ecdVoltage) {
        this.m_ecdVoltage = m_ecdVoltage;
    }

    public String getM_ecdCurrent() {
        return m_ecdCurrent;
    }

    public void setM_ecdCurrent(String m_ecdCurrent) {
        this.m_ecdCurrent = m_ecdCurrent;
    }

    public String getM_reflectronStatus() {
        return m_reflectronStatus;
    }

    public void setM_reflectronStatus(String m_reflectronStatus) {
        this.m_reflectronStatus = m_reflectronStatus;
    }

    public String getM_finalMsStage() {
        return m_finalMsStage;
    }

    public void setM_finalMsStage(String m_finalMsStage) {
        this.m_finalMsStage = m_finalMsStage;
    }

    public String getM_ionMobilityGas() {
        return m_ionMobilityGas;
    }

    public void setM_ionMobilityGas(String m_ionMobilityGas) {
        this.m_ionMobilityGas = m_ionMobilityGas;
    }

    public String getM_ionMobilityPressure() {
        return m_ionMobilityPressure;
    }

    public void setM_ionMobilityPressure(String m_ionMobilityPressure) {
        this.m_ionMobilityPressure = m_ionMobilityPressure;
    }

    public String getM_ionMobilityInstrumentParameters() {
        return m_ionMobilityInstrumentParameters;
    }

    public void setM_ionMobilityInstrumentParameters(String m_ionMobilityInstrumentParameters) {
        this.m_ionMobilityInstrumentParameters = m_ionMobilityInstrumentParameters;
    }

    public String getM_ftIcrPeakSelection() {
        return m_ftIcrPeakSelection;
    }

    public void setM_ftIcrPeakSelection(String m_ftIcrPeakSelection) {
        this.m_ftIcrPeakSelection = m_ftIcrPeakSelection;
    }

    public String getM_ftIcrPulse() {
        return m_ftIcrPulse;
    }

    public void setM_ftIcrPulse(String m_ftIcrPulse) {
        this.m_ftIcrPulse = m_ftIcrPulse;
    }

    public String getM_ftIcrWidth() {
        return m_ftIcrWidth;
    }

    public void setM_ftIcrWidth(String m_ftIcrWidth) {
        this.m_ftIcrWidth = m_ftIcrWidth;
    }

    public String getM_ftIcrVoltage() {
        return m_ftIcrVoltage;
    }

    public void setM_ftIcrVoltage(String m_ftIcrVoltage) {
        this.m_ftIcrVoltage = m_ftIcrVoltage;
    }

    public String getM_ftIcrDecayTime() {
        return m_ftIcrDecayTime;
    }

    public void setM_ftIcrDecayTime(String m_ftIcrDecayTime) {
        this.m_ftIcrDecayTime = m_ftIcrDecayTime;
    }

    public String getM_ftIcrIR() {
        return m_ftIcrIR;
    }

    public void setM_ftIcrIR(String m_ftIcrIR) {
        this.m_ftIcrIR = m_ftIcrIR;
    }

    public String getM_ftIcrOtherParameters() {
        return m_ftIcrOtherParameters;
    }

    public void setM_ftIcrOtherParameters(String m_ftIcrOtherParameters) {
        this.m_ftIcrOtherParameters = m_ftIcrOtherParameters;
    }

    public String getM_detectorType() {
        return m_detectorType;
    }

    public void setM_detectorType(String m_detectorType) {
        this.m_detectorType = m_detectorType;
    }

    public String getM_acquisitionNumber() {
        return m_acquisitionNumber;
    }

    public void setM_acquisitionNumber(String m_acquisitionNumber) {
        this.m_acquisitionNumber = m_acquisitionNumber;
    }

    public String getM_peakLists() {
        return m_peakLists;
    }

    public void setM_peakLists(String m_peakLists) {
        this.m_peakLists = m_peakLists;
    }

    public String getM_rawDataScoring() {
        return m_rawDataScoring;
    }

    public void setM_rawDataScoring(String m_rawDataScoring) {
        this.m_rawDataScoring = m_rawDataScoring;
    }

    public String getM_smoothing() {
        return m_smoothing;
    }

    public void setM_smoothing(String m_smoothing) {
        this.m_smoothing = m_smoothing;
    }

    public String getM_backgroundThreshold() {
        return m_backgroundThreshold;
    }

    public void setM_backgroundThreshold(String m_backgroundThreshold) {
        this.m_backgroundThreshold = m_backgroundThreshold;
    }

    public String getM_signalToNoise() {
        return m_signalToNoise;
    }

    public void setM_signalToNoise(String m_signalToNoise) {
        this.m_signalToNoise = m_signalToNoise;
    }

    public String getM_peakHeight() {
        return m_peakHeight;
    }

    public void setM_peakHeight(String m_peakHeight) {
        this.m_peakHeight = m_peakHeight;
    }

    public String getM_retentionTimes() {
        return m_retentionTimes;
    }

    public void setM_retentionTimes(String m_retentionTimes) {
        this.m_retentionTimes = m_retentionTimes;
    }

    public String getM_intensityValues() {
        return m_intensityValues;
    }

    public void setM_intensityValues(String m_intensityValues) {
        this.m_intensityValues = m_intensityValues;
    }

    public String getM_softwareFile() {
        return m_softwareFile;
    }

    public void setM_softwareFile(String m_softwareFile) {
        this.m_softwareFile = m_softwareFile;
    }

    public String getM_databaseMatching() {
        return m_databaseMatching;
    }

    public void setM_databaseMatching(String m_databaseMatching) {
        this.m_databaseMatching = m_databaseMatching;
    }

    public String getM_databaseSettings() {
        return m_databaseSettings;
    }

    public void setM_databaseSettings(String m_databaseSettings) {
        this.m_databaseSettings = m_databaseSettings;
    }

    public String getM_databaseQueried() {
        return m_databaseQueried;
    }

    public void setM_databaseQueried(String m_databaseQueried) {
        this.m_databaseQueried = m_databaseQueried;
    }

    public String getM_taxonomicalRestrictions() {
        return m_taxonomicalRestrictions;
    }

    public void setM_taxonomicalRestrictions(String m_taxonomicalRestrictions) {
        this.m_taxonomicalRestrictions = m_taxonomicalRestrictions;
    }

    public String getM_otherRestrictions() {
        return m_otherRestrictions;
    }

    public void setM_otherRestrictions(String m_otherRestrictions) {
        this.m_otherRestrictions = m_otherRestrictions;
    }

    public String getM_allowedCleavages() {
        return m_allowedCleavages;
    }

    public void setM_allowedCleavages(String m_allowedCleavages) {
        this.m_allowedCleavages = m_allowedCleavages;
    }

    public String getM_massAccuracy() {
        return m_massAccuracy;
    }

    public void setM_massAccuracy(String m_massAccuracy) {
        this.m_massAccuracy = m_massAccuracy;
    }

    public String getM_parentError() {
        return m_parentError;
    }

    public void setM_parentError(String m_parentError) {
        this.m_parentError = m_parentError;
    }

    public String getM_fragmentError() {
        return m_fragmentError;
    }

    public void setM_fragmentError(String m_fragmentError) {
        this.m_fragmentError = m_fragmentError;
    }

    public String getM_scoringMethod() {
        return m_scoringMethod;
    }

    public void setM_scoringMethod(String m_scoringMethod) {
        this.m_scoringMethod = m_scoringMethod;
    }

    public String getM_scoringAlgorithm() {
        return m_scoringAlgorithm;
    }

    public void setM_scoringAlgorithm(String m_scoringAlgorithm) {
        this.m_scoringAlgorithm = m_scoringAlgorithm;
    }

    public String getM_validationMethod() {
        return m_validationMethod;
    }

    public void setM_validationMethod(String m_validationMethod) {
        this.m_validationMethod = m_validationMethod;
    }

    public List<String> getSoftwareTwoName() {
        return softwareTwoName;
    }

    public void setSoftwareTwoName(List<String> softwareTwoName) {
        this.softwareTwoName = softwareTwoName;
    }

    public List<String> getSoftwareTwoVersion() {
        return softwareTwoVersion;
    }

    public void setSoftwareTwoVersion(List<String> softwareTwoVersion) {
        this.softwareTwoVersion = softwareTwoVersion;
    }

    public List<String> getSoftwareThreeSoftware() {
        return softwareThreeSoftware;
    }

    public void setSoftwareThreeSoftware(List<String> softwareThreeSoftware) {
        this.softwareThreeSoftware = softwareThreeSoftware;
    }

    public List<String> getSoftwareThreeName() {
        return softwareThreeName;
    }

    public void setSoftwareThreeName(List<String> softwareThreeName) {
        this.softwareThreeName = softwareThreeName;
    }

    public List<String> getSoftwareThreeFormat() {
        return softwareThreeFormat;
    }

    public void setSoftwareThreeFormat(List<String> softwareThreeFormat) {
        this.softwareThreeFormat = softwareThreeFormat;
    }

    public List<String> getSoftwareThreeArea() {
        return softwareThreeArea;
    }

    public void setSoftwareThreeArea(List<String> softwareThreeArea) {
        this.softwareThreeArea = softwareThreeArea;
    }

    public List<String> getSoftwareThreeURL() {
        return softwareThreeURL;
    }

    public void setSoftwareThreeURL(List<String> softwareThreeURL) {
        this.softwareThreeURL = softwareThreeURL;
    }

    public List<String> getSoftwareFourSoftware() {
        return softwareFourSoftware;
    }

    public void setSoftwareFourSoftware(List<String> softwareFourSoftware) {
        this.softwareFourSoftware = softwareFourSoftware;
    }

    public List<String> getSoftwareFourVersion() {
        return softwareFourVersion;
    }

    public void setSoftwareFourVersion(List<String> softwareFourVersion) {
        this.softwareFourVersion = softwareFourVersion;
    }

    public List<String> getSoftwareFourCustomizations() {
        return softwareFourCustomizations;
    }

    public void setSoftwareFourCustomizations(List<String> softwareFourCustomizations) {
        this.softwareFourCustomizations = softwareFourCustomizations;
    }

    public List<String> getSoftwareFourSettings() {
        return softwareFourSettings;
    }

    public void setSoftwareFourSettings(List<String> softwareFourSettings) {
        this.softwareFourSettings = softwareFourSettings;
    }

    public List<String> getSoftwareFiveName() {
        return softwareFiveName;
    }

    public void setSoftwareFiveName(List<String> softwareFiveName) {
        this.softwareFiveName = softwareFiveName;
    }

    public List<String> getSoftwareFiveFormat() {
        return softwareFiveFormat;
    }

    public void setSoftwareFiveFormat(List<String> softwareFiveFormat) {
        this.softwareFiveFormat = softwareFiveFormat;
    }

    public List<String> getSoftwareFiveURL() {
        return softwareFiveURL;
    }

    public void setSoftwareFiveURL(List<String> softwareFiveURL) {
        this.softwareFiveURL = softwareFiveURL;
    }

    public List<String> getSoftwareSixName() {
        return softwareSixName;
    }

    public void setSoftwareSixName(List<String> softwareSixName) {
        this.softwareSixName = softwareSixName;
    }

    public List<String> getSoftwareSixVersion() {
        return softwareSixVersion;
    }

    public void setSoftwareSixVersion(List<String> softwareSixVersion) {
        this.softwareSixVersion = softwareSixVersion;
    }

    public List<String> getSoftwareSixType() {
        return softwareSixType;
    }

    public void setSoftwareSixType(List<String> softwareSixType) {
        this.softwareSixType = softwareSixType;
    }

    public List<String> getSoftwareSixCustomizations() {
        return softwareSixCustomizations;
    }

    public void setSoftwareSixCustomizations(List<String> softwareSixCustomizations) {
        this.softwareSixCustomizations = softwareSixCustomizations;
    }

    public List<String> getSoftwareSixSettings() {
        return softwareSixSettings;
    }

    public void setSoftwareSixSettings(List<String> softwareSixSettings) {
        this.softwareSixSettings = softwareSixSettings;
    }

    public List<String> getSoftwareSevenName() {
        return softwareSevenName;
    }

    public void setSoftwareSevenName(List<String> softwareSevenName) {
        this.softwareSevenName = softwareSevenName;
    }

    public List<String> getSoftwareSevenFormat() {
        return softwareSevenFormat;
    }

    public void setSoftwareSevenFormat(List<String> softwareSevenFormat) {
        this.softwareSevenFormat = softwareSevenFormat;
    }

    public List<String> getSoftwareSevenURL() {
        return softwareSevenURL;
    }

    public void setSoftwareSevenURL(List<String> softwareSevenURL) {
        this.softwareSevenURL = softwareSevenURL;
    }

    public String getM_fileName() {
        return m_fileName;
    }

    public void setM_fileName(String m_fileName) {
        this.m_fileName = m_fileName;
    }

    public String getM_fileFormat() {
        return m_fileFormat;
    }

    public void setM_fileFormat(String m_fileFormat) {
        this.m_fileFormat = m_fileFormat;
    }

    public String getM_fileURI() {
        return m_fileURI;
    }

    public void setM_fileURI(String m_fileURI) {
        this.m_fileURI = m_fileURI;
    }
}
