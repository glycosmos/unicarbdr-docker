package models.DBviews;

import javax.persistence.*;

import com.avaje.ebean.*;
import models.Lcmucin;
import src.Util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;


@Entity
@Table(name="web.v_reference")
public class vReferences extends Model {

    @Id
    public Long journal_reference_id;

    public String title;
    public String journal_title;
    public String publication_year;
    public String authors;
    public String pubmed_id;
    public String pages;


    public static Find<Long,vReferences> find = new Find<Long,vReferences>(){};


    public static PagedList<vReferences> page(int page, int pageSize, String sortBy, String order, String filter) {

/*      This is the right query. The one beow is a temporal fix because Jin needs to see the structures but Niclas
        wants only the new MIRAGE compliant entries ot be displayed on the website

        PagedList<vReferences> p=find.where()
                .ilike("title", "%" + filter + "%")
                .orderBy(sortBy + " " + order)
                .findPagedList(page,pageSize);

*/

        PagedList<vReferences> p = null;

//        if(user_id==1) {
            p = find.where().conjunction()
                    .ne("journal_reference_id", 343)
                    .endJunction()
                    .disjunction()
                    .ilike("title", "%" + filter + "%")
                    .endJunction()
                    .orderBy(sortBy + " " + order)
                    .findPagedList(page, pageSize);
/*        }else{
            p = find.where().conjunction()
                    .ne("journal_reference_id", 343).eq("user_id",user_id)
                    .endJunction()
                    .disjunction()
                    .ilike("title", "%" + filter + "%")
                    .endJunction()
                    .orderBy(sortBy + " " + order)
                    .findPagedList(page, pageSize);
        }*/
        return p;
    }



    public static String getToolTipText(){
        return "References";
    }


    public static List<vReferences> findByParams(Map<String, String> params){

        Junction<Lcmucin> junction;
        junction = Ebean.find(Lcmucin.class).where().conjunction();
        junction.add(Expr.isNotNull("title"));

        if(!params.get("author").isEmpty())
            junction.add(Expr.ilike("authors","%"+params.get("author")+"%"));

        if(!params.get("title").isEmpty())
            junction.add(Expr.ilike("title","%"+params.get("title")+"%"));

        if(!params.get("year").isEmpty())
            junction.add(Expr.eq("publication_year",Integer.parseInt(params.get("year"))));

        if(!params.get("pubmed").isEmpty())
            junction.add(Expr.eq("pubmed_id",Integer.parseInt(params.get("pubmed"))));



        List<vReferences> p = find.where().conjunction()
                .add(junction)
                .orderBy("publication_year desc" )
                .findList();

        return p;

    }



    public static List<String> findReferencesList() {
        List<String> result = new ArrayList<String>();
        List<vReferences> refs = vReferences.find.all();

        for (vReferences r : refs){
            result.add(r.journal_title);
            result.add(r.authors);
        }

        result = Util.unique(result);
        try
        {
            Collections.sort(result);
        }
        catch(Exception ex)
        {
            System.out.println(ex.getCause());
        }

        return result;
    }


}


/*

Checar el ultimo valor en la sequencia de journal_reference
SELECT MAX(journal_reference_id) FROM core.journal_reference;
SELECT nextval('core.journal_reference_journal_reference_id_seq');
SELECT setval('core.journal_reference_journal_reference_id_seq', (SELECT MAX(journal_reference_id) FROM core.journal_reference));



drop table core.reference cascade

ALTER TABLE core.journal_reference DROP COLUMN reference_id;

create schema web

CREATE VIEW web.v_Reference AS
select distinct core.journal_reference.journal_reference_id, title, publication_year, authors,  pubmed_id from core.journal
inner join core.journal_reference on core.journal.journal_id = core.journal_reference.journal_id
inner join hplc.lcmucin on core.journal_reference.journal_reference_id = hplc.lcmucin.journal_reference_id

CREATE VIEW web.v_reference AS
select distinct core.journal_reference.journal_reference_id, title, journal_title, publication_year, authors,  pubmed_id from core.journal
inner join core.journal_reference on core.journal.journal_id = core.journal_reference.journal_id
inner join core.metadata on core.journal_reference.journal_reference_id = core.metadata.journal_reference_id
where show=true


 */