package models.DBviews;

import com.avaje.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.IOException;
import java.util.*;

@Entity
@Table(name="web.v_miragereport_ms")
public class vMirageMs extends Model {
    @Id
    public Long journal_reference_id;
    public String ms_customizations;
    public String ion_mode;
    public Date date_completed;
    public String hardware_options;
    public String device_model;
    public String ionisation_type;
    public String manufacturer_name;
    public String url;
    public String esi_supply_type;
    public String esi_interface_name;
    public String esi_interface_details;
    public String esi_sprayer_name;
    public String esi_sprayer_details;
    public String esi_voltages;
    public String esi_prompt;
    public String esi_in_source;
    public String esi_other_param;
    public String plate_composition;
    public String matrix_composition;
    public String deposition_technique;
    public String maldi_voltages;
    public String maldi_prompt;
    public String psd_lid_summary;
    public String delayed_extraction;
    public String laser_type;
    public String laser_details;
    public String detector_type;
    public String gas;
    public String pressure;
    public String instrument_parameters;
    public String reflectron_status;
    public String peak_selection;
    public String pulse;
    public String width;
    public String maldi_voltage;
    public String decay_time;
    public String ir;
    public String ft_other;
    public String ms_achieved;


    public String ecd_emitter_type;
    public String ecd_voltage;
    public String ecd_current;
    public String cid_gas_composition;
    public String cid_gas_pressure;
    public String cid_collision_energy;
    public String etd_reagent_gas;
    public String etd_pressure;
    public String etd_reaction_time;
    public String etd_reagent_atoms;



    public static Find<Long,vMirageMs> find = new Find<Long,vMirageMs>(){};

    public static vMirageMs findByJournalId(Long id) {
        vMirageMs listSamples = find.where().eq("journal_reference_id",id).findUnique();
        return listSamples;
    }



}


