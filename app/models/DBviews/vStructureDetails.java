package models.DBviews;

import javax.persistence.*;

import com.avaje.ebean.Model;


@Entity
@Table(name="web.v_structure_details")
public class vStructureDetails extends Model {

    @Id
    public Long scan_id;
    public double mass;
    public String reducing_name;
    public String pers_name;


    public static Find<Long,vStructureDetails> find = new Find<Long,vStructureDetails>(){};


}