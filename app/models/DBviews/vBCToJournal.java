package models.DBviews;

import javax.persistence.*;
import com.avaje.ebean.Model;

import java.util.List;


@Entity
@Table(name="web.v_bc_to_journal")
public class vBCToJournal extends Model{

//    @Id
    public Long journal_reference_id;
    public String ncbi_id;
    public String mesh_id;

    public String taxon;
    public String tissue_taxon;
    public String glycoprotein_name;
    public String cell_line_name;


    public static Find<Long,vBCToJournal> find = new Find<Long,vBCToJournal>(){};


    public static List<vBCToJournal> findbyJournalId(Long id) {
        return find.where().eq("journal_reference_id",id).findList();
    }
}




