package models.DBviews;

import javax.persistence.*;

//import ch.qos.logback.core.rolling.helper.IntegerTokenConverter;
import com.avaje.ebean.Ebean;
import com.avaje.ebean.Expr;
import com.avaje.ebean.Junction;
import com.avaje.ebean.Model;
//import play.data.validation.ValidationError;
import src.Util;

//import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Entity
@Table(name="web.v_advanced_search")

public class vAdvancedSearch extends Model {
    @Id
    public Long scan_id;

    public Double base_peak_mz;
    public Double mass;
    public int charge;
    public String authors;
    public String title;
    public int publication_year;
    public int pubmed_id;

    public int glycan_sequence_id;
    public String sequence_gws;
    public Integer hex;
    public int dhex;
    public int hexnac;
    public int neuac;
    public int neugc;
    public int persubstitution_id;
    public int reducing_end_id;
    public int taxonomy_id;
    public String taxon;
    public int tissue_taxonomy_id;
    public String tissue_taxon;


    public static Find<Long, vAdvancedSearch> find = new Find<Long, vAdvancedSearch>() {
    };


    public static List<vAdvancedSearch> findByParams(Map<String, String> params){

        Junction<vAdvancedSearch> junction;
        junction = find.where().conjunction();
        junction.add(Expr.eq("pen", 0));


        String sTempo;
        if(params.get("massType").matches("r"))
            sTempo = "base_peak_mz";
        else
            sTempo = "mass";


        if(!params.get("mass").isEmpty() && params.get("mass").length()>0){
            junction.add(makeQueryMass(params.get("mass"), params.get("error"), params.get("errorType"), sTempo));
        }

        sTempo=params.get("authors");
        if(!sTempo.isEmpty() && sTempo.length()>0){
            junction.add(Expr.icontains("authors", sTempo));
        }

        sTempo=params.get("title");
        if(!sTempo.isEmpty() && sTempo.length()>0){
            junction.add(Expr.icontains("title", sTempo));
        }

        sTempo=params.get("year");
        if(!sTempo.isEmpty() && sTempo.length()>0){
            junction.add(Expr.eq("publication_year", Integer.parseInt(sTempo)));
        }

        sTempo=params.get("pubmed");
        if(!sTempo.isEmpty() && sTempo.length()>0){
            junction.add(Expr.eq("pubmed_id", Integer.parseInt(sTempo)));
        }

        sTempo=params.get("persId");
        if(!sTempo.isEmpty() && sTempo.length()>0){
            junction.add(Expr.eq("persubstitution_id", Integer.parseInt(sTempo)));
        }

        sTempo=params.get("redEndId");
        if(!sTempo.isEmpty() && sTempo.length()>0){
            junction.add(Expr.eq("reducing_end_id", Integer.parseInt(sTempo)));
        }


        sTempo=params.get("charge");
        if(!sTempo.isEmpty() && !sTempo.equals("0")){
            junction.add(Expr.eq("charge", Integer.parseInt(sTempo)));
        }


        sTempo=params.get("tissue_taxonomy_id");
        if(!sTempo.isEmpty() && sTempo.length()>0){
            junction.add(Expr.eq("tissue_taxonomy_id", Integer.parseInt(sTempo)));
        }

        sTempo=params.get("taxonomy_id");
        if(!sTempo.isEmpty() && sTempo.length()>0){
            junction.add(Expr.eq("taxonomy_id", Integer.parseInt(sTempo)));
        }



        sTempo = params.get("selHex");
        if(!sTempo.equals("p")){
            junction.add(makeQueryComp(params.get("hex"), params.get("hex2"), sTempo, "hex"));
        }

        sTempo = params.get("selDHex");
        if(!sTempo.equals("p")){
            junction.add(makeQueryComp(params.get("dhex"), params.get("dhex2"), sTempo, "dhex"));
        }

        sTempo = params.get("selHexNac");
        if(!sTempo.equals("p")){
            junction.add(makeQueryComp(params.get("hexnac"), params.get("hexnac2"), sTempo, "hexnac"));
        }

        sTempo = params.get("selNeuAc");
        if(!sTempo.equals("p")){
            junction.add(makeQueryComp(params.get("neuAc"), params.get("neuAc2"), sTempo, "neuac"));
        }

        sTempo = params.get("selNeuGc");
        if(!sTempo.equals("p")){
            junction.add(makeQueryComp(params.get("neuGc"), params.get("neuGc2"), sTempo, "neugc"));
        }


        List<vAdvancedSearch> res = find.where().conjunction()
                .add(junction)
                .findList();


        return res;

    }


    public static List<vAdvancedSearch> findByParamsBasic(List<String> params){

        List<vAdvancedSearch> res = vAdvancedSearch.find.where().disjunction()
                .add(Expr.in("authors", params))
                .add(Expr.in("taxon", params))
                .add(Expr.in("tissue_taxon", params))
                .add(Expr.in("journal_title", params)
        ).findList();


        return res;
    }




    private static Junction makeQueryMass(String mass, String error, String typeError, String field){
        Junction<vAdvancedSearch> junction;
        junction = Ebean.find(vAdvancedSearch.class).where().conjunction();

        Double dMass = Double.parseDouble(mass);

        if(!error.isEmpty() && error.length()>0){
            Double dError = Util.formatError(dMass, typeError, Double.parseDouble(error));
            junction.add(Expr.between(field,(dMass-dError),(dMass+dError)));
        }else{
            junction.add(Expr.eq(field, dMass));
        }

        return junction;
    }


    private static Junction makeQueryComp(String comp1, String comp2, String selection, String field){

        Junction<vAdvancedSearch> junction;
        junction = Ebean.find(vAdvancedSearch.class).where().conjunction();


        if (selection.equals("n"))
            junction.add(Expr.eq(field, 0));
        else {
            if (!comp1.isEmpty() && !comp2.isEmpty())
                junction.add(Expr.between(field, Integer.parseInt(comp1), Integer.parseInt(comp2)));
            else {
                if (!comp1.isEmpty())
                    junction.add(Expr.eq(field, Integer.parseInt(comp1)));
                else
                    junction.add(Expr.gt(field, 0));
            }
        }

        return junction;
    }



    public static List<vAdvancedSearch> findStructures(long id){

        List<vAdvancedSearch> res = find.where().eq("glycan_sequence_id", id).findList();

        return res;
    }




}