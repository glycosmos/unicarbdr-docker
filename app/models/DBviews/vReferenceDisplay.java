package models.DBviews;

import javax.persistence.*;

import com.avaje.ebean.Model;
import com.avaje.ebean.*;
import com.avaje.ebean.Query;

import javax.persistence.*;
import java.util.List;


import org.eurocarbdb.application.glycanbuilder.BuilderWorkspace;
import org.eurocarbdb.application.glycanbuilder.GlycanRendererAWT;


@Entity
@Table(name="web.v_reference_display")
public class vReferenceDisplay extends Model{

    @Id
    public Long scan_id;
    public double base_peak_mz;
    public double retention_time;
    public String sequence_gws;
    public int journal_reference_id;
    public String charges;


    public static Find<Long,vReferenceDisplay> find = new Find<Long,vReferenceDisplay>(){};


    public static List<vReferenceDisplay> findByJournalId(Long id) {
        return find.where().eq("journal_reference_id",id).findList();
    }


    public void setCharges(){
        BuilderWorkspace workspace;
        workspace = new BuilderWorkspace("/tmp/config", true, new GlycanRendererAWT());

        org.eurocarbdb.application.glycanbuilder.Glycan glycan = org.eurocarbdb.application.glycanbuilder.Glycan.fromString(this.sequence_gws);

/*
        System.out.println("0.- "+this.sequence_gws);
        System.out.println("1.- "+glycan.getCharges().toString());
        System.out.println("2.- "+ glycan.getChargesAndExchanges());
        System.out.println("3.- "+ glycan.getNeutralExchanges());
        System.out.println("4.- "+ glycan.getMassOptions().getIsotope());
        System.out.println("5.- "+ glycan.getMassOptions().ISOTOPE);
        System.out.println("6.- "+ glycan.getCharges().isNegative());
        System.out.println("7.- "+ glycan.getCharges().getIons());
        System.out.println("8.- "+ glycan.getCharges().getIonsMass());
        System.out.println("9.- "+ glycan.getMassOptions().removeExchanges().getIsotope().toString());
        System.out.println("10.- "+ glycan.getCharges().getNoCharges());
*/

        String sign;
        String nExc;

        if(glycan.getCharges().toString().contains("-"))
            sign="-";
        else
            sign="+";

        if(glycan.getNeutralExchanges().toString().contains("0"))
            nExc="";
        else
            nExc="+("+glycan.getNeutralExchanges().toString()+")";

        String res= "[M "+ nExc + " " + sign +  glycan.getCharges().getNoCharges();
        res+= glycan.getCharges().getIons().elementAt(0).toString()+ "]";
        res+="<sup>"+ glycan.getCharges().getNoCharges() +  sign + "</sup>";

        this.charges=res;


    }

}
