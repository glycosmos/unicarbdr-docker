package models.DBviews;

import com.avaje.ebean.Model;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import src.PairWrapper;

import javax.persistence.*;
import javax.persistence.Table;
import java.io.IOException;
import java.util.*;

@Entity
@Table(name="web.v_similar_spectra")
public class vSimilarSpectra extends Model {

    public Long scan_id;
    public Long glycan_sequence_id;
    public String sequence_gws;
    public double base_peak_mz;
    public double base_peak_intensity;
    public boolean show;
    public String peaks;

    public static Find<Long,vSimilarSpectra> find = new Find<Long,vSimilarSpectra>(){};


    public static Map<PairWrapper, PeakList> generatePeakLists(Double parentMass, Double error) throws IOException {

        List<vSimilarSpectra> vS = vSimilarSpectra.find.where().between("base_peak_mz", (parentMass - error), (parentMass + error)).findList();
        Map<PairWrapper, org.expasy.mzjava.core.ms.peaklist.PeakList> peakListMap = new HashMap<PairWrapper, PeakList>();

        for(vSimilarSpectra sc: vS){
            org.expasy.mzjava.core.ms.peaklist.DoubleFloatPeakList peaks = new org.expasy.mzjava.core.ms.peaklist.DoubleFloatPeakList<>();

            List<String> items = Arrays.asList(sc.peaks.split("\\s*,\\s*"));

            for(String pK: items){
                String[] parts = pK.split("-");
                peaks.add(Double.parseDouble(parts[0]),Double.parseDouble(parts[1]));
            }

            peakListMap.put((new PairWrapper((int)(long) sc.scan_id, sc.glycan_sequence_id)),peaks);
        }
        return peakListMap;
    }

}

