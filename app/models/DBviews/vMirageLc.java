package models.DBviews;

import com.avaje.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;


@Entity
@Table(name="web.v_miragereport_lc")
public class vMirageLc extends Model {
    @Id

    public Long journal_reference_id;
    public String column_model;
    public String packing_material;
    public String column_size_width;
    public String column_size_length;
    public String type_chromatography;
    public String particle_size;
    public int profile;
    public String temperature;
    public String solvent_a;
    public String solvent_b;
    public String other_solvent;
    public String flow_rate;
    public String flow_gradient;
    public String run_time;
    public String phase;
    public String injector_type;
    public String injector_settings;
    public String inst_model;
    public String man_name;
    public String col_url;
    public String inst_name;
    public String inst_url;

    public static Find<Long,vMirageLc> find = new Find<Long,vMirageLc>(){};


    public static vMirageLc findByJournalId(Long id) {

        vMirageLc listLcs = find.where().eq("journal_reference_id",id).findUnique();
        return listLcs;
    }

}






