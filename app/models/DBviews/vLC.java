package models.DBviews;

import javax.persistence.*;
import com.avaje.ebean.Model;


@Entity
@Table(name="web.v_lc")
public class vLC extends Model {

    @Id
    public Long journal_reference_id;


    public String model;
    public String runrate;
    public String solvent_a;
    public String solvent_b;
    public String manufacturer_name;


    public static Find<Long,vLC> find = new Find<Long,vLC>(){};


    public static vLC findByJournalId(Long id) {
        return find.where().eq("journal_reference_id",id).findUnique();

    }


}