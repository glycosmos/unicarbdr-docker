package models.DBviews;

        import com.avaje.ebean.Model;

        import javax.persistence.Entity;
        import javax.persistence.Id;
        import javax.persistence.Table;
        import java.io.IOException;
        import java.util.*;


@Entity
@Table(name="web.v_journal_responsible")
public class vJournalResponsible extends Model {
    @Id
    public Long journal_reference_id;
    public String title;
    public String responsible_name;
    public String affiliation;
    public String contact_information;

    public static Find<Long,vJournalResponsible> find = new Find<Long,vJournalResponsible>(){};


    public static vJournalResponsible findByJournalId(Long id) {
        vJournalResponsible journalResp = find.where().eq("journal_reference_id",id).findUnique();
        return journalResp;
    }



}
