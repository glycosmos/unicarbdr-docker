package models.DBviews;

import com.avaje.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.IOException;
import java.util.*;


@Entity
@Table(name="web.v_mirageReport_sample")
public class vMirageSample extends Model {
    @Id
    public Long sample_id;
    public String general_info;
    public String growth_harvest;
    public String treatment_storage;
    public String vendor_item_info;
    public String generation_material;
    public String purification;
    public String journal_reference_id;
    public String sample_Name;
    public String starting_material;
    private String treatments;
    private String tissues;
    private String taxonomies;
    private String cell_lines;
    private String glycoproteins;



    public HashSet<String> getTaxonomies() {
        HashSet<String> taxons = new HashSet<String>();

        taxons.addAll(Arrays.asList(this.taxonomies.split("\\*\\*")));
        return taxons;
    }
    public HashSet<String> getTissues() {
        HashSet<String> tiss = new HashSet<String>();

        tiss.addAll(Arrays.asList(this.tissues.split("\\*\\*")));
        return tiss;
    }
    public HashSet<String> getCellLines() {
        HashSet<String> cells = new HashSet<String>();

        if(this.cell_lines !=null &&this.cell_lines.length()>1)
            cells.addAll(Arrays.asList(this.cell_lines.split("\\*\\*")));

        return cells;
    }
    public HashSet<String> getGlycoproteins() {
        HashSet<String> gps = new HashSet<String>();

        if(this.glycoproteins != null && this.glycoproteins.length()>1)
            gps.addAll(Arrays.asList(this.glycoproteins.split("\\*\\*")));
        return gps;
    }
    public HashSet<String> getTreatments() {
        HashSet<String> treats = new HashSet<String>();

        if(this.treatments !=null && this.treatments.length()>1)
            treats.addAll(Arrays.asList(this.treatments.split("\\*\\*")));
        return treats;
    }


    public void setTaxonomies(String taxonomies) {
        this.taxonomies=taxonomies;
    }
    public void setTissues(String tissues) {
        this.tissues=tissues;
    }
    public void setCell_lines(String cell_lines) {
        this.cell_lines=cell_lines;
    }
    public void setGlycoproteins(String glycoproteins) {
        this.glycoproteins=glycoproteins;
    }
    public void setTreatments(String treatments) {
        this.treatments=treatments;
    }


    public static Find<Long,vMirageSample> find = new Find<Long,vMirageSample>(){};

    public static List<vMirageSample> findByJournalId(Long id) {
        List<vMirageSample> listSamples = find.where().eq("journal_reference_id",id).findList();
        return listSamples;
    }



}






