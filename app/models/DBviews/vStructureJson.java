package models.DBviews;

import com.avaje.ebean.Model;

import javax.persistence.*;


@Entity
@Table(name="web.v_structure_json")
public class vStructureJson extends Model {

    @Id
    public Long scan_id;

    public String sequence_gws;
    public String sequence_ct;
    public double base_peak_mz;
    public String taxon;
    public String tissue_taxon;
    public int charge;
    public String reducing_end;


    public static Find<Long,vStructureJson> find = new Find<Long,vStructureJson>(){};


}