package models.DBviews;

import javax.persistence.*;

import com.avaje.ebean.Model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@Entity
@Table(name="web.v_biological_context")
public class vBiologicalContext extends Model{

    @Id
//    public Long sample_id;
    public Long scan_id;

    public String taxon;
    public int ncbi_id;
    public String tissue_taxon;
    public String mesh_id;
    public String glycoprotein_name;
    public String cell_line_name;
    public int journal_reference_id;

    public static Find<Long,vBiologicalContext> find = new Find<Long,vBiologicalContext>(){};


    public static List<vBiologicalContext> findbyScanId(Long id) {
        return find.where().eq("scan_id",id).findList();
    }

}


