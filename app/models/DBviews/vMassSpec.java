package models.DBviews;

import com.avaje.ebean.Model;
import play.mvc.Controller;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="web.v_mass_spec")
public class vMassSpec extends Model{
    @Id
    public Long journal_reference_id;

    public String manufacturer_name;
    public String model;
    public String ionisation_type;


    public static Find<Long,vMassSpec> find = new Find<Long,vMassSpec>(){};


    public static vMassSpec findByJournalId(Long id) {
        //KFK return find.where().eq("journal_reference_id",id).findUnique();
        if (find.where().eq("journal_reference_id",id).findList().size() > 0) {
            return find.where().eq("journal_reference_id",id).findList().get(0);
        } 
        vMassSpec dummy = new vMassSpec();
	dummy.journal_reference_id = new Long(-1);
	dummy.manufacturer_name = "";
	dummy.model = "";
	dummy.ionisation_type = "";
 	return dummy;
    }


}
