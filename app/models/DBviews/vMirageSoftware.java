package models.DBviews;

import com.avaje.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;


@Entity
@Table(name="web.v_miragereport_software")
public class vMirageSoftware extends Model {
    @Id
    public Long ms_to_software_id;
    public String journal_reference_id;
    public String software_id;
    public String name_software;
    public String version_software;
    public String manufacturer;
    public String task;
    public String files;



    public static Find<Long,vMirageSoftware> find = new Find<Long,vMirageSoftware>(){};

    public static List<vMirageSoftware> findByJournalId(Long id) {
        List<vMirageSoftware> listSoft = find.where().eq("journal_reference_id",id).findList();

        return listSoft;
    }

    public HashSet<String> getFiles() {
        HashSet<String> files = new HashSet<String>();

        if(this.files !=null &&this.files.length()>1)
            files.addAll(Arrays.asList(this.files.split("\\*\\*")));
        return files;
    }





}


