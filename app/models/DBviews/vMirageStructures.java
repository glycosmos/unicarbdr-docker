package models.DBviews;

import com.avaje.ebean.Model;

import javax.persistence.*;


@Entity
@Table(name="web.v_mirage_structures")
public class vMirageStructures extends Model {

    @Id
    public Long scan_id;

    public double base_peak_mz;
    public double retention_time;
    public String stability;
    public String orthogonal_approaches;
    public String quantity;
    public String status;
    public String validation_format;
    public String validation_level;
    public String validation_result;
    public String name_database;
    public String taxonomical_restrictions;
    public String other_restrictions;
    public String allowed_cleavages;
    public String mass_accuracy;
    public String parent_error;
    public String fragment_error;
    public String scoring_method;
    public String scoring_algorithm;
    public String scoring_result;
    public String scoring_format;


    public static Find<Long,vMirageStructures> find = new Find<Long,vMirageStructures>(){};


    public static vMirageStructures findByScanId(Long id) {
        vMirageStructures struct = find.where().eq("scan_id",id).findUnique();
        return struct;
    }


}