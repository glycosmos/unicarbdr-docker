package models.HPLC;

import com.avaje.ebean.Model;
import models.core.Manufacturer;

import javax.persistence.*;
import java.util.LinkedHashMap;
import java.util.Map;

@Entity
@Table(name="hplc.column")
public class Column extends Model {

    @Id
    public Long column_id;

//    public String manufacturer;
    public String model;
    public String type_chromatography;
    public String packing_material;
    public String column_size_width;
    public String column_size_length;
    public String particle_size;


    @OneToOne
    @JoinColumn(name="manufacturer_id")
    public Manufacturer manufacturer;

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "column")
    public LC lcColumn;

    public static Find<Long,Column> find = new Find<Long,Column>(){};


    public static Map<String,String> options() {
        LinkedHashMap<String,String> options = new LinkedHashMap<String,String>();
        for(Column c: Column.find.orderBy("model").findList()) {
            options.put(c.model, c.model);
        }
        return options;
    }


}