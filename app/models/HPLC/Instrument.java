package models.HPLC;

import com.avaje.ebean.Model;
import models.core.Manufacturer;

import javax.persistence.*;
import java.util.LinkedHashMap;
import java.util.Map;


@Entity
@Table(name="hplc.Instrument")
public class Instrument extends Model{

    @Id
    public Long instrument_id;

    public String model;

    @ManyToOne
    @JoinColumn(name="manufacturer_id")
    public Manufacturer manufacturer;

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "instrument")
    public LC lcInstrument;



    public static Find<Long,Instrument> find = new Find<Long,Instrument>(){};

    public static Map<String, String> optionsBrand() {
        LinkedHashMap<String, String> options = new LinkedHashMap<>();
        for(Instrument d: Instrument.find.orderBy("model").findList()) {
            options.put(d.model, d.model);
        }
        return options;
    }
}
