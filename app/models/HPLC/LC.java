package models.HPLC;

import com.avaje.ebean.Model;
import com.google.inject.Inject;
import models.MS.Scan;
import models.core.JournalReference;

import javax.persistence.*;


@Entity
@Table(name="hplc.lc")
public class LC extends Model{
    @Id
    public Long lc_id;

    @OneToOne
    @JoinColumn(name = "method_run_id")
    public MethodRun methodRun;

    @OneToOne
    @JoinColumn(name = "column_id")
    public Column column;

    @OneToOne
    @JoinColumn(name = "instrument_id")
    public Instrument instrument;

    @OneToOne
    @JoinColumn(name = "injector_id")
    public Injector injector;

    @OneToOne
    @JoinColumn(name="journal_Reference_id")
    public JournalReference journalReference;


/*    @OneToOne(cascade = CascadeType.ALL, mappedBy = "lc")
    public Scan scanToLc;
*/

}