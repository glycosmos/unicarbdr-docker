package models.HPLC;
import com.avaje.ebean.Model;
import models.core.Manufacturer;

import javax.persistence.*;


@Entity
@Table(name="hplc.injector")
public class Injector extends Model{
    @Id
    public Long injector_id;

    public String injector_type;
    public String injector_settings;

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "injector")
    public LC lcInjector;


    public static Find<Long,Injector> find = new Find<Long,Injector>(){};
}