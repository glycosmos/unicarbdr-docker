package models.samplePrep;

import com.avaje.ebean.Model;
import service.SelectOption;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.LinkedHashMap;
//import java.util.List;
import java.util.List;
import java.util.Map;

@Entity
@Table(name="samplePrep.taxonomy")
public class Taxonomy extends Model {

    @Id
    public Long taxonomy_id;

    public Taxonomy parentTaxonomy;

    public int ncbi_id;
    public String rank;
    public String taxon;


    @OneToMany(cascade = CascadeType.ALL, mappedBy = "taxonomy")
    public List<Sample> sampleTax;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "taxonomy")
    public List<Glycoprotein> glycoproteinTax;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "taxonomyEx")
    public List<Glycoprotein> glycoproteinExTax;


    public static Find<Long,Taxonomy> find = new Find<Long,Taxonomy>(){};

    public static Map<String,String> options() {
        LinkedHashMap<String,String> options = new LinkedHashMap<>();
        for(Taxonomy t: Taxonomy.find.where().ilike("taxon","homo%").eq("rank","species").orderBy("taxon").findList()) {
            if(options.size()<150) {                                      //Borrar
                options.put(t.taxonomy_id.toString(), t.taxon);
            }
        }
        return options;
    }

    public static Map<String,String> optionsAuto(String term) {
        LinkedHashMap<String,String> options = new LinkedHashMap<>();
        for(Taxonomy t: Taxonomy.find.where().ilike("taxon", term + "%").eq("rank","species").orderBy("taxon").select("taxonomy_id").select("taxon").findList()) {
                options.put(t.taxonomy_id.toString(), t.taxon);
        }
        return options;
    }

    public static List<SelectOption> optionsAutoTwo(String term) {
        List<SelectOption> list = new ArrayList<>();
        for(Taxonomy t: Taxonomy.find.where().ilike("taxon", term + "%").eq("rank","species").orderBy("taxon").select("taxonomy_id").select("taxon").findList()) {
            list.add(new SelectOption(t.taxon, t.taxon));
        }
        return list;
    }


    public static Taxonomy findById(Long id) {
        return find.where().eq("taxonomy_id",id).select("taxonomy_id").findUnique();

    }


}
