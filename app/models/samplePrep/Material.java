package models.samplePrep;

import com.avaje.ebean.Model;
import models.samplePrep.Sample;
import scala.collection.parallel.mutable.ParArray;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Entity
@Table(name="samplePrep.material")
public class Material extends Model{

    @Id
    public Long material_id;
    public String description;
    public String material_type;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "sampleName")
    public List<Sample> sampleMatName;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "startingMaterial")
    public List<Sample> sampleMatStart;

    public static Find<Long,Material> find = new Find<Long,Material>(){};

    public static Map<String,String> options() {
        LinkedHashMap<String,String> options = new LinkedHashMap<String,String>();
        for(Material d: Material.find.findList()) {
            options.put(d.description.toString(), d.description);
        }
        return options;
    }
    public static Map<String, String> optionsStarting() {
        LinkedHashMap<String, String> options = new LinkedHashMap<>();
        for(Material d: Material.find.where().eq("material_type", 1).findList()) {
            options.put(d.description, d.description);
        }
        return options;
    }

    public static Map<String, String> optionsSample() {
        LinkedHashMap<String, String> options = new LinkedHashMap<>();
        for(Material d: Material.find.where().eq("material_type", 2).findList()) {
            options.put(d.description, d.description);
        }
        return options;
    }

}

