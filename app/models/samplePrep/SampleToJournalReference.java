package models.samplePrep;

import com.avaje.ebean.Model;

import javax.persistence.*;

import models.MS.Scan;
import models.core.JournalReference;

import java.util.List;

@Entity
@Table(name="samplePrep.sample_to_journal_reference")

public class SampleToJournalReference extends Model {
    @Id
    public Long sample_to_journal_reference_id;

    @ManyToOne
    @JoinColumn(name="sample_id")
    public Sample sample;

    @ManyToOne
    @JoinColumn(name="journal_reference_id")
    public JournalReference journalReference;


    @OneToMany(cascade = CascadeType.ALL, mappedBy = "sampleToJournalReference")
    public List<Scan> scanSampleJRef;


    public static Find<Long,SampleToJournalReference> find = new Find<Long,SampleToJournalReference>(){};

}