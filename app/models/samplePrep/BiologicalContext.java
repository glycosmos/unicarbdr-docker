
package models.samplePrep;

import com.avaje.ebean.Model;
import javax.persistence.*;
import java.util.List;
import java.util.ArrayList;
import java.util.*;

@Entity
@Table(name="samplePrep.biological_context")
public class BiologicalContext extends Model {

    @Id
    public Long biological_context_id;
/*
    @OneToOne
    @JoinColumn(name="taxonomy_id")
    public Taxonomy taxonomy;

    @OneToOne
    @JoinColumn(name="tissue_taxonomy_id")
    public Tissue tissue;

    @OneToOne
    @JoinColumn(name="glycoprotein_id")
    public Glycoprotein glycoprotein;


    @OneToOne(cascade = CascadeType.ALL, mappedBy = "biologicalContext")
    public Sample sampleBioCont;


    public static Find<Long,BiologicalContext> find = new Find<Long,BiologicalContext>(){};


*/
/*    public static List<BiologicalContext> findbyJournalId(Long id) {

        return find.where().eq("metadataBC.journalReference.journal_reference_id",id).findList();

    }
*//*

    public static int[] getCounts() {
        int[] counts = new int[2];

        List<BiologicalContext> contexts =
                find.fetch("tissue","tissue_taxonomy_id").fetch("taxonomy", "taxonomy_id").findList();

        List<Long> uniqueTissues = new ArrayList<>();
        List<Long> uniqueTaxonomies = new ArrayList<>();
        for(BiologicalContext bc: contexts){
            if(bc.tissue != null
                    && bc.tissue.tissue_taxonomy_id != null
                    && !uniqueTissues.contains(bc.tissue.tissue_taxonomy_id)) {
                uniqueTissues.add(bc.tissue.tissue_taxonomy_id);
            }

            if(bc.taxonomy != null
                    && bc.taxonomy.taxonomy_id != null
                    && !uniqueTaxonomies.contains(bc.taxonomy.taxonomy_id)) {
                uniqueTaxonomies.add(bc.taxonomy.taxonomy_id);
            }
        }

        counts[0] = uniqueTissues.size();
        counts[1] = uniqueTaxonomies.size();

        return counts;
    }


    public static List<Map<String,String>> findTaxTissueList() {
        LinkedHashMap<String,String> taxs = new LinkedHashMap<String,String>();
        LinkedHashMap<String,String> tissues = new LinkedHashMap<String,String>();

        List<Map<String,String>> taxTiss = new ArrayList<>();


        for(BiologicalContext tax: BiologicalContext.find.orderBy("taxonomy.taxon").findList()) {
            taxs.put(tax.taxonomy.taxonomy_id.toString(), tax.taxonomy.taxon);
            tissues.put(tax.tissue.tissue_taxonomy_id.toString(), tax.tissue.tissue_taxon);
        }
        taxTiss.add(taxs);
        taxTiss.add(tissues);

        return taxTiss;
    }
*/
}
