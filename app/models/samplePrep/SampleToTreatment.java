package models.samplePrep;

import com.avaje.ebean.Model;

import javax.persistence.*;

@Entity
@Table(name="samplePrep.sample_to_treatment")
public class SampleToTreatment extends Model{

    @Id
    public Long sample_to_treatment_id;

    @ManyToOne
    @JoinColumn(name="treatment_id")
    public Treatment treatment;


    @ManyToOne
    @JoinColumn(name="sample_id")
    public Sample sample;


    public String purpose;
    public String reaction_conditions;
    public String vendor_expression;
    public String origin_glycosidase;
    public String type_modification;


    public static Find<Long,SampleToTreatment> find = new Find<Long,SampleToTreatment>(){};

}
