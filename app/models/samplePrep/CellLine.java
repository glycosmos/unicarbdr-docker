package models.samplePrep;


import com.avaje.ebean.Model;
import models.samplePrep.Sample;
import service.SelectOption;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Entity
@Table(name="samplePrep.cell_line")

public class CellLine extends Model{
    @Id
    public Long cell_line_id;

    public String cell_line_name;
    public String owl_id;
    public String comment;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cellLine")
    public List<Sample> sampleCellLine;

    public static Find<Long,CellLine> find = new Find<Long,CellLine>(){};

    public static Map<String,String> options() {
        LinkedHashMap<String,String> options = new LinkedHashMap<String,String>();
        for(CellLine t: CellLine.find.orderBy("cell_line_name").findList()) {
            if(options.size()<150) {                                      //Borrar
                options.put(t.cell_line_id.toString(), t.cell_line_name);
            }
        }
        return options;
    }

    public static List<SelectOption> optionsAutoTwo(String term) {
        List<SelectOption> list = new ArrayList<>();
        for(CellLine c: CellLine.find.where().ilike("cell_line_name", term + "%").orderBy("cell_line_name").select("cell_line_id").select("cell_line_name").findList()) {
            list.add(new SelectOption(c.cell_line_name, c.cell_line_name));
        }
        return list;
    }
}

