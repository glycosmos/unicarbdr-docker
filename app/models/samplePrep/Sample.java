package models.samplePrep;

import com.avaje.ebean.Model;
import models.core.JournalReference;
import service.SelectOption;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Entity
@Table(name="samplePrep.sample")

public class Sample extends Model{
    private static final Logger sampleLogger = LoggerFactory.getLogger(Sample.class);

    @Id
    public Long sample_id;

    public String general_info;
    public String growth_harvest;
    public String treatment_storage;
    public String vendor_item_info;
    public String generation_material;
    public String purification;

/*    @OneToOne
    @JoinColumn(name="biological_context_id")
    public BiologicalContext biologicalContext;
*/

    @ManyToOne
    @JoinColumn(name="taxonomy_id")
    public Taxonomy taxonomy;

    @ManyToOne
    @JoinColumn(name="tissue_taxonomy_id")
    public Tissue tissue;

    @ManyToOne
    @JoinColumn(name="glycoprotein_id")
    public Glycoprotein glycoprotein;


    @ManyToOne
    @JoinColumn(name="sample_name_id")
    public Material sampleName;

    @ManyToOne
    @JoinColumn(name="starting_material_id")
    public Material startingMaterial;

    @ManyToOne
    @JoinColumn(name="cell_line_id")
    public CellLine cellLine;

/*    @ManyToOne
    @JoinColumn(name="journal_Reference_id")
    public JournalReference journalReference;
*/

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "sample")
    public List<SampleToTreatment> sampleToTreSamp;



    public static Find<Long,Sample> find = new Find<Long,Sample>(){};



    public static int[] getCounts() {
        int[] counts = new int[2];

        List<Sample> contexts =
                find.fetch("tissue","tissue_taxonomy_id").fetch("taxonomy", "taxonomy_id").findList();

        List<Long> uniqueTissues = new ArrayList<>();
        List<Long> uniqueTaxonomies = new ArrayList<>();
        for(Sample sp: contexts){
            if(sp.tissue != null
                    && sp.tissue.tissue_taxonomy_id != null
                    && !uniqueTissues.contains(sp.tissue.tissue_taxonomy_id)) {
                uniqueTissues.add(sp.tissue.tissue_taxonomy_id);
            }

            if(sp.taxonomy != null
                    && sp.taxonomy.taxonomy_id != null
                    && !uniqueTaxonomies.contains(sp.taxonomy.taxonomy_id)) {
                uniqueTaxonomies.add(sp.taxonomy.taxonomy_id);
            }
        }

        counts[0] = uniqueTissues.size();
        counts[1] = uniqueTaxonomies.size();

        return counts;
    }




    public static List<Map<String,String>> findTaxTissueList() {
        LinkedHashMap<String,String> taxs = new LinkedHashMap<String,String>();
        LinkedHashMap<String,String> tissues = new LinkedHashMap<String,String>();

        List<Map<String,String>> taxTiss = new ArrayList<>();

	sampleLogger.error("Getting taxonomy having length={}", Sample.find.orderBy("taxonomy.taxon").findList().size());

        for(Sample tax: Sample.find.orderBy("taxonomy.taxon").findList()) {
	    sampleLogger.error("tax.taxonomy={}", tax.taxonomy);
	    if (tax.taxonomy != null) { sampleLogger.error("tax.taxonomy.taxonomy_id={}", tax.taxonomy.taxonomy_id); }
	    if (tax.taxonomy != null && tax.taxonomy.taxonomy_id != null) {
                taxs.put(tax.taxonomy.taxonomy_id.toString(), tax.taxonomy.taxon);
 	    }
	    sampleLogger.error("tax.tissue={}", tax.tissue);
	    if (tax.tissue != null) {sampleLogger.error("tax.tissue.tissue_taxonomy_id={}", tax.tissue.tissue_taxonomy_id);}
	    if (tax.tissue != null && tax.tissue.tissue_taxonomy_id != null) {
                tissues.put(tax.tissue.tissue_taxonomy_id.toString(), tax.tissue.tissue_taxon);
            }
        }
        taxTiss.add(taxs);
        taxTiss.add(tissues);

        return taxTiss;
    }



    public static Sample findbyJournalId(Long id) {
        return find.where().eq("journal_reference_id",id).findUnique();
    }



}





