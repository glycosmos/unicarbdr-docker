package models.samplePrep;

import com.avaje.ebean.Model;
import models.samplePrep.BiologicalContext;
import models.samplePrep.Taxonomy;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="samplePrep.glycoprotein")
public class Glycoprotein extends Model {

    @Id
    public Long glycoprotein_id;
    public String uniprot_id;
    public String glycoprotein_name;
    public String description;

    @ManyToOne
    @JoinColumn(name="native_species")
    public Taxonomy taxonomy;


    @ManyToOne
    @JoinColumn(name="expressed_species")
    public Taxonomy taxonomyEx;


    @OneToMany(cascade = CascadeType.ALL, mappedBy = "glycoprotein")
    public List<Sample> sampleGlycoprotein;


}


