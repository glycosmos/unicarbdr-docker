package models.samplePrep;

import com.avaje.ebean.Model;
import service.SelectOption;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name="samplePrep.tissue_taxonomy")
public class Tissue extends Model {

    @Id
    public Long tissue_taxonomy_id;

    public Tissue parentTissue;
    public String tissue_taxon;
    public String mesh_id;
    public String description;
    public Date date_last_modified;


    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tissue")
    public List<Sample> sampleTissue;


    public static Find<Long,Tissue> find = new Find<Long,Tissue>(){};

    public static Map<String,String> options() {
        LinkedHashMap<String,String> options = new LinkedHashMap<String,String>();
        for(Tissue t: Tissue.find.orderBy("tissue_taxon").findList()) {
            options.put(t.tissue_taxonomy_id.toString(), t.tissue_taxon);
        }
        return options;
    }

    public static Tissue findById(Long id) {
        return find.where().eq("tissue_taxonomy_id",id).select("tissue_taxonomy_id").findUnique();

    }

    public static List<SelectOption> optionsAutoTwo(String term) {
        List<SelectOption> list = new ArrayList<>();
        for(Tissue t: Tissue.find.where().ilike("tissue_taxon", term + "%").orderBy("tissue_taxon").select("tissue_taxonomy_id").select("tissue_taxon").findList()) {
            list.add(new SelectOption(t.tissue_taxon, t.tissue_taxon));
        }
        return list;
    }
}
