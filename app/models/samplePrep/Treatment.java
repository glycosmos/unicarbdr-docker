package models.samplePrep;

import com.avaje.ebean.Model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="samplePrep.treatment")
public class Treatment extends Model{
    @Id
    public Long treatment_id;


    public String treatment_name;
    public String treatment_type;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "treatment")
    public List<SampleToTreatment> sampleToTreTre;


    public static Find<Long,Treatment> find = new Find<Long,Treatment>(){};

    public static List<String> optionsIsoEnzymaticL() {
        List<String> options = new ArrayList<String>();
//        for(Treatment d: Treatment.find.where().eq("treatment_type", 1).setOrderBy("treatment_name").findList()) {
        for(Treatment d: Treatment.find.where().gt("treatment_type", 8).setOrderBy("treatment_name").findList()) {
            options.add(d.treatment_name);
        }
        return options;
    }

    public static List<String> optionsIsoChemicalL() {
        List<String> options = new ArrayList<String>();
//        for(Treatment d: Treatment.find.where().eq("treatment_type", 2).setOrderBy("treatment_name").findList()) {
        for(Treatment d: Treatment.find.where().gt("treatment_type", 8).setOrderBy("treatment_name").findList()) {
            options.add(d.treatment_name);
        }
        return options;
    }

    public static List<String> optionsModEnzymaticL() {
        List<String> options = new ArrayList<String>();
//        for(Treatment d: Treatment.find.where().eq("treatment_type", 1).setOrderBy("treatment_name").findList()) {
        for(Treatment d: Treatment.find.where().gt("treatment_type", 8).setOrderBy("treatment_name").findList()) {
            options.add(d.treatment_name);
        }
        return options;
    }

    public static List<String> optionsModChemicalL() {
        List<String> options = new ArrayList<String>();
//        for(Treatment d: Treatment.find.where().eq("treatment_type", 2).setOrderBy("treatment_name").findList()) {
        for(Treatment d: Treatment.find.where().gt("treatment_type", 8).setOrderBy("treatment_name").findList()) {
            options.add(d.treatment_name);
        }
        return options;
    }

}
