package models;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Model;

import javax.persistence.*;
import java.util.List;


@Entity
@Table(name="hplc.lcmucin")
public class Lcmucin extends Model {

    @Id
    public Long lcmucin_id;

    /*
    @OneToOne
    @JoinColumn(name="glycan_sequence_id")
    public GlycanSequence glycanSequence;


    @OneToOne
    @JoinColumn(name="scan_id")
    public Scan scan;


    @OneToOne
    @JoinColumn(name="persubstitution_id")
    public Persubstitution persubstitution;

*/


    public String gwpname;
    public String ionization_method;
    public double retention_time;
    public String note;

    /*
    @OneToOne
    @JoinColumn(name="reducing_end_id")
    public ReducingEnd reducingEnd;

    public Boolean show;


    @ManyToOne
    @JoinColumn(name="metadata_id")
    public Metadata metadata;


*/

    public static Find<Long,Lcmucin> find = new Find<Long,Lcmucin>(){};

    public static List<Lcmucin> searchMsData() {

        List<Lcmucin> lcmucins = Ebean.find(Lcmucin.class)
                .where().in("metadata.journal_reference.journal_reference_id",270,272,273,323,324,327,328,304,305,320,321,325,326,330,331,332,333,335).eq("show_hide", Boolean.FALSE).isNotNull("glycan_sequence.sequence_kcf").orderBy("lcmucin_id").findList();
        return lcmucins;

    }


    public static List<Lcmucin> findByMetadata(Long id) {
        return find.where().eq("metadata_id",id).findList();
    }



}
