package models.core;

import javax.persistence.*;
import com.avaje.ebean.Model;

import models.HPLC.LC;
import models.MS.MS;
import models.MS.Scan;
import models.samplePrep.Sample;
import play.data.validation.*;
import com.avaje.ebean.*;

import java.util.List;

@Entity
@Table(name="core.journal_reference")
public class JournalReference extends Model{

    @Id
    public Long journal_reference_id;

    public int pubmed_id;

    @Constraints.Required
    public String authors;
    @Constraints.Required
    public String title;
    public int publication_year;
    public Long journal_volume;
    public Long journal_start_page;
    public Long journal_end_page;
    public Boolean show;
    public int user_id;

/*    @OneToOne(cascade = CascadeType.ALL, mappedBy = "journalReference")
    public Metadata metadataJRef;
*/

    @ManyToOne
    @JoinColumn(name="journal_id")
    public Journal journal;

/*
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "journalReference")
    public List<Scan> scanJRef;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "journalReference")
    public List<Sample> sampleJRef;
*/

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "journalReference")
    public LC lcJRef;

//    @OneToOne(cascade = CascadeType.ALL, mappedBy = "journalReference")
//    public MS msJRef;

    @ManyToOne
    @JoinColumn(name="responsible_id")
    public Responsible responsible;

    public static int getCount() {
        return Ebean.find(JournalReference.class).where().eq("show",true).findRowCount();
    }

    public static Find<Long,JournalReference> find = new Find<Long,JournalReference>(){};

    public static JournalReference findById(int input) {
        return JournalReference.find.where().eq("journal_reference_id", input).findList().get(0);
    }
}
