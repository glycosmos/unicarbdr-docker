package models.core;

        import javax.persistence.*;

        import com.avaje.ebean.Model;

        import java.util.List;


@Entity
@Table(name="core.responsible")
public class Responsible extends Model {

    @Id
    public Long responsible_id;
    public String responsible_name;
    public String affiliation;
    public String contact_information;


    @OneToMany(cascade = CascadeType.ALL, mappedBy = "responsible")
    public List<JournalReference> jRefResp;




}


