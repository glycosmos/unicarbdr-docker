package models.core;

import javax.persistence.*;

import com.avaje.ebean.Model;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


@Entity
@Table(name="core.journal")
public class Journal extends Model {

    @Id
    public Long journal_id;
    public String journal_title;
    public String journal_abbrev;


    @OneToMany(cascade = CascadeType.ALL, mappedBy = "journal")
    public List<JournalReference> jRefJ;



    public static Find<Long,Journal> find = new Find<Long,Journal>(){};

    public static Map<String,String> options() {
        LinkedHashMap<String,String> options = new LinkedHashMap<String,String>();
        for(Journal j: Journal.find.orderBy("journal_title").findList()) {
            options.put(j.journal_id.toString(), j.journal_title);
        }
        return options;
    }

    public static Journal findById(Long id) {
        return find.where().eq("journal_id",id).select("journal_id").findUnique();

    }

    public static Journal findByTitle(String title) {
        return find.where().eq("journal_title",title).findUnique();

    }


}
