package models.core;

import com.avaje.ebean.Model;
import javax.persistence.*;

@Entity
@Table(name="core.glycan_composition")
public class GlycanComposition extends Model {

    @Id
    public Long glycan_composition_id;

    private double HEXNAC = 203.0794;
    private double HEX = 162.0528;
    private double DHEX= 146.0579;
    private double NEUAC= 291.0954;
    private double NEUGC=307.0903;
    private double SULF=79.9568;
    private double GLURO=176.03209;
    private double REDEND = 18.0105546;
    private double PENTO = 132.0423;
    private double METHYL = 14.0271;




    public int pen;
    public int dhex;
    public int hex;
    public int hexnac;
    public int neugc;
    public int neuac;
    public int sulf;
    public int gluro;
    public int methyl;
    public double mass;



/*    @OneToOne
    @JoinColumn(name="glycan_sequence_id")
    public GlycanSequence glycanSequence;
*/
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "glycanComposition")
    public GlycanSequence glycanSeqCompo;


    public void setMass(){
        this.mass = (this.pen*PENTO) + (this.dhex*DHEX) + (this.hex*HEX) + (this.hexnac*HEXNAC) + (this.neugc*NEUGC) + (this.neuac*NEUAC) + (this.sulf*SULF) + (this.gluro*GLURO) + (this.methyl*METHYL) + REDEND;
    }



/*
    public static void saveComposition (GlycanSequence gSeq, Map<String,Double> composition){

        GlycanComposition gComp= new GlycanComposition();
        if (gSeq.glycan_sequence_id>0) {

            for (Map.Entry<String, Double> entry : composition.entrySet()) {

                String key=entry.getKey().toString();

                switch (key) {
                    case "hex":
                        gComp.hex=entry.getValue().intValue();
                        break;
                    case "hexNac":
                        gComp.hexnac =entry.getValue().intValue();
                        break;
                    case "dHex":
                        gComp.dhex =entry.getValue().intValue();
                        break;
                    case "neuAc":
                        gComp.neuac =entry.getValue().intValue();
                        break;
                    case "sulf":
                        gComp.sulf =entry.getValue().intValue();
                        break;
                    case "gluronic":
                        gComp.gluro =entry.getValue().intValue();
                        break;
                    case "mass":
                        gComp.mass=entry.getValue();
                        break;

                    default:
                        throw new IllegalArgumentException("Invalid key: " + key);
                }
            }
            gComp.glycanSequence=gSeq;
            gComp.save();
        }

    }
*/



}
