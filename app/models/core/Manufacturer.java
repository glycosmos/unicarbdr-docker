package models.core;

import com.avaje.ebean.Model;
import models.HPLC.*;
import models.MS.Device;
import models.samplePrep.Material;

import javax.persistence.*;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Entity
@Table(name="core.manufacturer")
public class Manufacturer extends Model {

    @Id
    public Long manufacturer_id;

    public String manufacturer_name;
    public String url;
    public boolean hplc;
    public boolean column;
    public boolean spectrometer;


    @OneToMany(cascade = CascadeType.ALL, mappedBy = "manufacturer")
    public List<Device> deviceManuf;


    @OneToMany(cascade = CascadeType.ALL, mappedBy = "manufacturer")
    public List<models.HPLC.Column> columnManuf;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "manufacturer")
    public List<Instrument> instrumentManuf;


    public static Find<Long,Manufacturer> find = new Find<Long,Manufacturer>(){};

    public static Map<String,String> options() {
        LinkedHashMap<String,String> options = new LinkedHashMap<String,String>();
        for(Manufacturer d: Manufacturer.find.orderBy("model").findList()) {
            options.put(d.manufacturer_id.toString(), d.manufacturer_name);
//            System.out.println(d.manufacturer_name);
        }
        return options;
    }

    public static Map<String, String> optionsHPLC() {
        LinkedHashMap<String, String> options = new LinkedHashMap<>();
        for(Manufacturer d: Manufacturer.find.where().eq("hplc", true).findList()) {
            options.put(d.manufacturer_name, d.manufacturer_name);
        }
        return options;
    }

    public static Map<String, String> optionsColumn() {
        LinkedHashMap<String, String> options = new LinkedHashMap<>();
        for(Manufacturer d: Manufacturer.find.where().eq("column", true).findList()) {
            options.put(d.manufacturer_name, d.manufacturer_name);
        }
        return options;
    }

    public static Map<String, String> optionsSpectrometer() {
        LinkedHashMap<String, String> options = new LinkedHashMap<>();
        for(Manufacturer d: Manufacturer.find.orderBy("manufacturer_name").where().eq("spectrometer", true).findList()) {
            options.put(d.manufacturer_name, d.manufacturer_name);
        }
        return options;
    }
}
