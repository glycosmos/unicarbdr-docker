package models.core;

import com.avaje.ebean.Model;
import javax.persistence.*;

import models.MS.Scan;

import java.util.List;

@Entity
@Table(name="core.glycan_sequence")
public class GlycanSequence extends Model {

    @Id
    public Long glycan_sequence_id;

    public String sequence_iupac;
    public String sequence_ct;
    public String sequence_ct_condensed;
    public String sequence_gws;
//    private Date date_entered;
    public String glytoucan_id;
    public String sequence_kcf;
    public int contributor_id;

/*    @OneToOne(cascade = CascadeType.ALL, mappedBy = "glycanSequence")
    public Scan scanGlySeq;



    public static Find<Long,GlycanSequence> find = new Find<Long,GlycanSequence>(){};

    public static GlycanSequence findByStructure(String seqGWS) {
        GlycanSequence gSequence = find.where().eq("sequence_gws", seqGWS).select("glycan_sequence_id").findUnique();

        return gSequence;
    }
*/

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "glycanSequence")
    public Scan scanGlySeq;


/*    @OneToOne(cascade = CascadeType.ALL, mappedBy = "glycanSequence")
    public GlycanComposition glyCompGlySeq;
*/

    @OneToOne
    @JoinColumn(name="glycan_composition_id")
    public GlycanComposition glycanComposition;


    public static Find<Long,GlycanSequence> find = new Find<Long,GlycanSequence>(){};

    public static GlycanSequence findByStructure(String seqGWS) {
        GlycanSequence gSequence = find.where().eq("sequence_gws", seqGWS).select("glycan_sequence_id").findUnique();

        return gSequence;
    }

    public static GlycanSequence findById(int input) {
        return GlycanSequence.find.where().eq("glycan_sequence_id", input).findList().get(0);
    }










    public GlycanSequence saveGlycanSequence (String gwsSeq, String ctSequence){

        GlycanSequence gSeqExist = GlycanSequence.findByStructure(gwsSeq);
        if (gSeqExist == null) {
            gSeqExist = new GlycanSequence();
            gSeqExist.sequence_gws = gwsSeq;
            gSeqExist.sequence_ct = ctSequence;
            gSeqExist.contributor_id = 4191;

            gSeqExist.save();

        }else
            System.out.println("Ya existe " + gwsSeq);

        return gSeqExist;
    }


}










