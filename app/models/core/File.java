package models.core;

import com.avaje.ebean.Model;
import models.MS.MSToSoftware;
import models.MS.ScanToDatabase;

import javax.persistence.*;

@Entity
@Table(name="ms.file")
public class File extends Model{
    @Id
    public Long file_id;

    public String file_name;
    public String format;
    public String url;
    public String identification_target_area;


    @ManyToOne
    @JoinColumn(name="ms_to_software_id")
    public MSToSoftware msToSoftware;


    @OneToOne
    @JoinColumn(name="scan_to_database_id")
    public ScanToDatabase scanToDatabase;

}




