package models.lists;

import javax.persistence.Entity;
import javax.persistence.Table;
import com.avaje.ebean.Model;
import javax.persistence.*;
import java.util.*;


@Entity
@Table(name="mirage.starting_material")
public class StartingMaterial extends Model{

    @Id
    Long starting_material_id;

    String description;

    public static Find<Long,StartingMaterial> find = new Find<Long,StartingMaterial>(){};

    public static Map<String,String> options() {
        LinkedHashMap<String,String> options = new LinkedHashMap<String,String>();
        for(StartingMaterial d: StartingMaterial.find.findList()) {
            options.put(d.description.toString(), d.description);
        }
        return options;
    }
}
