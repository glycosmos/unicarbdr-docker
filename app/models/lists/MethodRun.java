package models.HPLC;

import com.avaje.ebean.Model;
import javax.persistence.*;

@Entity
@Table(name="hplc.method_run")
public class MethodRun extends Model {

    @Id
    public Long method_run_id;

    public int profile;
    public String temperature;
    public String solvent_a;
    public String solvent_b;
    public String other_solvent;
    public String flow_rate;
    public String flow_gradient;
    public String run_time;
    public String phase;

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "methodRun")
    public LC lcMethodRun;

}


