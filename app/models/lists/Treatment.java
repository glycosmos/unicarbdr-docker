package models.lists;


import com.avaje.ebean.Model;
import javax.persistence.*;

import java.util.*;


@Entity
@Table(name="samplePrep.treatment")
public class Treatment extends Model {

    @Id
    public Long treatment_id;

    public String treatment_name;
    public String treatment_type;

    public static Find<Long,Treatment> find = new Find<Long,Treatment>(){};

    public static Map<String,String> optionsIso() {
        LinkedHashMap<String,String> options = new LinkedHashMap<String,String>();
        for(Treatment d: Treatment.find.where().eq("treatment_type", 1).findList()) {
            options.put(d.treatment_id.toString(), d.treatment_name);
        }
        return options;
    }

    public static Map<String,String> optionsMod() {
        LinkedHashMap<String,String> options = new LinkedHashMap<String,String>();
        for(Treatment d: Treatment.find.where().eq("treatment_type", 2).findList()) {
            options.put(d.treatment_id.toString(), d.treatment_name);
        }
        return options;
    }

    public static List<String> optionsIsoL() {
        List<String> options = new ArrayList<String>();
        for(Treatment d: Treatment.find.where().eq("treatment_type", "1").findList()) {
            options.add(d.treatment_name);
        }
        return options;
    }

    public static List<String> optionsModL() {
        List<String> options = new ArrayList<String>();
        for(Treatment d: Treatment.find.where().eq("treatment_type", "2").findList()) {
            options.add(d.treatment_name);
        }
        return options;
    }
}
