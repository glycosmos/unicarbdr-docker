package models.MS;

import com.avaje.ebean.Model;

import javax.persistence.*;


@Entity
@Table(name="ms.scan_to_ion")
public class ScanToIon extends Model {

    @Id
    public Long scan_to_ion_id;

    @ManyToOne
    @JoinColumn(name="scan_id")
    public Scan scan;


    @ManyToOne
    @JoinColumn(name="ion_id")
    public Ion ion;


    public int charge;
    public boolean gain;
    public String neutralexchange;

    public static Find<Long,ScanToIon> find = new Find<Long,ScanToIon>(){};

}
