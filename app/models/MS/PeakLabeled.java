package models.MS;

import com.avaje.ebean.Model;
import javax.persistence.*;
import java.util.Comparator;
import java.util.List;
import java.util.ArrayList;

@Entity
@Table(name="ms.peak_labeled")
public class PeakLabeled extends Model {

    @Id
    public Long peak_labeled_id;


    public double mz_value;
    public double intensity_value;


    @OneToMany(cascade = CascadeType.ALL, mappedBy = "peakLabeled")
    public List<PeakAnnotated> pAnnotLab = new ArrayList<PeakAnnotated>();

    @ManyToOne
    @JoinColumn(name="peak_list_id")
    public PeakList peakList;


    public double getMz_value(){
        return mz_value;
    }

    public static Comparator<PeakLabeled> StuMzComparator = new Comparator<PeakLabeled>() {
        public int compare(PeakLabeled s1, PeakLabeled s2) {
            Double mz1 = s1.getMz_value();
            Double mz2 = s2.getMz_value();

            return mz1.compareTo(mz2);
        }
    };


}
