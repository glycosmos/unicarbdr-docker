package models.MS;

import com.avaje.ebean.Model;

import javax.persistence.*;

@Entity
@Table(name="ms.cid")

public class CID extends Model{
    @Id
    public Long cid_id;

    public String gas_composition;
    public String gas_pressure;
    public String collision_energy;

    @OneToOne
    @JoinColumn(name="collision_cell_id")
    public CollisionCell collisionCell;


}
