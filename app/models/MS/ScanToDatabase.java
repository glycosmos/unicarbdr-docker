package models.MS;

import com.avaje.ebean.Model;
import models.core.File;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="ms.scan_to_database")
public class ScanToDatabase extends Model{
    @Id
    public Long scan_to_database_id;

    public String taxonomical_restrictions;
    public String other_restrictions;
    public String allowed_cleavages;
    public String mass_accuracy;
    public String parent_error;
    public String fragment_error;

    public String scoring_method;
    public String scoring_algorithm;
    public String scoring_result;
    public String scoring_format;


    @OneToOne
    @JoinColumn(name="scan_id")
    public Scan scan;

    @ManyToOne
    @JoinColumn(name="database_id")
    public Database database;

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "scanToDatabase")
    public File fileScanToData;


}
