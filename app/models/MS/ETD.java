package models.MS;

import com.avaje.ebean.Model;

import javax.persistence.*;

@Entity
@Table(name="ms.etd")

public class ETD extends Model{
    @Id
    public Long etd_id;

    public String reagent_gas;
    public String pressure;
    public String reaction_time;
    public String reagent_atoms;

    @OneToOne
    @JoinColumn(name="collision_cell_id")
    public CollisionCell collisionCell;


}
