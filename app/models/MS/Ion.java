package models.MS;

import com.avaje.ebean.Model;

import javax.persistence.*;
import java.util.List;


@Entity
@Table(name = "ms.ion")
public class Ion extends Model {

    @Id
    public Long ion_id;
    public String ion_type;

    /*PARA REMOVER ???

    public int charge;
    public boolean positive;
    public boolean atomer;

     */

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "ion")
    public List<ScanToIon> scToIonIon;

    public static Find<Long,Ion> find = new Find<Long,Ion>(){};


    public static Ion findbyId(Long id) {
        return find.where().eq("ion_id",id).select("ion_id").findUnique();
    }


}

