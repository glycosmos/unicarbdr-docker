package models.MS;

import com.avaje.ebean.Model;
import javax.persistence.*;


@Entity
@Table(name="ms.maldi")
public class MALDI extends Model{
    @Id
    public Long maldi_id;

    public String plate_composition;
    public String matrix_composition;
    public String deposition_technique;
    public String voltages;
    public String prompt_fragmentation;
    public String psd_lid_summary;
    public String delayed_extraction;
    public String laser_type;
    public String laser_details;


    @OneToOne
    @JoinColumn(name="ms_id")
    public MS ms;


}
