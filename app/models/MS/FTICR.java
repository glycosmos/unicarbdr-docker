package models.MS;

import com.avaje.ebean.Model;

import javax.persistence.*;

@Entity
@Table(name="ms.ft_icr")
public class FTICR extends Model {
    @Id
    public Long ft_icr_id;

    public String peak_selection;
    public String pulse;
    public String width;
    public String voltage;
    public String decay_time;
    public String ir;
    public String other_parameters;

    @OneToOne
    @JoinColumn(name = "ms_id")
    public MS ms;
}