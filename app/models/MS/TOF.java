package models.MS;

import com.avaje.ebean.Model;
import models.MS.MS;

import javax.persistence.*;

@Entity
@Table(name="ms.tof")
public class TOF extends Model{

    @Id
    public Long tof_id;


    public String reflectron_status;

    @OneToOne
    @JoinColumn(name="ms_id")
    public MS ms;


}

