package models.MS;

import com.avaje.ebean.Expr;
import com.avaje.ebean.Junction;
import com.avaje.ebean.Model;
import org.eurocarbdb.application.glycoworkbench.Peak;
import src.PairWrapper;

import javax.persistence.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Entity
@Table(name="ms.peak_list")
public class PeakList extends Model{

    @Id
    public Long peak_list_id;

    public double base_peak_mz;
    public double base_peak_intensity;
    public double retention_time;

    public String acquisition_number;
    public String parameters_generation;
    public String raw_data_scoring;
    public String smoothing;
    public String threshold_algorithm;
    public String signal_to_noise;
    public String peak_height;


    @OneToOne
    @JoinColumn(name="scan_id")
    public Scan scan;


    @OneToMany(cascade = CascadeType.ALL, mappedBy = "peakList")
    public List<PeakLabeled> peakLabList;


    public static Find<Long,PeakList> find = new Find<Long,PeakList>(){};

    public static PeakList findById(int input) {
        return PeakList.find.where().eq("peak_list_id", input).findList().get(0);
    }

}
