package models.MS;

import com.avaje.ebean.Model;
import javax.persistence.*;

@Entity
@Table(name="ms.esi")
public class ESI extends Model{

    @Id
    public Long esi_id;

    public String supply_type;
    public String interface_name;
    public String interface_details;
    public String sprayer_name;
    public String sprayer_details;
    public String voltages;
    public String prompt_fragmentation;
    public String in_source_dissociation;
    public String other_parameters;


    @OneToOne
    @JoinColumn(name="ms_id")
    public MS ms;

}
