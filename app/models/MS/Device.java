package models.MS;


import com.avaje.ebean.Model;
import models.core.Manufacturer;

import javax.persistence.*;
import java.util.LinkedHashMap;
import java.util.Map;

@Entity
@Table(name="ms.device")
public class Device extends Model {

    @Id
    public Long device_id;


    @ManyToOne
    @JoinColumn(name="manufacturer_id")
    public Manufacturer manufacturer;

    public String model;
    public String ionisation_type;


/*    @OneToOne(cascade = CascadeType.ALL, mappedBy = "device")
    public Lc lcDev;
*/



    public static Find<Long,Device> find = new Find<Long,Device>(){};

    public static Map<String,String> options() {
        LinkedHashMap<String,String> options = new LinkedHashMap<String,String>();
        for(Device d: Device.find.orderBy("model").findList()) {
            options.put(d.model, d.model);
        }
        return options;
    }

    public static Device findById(Long id) {
        return find.where().eq("device_id",id).select("device_id").findUnique();
    }


}
