package models.MS;

import com.avaje.ebean.Model;
import javax.persistence.*;


@Entity
@Table(name="ms.peak_annotated")
public class PeakAnnotated extends Model {

    @Id
    public Long peak_annotated_id;


    @ManyToOne
    @JoinColumn(name = "peak_labeled_id")
    public PeakLabeled peakLabeled;

    public String sequence_gws;
    public double calculated_mass;
    public String fragmentation_type;
}