package models.MS;

import com.avaje.ebean.Model;
import javax.persistence.*;
import java.util.LinkedHashMap;
import java.util.List;

@Entity
@Table(name="ms.database")
public class Database extends Model{
    @Id
    public Long database_id;
    public String name_database;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "database")
    public List<ScanToDatabase> scanToDat;

    public static Find<Long, Database> find = new Find<Long, Database>(){};

    public static LinkedHashMap<String, String> options() {
        LinkedHashMap<String, String> options = new LinkedHashMap<>();
        for(Database c: Database.find.orderBy("name_database").findList()) {
            options.put(c.name_database, c.name_database);
        }
        return options;
    }

}
