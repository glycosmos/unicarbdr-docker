package models.MS;

import com.avaje.ebean.Model;

import javax.persistence.*;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


@Entity
@Table(name="ms.persubstitution")
public class Persubstitution extends Model {

    @Id
    private Long persubstitution_id;
    public String abbreviation;
    public String name;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "persubstitution")
    public List<Scan> ScanPers;


    public static Find<Long,Persubstitution> find = new Find<Long,Persubstitution>(){};

    public static Map<String,String> options() {
        LinkedHashMap<String,String> options = new LinkedHashMap<>();
        for(Persubstitution p: Persubstitution.find.orderBy("abbreviation").findList()) {
            options.put(p.persubstitution_id.toString(), p.name);
        }
        return options;
    }

    public static Persubstitution findbyId(Long id) {
        return find.where().eq("persubstitution_id",id).findUnique();
    }


}