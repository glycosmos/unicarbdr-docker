package models.MS;

import com.avaje.ebean.Model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="ms.software")
public class Software extends Model{
    @Id
    public Long software_id;

    public String name_software;
    public String manufacturer;
    public String availability;

    public boolean acquisition;
    public boolean analysis;
    public boolean processing;
    public boolean quantitation;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "software")
    public List<MSToSoftware> msToSoftSoft;

    public static Find<Long, Software> find = new Find<Long, Software>(){};

    public static List<String> options() {
        List<String> options = new ArrayList<>();
        for(Software c: Software.find.orderBy("name_software").findList()) {
            options.add(c.name_software);
        }
        return options;
    }

}
