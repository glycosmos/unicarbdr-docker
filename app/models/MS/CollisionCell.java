package models.MS;


import com.avaje.ebean.Model;

import javax.persistence.*;

@Entity
@Table(name="ms.collision_cell")
public class CollisionCell extends Model {


    @Id
    public Long collision_cell_id;

   // public String gas;
   // public String pressure;
   // public String instrument_parameters;

    @OneToOne
    @JoinColumn(name="ms_id")
    public MS ms;


    @OneToOne(cascade = CascadeType.ALL, mappedBy = "collisionCell")
    public CID cidCollisionCell;

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "collisionCell")
    public ETD etdCollisionCell;

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "collisionCell")
    public ECD ecdCollisionCell;


}


