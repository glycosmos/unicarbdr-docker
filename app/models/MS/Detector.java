package models.MS;

import com.avaje.ebean.Model;

import javax.persistence.*;

@Entity
@Table(name="ms.detector")
public class Detector extends Model {
    @Id
    public Long detector_id;

    public String detector_type;

    @OneToOne
    @JoinColumn(name = "ms_id")
    public MS ms;
}