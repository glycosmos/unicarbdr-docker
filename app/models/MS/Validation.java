package models.MS;

import com.avaje.ebean.Model;
import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="ms.validation")
public class Validation extends Model{
    @Id
    public Long validation_id;
    public String validation_level;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "validation")
    public List<ScanToValidation> scanToVal;

    public static Find<Long,Validation> find = new Find<Long,Validation>(){};

}
