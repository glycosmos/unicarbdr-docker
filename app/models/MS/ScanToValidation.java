package models.MS;

import com.avaje.ebean.Model;
import javax.persistence.*;

@Entity
@Table(name="ms.scan_to_validation")
public class ScanToValidation extends Model {

    @Id
    public Long scan_to_validation_id;

    public String status;
    public String validation_result;

    @ManyToOne
    @JoinColumn(name="validation_id")
    public Validation validation;

    @OneToOne
    @JoinColumn(name="scan_id")
    public Scan scan;

}
