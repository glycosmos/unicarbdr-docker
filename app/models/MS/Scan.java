package models.MS;

import com.avaje.ebean.Model;
import com.avaje.ebean.*;
import models.HPLC.LC;
import models.core.GlycanSequence;
import models.core.JournalReference;
import models.samplePrep.SampleToJournalReference;
import src.PairWrapper;

import javax.persistence.*;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.*;

@Entity
@Table(name="ms.scan")
public class Scan extends Model {

    @Id
    public Long scan_id;

    public int ms_exponent;


    @ManyToOne
    @JoinColumn(name="persubstitution_id")
    public Persubstitution persubstitution;

    @ManyToOne
    @JoinColumn(name="reducing_end_id")
    public ReducingEnd reducingEnd;


    @OneToOne
    @JoinColumn(name="glycan_sequence_id")
    public GlycanSequence glycanSequence;


    public boolean show;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "scan")
    public List<ScanToIon> scanToion;


    public String note;

    public String stability;
    public String orthogonal_approaches;
    public String quantity;


    @ManyToOne
    @JoinColumn(name="journal_reference_id")
    public JournalReference journalReference;


    @ManyToOne
    @JoinColumn(name="sample_to_journal_reference_id")
    public SampleToJournalReference sampleToJournalReference;


    @OneToOne(cascade = CascadeType.ALL, mappedBy = "scan")
    public PeakList peakListScan;


    @OneToOne(cascade = CascadeType.ALL, mappedBy = "scan")
    public ScanToValidation scanToValScan;

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "scan")
    public ScanToDatabase scanToDatScan;


    public static Find<Long,Scan> find = new Find<Long,Scan>(){};

    public static int getSpectraCount() {
        return Scan.find.findRowCount();
    }

    public static int[] getCounts() {


        int[] counts = new int[2];

        List<Scan> scans = Scan.find.fetch("glycan_sequence","glycan_sequence_id").where().eq("show",true).findList();

        // Spectrum count.
        counts[0] = scans.size();

        List<Long> uniqueSeq = new ArrayList<Long>();
        for(Scan s: scans){
            if(s.glycanSequence != null
                    && s.glycanSequence.glycan_sequence_id != null
                    && !uniqueSeq.contains(s.glycanSequence.glycan_sequence_id)) {
                uniqueSeq.add(s.glycanSequence.glycan_sequence_id);
            }
        }

        // Structure count.
        counts[1] = uniqueSeq.size();

        return counts;
    }






    public static Map<PairWrapper, org.expasy.mzjava.core.ms.peaklist.PeakList> generatePeakLists(Double parentMass, Double error) throws IOException {

        Junction<Scan> junction;
        junction = find.where().conjunction();
        junction.add(Expr.eq("show",true));


        if(parentMass>0.0) {
            junction.add(Expr.between("base_peak_mz", (parentMass - error), (parentMass + error)));
        }


        List<Scan> data = find.where().conjunction()
                .add(junction)
                .findList();

        List<PeakLabeled> labeled = new ArrayList<PeakLabeled>();
        Map<PairWrapper, org.expasy.mzjava.core.ms.peaklist.PeakList> peakListMap = new HashMap<PairWrapper, org.expasy.mzjava.core.ms.peaklist.PeakList>();


        for(Scan sc: data){
//            labeled= sc.pLabeledScan;

            labeled= sc.peakListScan.peakLabList;

            org.expasy.mzjava.core.ms.peaklist.DoubleFloatPeakList peaks = new org.expasy.mzjava.core.ms.peaklist.DoubleFloatPeakList<>();

            for (PeakLabeled pl : labeled) {
                peaks.add(pl.mz_value, pl.intensity_value);
            }
            peakListMap.put((new PairWrapper((int)(long) sc.scan_id, sc.glycanSequence.glycan_sequence_id)),peaks);
        }
        return peakListMap;
    }




}

