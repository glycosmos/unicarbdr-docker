package models.MS;

import com.avaje.ebean.Model;

import javax.persistence.*;

@Entity
@Table(name="ms.ecd")
public class ECD extends Model{
    @Id
    public Long ecd_id;

    public String emitter_type;
    public String voltage;
    public String current;

    @OneToOne
    @JoinColumn(name="collision_cell_id")
    public CollisionCell collisionCell;


}
