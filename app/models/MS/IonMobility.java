package models.MS;

import com.avaje.ebean.Model;

import javax.persistence.*;

@Entity
@Table(name="ms.ion_mobility")
public class IonMobility extends Model{

    @Id
    public Long ion_mobility_id;

    public String gas;
    public String pressure;
    public String instrument_parameters;

    @OneToOne
    @JoinColumn(name="ms_id")
    public MS ms;

}

