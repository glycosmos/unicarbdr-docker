package models.MS;

import com.avaje.ebean.Model;
import models.core.JournalReference;

import javax.persistence.*;
import java.util.Date;


@Entity
@Table(name="ms.ms")

public class MS extends Model{
    @Id
    public Long ms_id;

    public String customizations;
    public String ion_mode;
    public String hardware_options;
    public Date date_completed;


    @ManyToOne
    @JoinColumn(name="device_id")
    public Device device;


    @OneToOne(cascade = CascadeType.ALL, mappedBy = "ms")
    public IonMobility ionMobMS;

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "ms")
    public TOF tofMs;

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "ms")
    public CollisionCell collCellMS;

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "ms")
    public IonTrap ionTrapMs;

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "ms")
    public FTICR fticrMs;

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "ms")
    public Detector detectorMs;

    @OneToOne
    @JoinColumn(name="journal_Reference_id")
    public JournalReference journalReference;


}
