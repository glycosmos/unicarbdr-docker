package models.MS;


import com.avaje.ebean.Model;

import javax.persistence.*;

@Entity
@Table(name="ms.ion_trap")

public class IonTrap extends Model{
    @Id
    public Long ion_trap_id;
    public String ms_achieved;

    @OneToOne
    @JoinColumn(name="ms_id")
    public MS ms;

}

