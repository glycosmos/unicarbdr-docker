package models.MS;

import com.avaje.ebean.Model;

import javax.persistence.*;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


@Entity
@Table(name="ms.reducing_end")
public class ReducingEnd extends Model {

    @Id
    private Long reducing_end_id;
    public String abbreviation;
    public String name;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "reducingEnd")
    public List<Scan> ScanRedEnd;


    public static Find<Long,ReducingEnd> find = new Find<Long,ReducingEnd>(){};

    public static Map<String,String> options() {
        LinkedHashMap<String,String> options = new LinkedHashMap<>();
        for(ReducingEnd r: ReducingEnd.find.orderBy("reducing_end_id").findList()) {
            options.put(r.reducing_end_id.toString(), r.name);
        }
        return options;
    }

    public static ReducingEnd findbyId(Long id) {
        return find.where().eq("reducing_end_id",id).findUnique();
    }



}

