package models.MS;

import com.avaje.ebean.Model;
import models.core.File;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="ms.ms_to_software")
public class MSToSoftware extends Model {
    @Id
    public Long ms_to_software_id;

    public String version_software;
    public String customizations;
    public String software_settings;
    public String upgrades;
    public String type_processing;
    public String task;
    public String switching_criteria;
    public String isolation_width;
    public String location_file;

    @ManyToOne
    @JoinColumn(name="software_id")
    public Software software;

    @ManyToOne
    @JoinColumn(name="ms_id")
    public MS ms;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "msToSoftware")
    public List<File> fileMsToSoft;
}