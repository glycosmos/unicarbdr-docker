
package models;


import com.avaje.ebean.Model;
import models.MS.PeakLabeled;

import javax.persistence.*;

@Entity
@Table(name="im.cross_section")
public class CrossSection extends Model {

    @Id
    public Long cross_section_id;

    @OneToOne
    @JoinColumn(name = "peak_labeled_id")
    public PeakLabeled peakLabeled;


    public double ccs_value;


    public static Find<Long,CrossSection> find = new Find<Long,CrossSection>(){};


}
