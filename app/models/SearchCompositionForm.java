package models;

import play.data.validation.ValidationError;

import com.avaje.ebean.Model;

import java.util.ArrayList;
import java.util.List;


public class SearchCompositionForm {
    private Double mass;
    private char massType;

    private Double error;
    private char errorType;

    private int charge;
    private char chargeMode;

    private int persId;
    private int redEndId;


    private char selDHex;
    private Integer dhex;
    private Integer dhex2;


    private char selHex;
    private Integer hex;
    private Integer hex2;


    private char selHexNac;
    private Integer hexnac;
    private Integer hexnac2;



    private char selNeuGc;
    private Integer neuGc;
    private Integer neuGc2;



    private char selNeuAc;
    private Integer neuAc;
    private Integer neuAc2;



    public List<ValidationError> validate() {

        List<ValidationError> errors = new ArrayList<>();
        if(mass !=null && mass != (double)mass){
            errors.add(new ValidationError("", "Invalid mass"));
            return  errors;
        }
        if(error !=null && error != (double)error){
            errors.add(new ValidationError("", "Invalid error"));
            return  errors;
        }

        if(hex !=null && hex != (int)hex){
            errors.add(new ValidationError("", "Invalid error"));
            return  errors;
        }

        if(hex2 !=null && hex2 != (int)hex2){
            errors.add(new ValidationError("", "Invalid error"));
            return  errors;
        }

/*        if(hex2 > hex){
            System.out.println("mmmh -> " + hex + " - " + hex2);
            errors.add(new ValidationError("", "Invalid error"));
            return  errors;
        }
*/
        if(dhex !=null && dhex != (int)dhex){
            errors.add(new ValidationError("", "Invalid error"));
            return  errors;
        }
        if(dhex2 !=null && dhex2 != (int)dhex2){
            errors.add(new ValidationError("", "Invalid error"));
            return  errors;
        }
        if(hexnac !=null && hexnac != (int)hexnac){
            errors.add(new ValidationError("", "Invalid error"));
            return  errors;
        }
        if(hexnac2 !=null && hexnac2 != (int)hexnac2){
            errors.add(new ValidationError("", "Invalid error"));
            return  errors;
        }
        if(neuAc !=null && neuAc != (int)neuAc){
            errors.add(new ValidationError("", "Invalid error"));
            return  errors;
        }
        if(neuAc2 !=null && neuAc2 != (int)neuAc2){
            errors.add(new ValidationError("", "Invalid error"));
            return  errors;
        }
        if(neuGc !=null && neuGc != (int)neuGc){
            errors.add(new ValidationError("", "Invalid error"));
            return  errors;
        }
        if(neuGc2 !=null && neuGc2 != (int)neuGc2){
            errors.add(new ValidationError("", "Invalid error"));
            return  errors;
        }


        return null;
    }


    public Double getMass() {
        return this.mass;
    }
    public char getMassType() { return this.massType; }

    public Double getError() {
        return this.error;
    }
    public char getErrorType() { return this.errorType; }

    public int getCharge() { return this.charge; }
    public char getChargeMode() { return this.chargeMode; }

    public int getPersId() { return this.persId; }
    public int getRedEndId() { return this.redEndId; }



    public Integer getHex() {
        return this.hex;
    }
    public Integer getHexNac() {
        return this.hexnac;
    }
    public Integer getDhex() {
        return this.dhex;
    }
    public Integer getNeuAc() {
        return this.neuAc;
    }
    public Integer getNeuGc() {
        return this.neuGc;
    }


    public Integer getHex2() {
        return this.hex2;
    }
    public Integer getHexNac2() {
        return this.hexnac2;
    }
    public Integer getDhex2() {
        return this.dhex2;
    }
    public Integer getNeuAc2() {
        return this.neuAc2;
    }
    public Integer getNeuGc2() {
        return this.neuGc2;
    }



    public char getSelDHex() { return this.selDHex; }
    public char getSelHex() {return this.selHex; }
    public char getSelHexNac() { return this.selHexNac; }
    public char getSelNeuGc() { return this.selNeuGc; }
    public char getSelNeuAc() { return this.selNeuAc; }




    public void setMass(Double mass) {this.mass = mass;}
    public void setMassType(char massType){ this.massType=massType; }
    public void setError(Double error) { this.error = error; }
    public void setErrorType(char errorType){ this.errorType=errorType; }
    public void setCharge(int charge) { this.charge = charge; }
    public void setChargeMode(char chargeMode) { this.chargeMode = chargeMode; }
    public void setPersId(char persId) { this.persId=persId; }
    public void setRedEndId(char redEndId) { this.redEndId = redEndId; }


    public void setHex(Integer hex) {
        this.hex = hex;
    }
    public void setHexnac(Integer hexnac) {
        this.hexnac = hexnac;
    }
    public void setDhex(Integer dhex) {
        this.dhex = dhex;
    }
    public void setNeuAc(Integer neuAc) {
        this.neuAc = neuAc;
    }
    public void setNeuGc(Integer neuGc) {
        this.neuGc = neuGc;
    }


    public void setHex2(Integer hex2) {
        this.hex2 = hex2;
    }
    public void setHexnac2(Integer hexnac2) {
        this.hexnac2 = hexnac2;
    }
    public void setDhex2(Integer dhex2) {
        this.dhex2 = dhex2;
    }
    public void setNeuAc2(Integer neuAc2) {
        this.neuAc2 = neuAc2;
    }
    public void setNeuGc2(Integer neuGc2) {
        this.neuGc2 = neuGc2;
    }


    public void setSelDHex(char selDHex) { this.selDHex= selDHex; }
    public void setSelHex(char selHex) {this.selHex= selHex; }
    public void setSelHexNac(char selHexNac) { this.selHexNac= selHexNac; }
    public void setSelNeuGc(char selNeuGc) { this.selNeuGc= selNeuGc; }
    public void setSelNeuAc(char selNeuAc) { this.selNeuAc= selNeuAc; }







}
