package models;

import com.avaje.ebean.Model;
import play.data.validation.Constraints;
import play.data.validation.ValidationError;

import java.util.ArrayList;
import java.util.List;


public class SearchMSMSForm extends Model {

    private Double mass;

    @Constraints.Required(message = "Peak list is a mandatory field.")
    @Constraints.Pattern(value = "[0-9\\.]+\\s[0-9\\.]+", message = "Wrong format.")
    private String peaks;

    private Double error;
    private char errorType;
    private Double frError;
    private char frErrorType;
    private char method;


    public List<ValidationError> validate() {

        List<ValidationError> errors = new ArrayList<>();
        if(mass !=null && mass != (double)mass){
            errors.add(new ValidationError("", "Invalid mass"));
            return  errors;
        }
        if(error !=null && error != (double)error){
            errors.add(new ValidationError("", "Invalid error"));
            return  errors;
        }
        if(frError !=null && frError != (double)frError){
            errors.add(new ValidationError("", "Invalid error"));
            return  errors;
        }

        return null;
    }


    public Double getMass() {
        return this.mass;
    }
    public Double getError() {
        return this.error;
    }
    public Double getFrError() {return this.frError;}
    public char getErrorType() {
        return this.errorType;
    }
    public char getFrErrorType() {return this.frErrorType;}
    public String getPeaks() { return this.peaks; }


    public void setMass(Double mass) {
        this.mass = mass;
    }
    public void setError(Double error) { this.error = error; }
    public void setFrError(Double frError) { this.frError = frError; }
    public void setErrorType(char errorType) { this.errorType = errorType; }
    public void setFrErrorType(char frErrorType) { this.frErrorType = frErrorType; }
    public void setPeaks(String peaks) { this.peaks = peaks; }
}