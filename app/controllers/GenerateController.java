package controllers;


import com.feth.play.module.pa.PlayAuthenticate;
import jdk.nashorn.internal.runtime.regexp.joni.constants.OPCode;
import models.HPLC.Column;
import models.HPLC.Instrument;
import models.MS.Database;
import models.MS.Device;
import models.MS.Software;
import models.core.Manufacturer;
import models.samplePrep.*;
import org.apache.commons.lang3.text.translate.NumericEntityUnescaper;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import play.Configuration;
import play.api.Play;
import play.data.Form;
import play.data.FormFactory;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import service.*;
import src.FileGenerator;
import src.*;


import javax.inject.Inject;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.*;
import java.util.*;



import service.UserProvider;

public class GenerateController extends Controller{

    private FormFactory formFactory;
    private Configuration configuration;


    private final PlayAuthenticate auth;
    private final UserProvider userProvider;




    @Inject
    public GenerateController(Configuration configuration, FormFactory formFactory, final PlayAuthenticate auth, final UserProvider userProvider) {
        this.configuration = configuration;
        this.formFactory = formFactory;

        this.auth = auth;
        this.userProvider = userProvider;

    }

    // API for Tissue, Taxonomy and Cell Line
    public Result cellToJson(String term) {
        return ok(Json.toJson(CellLine.optionsAutoTwo(term)));
    }

    public Result speciesToJson(String term) {
        return ok(Json.toJson(Taxonomy.optionsAutoTwo(term)));
    }

    public Result originToJson(String term) {
        return ok(Json.toJson(Tissue.optionsAutoTwo(term)));
    }


    public Result generate() {
        Form<GenerateMirageForm> mirageForm = formFactory.form(GenerateMirageForm.class);

        return ok( views.html.generateFiles.generateMirage.render("main", mirageForm, this.userProvider) );
    }


    public Result generateMirage(String setup) {
        Form<GenerateMirageForm> mirageForm = formFactory.form(GenerateMirageForm.class);

        /*switch (setup){
            case "ESI-MS": return ok( views.html.generateFiles.generateMirage.render("ESI-MS", mirageForm, this.userProvider) );

            case "ESI-MS/MS": return ok( views.html.generateFiles.generateMirage.render("ESI-MS/MS", mirageForm, this.userProvider) );

            case "MALDI-MS": return ok( views.html.generateFiles.generateMirage.render("MALDI-MS", mirageForm, this.userProvider) );

            case "MALDI-MS/MS":  return ok( views.html.generateFiles.generateMirage.render("MALDI-MS/MS", mirageForm, this.userProvider) );

            default:*/  return ok( views.html.generateFiles.generateMirage.render("main", mirageForm, this.userProvider) );
        //}
    }



    // (Not used!!) Generates a Mirage.xlsx file for the user
    public Result generateFile() {
        Form<GenerateMirageForm> form = formFactory.form(GenerateMirageForm.class).bindFromRequest();

        if(form.hasErrors()){
            return ok(views.html.generateFiles.generateMirage.render("ESI-MS", form, this.userProvider));
        }

        GenerateMirageForm mirageForm = formFactory.form(GenerateMirageForm.class).bindFromRequest().get();

        String path = configuration.underlying().getString("fileGeneratedPath") + "MIRAGE-" + new Date().getTime() + ".xlsx";
        try {
            /*XSSFWorkbook wb = new FileGenerator().createMirageWorkbook(mirageForm);
            FileOutputStream fileOut = new FileOutputStream(path);
            wb.write(fileOut);
            fileOut.close();*/
            return ok(new File(path));
        } catch (Exception e) {
            e.printStackTrace();
            return internalServerError("cant generate file");
        }finally {
            //Thread thread = new Thread(new deleteMirageFile(path));
            //thread.start();
        }
    }


    // Set up form for Sampleprep, LC and MS
    public Result generateForm(){
        Form<GenerateMirageForm> form = formFactory.form(GenerateMirageForm.class).bindFromRequest();

        if(form.hasErrors()){
            System.out.println("errors size: "+form.errors());
            return ok(views.html.generateFiles.generateMirage.render("ESI-MS", form, this.userProvider));
        }

        // Set up forms for view
        GenerateMirageForm f = form.get();
        FormMS ms = new FormMS();

        if (f.isGenMSMirage()){

            ms.setM_CheckCID(f.isGenCID());
            System.out.println(ms.isM_CheckCID());
            ms.setM_CheckECD(f.isGenECD());
            ms.setM_CheckETD(f.isGenETD());
            ms.setM_CheckTOF(f.isGenTOF());
            ms.setM_CheckIonTrap(f.isGenIonTrap());
            ms.setM_CheckIonMobility(f.isGenIonMobility());
            ms.setM_CheckFTICR(f.isGenFTICR());
            ms.setM_CheckDetectors(f.isGenDetectors());
            ms.setM_CheckESI(false);
            ms.setM_CheckMALDI(false);
        }

        // Forms for tabs
        Form<FormSample> formSample = formFactory.form(FormSample.class);
        Form<FormLC> formLC = formFactory.form(FormLC.class);
        Form<FormMS> formMS = formFactory.form(FormMS.class).fill(ms);
        Form<FormStructure> formStructure = formFactory.form(FormStructure.class);

        // Forms for tables
        FormTables filler = new FormTables(new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>());
        Form<FormTables> formTables = formFactory.form(FormTables.class).fill(filler);

        // Map for forms
        Map<String, Form<?>> forms = new HashMap<>();
            if (f.isGenSamplePrep()) {forms.put("Sample", formSample); }
            if (f.isGenLCMS()){forms.put("LC", formLC);}
            if (f.isGenMSMirage()){forms.put("MS", formMS); }
            //forms.put("Structure", formStructure);
            forms.put("Tables", formTables);

        // Map for options
        Map<String, Map<String, String>> options = new LinkedHashMap<>();
        options.put("CellType", CellLine.options());
        options.put("StartingMaterial", Material.optionsStarting());
        options.put("SampleMaterial", Material.optionsSample());

        options.put("HplcManufacturer", Manufacturer.optionsHPLC());
        options.put("HplcBrand", Instrument.optionsBrand());
        options.put("ColumnManufacturer", Manufacturer.optionsColumn());
        options.put("ColumnModel", Column.options());

        options.put("InstrumentManufacturer", Manufacturer.optionsSpectrometer());
        options.put("InstrumentModel", Device.options());

        options.put("Database", Database.options());

        return ok( views.html.generateFiles.generateUnicarbForm.render(forms, options, createTables(), this.userProvider));
    }



    // Creates the objects for the table-elements
    private List<TableFragment> createTables(){

        TableFragment isoOne = new TableFragment();
        isoOne.setName("isoOne");
        isoOne.setFields(Arrays.asList(
                new TableFragmentField(1,"isoOneEnzymes","Enzymes used", Treatment.optionsIsoEnzymaticL(), "Describe any enzymes used to for the purpose of oligosaccharide removal (e.g. PNGase F) or for modification of the starting material (e.g. trypsin protease)."),
                new TableFragmentField(0,"isoOneVendor","Vendor or enzyme production", "Specify where it was obtained (vendor) or for enzymes produced in-house, describe expression and purification procedure."),
                new TableFragmentField(0,"isoOneConditions","Reaction conditions", "State if sample material was treated in-solution or immobilized (SDS-PAGE, PVDF etc.) as well as temperature, duration, volume, enzyme concentration.")
        ));

        TableFragment isoTwo = new TableFragment();
        isoTwo.setName("isoTwo");
        isoTwo.setFields(Arrays.asList(
                new TableFragmentField(1,"isoTwoChemicals","Chemical methods",Treatment.optionsIsoChemicalL(), "E.g. (hydrazinolysis, β-elimination etc.)"),
                new TableFragmentField(0,"isoTwoConditions","Reaction conditions")
        ));

        TableFragment modOne = new TableFragment();
        modOne.setName("modOne");
        modOne.setFields(Arrays.asList(
                new TableFragmentField(1,"modOneEnzymes","Enzymes used",Treatment.optionsModEnzymaticL()),
                new TableFragmentField(0,"modOneConditions","Reaction conditions"),
                new TableFragmentField(0,"modOneCOrigin","Origin of novel enzyme")
        ));

        TableFragment modTwo = new TableFragment();
        modTwo.setName("modTwo");
        modTwo.setFields(Arrays.asList(
                new TableFragmentField(1,"modTwoChemicals","Chemical methods",Treatment.optionsModChemicalL()),
                new TableFragmentField(0,"modTwoModification","Type of modification", "(e.g. hydrolysis, sample tagging (including fluorescent labels), isotopic labelling, permethylation/peracetylation, etc.)."),
                new TableFragmentField(0,"modTwoConditions","Reaction conditions")
        ));

        TableFragment softwareOne = new TableFragment();
        softwareOne.setName("softwareOne");
        softwareOne.setFields(Arrays.asList(
                new TableFragmentField(1,"softwareOneName","Software name", Software.options(), "The instrument management and data analysis package name", 150),
                new TableFragmentField(0,"softwareOneVersion","Version", 100),
                new TableFragmentField(0,"softwareOneUpgrades","Upgrades", "Upgrades not reflected in version number", 150),
                new TableFragmentField(0,"softwareOneSwitchingCriteria","Switching criteria","The list of conditions that cause the switch from survey or zoom mode (MS^1) to or tandem mode (MS^n where n > 1); e.g., ‘parent ion’ mass lists, neutral loss criteria and so on.", 110),
                new TableFragmentField(0,"softwareOneIsolation","Isolation width","For tandem instruments (i.e., multi-stage instruments such as triple quads and TOF-TOFs, plus ion traps and equivalents) the total width (i.e., not half for plus-or-minus) of the gate applied around a selected precursor ion m/z, provided for all levels or by MS level.", 100),
                new TableFragmentField(0,"softwareOneFile","Location of ‘parameters’ file", "The location and name under which the mass spectrometer’s parameter settings file for the run is stored, if available. Ideally this should be a URI including filename, or most preferably an LSID, where feasible. Location of file should be mentioned.", 170)
        ));

        TableFragment softwareTwo = new TableFragment();
        softwareTwo.setName("softwareTwo");
        softwareTwo.setFields(Arrays.asList(
                new TableFragmentField(1,"softwareTwoName","Software name",Software.options()),
                new TableFragmentField(0,"softwareTwoVersion","Version")
        ));

        TableFragment softwareThree = new TableFragment();
        softwareThree.setName("softwareThree");
        softwareThree.setFields(Arrays.asList(
                new TableFragmentField(1,"softwareThreeSoftware","Software",Arrays.asList(), 160),
                new TableFragmentField(0,"softwareThreeName","Name", 160),
                new TableFragmentField(0,"softwareThreeFormat","Format", 160),
                new TableFragmentField(0,"softwareThreeArea","Link to the target area", 160),
                new TableFragmentField(0,"softwareThreeURL","URL", 160)
        ));

        TableFragment softwareFour = new TableFragment();
        softwareFour.setName("softwareFour");
        softwareFour.setFields(Arrays.asList(
                new TableFragmentField(1,"softwareFourSoftware","Software Name",Software.options()),
                new TableFragmentField(0,"softwareFourVersion","Version"),
                new TableFragmentField(0,"softwareFourCustomizations","Customizations", "Customizations made to the software"),
                new TableFragmentField(0,"softwareFourSettings","Software settings")
        ));

        TableFragment softwareFive = new TableFragment();
        softwareFive.setName("softwareFive");
        softwareFive.setFields(Arrays.asList(
                new TableFragmentField(1,"softwareFiveSoftware","Software", Arrays.asList()),
                new TableFragmentField(0,"softwareFiveName","Name"),
                new TableFragmentField(0,"softwareFiveFormat","Format"),
                new TableFragmentField(0,"softwareFiveURL","URL")
        ));

        TableFragment softwareSix = new TableFragment();
        softwareSix.setName("softwareSix");
        softwareSix.setFields(Arrays.asList(
                new TableFragmentField(1,"softwareSixName","Software name", Software.options(), 160),
                new TableFragmentField(0,"softwareSixVersion","Version", 160),
                new TableFragmentField(0,"softwareSixType","Software type", 160),
                new TableFragmentField(0,"softwareSixCustomizations","Customizations","Customizations made to the software", 160),
                new TableFragmentField(0,"softwareSixSettings","Software settings", 160)
        ));

        TableFragment softwareSeven = new TableFragment();
        softwareSeven.setName("softwareSeven");
        softwareSeven.setFields(Arrays.asList(
                new TableFragmentField(1,"softwareSevenSoftware","Software", Arrays.asList()),
                new TableFragmentField(0,"softwareSevenName","Name"),
                new TableFragmentField(0,"softwareSevenFormat","Format"),
                new TableFragmentField(0,"softwareSevenURL","URL")
        ));

        TableFragment softwareEight = new TableFragment();
        softwareEight.setName("softwareEight");
        softwareEight.setFields(Arrays.asList(
                new TableFragmentField(1,"softwareEightSoftware","Software", Software.options()),
                new TableFragmentField(0,"softwareEightName","Name"),
                new TableFragmentField(0,"softwareEightFormat","Format"),
                new TableFragmentField(0,"softwareEightURI","URI")
        ));

        return Arrays.asList(isoOne, isoTwo, modOne, modTwo, softwareOne, softwareTwo, softwareThree, softwareFour, softwareFive, softwareSix, softwareSeven, softwareEight);
    }

    // Generate UniCarb file for user
    public Result uploadForm() {

        FormSample formSampleRes = formFactory.form(FormSample.class).bindFromRequest().get();
        FormLC formLCRes = formFactory.form(FormLC.class).bindFromRequest().get();
        FormMS formMSRes = formFactory.form(FormMS.class).bindFromRequest().get();
        FormTables formTablesRes = formFactory.form(FormTables.class).bindFromRequest().get();

        String fileName =  "MIRAGE-" + new Date().getTime() + ".xlsx";
        String path =  configuration.underlying().getString("fileGeneratedPath") + fileName;
        try {
            XSSFWorkbook wb = new FileGenerator().createUniCarbFile(formSampleRes,formLCRes,formMSRes, formTablesRes);
            FileOutputStream fileOut = new FileOutputStream(path);
            wb.write(fileOut);
            fileOut.close();
            response().setContentType("application/x-download");
            response().setHeader("Content-disposition","attachment; filename=" + fileName);
            return ok(new File(path));

        } catch (Exception e) {
            e.printStackTrace();
            return internalServerError("cant generate file");
        }finally {
            //Thread thread = new Thread(new deleteMirageFile(path));
            //thread.start();
        }
    }




    // Runnable for thread removal of Mirage file
    class deleteMirageFile implements Runnable{
        public String path;

        public deleteMirageFile(String path) {
            this.path = path;
        }

        @Override
        public void run() {
            try {
                Thread.sleep(120000);
                Files.delete(Paths.get(path));

            } catch (NoSuchFileException x) {
                System.err.format("%s: no such" + " file or directory%n", path);
            } catch (DirectoryNotEmptyException x) {
                System.err.format("%s not empty%n", path);
            } catch (IOException x) {
                // File permission problems are caught here.
                System.err.println(x);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
