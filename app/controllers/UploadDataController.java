package controllers;


import com.feth.play.module.pa.PlayAuthenticate;
import models.HPLC.Column;
import models.HPLC.Instrument;
import models.MS.*;
import models.core.GlycanComposition;
import models.core.Manufacturer;
import models.samplePrep.*;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.eurocarbdb.application.glycanbuilder.GlycanRendererAWT;
import org.eurocarbdb.application.glycoworkbench.*;
import org.eurocarbdb.application.glycoworkbench.PeakList;
import org.eurocarbdb.application.glycoworkbench.Scan;
import play.Configuration;
import play.data.DynamicForm;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.Http;
import play.mvc.Result;
import service.*;
import src.DataUploader;
import src.FileExtractor;
import src.TableFragment;
import src.TableFragmentField;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.util.*;

import service.UserProvider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UploadDataController extends play.mvc.Controller {
    private FormFactory formFactory;
    private Configuration configuration;

    private final PlayAuthenticate auth;
    private final UserProvider userProvider;

    private final Logger uploadLogger = LoggerFactory.getLogger(this.getClass());



    @Inject
    public UploadDataController(Configuration configuration, FormFactory formFactory, final PlayAuthenticate auth, final UserProvider userProvider) {
        this.configuration = configuration;
        this.formFactory = formFactory;

        this.auth = auth;
        this.userProvider = userProvider;

    }


    // Uploads mirage.xlsx and .gwp files, setup of forms
    public Result upload(){
        uploadLogger.error("upload started");
      
        Http.MultipartFormData md=request().body().asMultipartFormData();
        List<Http.MultipartFormData.FilePart> file;
        file=md.getFiles();

        String filePathExcel = null;
        List<String> filePathsGwp = new ArrayList<>();

/*        for(Http.MultipartFormData.FilePart p: file){
            String filePath = saveFile(p);
            if(filePath.substring(filePath.length() - 3, filePath.length()).equals("lsx")){
                filePathExcel = saveFile(p);
            } else if(filePath.substring(filePath.length() - 3, filePath.length()).equals("gwp")){
                filePathsGwp.add(filePath);
            }
        }*/

        for(Http.MultipartFormData.FilePart p: file){
            String filePath = saveFile(p);

            if(filePath.substring(filePath.length() - 3, filePath.length()).equals("gwp"))
                filePathsGwp.add(filePath);
            else
                filePathExcel = filePath;
        }

        uploadLogger.error("checking Gwp");

        if (filePathsGwp.size() != 0) {
            // Forms for tabs
            Form<FormSample> formSample = formFactory.form(FormSample.class);
            Form<FormLC> formLC = formFactory.form(FormLC.class);
            Form<FormMS> formMS = formFactory.form(FormMS.class);
            Form<FormStructure> formStructure = formFactory.form(FormStructure.class);

            // Forms for tables
            FormTables filler = new FormTables(new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>());
            Form<FormTables> formTables = formFactory.form(FormTables.class).fill(filler);

            // MS form
            FormMS ms = new FormMS();

            // Uploading the journal ref and adding its id and the gwp file path to ms form
            FormPubMed formPubMed = formFactory.form(FormPubMed.class).bindFromRequest().get();
            ms.setJournalReferenceID(new DataUploader().persistPubMed(formPubMed,this.userProvider.getUser(session()).id.intValue()));
            ms.setGwpFiles(filePathsGwp);
            formMS = formFactory.form(FormMS.class).fill(ms);

            String res = "No Excel File :(";
            if(filePathExcel != null){

                try {
                    FileExtractor f = new FileExtractor(filePathExcel);

                    formSample = formFactory.form(FormSample.class).fill(f.getSampleForm());
                    formLC = formFactory.form(FormLC.class).fill(f.getLC());
                    ms = f.getMS();
                    ms.setGwpFiles(filePathsGwp);
                    ms.setJournalReferenceID(formMS.get().getJournalReferenceID());
                    formMS = formFactory.form(FormMS.class).fill(ms);
                    formStructure = formFactory.form(FormStructure.class).fill(f.getStructures());

                    formTables = formFactory.form(FormTables.class).fill(populatedFormTables(formSample.get(),formMS.get()));

                } catch (InvalidFormatException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            // Form for structures
            List<StructureForm> structureList = StructureListGenerator(filePathsGwp);
            StructureForms forms = new StructureForms(structureList);
            Form<StructureForms> structureFormList = formFactory.form(StructureForms.class).fill(forms);

            // Map for options
            Map<String, Map<String, String>> options = new LinkedHashMap<>();
            options.put("CellType", CellLine.options());
            options.put("StartingMaterial", Material.optionsStarting());
            options.put("SampleMaterial", Material.optionsSample());

            options.put("HplcManufacturer", Manufacturer.optionsHPLC());
            options.put("HplcBrand", Instrument.optionsBrand());
            options.put("ColumnManufacturer", Manufacturer.optionsColumn());
            options.put("ColumnModel", Column.options());

            options.put("InstrumentManufacturer", Manufacturer.optionsSpectrometer());
            options.put("InstrumentModel", Device.options());

            options.put("Database", Database.options());

            uploadLogger.error("returning OK");

            return ok( views.html.dataUpload.uploadresult.render(structureFormList, formMS, formSample, formLC, formStructure, structureList, options, createTables(), formTables, this.userProvider));
        } else {
            uploadLogger.error("returning badrequest");
            flash("error", "Missing file(s)");
            return badRequest("Missing file(s)");
        }
    }

    public Result uploadData() {
        DynamicForm requestData = formFactory.form().bindFromRequest();

        // Collect data from forms
        StructureForms sFormsRes = formFactory.form(StructureForms.class).bindFromRequest().get();
        FormSample formSampleRes = formFactory.form(FormSample.class).bindFromRequest().get();
        FormLC formLCRes = formFactory.form(FormLC.class).bindFromRequest().get();
        FormMS formMSRes = formFactory.form(FormMS.class).bindFromRequest().get();
        FormStructure formStructureRes = formFactory.form(FormStructure.class).bindFromRequest().get();
        FormTables formTablesRes = formFactory.form(FormTables.class).bindFromRequest().get();


        // Upload data to DB
        DataUploader dataUploader = new DataUploader();
        long id= dataUploader.persistSampleForm(formSampleRes, formMSRes.getJournalReferenceID());
        dataUploader.persistLCFrom(formLCRes, formMSRes.getJournalReferenceID());
        dataUploader.persistMSFrom(formMSRes);
        dataUploader.saveStructures(formMSRes, id);

//      ------------------------- Reset ------- Forms ------------ >
        /* Form<FormSample> formSample = formFactory.form(FormSample.class).fill(formSampleRes);
        Form<FormLC> formLC = formFactory.form(FormLC.class).fill(formLCRes);
        Form<FormMS> formMS = formFactory.form(FormMS.class).fill(formMSRes);
        Form<FormStructure> formStructure = formFactory.form(FormStructure.class).fill(formStructureRes);

        Form<StructureForms> structureFormList = formFactory.form(StructureForms.class).fill(sFormsRes);
        Form<FormTables> formTables = formFactory.form(FormTables.class).fill(formTablesRes);

        // Map for options
        Map<String, Map<String, String>> options = new LinkedHashMap<>();
        options.put("CellType", CellLine.options());
        options.put("StartingMaterial", Material.optionsStarting());
        options.put("SampleMaterial", Material.optionsSample());

        options.put("HplcManufacturer", Manufacturer.optionsHPLC());
        options.put("HplcBrand", Instrument.optionsBrand());
        options.put("ColumnManufacturer", Manufacturer.optionsColumn());
        options.put("ColumnModel", Column.options());

        options.put("InstrumentManufacturer", Manufacturer.optionsSpectrometer());
        options.put("InstrumentModel", Device.options());

        options.put("Database", Database.options());

        */

       // return ok( views.html.dataUpload.uploadresult.render(structureFormList, formMS, formSample, formLC, formStructure, sFormsRes.getList(),options, createTables(), formTables));
       // return ok( views.html.dataUpload.uploaded.render(this.userProvider));
        //routes.HomeController.mirageReport(formMSRes.getJournalReferenceID());
        return redirect(controllers.routes.HomeController.mirageReport(formMSRes.getJournalReferenceID()));
    }




    private String saveFile(Http.MultipartFormData.FilePart<File> uploadedFile){

        if (uploadedFile != null) {
            String fileName = uploadedFile.getFilename();
//            File file = uploadedFile.getFile();
//            file.renameTo(new File(configuration.underlying().getString("fileUploadPath"), fileName));
//            String filePath = configuration.underlying().getString("fileUploadPath") + fileName;

            String filePath= configuration.underlying().getString("fileUploadPath") + fileName;
            Path moveFrom = FileSystems.getDefault().getPath(uploadedFile.getFile().getPath());
            Path target = FileSystems.getDefault().getPath(filePath);
            try{
                Files.move(moveFrom,target, StandardCopyOption.REPLACE_EXISTING);

            } catch (IOException e) {
                e.printStackTrace();
            }
            return filePath;

        } else {
            flash("error", "Missing file");
            return null;
        }
    }



    // Generates a list of the Structures
    public List<StructureForm> StructureListGenerator(List<String> gwpPathList) {
        GlycanWorkspace theWorkspace = new GlycanWorkspace("/tmp/conf", true, new GlycanRendererAWT());
        List<StructureForm> structureList = new ArrayList<>();

        for(String f : gwpPathList) {
            theWorkspace.open(f, false, false);

            for (Scan scan : theWorkspace.getAllScans()) {

                Map<String,String> notes = new FileExtractor().getGwpNotes(scan.getNotes().getText());
                StructureForm structureForm = new StructureForm();
                structureForm.setScanMS(notes.get("scan ms"));
                structureForm.setScanMSMS(notes.get("scan ms/ms"));
                structureForm.setPrecursor(notes.get("precursor"));
                structureForm.setRetentionTime(notes.get("retention time"));
                structureForm.setStability(notes.get("stability"));
                structureForm.setOrthogonal(notes.get("orthogonal"));
                structureForm.setQuantity(notes.get("quantity"));
                structureForm.setQuantitationMethod(notes.get("Quantitation method"));
                structureForm.setDatabase(notes.get("database"));
                structureForm.setTaxRestrictions(notes.get("taxonomical restrictions"));
                structureForm.setOtherRestrictions(notes.get("other restrictions"));
                structureForm.setAllowedCleavages(notes.get("allowed cleavages"));
                structureForm.setAllowedCleavages(notes.get("mass accuracy"));
                structureForm.setParentError(notes.get("parent error"));
                structureForm.setFragmentError(notes.get("fragment error"));
                structureForm.setScoringMethod(notes.get("scoring method"));
                structureForm.setScoringValueFormat(notes.get("scoring value format"));
                structureForm.setScoringAlgorithm(notes.get("scoring algorithm"));
                structureForm.setScoringResult(notes.get("scoring result"));
                structureForm.setValidationStatus(notes.get("validation status"));
                structureForm.setValidationValueFormat(notes.get("validation value format"));
                structureForm.setValidationResults(notes.get("validation results"));
                structureForm.setOther(notes.get("other"));

              if (!scan.getStructures().isEmpty()){
                  org.eurocarbdb.application.glycanbuilder.Glycan glycan = org.eurocarbdb.application.glycanbuilder.Glycan.fromString(scan.getStructures().toString());

                  structureForm.setReducingMass(getComposition(glycan.toGlycoCTCondensed()).mass);
                  structureForm.setReducingEnd(getReducingEnd(scan.getStructures().toString()));
                  structureForm.setPersubstitution(getPersubstitution(scan.getStructures().toString()));
                  structureForm.setPeakLabeledList(labeledPeaks(scan.getPeakList()));
                  structureForm.setStructure(scan.getStructures().toString());
                  structureForm.setPeaks(peaksToString(scan.getPeakList()));
                  structureList.add(structureForm);
              }
            }

        }
        return structureList;
    }

    private String getReducingEnd(String structure){
        String[] parts = structure.split(",");
        ReducingEnd reducingEnd = ReducingEnd.find.where().eq("abbreviation", parts[parts.length -1]).findUnique();
        return reducingEnd.name;
    }

    private String getPersubstitution(String structure){
        String[] parts = structure.split(",");
        Persubstitution persubstitution = Persubstitution.find.where().eq("abbreviation", parts[parts.length -4]).findUnique();
        return persubstitution.name;
    }

    private List<PeakLabeled> labeledPeaks(PeakList pl){
        List<PeakLabeled> list = new ArrayList<>();

        for (int i =0;i<pl.size();i++) {

            if(pl.getIntensity(i) != 0){

                PeakLabeled p = new PeakLabeled();
                p.intensity_value = pl.getIntensity(i);
                p.mz_value = pl.getMZ(i);

                list.add(p);
            }
        }
        return list;
    }

    public String peaksToString(PeakList pl){

        StringBuilder stringBuilder = new StringBuilder();

        for (int i =0;i<pl.size();i++) {
            stringBuilder.append(String.valueOf(pl.getMZ(i)));
            stringBuilder.append("-");
            stringBuilder.append(String.valueOf(pl.getIntensity(i)));
            stringBuilder.append("!");
        }
        return stringBuilder.toString();
    }


    // Creates the objects for the table-elements
    private List<TableFragment> createTables(){

        TableFragment isoOne = new TableFragment();
        isoOne.setName("isoOne");
        isoOne.setFields(Arrays.asList(
                new TableFragmentField(1,"isoOneEnzymes","Enzymes used", Treatment.optionsIsoEnzymaticL(), "Describe any enzymes used to for the purpose of oligosaccharide removal (e.g. PNGase F) or for modification of the starting material (e.g. trypsin protease)."),
                new TableFragmentField(0,"isoOneVendor","Vendor or enzyme production", "Specify where it was obtained (vendor) or for enzymes produced in-house, describe expression and purification procedure."),
                new TableFragmentField(0,"isoOneConditions","Reaction conditions", "State if sample material was treated in-solution or immobilized (SDS-PAGE, PVDF etc.) as well as temperature, duration, volume, enzyme concentration.")
        ));

        TableFragment isoTwo = new TableFragment();
        isoTwo.setName("isoTwo");
        isoTwo.setFields(Arrays.asList(
                new TableFragmentField(1,"isoTwoChemicals","Chemical methods",Treatment.optionsIsoChemicalL(), "E.g. (hydrazinolysis, β-elimination etc.)"),
                new TableFragmentField(0,"isoTwoConditions","Reaction conditions")
        ));

        TableFragment modOne = new TableFragment();
        modOne.setName("modOne");
        modOne.setFields(Arrays.asList(
                new TableFragmentField(1,"modOneEnzymes","Enzymes used",Treatment.optionsModEnzymaticL()),
                new TableFragmentField(0,"modOneConditions","Reaction conditions"),
                new TableFragmentField(0,"modOneCOrigin","Origin of novel enzyme")
        ));

        TableFragment modTwo = new TableFragment();
        modTwo.setName("modTwo");
        modTwo.setFields(Arrays.asList(
                new TableFragmentField(1,"modTwoChemicals","Chemical methods",Treatment.optionsModChemicalL()),
                new TableFragmentField(0,"modTwoModification","Type of modification", "(e.g. hydrolysis, sample tagging (including fluorescent labels), isotopic labelling, permethylation/peracetylation, etc.)."),
                new TableFragmentField(0,"modTwoConditions","Reaction conditions")
        ));

        TableFragment softwareOne = new TableFragment();
        softwareOne.setName("softwareOne");
        softwareOne.setFields(Arrays.asList(
                new TableFragmentField(1,"softwareOneName","Software name", Software.options(), "The instrument management and data analysis package name", 150),
                new TableFragmentField(0,"softwareOneVersion","Version", 100),
                new TableFragmentField(0,"softwareOneUpgrades","Upgrades", "Upgrades not reflected in version number", 150),
                new TableFragmentField(0,"softwareOneSwitchingCriteria","Switching criteria","The list of conditions that cause the switch from survey or zoom mode (MS^1) to or tandem mode (MS^n where n > 1); e.g., ‘parent ion’ mass lists, neutral loss criteria and so on.", 110),
                new TableFragmentField(0,"softwareOneIsolation","Isolation width","For tandem instruments (i.e., multi-stage instruments such as triple quads and TOF-TOFs, plus ion traps and equivalents) the total width (i.e., not half for plus-or-minus) of the gate applied around a selected precursor ion m/z, provided for all levels or by MS level.", 100),
                new TableFragmentField(0,"softwareOneFile","Location of ‘parameters’ file", "The location and name under which the mass spectrometer’s parameter settings file for the run is stored, if available. Ideally this should be a URI including filename, or most preferably an LSID, where feasible. Location of file should be mentioned.", 170)
        ));

        TableFragment softwareTwo = new TableFragment();
        softwareTwo.setName("softwareTwo");
        softwareTwo.setFields(Arrays.asList(
                new TableFragmentField(1,"softwareTwoName","Software name", Software.options()),
                new TableFragmentField(0,"softwareTwoVersion","Version")
        ));

        TableFragment softwareThree = new TableFragment();
        softwareThree.setName("softwareThree");
        softwareThree.setFields(Arrays.asList(
                new TableFragmentField(1,"softwareThreeSoftware","Software", Software.options(), 160),
                new TableFragmentField(0,"softwareThreeName","Name", 160),
                new TableFragmentField(0,"softwareThreeFormat","Format", 160),
                new TableFragmentField(0,"softwareThreeArea","Link to the target area", 160),
                new TableFragmentField(0,"softwareThreeURL","URL", 160)
        ));

        TableFragment softwareFour = new TableFragment();
        softwareFour.setName("softwareFour");
        softwareFour.setFields(Arrays.asList(
                new TableFragmentField(1,"softwareFourSoftware","Software Name", Software.options()),
                new TableFragmentField(0,"softwareFourVersion","Version"),
                new TableFragmentField(0,"softwareFourCustomizations","Customizations", "Customizations made to the software"),
                new TableFragmentField(0,"softwareFourSettings","Software settings")
        ));

        TableFragment softwareFive = new TableFragment();
        softwareFive.setName("softwareFive");
        softwareFive.setFields(Arrays.asList(
                new TableFragmentField(1,"softwareFiveSoftware","Software", Software.options()),
                new TableFragmentField(0,"softwareFiveName","Name"),
                new TableFragmentField(0,"softwareFiveFormat","Format"),
                new TableFragmentField(0,"softwareFiveURL","URL")
        ));

        TableFragment softwareSix = new TableFragment();
        softwareSix.setName("softwareSix");
        softwareSix.setFields(Arrays.asList(
                new TableFragmentField(1,"softwareSixName","Software name",  Software.options(), 160),
                new TableFragmentField(0,"softwareSixVersion","Version", 160),
                new TableFragmentField(0,"softwareSixType","Software type", 160),
                new TableFragmentField(0,"softwareSixCustomizations","Customizations","Customizations made to the software", 160),
                new TableFragmentField(0,"softwareSixSettings","Software settings", 160)
        ));

        TableFragment softwareSeven = new TableFragment();
        softwareSeven.setName("softwareSeven");
        softwareSeven.setFields(Arrays.asList(
                new TableFragmentField(1,"softwareSevenSoftware","Software",  Software.options()),
                new TableFragmentField(0,"softwareSevenName","Name"),
                new TableFragmentField(0,"softwareSevenFormat","Format"),
                new TableFragmentField(0,"softwareSevenURL","URL")
        ));

        return Arrays.asList(isoOne, isoTwo, modOne, modTwo, softwareOne, softwareTwo, softwareThree, softwareFour, softwareFive, softwareSix, softwareSeven);
    }

    // Populated form for tables
    private FormTables populatedFormTables(FormSample sample, FormMS ms){
        FormTables form = new FormTables();

        // from FormSample
        form.setIsoOneEnzymes(sample.getIsoOneEnzymes());
        form.setIsoOneVendor(sample.getIsoOneVendor());
        form.setIsoOneConditions(sample.getIsoOneConditions());
        form.setIsoTwoChemicals(sample.getIsoTwoChemicals());
        form.setIsoTwoConditions(sample.getIsoTwoConditions());
        form.setModOneEnzymes(sample.getModOneEnzymes());
        form.setModOneConditions(sample.getModOneConditions());
        form.setModOneCOrigin(sample.getModOneCOrigin());
        form.setModTwoChemicals(sample.getModTwoChemicals());
        form.setModTwoModification(sample.getModTwoModification());
        form.setModTwoConditions(sample.getModTwoConditions());

        // From FormMS
        form.setSoftwareOneName(ms.getSoftwareOneName());
        form.setSoftwareOneVersion(ms.getSoftwareOneVersion());
        form.setSoftwareOneUpgrades(ms.getSoftwareOneUpgrades());
        form.setSoftwareOneSwitchingCriteria(ms.getSoftwareOneSwitchingCriteria());
        form.setSoftwareOneIsolation(ms.getSoftwareOneIsolation());
        form.setSoftwareOneFile(ms.getSoftwareOneFile());

        form.setSoftwareTwoName(ms.getSoftwareTwoName());
        form.setSoftwareTwoVersion(ms.getSoftwareTwoVersion());

        form.setSoftwareThreeSoftware(ms.getSoftwareThreeSoftware());
        form.setSoftwareThreeName(ms.getSoftwareThreeName());
        form.setSoftwareThreeFormat(ms.getSoftwareThreeFormat());
        form.setSoftwareThreeArea(ms.getSoftwareThreeArea());
        form.setSoftwareThreeURL(ms.getSoftwareThreeURL());

        form.setSoftwareFourSoftware(ms.getSoftwareFourSoftware());
        form.setSoftwareFourVersion(ms.getSoftwareFourVersion());
        form.setSoftwareFourCustomizations(ms.getSoftwareFourCustomizations());
        form.setSoftwareFourSettings(ms.getSoftwareFourSettings());

        form.setSoftwareFiveSoftware(ms.getSoftwareFiveSoftware());
        form.setSoftwareFiveName(ms.getSoftwareFiveName());
        form.setSoftwareFiveFormat(ms.getSoftwareFiveFormat());
        form.setSoftwareFiveURL(ms.getSoftwareFiveURL());

        form.setSoftwareSixName(ms.getSoftwareSixName());
        form.setSoftwareSixVersion(ms.getSoftwareSixVersion());
        form.setSoftwareSixType(ms.getSoftwareSixType());
        form.setSoftwareSixCustomizations(ms.getSoftwareSixCustomizations());
        form.setSoftwareSixSettings(ms.getSoftwareSixSettings());

        form.setSoftwareSevenSoftware(ms.getSoftwareSevenSoftware());
        form.setSoftwareSevenName(ms.getSoftwareSevenName());
        form.setSoftwareSevenFormat(ms.getSoftwareSevenFormat());
        form.setSoftwareSevenURL(ms.getSoftwareSevenURL());

        return form;
    }

    public GlycanComposition getComposition(String sequenceCTCond) {
        String[] arrayCT = sequenceCTCond.split("\\n");
        GlycanComposition gC = new GlycanComposition();

        int sulf =0;
        int hexNac =0;
        int hex =0;
        int dHex =0;
        int totalHex=0;
        int neuGc=0;

        int glycolyl=0;
        int gluronic=0;
        int neuAc =0;
        int pento=0;
//        double mass =0.0;


        for(int i=0;i<arrayCT.length;i++){

            if (arrayCT[i].indexOf("HEX") != -1) {
                hex++;
            }

            if (arrayCT[i].indexOf("acetyl") != -1) {
                hexNac++;
            }

            if (arrayCT[i].indexOf("sulfate") != -1) {
                sulf++;
            }

            if (arrayCT[i].indexOf("lgal") != -1) {
                dHex++;
            }

            if (arrayCT[i].indexOf("dgro-dgal-NON") != -1) {
                neuGc++;
            }

            if (arrayCT[i].indexOf("glycolyl") != -1) {
                glycolyl++;
            }

            if (arrayCT[i].indexOf("HEX-1:5|6:a") != -1) {   /**Achtung**/
                gluronic++;
            }

            if (arrayCT[i].indexOf("dxyl") != -1) {
                pento++;
            }


        }
        neuAc = (neuGc-glycolyl);
        totalHex = ((hex - hexNac)-dHex)-gluronic + neuAc;
        hexNac= hexNac - neuAc;

        gC.pen=pento;
        gC.dhex=dHex;
        gC.hex=totalHex;
        gC.hexnac=hexNac;
        gC.neuac=neuAc;
        gC.neugc=glycolyl;
        gC.sulf=sulf;
        gC.gluro=gluronic;
        gC.setMass();

        return gC;
    }
}
