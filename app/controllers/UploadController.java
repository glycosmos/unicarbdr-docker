package controllers;

import com.feth.play.module.pa.PlayAuthenticate;
import models.*;
import play.Configuration;
import play.data.FormFactory;
import play.mvc.*;
import service.UserProvider;
import views.html.*;

import javax.inject.Inject;

public class UploadController extends Controller {
    private final PlayAuthenticate auth;
    private final UserProvider userProvider;


    @Inject
    public UploadController(final PlayAuthenticate auth, final UserProvider userProvider) {
        this.auth = auth;
        this.userProvider = userProvider;

    }


    public Result upload() {
        return ok(views.html.upload.upload.render(this.userProvider));
    }
}