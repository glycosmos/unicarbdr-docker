package controllers;


import models.core.GlycanSequence;
import play.mvc.Controller;
import play.mvc.Result;
import org.eurocarbdb.application.glycanbuilder.*;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;


public class ImageController extends Controller {

    public synchronized Result showImage(String structure, String notation, String style) throws Exception {

        BuilderWorkspace workspace;
        workspace = new BuilderWorkspace("/tmp/config", true, new GlycanRendererAWT());

        org.eurocarbdb.application.glycanbuilder.Glycan glycan = org.eurocarbdb.application.glycanbuilder.Glycan.fromString(structure);

        if (notation == null || notation.isEmpty() || notation.equalsIgnoreCase("cfg"))
            workspace.setNotation(GraphicOptions.NOTATION_CFG);
        else if (notation.equalsIgnoreCase("uoxf"))
            workspace.setNotation(GraphicOptions.NOTATION_UOXF);
        else if (notation.equalsIgnoreCase("iupac"))
            workspace.setNotation(GraphicOptions.NOTATION_TEXT);
        else
            throw new IllegalArgumentException("Notation " + notation + " is not supported");


        if (style.equalsIgnoreCase("normal"))
            workspace.setDisplay(GraphicOptions.DISPLAY_NORMAL);
        else
            workspace.setDisplay(GraphicOptions.DISPLAY_NORMALINFO);



        ByteArrayOutputStream byteArrayOutputStream= new ByteArrayOutputStream();

        BufferedImage img = workspace.getGlycanRenderer().getImage(glycan, true, false, true, 2);

        ImageIO.write(img, "png", byteArrayOutputStream);

        return ok(byteArrayOutputStream.toByteArray()).as("image/png");


    }



    public synchronized Result showImageId(Long id, String notation, String style) throws Exception {

        BuilderWorkspace workspace;
        workspace = new BuilderWorkspace("/tmp/config", true, new GlycanRendererAWT());

        String structure = GlycanSequence.find.byId(id).sequence_gws;

        org.eurocarbdb.application.glycanbuilder.Glycan glycan = org.eurocarbdb.application.glycanbuilder.Glycan.fromString(structure);

        if (notation == null || notation.isEmpty() || notation.equalsIgnoreCase("cfg"))
            workspace.setNotation(GraphicOptions.NOTATION_CFG);
        else if (notation.equalsIgnoreCase("uoxf"))
            workspace.setNotation(GraphicOptions.NOTATION_UOXF);
        else if (notation.equalsIgnoreCase("iupac"))
            workspace.setNotation(GraphicOptions.NOTATION_TEXT);
        else
            throw new IllegalArgumentException("Notation " + notation + " is not supported");


        if (style.equalsIgnoreCase("normal"))
            workspace.setDisplay(GraphicOptions.DISPLAY_NORMAL);
        else
            workspace.setDisplay(GraphicOptions.DISPLAY_NORMALINFO);



        ByteArrayOutputStream byteArrayOutputStream= new ByteArrayOutputStream();

        BufferedImage img = workspace.getGlycanRenderer().getImage(glycan, true, false, true, 2);

        ImageIO.write(img, "png", byteArrayOutputStream);

        return ok(byteArrayOutputStream.toByteArray()).as("image/png");


    }
}
