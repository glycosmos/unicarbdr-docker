package controllers;

import be.objectify.deadbolt.java.actions.Group;
import be.objectify.deadbolt.java.actions.Restrict;
import com.fasterxml.jackson.databind.JsonNode;
import com.feth.play.module.pa.PlayAuthenticate;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import models.*;
import models.DBviews.*;
import models.MS.PeakList;
import models.MS.PeakLabeled;
import models.MS.Scan;
import models.core.GlycanSequence;
import models.core.Journal;
import models.core.Responsible;
import models.core.JournalReference;
import models.samplePrep.*;
import org.apache.poi.util.SystemOutLogger;
import org.eurocarbdb.application.glycanbuilder.BuilderWorkspace;
import org.eurocarbdb.application.glycanbuilder.GlycanRendererAWT;
import org.eurocarbdb.application.glycoworkbench.Peak;
import play.Configuration;
import play.data.Form;
import play.data.FormFactory;
import play.libs.Json;
import play.mvc.*;
import providers.MyUsernamePasswordAuthProvider;
import service.FormPubMed;
import service.UserProvider;
import src.PairWrapper;
import views.html.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


//import javax.management.StringValueExp;
import javax.inject.Inject;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.*;


public class HomeController extends Controller {
    private FormFactory formFactory;
    private final Logger msLogger = LoggerFactory.getLogger(this.getClass());

    @Inject
    public HomeController(final PlayAuthenticate auth, final MyUsernamePasswordAuthProvider provider,
                          final UserProvider userProvider, FormFactory formFactory) {
        this.auth = auth;
        this.provider = provider;
        this.userProvider = userProvider;
        this.formFactory = formFactory;
    }



    public Result index() {
        int referenceCount = JournalReference.getCount();
        int[] scanCounts = Scan.getCounts();
        int[] spCounts = Sample.getCounts();
        return ok(index.render(referenceCount, scanCounts[0], scanCounts[1], spCounts[0], spCounts[1], this.userProvider));
    }

    public Result about() {
        return ok(about.render(this.userProvider));
    }


    public Result listReferences(int page, String sortBy, String order, String filter) {
        return ok(views.html.references.listReferences.render(vReferences.page(page, 10, sortBy, order, filter), sortBy, order, filter, this.userProvider));
    }

    public Result referenceDisplay(Long id) throws Exception {
        List<vBCToJournal> bioJ = vBCToJournal.findbyJournalId(id);
        List<vReferenceDisplay> vRefD = vReferenceDisplay.findByJournalId(id);
        for(vReferenceDisplay gwsStr: vRefD){
            gwsStr.setCharges();
        }

/*        views.html.references.referenceDisplay.render(
                vReferences.find.byId(id),
                vReferenceDisplay.findByJournalId(id),
                vLC.findByJournalId(id),
                bioJ,
                vMassSpec.findByJournalId(id), this.userProvider
        )*/


        return ok(
                views.html.references.referenceDisplay.render(
                        vReferences.find.byId(id),
                        vRefD,
                        vLC.findByJournalId(id),
                        bioJ,
                        vMassSpec.findByJournalId(id), this.userProvider
                )
        );
    }

    public Result msData(Long id) {
        Scan scan =Scan.find.byId(id);
	msLogger.error("scan={}", scan.scan_id);
	msLogger.error("scan.glycanSequence={}", scan.glycanSequence);
	msLogger.error("scan.glycanSequence.ID={}", scan.glycanSequence.glycan_sequence_id);
	msLogger.error("scan.peakListScan.ID={}", scan.peakListScan.peak_list_id);
        PeakList plscan = PeakList.findById(scan.peakListScan.peak_list_id.intValue());
	msLogger.error("peakList.ID={}", plscan.peak_list_id);
	msLogger.error("peakList.scanID={}", plscan.scan.scan_id);
        //List<PeakLabeled> pL= scan.peakListScan.peakLabList;
	msLogger.error("peaklist.peakLabList.ID={}", plscan.peakLabList);
        List<PeakLabeled> pL= plscan.peakLabList;
	msLogger.error("peaklist={}", pL);
	long journalRef = -1;
        String seqGWS = "";
	try {
            Collections.sort(pL, Comparator.nullsLast(PeakLabeled.StuMzComparator));
            //long journalRef=scan.journalReference.journal_reference_id;
            if (scan.journalReference != null) {
                journalRef=scan.journalReference.journal_reference_id;
            } 
            //journalRef=scan.sampleToJournalReference.journalReference.journal_reference_id;
	    msLogger.error("journalRef.ID={}", journalRef);
            // KFK 2020/2/18
            seqGWS = GlycanSequence.findById(scan.glycanSequence.glycan_sequence_id.intValue()).sequence_gws;
	    msLogger.error("glycanSequence={}", seqGWS);

        //                views.html.msData.render(
//                        vReferences.find.byId(journalRef),
//                        scan, pL,
//                        vStructureDetails.find.byId(id),
//                        simSpectraPeaks(pL, scan.scan_id, scan.peakListScan.base_peak_mz),
//                        vBiologicalContext.find.byId(id),
//                        vLC.findByJournalId(journalRef),
//                       vMassSpec.findByJournalId(journalRef),
//                        vMirageStructures.find.byId(id),
//                        this.userProvider
//                )

	} catch (Throwable t) {
		msLogger.error("Exception in sorting msData", t);
		return notFound("<h1>MS Data for "+id+" was not found</h1>Please go back to the previous page.").as("text/html");
	}

        GWPController gwpc= new GWPController();

        return ok(
                views.html.msData.render(
                        vReferences.find.byId(journalRef),
                        scan, pL,
                        //gwpc.getMass(scan.glycanSequence.sequence_gws),
                        //gwpc.getDerivatization(scan.glycanSequence.sequence_gws),
                        //gwpc.getReducingEnd(scan.glycanSequence.sequence_gws),
                        seqGWS,
                        gwpc.getMass(seqGWS),
                        gwpc.getDerivatization(seqGWS),
                        gwpc.getReducingEnd(seqGWS),

                        simSpectraPeaks(pL, scan.scan_id, scan.peakListScan.base_peak_mz),
                        vBiologicalContext.find.byId(id),
                        vLC.findByJournalId(journalRef),
                        vMassSpec.findByJournalId(journalRef),
                        vMirageStructures.findByScanId(id),
                        this.userProvider
                )

        );
    }




    public Result format(String notation) {
        session("notation", notation);
        String refererUrl = request().getHeader("referer");
        return redirect(refererUrl);
    }


    public Map<PairWrapper, Double> simSpectraPeaks(List<PeakLabeled> pLabeled, long id, double basePeak) {
        Map<PairWrapper, Double> sortedMap = new HashMap<PairWrapper, Double>();

        String peaks = "";
        for(PeakLabeled pl: pLabeled){
            peaks += String.valueOf(pl.mz_value) + "\t" + String.valueOf(pl.intensity_value)+ "\n";
        }

        try {
            sortedMap = SearchController.searchMSMSId(id, basePeak, peaks);
        }catch (IOException e) {
            System.out.println("Error: " + e);
        }
        return sortedMap;
    }



    public Result library() {
        return ok(
                views.html.downloads.libraries.render(this.userProvider)
        );
    }


    public Result allStructuresToJson()
    {
        List<vStructureJson> strJson = vStructureJson.find.all();
        return ok(Json.toJson(strJson));
    }


    public Result testOne() {  //Autocomplete -biological context -

        return ok(
                views.html.tests.autoComplete.render(this.userProvider)
        );
    }

    public Result testTwo() {   //- database and files for MIRAGE -
        return ok(
                views.html.tests.databaseFile.render(this.userProvider)
        );
    }


    public Result mirageReport(long id) {

        List<vMirageSample> mirageSample = vMirageSample.findByJournalId(id);
        vJournalResponsible mirageJRef = vJournalResponsible.findByJournalId(id);
        vMirageLc mirageLc = vMirageLc.findByJournalId(id);
        vMirageMs mirageMs = vMirageMs.findByJournalId(id);
        List<vMirageSoftware> mirageSoft = vMirageSoftware.findByJournalId(id);


        return ok(
                views.html.mirageReport.report.render(mirageSample, mirageJRef, mirageLc, mirageMs, mirageSoft, this.userProvider)
        );

    }




////////  Authentication ****************************



    public static final String FLASH_MESSAGE_KEY = "message";
    public static final String FLASH_ERROR_KEY = "error";
    public static final String USER_ROLE = "user";

    private final PlayAuthenticate auth;

    private final MyUsernamePasswordAuthProvider provider;

    private final UserProvider userProvider;

    public static String formatTimestamp(final long t) {
        return new SimpleDateFormat("yyyy-dd-MM HH:mm:ss").format(new Date(t));
    }

/*    public Result index() {
        int referenceCount = JournalReference.getCount();
        int[] scanCounts = Scan.getCounts();
        int[] bcCounts = BiologicalContext.getCounts();
        return ok(index.render(referenceCount, scanCounts[0], scanCounts[1], bcCounts[0], bcCounts[1]));
    }
*/


    @Restrict(@Group(HomeController.USER_ROLE))
    public Result uploadIndex() {
        Form<FormPubMed> pubMedForm = formFactory.form(FormPubMed.class);
        final User localUser = this.userProvider.getUser(session());
        return ok(views.html.dataUpload.index.render(this.userProvider, localUser,pubMedForm));
    }

    @Restrict(@Group(HomeController.USER_ROLE))
    public Result profile() {
        final User localUser = userProvider.getUser(session());
        return ok(views.html.auth.profile.render(this.auth, this.userProvider, localUser));
    }

    public Result login() {
        return ok(views.html.auth.login.render(this.auth, this.userProvider,  this.provider.getLoginForm()));
    }

    public Result doLogin() {
        com.feth.play.module.pa.controllers.Authenticate.noCache(response());
        final Form<MyUsernamePasswordAuthProvider.MyLogin> filledForm = this.provider.getLoginForm()
                .bindFromRequest();
        if (filledForm.hasErrors()) {
            // User did not fill everything properly
            return badRequest(views.html.auth.login.render(this.auth, this.userProvider, filledForm));
        } else {
            // Everything was filled
            return this.provider.handleLogin(ctx());
        }
    }

    public Result signup() {
        return ok(views.html.auth.signup.render(this.auth, this.userProvider, this.provider.getSignupForm()));
    }
/*
    public Result jsRoutes() {
        return ok(
                play.routing.JavaScriptReverseRouter.create("jsRoutes", routes.javascript.Signup.forgotPassword()))
                .as("text/javascript");

    }
*/
    public Result doSignup() {
        com.feth.play.module.pa.controllers.Authenticate.noCache(response());
        final Form<MyUsernamePasswordAuthProvider.MySignup> filledForm = this.provider.getSignupForm().bindFromRequest();



/* ******************/
        final Map<String, String[]> values = request().body().asFormUrlEncoded();
        final String gcaptchaCode = values.get("g-recaptcha-response")[0];
        String error = "-1";
//********************/


        if (filledForm.hasErrors()) {
            // User did not fill everything properly
            return badRequest(views.html.auth.signup.render(this.auth, this.userProvider, filledForm));
        } else {



//'''''''''''''''''
            if (gcaptchaCode == null || gcaptchaCode.isEmpty()) {
		msLogger.error("gcaptchacode=*{}*", gcaptchaCode);
                flash("error", "You need to successfully solve the reCAPTCHA at the bottom of the form in order to signup.");
                return badRequest(views.html.auth.signup.render(this.auth, this.userProvider, filledForm));
            }

            // Find out if Google likes the Captcha
            String json = captchaResp(gcaptchaCode);
		msLogger.error("json=*{}*", json);

            // Check if an error occured while contacting Google and processing
            if (json.equals(error)) {
                flash("error", "An error occured while attempting to resolve the Google Captcha. Try again?");
                return badRequest(views.html.auth.signup.render(this.auth, this.userProvider, filledForm));
            }

            // Turn the json string into a Json object
            JsonNode jobj = Json.parse(json);
            Boolean captchaPassed = jobj.findPath("success").booleanValue();
		msLogger.error("captchapassed=*{}*", captchaPassed);

/*
            if (captchaPassed) {
                return this.provider.handleSignup(ctx());

            } else {
                // Error codes are in jobj.findPath("error-codes").textValue();
                flash("error", "Some error occurred. Error code "+jobj.findPath("error-codes").textValue());
                return badRequest(views.html.auth.signup.render(this.auth, this.userProvider, filledForm));
            }
*/

//'''''''''''''''''
//
            return this.provider.handleSignup(ctx());


        }
    }


    public String captchaResp(String gcaptchaCode) {
        String googUrl = "https://www.google.com/recaptcha/api/siteverify";
        String encSecret = "";
        String encCapcode = "";
        String error = "-1";
        URL url = null;

        Config conf = ConfigFactory.load();
        String gsecretKey = conf.getString("gcaptcha.gsecretKey");

        try {
            encSecret = URLEncoder.encode(gsecretKey, "UTF-8");
            encCapcode = URLEncoder.encode(gcaptchaCode, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return error;
        }
        String query = "secret=" + encSecret + "&response=" + encCapcode;
        try {
            url = new URL(googUrl + "?" + query);
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return error;
        }

        StringBuilder stringBuilder = new StringBuilder();
        try {
            // Check if Google validates the captcha response
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Accept", "application/json");
            // 10 seconds max to respond
            connection.setReadTimeout(10 * 1000);
            connection.connect();
            if (connection.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + connection.getResponseCode());
            }
            // read the output
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String line = null;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line);
            }
            return stringBuilder.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return error;
    }



}
