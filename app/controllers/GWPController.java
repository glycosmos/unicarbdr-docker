package controllers;


import models.core.GlycanSequence;
import play.mvc.Controller;
import play.mvc.Result;
import org.eurocarbdb.application.glycanbuilder.*;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GWPController extends Controller {
    private final Logger gwpLogger = LoggerFactory.getLogger(this.getClass());

    BuilderWorkspace workspace = new BuilderWorkspace("/tmp/config", true, new GlycanRendererAWT());

    public synchronized Result showImage(String structure, String notation, String style) throws Exception {
        org.eurocarbdb.application.glycanbuilder.Glycan glycan = org.eurocarbdb.application.glycanbuilder.Glycan.fromString(structure);
        if (notation == null || notation.isEmpty() || notation.equalsIgnoreCase("cfg"))
            workspace.setNotation(GraphicOptions.NOTATION_CFG);
        else if (notation.equalsIgnoreCase("uoxf"))
            workspace.setNotation(GraphicOptions.NOTATION_UOXF);
        else if (notation.equalsIgnoreCase("iupac"))
            workspace.setNotation(GraphicOptions.NOTATION_TEXT);
        else
            throw new IllegalArgumentException("Notation " + notation + " is not supported");


        if (style.equalsIgnoreCase("normal"))
            workspace.setDisplay(GraphicOptions.DISPLAY_NORMAL);
        else
            workspace.setDisplay(GraphicOptions.DISPLAY_NORMALINFO);



        ByteArrayOutputStream byteArrayOutputStream= new ByteArrayOutputStream();

        BufferedImage img = workspace.getGlycanRenderer().getImage(glycan, true, false, true, 2);

        ImageIO.write(img, "png", byteArrayOutputStream);

        return ok(byteArrayOutputStream.toByteArray()).as("image/png");


    }



    public synchronized Result showImageId(Long id, String notation, String style) throws Exception {


        String structure = GlycanSequence.find.byId(id).sequence_gws;

        org.eurocarbdb.application.glycanbuilder.Glycan glycan = org.eurocarbdb.application.glycanbuilder.Glycan.fromString(structure);

        if (notation == null || notation.isEmpty() || notation.equalsIgnoreCase("cfg"))
            workspace.setNotation(GraphicOptions.NOTATION_CFG);
        else if (notation.equalsIgnoreCase("uoxf"))
            workspace.setNotation(GraphicOptions.NOTATION_UOXF);
        else if (notation.equalsIgnoreCase("iupac"))
            workspace.setNotation(GraphicOptions.NOTATION_TEXT);
        else
            throw new IllegalArgumentException("Notation " + notation + " is not supported");


        if (style.equalsIgnoreCase("normal"))
            workspace.setDisplay(GraphicOptions.DISPLAY_NORMAL);
        else
            workspace.setDisplay(GraphicOptions.DISPLAY_NORMALINFO);



        ByteArrayOutputStream byteArrayOutputStream= new ByteArrayOutputStream();

        BufferedImage img = workspace.getGlycanRenderer().getImage(glycan, true, false, true, 2);

        ImageIO.write(img, "png", byteArrayOutputStream);

        return ok(byteArrayOutputStream.toByteArray()).as("image/png");

    }



    public double getMass(String seqGWS) {
        gwpLogger.error("getting mass for {}", seqGWS);
        try {
          
            org.eurocarbdb.application.glycanbuilder.Glycan glycan = org.eurocarbdb.application.glycanbuilder.Glycan.fromString(seqGWS);
            return glycan.computeMass()-2;
        } catch (Exception e) {
            gwpLogger.error("Exception getting glycan and computing mass");
            
        }
        return 0;
    }

    public String getDerivatization(String seqGWS) {
        String result;
        org.eurocarbdb.application.glycanbuilder.Glycan glycan = org.eurocarbdb.application.glycanbuilder.Glycan.fromString(seqGWS);
        String deriv = glycan.getMassOptions().DERIVATIZATION;


        switch (deriv) {
            case "Und":
                result = "Underivatised";
                break;
            case "perMe":
                result = "Per-methylation";
                break;
            case "perDMe":
                result = "Per-deuteromethylation";
                break;
            case "perAc":
                result = "Per-acetylation";
                break;
            case "perDac":
                result = "Per-deuteroacetylation";
                break;
            default:
                throw new IllegalArgumentException("Invalid reducing end: " + seqGWS);
        }
        return result;
    }


    public String getReducingEnd(String seqGWS) {
        String result;
        org.eurocarbdb.application.glycanbuilder.Glycan glycan = org.eurocarbdb.application.glycanbuilder.Glycan.fromString(seqGWS);
        String redEnd= glycan.getMassOptions().getReducingEndTypeString();


        switch (redEnd) {
            case "freeEnd":
                result = "Free reducing end";
                break;
            case "redEnd":
                result = "Reduced";
                break;
            case "PA":
                result = "2-Aminopyridine";
                break;
            case "2AP":
                result = "2-Aminopyridine";
                break;
            case "2AB":
                result = "2-Aminobenzamide";
                break;
            case "AA":
                result = "Anthranilic Acid";
                break;
            case "DAP":
                result = "2,6-Diaminopyridine";
                break;
            case "4AB":
                result = "4-Aminobenzamidine";
                break;
            case "DAPMAB":
                result = "4-(N-[2,4-Diamino-6-pteridinylmethyl]amino)benzoic acid";
                break;
            case "AMC":
                result = "7-Amino-4-methylcoumarin";
                break;
            case "6AQ":
                result = "6-Aminoquinoline";
                break;
            case "2AAc":
                result = "2-Aminoacridone";
                break;
            case "FMC":
                result = "9-Fluorenylmethyl carbazate";
                break;
            case "DH":
                result = "Dansylhydrazine";
                break;
            default:
                throw new IllegalArgumentException("Invalid reducing end: " + seqGWS);
        }
        return result;
    }










}


