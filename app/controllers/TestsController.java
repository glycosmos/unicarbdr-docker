package controllers;

import models.core.GlycanComposition;
import play.mvc.Controller;
import play.mvc.Result;


public class TestsController extends Controller {

    private double HEXNAC = 203.0794;
    private double HEX = 162.0528;
    private double DHEX= 146.0579;
    private double NEUAC= 291.0954;
    private double NEUGC=307.0903;
    private double SULF=79.9568;
    private double GLURO=176.03209;
    private double REDEND = 18.0105546;
    private double PENTO = 132.0423;
    private double METHYL = 14.0271;


    public Result composition(){

        GlycanComposition gt = getComposition("RES\n" +
                "1b:o-dgal-HEX-0:0|1:aldi\n" +
                "2s:n-acetyl\n" +
                "3b:b-dgal-HEX-1:5\n" +
                "4b:a-dgro-dgal-NON-2:6|1:a|2:keto|3:d\n" +
                "5s:n-acetyl\n" +
                "6b:b-dglc-HEX-1:5\n" +
                "7s:n-acetyl\n" +
                "LIN\n" +
                "1:1d(2+1)2n\n" +
                "2:1o(3+1)3d\n" +
                "3:3o(3+2)4d\n" +
                "4:4d(5+1)5n\n" +
                "5:1o(6+1)6d\n" +
                "6:6d(2+1)7n");

        System.out.println("Mass -> " + gt.mass);
        System.out.println("Hex -> " + gt.hex);
        System.out.println("dHex -> " + gt.dhex);
        System.out.println("hexnac -> " + gt.hexnac);
        System.out.println("Glucuronic acid -> " + gt.gluro);
        System.out.println("neuraminic acid -> " + gt.neuac);
        System.out.println("Pentose -> " + gt.pen);
        System.out.println("Sulfates -> " + gt.sulf);

        return ok();
    }



    public GlycanComposition getComposition(String sequenceCTCond) {
        String[] arrayCT = sequenceCTCond.split("\\n");
        GlycanComposition gC = new GlycanComposition();

        int sulf =0;
        int hexNac =0;
        int hex =0;
        int dHex =0;
        int totalHex=0;
        int neuGc=0;

        int glycolyl=0;
        int gluronic=0;
        int neuAc =0;
        int pento=0;
//        double mass =0.0;


        for(int i=0;i<arrayCT.length;i++){

            if (arrayCT[i].indexOf("HEX") != -1) {
                hex++;
            }

            if (arrayCT[i].indexOf("acetyl") != -1) {
                hexNac++;
            }

            if (arrayCT[i].indexOf("sulfate") != -1) {
                sulf++;
            }

            if (arrayCT[i].indexOf("lgal") != -1) {
                dHex++;
            }

            if (arrayCT[i].indexOf("dgro-dgal-NON") != -1) {
                neuGc++;
            }

            if (arrayCT[i].indexOf("glycolyl") != -1) {
                glycolyl++;
            }

            if (arrayCT[i].indexOf("HEX-1:5|6:a") != -1) {   /**Achtung**/
                gluronic++;
            }

            if (arrayCT[i].indexOf("dxyl") != -1) {
                pento++;
            }


        }
        neuAc = (neuGc-glycolyl);
        totalHex = ((hex - hexNac)-dHex)-gluronic + neuAc;
        hexNac= hexNac - neuAc;

        gC.pen=pento;
        gC.dhex=dHex;
        gC.hex=totalHex;
        gC.hexnac=hexNac;
        gC.neuac=neuAc;
        gC.neugc=glycolyl;
        gC.sulf=sulf;
        gC.gluro=gluronic;
        gC.setMass();

        return gC;
    }





/*
    private Double calcMass(double numRes, int typeRes) {

        double mass=0.0;
        switch (typeRes) {
            case 1:             //hexnac
                mass= numRes * HEXNAC;
                break;
            case 2:             //hexoses
                mass= numRes * HEX;
                break;
            case 3:             //dhex
                mass= numRes * DHEX;
                break;
            case 4:             //NeuAc
                mass= numRes * NEUAC;
                break;
            case 5:             //NeuGc
                mass= numRes * NEUGC;
                break;
            case 6:             //Sulfates
                mass= numRes * SULF;
                break;
            case 7:             //Glucuronic acid
                mass= numRes * GLURO;
                break;
            case 8:             //Pentose - Xylose
                mass= numRes * PENTO;
                break;

        }

        return mass;
    }

*/

}


