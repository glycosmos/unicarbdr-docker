package controllers;

import com.feth.play.module.pa.PlayAuthenticate;
import models.DBviews.vAdvancedSearch;
import models.MS.PeakList;
import models.MS.Scan;
import models.DBviews.vSimilarSpectra;
import models.MS.Persubstitution;
import models.MS.ReducingEnd;
import models.samplePrep.BiologicalContext;
import models.samplePrep.Sample;
import play.mvc.Controller;
import play.mvc.Result;
import models.*;
import play.data.DynamicForm;
import play.data.Form;
import play.data.FormFactory;
import service.UserProvider;
import src.Util;
//import views.html.references.listReferences;
import views.html.search.*;
import views.html.search.results.*;

import javax.inject.Inject;
import java.io.IOException;
import java.util.*;

import src.PairWrapper;
import org.expasy.mzjava.core.ms.AbsoluteTolerance;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.expasy.mzjava.core.ms.spectrasim.NdpSimFunc;
import org.expasy.mzjava.core.ms.spectrasim.SimFunc;

import service.UserProvider;

import static models.DBviews.vReferences.findReferencesList;


public class SearchController extends Controller {
    private FormFactory formFactory;

    private final PlayAuthenticate auth;
    private final UserProvider userProvider;

    @Inject
    public SearchController(FormFactory formFactory, final PlayAuthenticate auth, final UserProvider userProvider) {
        this.formFactory = formFactory;
        this.auth = auth;
        this.userProvider = userProvider;

    }


    public Result search() {


        List<Map<String,String>> taxTissue = Sample.findTaxTissueList();
        Map<String,String> taxon = taxTissue.get(0);
        Map<String,String> tissue = taxTissue.get(1);


        List<String> refs = findReferencesList();
        Form<vAdvancedSearch> frAdvanced = formFactory.form(vAdvancedSearch.class);
        Form<SearchMSMSForm> frMSMS = formFactory.form(SearchMSMSForm.class);

        return ok(searchIndex.render(Persubstitution.options(), ReducingEnd.options(), taxon, tissue, refs, frAdvanced, frMSMS, this.userProvider));

    }



    public Result searchAdvanced(){
        Form<vAdvancedSearch> frmAdvanced = formFactory.form(vAdvancedSearch.class).bindFromRequest();
        return ok(resAdvanced.render(vAdvancedSearch.findByParams(frmAdvanced.data()),this.userProvider));
    }


    public Result searchBasic(){
        Form<vAdvancedSearch> frmAdvanced = formFactory.form(vAdvancedSearch.class).bindFromRequest();
        Map<String, String> params = frmAdvanced.data();

        List<String> values = new ArrayList<>();

        for (Object value : params.values()) {
            values.add(value.toString());
        }

        return ok(resAdvanced.render(vAdvancedSearch.findByParamsBasic(values),this.userProvider));
    }



    public  Result searchMSMS() throws IOException {
        DynamicForm bindedForm = formFactory.form().bindFromRequest();
        Map<PairWrapper, Double> resultMap = new HashMap<>();
        Double parentMass=0.0;
        Double error=0.01;
        Double fragError=0.0;

        try {
            org.expasy.mzjava.core.ms.peaklist.PeakList<PeakAnnotation> peakListQuery = readPeaks(String.valueOf(bindedForm.get("peaks")).replace(",","."));

            if(bindedForm.get("precursor").length()>0 && !bindedForm.get("precursor").isEmpty()) {
                parentMass = Util.getDoubleParam(bindedForm.get("precursor"));
            }
            error = Util.formatError(parentMass, bindedForm.get("errorType"), Util.getDoubleParam(bindedForm.get("error")));
            fragError = Util.formatError(parentMass, bindedForm.get("frErrorType"), Util.getDoubleParam(bindedForm.get("frError")));

            if(fragError<0.2)
                fragError=0.2;

            AbsoluteTolerance fragmentTolerance = new AbsoluteTolerance(fragError);
            SimFunc<PeakAnnotation, PeakAnnotation> simFunc = new NdpSimFunc<PeakAnnotation, PeakAnnotation>(2,fragmentTolerance);
            Map<PairWrapper, org.expasy.mzjava.core.ms.peaklist.PeakList> peakListMap = Scan.generatePeakLists(parentMass, error);

            for(Map.Entry<PairWrapper, org.expasy.mzjava.core.ms.peaklist.PeakList> entryMap : peakListMap.entrySet()){
                double score = simFunc.calcSimilarity(peakListQuery, entryMap.getValue());
                if(score > 0.2){
                    resultMap.put(entryMap.getKey(),score);
                }
            }

            Map<PairWrapper, Double> sortedMap = sortByScore(resultMap);


            return ok(resMSMS.render(sortedMap, this.userProvider));

        }catch (IOException e) {
            return redirect("/search");
        }

    }


    private static org.expasy.mzjava.core.ms.peaklist.PeakList readPeaks(String peakFile) throws IOException {
        Scanner scanner =  new Scanner(peakFile);
        org.expasy.mzjava.core.ms.peaklist.PeakList<PeakAnnotation> peakListQuery = new org.expasy.mzjava.core.ms.peaklist.DoublePeakList<PeakAnnotation>();

        try {
            while (scanner.hasNextLine()) {
                Scanner scannerLine = new Scanner(scanner.nextLine());
                scannerLine.useDelimiter("\t");
                if (scannerLine.hasNext()) {
                    Double mz = Double.valueOf(scannerLine.next());
                    Double intensity = Double.valueOf(scannerLine.next());
                    peakListQuery.add(mz, intensity);
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return peakListQuery;
    }




    private static Map<PairWrapper, Double> sortByScore(Map<PairWrapper, Double> unsortMap) {

        // Convert Map to List
        List<Map.Entry<PairWrapper, Double>> list = new LinkedList<>(unsortMap.entrySet());

        // Sort list with comparator
        Collections.sort(list, new Comparator<Map.Entry<PairWrapper, Double>>() {
            public int compare(Map.Entry<PairWrapper, Double> o1,
                               Map.Entry<PairWrapper, Double> o2) {
                return (o1.getValue()).compareTo(o2.getValue());
            }
        });

        // Convert sorted map back to a Map
        Map<PairWrapper, Double> sortedMap = new LinkedHashMap<>();
        ListIterator<Map.Entry<PairWrapper, Double>> it = list.listIterator(list.size());
        while (it.hasPrevious()) {
            Map.Entry<PairWrapper, Double> entry = it.previous();
            sortedMap.put(entry.getKey(), entry.getValue());
        }

        return sortedMap;
    }




    public static Map<PairWrapper, Double> searchMSMSId(long id, double parentMass, String peaks) throws IOException {
        Map<PairWrapper, Double> resultMap = new HashMap<>();
        Double error=0.5;
        Double fragError=0.2;


        try {
            org.expasy.mzjava.core.ms.peaklist.PeakList<PeakAnnotation> peakListQuery = readPeaks(peaks);

            AbsoluteTolerance fragmentTolerance = new AbsoluteTolerance(fragError);
            SimFunc<PeakAnnotation, PeakAnnotation> simFunc = new NdpSimFunc<PeakAnnotation, PeakAnnotation>(2,fragmentTolerance);

            Map<PairWrapper, org.expasy.mzjava.core.ms.peaklist.PeakList> peakListMap = vSimilarSpectra.generatePeakLists(parentMass, error);

            for(Map.Entry<PairWrapper, org.expasy.mzjava.core.ms.peaklist.PeakList> entryMap : peakListMap.entrySet()){
                double score = simFunc.calcSimilarity(peakListQuery, entryMap.getValue());
                if(score > 0.5 && (entryMap.getKey().getid() != id )){
                    resultMap.put(entryMap.getKey(),score);
                }
            }

            Map<PairWrapper, Double> sortedMap = sortByScore(resultMap);
            return sortedMap;

        }catch (IOException e) {
            System.out.println("Error: " + e);
        }

        return null;
    }




    public Result searchStructures(Long id) {
        List<vAdvancedSearch> res = vAdvancedSearch.findStructures(id);

        return ok(resAdvanced.render(res,this.userProvider));

    }








}